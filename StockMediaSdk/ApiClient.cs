﻿using StockMediaSdk.Data.Model;
using StockMediaSdk.UI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;

namespace StockMediaSdk
{
    public sealed class ApiClient
    {
        #region Constants
        internal const string VideoBlocks = "https://api.videoblocks.com";
        internal const string GraphicStock = "https://api.graphicstock.com";
        internal const string AudioBlocks = "https://api.audioblocks.com";

        internal const string SearchResource = "/api/v1/stock-items/search";
        internal const string DownloadResource = "/api/v1/stock-items/download";

        internal static string PublicKey = string.Empty;
        internal static string PrivateKey = string.Empty;
        #endregion

        #region Static
        private static ApiClient _instance;
        public static ApiClient Shared => _instance;

        static ApiClient()
        {
            _instance = new ApiClient();
        }
        public static void Initialize(string publicKey, string privateKey)
        {
            PublicKey = publicKey;
            PrivateKey = privateKey;
        }
        #endregion

        private SearchDialog _searchDialog;
        private double _parentWidth;
        private double _parentHeight;

        public async Task<List<StorageFile>> ShowSearchDialogAsync(MediaType mediaType, IStorageFolder storageFolder, bool isSingleSelection = false)
        {
            _searchDialog = new SearchDialog(mediaType, storageFolder, isSingleSelection);
            _searchDialog.UpdateSize(_parentWidth, _parentHeight);
            await _searchDialog.ShowAsync();
            var downloadedFiles = _searchDialog.DownloadedFiles;
            _searchDialog = null;
            return downloadedFiles;
        }

        public void UpdateSize(double parentWidth, double parentHeight)
        {
            _parentWidth = parentWidth;
            _parentHeight = parentHeight;
            _searchDialog?.UpdateSize(_parentWidth, _parentHeight);
        }

        #region Private Methods
        private ApiClient()
        {
        }
        #endregion
    }
}
