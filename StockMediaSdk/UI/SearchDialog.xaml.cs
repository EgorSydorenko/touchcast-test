﻿using StockMediaSdk.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Storage;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace StockMediaSdk.UI
{
    public sealed partial class SearchDialog : ContentDialog
    {
        private static readonly string[] SortText = new string[]
        {
            "​Most relevant", "Most downloaded", "Highest rated",
            "Most recent", "Trending now", "Undiscovered"
        };
        private static readonly string[] VideoTypeText = new string[]
        {
            "All", "Footage", "Motion backgrounds", "Templates"
        };

        private static readonly string[] VideoQualityText = new string[]
        {
            "HD", "SD"
        };

        private static readonly string[] GraphicTypeText = new string[]
        {
            "All", "Photos", "Illustrations", "Vectors"
        };

        private static readonly string[] GraphicCategoriesText = new string[]
        {
            "All", "Design Elements", "Objects", "Business", "People",
            "Holidays And Celebrations", "Nature And Wildlife",
            "Buildings And Landmarks", "Sports And Outdoors",
            "Fictional", "Travel And Transportation",
            "Health And Science", "Backgrounds", "Cultural",
            "Beauty And Fashion"
        };

        private static readonly string[] GraphicOrientationText = new string[]
        {
            "All", "Portrait", "Square", "Landscape"
        };

        private const double DefaultWidth = 1290;
        private const double DefaultHeight = 900;
        private const double DefaultAspectRatio = DefaultWidth / DefaultHeight;
        private const double ParentPadding = 40;
        private const double DownloadingWidth = 600;
        private const double DownloadingHeight = 230;
        private const double DownloadingAspectRatio = DownloadingWidth / DownloadingHeight;
        private const int ItemRowsPerPage = 5;

        private const string NoConnectionMessageText = "NoConnectionMessageText";
        private const string NoResultMessageText = "NoResultMessageText";
        private const string MessageTitleText = "MessageTitleText";
        private const string MessageContentText = "MessageContentText";

        private readonly string NoConnectionMessage;
        private readonly string NoResultMessage;
        private readonly string MessageTitle;
        private readonly string MessageContent;

        #region Private Fields
        private readonly StockMediaQueryResult _queryResult;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly List<ItemPreview> _selectedMedia = new List<ItemPreview>();
        private readonly bool _isSingleSelection;

        private ScrollViewer _scrollViewer;
        private bool _isLoading;
        private bool _isDownloadingInProgress;
        private int _currentItems;
        private double _deltaBytes;
        private double _verticalOffset;
        #endregion

        private MediaType MediaType => (Video.IsChecked.Value ? MediaType.Video : MediaType.None) | (Photo.IsChecked.Value ? MediaType.Photo : MediaType.Audio);

        public List<StorageFile> DownloadedFiles => _queryResult.DownloadedFiles;

        #region Life Cycle
        public SearchDialog(MediaType mediaType, IStorageFolder storageFolder, bool isSingleSelection)
        {
            var resourceContext = ResourceContext.GetForViewIndependentUse();
            var resourceMap = ResourceManager.Current.MainResourceMap.GetSubtree("StockMediaSdk/Resources");
            var resourceValue = resourceMap.GetValue(NoConnectionMessageText, resourceContext);
            NoConnectionMessage = resourceValue.ValueAsString;
            resourceValue = resourceMap.GetValue(NoResultMessageText, resourceContext);
            NoResultMessage = resourceValue.ValueAsString;
            resourceValue = resourceMap.GetValue(MessageTitleText, resourceContext);
            MessageTitle = resourceValue.ValueAsString;
            resourceValue = resourceMap.GetValue(MessageContentText, resourceContext);
            MessageContent = resourceValue.ValueAsString;

            InitializeComponent();

            _isSingleSelection = isSingleSelection;

            _queryResult = new StockMediaQueryResult(storageFolder);

            if (Resources["TouchcastContentDialogStyle"] is Style touchcastContentDialogStyle)
            {
                Style = touchcastContentDialogStyle;
            }
            Opened += SearchDialog_Opened;

            Video.Visibility = ((mediaType & MediaType.Video) == MediaType.Video) ? Visibility.Visible : Visibility.Collapsed;
            Photo.Visibility = ((mediaType & MediaType.Photo) == MediaType.Photo) ? Visibility.Visible : Visibility.Collapsed;

            if ((mediaType & MediaType.Video) == MediaType.Video)
            {
                Video.IsChecked = true;
                Photo.IsChecked = false;
                VideoOptions.Visibility = Visibility.Visible;
                Video.Checked += Video_Checked;
                Video.Unchecked += Video_Unchecked;
            }

            if (mediaType == MediaType.Photo)
            {
                Photo.IsChecked = true;
                VideoOptions.Visibility = Visibility.Collapsed;
                GraphicOptions.Visibility = Visibility.Visible;
            }

            foreach (var text in SortText)
            {
                Sort.Items.Add(text);
            }
            Sort.SelectedIndex = 1;
            Sort.SelectionChanged += Sort_SelectionChanged;

            foreach (var text in VideoTypeText)
            {
                VideoType.Items.Add(text);
            }
            VideoType.SelectedIndex = 0;
            VideoType.SelectionChanged += VideoType_SelectionChanged;

            foreach (var text in VideoQualityText)
            {
                VideoQuality.Items.Add(text);
            }
            VideoQuality.SelectedIndex = 0;
            VideoQuality.SelectionChanged += VideoQuality_SelectionChanged;

            foreach (var text in GraphicTypeText)
            {
                GraphicType.Items.Add(text);
            }
            GraphicType.SelectedIndex = 0;
            GraphicType.SelectionChanged += GraphicType_SelectionChanged;

            foreach (var text in GraphicCategoriesText)
            {
                GraphicCategories.Items.Add(text);
            }
            GraphicCategories.SelectedIndex = 0;
            GraphicCategories.SelectionChanged += GraphicCategories_SelectionChanged;

            foreach (var text in GraphicOrientationText)
            {
                GraphicOrientation.Items.Add(text);
            }
            GraphicOrientation.SelectedIndex = 0;
            GraphicOrientation.SelectionChanged += GraphicOrientation_SelectionChanged;

            GraphicTransparency.Checked += GraphicTransparency_Checked;
            GraphicTransparency.Unchecked += GraphicTransparency_Unchecked;
        }

#if DEBUG
        ~SearchDialog()
        {
            System.Diagnostics.Debug.WriteLine("************** StockMediaSDK.SearchDialog Destructor ***************");
        }
#endif

        private async void SearchDialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            _queryResult.DownloadedFiles.Clear();
            await UpdateSearchResult();

            _scrollViewer = FindChild<ScrollViewer>(List, null);
            if (_scrollViewer != null)
            {
                _scrollViewer.ViewChanged += ScrollViewer_ViewChanged;
            }
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Windows.UI.Colors.Transparent);
                }
            }
        }
        #endregion

        #region Public Methods
        public void UpdateSize(double parentWidth, double parentHeight)
        {
            if (parentHeight == 0 || parentWidth == 0)
            {
                Height = DefaultHeight;
                Width = DefaultWidth;
                return;
            }

            parentWidth -= ParentPadding;
            parentHeight -= ParentPadding;
            var height = parentHeight;
            var width = parentWidth;
            var aspect = parentWidth / parentHeight;
            if (_isDownloadingInProgress)
            {
                if (height > DownloadingHeight)
                {
                    height = DownloadingHeight;
                }
                if (width > DownloadingWidth)
                {
                    width = DownloadingWidth;
                }

                if (aspect > DownloadingAspectRatio)
                {
                    width = height * DownloadingAspectRatio;
                    if (width > parentWidth)
                    {
                        width = parentWidth;
                        height = parentWidth / DownloadingAspectRatio;
                    }
                }
                else
                {
                    height = width / DownloadingAspectRatio;
                    if (height > parentHeight)
                    {
                        height = parentHeight;
                        width = height * DownloadingAspectRatio;
                    }
                }
                Container.Width = width;
                ViewBox.Width = width;
                ViewBox.Height = height;
            }
            else
            {
                if (aspect > DefaultAspectRatio)
                {
                    if (height > MaxHeight)
                    {
                        height = MaxHeight;
                    }
                    width = height * DefaultAspectRatio;
                    if (width > parentWidth)
                    {
                        width = parentWidth;
                        height = parentWidth / DefaultAspectRatio;
                    }
                }
                else
                {
                    if (width > MaxWidth)
                    {
                        width = MaxWidth;
                    }
                    height = width / DefaultAspectRatio;
                    if (height > parentHeight)
                    {
                        height = parentHeight;
                        width = height * DefaultAspectRatio;
                    }
                }
                Height = height;
                Width = width;
            }
        }
        #endregion

        #region Callbacks
        private async void GraphicOrientation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void GraphicCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void GraphicType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void GraphicTransparency_Unchecked(object sender, RoutedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void GraphicTransparency_Checked(object sender, RoutedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void VideoQuality_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void VideoType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void Sort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            _cancellationTokenSource?.Cancel();
            Hide();
        }

        private async void InsertButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            await InsertMedia();
        }

        private async void TextBox_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                await UpdateSearchResult();
            }
        }

        private async void Video_Checked(object sender, RoutedEventArgs e)
        {
            GraphicOptions.Visibility = Visibility.Collapsed;
            VideoOptions.Visibility = Visibility.Visible;
            await UpdateSearchResult();
        }

        private async void Video_Unchecked(object sender, RoutedEventArgs e)
        {
            VideoOptions.Visibility = Visibility.Collapsed;
            GraphicOptions.Visibility = Visibility.Visible;
            await UpdateSearchResult();
        }

        private void UpdateInsertButtonEnable()
        {
            InsertButton.IsEnabled = List.Items.OfType<ItemPreview>().Any(i => i.IsSelected);
        }

        private async void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (_isLoading) return;

            _isLoading = true;
            if (_scrollViewer.VerticalOffset > _verticalOffset && _scrollViewer.ExtentHeight - _scrollViewer.VerticalOffset < ItemPreview.ItemHeight * ItemRowsPerPage)
            {
                await LoadPage();
            }
            _isLoading = false;
        }

        private async void List_DoubleTapped(object sender, Windows.UI.Xaml.Input.DoubleTappedRoutedEventArgs e)
        {
            await InsertMedia();
        }

        private void List_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (_isSingleSelection)
            {
                var unselectedItem = List.Items.OfType<ItemPreview>().Where(i => (i.IsSelected && !i.IsLastSelected)).ToList();
                foreach (var item in unselectedItem)
                {
                    item.IsSelected = false;
                }
            }
            var lastSelected = List.Items.OfType<ItemPreview>().Where(i => i.IsLastSelected).ToList();
            foreach(var item in lastSelected)
            {
                item.IsLastSelected = false;
            }
            UpdateInsertButtonEnable();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Search.Text = String.Empty;
            if (Video.IsChecked.Value)
            {
                VideoType.SelectedIndex = 0;
                Sort.SelectedIndex = 0;
                VideoQuality.SelectedIndex = 0;
            }
            else if (Photo.IsChecked.Value)
            {
                GraphicType.SelectedIndex = 0;
                GraphicCategories.SelectedIndex = 0;
                GraphicOrientation.SelectedIndex = 0;
                GraphicTransparency.IsChecked = false;
            }
        }

        private async void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private async void Image_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            await UpdateSearchResult();
        }

        private void InsertButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Brush textForeground = Resources["AcceptTextBrush"] as SolidColorBrush;
            UseTextBox.Foreground = InsertButton.IsEnabled ? textForeground : new SolidColorBrush(Colors.White);
        }

        #endregion

        #region Private Methods
        private async Task UpdateSearchResult()
        {
            UpdateSearchRequest();
            UpdateInsertButtonEnable();

            List.Items.Clear();
            _currentItems = 0;
            await LoadPage();
        }

        private void UpdateSearchRequest()
        {
            _queryResult.SearchRequest.MediaType = MediaType;
            _queryResult.SearchRequest.Keywords = Search.Text;
            _queryResult.SearchRequest.Sort = (SortMetric)Sort.SelectedIndex;

            _queryResult.SearchRequest.VideoContentType = (VideoContentType)VideoType.SelectedIndex;
            _queryResult.SearchRequest.VideoQuality = (VideoQuality)VideoQuality.SelectedIndex;

            _queryResult.SearchRequest.GraphicContentType = (GraphicContentType)GraphicType.SelectedIndex;
            _queryResult.SearchRequest.GraphicCategories = (GraphicCategories)GraphicCategories.SelectedIndex;
            _queryResult.SearchRequest.GraphicOrientation = (GraphicOrientation)GraphicOrientation.SelectedIndex;
            _queryResult.SearchRequest.HasTransparency = GraphicTransparency.IsChecked.Value;
        }

        private async Task InsertMedia()
        {
            _isDownloadingInProgress = true;
            Container.Width = DownloadingWidth;
            Container.Height = DownloadingHeight;
            ViewBox.Width = DownloadingWidth;
            ViewBox.Height = DownloadingHeight;

            // for Win 10 build 10586
            ViewBox.VerticalAlignment = VerticalAlignment.Top;
            ViewBox.HorizontalAlignment = HorizontalAlignment.Left;
            MaxWidth = DownloadingWidth;
            MaxHeight = DownloadingHeight + 50;

            DownloadPanel.Visibility = Visibility.Visible;
            SelectionPanel.Visibility = Visibility.Collapsed;
            InsertButton.Visibility = Visibility.Collapsed;
            Title.Visibility = Visibility.Collapsed;
            DownloadingTitle.Visibility = Visibility.Visible;

            int badResultCounter = 0;
            var totalBytesToDownload = 0.0;
            foreach (var info in List.Items.OfType<ItemPreview>().Where(i => i.IsSelected))
            {
                info.MediaType = MediaType;
                _selectedMedia.Add(info);

                var preferFormatKey = _queryResult.GetPreferFormat(info.SearchInfo);
                if (preferFormatKey != null && info.SearchInfo.download_format_details.ContainsKey(preferFormatKey))
                {
                    totalBytesToDownload += info.SearchInfo.download_format_details[preferFormatKey].file_size_bytes;
                }
            }

            LoadedProgress.Value = 0;
            LoadedProgress.Maximum = totalBytesToDownload;
            _cancellationTokenSource = new CancellationTokenSource();
            foreach (var info in _selectedMedia)
            {
                _deltaBytes = 0;
                var result = await _queryResult.DownloadMediaAsync(info.MediaType, info.SearchInfo, DownloadProgress, _cancellationTokenSource.Token);
                if (result)
                {
                    info.IsSelected = false;
                }
                else
                {
                    ++badResultCounter;
                }
                if (_cancellationTokenSource.IsCancellationRequested)
                {
                    break;
                }
            }
            _cancellationTokenSource.Dispose();
            _cancellationTokenSource = null;

            if (badResultCounter > 0)
            {
                var dialog = new Windows.UI.Popups.MessageDialog(String.Format(MessageContent, badResultCounter), MessageTitle);
                dialog.Commands.Clear();
                dialog.Commands.Add(new Windows.UI.Popups.UICommand
                {
                    Label = "OK",
                });
                await dialog.ShowAsync();
            }

            Hide();
        }
        private async Task LoadPage()
        {
            BusyIndicator.Visibility = Visibility.Visible;
            int totalItems = 0;
            var results = await _queryResult.GetMediaAsync(_currentItems, 150, _cancellationTokenSource.Token);
            if (results != null && results.success)
            {
                totalItems = results.totalSearchResults;
                for (int i = 0; i < results.info.Length; i++)
                {
                    List.Items.Add(new ItemPreview(results.info[i]));
                }
                _currentItems += results.info.Length;
            }

            BusyIndicator.Visibility = Visibility.Collapsed;

            MessageTextBlock.Visibility = (!_queryResult.IsSuccess || totalItems == 0) ? Visibility.Visible : Visibility.Collapsed;
            if (!_queryResult.IsSuccess)
            {
                MessageTextBlock.Text = NoConnectionMessage;
            }
            else if (totalItems == 0)
            {
                MessageTextBlock.Text = NoResultMessage;
            }
            _verticalOffset = _scrollViewer?.VerticalOffset ?? 0.0;
        }
        private T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null)
            {
                return null;
            }

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null)
                    {
                        break;
                    }
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        private void DownloadProgress(Windows.Networking.BackgroundTransfer.DownloadOperation download)
        {
            // DownloadOperation.Progress is updated in real-time while the operation is ongoing. Therefore,
            // we must make a local copy so that we can have a consistent view of that ever-changing state
            // throughout this method's lifetime.
            Windows.Networking.BackgroundTransfer.BackgroundDownloadProgress currentProgress = download.Progress;

            var bytesDownloaded = currentProgress.BytesReceived - _deltaBytes;
            _deltaBytes = currentProgress.BytesReceived;

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                LoadedProgress.Value += bytesDownloaded;
                var percent = (int)(LoadedProgress.Value * 100 / LoadedProgress.Maximum);
                Percent.Text = $"{percent}%";
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }
        #endregion
    }
}
