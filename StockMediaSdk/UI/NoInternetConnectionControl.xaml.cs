﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace StockMediaSdk.UI
{
    public sealed partial class NoInternetConnectionControl : UserControl
    {
        public delegate void TryNow();
        public event TryNow TryToConnectEvent;
        public NoInternetConnectionControl()
        {
            InitializeComponent();
        }

        private void TryNowButton_Click(object sender, RoutedEventArgs e)
        {
            TryToConnectEvent?.Invoke();
        }
    }
}
