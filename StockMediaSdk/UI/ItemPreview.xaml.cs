﻿using StockMediaSdk.Data.Model;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace StockMediaSdk.UI
{
    public sealed partial class ItemPreview : UserControl, INotifyPropertyChanged
    {
        public static double ItemHeight = 160;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Private fields
        private bool _isSelected = false;
        private SearchInfo _searchInfo;
        #endregion

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                RaisePropertyChanged();
            }
        }

        public MediaType MediaType { get; set; }

        public string ThumbnailUrl => _searchInfo.thumbnail_url;
        public string Title => _searchInfo.title;

        public bool IsLastSelected { get; set; }

        public SearchInfo SearchInfo => _searchInfo;

        public ItemPreview(SearchInfo searchInfo)
        {
            InitializeComponent();

            _searchInfo = searchInfo;
            TitleBlock.Text = _searchInfo.title;
            Image.UriSource = new Uri(_searchInfo.thumbnail_url);
        }

        private void UserControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            IsSelected = !IsSelected;
            IsLastSelected = IsSelected;
        }

        private void UserControl_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            IsSelected = true;
            IsLastSelected = IsSelected;
        }
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
