﻿using System.Collections.Generic;

namespace StockMediaSdk.Data.Model
{
    public class DownloadInfo
    {
        public string url { set; get; }
        public Dictionary<string, string> alternateFormats { set; get; }
    }
    public class DownloadResponse
    {
        public bool success { set; get; }
        public DownloadInfo info { set; get; }
    }
}
