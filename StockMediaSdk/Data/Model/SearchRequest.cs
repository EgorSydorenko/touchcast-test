﻿namespace StockMediaSdk.Data.Model
{
    public enum MediaType
    {
        None = 0,
        Video = 0x01,
        Photo = 0x02,
        Audio = 0x04
    }

    internal enum SortMetric
    {
      most_relevant,
      most_downloaded,
      highest_rated,
      most_recent,
      trending_now,
      undiscovered
    }

    internal enum VideoContentType
    {
	    all,
        footage,
        motionbackgrounds,
        templates
    }

    internal enum GraphicContentType
    {
        all, photos, illustrations, vectors
    }

    internal enum VideoQuality
    {
        HD = 0,
        SD,
        _4K
    }

    internal enum GraphicCategories
    {
        all = 0,
        designElements,
        objects, business, people,
        holidaysAndCelebrations, natureAndWildlife,
        buildingsAndLandmarks, sportsAndOutdoors,
        fictional, travelAndTransportation,
        healthAndScience, backgrounds, cultural,
        beautyAndFashion
    }

    internal enum GraphicOrientation
    {
        all, portrait, square, landscape
    }

    class SearchRequest
    {
        public MediaType MediaType { get; set; }
        public string Keywords { get; set; }
        public int Page { get; set; }
        public int NumResults { get; set; }
        public SortMetric Sort { get; set; }

        // video
        public VideoContentType VideoContentType { get; set; }
        public VideoQuality VideoQuality { get; set; }

        // graphic
        public GraphicContentType GraphicContentType { get; set; }
        public GraphicCategories GraphicCategories { get; set; }
        public GraphicOrientation GraphicOrientation { get; set; }
        public bool HasTransparency { get; set; }
    }
}
