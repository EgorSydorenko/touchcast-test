﻿namespace StockMediaSdk.Data.Model
{
    internal class SearchResponse
    {
        public bool success { set; get; }
        public int totalSearchResults { set; get; }
        public SearchInfo[] info { set; get; }
    }
}
