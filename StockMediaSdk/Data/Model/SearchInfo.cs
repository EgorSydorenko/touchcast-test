﻿using System.Collections.Generic;

namespace StockMediaSdk.Data.Model
{
    public class Detail
    {
        public double bitrate_kbps { set; get; }
        public long file_size_bytes { set; get; }
        public string format_name { set; get; }
        public double frame_rate { set; get; }
        public string video_codec { set; get; }

        public double dpi { set; get; }
        public double height { set; get; }
        public double width { set; get; }
    }
    public class SearchInfo 
    {
        public string id { set; get; }
        public string title { set; get; }
        public string details_url { set; get; }
        public string preview_url { set; get; }
        public string thumbnail_url { set; get; }
        public string[] download_formats { set; get; }
        public Dictionary<string, Detail> download_format_details { set; get; }
        public string[] purchase_urls { set; get; }
}
}
