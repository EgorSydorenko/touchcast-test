﻿using Newtonsoft.Json;
using StockMediaSdk.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Windows.Networking.BackgroundTransfer;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;

namespace StockMediaSdk
{
    internal class StockMediaQueryResult
    {
        public const int ItemsPerPage = 52;

        #region Private Fields
        private const string downloaderId = "1234";
        private const string DefaultSearchKeyword = "abstract backgrounds";

        private readonly IStorageFolder _storageFolder;
        private readonly List<StorageFile> _downloadedFiles = new List<StorageFile>();
        private readonly SearchRequest _searchRequest = new SearchRequest();
        private static readonly string[] VideoQualityFilter = new string[]
        {
            "HD", "SD", "4K"
        };
        private static readonly string[] GraphicCategoriesFilter = new string[]
        {
            "all", "design-elements",
        "objects", "business", "people",
        "holidays-and-celebrations", "nature-and-wildlife",
        "buildings-and-landmarks", "sports-and-outdoors",
        "fictional", "travel-and-transportation",
        "health-and-science", "backgrounds", "cultural",
        "beauty-and-fashion"
        };

        private readonly IReadOnlyList<string> AvailableFormats = new List<string> { "PNG", "JPG", "HDMP4" };
        #endregion
        public List<StorageFile> DownloadedFiles => _downloadedFiles;

        public SearchRequest SearchRequest
        {
            get
            {
                return _searchRequest;
            }
        }

        public bool IsSuccess { get; private set; }

        public StockMediaQueryResult(IStorageFolder storageFolder)
        {
            _storageFolder = storageFolder;
        }
        public async Task<int> GetItemsCountAsync()
        {
            int result = 0;
            var searchResult = await GetMediaAsync(0, 10, default(CancellationToken));
            if (searchResult != null && searchResult.success)
            {
                result = searchResult.totalSearchResults;
            }
            return result;
        }
        public async Task<SearchResponse> GetMediaAsync(int firstIndex, uint length, CancellationToken cancellationToken)
        {
            var page = firstIndex / ItemsPerPage + 1;
            var result = await GetAvailableInfoAsync(page, ItemsPerPage, cancellationToken);
            if (result != null && result.success)
            {
                if ((firstIndex % ItemsPerPage) != 0)
                {
                    var info = new List<SearchInfo>();
                    var startIndex = ItemsPerPage - (page * ItemsPerPage - firstIndex);
                    while (startIndex < result.info.Length)
                    {
                        info.Add(result.info[startIndex++]);
                    }

                    ++page;
                    result = await GetAvailableInfoAsync(page, ItemsPerPage, cancellationToken);
                    if (result != null && result.success)
                    {
                        startIndex = 0;
                        while (info.Count < length && startIndex < result.info.Length)
                        {
                            info.Add(result.info[startIndex++]);
                        }
                        result.info = info.ToArray();
                    }
                }
            }
            return result;
        }

        public string GetPreferFormat(SearchInfo info)
        {
            if (info?.download_formats == null)
            {
                return null;
            }

            var preferFormat = info.download_formats.FirstOrDefault(i => i.ToUpper() == "PNG");
            if (String.IsNullOrEmpty(preferFormat))
            {
                preferFormat = info.download_formats.FirstOrDefault(i => i.ToUpper() == "JPG");
                if (String.IsNullOrEmpty(preferFormat))
                {
                    preferFormat = info.download_formats.FirstOrDefault(i => AvailableFormats.Contains(i.ToUpper()));
                }
            }
            return preferFormat;
        }

        public async Task<bool> DownloadMediaAsync(MediaType mediaType, SearchInfo info, Action<DownloadOperation> handler, CancellationToken cancellationToken)
        {
            var result = false;
            IsSuccess = false;
            StorageFile destinationFile = null;
            var requestMessage = CreateRequestMessage(mediaType, $"{ApiClient.DownloadResource}/{info.id}/{downloaderId}/", "");
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.SendAsync(requestMessage);
                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    var downloadResponse = JsonConvert.DeserializeObject<DownloadResponse>(jsonResponse);
                    if (downloadResponse != null && downloadResponse.success)
                    {
                        var url = downloadResponse.info.url;
                        var preferFormatKey = GetPreferFormat(info);
                        if (preferFormatKey != null && downloadResponse.info.alternateFormats.ContainsKey(preferFormatKey))
                        {
                            url = downloadResponse.info.alternateFormats[preferFormatKey];
                        }
                        
                        var fileExtension = url.Substring(0, url.IndexOf('?')).Substring(url.LastIndexOf('.'));
                        destinationFile = await _storageFolder.CreateFileAsync(Guid.NewGuid().ToString() + fileExtension);

                        var downloader = new BackgroundDownloader();
                        var download = downloader.CreateDownload(new Uri(url), destinationFile);
                        var progressCallback = new Progress<DownloadOperation>(handler);

                        await download.StartAsync().AsTask(cancellationToken, progressCallback);

                        var responseInfo = download.GetResponseInformation();
                        if (responseInfo.StatusCode == 200)
                        {
                            _downloadedFiles.Add(destinationFile);
                            result = true;
                            IsSuccess = true;
                        }
                    }
                }
            }
            catch (TaskCanceledException)
            {
                if (destinationFile != null)
                {
                    await destinationFile.DeleteAsync();
                }
                result = true;
            }
            catch
            {

            }
            return result;
        }

        #region Private Methods
        private async Task<SearchResponse> GetInfoAsync(int page, int length, CancellationToken cancellationToken)
        {
            var search = _searchRequest.Keywords;
            if (String.IsNullOrEmpty(search))
            {
                search = DefaultSearchKeyword;
            }
            var resource = $"keywords={search}&page={page}&num_results={length}&sort={_searchRequest.Sort}";
            if (_searchRequest.MediaType == MediaType.Video)
            {
                resource += $"&content_type={_searchRequest.VideoContentType}&quality={VideoQualityFilter[(int)_searchRequest.VideoQuality]}";
            }
            if (_searchRequest.MediaType == MediaType.Photo)
            {
                resource += $"&content_type={_searchRequest.GraphicContentType}";
                if (_searchRequest.GraphicCategories != GraphicCategories.all)
                {
                    resource += $"&categories={GraphicCategoriesFilter[(int)_searchRequest.GraphicCategories]}";
                }
                if (_searchRequest.GraphicOrientation != GraphicOrientation.all)
                {
                    resource += $"&orientation={_searchRequest.GraphicOrientation}";
                }
                if (_searchRequest.HasTransparency)
                {
                    resource += "&has_transparency=true";
                }
            }
            resource += "&is_safe_for_work=true";

            var requestMessage = CreateRequestMessage(_searchRequest.MediaType, ApiClient.SearchResource, resource);
            SearchResponse result = null;
            IsSuccess = false;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.SendAsync(requestMessage, cancellationToken);
                    if (response != null)
                    {
                        var jsonResponse = await response.Content.ReadAsStringAsync();
                        if (jsonResponse != null)
                        {
                            result = JsonConvert.DeserializeObject<SearchResponse>(jsonResponse, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            IsSuccess = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            return result;
        }

        private async Task<SearchResponse> GetAvailableInfoAsync(int page, int length, CancellationToken cancellationToken)
        {
            var result = await GetInfoAsync(page, ItemsPerPage, cancellationToken);
            if (result != null && result.success)
            {
                var infos = new List<SearchInfo>();
                foreach(var info in result.info)
                {
                    if (IsAvailableFormat(info))
                    {
                        infos.Add(info);
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine(info.title);
                    }
                }
                result.info = infos.ToArray();
            }
            return result;
        }

        private HttpRequestMessage CreateRequestMessage(MediaType mediaType, string resource, string param)
        {
            var unixTimestamp = ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string url = String.Empty;
            if ((mediaType & MediaType.Video) == MediaType.Video)
            {
                url = ApiClient.VideoBlocks;
            }
            else if ((mediaType & MediaType.Photo) == MediaType.Photo)
            {
                url = ApiClient.GraphicStock;
            }
            else if ((mediaType & MediaType.Audio) == MediaType.Audio)
            {
                url = ApiClient.AudioBlocks;
            }
            else
            {
                return null;
            }

            var hmac = GetSHA256Key(ApiClient.PrivateKey + unixTimestamp, resource);
            if (!String.IsNullOrEmpty(param))
            {
                param += '&';
            }
            url = $"{url}{resource}?{param}APIKEY={ApiClient.PublicKey}&HMAC={hmac}&EXPIRES={unixTimestamp}";
            return new HttpRequestMessage(HttpMethod.Get, new Uri(url));
        }

        private string GetSHA256Key(string secretKey, string value)
        {
            var buffer = CryptographicBuffer.ConvertStringToBinary(secretKey, BinaryStringEncoding.Utf8);
            var objMacProv = MacAlgorithmProvider.OpenAlgorithm(MacAlgorithmNames.HmacSha256);
            var hash = objMacProv.CreateHash(buffer);
            hash.Append(CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8));
            return CryptographicBuffer.EncodeToHexString(hash.GetValueAndReset());
        }
        private bool IsAvailableFormat(SearchInfo info)
        {
            if (info?.download_formats == null)
            {
                return false;
            }

            return info.download_formats.Any(i => AvailableFormats.Contains(i.ToUpper()));
        }
        #endregion
    }
}
