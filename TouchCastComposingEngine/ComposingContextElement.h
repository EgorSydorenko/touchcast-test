#pragma once

#include <crtdbg.h>
#include <pplawait.h>
#include <collection.h>
#include <mutex>
#include "ComposingTypes.h"


namespace TouchCastComposingEngine
{
	public ref class TouchcastShapeElement sealed : Platform::Object 
	{
	public:
		property Windows::UI::Color Color
		{
			Windows::UI::Color get()
			{
				return _color;
			}

			void set(Windows::UI::Color color)
			{
				_color = color;
			}
		}

		property float StrokeWidth 
		{
			float get() 
			{
				return _strokeWidth;
			}

			void set(float thickness) 
			{
				_strokeWidth = thickness;
			}
		}

		property Windows::UI::Color StrokeColor
		{
			Windows::UI::Color get()
			{
				return _strokeColor;
			}

			void set(Windows::UI::Color color)
			{
				_strokeColor = color;
			}
		}
		
		property ComposingShapeSubType ShapeSubType;

		TouchcastShapeElement(Windows::UI::Color color, Windows::UI::Color strokeColor, float strokeWidth)
		{
			_color = color;
			_strokeColor = strokeColor;
			_strokeWidth = strokeWidth;
		}

	private:
		Windows::UI::Color _color;
		Windows::UI::Color _strokeColor;
		float _strokeWidth;
	};

	public ref class ComposingContextElement sealed : Platform::Object
	{
	public:
		property static Platform::String^ EffectKeyType
		{
			Platform::String^ get()
			{
				return "EffectType";
			} 
		}
		//common propertries
		property ComposingContextElementType Type;
		property Windows::Foundation::Rect RelativePosition;
		property ContentMode ContentMode;
		property Windows::Graphics::Imaging::BitmapSize Size;
		property bool IsVideoVappFrame;
		property Platform::String^ RecordableItemID
		{
			Platform::String^ get()
			{
				return _recordableItemID;
			}
		}

		//this property is available only for Bitmap Type
		property  Microsoft::Graphics::Canvas::ICanvasImage^ Bitmap
		{
			Microsoft::Graphics::Canvas::ICanvasImage^ get()
			{
				std::lock_guard<std::mutex> lock(_bitmapLock);
				return _bitmap;
			}
			void set(Microsoft::Graphics::Canvas::ICanvasImage^ bitmap)
			{
				std::lock_guard<std::mutex> lock(_bitmapLock);
				_bitmap = nullptr;
				_bitmap = dynamic_cast<Microsoft::Graphics::Canvas::CanvasBitmap^>(bitmap);
			}
		}

		property TouchcastShapeElement^ ShapeElement
		{
			TouchcastShapeElement^ get() 
			{
				return _shapeElement;
			}
			void set(TouchcastShapeElement^ element)
			{
				_shapeElement = element;
			}
		}

		ComposingContextElement(Platform::String^ id)
		{
			_recordableItemID = id;
			Type = ComposingContextElementType::Unknown;
			RelativePosition = Windows::Foundation::Rect(0.0, 0.0, 0.0, 0.0);
			ContentMode = TouchCastComposingEngine::ContentMode::ScaleAspectFill;
			_bitmap = nullptr;
			_shapeElement = nullptr;
			_strokes = ref new Platform::Collections::Vector<Windows::UI::Input::Inking::InkStroke ^>();

		}

		//use this method to load bitmap from UIElement
		//Platform::Object^ object convert to Windows::UI::Xaml::Media::Imaging::IRenderTargetBitmap^> because of warning  C4453
		void LoadBitmapFromRenderTargetBitmap(Platform::Object^ object,
			Microsoft::Graphics::Canvas::ICanvasResourceCreator^ resourceCreator) __resumable
		{
			try
			{
				_renderingLock.lock();
				auto renderTargetBitmap = static_cast<Windows::UI::Xaml::Media::Imaging::IRenderTargetBitmap^>(object);
				if (!renderTargetBitmap || !resourceCreator)
				{
					//resource creator or target bitmap are missing
					_ASSERT(0);
					return;
				}
				
				Concurrency::create_task(renderTargetBitmap->GetPixelsAsync()).then([this, renderTargetBitmap, resourceCreator]
				(Windows::Storage::Streams::IBuffer^ buffer)
				{
					
					if (nullptr == buffer && resourceCreator == nullptr)
					{
						_renderingLock.unlock();
						return;
					}

					try
					{
						 auto bitmap = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromBytes(resourceCreator,
							buffer,
							renderTargetBitmap->PixelWidth,
							renderTargetBitmap->PixelHeight,
							Windows::Graphics::DirectX::DirectXPixelFormat::B8G8R8A8UIntNormalized);
						
						 if (bitmap != nullptr)
						 {
							 Bitmap = bitmap;
						 }

						 _renderingLock.unlock();
					}
					catch (Platform::Exception^ ex)
					{
						_renderingLock.unlock();
					}
				});
			}
			catch (Platform::Exception^ ex) 
			{
				_renderingLock.unlock();
			}
		}

		void AppendStrokes(Windows::Foundation::Collections::IVectorView<Windows::UI::Input::Inking::InkStroke ^>^ newStrokes)
		{
			std::lock_guard<std::mutex>lock(_strokesLock);

			for (unsigned int i = 0; i < newStrokes->Size; i++)
			{
				_strokes->Append(newStrokes->GetAt(i));
			}
		}

		void RemoveStrokes(Windows::Foundation::Collections::IVectorView<Windows::UI::Input::Inking::InkStroke ^>^ strokesToBeRemoved)
		{
			std::lock_guard<std::mutex>lock(_strokesLock);
			
			for (unsigned int i = 0; i < strokesToBeRemoved->Size; i++)
			{
				Windows::UI::Input::Inking::InkStroke^ strokeToRemove = strokesToBeRemoved->GetAt(i);

				for (unsigned int j = 0; j < _strokes->Size; j++)
				{
					Windows::UI::Input::Inking::InkStroke^ strokeToCheck = _strokes->GetAt(j);

					if (strokeToRemove->Id == strokeToCheck->Id)
					{
						_strokes->RemoveAt(j);
						break;
					}
				}
				
			}
		}

		//Inking appears to be very thread sensitive and requires making sure container sill managers strokes, while other process draws these strokes
		//because of that few stroke managemenet methods created
		//Typical Usage:
		//auto strokes = LockInkingAndReturnCurrentStrokes();
		//draw(strokes);
		//UnlockInking();
		Windows::Foundation::Collections::IVector<Windows::UI::Input::Inking::InkStroke ^>^ LockInkingAndReturnCurrentStrokes()
		{
			_strokesLock.lock();
			return _strokes;
		}

		void UnlockInking()
		{
			_strokesLock.unlock();
		}

		
	private:
		static Microsoft::Graphics::Canvas::ICanvasResourceCreator^ _resourceCreator;
		Microsoft::Graphics::Canvas::CanvasBitmap^	_bitmap;
		Platform::Collections::Vector<Windows::UI::Input::Inking::InkStroke ^>^ _strokes;
		TouchcastShapeElement^ _shapeElement;
		Platform::String^ _recordableItemID;

		std::mutex _strokesLock;
		std::mutex _bitmapLock;
		std::mutex _renderingLock;
	};
}