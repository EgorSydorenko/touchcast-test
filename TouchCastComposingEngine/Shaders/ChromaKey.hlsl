#define D2D_INPUT_COUNT 2
#define D2D_INPUT0_COMPLEX
#define D2D_INPUT1_COMPLEX
#include "d2d1effecthelpers.hlsli"
//Input parametrs
float sensivity = 0.3;
float smoothing = 0.1;
//removable color in YUVW format
float Y, U, V, W;

float4 rgb_to_yuv(float4 rgba);
float4 yuv_to_rgb(float4 yuvw);

D2D_PS_ENTRY(main)
{
	//Get input pixel color
	float4 input = D2DGetInput(0);
	//Crate removableYUV
	float4 removableYUV = float4(Y, U, V, W);
	//Convert pixel color to YUVW format
	float4 inputYUV = rgb_to_yuv(input);
	//Calculate difference between pixel color and removable color
	float2 diff = inputYUV.yz - removableYUV.yz;
	//Calculate color distance from difference
	float colorDist = sqrt(dot(diff, diff));
	//Calculate alpha coefficient
	float smoothStep = smoothstep(sensivity, sensivity + smoothing, colorDist);
	//Change input color if needed
	input *= smoothStep;

	return input;
}

//Convert RGBA color format to YUVW color format
float4 rgb_to_yuv(float4 rgba)
{
	float4 resultYUVW;
	resultYUVW.x = 0.2989f * rgba.r + 0.5866f * rgba.g + 0.1145f * rgba.b;
	resultYUVW.y = 0.7132f * (rgba.r - resultYUVW.x);
	resultYUVW.z = 0.5647f * (rgba.b - resultYUVW.x);
	resultYUVW.w = rgba.a;

	return resultYUVW;
}

//Convert YUVW color format to RGBA color format
float4 yuv_to_rgb(float4 yuvw)
{
	float4 resultRGBA;

	resultRGBA.r = yuvw.x + yuvw.y / 0.7132f;
	resultRGBA.b = yuvw.x + yuvw.z / 0.5647f;
	resultRGBA.g = (yuvw.x - 0.2989f * resultRGBA.r - 0.1145f * resultRGBA.b) / 0.5866f;
	resultRGBA.a = yuvw.w;

	return resultRGBA;
}

