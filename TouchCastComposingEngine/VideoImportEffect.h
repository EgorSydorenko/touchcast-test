#pragma once

#pragma once
#include "ComposingTypes.h"
#include "ComposingContext.h"

using namespace Windows::Storage::Streams;
using namespace Windows::UI::Xaml::Media::Imaging;
using namespace Windows::Foundation::Collections;
using namespace Platform::Collections;

namespace TouchCastComposingEngine
{
	public ref class VideoImportEffect sealed : public  Windows::Media::Effects::IBasicVideoEffect
	{
	public:
#pragma region Filter Properties

		property bool TimeIndependent
		{
			//video import video effect depends on time as app
			//expects to receive frames in the correct time order
			//do not change the value without decent investigation and testing
			virtual bool get() { return false; }
		}

		property bool IsReadOnly
		{
			//video effect could be readonly for vapp mode as we do not need to modify incoming frame
			//but on some platforms setting read-only to true cases frame reordering so setting false here
			//do not change the value without decent investigation and testing
			virtual bool get() { return false; }
		};

		VideoImportEffect()
		{
			_videoEncodingProperties = ref new Platform::Collections::Vector<Windows::Media::MediaProperties::VideoEncodingProperties^>();

			auto encodingProperties = ref new Windows::Media::MediaProperties::VideoEncodingProperties();
			encodingProperties->Subtype = "ARGB32";
			_videoEncodingProperties->InsertAt((unsigned int)0, encodingProperties);
		}

		property Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::VideoEncodingProperties^>^ SupportedEncodingProperties
		{
			virtual Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::VideoEncodingProperties^>^ get()
			{
				

				return _videoEncodingProperties->GetView();
			}
		}

		property Windows::Media::Effects::MediaMemoryTypes SupportedMemoryTypes
		{
			virtual Windows::Media::Effects::MediaMemoryTypes get() { return Windows::Media::Effects::MediaMemoryTypes::Gpu; }
		}

#pragma endregion

		virtual void DiscardQueuedFrames()
		{
			if (_type == VideoEffectType::VideoImportType)
			{
				ComposingContext::Shared()->ClearBuffer();
			}
		}

		virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
		{
			if (_type == VideoEffectType::VideoImportType)
			{
				ComposingContext::Shared()->ClearBuffer();
			}
		}

		virtual void SetProperties(Windows::Foundation::Collections::IPropertySet^ configuration)
		{
			if (configuration->HasKey(ComposingContextElement::EffectKeyType))
			{
				_type = (VideoEffectType)configuration->Lookup(ComposingContextElement::EffectKeyType);
				if (_type == VideoEffectType::VideoVappType)
				{
					_vappId = (Platform::String ^)configuration->Lookup("Id");
				}
			}
		}

		virtual void SetEncodingProperties(Windows::Media::MediaProperties::VideoEncodingProperties ^encodingProperties, Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice ^device)
		{
			if (_canvasDevice == nullptr)
			{
				if (_type == VideoEffectType::VideoImportType)
				{
					_canvasDevice = Microsoft::Graphics::Canvas::CanvasDevice::CreateFromDirect3D11Device(device);
					ComposingContext::Shared()->PreviewingDevice = _canvasDevice;
				}
			}
		}

		virtual void ProcessFrame(Windows::Media::Effects::ProcessVideoFrameContext ^context)
		{
			ComposingContext::Shared()->LockComposing();
			try 
			{
				if (_type == VideoEffectType::VideoVappType)
				{
					//Need to Update Device Every time becuse user can change Camera in any moment
					_canvasDevice = ComposingContext::Shared()->PreviewingDevice;
				}

				auto inputBitmap = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromDirect3D11Surface(_canvasDevice, context->InputFrame->Direct3DSurface);
				Microsoft::Graphics::Canvas::ICanvasImage^ frameToRenderToOutput = nullptr;

				switch (_type)
				{
				case TouchCastComposingEngine::VideoEffectType::VideoImportType:
				{

					if (ComposingContext::Shared()->ChromaKeyEffect->IsChromaKeyEffectReady)
					{
						frameToRenderToOutput = ComposingContext::Shared()->ChromaKeyEffect->ProcessEffect(inputBitmap, ChromaKeyType::Previewing);
					}

					ComposingContext::Shared()->PushNewVideoFrame(inputBitmap, context->InputFrame->RelativeTime->Value, context->InputFrame->Duration->Value);
				}
				break;

				case TouchCastComposingEngine::VideoEffectType::VideoVappType:
				{
					ComposingContext::Shared()->AddVideoVappBitmapById(inputBitmap, _vappId);
				}
				break;
				}

				if (frameToRenderToOutput == nullptr)
				{
					frameToRenderToOutput = inputBitmap;
				}

				//as isReadonly set to false we have to draw the same frame to the outuput
				//redundant operation for vapp mode but as we have issue on some platforms related to reordering of the frames
				//do not change this without decent testing and analysis
				auto outputBitmap = Microsoft::Graphics::Canvas::CanvasRenderTarget::CreateFromDirect3D11Surface(_canvasDevice, context->OutputFrame->Direct3DSurface);
				auto drawingSession = outputBitmap->CreateDrawingSession();
				drawingSession->Clear(Windows::UI::Colors::Transparent);
				drawingSession->DrawImage(frameToRenderToOutput);
			}
			catch (Platform::Exception^ ex)
			{

			}
			ComposingContext::Shared()->UnlockComposing();
		}

	private:
		Microsoft::Graphics::Canvas::ICanvasResourceCreator^		_canvasDevice = nullptr;
		Platform::String^ _vappId;
		Platform::Collections::Vector<Windows::Media::MediaProperties::VideoEncodingProperties^>^ _videoEncodingProperties;
		Windows::Foundation::Rect* _videoFrameRect = nullptr;
		VideoEffectType _type;
	};
}