﻿#pragma once
#include <collection.h>

#include "ComposingTypes.h"
#include "ComposingContext.h"
#include "WindowsNumerics.h"

namespace TouchCastComposingEngine
{
		public ref class ComposingVideoEffect sealed : public  Windows::Media::Effects::IBasicVideoEffect
		{
		
		public:

			property bool TimeIndependent
			{
				//composing effect is time dependant
				//so the result must be false
				virtual bool get() { return false; }
			}

			property bool IsReadOnly
			{
				//composing effect does change output frame
				//so the result must be false
				virtual bool get() { return false; }
			};

			virtual void ProcessFrame(Windows::Media::Effects::ProcessVideoFrameContext^ context)
			{
				try
				{
					ComposingContext::Shared()->LockComposing();
					if (_canvasDevice == nullptr
						&& ComposingContext::Shared()->PreviewingDevice == nullptr)
					{
						//canvas device is not set, cant process frame
						//_ASSERT(0);
						ComposingContext::Shared()->UnlockComposing();
						return;
					}

					//auto str = "Time: " + ComposingContext::Shared()->Time.Duration;
					//OutputDebugString(str->Data());

					if (!_canvasDevice)
					{
						_canvasDevice = ComposingContext::Shared()->PreviewingDevice;
					}

					auto inputBitmap = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromDirect3D11Surface(_canvasDevice, context->InputFrame->Direct3DSurface);
					auto renderTarget = Microsoft::Graphics::Canvas::CanvasRenderTarget::CreateFromDirect3D11Surface(_canvasDevice, context->OutputFrame->Direct3DSurface);
					auto ds = renderTarget->CreateDrawingSession();

					ds->Clear(ComposingContext::Shared()->FrameBackgroundColor);

					auto elementsList = ComposingContext::Shared()->Elements;

					if (!elementsList)
					{
						ComposingContext::Shared()->UnlockComposing();
						return;

					}
					auto size = elementsList->Size;
					for (unsigned int i = 0; i < elementsList->Size; i++)
					{
						ComposingContextElement^ element = elementsList->GetAt(i);

						switch (element->Type)
						{
						case  ComposingContextElementType::CameraPreview:
						case ComposingContextElementType::Video:
						{

							auto frame = element->Type == ComposingContextElementType::CameraPreview ?
								inputBitmap : ComposingContext::Shared()->PopNearestVideoFrame(context->InputFrame->RelativeTime->Value);

							//auto frame = element->Type == ComposingContextElementType::CameraPreview ?
							//	inputBitmap : ComposingContext::Shared()->VideoImportBitmap;

							Microsoft::Graphics::Canvas::ICanvasImage ^ processedFrame = nullptr;

							if (element->Type == ComposingContextElementType::CameraPreview)
							{
								processedFrame = ComposingContext::Shared()->RotatePreview(frame, inputBitmap->SizeInPixels);
								processedFrame = ComposingContext::Shared()->ChromaKeyEffect->ProcessEffect((Microsoft::Graphics::Canvas::CanvasBitmap ^)processedFrame, ChromaKeyType::Recording);

								if (ComposingContext::Shared()->State == ComposingState::Recording)
								{
									ComposingContext::Shared()->DrawCameraFeed(processedFrame, inputBitmap->SizeInPixels);
								}
							}
							else
							{
								processedFrame = frame;
							}


							if (processedFrame != nullptr)
							{
								ComposingContext::DrawBitmap(ds,
									processedFrame,
									frame->SizeInPixels,
									Windows::Foundation::Rect(element->RelativePosition),
									renderTarget->SizeInPixels,
									element->ContentMode);
							}
						} break;

						case ComposingContextElementType::Bitmap:
						{
							auto bitmap = element->IsVideoVappFrame ? ComposingContext::Shared()->GetVideoVappBitmapById(element->RecordableItemID) : element->Bitmap;
							if (bitmap != nullptr)
							{
								try
								{
									ComposingContext::DrawBitmap(ds,
										dynamic_cast<Microsoft::Graphics::Canvas::CanvasBitmap ^>(bitmap),
										Windows::Foundation::Rect(element->RelativePosition),
										renderTarget->SizeInPixels,
										element->ContentMode);
								}
								catch (Platform::Exception^ e)
								{

								}
							}
						} break;

						case ComposingContextElementType::Strokes:
						{
							auto strokes = element->LockInkingAndReturnCurrentStrokes();

							if (strokes->Size > 0)
							{
								//scaling canvas, strokes are drawn in the view size
								//and camera image size might be different
								auto previousTransform = ds->Transform;
								ds->Transform = Windows::Foundation::Numerics::make_float3x2_scale(inputBitmap->SizeInPixels.Width / (float)ComposingContext::Shared()->VideoSize.Width,
									inputBitmap->SizeInPixels.Height / (float)ComposingContext::Shared()->VideoSize.Height);
								//drawing inking
								ds->DrawInk(strokes);
								//restore canas tranform
								ds->Transform = previousTransform;
							}

							element->UnlockInking();

						} break;

						case ComposingContextElementType::Shape:
						{
							try 
							{
								ComposingContext::DrawShape(ds, Windows::Foundation::Rect(element->RelativePosition), element->ShapeElement, renderTarget->SizeInPixels);
							}
							catch (Platform::Exception^ e) 
							{

							}
						} break;

						default: _ASSERT(0); break;
						}
					}
					//auto t1 = context->OutputFrame->RelativeTime->Value;
					//auto t2 = context->OutputFrame->SystemRelativeTime->Value;
					//auto t3 = context->OutputFrame->Duration->Value;
					UpdateRcordingDuration(context->OutputFrame->RelativeTime->Value);

				}
				catch (Platform::Exception^ ex)
				{

				}
				ComposingContext::Shared()->UnlockComposing();
			}

			virtual void DiscardQueuedFrames()
			{

			}

			virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
			{

			}

			virtual void SetProperties(Windows::Foundation::Collections::IPropertySet^ configuration)
			{

			}

			virtual void SetEncodingProperties(Windows::Media::MediaProperties::VideoEncodingProperties^ encodingProperties, Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice^ device)
			{
				if (_canvasDevice == nullptr)
				{
					_canvasDevice = ComposingContext::Shared()->PreviewingDevice;
					if (ComposingContext::Shared()->СompletedStrokesBitmap == nullptr)
					{
						isNeedClearCanvas = true;
					}
					_timeOfFirstFrame.Duration = 0;
				}
			}

#pragma region Filter Properties
			
			property Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::VideoEncodingProperties^>^ SupportedEncodingProperties
			{
				virtual Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::VideoEncodingProperties^>^ get()
				{
					auto result = ref new Platform::Collections::Vector<Windows::Media::MediaProperties::VideoEncodingProperties^>();

					auto encodingProperties = ref new Windows::Media::MediaProperties::VideoEncodingProperties();
					encodingProperties->Subtype = "ARGB32";
					result->InsertAt(0, encodingProperties);

					return result->GetView();
				}
			}

			property Windows::Media::Effects::MediaMemoryTypes SupportedMemoryTypes
			{
				virtual Windows::Media::Effects::MediaMemoryTypes get() { return Windows::Media::Effects::MediaMemoryTypes::Gpu; }
			}

#pragma endregion
		
#pragma region Private Drawing Methods
		
		private:
			bool isNeedClearCanvas;
			
#pragma endregion
		
	private:
			Microsoft::Graphics::Canvas::ICanvasResourceCreator^		_canvasDevice = nullptr;
			Windows::Foundation::TimeSpan _timeOfFirstFrame;
			Windows::Foundation::TimeSpan _recordingTime;


			//Todo rename
			void UpdateRcordingDuration(Windows::Foundation::TimeSpan time)
			{
				
				if (_timeOfFirstFrame.Duration == 0)
				{
					_timeOfFirstFrame = time;
					Windows::Foundation::TimeSpan defaultime;
					defaultime.Duration = 0;
					ComposingContext::Shared()->Time = defaultime;
				}
				else
				{
					//_recordingTime.Duration = ComposingContext::Shared()->Time.Duration + time.Duration;
					_recordingTime.Duration = time.Duration - _timeOfFirstFrame.Duration;
					ComposingContext::Shared()->Time = _recordingTime;
				}
				
			}
		};
}