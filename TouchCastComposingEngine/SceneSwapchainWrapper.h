
#pragma once
#include <mutex>

namespace TouchCastComposingEngine
{
	/*
	public  delegate void SceneSwapchainWrapperChanged();

	public ref class SceneSwapchainWrapper sealed : Platform::Object
	{
	public:
		
		event SceneSwapchainWrapperChanged^ SceneSwapchainWrapperChangedEvent;
		
		SceneSwapchainWrapper(Microsoft::Graphics::Canvas::ICanvasResourceCreator^ previewingDevice, int numberOfSkipFramesBeforeRedrawing)
		{
			IsRenderTargetEmpty = true;
			_size = Windows::Foundation::Size(244, 137);
			_isRenderTargetBusy = false;
			_numberOfSkipFramesBeforeRedrawing = numberOfSkipFramesBeforeRedrawing;
			CreateSwapchainWithDevice(previewingDevice);
		}
		
		property Microsoft::Graphics::Canvas::ICanvasSwapChain^ Swapchain
		{
			void set(Microsoft::Graphics::Canvas::ICanvasSwapChain^ value)
			{
				std::lock_guard<std::mutex> lock(_lock);
				_swapchain = (Microsoft::Graphics::Canvas::CanvasSwapChain ^)value;
				_framesLeftBeforeRedrawing = 0;

				try
				{
					SceneSwapchainWrapperChangedEvent();
				}
				catch(Platform::Exception^ e)
				{}
			}
			Microsoft::Graphics::Canvas::ICanvasSwapChain^ get()
			{
				std::lock_guard<std::mutex> lock(_lock);
				return _swapchain;
			}
		}

		property Microsoft::Graphics::Canvas::CanvasRenderTarget^ SwapchainRenderTarget
		{
			void set(Microsoft::Graphics::Canvas::CanvasRenderTarget^ value)
			{
				std::lock_guard<std::mutex> lock(_renderTargetLock);
				_canvasRenderTraget = value;
			}
			Microsoft::Graphics::Canvas::CanvasRenderTarget^ get()
			{
				std::lock_guard<std::mutex> lock(_renderTargetLock);
				return _canvasRenderTraget;
			}
		}

		property bool IsRenderTargetBusy
		{
			void set(bool value)
			{
				_isRenderTargetBusy = value;
			}
			bool get()
			{
				return _isRenderTargetBusy;
			}

		}

		property Windows::Foundation::Size Size
		{
			Windows::Foundation::Size get()
			{
				return _size;
			}
		}

		property bool IsRenderTargetEmpty;
		
		void LockRenderTargetProcessing()
		{
			_renderTargetLock.lock();
		}

		void ReleaseRenderTargetProcessing()
		{
			_renderTargetLock.unlock();
		}

		bool ShouldRedraw()
		{
			_framesLeftBeforeRedrawing--;

			bool result = false;

			if (_framesLeftBeforeRedrawing < 0)
			{
				result = true;
				_framesLeftBeforeRedrawing = _numberOfSkipFramesBeforeRedrawing;
			}

			return result;
		}

		void CreateSwapchainWithDevice(Microsoft::Graphics::Canvas::ICanvasResourceCreator^ device)
		{
			this->Swapchain = nullptr;
			this->Swapchain = ref new Microsoft::Graphics::Canvas::CanvasSwapChain(device, this->Size.Width, this->Size.Height, 96);
			this->SwapchainRenderTarget = nullptr;
			this->SwapchainRenderTarget = ref new Microsoft::Graphics::Canvas::CanvasRenderTarget(device, this->Size.Width, this->Size.Height, 96);

			//with new surface we would like to redraw asap
			_framesLeftBeforeRedrawing = 0;
		}

	private:
		Microsoft::Graphics::Canvas::CanvasSwapChain^	_swapchain;
		Microsoft::Graphics::Canvas::CanvasRenderTarget^ _canvasRenderTraget;
		std::mutex _lock;
		std::mutex _renderTargetLock;
		Windows::Foundation::Size _size;
		bool _isRenderTargetBusy;

		int  _numberOfSkipFramesBeforeRedrawing;
		int  _framesLeftBeforeRedrawing;
	};
	*/
}

