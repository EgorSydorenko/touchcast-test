#pragma once

#include <collection.h>
#include <d3d11.h>

namespace TouchCastComposingEngine 
{
	public ref class GraphicAdapterInfo sealed : Platform::Object
	{
	public:
		property Platform::String^ Description
		{
			Platform::String^ get()
			{
				return _description;
			}
		}
		property unsigned __int64 DedicatedVideoMemory
		{
			unsigned __int64 get()
			{
				return _dedicatedVideoMemory;
			}
		}
		property unsigned __int64 DedicatedSystemMemory
		{
			unsigned __int64 get()
			{
				return _dedicatedSystemMemory;
			}
		}

		property unsigned __int64 SharedSystemMemory
		{
			unsigned __int64 get()
			{
				return _sharedSystemMemory;
			}
		}

		GraphicAdapterInfo(Platform::String^ description, unsigned __int64 dedicatedVideoMemory, unsigned __int64 dedicatedSystemMemory, unsigned __int64 sharedSystemMemory)
		{
			_description = description;
			_dedicatedVideoMemory = dedicatedVideoMemory;
			_dedicatedSystemMemory = dedicatedSystemMemory;
			_sharedSystemMemory = sharedSystemMemory;
		}
	private:
		Platform::String^ _description;
		unsigned __int64 _dedicatedVideoMemory;
		unsigned __int64 _dedicatedSystemMemory;
		unsigned __int64 _sharedSystemMemory;
	};

	public ref class HardwareInfo sealed : Platform::Object
	{
	public:
		property double RamSizeInGB
		{
			double get()
			{
				return _ramSizeInGB;
			}
		}

		property unsigned int NumberOfCores
		{
			unsigned int get()
			{
				return _numberOfCores;
			}
		}

		property Windows::Foundation::Collections::IVector<GraphicAdapterInfo^>^ GraphicAdaptersInfo
		{
			Windows::Foundation::Collections::IVector<GraphicAdapterInfo^>^ get()
			{
				return _adapters;
			}
		}

		property unsigned int DisplayWidth
		{
			unsigned int get()
			{
				return _displayWidth;
			}
		}

		property unsigned int DisplayHeight
		{
			unsigned int get()
			{
				return _displayHeight;
			}
		}

		bool TryGetInformation() 
		{
			return TryGetRamSize() && TryGetCpuCoresNumber() && TryGetGraphicAdaptersInfo() && TryGetDisplayInfo();
		}

	private:
		double _ramSizeInGB;
		unsigned int _numberOfCores;
		Windows::Foundation::Collections::IVector<GraphicAdapterInfo^>^ _adapters = ref new Platform::Collections::Vector<GraphicAdapterInfo^ >();;
		unsigned int _displayWidth;
		unsigned int _displayHeight;

		bool TryGetRamSize()
		{
			auto result = false;
			try
			{
				// RAM
				MEMORYSTATUSEX statex;
				statex.dwLength = sizeof(statex); // I misunderstand that
				GlobalMemoryStatusEx(&statex);
				_ramSizeInGB = (double)statex.ullTotalPhys / (1024 * 1024 * 1024);
				result = true;
			}
			catch (Platform::Exception^ ramException)
			{

			}
			return result;
		}

		bool TryGetCpuCoresNumber()
		{
			auto result = false;
			try
			{
				//// CPU Copy the hardware information to the SYSTEM_INFO structure. 
				SYSTEM_INFO siSysInfo;
				GetSystemInfo(&siSysInfo);
				_numberOfCores = (unsigned int)siSysInfo.dwNumberOfProcessors;
				result = true;
			}
			catch (Platform::Exception^ cpuException)
			{

			}
			return result;
		}

		bool TryGetGraphicAdaptersInfo() 
		{
			auto result = false;
			try
			{
				IDXGIAdapter * pDXGIAdapter;
				HRESULT hr = GetAdapter(&pDXGIAdapter);
				if (hr == S_OK)
				{
					IDXGIFactory * pIDXGIFactory = nullptr;
					hr = pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void **)&pIDXGIFactory);

					if (hr == S_OK)
					{
						UINT i = 0;
						IDXGIAdapter * pAdapter;
						while (pIDXGIFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND)
						{
							DXGI_ADAPTER_DESC adapterDesc;
							pAdapter->GetDesc(&adapterDesc);

							AddGraphicAdapter(adapterDesc);

							i++;
						}
						pIDXGIFactory->Release();
						result = true;
					}
					pDXGIAdapter->Release();
				}
			}
			catch (Platform::Exception^ gpuException)
			{

			}
			return result;
		}

		void AddGraphicAdapter(DXGI_ADAPTER_DESC & adapterDesc)
		{
			auto description = ref new Platform::String(adapterDesc.Description);
			SIZE_T dedicatedVideoMemory = adapterDesc.DedicatedVideoMemory;
			SIZE_T dedicatedSystemMemory = adapterDesc.DedicatedSystemMemory;
			SIZE_T sharedSystemMemory = adapterDesc.SharedSystemMemory;

			auto adapter = ref new GraphicAdapterInfo(description, dedicatedVideoMemory, dedicatedSystemMemory, sharedSystemMemory);
			_adapters->Append(adapter);
		}

		bool TryGetDisplayInfo() 
		{
			auto result = false;
			try
			{
				IDXGIAdapter * pDXGIAdapter;
				HRESULT hr = GetAdapter(&pDXGIAdapter);
				if (hr == S_OK)
				{
					IDXGIOutput *output;
					hr = pDXGIAdapter->EnumOutputs(0, &output);
					if (hr == S_OK)
					{
						DXGI_OUTPUT_DESC desc;
						hr = output->GetDesc(&desc);
						if (hr == S_OK)
						{
							_displayWidth = desc.DesktopCoordinates.right;
							_displayHeight = desc.DesktopCoordinates.bottom;

							result = true;
						}
						output->Release();
					}
					pDXGIAdapter->Release();
				}
			}
			catch (Platform::Exception^ gpuException)
			{

			}
			return result;
		}

		HRESULT GetAdapter(IDXGIAdapter ** pDXGIAdapter)
		{
			auto device = ref new Microsoft::Graphics::Canvas::CanvasDevice(false);
			IDXGIDevice * pDXGIDevice;
			DWORD createDeviceFlags = 0;
#ifdef _DEBUG
			createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

			Microsoft::WRL::ComPtr<ID3D11Device> nativeDevice;
			Microsoft::WRL::ComPtr<ID3D11DeviceContext> context;
			D3D_FEATURE_LEVEL fl;
			HRESULT hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE,
				nullptr, createDeviceFlags, nullptr,
				0, D3D11_SDK_VERSION, &nativeDevice, &fl, &context);
			if (hr == S_OK)
			{
				hr = nativeDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)&pDXGIDevice);
				if (hr == S_OK)
				{
					hr = pDXGIDevice->GetAdapter(pDXGIAdapter);
				}
			}
			nativeDevice = nullptr;
			context = nullptr;
			return hr;
		}
	};

}
