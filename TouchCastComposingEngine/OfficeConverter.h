#pragma once
#include "Manager.h"

namespace TouchCastComposingEngine
{
	namespace Win32BackgroundPipe
	{
		public ref class OfficeConverter sealed : Platform::Object
		{
		public:
			static Windows::Foundation::IAsyncOperation<Platform::String^>^ ConvertMicrosoftOfficeDocument(Platform::String^ sourceFileName, Platform::String^ destinationFileName)
			{
				auto valueSet = PrepareValueSetForDocumentConversion(sourceFileName, destinationFileName);
				return create_async([valueSet]
				{
					return create_task(Manager::Shared()->SendMessage(ServiceType::MSOfficeDocumentConverter, valueSet)).then([](task<Platform::Object^> taskResponce) {
						auto responseObject = taskResponce.get();
						Platform::String^ retrunValue = nullptr;
						if (responseObject != nullptr)
						{
							retrunValue = dynamic_cast<Platform::String^>(responseObject);
						}
						return retrunValue;
					});					
				});
			}

			static Windows::Foundation::IAsyncAction^ Start()
			{
				return Manager::Shared()->StartNativeService(ServiceType::MSOfficeDocumentConverter);
			}

			static void Initialize(Windows::ApplicationModel::Activation::BackgroundActivatedEventArgs^ backgroundEventArgs)
			{
				Manager::Shared()->Initialize(backgroundEventArgs);
			}

			static bool IsResponceSuccessfull(Platform::String^ responce)
			{
				if (responce == Constant::Instance()->Ok)
				{
					return true;
				}
				else 
				{
					return false;
				}
			}
			static Platform::String^ GetError(Platform::String^ errorCode)
			{
				return nullptr;
			}
		private:
			static Windows::Foundation::Collections::ValueSet^  PrepareValueSetForDocumentConversion(Platform::String^ sourceFileName, Platform::String^ destinationFileName)
			{
				auto result = ref new Windows::Foundation::Collections::ValueSet();
				result->Insert(Constant::Instance()->Namespace, Constant::Instance()->MicrosoftOffice);
				result->Insert(Constant::Instance()->Cmd, Constant::Instance()->ConvertToPdf);
				result->Insert(Constant::Instance()->SourceFile, sourceFileName);
				result->Insert(Constant::Instance()->DestinationFile, destinationFileName);
				return result;
			}
		};
	}
}
