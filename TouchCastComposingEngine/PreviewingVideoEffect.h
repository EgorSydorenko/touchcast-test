﻿
#pragma once
#include "ComposingTypes.h"
#include "ComposingContext.h"
#include "ComposingTypes.h"

using namespace Windows::Storage::Streams;
using namespace Windows::UI::Xaml::Media::Imaging;
using namespace Windows::Foundation::Collections;
using namespace Platform::Collections;

namespace TouchCastComposingEngine
{
	public ref class PreviewingVideoEffect sealed : public  Windows::Media::Effects::IBasicVideoEffect
	{
	public:
#pragma region Filter Properties

		property bool TimeIndependent
		{
			//previewing video effect depends on time as app
			//expects to receive frames in the correct time order
			//do not change the value without decent investigation and testing
			virtual bool get() { return false; }
		}

		property bool IsReadOnly
		{
			//although the effect does not have to draw anything to the ouput frame
			//and uses frames for sending to preview only, there is system problem:
			//setting value to true causes weird reordering of the preview frames on some platforms
			//do not change the value without decent investigation and testing
			virtual bool get() { return false; }
		}

		property Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::VideoEncodingProperties^>^ SupportedEncodingProperties
		{
			virtual Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::VideoEncodingProperties^>^ get()
			{
				auto result = ref new Platform::Collections::Vector<Windows::Media::MediaProperties::VideoEncodingProperties^>();

				auto encodingProperties = ref new Windows::Media::MediaProperties::VideoEncodingProperties();
				encodingProperties->Subtype = "ARGB32";
				result->InsertAt(0, encodingProperties);

				return result->GetView();
			}
		}

		property Windows::Media::Effects::MediaMemoryTypes SupportedMemoryTypes
		{
			virtual Windows::Media::Effects::MediaMemoryTypes get() { return Windows::Media::Effects::MediaMemoryTypes::Gpu; }
		}

#pragma endregion

		virtual void DiscardQueuedFrames()
		{

		}

		virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
		{

		}

		virtual void SetProperties(Windows::Foundation::Collections::IPropertySet^ configuration)
		{

		}

		virtual void SetEncodingProperties(Windows::Media::MediaProperties::VideoEncodingProperties ^encodingProperties, Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice ^device)
		{
			if (_canvasDevice == nullptr)
			{
				_canvasDevice = Microsoft::Graphics::Canvas::CanvasDevice::CreateFromDirect3D11Device(device);
				ComposingContext::Shared()->PreviewingDevice = _canvasDevice;
				if (ComposingContext::Shared()->СompletedStrokesBitmap != nullptr)
				{
					auto renderTarget = Microsoft::Graphics::Canvas::CanvasRenderTarget::CreateFromDirect3D11Surface(_canvasDevice, 
						(Windows::Graphics::DirectX::Direct3D11::IDirect3DSurface ^)ComposingContext::Shared()->СompletedStrokesBitmap);
					ComposingContext::Shared()->СompletedStrokesBitmap = renderTarget;
				}
			}
		}

		virtual void ProcessFrame(Windows::Media::Effects::ProcessVideoFrameContext ^context)
		{
			ComposingContext::Shared()->LockComposing();
			try {
				auto inputBitmap = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromDirect3D11Surface(_canvasDevice, context->InputFrame->Direct3DSurface);
				if (ComposingContext::Shared()->State == ComposingState::Previewing
					|| ComposingContext::Shared()->State == ComposingState::Paused)
				{
					ComposingContext::Shared()->ProcessPreview(inputBitmap, inputBitmap->SizeInPixels);
				}

				//as isReadonly set to false we have to draw the same frame to the outuput
				//redundant operation but as we have issue on some platforms related to reordering of the frames
				//do not change this without decent testing and analysis

				auto outputBitmap = Microsoft::Graphics::Canvas::CanvasRenderTarget::CreateFromDirect3D11Surface(_canvasDevice, context->OutputFrame->Direct3DSurface);
				auto drawingSession = outputBitmap->CreateDrawingSession();
				drawingSession->DrawImage(inputBitmap);
			}
			catch (Platform::Exception^ ex)
			{

			}
			ComposingContext::Shared()->UnlockComposing();
		}

		private:
			Microsoft::Graphics::Canvas::ICanvasResourceCreator^		_canvasDevice = nullptr;
	};
}