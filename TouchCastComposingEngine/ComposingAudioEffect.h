#pragma once

#include <collection.h>
#include "AudioImportEffect.h"

namespace TouchCastComposingEngine
{
	public ref class ComposingAudioEffect sealed : public  Windows::Media::Effects::IBasicAudioEffect
	{
	
	public:
		
		ComposingAudioEffect()
		{
			_prefferedAudioFormat = ref new Platform::Collections::Vector<Windows::Media::MediaProperties::AudioEncodingProperties^>();
			auto audioProperties = ComposingContext::Shared()->Audio->PrefferedAudioFormat;
			_prefferedAudioFormat->Append(audioProperties);
		}

		property Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::AudioEncodingProperties ^>^ SupportedEncodingProperties
		{
			virtual Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::AudioEncodingProperties ^>^ get()
			{
				return _prefferedAudioFormat->GetView();
			}
		}

		property bool UseInputFrameForOutput
		{
			virtual bool  get() { return false; }
		}

		virtual void SetEncodingProperties(Windows::Media::MediaProperties::AudioEncodingProperties^ encodingProperties)
		{
			_actualAudioFormat = encodingProperties;
		}

		virtual void SetProperties(Windows::Foundation::Collections::IPropertySet^ configuration)
		{

		}

		virtual void ProcessFrame(Windows::Media::Effects::ProcessAudioFrameContext ^ context)
		{
			auto outputFrame = context->OutputFrame;
			auto outputBuffer = outputFrame->LockBuffer(Windows::Media::AudioBufferAccessMode::ReadWrite);
			ComposingContext::Shared()->Audio->PopAudioToBuffer(outputBuffer, _actualAudioFormat, outputFrame->RelativeTime->Value, outputFrame->Duration->Value);
		}

		virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
		{

		}

		virtual void DiscardQueuedFrames()
		{

		}
	private:

		Platform::Collections::Vector<Windows::Media::MediaProperties::AudioEncodingProperties^>^	_prefferedAudioFormat;
		Windows::Media::MediaProperties::AudioEncodingProperties^									_actualAudioFormat;
	};
}