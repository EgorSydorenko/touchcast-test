﻿#pragma once
#include <crtdbg.h>
#include <pplawait.h>
#include <future>
#include "agile.h"
#include <wrl.h>
#include <robuffer.h>
#include <collection.h>
#include "ComposingTypes.h"
#include "WindowsNumerics.h"

using namespace Windows::Foundation::Numerics;

namespace TouchCastComposingEngine
{
	//TODO: Add Readonly property of Chromakey Color.
	public  delegate void ColorChangedEvent(Windows::UI::Color color);
	public  delegate void TouchcastChromaKeyEffectReady();
	public ref class TouchcastChromaKeyEffect sealed : Platform::Object
	{
	public:
		event ColorChangedEvent^ ColorChangedEvent;
		event TouchcastChromaKeyEffectReady^ TouchcastChromaKeyEffectReadyEvent;
		property Microsoft::Graphics::Canvas::ICanvasResourceCreator^ CanvasDevice
		{
			void set(Microsoft::Graphics::Canvas::ICanvasResourceCreator^ canvasDevice)
			{
				_canvasDevice = canvasDevice;
				_isChromaKeyEffectReady = true;
			}

			Microsoft::Graphics::Canvas::ICanvasResourceCreator^ get()
			{
				return _canvasDevice;
			}
		}

		property bool IsChromaKeyEffectReady
		{
			bool get()
			{
				return _isChromaKeyEffectReady;
			}

			void set(bool value)
			{
				_isChromaKeyEffectReady = value;
				if (_isChromaKeyEffectReady)
				{
					TouchcastChromaKeyEffectReadyEvent();
				}
			}
		}


		//Relative coordinates of tap  (0..1)
		property Windows::Foundation::Point ChromakeyColorPoint
		{
			Windows::Foundation::Point get()
			{
				return _chromakeyColorPoint;
			}
			void set(Windows::Foundation::Point point)
			{
				_chromakeyColorPoint = point;
				_chromakeyColorWasChanged = true;
			}
		}

		property float Tolerance
		{
			float get()
			{
				return _tolerance;
			}

			void set(float value)
			{
				value = value < 0.f ? 0.f : value;
				value = value > 1.f ? 1.f : value;
				_tolerance = value;
			}
		}

		property float Smoothness
		{
			float get()
			{
				return _smoothness;
			}

			void set(float value)
			{
				value = value < 0.f ? 0.f : value;
				value = value > 1.f ? 1.f : value;
				_smoothness = value;
			}
		}

		property ChromaKeyState State
		{
			void set(ChromaKeyState state)
			{
				if (_state != state)
				{
					_state = state;
				}
			}

			ChromaKeyState get()
			{
				return _state;
			}
		}

		property Windows::UI::Color ChromaKeyColor
		{
			Windows::UI::Color get()
			{
				return _lastChromakeyColor;
			}
			void set(Windows::UI::Color value)
			{
				_lastChromakeyColor = value;
				ConvertLastColorToYUVW();
				try
				{
					this->ColorChangedEvent(_lastChromakeyColor);
				}
				catch (Platform::Exception^ e)
				{
				}
			}
		}

		property Windows::Foundation::Size CameraResolution
		{
			Windows::Foundation::Size get()
			{
				return _cameraResolution;
			}
			void set(Windows::Foundation::Size size)
			{
				_cameraResolution = size;
			}
		}

		property bool ChromakeyColorWasChanged
		{
			bool get()
			{
				return _chromakeyColorWasChanged;
			}

			void set(bool value)
			{
				_chromakeyColorWasChanged = value;
			}
		}

		TouchcastChromaKeyEffect()
		{
			InitializePixelShader();
			
			_compositeEffect = ref new Microsoft::Graphics::Canvas::Effects::CompositeEffect();

			/*_lastChromakeyColor = Windows::UI::Colors::Green;
			ConvertLastColorToYUVW();*/

			_notValidPoint.X = -1; _notValidPoint.Y = -1;
			_chromakeyColorPoint = _notValidPoint;
			_tolerance = 0;
			_smoothness = 0;
			_chromakeyColorWasChanged = false;
		}
		Microsoft::Graphics::Canvas::ICanvasImage^ ProcessEffect(Microsoft::Graphics::Canvas::ICanvasBitmap^ inputBitmap, ChromaKeyType type)
		{
			std::lock_guard<std::mutex> lock(_chromakeyLock);

			auto point = _chromakeyColorPoint;
			if (point != _notValidPoint)
			{
				auto colors = inputBitmap->GetPixelColors((int)(point.X * _cameraResolution.Width), (int)(point.Y * _cameraResolution.Height), 3, 3);
				if (colors->Length > 0)
				{
					_lastChromakeyColor = CalculateAvaregeColor(colors);
					ConvertLastColorToYUVW();

					try
					{
						this->ColorChangedEvent(_lastChromakeyColor);
					}
					catch (Platform::Exception^ e)
					{
					}
				}
				_chromakeyColorPoint = _notValidPoint;
			}
			try
			{
				auto currentPixelShaderEffect = _pixelShaderEffects->GetAt((unsigned int)type);
				if (currentPixelShaderEffect != nullptr)
				{
					if (_state == ChromaKeyState::ChromaKeyEnabled)
					{
						
						currentPixelShaderEffect->Properties->Insert("sensivity", _tolerance);
						currentPixelShaderEffect->Properties->Insert("smoothing", _smoothness);
						///
						// Passing float4 struct and float[4] array to shader not work.
						//See Win2d Ticket : https://github.com/Microsoft/Win2D/issues/422
						///
						currentPixelShaderEffect->Properties->Insert("Y", _yuvColor[0]);
						currentPixelShaderEffect->Properties->Insert("U", _yuvColor[1]);
						currentPixelShaderEffect->Properties->Insert("V", _yuvColor[2]);
						currentPixelShaderEffect->Properties->Insert("W", _yuvColor[3]);
						
						currentPixelShaderEffect->Source1 = inputBitmap;

						return currentPixelShaderEffect;
					}
				}
			}
			catch (Platform::Exception^ e)
			{

			}
			return inputBitmap;
		}

	private:
		Microsoft::Graphics::Canvas::Effects::CompositeEffect^ _compositeEffect;
		Platform::Collections::Vector<Microsoft::Graphics::Canvas::Effects::PixelShaderEffect^>^ _pixelShaderEffects;
		Windows::Foundation::Size _cameraResolution;
		Microsoft::Graphics::Canvas::ICanvasResourceCreator^		_canvasDevice;
		
		ChromaKeyState _state;
		std::mutex	_chromakeyLock;
		Windows::UI::Color _lastChromakeyColor;
		Windows::Foundation::Point _notValidPoint;
		Windows::Foundation::Point _chromakeyColorPoint;
		float _yuvColor[4];
		float _smoothness;
		float _tolerance;
		bool _chromakeyColorWasChanged;
		bool _isChromaKeyEffectReady;
		bool _isNeedToMirrorBackground;		

		void InitializePixelShader()
		{
			auto test = Windows::ApplicationModel::Package::Current->InstalledLocation->Path;
			_pixelShaderEffects = ref new Platform::Collections::Vector<Microsoft::Graphics::Canvas::Effects::PixelShaderEffect^>();
			Concurrency::create_task(Windows::ApplicationModel::Package::Current->InstalledLocation->GetFileAsync(L"TouchCastComposingEngine\\Shaders\\ChromaKey.bin"))
				.then([this](concurrency::task<Windows::Storage::StorageFile^> shaderBinFile) {
				if (!shaderBinFile.is_done()) return;
				LoadPixelShaderFromFile(shaderBinFile.get());
			});

		}

		void LoadPixelShaderFromFile(Windows::Storage::StorageFile^ file)
		{
			Concurrency::create_task(Windows::Storage::FileIO::ReadBufferAsync(file),
				concurrency::task_continuation_context::use_current()).then([this](concurrency::task<Windows::Storage::Streams::IBuffer^> byteBuffer)
			{
				if (!byteBuffer.is_done()) return;
				auto iBuffer = byteBuffer.get();
				unsigned int length;
				auto pbytes = GetPointerToPixelData(iBuffer, &length);
				Platform::Array<byte>^ arr = ref new Platform::Array<byte>(pbytes, length);
				auto chromakeyPreviewEffect = ref new Microsoft::Graphics::Canvas::Effects::PixelShaderEffect(arr);
				_pixelShaderEffects->Append(chromakeyPreviewEffect);
				auto chromakeyRecordingEffect = ref new Microsoft::Graphics::Canvas::Effects::PixelShaderEffect(arr);
				_pixelShaderEffects->Append(chromakeyRecordingEffect);
			});
		}
		Windows::UI::Color CalculateAvaregeColor(Platform::Array<Windows::UI::Color> ^ colors)
		{
			Windows::UI::Color avarageColor;
			int sumTransparency = 0;
			int sumRed = 0;
			int sumBlue = 0;
			int sumGreen = 0;
			for (unsigned int i = 0; i < colors->Length; i++)
			{
				sumTransparency += colors[i].A;
				sumRed += colors[i].R;
				sumGreen += colors[i].G;
				sumBlue += colors[i].B;
			}
			avarageColor.A = sumTransparency / colors->Length;
			avarageColor.R = sumRed / colors->Length;
			avarageColor.B = sumBlue / colors->Length;
			avarageColor.G = sumGreen / colors->Length;
			return avarageColor;
		}
		void ConvertLastColorToYUVW()
		{
			_yuvColor[0] = 0.2989f * ((float)_lastChromakeyColor.R / 255) + 0.5866f * ((float)_lastChromakeyColor.G / 255) + 0.1145f * ((float)_lastChromakeyColor.B / 255);
			_yuvColor[1] = 0.7132f * (((float)_lastChromakeyColor.R / 255) - _yuvColor[0]);
			_yuvColor[2] = 0.5647f * (((float)_lastChromakeyColor.B / 255) - _yuvColor[0]);
			_yuvColor[3] = ((float)_lastChromakeyColor.A / 255);
		}

		byte* GetPointerToPixelData(Windows::Storage::Streams::IBuffer^ pixelBuffer, unsigned int *length)
		{
			if (length != nullptr)
			{
				*length = pixelBuffer->Length;
			}
			// Query the IBufferByteAccess interface.
			Microsoft::WRL::ComPtr<Windows::Storage::Streams::IBufferByteAccess> bufferByteAccess;
			reinterpret_cast<IInspectable*>(pixelBuffer)->QueryInterface(IID_PPV_ARGS(&bufferByteAccess));

			// Retrieve the buffer data.
			byte* pixels = nullptr;
			bufferByteAccess->Buffer(&pixels);
			return pixels;
		}
	};

}