#pragma once
#pragma once
#include <pplawait.h>
#include "ComposingTypes.h"
#include "ComposingContext.h"

namespace TouchCastComposingEngine
{
	public ref class VideoVappCompositor sealed : Windows::Media::Effects::IVideoCompositor
	{
	public:
		VideoVappCompositor()
		{

		}

		// Inherited via IVideoCompositor
		virtual void SetProperties(Windows::Foundation::Collections::IPropertySet ^configuration)
		{
			if (configuration != nullptr && configuration->HasKey("Id"))
			{
				_type = (VideoEffectType)configuration->Lookup(ComposingContextElement::EffectKeyType);
				if (_type == VideoEffectType::VideoVappType)
				{
					_vappId = (Platform::String ^)configuration->Lookup("Id");
				}
			}
		}
		virtual property bool TimeIndependent
		{
			bool get()
			{
				return false;
			}
		}
		virtual void SetEncodingProperties(Windows::Media::MediaProperties::VideoEncodingProperties ^backgroundProperties, Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice ^device)
		{
			if (_canvasDevice == nullptr)
			{
				_canvasDevice = Microsoft::Graphics::Canvas::CanvasDevice::CreateFromDirect3D11Device(device);
			}
		}
		virtual void CompositeFrame(Windows::Media::Effects::CompositeVideoFrameContext ^context)
		{
			if (ComposingContext::Shared()->State == ComposingState::Previewing
				|| ComposingContext::Shared()->State == ComposingState::Paused)
			{
				return;
			}
			if (_type == VideoEffectType::VideoVappType)
			{
				try
				{
					auto device = ComposingContext::Shared()->PreviewingDevice;
					if (device != nullptr)
					{
						auto inputBitmap = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromDirect3D11Surface(device, context->OutputFrame->Direct3DSurface);
						ComposingContext::Shared()->AddVideoVappBitmapById(inputBitmap, _vappId);
					}
				}
				catch (Platform::Exception ^ e)
				{

				}
			}
		}
		virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
		{
			_canvasDevice = nullptr;
		}
		virtual void DiscardQueuedFrames()
		{

		}
	private:
		Microsoft::Graphics::Canvas::ICanvasResourceCreator^		_canvasDevice = nullptr;
		std::mutex	_lock;
		Platform::String^ _vappId;
		VideoEffectType _type;

	};
}


