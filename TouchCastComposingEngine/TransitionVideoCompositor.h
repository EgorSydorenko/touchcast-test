#pragma once
#include <pplawait.h>

namespace TouchCastComposingEngine
{
	public ref class TransitionVideoCompositor sealed : Windows::Media::Effects::IVideoCompositor
	{
	public:
		TransitionVideoCompositor(){}

		// Inherited via IVideoCompositor
		virtual void SetProperties(Windows::Foundation::Collections::IPropertySet ^configuration)
		{

		}
		virtual property bool TimeIndependent
		{
			bool get()
			{
				return false;
			}
		}
		virtual void SetEncodingProperties(Windows::Media::MediaProperties::VideoEncodingProperties ^backgroundProperties, Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice ^device)
		{
			if (_canvasDevice == nullptr)
			{
				_canvasDevice = Microsoft::Graphics::Canvas::CanvasDevice::CreateFromDirect3D11Device(device);
			}
		}
		virtual void CompositeFrame(Windows::Media::Effects::CompositeVideoFrameContext ^context)
		{
			try
			{
				std::lock_guard<std::mutex> lock(_lock);
				auto outputSurface = context->OutputFrame->Direct3DSurface;
				auto outputRenderTarget = Microsoft::Graphics::Canvas::CanvasRenderTarget::CreateFromDirect3D11Surface(_canvasDevice, outputSurface);
				auto drawingSession = outputRenderTarget->CreateDrawingSession();
				auto currentFrameTimeInComposition = context->OutputFrame->RelativeTime->Value;
				if (context->SurfacesToOverlay->Size >= 1)
				{
					for (unsigned int i = 0; i < context->SurfacesToOverlay->Size; i++)
					{
						auto surface = context->SurfacesToOverlay->GetAt(i);
						if (surface != nullptr)
						{
							auto surfaceOverlay = context->GetOverlayForSurface(surface);
							auto overlayCanvasBitmap = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromDirect3D11Surface(_canvasDevice, surface);
							float opacity = 0;
							if (surfaceOverlay->Opacity == 0.0)
							{
								opacity = (float)(currentFrameTimeInComposition.Duration - surfaceOverlay->Delay.Duration) / surfaceOverlay->Clip->TrimmedDuration.Duration;
							}
							else
							{
								opacity = 1 - (float)(currentFrameTimeInComposition.Duration - surfaceOverlay->Delay.Duration) / surfaceOverlay->Clip->TrimmedDuration.Duration;
							}
							drawingSession->DrawImage(overlayCanvasBitmap, 0, 0, surfaceOverlay->Position, opacity);
						}

					}
				}
				else
				{

				}
			}
			catch(Platform::Exception ^ e)
			{

			}
		}
		virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
		{
			_canvasDevice = nullptr;
		}
		virtual void DiscardQueuedFrames()
		{

		}
	private:
		Microsoft::Graphics::Canvas::ICanvasResourceCreator^		_canvasDevice = nullptr;
		std::mutex	_lock;
	};
}

