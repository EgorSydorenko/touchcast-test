#pragma once
#include <collection.h>
#include "Service.h"
#include "Constants.h"

namespace TouchCastComposingEngine
{
	namespace Win32BackgroundPipe
	{
		public ref class Manager sealed : Platform::Object
		{
		internal:

			static Manager^ Shared()
			{
				static Manager^ _instance;
				if (_instance == nullptr)
				{
					_instance = ref new Manager();
				}

				return _instance;
			}

			bool IsServiceReady(ServiceType type)
			{
				return TryGetBackgroundService(type) != nullptr;
			}

			Windows::Foundation::IAsyncAction^ StartNativeService(ServiceType type)
			{
				auto backgroundService = TryGetBackgroundService(type);
				if (backgroundService != nullptr)
				{
					return create_async([] {});
				}
				auto id = TypeToString(type);
				return Windows::ApplicationModel::FullTrustProcessLauncher::LaunchFullTrustProcessForCurrentAppAsync(id);

			}

			Service^ TryGetBackgroundService(ServiceType type)
			{
				Service^ result = nullptr;
				if (_connections->HasKey(type))
				{
					result = _connections->Lookup(type);
				}
				return result;
			}

			void Initialize(Windows::ApplicationModel::Activation::BackgroundActivatedEventArgs^ backgroundEventArgs)
			{
				auto triggerDetails = dynamic_cast<Windows::ApplicationModel::AppService::AppServiceTriggerDetails^>(backgroundEventArgs->TaskInstance->TriggerDetails);
				if (triggerDetails != nullptr)
				{
					auto backgroundService = ref new Service(triggerDetails->AppServiceConnection, backgroundEventArgs->TaskInstance->GetDeferral());
					auto taskInstance = backgroundEventArgs->TaskInstance;
					_cancelToken = taskInstance->Canceled += ref new Windows::ApplicationModel::Background::BackgroundTaskCanceledEventHandler(this, &Manager::Cancel);

					ServiceType type = StringToWin32ServiceType(taskInstance->Task->Name);
					_connections->Insert(type, backgroundService);
				}
			}

			Windows::Foundation::IAsyncAction^ StopBackgroundService(ServiceType type)
			{
				auto service = TryGetBackgroundService(type);
				if (service != nullptr)
				{
					return service->Close();
				}
				else
				{
					return create_async([] {});
				}
			}
			Windows::Foundation::IAsyncOperation<Platform::Object^>^ SendMessage(ServiceType type, Windows::Foundation::Collections::ValueSet^ data)
			{
				return create_async([this, type, data]()
				{
					auto backgroundService = TryGetBackgroundService(type);
					if (backgroundService != nullptr)
					{
						return backgroundService->SendMessage(data).then([this](task<Windows::ApplicationModel::AppService::AppServiceResponse^> responseTask)
						{
							auto response = responseTask.get();
							return this->ProcessResponse(response->Message);
						});
					}
					else
					{
						return create_task([] 
						{
							return create_async([]() -> Platform::Object^
							{
								return nullptr;
							});
						});
					}
				}
				);
			}



		private:

			Manager()
			{
				_connections = ref new Platform::Collections::Map<ServiceType, Service^>();
			}

			void Cancel(Windows::ApplicationModel::Background::IBackgroundTaskInstance^ sender, Windows::ApplicationModel::Background::BackgroundTaskCancellationReason reason)
			{
				ServiceType type = StringToWin32ServiceType(sender->Task->Name);
				sender->Canceled -= _cancelToken;
				if (_connections->HasKey(type))
				{
					auto service = _connections->Lookup(type);
					service->Complete();
					_connections->Remove(type);
				}
			}

			Platform::Collections::Map<ServiceType, Service ^> ^ _connections;
			Windows::Foundation::EventRegistrationToken _cancelToken;

			Platform::String^ TypeToString(ServiceType type)
			{
				Platform::String^ nameSpace = nullptr;
				switch (type)
				{
				case ServiceType::MSOfficeDocumentConverter:
					nameSpace = Constant::Instance()->MicrosoftOffice;
					break;
				case ServiceType::Prisma:
					nameSpace = Constant::Instance()->Prisma;
					break;
				case ServiceType::Personify:
					nameSpace = Constant::Instance()->Personify;
					break;
				}
				return nameSpace;
			}

			ServiceType StringToWin32ServiceType(Platform::String^ str)
			{
				ServiceType type;
				if (str->Equals(Constant::Instance()->MicrosoftOffice))
				{
					type = ServiceType::MSOfficeDocumentConverter;
				}
				else if (str->Equals(Constant::Instance()->Prisma))
				{
					type = ServiceType::Prisma;
				}
				else if (str->Equals(Constant::Instance()->Personify))
				{
					type = ServiceType::Personify;
				}
				return type;
			}

			Platform::Object^ ProcessResponse(Windows::Foundation::Collections::ValueSet^ valueSet)
			{
				Platform::Object^ result = nullptr;
				if (valueSet->HasKey(Constant::Instance()->Result))
				{
					auto value = valueSet->Lookup(Constant::Instance()->Result);
					auto str = value->ToString();

					if (str == Constant::Instance()->Ok)
					{
						result = value;

						if (valueSet->HasKey(Constant::Instance()->Output))
						{
							result = valueSet->Lookup(Constant::Instance()->Output);
						}
					}
					else if (str == Constant::Instance()->Failed)
					{
						result = value;

						if (valueSet->HasKey(Constant::Instance()->ErrorCode))
						{
							result = valueSet->Lookup(Constant::Instance()->ErrorCode);
						}
					}
				}
				return result;
			}


		};
	}
}