#pragma once
#include <pplawait.h>
#include <ppltasks.h>
using namespace concurrency;
namespace TouchCastComposingEngine
{
	namespace Win32BackgroundPipe
	{
		public enum class ServiceType
		{
			MSOfficeDocumentConverter,
			Prisma,
			Personify
		};

		public ref class Service sealed : Platform::Object
		{
		internal:
#pragma region Properties

			property Windows::ApplicationModel::AppService::AppServiceConnection^ Connection
			{
				Windows::ApplicationModel::AppService::AppServiceConnection^ get()
				{
					return _connection;
				}
			}

#pragma endregion

			Service(Windows::ApplicationModel::AppService::AppServiceConnection^ connection, Windows::ApplicationModel::Background::BackgroundTaskDeferral^ deferal)
			{
				_connection = connection;
				_deferal = deferal;

				_closedToken = _connection->ServiceClosed += ref new Windows::Foundation::TypedEventHandler<Windows::ApplicationModel::AppService::AppServiceConnection^,
					Windows::ApplicationModel::AppService::AppServiceClosedEventArgs^>(this, &Service::Connection_ServiceClosed);
			}

			Windows::Foundation::IAsyncAction^ Close()
			{
				return create_async([]() {});
			}

			task<Windows::ApplicationModel::AppService::AppServiceResponse^> SendMessage(Windows::Foundation::Collections::ValueSet^ valueSet)
			{
				return concurrency::create_task(_connection->SendMessageAsync(valueSet));
			}
			void Complete() 
			{
				_connection->ServiceClosed -= _closedToken;
				_deferal->Complete();
				_connection = nullptr;
				_deferal = nullptr;
			}
		private:



			Windows::ApplicationModel::AppService::AppServiceConnection^ _connection;
			Windows::ApplicationModel::Background::BackgroundTaskDeferral^ _deferal;
			Windows::Foundation::EventRegistrationToken _closedToken;

			void Connection_ServiceClosed(Windows::ApplicationModel::AppService::AppServiceConnection^ sender, Windows::ApplicationModel::AppService::AppServiceClosedEventArgs^ eventArgs)
			{
				_connection->ServiceClosed -= _closedToken;
			}
		};
	}
}