#pragma once

#include <collection.h>
#include "ComposingContext.h"
#include <sstream>

namespace TouchCastComposingEngine
{
	public ref class AudioImportEffect sealed : public  Windows::Media::Effects::IBasicAudioEffect
	{
	public:

		AudioImportEffect()
		{
			_prefferedAudioFormat = ref new Platform::Collections::Vector<Windows::Media::MediaProperties::AudioEncodingProperties^>();
			auto audioProperties = ComposingContext::Shared()->Audio->PrefferedAudioFormat;
			_prefferedAudioFormat->Append(audioProperties);
		}

		property Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::AudioEncodingProperties ^>^ SupportedEncodingProperties
		{
			virtual Windows::Foundation::Collections::IVectorView<Windows::Media::MediaProperties::AudioEncodingProperties ^>^ get()
			{
				return _prefferedAudioFormat->GetView();
			}
		}

		property bool UseInputFrameForOutput
		{
			virtual bool  get() { return true; }
		}

		virtual void SetEncodingProperties(Windows::Media::MediaProperties::AudioEncodingProperties ^ encodingProperties)
		{
			_actualAudioFormat = encodingProperties;
		}

		virtual void SetProperties(Windows::Foundation::Collections::IPropertySet^ configuration)
		{

		}

		virtual void ProcessFrame(Windows::Media::Effects::ProcessAudioFrameContext ^ context)
		{
			if (!ComposingContext::Shared()->IsRecording) return;
			auto inputFrame = context->InputFrame;
			auto inputAudioBuffer = inputFrame->LockBuffer(Windows::Media::AudioBufferAccessMode::Read);
			ComposingContext::Shared()->Audio->PushAudioFromBuffer(inputAudioBuffer, _actualAudioFormat, inputFrame->RelativeTime->Value, inputFrame->Duration->Value);
			
		}

		virtual void Close(Windows::Media::Effects::MediaEffectClosedReason reason)
		{
			ComposingContext::Shared()->Audio->Reset();
		}

		virtual void DiscardQueuedFrames()
		{
			ComposingContext::Shared()->Audio->Reset();
		}

	private:

		Platform::Collections::Vector<Windows::Media::MediaProperties::AudioEncodingProperties^>^	_prefferedAudioFormat;
		Windows::Media::MediaProperties::AudioEncodingProperties^									_actualAudioFormat;
	};
}