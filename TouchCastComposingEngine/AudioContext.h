#pragma once

#include <collection.h>
#include <mutex>
#include <MemoryBuffer.h>
#include <iostream>
#define _USE_MATH_DEFINES
#include "math.h"
#include "WindowsNumerics.h"
#include "intsafe.h"

namespace TouchCastComposingEngine
{
	public ref class AudioContext sealed
	{
	public:

		//audio format
		property Windows::Media::MediaProperties::AudioEncodingProperties^ PrefferedAudioFormat
		{
			Windows::Media::MediaProperties::AudioEncodingProperties^ get()
			{				
				return audioProperties;
			}
		}

		property float MaxBufferCapacityInSeconds
		{
			float get()
			{
				//buffer will store up to 1 sec of audio
				return 1.0f;
			}
		}


		void PushAudioFromBuffer(Windows::Media::AudioBuffer^ buffer, Windows::Media::MediaProperties::AudioEncodingProperties^ actualAudioFormat,
			Windows::Foundation::TimeSpan time, Windows::Foundation::TimeSpan duration)
		{
			if (!buffer)
			{
				_ASSERT(0);
				return;
			}

			if (!AudioFormatsAreCompatible(actualAudioFormat, PrefferedAudioFormat))
			{
				_ASSERT(0);
				return;
			}

			//getting access to buffer bytes
			auto audioBufferReference = buffer->CreateReference();
			size_t bufferLength = buffer->Length;

			Microsoft::WRL::ComPtr<Windows::Foundation::IMemoryBufferByteAccess> bufferWithButesAccess = nullptr;

			HRESULT result = S_OK;

			result = reinterpret_cast<IInspectable*>(audioBufferReference)->QueryInterface(IID_PPV_ARGS(&bufferWithButesAccess));

			if (result != S_OK)
			{
				_ASSERT(0);
				return;
			}

			BYTE* bufferContents = nullptr;
			UINT32 bufferSize = 0;

			result = bufferWithButesAccess->GetBuffer(&bufferContents, &bufferSize);
			
			if (actualAudioFormat->ChannelCount != PrefferedAudioFormat->ChannelCount)
			{
				size_t tmpBufferLength = (size_t)(bufferLength * (float)((float)PrefferedAudioFormat->ChannelCount / (float)actualAudioFormat->ChannelCount));
				BYTE * tmpBuffer = new BYTE[tmpBufferLength];
				AudioChanelConverter::ConvertBufferChannels(bufferContents, bufferLength, actualAudioFormat, tmpBuffer, tmpBufferLength, PrefferedAudioFormat);
				//_audioBuffer->PushAudioBytes(tmpBuffer, tmpBufferLength);
				_audioBuffer->PushTCAudioFrame(new TCAudioFrame(tmpBuffer, tmpBufferLength, time, duration));
				delete [] tmpBuffer;
			}
			else
			{
				_audioBuffer->PushTCAudioFrame(new TCAudioFrame(bufferContents, bufferLength,time,duration));
			}
		}

		void PopAudioToBuffer(Windows::Media::AudioBuffer^ buffer, Windows::Media::MediaProperties::AudioEncodingProperties^ actualAudioFormat,
			Windows::Foundation::TimeSpan time, Windows::Foundation::TimeSpan duration)
		{

			if (!buffer)
			{
				_ASSERT(0);
				return;
			}

			if (!AudioFormatsAreCompatible(actualAudioFormat, PrefferedAudioFormat))
			{
				_ASSERT(0);
				return;
			}

			//getting access to buffer bytes
			auto audioBufferReference = buffer->CreateReference();
			auto bufferLength = buffer->Length;

			Microsoft::WRL::ComPtr<Windows::Foundation::IMemoryBufferByteAccess> bufferWithButesAccess = nullptr;

			HRESULT hresult = S_OK;

			hresult = reinterpret_cast<IInspectable*>(audioBufferReference)->QueryInterface(IID_PPV_ARGS(&bufferWithButesAccess));

			if (hresult != S_OK)
			{
				_ASSERT(0);
				return;
			}

			BYTE* bufferContents = nullptr;
			UINT32 bufferSize = 0;

			hresult = bufferWithButesAccess->GetBuffer(&bufferContents, &bufferSize);

			if (hresult != S_OK)
			{
				_ASSERT(0);
				return;
			}

			if (actualAudioFormat->ChannelCount != PrefferedAudioFormat->ChannelCount)
			{
				size_t tmpBufferLength = (size_t)(bufferLength * (float)((float)PrefferedAudioFormat->ChannelCount / (float)actualAudioFormat->ChannelCount));
				BYTE * tmpBuffer = new BYTE[tmpBufferLength];
				_audioBuffer->PopBytes(time, duration, tmpBufferLength, tmpBuffer);
				
				AudioChanelConverter::ConvertBufferChannels(tmpBuffer, tmpBufferLength, PrefferedAudioFormat, bufferContents, bufferLength, actualAudioFormat);
				delete [] tmpBuffer;
			}
			else
			{
				_audioBuffer->PopBytes(time, duration, (size_t)bufferLength, bufferContents);
			}

		}

		void Reset()
		{
			_audioBuffer->Reset();
		}

		AudioContext()
		{
			audioProperties = Windows::Media::MediaProperties::AudioEncodingProperties::CreatePcm(48000, 2, 16);
			audioProperties->Subtype = Windows::Media::MediaProperties::MediaEncodingSubtypes::Pcm;
			size_t bufferCapacity = (size_t)(this->MaxBufferCapacityInSeconds *
				this->PrefferedAudioFormat->SampleRate *
				this->PrefferedAudioFormat->BitsPerSample *
				this->PrefferedAudioFormat->ChannelCount /
				8.0f);

			_audioBuffer = new TCAudioFrameBuffer();
		}

		virtual ~AudioContext()
		{
			delete _audioBuffer;
		}

		void VideoPaused()
		{
			_audioBuffer->Pause();
		}

		void SeekStarted()
		{
			_audioBuffer->SeekStarted();
		}
		void SeekCompleted()
		{
			_audioBuffer->SeekCompleted();
		}

	private:

		bool AudioFormatsAreCompatible(Windows::Media::MediaProperties::AudioEncodingProperties^ f1, Windows::Media::MediaProperties::AudioEncodingProperties^ f2)
		{
			if (f1 == nullptr || f2 == nullptr)
			{
				return false;
			}

			if (f1->SampleRate != f2->SampleRate)
			{
				return false;
			}

			if (f1->BitsPerSample != f2->BitsPerSample)
			{
				return false;
			}

			/*
			**No need to check channel count because we have  AudioChanelConverter
			*/
			
			return true;
		}

		class CircularAudioBuffer
		{
		public:

			CircularAudioBuffer(size_t size)
			{
				_bufferBytesCapacity = size;
				_buffer = new BYTE[_bufferBytesCapacity];

				_readPointer = 0;
				_writePointer = 0;
				_currentBytesSize = 0;

				//PerformTest();
			}

			virtual ~CircularAudioBuffer()
			{
				if (_buffer)
				{
					delete[] _buffer;
					_buffer = nullptr;
				}
			}

			size_t PushAudioBytes(BYTE* bytes, UINT32 size)
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);

				size_t bytesWritten = 0;

				while (bytesWritten < size)
				{
					size_t bytesToWrite = size - bytesWritten;

					bool moveWritePointerToBeginning = false;

					if (bytesToWrite > _bufferBytesCapacity - _writePointer)
					{
						bytesToWrite = _bufferBytesCapacity - _writePointer;
						moveWritePointerToBeginning = true;
					}

					memcpy(_buffer + _writePointer, bytes + bytesWritten, bytesToWrite);

					bytesWritten += bytesToWrite;
					_currentBytesSize += bytesToWrite;
					_writePointer += bytesToWrite;

					if (moveWritePointerToBeginning)
					{
						_writePointer = 0;
					}

					if (_currentBytesSize > _bufferBytesCapacity)
					{
						//overflow
						//we exceeded size of the buffer
						//older data was removed
						//read pointer should be equal to write pointer
						_readPointer = _writePointer;
						_currentBytesSize = _bufferBytesCapacity;
					}
				}

				return size;
			}

			size_t PopAudioBytesToBuffer(BYTE* bytes, UINT32 size, bool resetAfterPause = false)
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);
				size_t bytesRead = 0;

				while (bytesRead < size)
				{
					size_t bytesToRead = size - bytesRead;

					if (_currentBytesSize < bytesToRead)
					{
						bytesToRead = _currentBytesSize;
					}

					bool moveReadPointerToBeginning = false;

					if (bytesToRead > _bufferBytesCapacity - _readPointer)
					{
						bytesToRead = _bufferBytesCapacity - _readPointer;
						moveReadPointerToBeginning = true;
					}

					if (bytesToRead > 0 && _currentBytesSize > 0)
					{
						memcpy(bytes + bytesRead, _buffer + _readPointer, bytesToRead);

						bytesRead += bytesToRead;
						_readPointer += bytesToRead;
						_currentBytesSize -= bytesToRead;
					}
					else
					{
						//clearing the rest of the memory
						memset(bytes + bytesRead, 0, size - bytesRead);
						return size;
					}

					if (moveReadPointerToBeginning)
					{
						_readPointer = 0;
					}
				}

				//auto str = "Pop Size:\t" + _currentBytesSize + "\tRead buffer:\t"+ size +"\tRead Pointer:\t"+ _readPointer + "\tWrite Pointer:\t"+_writePointer+"\n";
				//OutputDebugString(str->Data());
				return bytesRead;
			}

			size_t CurrentSize()
			{
				return _currentBytesSize;
			}

			void Reset()
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);
				_readPointer = 0;
				_writePointer = 0;
				_currentBytesSize = 0;
			}

		private:

			//function tests that reading and writing to cyclic buffer works propertly
			void PerformTest()
			{
				int iterations = 10000;
				int bufferSize = 1024;

				BYTE* buffer = new BYTE[bufferSize];
				BYTE* testBuffer = new BYTE[bufferSize];
				size_t counter = 0;

				for (int i = 0; i < iterations; i++)
				{
					//populating
					for (int t = 0; t < bufferSize; t++)
					{
						counter++;
						testBuffer[t] = buffer[t] = counter % 0xff;
					}

					//writing
					int putButes = 0;
					while (putButes < bufferSize)
					{
						PushAudioBytes(buffer + putButes, 8);
						putButes += 8;
					}

					memset(buffer, 0, bufferSize);

					//reading
					int readButes = 0;
					while (readButes < bufferSize)
					{
						PopAudioBytesToBuffer(buffer + readButes, 512);
						readButes += 512;
					}

					//checking
					for (int t = 0; t < bufferSize; t++)
					{
						if (testBuffer[t] != buffer[t])
						{
							_ASSERT(0);
						}
					}
				}

				DEBUG_EVENT();
			}

		private:
			BYTE*		_buffer = nullptr;
			size_t		_bufferBytesCapacity = 0;

			size_t		_readPointer = 0;
			size_t		_writePointer = 0;
			size_t		_currentBytesSize = 0;
			std::mutex	_audioBufferLock;
		};

		class AudioChanelConverter
		{
		public:

			static HRESULT ConvertBufferChannels(BYTE * inputBuffer,
				size_t inputBufferLength,
				Windows::Media::MediaProperties::AudioEncodingProperties^ inputAudioEncodingProperties,
				BYTE * outputBuffer, 
				size_t outputBufferLength, 
				Windows::Media::MediaProperties::AudioEncodingProperties^ outputAudioEncodingProperties)
			{
				if (inputAudioEncodingProperties == nullptr)
				{
					_ASSERT(0);
					return E_FAIL;
				}

				if (outputAudioEncodingProperties == nullptr)
				{
					_ASSERT(0);
					return E_FAIL;
				}

				memset(outputBuffer, 0, outputBufferLength);

				if (inputAudioEncodingProperties->BitsPerSample != outputAudioEncodingProperties->BitsPerSample)
				{
					_ASSERT(0);
					return E_FAIL;
				}

				if (outputBufferLength != ((float)outputAudioEncodingProperties->ChannelCount / (float)inputAudioEncodingProperties->ChannelCount) * inputBufferLength)
				{
					_ASSERT(0);
					return E_FAIL;
				}

				size_t inputStride = (inputAudioEncodingProperties->BitsPerSample / CHAR_BIT)  * inputAudioEncodingProperties->ChannelCount;
				size_t outputStride = (outputAudioEncodingProperties->BitsPerSample / CHAR_BIT) * outputAudioEncodingProperties->ChannelCount;

				size_t copyCycleCount = outputStride / inputStride;
				copyCycleCount = copyCycleCount == 0 ? 1 : copyCycleCount;

				size_t lowestStride = inputStride < outputStride ? inputStride : outputStride;

				size_t inputIndex = 0;

				while (inputIndex < inputBufferLength)
				{
					size_t  copyCycleIndex = 0;

					while (copyCycleIndex < copyCycleCount)
					{
						memcpy(outputBuffer, inputBuffer, lowestStride);
						outputBuffer += lowestStride;
						copyCycleIndex++;
					}

					inputBuffer += inputStride;
					inputIndex += inputStride;
				}

				return S_OK;
			}
		};

		class TCAudioFrame 
		{
		public:

			BYTE* Buffer()
			{
				return _buffer;
			}
			

			size_t BufferSize()
			{
				return _bufferSize;
			}
			
			Windows::Foundation::TimeSpan FrameTime()
			{
				return _frameTime;
			}
			

			Windows::Foundation::TimeSpan FrameDuration()
			{
				return _frameDuration;
			}
			

			TCAudioFrame(BYTE * bytes, size_t bufferSize, Windows::Foundation::TimeSpan frameTime, Windows::Foundation::TimeSpan frameDuration)
			{
				_bufferSize = bufferSize;
				_frameTime = frameTime;
				_frameDuration = frameDuration;
				_buffer = new BYTE[_bufferSize];
				memcpy(_buffer, bytes, _bufferSize);
				
			}
			~TCAudioFrame()
			{
				delete[] _buffer;
			}
			

		private:
			BYTE*							_buffer = nullptr;
			size_t							_bufferSize = 0;
			Windows::Foundation::TimeSpan	_frameTime;
			Windows::Foundation::TimeSpan	_frameDuration;
			double							_bytesPerTick;
		};

		class TCAudioFrameBuffer
		{
		public:
			TCAudioFrameBuffer()
			{
				_audiobuffer = new std::vector<TCAudioFrame*>();
				//audiobuffer = ref new Platform::Collections::Vector<TCAudioFrame *>();
			}

			void PopBytes(Windows::Foundation::TimeSpan time, Windows::Foundation::TimeSpan duration, size_t bufferSize, BYTE* bytes)
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);
				memset(bytes, 0, bufferSize);
				if (_audiobuffer->size() == 0)
				{	
					return;
				}
				 
				if (TimeRange == LONGLONG_ERROR)
				{
					if (_audiobuffer->size() > 0)
					{
						TimeRange = time.Duration - _audiobuffer->at(0)->FrameTime().Duration;
					}
				}

				Windows::Foundation::TimeSpan startTime;
				startTime.Duration = time.Duration - TimeRange;

				Windows::Foundation::TimeSpan endTime;
				endTime.Duration = startTime.Duration + duration.Duration;
				auto bytesPerTick = (double)bufferSize / duration.Duration;

				size_t startWritePointer = 0;
				std::vector<unsigned int> * removeIndexes = new std::vector<unsigned int>();
				
				//Platform::Collections::Vector<int>^ toRemove = ref new Platform::Collections::Vector<int>();
				
				for (unsigned int i = 0; i < _audiobuffer->size(); i++)
				{
					auto audioFrame = _audiobuffer->at(i);
					auto frameStartTime = audioFrame->FrameTime().Duration;
					auto frameEndTime = frameStartTime + audioFrame->FrameDuration().Duration;

					bool isInFrameRange = (frameStartTime <= startTime.Duration && startTime.Duration < frameEndTime);
					bool isPrefiousFrame = frameStartTime <= lastWitedFrameTime && frameEndTime > lastWitedFrameTime;
					

					if (isInFrameRange || isPrefiousFrame)
					{
						size_t startReadPointer = 0;
						size_t size = 0;

						if (isPrefiousFrame)
						{
							startReadPointer = (size_t)ReadOffset;
							if (startReadPointer >= audioFrame->BufferSize())
							{
								startReadPointer = 0;
							}
							size = bufferSize - startWritePointer;
							if (audioFrame->BufferSize() < size + startReadPointer)
							{
								size = audioFrame->BufferSize() - startReadPointer;
							}
						}
						else if (isInFrameRange)
						{
							
							long long timeDelta = (startTime.Duration - frameStartTime);
							startReadPointer += (size_t)(timeDelta * bytesPerTick);
							size = (size_t)(bytesPerTick * (audioFrame->FrameDuration().Duration - timeDelta));

							if (size > (bufferSize - startWritePointer))
							{
								size = bufferSize - startWritePointer;
							}

							if (startReadPointer + size > audioFrame->BufferSize())
							{
								if (audioFrame->BufferSize() > size)
								{
									startReadPointer = audioFrame->BufferSize() - size;
								}
								else 
								{
									startReadPointer = 0;
									size = audioFrame->BufferSize();
								}
							}
						}

						if (startReadPointer >= audioFrame->BufferSize())
						{
							startReadPointer = 0;
							size = bufferSize - startWritePointer;
							if (audioFrame->BufferSize() < size + startReadPointer)
							{
								size = audioFrame->BufferSize() - startReadPointer;
							}
						}
						
						try
						{
							memcpy(bytes + startWritePointer, audioFrame->Buffer() + startReadPointer, size);
						}
						catch (Platform::Exception^ e)
						{
							memset(bytes + startWritePointer, 0, size);
						}

						startWritePointer += size;

						if (lastWitedFrameTime == LONGLONG_ERROR)
						{
							lastWitedFrameTime = (long long)(frameStartTime + (size / bytesPerTick));
						}
						else 
						{
							lastWitedFrameTime += (long long)(size / bytesPerTick);
							if ((frameEndTime - lastWitedFrameTime) <= 1)
							{
								removeIndexes->push_back(i);
								lastWitedFrameTime = frameEndTime + 1;	
							}
						}

						if(ReadOffset + size >= audioFrame->BufferSize())
						{
							ReadOffset = 0;
						}
						else 
						{
							ReadOffset = size + startReadPointer;
						}	
					}

					if (startWritePointer >= bufferSize)
					{
						break;
					}
				}

				if (removeIndexes->size() > 0)
				{
					if (removeIndexes->size() > 1)
					{
						std::sort(removeIndexes->begin(), removeIndexes->end(), std::greater<unsigned int>());
					}

					for (unsigned int i = 0; i < removeIndexes->size(); i++)
					{
						if (_audiobuffer->size() > removeIndexes->at(i))
						{
							_audiobuffer->erase(_audiobuffer->begin() + removeIndexes->at(i));
						}
					}
				}

				delete removeIndexes;
				//for each (auto index in toRemove)
				//{
				//	if(_audiobuffer->size() > index)
				//	{
				//		_audiobuffer->erase(_audiobuffer->begin() + index);
				//	}
				//}
				

				if (startWritePointer < bufferSize)
				{
					memset(bytes + startWritePointer, 0, bufferSize - startWritePointer);
				}
			}

			void Reset()
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);
				_audiobuffer->clear();
				ResetAudioPosition();
			}

			void Pause()
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);
				_audiobuffer->clear();
				ResetAudioPosition();
			}

			void PushTCAudioFrame(TCAudioFrame* frame)
			{
				std::lock_guard<std::mutex> lock(_audioBufferLock);
				_audiobuffer->push_back(frame);

			}

			void SeekStarted()
			{
				//std::lock_guard<std::mutex> lock(_audioBufferLock);
				//_audiobuffer->clear();
				//ResetAudioPosition();
				//_isSeeking = true;
			}

			void SeekCompleted()
			{
				//std::lock_guard<std::mutex> lock(_audioBufferLock);
				//_isSeeking = false;
				//_isDiscardedQueue = false;
				//_seekCompletedTime = std::chrono::system_clock::now();
			}

		private:
			std::vector<TCAudioFrame*>* _audiobuffer;
			std::mutex	_audioBufferLock;
			long long TimeRange = LONGLONG_ERROR;
			long long lastWitedFrameTime = LONGLONG_ERROR;
			long long ReadOffset = 0;
			bool  _isSeeking;
			bool _isDiscardedQueue;
			std::chrono::time_point<std::chrono::system_clock> _resetTime, _seekCompletedTime;

			void ResetAudioPosition()
			{
				TimeRange = LONGLONG_ERROR;
				lastWitedFrameTime = LONGLONG_ERROR;
				ReadOffset = 0;
			}
		};

	private:
		//CircularAudioBuffer*	_audioBuffer = nullptr;
		TCAudioFrameBuffer *	_audioBuffer = nullptr;

		Windows::Media::MediaProperties::AudioEncodingProperties ^ audioProperties;


		
	};
}
