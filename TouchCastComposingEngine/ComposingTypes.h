
#pragma once

namespace TouchCastComposingEngine
{

	public enum class TransitionType
	{
		None,
		CrossFade,
		ThrowBlack,
		ThrowWhite
	};


	public enum class ContentMode
	{
		ScaleToFill,
		ScaleAspectFit,
		ScaleAspectFill
	};

	public enum class ComposingContextElementType
	{
		Unknown,
		CameraPreview,
		Shape,
		Bitmap,
		Video,
		Strokes
	};
	public enum class ComposingShapeSubType
	{
		Unknown,
		Rectangle,
		Circle,
		Arrow,
		Line
	};
	public enum class BitmapRotation
	{
		NoRotation = 0,
		Rotation90DegreesCCW = 90,
		Rotation180DegreesCCW = 180,
		Rotation270DegreesCCW = 270,
	};

	public enum class ChromaKeyState
	{
		ChromaKeyDisabled,
		ChromaKeyEnabled
	};

	public enum class VideoEffectType
	{
		VideoImportType = 0,
		VideoVappType = 1
	};

	public enum class ComposingModeState
	{
		CameraRecording = 0,
		VideoImport = 1,
	};

	public enum class ComposingState
	{
		None = 0,
		Previewing = 1,
		Paused = 2,
		Recording = 3
	};

	public enum class ChromaKeyType
	{
		Previewing = 0,
		Recording = 1
	};
}