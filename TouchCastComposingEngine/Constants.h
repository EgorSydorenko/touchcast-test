#pragma once

namespace TouchCastComposingEngine
{
	namespace Win32BackgroundPipe
	{
		ref class Constant sealed : Platform::Object
		{
		public:
			static Constant^ Instance()
			{
				static Constant^ _instance;
				if (_instance == nullptr)
				{
					_instance = ref new Constant();
				}
				return _instance;
			}
			property Platform::String^ Namespace
			{
				Platform::String^ get()
				{
					return "namespace";
				}
			}
			property Platform::String^ Cmd
			{
				Platform::String^ get()
				{
					return "cmd";
				}
			}

			property Platform::String^ DestinationFile
			{
				Platform::String^ get()
				{
					return "dst_file";
				}
			}
			property Platform::String^ SourceFile
			{
				Platform::String^ get()
				{
					return "src_file";
				}
			}

			property Platform::String^ Width
			{
				Platform::String^ get()
				{
					return "width";
				}
			}

			property Platform::String^ Height
			{
				Platform::String^ get()
				{
					return "height";
				}
			}

			property Platform::String^ ModelFile
			{
				Platform::String^ get()
				{
					return "model_file";
				}
			}

			property Platform::String^ Input
			{
				Platform::String^ get()
				{
					return "input";
				}
			}

			property Platform::String^ Result
			{
				Platform::String^ get()
				{
					return "result";
				}
			}

			property Platform::String^ MicrosoftOffice
			{
				Platform::String^ get()
				{
					return "ms_office";
				}
			}

			property Platform::String^ Prisma
			{
				Platform::String^ get()
				{
					return "prisma";
				}
			}


			property Platform::String^ Personify
			{
				Platform::String^ get()
				{
					return "personify";
				}
			}

			//Cmd values:
			property Platform::String^ ConvertToPdf
			{
				Platform::String^ get()
				{
					return "convert_to_pdf";
				}
			}

			property Platform::String^ Start
			{
				Platform::String^ get()
				{
					return "start";
				}
			}

			property Platform::String^ Process
			{
				Platform::String^ get()
				{
					return "process";
				}
			}

			property Platform::String^ Stop
			{
				Platform::String^ get()
				{
					return "stop";
				}
			}

			//Result values:

			property Platform::String^ Ok
			{
				Platform::String^ get()
				{
					return "ok";
				}
			}

			property Platform::String^ Failed
			{
				Platform::String^ get()
				{
					return "failed";
				}
			}

			property Platform::String^ Output
			{
				Platform::String^ get()
				{
					return "output";
				}
			}

			property Platform::String^ ErrorCode
			{
				Platform::String^ get()
				{
					return "error_code";
				}
			}
		};
	}
}
