﻿#pragma once

#include <collection.h>
#include <mutex>

#include "ComposingTypes.h"
#include "TouchcastChromaKeyEffect.h"
#include "ComposingContextElement.h"
#include "AudioContext.h"
#include "WindowsNumerics.h"
#include "SceneSwapchainWrapper.h"
#include "HardwareInfo.h"
#include "Manager.h"

namespace TouchCastComposingEngine
{
	public delegate void SwapChainReady();
	public ref class TCVideoFrame sealed : Platform::Object
	{
	public:
		TCVideoFrame(Microsoft::Graphics::Canvas::ICanvasBitmap^ videoFrame, Windows::Foundation::TimeSpan time, Windows::Foundation::TimeSpan duration)
		{
			_videoFrame = videoFrame;
			_frameDuration = duration;
			_frameTime = time;
		}

		property Microsoft::Graphics::Canvas::ICanvasBitmap^ VideoFrame
		{
			Microsoft::Graphics::Canvas::ICanvasBitmap^ get()
			{
				return _videoFrame;
			}
		}

		property Windows::Foundation::TimeSpan FrameTime 
		{
			Windows::Foundation::TimeSpan get()
			{
				return _frameTime;
			}
		}

		property Windows::Foundation::TimeSpan FrameDuration
		{
			Windows::Foundation::TimeSpan get()
			{
				return _frameDuration;
			}
		}

	private:
		Microsoft::Graphics::Canvas::ICanvasBitmap^ _videoFrame;
		Windows::Foundation::TimeSpan _frameTime;
		Windows::Foundation::TimeSpan _frameDuration;
	};


	public ref class ComposingContext sealed : Platform::Object
	{
	public:
		
		static ComposingContext^ Shared()
		{
			static ComposingContext^ _sharedInstance = nullptr;

			if (_sharedInstance == nullptr || _sharedInstance->_isNeedContextReset)
			{
				_sharedInstance = ref new ComposingContext();
			}

			return _sharedInstance;
		}

		void ResetContext() 
		{
			_isNeedContextReset = true;
			_chromakeyEffect = nullptr;
			_audioContext = nullptr;
			_previewRenderTarget = nullptr;
			if (_videoImportFrames != nullptr)
			{
				_videoImportFrames->Clear();
			}
			if (_audioContext != nullptr)
			{
				_audioContext->Reset();
			}
			_swapchain = nullptr;
			_elements = nullptr;
			_videoVappBitmap = nullptr;

		}

		property ComposingModeState ComposerState
		{
			ComposingModeState get()
			{
				return _composerState;
			}
			void set(ComposingModeState value)
			{
				_composerState = value;
			}
		}
		property bool IsNeedToUpdatePreview;

		property Windows::Foundation::TimeSpan Time
		{
			void set(Windows::Foundation::TimeSpan value)
			{
				std::lock_guard<std::mutex> lock(_recordingDurationLock);
				_recordingTime = value;
			}
			Windows::Foundation::TimeSpan get() 
			{
				std::lock_guard<std::mutex> lock(_recordingDurationLock);
				_recordingTime.Duration += 1;
				return _recordingTime;
			}
		}
		property Windows::UI::Color FrameBackgroundColor
		{
			Windows::UI::Color get()
			{
				return _frameBackgroundColor;
			}
			void set(Windows::UI::Color value) 
			{
				_frameBackgroundColor = value;
			}
		}
		property bool IsRecording
		{
			bool get()
			{
				return _isRecording;
			}
			void set(bool value)
			{
				_isRecording = value;
			}
		}

		property static Windows::Graphics::Imaging::BitmapSize VideoSize
		{
			Windows::Graphics::Imaging::BitmapSize get() 
			{
				Windows::Graphics::Imaging::BitmapSize bitmapSize;
				bitmapSize.Width = 1280;
				bitmapSize.Height = 720;
				return bitmapSize;
			}
		}

		property static float ThumbnailMultiplicationFactor
		{
			float get()
			{
				return 8.0;
			}
		}

		property static Windows::Graphics::Imaging::BitmapSize ThumbnailSize
		{
			Windows::Graphics::Imaging::BitmapSize get()
			{
				Windows::Graphics::Imaging::BitmapSize bitmapSize;
				bitmapSize.Width = (unsigned int)(ComposingContext::VideoSize.Width / ComposingContext::ThumbnailMultiplicationFactor);
				bitmapSize.Height = (unsigned int)(ComposingContext::VideoSize.Height / ComposingContext::ThumbnailMultiplicationFactor);
				return bitmapSize;
			}
		}

		property Windows::Foundation::Collections::IVector<ComposingContextElement^>^ Elements
		{
			Windows::Foundation::Collections::IVector<ComposingContextElement^>^ get()
			{
				std::lock_guard<std::mutex> lock(_elemetsLock);
				return _elements;
			}

			void set(Windows::Foundation::Collections::IVector<ComposingContextElement^>^ newList)
			{
				std::lock_guard<std::mutex> lock(_elemetsLock);
				_elements = newList;
			}
		}

		property Microsoft::Graphics::Canvas::ICanvasResourceCreator^ PreviewingDevice
		{
			void set(Microsoft::Graphics::Canvas::ICanvasResourceCreator^ device)
			{
				_previewingDevice = device;
				_chromakeyEffect->CanvasDevice = _previewingDevice;
				if (_composerState == ComposingModeState::CameraRecording)
				{
					_swapchain = ref new Microsoft::Graphics::Canvas::CanvasSwapChain(_previewingDevice, (float)ComposingContext::VideoSize.Width, (float)ComposingContext::VideoSize.Height, 96);
					_canvasRenderTarget = ref new Microsoft::Graphics::Canvas::CanvasRenderTarget(_previewingDevice, (float)ComposingContext::ThumbnailSize.Width, (float)ComposingContext::ThumbnailSize.Height, 96);
					if (SwapChainReadyEvent != nullptr)
					{
						SwapChainReadyEvent();
					}
				}
				//UpdateVideoBitmaps();

			}
			Microsoft::Graphics::Canvas::ICanvasResourceCreator^ get()
			{
				return _previewingDevice;
			}
		}

		property Microsoft::Graphics::Canvas::ICanvasRenderTarget^ СompletedStrokesBitmap
		{
			void set(Microsoft::Graphics::Canvas::ICanvasRenderTarget ^ value)
			{
				_completedStrokesBitmap = (Microsoft::Graphics::Canvas::CanvasRenderTarget ^)value;
			}
			Microsoft::Graphics::Canvas::ICanvasRenderTarget ^ get()
			{
				return _completedStrokesBitmap;
			}
		}

		property ComposingState State
		{
			ComposingState get()
			{
				return _state;
			}
			void set(ComposingState value)
			{
				_state = value;
				
			}
		}
		
		property Microsoft::Graphics::Canvas::ICanvasSwapChain^ CanvasSwapChain
		{
			Microsoft::Graphics::Canvas::ICanvasSwapChain^ get()
			{
				return _swapchain;
			}
		}

		property SwapChainReady^ SwapChainReadyEvent;
		property TouchCastComposingEngine::TouchcastChromaKeyEffect^ ChromaKeyEffect 
		{
			TouchCastComposingEngine::TouchcastChromaKeyEffect^ get()
			{
				return _chromakeyEffect;
			}
		}

		property AudioContext^ Audio
		{
			AudioContext^ get()
			{
				return _audioContext;
			}
		}

		property BitmapRotation CameraPreviewRotation;

		property bool CameraPreviewFlippedHorizontally
		{
		    bool get()
		    {
		        return _cameraPreviewFlippedHorizontally;
		    }

		    void set(bool value)
		    {
		        _cameraPreviewFlippedHorizontally = value;
		    }
		}

#pragma region Static functions
		static void DrawBitmap(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds,
			Microsoft::Graphics::Canvas::ICanvasBitmap^ bitmap,
			Windows::Foundation::Rect outputFullRect,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels,
			ContentMode contentMode)
		{
			DrawBitmap(ds, bitmap, bitmap->SizeInPixels, outputFullRect, outputSizeInPixels, contentMode);
		}

		static void DrawShape(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds, 
			Windows::Foundation::Rect outputFullRect, 
			TouchcastShapeElement^ shapeElement,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels)
		{
			switch (shapeElement->ShapeSubType)
			{
			case ComposingShapeSubType::Circle:
				DrawEllipse(ds,	outputFullRect,
					shapeElement->Color, shapeElement->StrokeColor,
					shapeElement->StrokeWidth, outputSizeInPixels);
				break;
			case ComposingShapeSubType::Rectangle:
				DrawRectangle(ds, outputFullRect,
					shapeElement->Color, shapeElement->StrokeColor,
					shapeElement->StrokeWidth, outputSizeInPixels);
				break;
			case ComposingShapeSubType::Line:
				DrawLine(ds, outputFullRect,
					shapeElement->StrokeColor, shapeElement->StrokeWidth,
					outputSizeInPixels);
				break;
			case ComposingShapeSubType::Arrow:
				DrawArrow(ds, outputFullRect,
					shapeElement->StrokeColor, shapeElement->StrokeWidth,
					outputSizeInPixels);
				break;
			}
		}

		static void DrawRectangle(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds,
			Windows::Foundation::Rect outputFullRect,
			Windows::UI::Color color,
			Windows::UI::Color borderColor,
			float thickness,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels)
		{
			auto x1 = outputFullRect.X * outputSizeInPixels.Width;
			auto y1 = outputFullRect.Y * outputSizeInPixels.Height;
			auto x2 = outputFullRect.Width * outputSizeInPixels.Width;
			auto y2 = outputFullRect.Height * outputSizeInPixels.Height;

			ds->FillRectangle(x1, y1, x2, y2, color);
			ds->DrawRectangle(x1, y1, x2, y2, borderColor, thickness * outputSizeInPixels.Width / ComposingContext::VideoSize.Width);
		}

		static void DrawEllipse(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds,
			Windows::Foundation::Rect outputFullRect,
			Windows::UI::Color color,
			Windows::UI::Color borderColor,
			float thickness,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels)
		{
			auto x1 = outputFullRect.X * outputSizeInPixels.Width;
			auto y1 = outputFullRect.Y * outputSizeInPixels.Height;
			auto x2 = outputFullRect.Width * outputSizeInPixels.Width;
			auto y2 = outputFullRect.Height * outputSizeInPixels.Height;

			ds->FillEllipse(x2 + x1 - x2 / 2, y2 + y1 - y2 / 2, x2 / 2, y2 / 2, color);
			ds->DrawEllipse(x2 + x1 - x2 / 2, y2 + y1 - y2 / 2, x2 / 2, y2 / 2, borderColor, thickness * outputSizeInPixels.Width / ComposingContext::VideoSize.Width);
		}

		static void DrawLine(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds,
			Windows::Foundation::Rect outputFullRect,
			Windows::UI::Color color,
			float thickness,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels) 
		{
			auto x1 = outputFullRect.X * outputSizeInPixels.Width;
			auto y1 = outputFullRect.Y * outputSizeInPixels.Height;
			auto x2 = outputFullRect.Width * outputSizeInPixels.Width;
			auto y2 = outputFullRect.Height * outputSizeInPixels.Height;

			ds->DrawLine(
				x1, y1,
				x2, y2,
				color,
				thickness * outputSizeInPixels.Width / ComposingContext::VideoSize.Width
			);
		}

		static void DrawArrow(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds,
			Windows::Foundation::Rect outputFullRect,
			Windows::UI::Color color,
			float thickness,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels)
		{
			using namespace Microsoft::Graphics::Canvas::Geometry;
			const double arrowAngle = 40 * M_PI / 180;
			const double arrowLength = 16.f * (float)outputSizeInPixels.Width / ComposingContext::VideoSize.Width;
			const float startPoint = 1.f * (float)outputSizeInPixels.Width / ComposingContext::VideoSize.Width;

			auto x1 = outputFullRect.X * outputSizeInPixels.Width;
			auto y1 = outputFullRect.Y * outputSizeInPixels.Height;
			auto x2 = outputFullRect.Width * outputSizeInPixels.Width;
			auto y2 = outputFullRect.Height * outputSizeInPixels.Height;
			thickness *= (float)outputSizeInPixels.Width / ComposingContext::VideoSize.Width;

			auto dY = y2 - y1;
			auto dX = x2 - x1;

			auto width = (double)sqrt(pow(dX, 2) + pow(dY, 2));
			auto rotation = (double)-asin(dY / width);
			if (dX < 0)
			{
				if (rotation < 0) rotation = M_PI - rotation;
				else rotation = -M_PI - rotation;
			}

			auto ax1 = (float)(arrowLength * cos(arrowAngle / 2 - rotation));
			auto ay1 = (float)(arrowLength * sin(arrowAngle / 2 - rotation));
			auto ax2 = (float)(arrowLength * cos(-arrowAngle / 2 - rotation));
			auto ay2 = (float)(arrowLength * sin(-arrowAngle / 2 - rotation));

			auto tmp = (thickness / 2) / sin((M_PI - arrowAngle) / 2);
			auto shift = tmp / tan(arrowAngle / 2);
			auto shiftX = (float)(shift * cos(-rotation));
			auto shiftY = (float)(shift * sin(-rotation));

			CanvasPathBuilder^ pathBuilder = ref new CanvasPathBuilder(ds);
			pathBuilder->BeginFigure(startPoint, startPoint);
			pathBuilder->AddLine(ax1, ay1);
			pathBuilder->AddLine(ax2, ay2);
			pathBuilder->EndFigure(CanvasFigureLoop::Closed);
			CanvasGeometry^ triangleGeometry = CanvasGeometry::CreatePath(pathBuilder);

			ds->DrawGeometry(
				triangleGeometry,
				x1 + shiftX, y1 + shiftY,
				color,
				thickness
			);
			ds->DrawLine(
				x1 + shiftX, y1 + shiftY,
				x2, y2,
				color,
				thickness
			);
		}

		static void DrawBitmap(Microsoft::Graphics::Canvas::CanvasDrawingSession^ ds,
			Microsoft::Graphics::Canvas::ICanvasImage^ bitmap,
			Windows::Graphics::Imaging::BitmapSize bitmapSize,
			Windows::Foundation::Rect outputFullRect,
			Windows::Graphics::Imaging::BitmapSize outputSizeInPixels,
			ContentMode contentMode)
		{
			//converting outputFullRect to absolute coordinates
			outputFullRect.X = outputFullRect.X * outputSizeInPixels.Width;
			outputFullRect.Y = outputFullRect.Y * outputSizeInPixels.Height;
			outputFullRect.Width = outputFullRect.Width * outputSizeInPixels.Width;
			outputFullRect.Height = outputFullRect.Height * outputSizeInPixels.Height;

			//defining souce rect, that would help to implement correct content mode
			Windows::Foundation::Rect sourceRect(0.0, 0.0, (float)bitmapSize.Width, (float)bitmapSize.Height);
			Windows::Foundation::Rect outputRect = outputFullRect;

			float outputAspect = (float)outputFullRect.Width / (float)outputFullRect.Height;
			float inputAspect = (float)bitmapSize.Width / bitmapSize.Height;

			switch (contentMode)
			{
			case  TouchCastComposingEngine::ContentMode::ScaleToFill:
				//nothing to do here
				break;

			case TouchCastComposingEngine::ContentMode::ScaleAspectFit:

				//in scale aspect fit mode we draw whole image so source rect is full
				//but output rect is not
				if (inputAspect > outputAspect)
				{
					outputRect.Width = outputFullRect.Width;
					outputRect.Height = outputRect.Width / inputAspect;
				}
				else
				{
					outputRect.Height = outputFullRect.Height;
					outputRect.Width = outputRect.Height * inputAspect;
				}

				outputRect.X = outputFullRect.X + (outputFullRect.Width - outputRect.Width) / 2;
				outputRect.Y = outputFullRect.Y + (outputFullRect.Height - outputRect.Height) / 2;

				break;

			case TouchCastComposingEngine::ContentMode::ScaleAspectFill:

				//in scale aspect fill mode output rect is full
				//but source rect is not full
				if (inputAspect < outputAspect)
				{
					sourceRect.Width = (float)bitmapSize.Width;
					sourceRect.Height = (float)bitmapSize.Width / outputAspect;
				}
				else
				{
					sourceRect.Height = (float)bitmapSize.Height;
					sourceRect.Width = (float)bitmapSize.Height * outputAspect;
				}

				sourceRect.X = (bitmapSize.Width - sourceRect.Width) / 2;
				sourceRect.Y = (bitmapSize.Height - sourceRect.Height) / 2;

				break;
			}

			//drawing bitmap

			ds->DrawImage(bitmap, outputRect, sourceRect);
		}

		Microsoft::Graphics::Canvas::ICanvasBitmap^ GetVideoVappBitmapById(Platform::String^ id)
		{
			std::lock_guard<std::mutex> lock(_videoVappLock);
			if (_videoVappFrames->HasKey(id))
			{
				return _videoVappFrames->Lookup(id);
			}
			return nullptr;
		}

		void AddVideoVappBitmapById(Microsoft::Graphics::Canvas::ICanvasBitmap^ bitmap, Platform::String^ id)
		{
			std::lock_guard<std::mutex> lock(_videoVappLock);
			if (_videoVappFrames->HasKey(id))
			{
				_videoVappFrames->Remove(id);
			}
			_videoVappFrames->Insert(id, bitmap);
		}

		void UpdateVideoBitmaps()
		{
			std::lock_guard<std::mutex> lock(_videoVappLock);
			try 
			{
				auto keys = ref new Platform::Collections::Vector<Platform::String^>();
				for each (auto keyvalue in _videoVappFrames)
				{
					keys->Append(keyvalue->Key);
				}

				for each (auto key in keys)
				{
					if (_videoVappFrames->HasKey(key))
					{
						auto  bitmap = _videoVappFrames->Lookup(key);
						if (!bitmap->Device->Equals(_previewingDevice))
						{
							_videoVappFrames->Remove(key);
							auto value = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromBytes(_previewingDevice,
								bitmap->GetPixelBytes(),
								bitmap->SizeInPixels.Width,
								bitmap->SizeInPixels.Height,
								bitmap->Format);
							_videoVappFrames->Insert(key, value);
						}
					}
				}
			}
			catch (Platform::Exception^ ex)
			{

			}
		}

		void PushNewVideoFrame(Microsoft::Graphics::Canvas::ICanvasBitmap^ videoFrame, Windows::Foundation::TimeSpan time, Windows::Foundation::TimeSpan duration)
		{
			static size_t MaxVideoBuffersize = 15;
			if (!_isRecording || _isSeeking) return;
			std::lock_guard<std::mutex> lock(_videoFramesLock);
			if (_videoImportFrames->Size >= MaxVideoBuffersize)
			{
				unsigned int removedItemIndex = 0;
				
				if (_videoImportFrames->Size > removedItemIndex)
				{
					auto removedTcFrame = _videoImportFrames->GetAt(removedItemIndex);
					for (unsigned int i = 1; i < _videoImportFrames->Size; i++)
					{
						auto frame = _videoImportFrames->GetAt(i);
						if (frame->FrameTime.Duration <= removedTcFrame->FrameTime.Duration)
						{
							removedTcFrame = frame;
							removedItemIndex = i;
							break;
						}
					}
				}
				_videoImportFrames->RemoveAt(removedItemIndex);
			}
			_videoImportFrames->Append(ref new TCVideoFrame(videoFrame, time, duration));
		}
		void LockComposing()
		{
			_composingDeviceLock.lock();
		}

		void UnlockComposing()
		{
			_composingDeviceLock.unlock();
		}

		void UpdateComposingItems(Windows::Foundation::Collections::IVector<ComposingContextElement^>^ itemsToUpdate)
		{
			std::lock_guard<std::mutex>lock(_composingDeviceLock);
			if (_previewingDevice != nullptr)
			{
				for (unsigned int i = 0; i < itemsToUpdate->Size; i++)
				{
					try
					{
						auto updatingObject = itemsToUpdate->GetAt(i);
						auto canvasBitmap = dynamic_cast<Microsoft::Graphics::Canvas::CanvasBitmap^>(updatingObject->Bitmap);
						if (canvasBitmap != nullptr)
						{
							auto value = Microsoft::Graphics::Canvas::CanvasBitmap::CreateFromBytes(_previewingDevice,
								canvasBitmap->GetPixelBytes(),
								canvasBitmap->SizeInPixels.Width,
								canvasBitmap->SizeInPixels.Height,
								canvasBitmap->Format);
							updatingObject->Bitmap = value;
							canvasBitmap = nullptr;
						}
					}
					catch (Platform::Exception^ ex)
					{

					}
				}

				UpdateVideoBitmaps();
				
			}
		}

		void VideoRecordingPaused()
		{
			_isRecording = false;
			_audioContext->VideoPaused();
			TimeRange = LONGLONG_ERROR;
			lastWritedTime = LONGLONG_ERROR;
		}
		void ProcessPreview(Microsoft::Graphics::Canvas::ICanvasImage^ outputBitmap, Windows::Graphics::Imaging::BitmapSize bitmapSize)
		{
			Microsoft::Graphics::Canvas::ICanvasImage^ bitmap = ComposingContext::Shared()->RotatePreview(outputBitmap, bitmapSize);
			if (ComposingContext::Shared()->ChromaKeyEffect->IsChromaKeyEffectReady)
			{
				bitmap = ComposingContext::Shared()->ChromaKeyEffect->ProcessEffect((Microsoft::Graphics::Canvas::ICanvasBitmap ^)bitmap, ChromaKeyType::Previewing);
			}

			DrawCameraFeed(bitmap, bitmapSize);
		}

		void DrawCameraFeed(Microsoft::Graphics::Canvas::ICanvasImage^ cameraPreviewBitmap, Windows::Graphics::Imaging::BitmapSize bitmapSize)
		{
				Windows::Foundation::Rect destinationRect;
				destinationRect.X = 0;
				destinationRect.Y = 0;
				destinationRect.Width = (float)_swapchain->SizeInPixels.Width;
				destinationRect.Height = (float)_swapchain->SizeInPixels.Height;

				float outputAspect = (float)destinationRect.Width / (float)destinationRect.Height;
				float inputAspect = (float)bitmapSize.Width / bitmapSize.Height;

				Windows::Foundation::Rect sourceRect;

				if (inputAspect < outputAspect)
				{
					sourceRect.Width = (float)bitmapSize.Width;
					sourceRect.Height = (float)bitmapSize.Width / outputAspect;
				}
				else
				{
					sourceRect.Height = (float)bitmapSize.Height;
					sourceRect.Width = (float)bitmapSize.Height * outputAspect;
				}

				sourceRect.X = (bitmapSize.Width - sourceRect.Width) / 2;
				sourceRect.Y = (bitmapSize.Height - sourceRect.Height) / 2;
				auto type = cameraPreviewBitmap->GetType();

				DrawToCanvasRenderTarget(cameraPreviewBitmap, destinationRect, sourceRect);

				try
				{
					auto ds = _swapchain->CreateDrawingSession(Windows::UI::Colors::Transparent);
					ds->DrawImage(cameraPreviewBitmap, destinationRect, sourceRect);

					//present frame immediatelly
					//not waiting for v-sync
					_swapchain->Present(0);
				}
				catch (Platform::Exception^ e)
				{
					auto result = e->Message;
				}
		}

		Windows::Foundation::IAsyncAction^ SaveRenderTargetToFile(Windows::Storage::Streams::IRandomAccessStream^ stream)
		{
			Windows::Foundation::IAsyncAction^ result = nullptr;
			std::lock_guard<std::mutex> lock(_canvasRenderTargetLock);
			
			if (_canvasRenderTarget != nullptr)
			{
				//creating new canvas with the new device 
				//to do not interfere with main rendering operaiton
				Microsoft::Graphics::Canvas::CanvasDevice^ device = Microsoft::Graphics::Canvas::CanvasDevice::GetSharedDevice();
				auto tmpTarget = ref new Microsoft::Graphics::Canvas::CanvasRenderTarget(device, (float)ComposingContext::ThumbnailSize.Width, (float)ComposingContext::ThumbnailSize.Height, 96);
				tmpTarget->CopyPixelsFromBitmap(_canvasRenderTarget);

				result = tmpTarget->SaveAsync(stream, Microsoft::Graphics::Canvas::CanvasBitmapFileFormat::Png);
			}
			else
			{
				result = Concurrency::create_async([] {});
			}
			return result;
		}

		Microsoft::Graphics::Canvas::ICanvasBitmap^ PopNearestVideoFrame(Windows::Foundation::TimeSpan time)
		{
			std::lock_guard<std::mutex> lock(_videoFramesLock);
			
			//Platform::String^ s = "_videoImportFrames count: " + _videoImportFrames->Size+"\n";
			//OutputDebugString(s->Data());
			if ((_isSeeking || _videoImportFrames->Size == 0) && _lastVideoFrame != nullptr)
			{
				return _lastVideoFrame->VideoFrame;
			}

			auto startTime = time.Duration - TimeRange;
			auto frame = FindLowestFrameByTime(time, startTime);

			if (frame == nullptr)
			{
				frame = FindBiggerFrameByTime(time, startTime);
			}

			return frame;
		}

		void ClearBuffer()
		{
			std::lock_guard<std::mutex> lock(_videoFramesLock);
			_videoImportFrames->Clear();
			TimeRange = LONGLONG_ERROR;
			lastWritedTime = LONGLONG_ERROR;
		}

		void SeekStarted()
		{
		}

		void SeekCompleted()
		{
		}

		
		static HardwareInfo^ TryGetHardwareInformation()
		{
			auto hardwareInfo = ref new HardwareInfo();
			if (hardwareInfo->TryGetInformation())
			{
				return hardwareInfo;
			}
			return nullptr;
		}

		Microsoft::Graphics::Canvas::ICanvasBitmap^ RotatePreview(Microsoft::Graphics::Canvas::ICanvasImage^ bitmap, Windows::Graphics::Imaging::BitmapSize bitmapSize)
		{
			auto cameraPreviewTransformedSize = CameraPreviewTransformedSize(bitmapSize);
			auto cameraPreviewTransform = CameraPreviewTransformation(bitmapSize);
			if(_previewRenderTarget != nullptr &&
			((_previewRenderTarget->SizeInPixels.Width != cameraPreviewTransformedSize.Width ||
				_previewRenderTarget->SizeInPixels.Height != cameraPreviewTransformedSize.Height) ||
				_previewRenderTarget->Device != _previewingDevice))
			{
				_previewRenderTarget = nullptr;
			}
			if (_previewRenderTarget == nullptr)
			{
				_previewRenderTarget = ref new Microsoft::Graphics::Canvas::CanvasRenderTarget(_previewingDevice, (float)cameraPreviewTransformedSize.Width, (float)cameraPreviewTransformedSize.Height, 96, Windows::Graphics::DirectX::DirectXPixelFormat::B8G8R8A8UIntNormalized,Microsoft::Graphics::Canvas::CanvasAlphaMode::Premultiplied);
			}
			auto size = _previewRenderTarget->SizeInPixels;
			auto renderTargetDrawSession = _previewRenderTarget->CreateDrawingSession();
			renderTargetDrawSession->Clear(Windows::UI::Colors::Transparent);
			renderTargetDrawSession->Transform = cameraPreviewTransform;
			renderTargetDrawSession->DrawImage(bitmap);
			
			return _previewRenderTarget;
		}

#pragma endregion

	private:
		bool _isNeedContextReset = false;
		bool _cameraPreviewFlippedHorizontally;
		std::mutex _composingDeviceLock;
		std::mutex	_elemetsLock;
		std::mutex	_previewElemetsLock;
		std::mutex	_videoFramesLock;
		std::mutex	_videoVappLock;
		std::mutex  _strokesLock;
		std::mutex  _recordingDurationLock;
		std::mutex _canvasRenderTargetLock;
		Windows::Foundation::Collections::IVector<ComposingContextElement^>^	_elements;
		Microsoft::Graphics::Canvas::CanvasRenderTarget^ _previewRenderTarget;
		TouchCastComposingEngine::TouchcastChromaKeyEffect^ _chromakeyEffect;
		Microsoft::Graphics::Canvas::ICanvasResourceCreator^ _previewingDevice;
		Microsoft::Graphics::Canvas::ICanvasBitmap^ _videoImportBitmap = nullptr;
		Microsoft::Graphics::Canvas::ICanvasBitmap^ _videoVappBitmap = nullptr;
		Microsoft::Graphics::Canvas::CanvasSwapChain^	_swapchain;
		Microsoft::Graphics::Canvas::CanvasRenderTarget^ _canvasRenderTarget = nullptr;
		ComposingState _state;
		AudioContext ^ _audioContext;
		Platform::Collections::Vector<TCVideoFrame^ >^ _videoImportFrames;
		Platform::Collections::Map<Platform::String^ , Microsoft::Graphics::Canvas::ICanvasBitmap^>^ _videoVappFrames;
		TCVideoFrame^  _lastVideoFrame;
		bool _isRecording = false;
		bool _isSeeking = false;
		bool _isRenderTargetLocked = false;
		ComposingModeState _composerState;
		Microsoft::Graphics::Canvas::CanvasRenderTarget ^			_completedStrokesBitmap = nullptr;
		Windows::Foundation::TimeSpan _recordingTime;
		Windows::UI::Color _frameBackgroundColor = Windows::UI::Colors::Black;

		long long TimeRange = LONGLONG_ERROR;
		long long lastWritedTime = LONGLONG_ERROR;
		const long deltaTimeRange = 300000;

		ComposingContext()
		{
			_chromakeyEffect = ref new TouchcastChromaKeyEffect();
			_videoImportFrames = ref new Platform::Collections::Vector<TCVideoFrame^ >();
			_videoVappFrames = ref new Platform::Collections::Map<Platform::String^ , Microsoft::Graphics::Canvas::ICanvasBitmap^>();
			_audioContext = ref new AudioContext();
		}

		void DrawToCanvasRenderTarget(Microsoft::Graphics::Canvas::ICanvasImage^ cameraPreviewBitmap, Windows::Foundation::Rect destinationRect, Windows::Foundation::Rect sourceRect)
		{
			std::lock_guard<std::mutex> lock(_canvasRenderTargetLock);

			auto ds = _canvasRenderTarget->CreateDrawingSession();
			ds->Clear(Windows::UI::Colors::Transparent);

			destinationRect.X /= ComposingContext::ThumbnailMultiplicationFactor;
			destinationRect.Y /= ComposingContext::ThumbnailMultiplicationFactor;
			destinationRect.Width /= ComposingContext::ThumbnailMultiplicationFactor;
			destinationRect.Height /= ComposingContext::ThumbnailMultiplicationFactor;

			ds->DrawImage(cameraPreviewBitmap, destinationRect, sourceRect);
			ds->Flush();
			ds = nullptr;

		}
		Windows::Graphics::Imaging::BitmapSize CameraPreviewTransformedSize(Windows::Graphics::Imaging::BitmapSize originalPreviewSize)
		{
			Windows::Graphics::Imaging::BitmapSize outputBitmapSize;
			if (CameraPreviewRotation == BitmapRotation::NoRotation ||
				CameraPreviewRotation == BitmapRotation::Rotation180DegreesCCW)
			{
				outputBitmapSize = originalPreviewSize;
			}
			else
			{
				outputBitmapSize.Width = originalPreviewSize.Height;
				outputBitmapSize.Height = originalPreviewSize.Width;
			}
			return outputBitmapSize;
		}


		Microsoft::Graphics::Canvas::ICanvasBitmap^ FindLowestFrameByTime(Windows::Foundation::TimeSpan time, long long startTime)
		{
			Microsoft::Graphics::Canvas::ICanvasBitmap^ nearestFrame = nullptr;
			auto lowerFrames = ref new Platform::Collections::Vector<TCVideoFrame^ >();
			for (unsigned int i = 0; i < _videoImportFrames->Size; i++)
			{
				auto frame = _videoImportFrames->GetAt(i);
				if (TimeRange == LONGLONG_ERROR)
				{
					TimeRange = time.Duration - frame->FrameTime.Duration;
					nearestFrame = frame->VideoFrame;
					lastWritedTime = frame->FrameTime.Duration;
					return nearestFrame;
				}
				auto val = time.Duration - TimeRange;
				// s = "TimeRange: " + TimeRange +" FrameTime.Duration: " + frame->FrameTime.Duration + " CurrentTime: " +  time.Duration+ " FrameTime: " + val + "\n";
				//OutputDebugString(s->Data());
				if (startTime - deltaTimeRange <= frame->FrameTime.Duration
					|| lastWritedTime >= frame->FrameTime.Duration)
				{
					lowerFrames->Append(frame);
					//break;
				}
			}

			unsigned int maxValueIndex = UINT_ERROR;

			if (lowerFrames->Size > 1)
			{
				for (unsigned int i = 1; i < lowerFrames->Size; i++)
				{
					if (maxValueIndex == UINT_ERROR)
					{
						maxValueIndex = 0;
					}

					auto curentValue = lowerFrames->GetAt(maxValueIndex);
					auto iteratedValue = lowerFrames->GetAt(i);
					if (iteratedValue->FrameTime.Duration > curentValue->FrameTime.Duration)
					{

						maxValueIndex = i;
					}
				}
			}
			else if (lowerFrames->Size != 0)
			{
				maxValueIndex = 0;
			}

			if (maxValueIndex != UINT_ERROR)
			{
				nearestFrame = lowerFrames->GetAt(maxValueIndex)->VideoFrame;
				lastWritedTime = lowerFrames->GetAt(maxValueIndex)->FrameTime.Duration;
				_lastVideoFrame = ref new TCVideoFrame(nearestFrame, lowerFrames->GetAt(maxValueIndex)->FrameTime, lowerFrames->GetAt(maxValueIndex)->FrameDuration);
				/*s = "WritedFrameTime: " + lowerFrames->GetAt(maxValueIndex)->FrameTime.Duration + "\n";
				OutputDebugString(s->Data());*/
			}

			return nearestFrame;
		}

		Microsoft::Graphics::Canvas::ICanvasBitmap^ FindBiggerFrameByTime(Windows::Foundation::TimeSpan time, long long startTime)
		{
			if (_videoImportFrames->Size > 0)
			{
				TCVideoFrame^ minFrame = _videoImportFrames->GetAt(0);
				for (unsigned int i = 1; i < _videoImportFrames->Size; i++)
				{
					auto frame = _videoImportFrames->GetAt(i);
					if (lastWritedTime < frame->FrameTime.Duration && frame->FrameTime.Duration < minFrame->FrameTime.Duration)
					{
						minFrame = frame;

					}
				}
				lastWritedTime = minFrame->FrameTime.Duration;
				_lastVideoFrame = minFrame;
				return minFrame->VideoFrame;
			}
			return nullptr;
		}

		Windows::Foundation::Numerics::float3x2 CameraPreviewTransformation(Windows::Graphics::Imaging::BitmapSize originalPreviewSize)
		{
			auto outputBitmapSize = CameraPreviewTransformedSize(originalPreviewSize);
			auto transformMatrix = Windows::Foundation::Numerics::float3x2::identity();

			if (CameraPreviewFlippedHorizontally)
			{
				transformMatrix = transformMatrix * Windows::Foundation::Numerics::make_float3x2_scale(-1.0f, 1.0f);
				transformMatrix = transformMatrix * Windows::Foundation::Numerics::make_float3x2_translation((float)originalPreviewSize.Width, 0.0f);
			}

			transformMatrix = transformMatrix * Windows::Foundation::Numerics::make_float3x2_rotation((float)((int)CameraPreviewRotation * M_PI / 180.0f),0);
			
			switch (CameraPreviewRotation)
			{
				case TouchCastComposingEngine::BitmapRotation::Rotation90DegreesCCW:
					transformMatrix = transformMatrix * Windows::Foundation::Numerics::make_float3x2_translation((float)outputBitmapSize.Width, 0.0f);
					break;
				case TouchCastComposingEngine::BitmapRotation::Rotation180DegreesCCW:
					transformMatrix = transformMatrix * Windows::Foundation::Numerics::make_float3x2_translation((float)outputBitmapSize.Width, (float)outputBitmapSize.Height);
					break;
				case TouchCastComposingEngine::BitmapRotation::Rotation270DegreesCCW:
					transformMatrix = transformMatrix * Windows::Foundation::Numerics::make_float3x2_translation(0.0f, (float)outputBitmapSize.Height);
					break;
			}

			return transformMatrix;
		}
	};
}