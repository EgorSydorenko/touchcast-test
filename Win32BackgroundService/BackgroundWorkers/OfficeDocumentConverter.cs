﻿using System;
using System.IO;
using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Word = Microsoft.Office.Interop.Word;

namespace Win32BackgroundService.BackgroundWorkers
{
    public class OfficeDocumentConverterResult
    {
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }

        public OfficeDocumentConverterResult(string errorMessage)
        {
            ErrorMessage = errorMessage;
            IsSuccess = String.IsNullOrEmpty(errorMessage);
        }
    }
    public class OfficeDocumentConverter
    {
        private const string DocExtension = ".doc";
        private const string DocxExtension = ".docx";
        private const string XlsxExtension = ".xlsx";
        private const string XlsExtension = ".xls";
        private const string PptExtension = ".ppt";
        private const string PptxExtension = ".pptx";
        public OfficeDocumentConverterResult ConvertDocument(string src, string dst)
        {
            OfficeDocumentConverterResult result = null;
            string extension = Path.GetExtension(src);
            switch (extension)
            {
                case DocExtension:
                case DocxExtension:
                    result = ConvertWordDocumentToPdf(src, dst);
                    break;
                case XlsExtension:
                case XlsxExtension:
                    result = ConvertExcelDocumentToPdf(src, dst);
                    break;
                case PptExtension:
                case PptxExtension:
                    result = ConvertPowerPointDocumentToPdf(src, dst);
                    break;
                default:
                    result = new OfficeDocumentConverterResult($"Unsupported file format {extension}");
                    break;
            }
            return result;
        }
        public OfficeDocumentConverterResult ConvertWordDocumentToPdf(string srcFile, string dstFile)
        {
            string resultMessage = null;

            Word.Application wordApp = null;
            Word.Document wordDoc = null;
            try
            {
                wordApp = new Word.Application
                {
                    DisplayAlerts = Word.WdAlertLevel.wdAlertsNone
                };
                wordDoc = wordApp.Documents.OpenNoRepairDialog(srcFile, true, false, false);
                wordDoc.ExportAsFixedFormat(dstFile, Word.WdExportFormat.wdExportFormatPDF);
            }
            catch (Exception ex) 
            {
                resultMessage = ex.Message;
            }
            finally
            {
                wordDoc?.Close(SaveChanges: false);
                wordApp?.Quit();
            }

            return new OfficeDocumentConverterResult(resultMessage);
        }

        public OfficeDocumentConverterResult ConvertExcelDocumentToPdf(string srcFile, string dstFile)
        {
            string resultMessage = null;
            Excel.Application excelApp = null;
            Excel.Workbook excelWorkBook = null;
            try
            {
                excelApp = new Excel.Application();
                excelWorkBook = excelApp.Workbooks.Open(
                    srcFile,
                    ReadOnly: true,
                    IgnoreReadOnlyRecommended: true,
                    Notify: false
                );
                excelWorkBook.ExportAsFixedFormat(
                    Type: Excel.XlFixedFormatType.xlTypePDF,
                    Filename: dstFile,
                    Quality: Excel.XlFixedFormatQuality.xlQualityStandard,
                    IncludeDocProperties: true,
                    OpenAfterPublish: false,
                    IgnorePrintAreas: true
                );
            }
            catch (Exception ex)
            {
                resultMessage = ex.Message;
            }
            finally
            {
                excelWorkBook?.Close(SaveChanges: false);
                excelApp?.Quit();
            }

            return new OfficeDocumentConverterResult(resultMessage);
        }

        public OfficeDocumentConverterResult ConvertPowerPointDocumentToPdf(string srcFile, string dstFile)
        {
            string resultMessage = null;
            PowerPoint.Application powerPointApp = null;
            PowerPoint.Presentation powerPointPresentation = null;
            try
            {
                powerPointApp = new PowerPoint.Application();
                powerPointPresentation = powerPointApp.Presentations.Open(srcFile,
                    MsoTriState.msoTrue,
                    MsoTriState.msoFalse,
                    MsoTriState.msoFalse);
                powerPointPresentation.ExportAsFixedFormat(dstFile, PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF);
            }
            catch (Exception ex)
            {
                resultMessage = ex.Message;
            }
            finally
            {
                powerPointPresentation?.Close();
                powerPointApp?.Quit();
            }

            return new OfficeDocumentConverterResult(resultMessage);
        }
    }
}
