﻿using System;
using System.Collections.Generic;
using System.Threading;
using Windows.ApplicationModel.AppService;
using Windows.Foundation.Collections;

namespace Win32BackgroundService.BackgroundWorkers
{
    class ServiceThread
    {
        private static readonly string serviceName = "PitchNativeService";

        private static readonly string NamespaceKey = "namespace";
        private static readonly string CommandKey = "cmd";
        private static readonly string DestinationFileKey = "dst_file";
        private static readonly string SourceFileKey = "src_file";
        private static readonly string ResultKey = "result";
        private static readonly string ErrorCodeKey = "error_code";

        //private static readonly string CloseValue = "close";
        private static readonly string MSOfficeValue = "ms_office";
        private static readonly string PrismaValue = "prisma";
        private static readonly string PersonifyValue = "prisma";
        private static readonly string ConvertToPdfValue = "convert_to_pdf";
        private static readonly string SuccessValue = "ok";
        private static readonly string FailedValue = "failed";

        private string _id;
        private readonly Thread _thread;
        private AppServiceConnection _connection = null;
        private readonly OfficeDocumentConverter _documentConverter = new OfficeDocumentConverter();
        private bool _isStopped;

        public delegate void ServiceClosed(ServiceThread sender);
        public event ServiceClosed ServiceClosedEvent;

        public static List<string> AllowedServiceNames = new List<string> { MSOfficeValue, PrismaValue, PersonifyValue };

        public String ID => _id;
        public ServiceThread(string id)
        {
            _id = id;

            _thread = new Thread(ServiceProc)
            {
                IsBackground = true,
            };
        }

        public void Start()
        {
            _thread.Start();
        }
        public void Stop()
        {
            _isStopped = true;
        }
        public void Wait()
        {
            _thread.Join();
            _connection?.Dispose();
        }

        /// <summary>
        /// Creates the app service connection
        /// </summary>
        private void ServiceProc()
        {
            _connection = new AppServiceConnection
            {
                AppServiceName = serviceName,
                PackageFamilyName = Windows.ApplicationModel.Package.Current.Id.FamilyName
            };
            _connection.RequestReceived += Connection_RequestReceived;
            _connection.ServiceClosed += Connection_ServiceClosed;

            using (var task = _connection.OpenAsync().AsTask())
            {
                var status = task.Result;

                Console.ForegroundColor = ConsoleColor.Red;
                switch (status)
                {
                    case AppServiceConnectionStatus.Success:
                        break;
                    case AppServiceConnectionStatus.AppNotInstalled:
                        Console.WriteLine("The app AppServicesProvider is not installed.");
                        return;
                    case AppServiceConnectionStatus.AppUnavailable:
                        Console.WriteLine("The app AppServicesProvider is not available.");
                        return;
                    case AppServiceConnectionStatus.AppServiceUnavailable:
                        Console.WriteLine(string.Format("The app AppServicesProvider is installed but it does not provide the app service {0}.", _connection.AppServiceName));
                        return;
                    case AppServiceConnectionStatus.Unknown:
                        Console.WriteLine(string.Format("An unkown error occurred while we were trying to open an AppServiceConnection."));
                        return;
                }
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Connection established - waiting for requests");
            Console.ResetColor();

            while (!_isStopped)
            {
                Thread.Sleep(100);
            }
        }
        /// <summary>
        /// Receives message from UWP app and sends a response back
        /// </summary>
        private void Connection_RequestReceived(AppServiceConnection sender, AppServiceRequestReceivedEventArgs args)
        {
            ValueSet valueSet = null;

            if (!args.Request.Message.ContainsKey(NamespaceKey) || !args.Request.Message.ContainsKey(CommandKey))
            {
                valueSet = new ValueSet
                {
                    { ResultKey, FailedValue },
                    { ErrorCodeKey, "Not set Namespace or Command" }
                };
            }
            else
            {
                var command = args.Request.Message[CommandKey];

                if (args.Request.Message[NamespaceKey].Equals(MSOfficeValue))
                {
                    var source = String.Empty;
                    var destination = String.Empty;

                    foreach (var item in args.Request.Message)
                    {
                        if (item.Key.Equals(SourceFileKey))
                        {
                            source = item.Value as string;
                        }
                        else if (item.Key.Equals(DestinationFileKey))
                        {
                            destination = item.Value as string;
                        }
                    }

                    if (!String.IsNullOrEmpty(source) && !String.IsNullOrEmpty(destination) && command.Equals(ConvertToPdfValue))
                    {
                        OfficeDocumentConverterResult result = null;
                        try
                        {
                            Console.WriteLine($"OFFICE CONVERT\tSRC:{source} DST:{destination}");
                            result = _documentConverter.ConvertDocument(source, destination);
                        }
                        catch (Exception ex)
                        {
                            result = new OfficeDocumentConverterResult(ex.Message);
                        }

                        if (result.IsSuccess)
                        {
                            valueSet = new ValueSet
                            {
                                { ResultKey, SuccessValue }
                            };
                        }
                        else
                        {
                            valueSet = new ValueSet
                            {
                                { ResultKey, FailedValue },
                                { ErrorCodeKey, result.ErrorMessage }
                            };
                        }
                    }
                    else
                    {
                        valueSet = new ValueSet
                        {
                            { ResultKey, FailedValue },
                            { ErrorCodeKey, "Wrong Command or parameters" }
                        };
                    }
                }
                else if (args.Request.Message[NamespaceKey].Equals(PrismaValue))
                {
                    valueSet = new ValueSet
                    {
                        { ResultKey, SuccessValue }
                    };
                }
                else
                {
                    valueSet = new ValueSet
                    {
                        { ResultKey, FailedValue },
                        { ErrorCodeKey, "Wrong Namespace" }
                    };
                }
            }
            if (valueSet != null && valueSet.ContainsKey(ErrorCodeKey))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(valueSet[ErrorCodeKey]);
                Console.ResetColor();
            }
            args.Request.SendResponseAsync(valueSet).Completed += delegate { };
        }
        private void Connection_ServiceClosed(AppServiceConnection sender, AppServiceClosedEventArgs args)
        {
            ServiceClosedEvent?.Invoke(this);
        }
    }
}
