﻿using System;
using Win32BackgroundService.BackgroundWorkers;

namespace Win32BackgroundService
{
    class Program
    {
        static void Main(string[] args)
        {
            string serviceId = null;
            foreach(var item in args)
            {
                if (ServiceThread.AllowedServiceNames.Contains(item))
                {
                    serviceId = item;
                    break;
                }
            }
            if (string.IsNullOrEmpty(serviceId))
            {
                return;
            }
            Console.WriteLine("Starting " + serviceId);
            var appServiceThread = new ServiceThread(serviceId);
            appServiceThread.ServiceClosedEvent += AppServiceThread_ServiceClosedEvent;
            appServiceThread.Start();

            appServiceThread.Wait();
        }

        private static void AppServiceThread_ServiceClosedEvent(ServiceThread sender)
        {
            sender.ServiceClosedEvent -= AppServiceThread_ServiceClosedEvent;
            sender.Stop();
        }
    }
}
