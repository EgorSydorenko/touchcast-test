﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TfSdk.Data;
using TfSdk.Data.Fabric.Models;
using TfSdk.UI;
using Windows.Storage;

namespace TfSdk
{
    public enum IUploaderState
    {
        Initialized,
        Uploading,
        Canceled,
        Failed,
        Completed
    }
    
    public class ApiClient
    {
        public enum State
        {
            NotAuthorized,
            Authorizing,
            Authorized,
            Failed
        }

        #region Constants
        internal const string JsonMimeType = "application/json";
        internal const string TctMimeType = "application/vnd.touchcast.tct";
        internal const string TcpMimeType = "application/vnd.touchcast.tcp";
        internal const string AuthHeaderKey = "Authorization";
        internal const string AuthHeaderValuePrefix = "Bearer ";
        private const string SettingsUserInfoKey = "UserInfo";
        private const string UserInfoFilename = "fabricuserinfo.txt";
        private const string GetListRequestPlaceHolder = "files/list/get?group={0}";
        private const string EmbedCodePattern = "<iframe width='640' height='404' src='{0}' frameborder='0' allowfullscreen> </iframe>";
        private const int ExpirationDays = 30;
        #endregion

        #region Event
        public delegate void ApiClientChanged();
        public event ApiClientChanged ApiClientStateChangedEvent;
        #endregion

        #region Static
        private static ApiClient _instance;
        public static ApiClient Shared => _instance;

        static ApiClient()
        {
            _instance = new ApiClient();
        }
        #endregion

        #region Private
        private State _condition;
        private ApiClientConfiguration _configuration;
        private SignInDialog _signInDialog;
        private SignInControl _signInControl;
        private UserInfo _userInfo;
        //private string _error;
        private HttpClient _httpClient;
        private CancellationTokenSource _cancellationTokenSource;
        private ApiClient()
        {
            _userInfo = new UserInfo();
            _httpClient = new HttpClient();
            _condition = State.NotAuthorized;
        }

        private async Task SignInDialog_SignInCompletedEvent(string token, string groupUniquePath, string error)
        {
            _signInControl.SignInCompletedEvent -= SignInDialog_SignInCompletedEvent;
            if (_signInDialog != null)
            {
                _signInDialog.SignInCanceledEvent -= SignInDialog_SignInCanceledEvent;
                _signInDialog.Hide();
            }
            _userInfo.token = token;
            _userInfo.groupUniquePath = groupUniquePath;
            await UpdateUserData();
        }

        private void SignInDialog_SignInCanceledEvent()
        {
            _signInDialog.SignInCanceledEvent -= SignInDialog_SignInCanceledEvent;
            _signInControl.SignInCompletedEvent -= SignInDialog_SignInCompletedEvent;
            _signInDialog.Hide();
            FailedCondition();
        }

        private async Task<bool> SaveToApplicationLocalSettings()
        {
            if (_userInfo == null)
            {
                return false;
            }
            try
            {
                _userInfo.expirationDate = DateTime.Now;
                var localFolder = ApplicationData.Current.LocalFolder;
                var json = JsonConvert.SerializeObject(_userInfo);
                var encryptedUserInfo = Cipher.Encryption(json);
                var settingsFile = await localFolder.CreateFileAsync(UserInfoFilename, CreationCollisionOption.ReplaceExisting);
                if (settingsFile != null)
                {
                    await FileIO.WriteTextAsync(settingsFile, encryptedUserInfo);
                    return true;
                }
            }
            catch { }
            
            return false;
        }

        private async Task ClearDataFromApplicationLocalSettings()
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            IStorageItem settingsFile = await localFolder.TryGetItemAsync(UserInfoFilename);

            if (settingsFile != null && settingsFile.IsOfType(StorageItemTypes.File))
            {
                await settingsFile.DeleteAsync();
            }
        }

        private async Task<bool> TryGetDataFromApplicationLocalSettings()
        {
            //Check apiendpoint
            try
            {
                var localFolder = ApplicationData.Current.LocalFolder;
                var settingsFile = await localFolder.GetFileAsync(UserInfoFilename);
                var encryptedJson = await FileIO.ReadTextAsync(settingsFile);
                var json = Cipher.Decryption(encryptedJson);
                _userInfo = JsonConvert.DeserializeObject<UserInfo>(json);
                if (String.IsNullOrEmpty(_userInfo.token)
                    || _configuration.ApiEndPointUrl != _userInfo.apiEndpointUrl
                    || _configuration.LoginEndPointUrl != _userInfo.loginEndpointUrl
                    || _userInfo?.info?.data?.user_id == null)
                {
                    await ClearDataFromApplicationLocalSettings();
                    return false;
                }
                if(_userInfo.organization == null || _userInfo.group == null)
                {
                    FigureoutCurrentUserGroupAndOrganization();
                }
                return true;
            }
            catch
            {

            }
           
            return false;
        }

        private object ConvertJsonToFileList(string json)
        {
            object result = null;
            string open = System.Text.RegularExpressions.Regex.Escape("{");
            string close = System.Text.RegularExpressions.Regex.Escape("}");
            System.Text.RegularExpressions.Regex permissionRegex = new System.Text.RegularExpressions.Regex($"\"permissions\":{open}.*{close}");
            if (permissionRegex.IsMatch(json))
            {
                result = JsonConvert.DeserializeObject<FilesListEx>(json);
            }
            else
            {
                result = JsonConvert.DeserializeObject<FilesList>(json);
            }

            return result;
        }
        private void FigureoutCurrentUserGroupAndOrganization()
        {
            try
            {
                if (_userInfo.info.groups != null && _userInfo.organizations != null)
                {
                    var currentGroup = _userInfo.info.groups.FirstOrDefault(x => x.path == _userInfo.groupUniquePath);
                    if (currentGroup != null)
                    {
                        _userInfo.group = currentGroup;
                        var currentOrganization = _userInfo.organizations.FirstOrDefault(o => o.organization_id == currentGroup.organization_id);
                        if (currentOrganization != null)
                        {
                            _userInfo.organization = currentOrganization;
                        }
                    }
                }
            }
            catch
            {
                _userInfo.organization = null;
            }
        }
        #region Requests
        private async Task<object> ValidateAuthenticationTokenRequest(string token)
        {
            try
            {
                var str = $"{{\"token\":\"{token}\"}}";
                var content = new StringContent(str, Encoding.UTF8, JsonMimeType);
              
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                    new Uri($"{_configuration.ApiEndPointUrl}user/validate-auth-token"))
                {
                    Content = content
                };
                var response = await HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);
                var json = await response.Content.ReadAsStringAsync();
                object responceData = null;
                try
                {
                    responceData = JsonConvert.DeserializeObject<AuthValidationResponse>(json);
                }
                catch
                {

                }
                if(responceData == null)
                {
                    try
                    {
                        responceData = JsonConvert.DeserializeObject<Error>(json);
                    }
                    catch
                    {
                        responceData = null;
                    }
                }
                return responceData;
            }
            catch (OperationCanceledException)
            {
                return "Cancel";
            }
            catch (Exception)
            {
               
            }
            return null;
        }

        private async Task<object> ListJoinedGroupsRequest()
        {
            try
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get,
                    new Uri($"{_configuration.ApiEndPointUrl}group/list/joined"));
                requestMessage.Headers.Add(AuthHeaderKey, AuthHeaderValuePrefix + _userInfo.token);
                var response = await HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Group>>(json);
                }
                else if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    await SignOut();
                }
            }
            catch (OperationCanceledException)
            {
                
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        private async Task<object> GetExtraTokensRequest()
        {
            try
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                   new Uri($"{_configuration.ApiEndPointUrl}user/get-extra-tokens"));
                requestMessage.Headers.Add(AuthHeaderKey, AuthHeaderValuePrefix + _userInfo.token);
                var response = await HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<ExtraAuthToken>>(json);
                }
                else if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    await SignOut();
                }
            }
            catch(Exception)
            {
                return null;
            }
            return null;
        }

        private async Task<object> ListJoinedOrganizationRequest()
        {
            try
            {
                if (_userInfo.info.groups == null)
                {
                    return null;
                }
                OrganizationPayload groupIds = new OrganizationPayload()
                {
                    ids = (from g in _userInfo.info.groups select g.organization_id).ToArray(),
                };
                var jsonPayload = JsonConvert.SerializeObject(groupIds);
                var payloadContent = new StringContent(jsonPayload, Encoding.UTF8, JsonMimeType);

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                    new Uri($"{_configuration.ApiEndPointUrl}organization/get"))
                {
                    Content = payloadContent,
                };
                requestMessage.Headers.Add(AuthHeaderKey, AuthHeaderValuePrefix + _userInfo.token);

                var response = await HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Organization>>(json);
                }
                else if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    await SignOut();
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception)
            {

            }

            return null;
        }

        private async Task<object> FileGetRequest(string fileId, string groupId)
        {
            try
            {
                //Request
                Payload payload = new Payload
                {
                    ids = new List<string>() { fileId },
                    limit = 1,
                    offset = 0,
                    sort = SortOption.created,
                    sort_directions = SortDirection.asc
                };

                var jsonPayload = JsonConvert.SerializeObject(payload);
                var payloadContent = new StringContent(jsonPayload, Encoding.UTF8, JsonMimeType);
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                    new Uri($"{_configuration.ApiEndPointUrl}{String.Format(GetListRequestPlaceHolder, groupId)}"))
                {
                    Content = payloadContent,
                };
                requestMessage.Headers.Add(AuthHeaderKey, AuthHeaderValuePrefix + _userInfo.token);

                //Response
                var response = await HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var filesList = ConvertJsonToFileList(json);
                    if (filesList is FilesList)
                    {
                        return (filesList as FilesList).files.FirstOrDefault();
                    }
                    else if (filesList is FilesListEx)
                    {
                        return (filesList as FilesListEx).files.FirstOrDefault();
                    }
                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    await SignOut();
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception)
            {

            }

            return null;
        }
        #endregion

        #endregion

        #region Internal
        internal ApiClientConfiguration Configuration => _configuration;
        internal HttpClient HttpClient => _httpClient;
        internal void ExpireSession(Error error)
        {
            FailedCondition();
            //save error
        }
        internal void UpdateUIControl(SignInControl control)
        {
            if (_signInControl != null)
            {
                _signInControl.SignInCompletedEvent -= SignInDialog_SignInCompletedEvent;
            }
            _signInControl = control;
            _signInControl.SignInCompletedEvent += SignInDialog_SignInCompletedEvent;
        }
        internal void FailedCondition()
        {
            Condition = State.Failed;
        }
        internal void AuthorizingCondition()
        {
            Condition = State.Authorizing;
        }
        #endregion

        #region  Public
        public UserInfo UserInfo => _userInfo;
        public State Condition
        {
            private set
            {
                _condition = value;
                ApiClientStateChangedEvent?.Invoke();
            }
            get => _condition;
        }
        public async Task SignIn(ApiClientConfiguration configuration, bool canBeClosed = true)
        {
            if (Condition != State.NotAuthorized)
            {
                return;
            }
            _configuration = configuration;

            _signInDialog = new SignInDialog(_configuration.LoginEndPointUrl, _configuration.RedirectUrl, canBeClosed);
            _signInDialog.SignInCanceledEvent += SignInDialog_SignInCanceledEvent;
            AuthorizingCondition();
            await _signInDialog.ShowAsync();
        }

        //Set State Failed only if session is not valid
        public async Task UpdateUserData()
        {
            if (Condition == State.NotAuthorized || Condition == State.Failed)
            {
                return;
            }
            bool isNeedToUpdateLocalSettings = true;
            _cancellationTokenSource = new CancellationTokenSource();
            var validationResponseObject = await ValidateAuthenticationTokenRequest(_userInfo.token);
            var checkExpirationDate = new Action(() =>
            {
                if ((_userInfo?.expirationDate != null && (DateTime.Now - _userInfo.expirationDate).Days < ExpirationDays))
                {
                    Condition = State.Authorized;
                    isNeedToUpdateLocalSettings = false;
                }
                else
                {
                    _userInfo.token = null;
                    FailedCondition();
                }
            });
            if (validationResponseObject is AuthValidationResponse)
            {
                _userInfo.info = validationResponseObject as AuthValidationResponse;
                if (_userInfo.info.valid)
                {
                    try
                    {
                        if (_userInfo.info.auth_info.provider == AuthInfo.KalturaProvider)
                        {
                            var kalturaData = await GetExtraTokensRequest();
                            if (kalturaData is List<ExtraAuthToken>)
                            {
                                _userInfo.kalturaTokenInfo = (kalturaData as List<ExtraAuthToken>).FirstOrDefault();
                            }
                            //Send special request for kaltura users
                        }

                        var groupsObject = await ListJoinedGroupsRequest();
                        if (groupsObject is List<Group>)
                        {
                            _userInfo.info.groups = groupsObject as List<Group>;
                        }

                        var organizationObject = await ListJoinedOrganizationRequest();
                        if (organizationObject is List<Organization>)
                        {
                            _userInfo.organizations = organizationObject as List<Organization>;
                        }

                        FigureoutCurrentUserGroupAndOrganization();

                        Condition = State.Authorized;
                    }
                    catch (Exception)
                    {
                        Condition = State.Authorized;
                    }
                }
                else
                {
                    FailedCondition();
                }
            }
            else if (validationResponseObject is Error errorData)
            {
                if (errorData.code == ErrorCode.ERROR_CODE_EXPIRED ||
                           errorData.code == ErrorCode.ERROR_ACCESS_TOKEN_NOT_FOUND)
                {
                    _userInfo.token = null;
                    FailedCondition();
                }
                else
                {
                    checkExpirationDate();
                }
            }
            else
            {
                checkExpirationDate();
            }

            if (Condition == State.Authorized && isNeedToUpdateLocalSettings)
            {
                _userInfo.apiEndpointUrl = _configuration.ApiEndPointUrl;
                _userInfo.loginEndpointUrl = _configuration.LoginEndPointUrl;
                _userInfo.redirectEndpointUrl = _configuration.RedirectUrl;

                await ClearDataFromApplicationLocalSettings();
                await SaveToApplicationLocalSettings();
            }
        }

        public async Task RestoreSavedSession(ApiClientConfiguration configuration)
        {
            if (Condition != State.NotAuthorized)
            {
                return;
            }

            AuthorizingCondition();
            _configuration = configuration;
            if (await TryGetDataFromApplicationLocalSettings() == true)
            {
                await UpdateUserData();
                return;
            }
            FailedCondition();
        }

        public async Task SignOut()
        {
            _userInfo = new UserInfo();
            _cancellationTokenSource?.Cancel();
            CloudUpload.UI.Helper.HttpHelper.ClearCookies(_configuration.LoginEndPointUrl);
            await CloudUpload.UI.Helper.HttpHelper.ClearCache();
            await ClearDataFromApplicationLocalSettings();
            Condition = State.NotAuthorized;
        }

        public IUploader CreateUploader()
        {
            if(_userInfo.kalturaTokenInfo == null)
            {
                return new Data.Fabric.FabricUploader(this);
            }
            else
            {
                return new Data.Kaltura.KalturaUploader(this);
            }
        }

        public IUploader CreateFabricUploader()
        {
            return new Data.Fabric.FabricUploader(this);
        }

        public IUploader CreateKalturaUploader()
        {
            return new Data.Kaltura.KalturaUploader(this);
        }

        public async Task SharingFileDialog(string shareEndPointUrl, string fileId, string groupId)
        {
            var shareDialog = new SharingDialog(shareEndPointUrl, fileId, groupId);
            await shareDialog.ShowAsync();
        }

        public async Task ViewUserProfileDialog(string viewProfileEndpoint)
        {
            var viewProfileDialog = new UserProfileDialog(viewProfileEndpoint);
            await viewProfileDialog.ShowAsync();
        }

        public async Task<FileShareResponse> GetFileSharedState(string fileId, string groupId)
        {
            FileShareResponse result = new FileShareResponse();

            var file = await FileGetRequest(fileId, groupId);
            if (file is File)
            {
                File currentFile = file as File;
                if (currentFile.permissions == FilePermission.anyone
                    && currentFile.external_permissions.security == FilePermissionSecurity.private_link
                    && currentFile.external_permissions.url != null)
                {
                    result.State = SharedState.Link;
                    result.Url = currentFile.external_permissions.url;
                    result.EmbedCode = String.Format(EmbedCodePattern, currentFile.external_permissions.url);
                }
                else if (currentFile.permissions == FilePermission.anyone
                         && currentFile.external_permissions.security == FilePermissionSecurity.private_link)
                {
                    result.State = SharedState.Group;
                    result.Url = String.Empty;
                    Group group = UserInfo.info.groups.Where(g => g.path == groupId).FirstOrDefault();
                    Organization organization = UserInfo.organizations.Where(o => o.organization_id == group.organization_id).FirstOrDefault();
                    if (group != null && organization != null)
                    {
                        result.Url = $"https://{organization.default_custom_domain}/g/{group.organization_path}/video/{fileId}";
                    }
                }
                else if (currentFile.permissions == FilePermission.me
                         && currentFile.external_permissions.security == FilePermissionSecurity.private_link)
                {
                    result.State = SharedState.OnlyMe;
                }
                else if (currentFile.permissions == FilePermission.me
                         && currentFile.external_permissions.security == FilePermissionSecurity.password)
                {
                    result.State = SharedState.Password;
                    result.Url = currentFile.external_permissions.url;
                }
            }
            else if (file is FileEx)
            {
                FileEx currentFile = file as FileEx;
                result.State = SharedState.SpecificPeople;
                result.Url = currentFile.external_permissions.url;
            }

            return result;
        }
        #endregion
    }
}
