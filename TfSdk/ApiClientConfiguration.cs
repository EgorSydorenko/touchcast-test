﻿using System;

namespace TfSdk
{
    public class ApplicationInfo
    {
        private string _appId;
        private string _deviceName;
        private string _product;
        private string _appVersion;

        public string AppId => _appId;
        public string DeviceName => _deviceName;
        public string Product => _product;
        public string AppVersion => _appVersion;

        public ApplicationInfo(string appId, string deviceName, string product, string appVersion)
        {
            _appId = appId;
            _deviceName = deviceName;
            _product = product;
            _appVersion = appVersion;
        }
    }
    public class ApiClientConfiguration
    {
        private Uri _loginEndPointUrl;
        private Uri _apiEndPointUrl;
        private Uri _redirectUrl;
        private Uri _viewProfileUrl;
        private ApplicationInfo _appInfo;
        public ApiClientConfiguration(string loginEndPoint, string apiEndPoint, string redirectEndpoint, string viewProfileEndpoint, ApplicationInfo appInfo)
        {
            _appInfo = appInfo;
            _loginEndPointUrl = new Uri(String.Format(loginEndPoint, redirectEndpoint, _appInfo.Product, _appInfo.AppId, _appInfo.DeviceName, _appInfo.AppVersion));
            _apiEndPointUrl = new Uri(apiEndPoint);
            _redirectUrl = new Uri(redirectEndpoint);
            _viewProfileUrl = new Uri(String.Format(viewProfileEndpoint, _appInfo.Product, _appInfo.AppId, _appInfo.DeviceName, _appInfo.AppVersion));
        }

        public Uri LoginEndPointUrl => _loginEndPointUrl;
        public Uri ApiEndPointUrl => _apiEndPointUrl;
        public Uri RedirectUrl => _redirectUrl;
        public Uri ViewProfileUrl => _viewProfileUrl;
        public ApplicationInfo AppInfo => _appInfo;
    }
}
