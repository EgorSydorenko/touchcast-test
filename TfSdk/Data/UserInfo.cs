﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabric.Data
{
    public class UserInfo
    {
        public string token { set; get; }
        public Uri loginEndpointUrl { set; get; }
        public Uri apiEndpointUrl { set; get; }
        public Uri redirectEndpointUrl { set; get; }
        public ExtraAuthToken kalturaTokenInfo { set; get; }
        public AuthValidationResponse info { set; get; }
        public List<Organization> organizations { get; set; }
    }
}
