﻿using System;
using System.Collections.Generic;
using Windows.Storage;

namespace TfSdk.Data
{
    public interface ICompressedTouchcastInfo
    {
        StorageFile TctFile { get; }
        string Title { get; }
        string Description { get; }
        string Group { get; }
    }

    public interface IUncompressedTouchcastInfo
    {
        StorageFile VideoFile { get; }
        string Title { get; }
        string Description { get; }
        StorageFile VideoThumbnailFile {  get; }

        void UpdateCommandByRecourceId(string id, string thumb, string url = "");
        void UpdateCommandByUrl(string fileUrl, string url = "");
        string ActionsXmlToString();
        System.Threading.Tasks.Task<List<Tuple<string, StorageFile>>> PrepareUploadingFiles();
    }

    public class TouchcastInfo
    {
        public ICompressedTouchcastInfo CompressedInfo { set; get; }
        public IUncompressedTouchcastInfo UncompressedInfo { set; get; }
    }
}
