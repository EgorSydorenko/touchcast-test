﻿using System.Collections.Generic;

namespace TfSdk.Data.Fabric.Models
{
    public class AwsS3Credentials
    {
        public string key { set; get; }
        public string secret { set; get; }
        public string session_token { set; get; }
        public string region { set; get; }
    }
    public class AuthInfo
    {
        public const string KalturaProvider = "kaltura";
        public string type { set; get; }
        public string provider { set; get; }
    }
    public class KalturaProfileData
    {
        public string profile_id { set; get; }
        public string endpoint { set; get; }
        public string partner_id { set; get; }
    }
    public class KalturaExtraInfo
    {
        public string profile_id { set; get; }
        public string endpoint { set; get; }
        public string partner_id { set; get; }
    }
    public class ExtraAuthToken
    {
        public string idp { set; get; }
        public string token { set; get; }
        public int? expires { set; get; }
        public KalturaExtraInfo extra { set; get; }
    }
    public class FileEdit
    {
        public string file_id{ set; get; }
        public string name { set; get; }
        public string description { set; get; }
    }
    public class FileUploadStartResponse
    {
        public string upload_type { set; get; }
        public string file_id { set; get; }
        public string upload_id { set; get; }
        public string bucket { set; get; }
        public string prefix { set; get; }
        public AwsS3Credentials aws { set; get; }
    }
    public class FileUploadFinishResponse
    {
        public List<string> files { set; get; }
    }
    public class FileUploadFinishRequestPayload
    {
        public string file_id { set; get; }
        public List<FileMetadata> files { set; get; }
    }
    public class FileMetadata
    {
        public string name { set; get; }
        public string path { set; get; }
        public string original_filename { set; get; }
        public string permissions { set; get; }
        public string description { set; get; }
        public int? video_player_settings_id { set; get; }
    }
    public class FileFilteringOptions
    {
        public FileType type { set; get; }
        public List<string> owner { set; get; }
        public List<string> ids { set; get; }
        public List<string> exclude_ids { set; get; }
        public string search { set; get; }
        public bool exclude_vapps { set; get; }
    }
    public class Payload
    {
        public List<string> ids { set; get; }
        public SortOption sort { set; get; }
        public SortDirection sort_directions { set; get; }
        public int? offset { set; get; }
        public int? limit { set; get; }
        public FileFilteringOptions filter { set; get; }
    }
    public class FilesList
    {
        public List<File> files { get; set; }
        public int? total_files { set; get; }
        public int? total_pages { set; get; }
    }
    public class FilesListEx
    {
        public List<FileEx> files;
        public int? total_files { set; get; }
        public int? total_pages { set; get; }
    }
    public class File
    {
        public string file_id { set; get; }
        public FileType type { set; get; }
        public FilePreviewStatus preview { set; get; }
        public string name { set; get; }
        public string description { set; get; }
        public string filename { set; get; }
        public string original_filename { set; get; }
        public string user_id { set; get; }
        public int? created { set; get; }
        public int? comments { set; get; }
        public int? views { set; get; }
        public int? downloads { set; get; }
        public int? filesize { set; get; }
        public FilePermission permissions { set; get; }
        public FileExternalPermissions external_permissions { set; get; }
        public FileStatus status { set; get; }
        public bool? started { set; get; }
    }
    public class FileEx
    {
        public string file_id { set; get; }
        public FileType type { set; get; }
        public FilePreviewStatus preview { set; get; }
        public string name { set; get; }
        public string description { set; get; }
        public string filename { set; get; }
        public string original_filename { set; get; }
        public string user_id { set; get; }
        public int? created { set; get; }
        public int? comments { set; get; }
        public int? views { set; get; }
        public int? downloads { set; get; }
        public int? filesize { set; get; }
        public FilePermissionEx permissions { set; get; }
        public FileExternalPermissions external_permissions { set; get; }
        public FileStatus status { set; get; }
        public bool? started { set; get; }
    }
    public class FilePermissionEx
    {
        public List<string> topic_ids { set; get; }
        public List<string> user_ids { set; get; }
        public List<string> gallery_ids { set; get; }
    }
    public class FileExternalPermissions
    {
        public string hash { set; get; }
        public FilePermissionSecurity security { set; get; }
        public string email { set; get; }
        public string url { set; get; }
        public string analytics_collect_email { set; get; }
        public string file_id { set; get; }
        public string group { set; get; }
        public int? views { set; get; }
        public List<string> security_email_list { set; get; }
        public List<string> security_domain_list { set; get; }
        //public Dictionary<string,string> video_player_settings { set; get; }
        public FileStatus status { set; get; }
    }
    public class FilesFilteringOptions
    {
        public FileType type { set; get; }
        public List<string> owner { set; get; }
        public List<string> ids { set; get; }
        public List<int> exclude_ids { set; get; }
        public string search { set; get; }
        public bool? exclude_vapps { set; get; }
    }
    public class Share
    {
        public string file_id { set; get; }
        /// <summary>
        /// Use Values from ExternaPermissionType
        ///         public const string Me = "me";
        ///         public const string SpecificPeople = "specific_people";
        ///         public const string AnyoneInsideOrganization = "anyone_inside_organization";
        ///         public const string PublicWithPassword = "public_with_password";
        ///         public const string Public = "public";
        /// </summary>
        public string permissions { set; get; }
        public List<string> ids { set; get; }
        public List<string> emails { set; get; }
        public List<string> email_domains { set; get; }
        public string password { set; get; }
    }
    public class AuthValidationResponse
    {
        public bool valid { set; get; }
        public int? expires { set; get; }
        public AuthInfo auth_info { set; get; }
        public string user_id { set; get; }
        public bool? fabric_user { set; get; }
        public User data { set; get; }
        public List<Group> groups { set; get; }
    }
    public class ExternaPermissionType
    {
        public const string Me = "me";
        public const string SpecificPeople = "specific_people";
        public const string AnyoneInsideOrganization = "anyone_inside_organization";
        public const string PublicWithPassword = "public_with_password";
        public const string Public = "public";
    }
    public class FileUploadedResponse
    {
        public List<string> file_ids { set; get; }
    }
    public class FileShareResponse
    {
       public SharedState State { get; set; }
       public string Url { get; set; }
       public string EmbedCode { get; set; }
    }
}
