﻿namespace TfSdk.Data.Fabric.Models
{
    public class Group
    {
        public string organization_id { set; get; }
        public string group_id { set; get; }
        public string name { set; get; }
        public string path { set; get; }
        public string organization_path { set; get; }
        public OrganizationOrGroupStatus status { set; get; }
        public int created { set; get; }
        public int total_members { set; get; }
        public UserJoinMode user_join_mode { set; get; }
        public UsernameDisplay username_display { set; get; }
        public InviteNewUser invite_new_user { set; get; }
        public NewTopic new_topic { set; get; }
        public string owner_id { set; get; }
        public int? video_player_settings_id { set; get; }
    }
}
