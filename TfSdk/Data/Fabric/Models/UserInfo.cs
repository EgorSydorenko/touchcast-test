﻿using System;
using System.Collections.Generic;

namespace TfSdk.Data.Fabric.Models
{
    public class UserInfo
    {
        public string token { set; get; }
        public string groupUniquePath { set; get; }
        public Uri loginEndpointUrl { set; get; }
        public Uri apiEndpointUrl { set; get; }
        public Uri redirectEndpointUrl { set; get; }
        public AuthValidationResponse info { set; get; }
        public List<Organization> organizations { get; set; }
        public ExtraAuthToken kalturaTokenInfo { set; get; }
        public Organization organization { set; get; }
        public Group group { set; get; }
        public DateTime expirationDate { set; get; }
        public bool IsAccentureUser
        {
            get
            {
                return ApiClient.Shared.UserInfo.kalturaTokenInfo != null
                    && ApiClient.Shared.UserInfo.kalturaTokenInfo.extra.endpoint != null
                    && ApiClient.Shared.UserInfo.kalturaTokenInfo.extra.endpoint.Contains("mediaexchange");
            }
        }

        public bool IsKalturaUser
        {
            get
            {
                return ApiClient.Shared.UserInfo.kalturaTokenInfo != null;
            }
        }
    }
}
