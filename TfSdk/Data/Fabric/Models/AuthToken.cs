﻿//Rename TouchCast.Fabric
namespace TfSdk.Data.Fabric.Models
{
    public class AuthToken
    {
        public bool valid { set; get; }
        public int expires { set; get; }
        public bool is_idp { set; get; }
        public string user_id { set; get; }
        public bool fabric_user { set; get; }
    }
}
