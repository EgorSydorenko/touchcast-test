﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TfSdk.Data.Fabric.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UserStatus
    {
        enabled,
        disabled,
        pending_verification
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UserOnlineStatus
    {
        online,
        away,
        offline
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OrganizationOrGroupStatus
    {
        disabled,
        enabled
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UserJoinMode
    {
        public_samedomain,
        public_anydomain,
        invite_samedomain,
        invite_anydomain
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SortOption
    {
        created,
        name,
        filename
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FilePreviewStatus
    {
        processing,
        yes,
        no,
        failed
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FileStatus
    {
        uploading,
        processing,
        ready,
        removed,
        failed
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FileType
    {
        image,
        video,
        performance,
        document,
        project,
        file
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SortDirection
    {
        desc,
        asc
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FilePermission
    {
        me,
        anyone,
        shared_with_me
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FilePermissionSecurity
    {
        private_link,
        password,
        email,
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UsernameDisplay
    {
        username,
        realname
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TitleType
    {
        general,
        direct
        //private
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum InviteNewUser
    {
        admin,
        anyone
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NewTopic
    {
        admin,
        anyone
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ErrorCode
    {
        ERROR_UNKNOWN,
        ERROR_GROUP_NOT_FOUND,
        ERROR_GROUP_DISABLED,
        ERROR_GROUP_INVALID,
        ERROR_GROUP_BEING_REGISTERED,
        ERROR_USER_PENDING_ACTIVATION,
        ERROR_USER_WRONG_DOMAIN,
        ERROR_USER_INVALID_LOGIN,
        ERROR_USER_DISABLED,
        ERROR_USER_GROUP_DUPLICATED,
        ERROR_USER_PERMISSIONS,
        ERROR_USER_CREATE,
        ERROR_USER_DATA_INVALID_PATH_KEY,
        ERROR_USER_DATA_INVALID_NUMBER_MOBILE,
        ERROR_USER_DATA_INVALID_NUMBER_PHONE,
        ERROR_USER_DATA_INVALID_PROFILE,
        ERROR_USER_DATA_INVALID_TIMEZONE_ID,
        ERROR_TOPIC_EMPTY_NAME,
        ERROR_TOPIC_EMPTY_DESCRIPTION,
        ERROR_TOPIC_NAME_ALREADY_EXISTS,
        ERROR_TOPIC_INVALID_NAME,
        ERROR_TOPIC_ID_REQUIRED,
        ERROR_TOPIC_NOT_FOUND,
        ERROR_TOPIC_USER_NOT_FOUND,
        ERROR_TOPIC_USER_ALREADY_MEMBER,
        ERROR_TOPIC_USER_NOT_MEMBER,
        ERROR_TOPIC_USER_MUST_BE_OWNER,
        ERROR_TOPIC_CREATOR_USER_CAN_NOT_REMOVE,
        ERROR_TOPIC_PERFORMANCE_DUPLICATE,
        ERROR_TOPIC_FILE_DUPLICATE,
        ERROR_TOPIC_FILE_POST_PERMISSION,
        ERROR_TOPIC_TYPE_NOT_VALID,
        ERROR_TOPIC_NOT_ALLOWED_TO_ARCHIVE,
        ERROR_DIRECT_TOPIC_USER_ID_REQUIRED,
        ERROR_DIRECT_TOPIC_USER_NOT_FOUND,
        ERROR_DIRECT_TOPIC_SAME_USERS,
        ERROR_DIRECT_TOPIC_ALREADY_EXISTS,
        ERROR_MESSAGE_TOPIC_EMPTY_MESSAGE,
        ERROR_MESSAGE_TOPIC_EMPTY_MESSAGE_ID,
        ERROR_MESSAGE_NOT_FOUND,
        ERROR_MESSAGE_EMPTY_BODY,
        ERROR_MESSAGE_NOT_BELONG_TOPIC,
        ERROR_MESSAGE_USER_NOT_OWNER,
        ERROR_PERFORMANCE_ID_REQUIRED,
        ERROR_PERFORMANCE_IDS_REQUIRED,
        ERROR_PERFORMANCE_NOT_FOUND,
        ERROR_PERFORMANCE_INVALID,
        ERROR_PERFORMANCE_DISABLED,
        ERROR_PERFORMANCE_CREATOR_VIDEO_CANNOT_ADDED,
        ERROR_FILE_ID_REQUIRED,
        ERROR_FILE_NOT_FOUND,
        ERROR_FILE_FORBIDDEN,
        ERROR_FILE_OWNER_CANNOT_ADDED,
        ERROR_FILE_UPLOAD_PROCESSED,
        ERROR_FILE_WITHOUT_PREVIEW,
        ERROR_FILE_DELETED,
        ERROR_FILE_ARCHIVED,
        ERROR_FILE_PREVIEW_NOT_FOUND,
        ERROR_FILE_VAPP_NOT_FOUND,
        ERROR_FILE_UPLOADING,
        ERROR_FILE_REMOVED,
        ERROR_FILE_CANT_DECODE_FILE_HASH,
        ERROR_FILE_COMMENT_NO_PERMISSION,
        ERROR_FILE_EXTERNAL_SECURITY_NOT_SUPPORTED,
        ERROR_FILE_EXTERNAL_SECURITY_INVALID_DOMAIN,
        ERROR_FILE_EXTERNAL_SECURITY_INVALID_CREDENTIALS,
        ERROR_FILE_EXTERNAL_HASH_NOT_FOUND,
        ERROR_FILE_EXTERNAL_NOT_AVAILABLE,
        ERROR_INVALID_PARAMS,
        ERROR_INVALID_REQUEST,
        ERROR_INVALID_CREDENTIALS,
        ERROR_INVALID_METHOD,
        ERROR_INVALID_EMAIL,
        ERROR_EMAIL_DOES_NOT_BELONG,
        ERROR_CODE_EXPIRED,
        ERROR_CODE_NOT_FOUND,
        ERROR_GROUP_SETTINGS_SHARING_NOT_ALLOWED,
        ERROR_ACCESS_TOKEN_NOT_FOUND,
        ERROR_REFRESH_TOKEN_NOT_FOUND,
        ERROR_FILE_PERMISSION_NOT_SUPPORTED,
        ERROR_FILE_UPLOADER_GENERATE_POLICY,
        ERROR_S3_CALL_API,
        ERROR_SNS_CALL_API,
        ERROR_USER_ID_REQUIRED,
        ERROR_USER_NOT_FOUND,
        ERROR_USER_FORBIDDEN,
        ERROR_CUSTOM_DOMAIN_ALREADY_EXISTS,
        ERROR_CUSTOM_DOMAIN_NOT_FOUND,
        ERROR_ORGANIZATION_NOT_FOUND,
        ERROR_ORGANIZATION_FORBIDDEN,
        ERROR_ORGANIZATION_CUSTOM_DOMAIN_NOT_FOUND,
        ERROR_GALLERY_NOT_FOUND,
        ERROR_GROUP_ASSET_TYPE_NOT_SUPPORTED,
        ERROR_GROUP_UPLOADER_GENERATE_POLICY,
        ERROR_GROUP_MIME_TYPE_NOT_SUPPORTED,
        ERROR_GROUP_FILE_HASH_NOT_FOUND,
        ERROR_SYSTEM_CONFIGURATION,
        ERROR_INVITATION_NOT_FOUND,
        ERROR_USER_ALREADY_JOINED,
        ERROR_INVITATION_ANOTHER_EMAIL,
        ERROR_INVITATION_AREADY_ACCEPTED
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OrganizationType
    {
        business,
        education,
        other
    }

    public enum SharedState
    {
        OnlyMe,
        SpecificPeople,
        Group,
        Password,
        Link,
    }
}
