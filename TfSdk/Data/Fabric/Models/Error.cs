﻿namespace TfSdk.Data.Fabric.Models
{
    public class Error
    {
        public ErrorCode code { set; get; }
        public string error { set; get; }
        public string auth_type { set; get; }
    }
}
