﻿namespace TfSdk.Data.Fabric.Models
{
    public class User
    {
        public string user_id { set; get; }
        public string name { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public string avatar_small { set; get; }
        public UserStatus status { set; get; }
        public int? created { set; get; }
        public int? last_login { set; get; }
        public UserOnlineStatus online_status { set; get; }
        public string email { set; get; }
    }
}
