﻿namespace TfSdk.Data.Fabric.Models
{
    public class Organization
    {
        public string organization_id { get; set; }
        public string name { get; set; }
        public string domain { get; set; }
        public OrganizationType type { get; set; }
        public string logo { get; set; }
        public OrganizationOrGroupStatus status { get; set; }
        public int created { get; set; }
        public string default_custom_domain { get; set; }
        public string[] customs_domains { get; set; }
    }

    public class OrganizationPayload
    {
        public string[] ids { get; set; }
    }
}
