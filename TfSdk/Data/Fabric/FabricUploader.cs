﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using TfSdk.Data.Fabric.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TfSdk.Data.Fabric
{
    internal class FabricUploader : IUploader
    {
        #region Constants

        private const string StartUploadingRequestPlaceHolder = "files/upload-start?app_id={0}&group={1}&app_version={2}&mime_type={3}";
        private const string FinalizeUploadingRequestPlaceHolder = "files/upload-finish?app_id={0}&group={1}&app_version={2}";
        private const string GetListRequestPlaceHolder = "files/list/get?app_id={0}&group={1}&app_version={2}";
        private const string FilesShareRequestPlaceHolder = "files/share?app_id={0}&group={1}&app_version={2}";
        private const string FileEditRequestPlaceHolder = "files/edit?app_id={0}&group={1}&app_version={2}";
        private const string FilesDeleteRequest = "files/delete?app_id={0}&group={1}&app_version={2}";
        private const string EmbedCodePattern = "<iframe width='640' height='404' src='{0}' frameborder='0' allowfullscreen> </iframe>";

        #endregion

        internal FabricUploader(ApiClient client)
        {
            _state = IUploaderState.Initialized;
            _apiClient = new WeakReference<ApiClient>(client);
            _uploadingFiles = new FileUploadedResponse();
            _uploadingFiles.file_ids = new List<string>();
        }

        #region Private

        private FileUploadedResponse _uploadingFiles;
        private WeakReference<ApiClient> _apiClient;
        private CancellationTokenSource _cancellationTokenSource;
        private string _permission = ExternaPermissionType.Me;
        private IUploaderState _state;
        private Error _lastError;
        private ICompressedTouchcastInfo _itemInfo;

        #region Requests

        private async Task<object> StartUploadingRequest()
        {
            try
            {
                var apiClient = ApiClient;
                if (apiClient == null)
                {
                    return null;
                }

                UploadingStartedEvent?.Invoke(this);

                var getParams = String.Format(StartUploadingRequestPlaceHolder,
                    Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppId),
                    Uri.EscapeDataString(_itemInfo.Group),
                    Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppVersion),
                    ApiClient.TctMimeType);

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get,
                    new Uri($"{apiClient.Configuration.ApiEndPointUrl}{getParams}"));

                requestMessage.Headers.Add(ApiClient.AuthHeaderKey,
                    ApiClient.AuthHeaderValuePrefix + apiClient.UserInfo.token);

                var response = await apiClient.HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);

                var json = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseData = JsonConvert.DeserializeObject<FileUploadStartResponse>(json);
                    return responseData;
                }
                else
                {
                    var responseData = JsonConvert.DeserializeObject<Error>(json);
                    return responseData;
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return null;
        }

        private FileUploadFinishRequestPayload PreparePayload(FileUploadStartResponse fabricResponse)
        {
            FileUploadFinishRequestPayload payload = new FileUploadFinishRequestPayload();
            payload.file_id = fabricResponse.file_id;
            FileMetadata metaData = new FileMetadata
            {
                name = System.IO.Path.GetFileNameWithoutExtension(_itemInfo.TctFile.Name),
                path = fabricResponse.upload_id,
                original_filename = _itemInfo.TctFile.Name,
                description = _itemInfo.Description,
                permissions = _permission
            };

            payload.files = new List<FileMetadata>() { metaData };
            return payload;
        }

        private async Task<PutObjectResponse> StartUploadToAwsS3(FileUploadStartResponse fabricResponse, string uploadId)
        {
            try
            {
                AWSCredentials credentials = new SessionAWSCredentials(fabricResponse.aws.key,
                    fabricResponse.aws.secret, fabricResponse.aws.session_token);

                var regionEndPoint = RegionEndpoint.GetBySystemName(fabricResponse.aws.region);
                var s3Client = new AmazonS3Client(credentials, regionEndPoint);

                PutObjectRequest uploadRequest = new PutObjectRequest()
                {
                    BucketName = fabricResponse.bucket,
                    Key = $"{fabricResponse.prefix}/{uploadId}",
                    FilePath = _itemInfo.TctFile.Path,
                    CannedACL = S3CannedACL.PublicRead
                };
                uploadRequest.StreamTransferProgress += OnStreamTransferProgress;
                var s3Response = await s3Client.PutObjectAsync(uploadRequest, _cancellationTokenSource.Token);
                uploadRequest.StreamTransferProgress -= OnStreamTransferProgress;

                return s3Response;
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return null;
        }

        private void OnStreamTransferProgress(object sender, StreamTransferProgressArgs e)
        {
            UploadingProgressEvent?.Invoke(sender, e.PercentDone);
        }

        private async Task<bool> PermissionsShareRequest(FileUploadStartResponse fabricResponse)
        {
            try
            {
                var apiClient = ApiClient;
                if (apiClient == null)
                {
                    return false;
                }

                Share share = new Share();
                share.file_id = fabricResponse.file_id;
                share.permissions = _permission;
                var shareContent = new StringContent(JsonConvert.SerializeObject(share), Encoding.UTF8, ApiClient.JsonMimeType);
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri($"{apiClient.Configuration.ApiEndPointUrl}{String.Format(FilesShareRequestPlaceHolder,Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppId) ,Uri.EscapeDataString(_itemInfo.Group), Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppVersion))}"))
                {
                    Content = shareContent
                };

                requestMessage.Headers.Add(ApiClient.AuthHeaderKey,
                    ApiClient.AuthHeaderValuePrefix + apiClient.UserInfo.token);

                var response = await apiClient.HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception)
            {

            }
            return false;
        }

        private async Task<object> FinalizeUploadingRequest(FileUploadFinishRequestPayload payload)
        {
            try
            {
                var apiClient = ApiClient;
                if (apiClient != null)
                {
                    var jsonPayload = JsonConvert.SerializeObject(payload);
                    var payloadContent = new StringContent(jsonPayload, Encoding.UTF8, ApiClient.JsonMimeType);
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                        new Uri($"{apiClient.Configuration.ApiEndPointUrl}{String.Format(FinalizeUploadingRequestPlaceHolder, Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppId) ,Uri.EscapeDataString(_itemInfo.Group), Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppVersion))}"))
                    {
                        Content = payloadContent
                    };

                    requestMessage.Headers.Add(ApiClient.AuthHeaderKey,
                        ApiClient.AuthHeaderValuePrefix + apiClient.UserInfo.token);

                    var response = await apiClient.HttpClient.SendAsync(requestMessage, _cancellationTokenSource.Token);

                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var finalizeResponseData =
                            JsonConvert.DeserializeObject<FileUploadedResponse>(jsonResponse);
                        return finalizeResponseData;
                    }
                    else
                    {
                        var finalizeResponseData =
                            JsonConvert.DeserializeObject<Error>(jsonResponse);
                        return finalizeResponseData;
                    }
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return null;
        }

        private async Task RemoveFileRequest(string group)
        {
            try
            {
                var apiClient = ApiClient;
                if (apiClient == null)
                {
                    return;
                }
                var json = JsonConvert.SerializeObject(_uploadingFiles);
                var deleteContent = new StringContent(json, Encoding.UTF8, ApiClient.JsonMimeType);
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                    $"{apiClient.Configuration.ApiEndPointUrl}{String.Format(FilesDeleteRequest, Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppId), Uri.EscapeDataString(group), Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppVersion))}")
                {
                    Content = deleteContent
                };

                requestMessage.Headers.Add(ApiClient.AuthHeaderKey,
                    ApiClient.AuthHeaderValuePrefix + apiClient.UserInfo.token);

                var response = await apiClient.HttpClient.SendAsync(requestMessage);
                var jsonResponse = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        private async Task<object> FilesGetListRequest(FileUploadStartResponse requestFinishPayload)
        {
            try
            {
                var apiClient = ApiClient;
                if (apiClient == null)
                {
                    return null;
                }
                Payload payload = new Payload();
                payload.ids = new List<string>() { requestFinishPayload.file_id };
                payload.limit = 1;
                payload.offset = 0;
                payload.sort = SortOption.created;
                payload.sort_directions = SortDirection.asc;

                var jsonPayload = JsonConvert.SerializeObject(payload);

                var payloadContent = new StringContent(jsonPayload, Encoding.UTF8, ApiClient.JsonMimeType);

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                    $"{apiClient.Configuration.ApiEndPointUrl}{String.Format(GetListRequestPlaceHolder,apiClient.Configuration.AppInfo.AppId, _itemInfo.Group, apiClient.Configuration.AppInfo.AppVersion)}")
                {
                    Content = payloadContent
                };

                requestMessage.Headers.Add(ApiClient.AuthHeaderKey,
                    ApiClient.AuthHeaderValuePrefix + apiClient.UserInfo.token);

                var response = await apiClient.HttpClient.SendAsync(requestMessage);
                var jsonResponse = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var finalizeResponseData =
                        JsonConvert.DeserializeObject<FilesList>(jsonResponse);
                    return finalizeResponseData;
                }
                else
                {
                    var finalizeResponseData =
                        JsonConvert.DeserializeObject<Error>(jsonResponse);
                    return finalizeResponseData;
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return null;
        }

        private async Task<object> UpdateNameAndDescriptionRequest(string name, string description, string group)
        {
            try
            {
                var apiClient = ApiClient;
                if (apiClient == null) return null;
                FileEdit fileIdit = new FileEdit()
                {
                    name = name,
                    description = description,
                    file_id = _uploadingFiles.file_ids.First()
                };
                var json = JsonConvert.SerializeObject(fileIdit);
                var content = new StringContent(json, Encoding.UTF8, ApiClient.JsonMimeType);
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,
                    new Uri(
                        $"{apiClient.Configuration.ApiEndPointUrl}{String.Format(FileEditRequestPlaceHolder, Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppId), Uri.EscapeDataString(group),Uri.EscapeDataString(apiClient.Configuration.AppInfo.AppVersion))}"))
                {
                    Content = content
                };
                requestMessage.Headers.Add(ApiClient.AuthHeaderKey,
                    ApiClient.AuthHeaderValuePrefix + apiClient.UserInfo.token);
                var response = await apiClient.HttpClient.SendAsync(requestMessage);
                var jsonResponse = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<File>(jsonResponse);
                }
                else
                {
                    return JsonConvert.DeserializeObject<Error>(jsonResponse);
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #endregion

        public IUploaderState State
        {
            private set
            {
                _state = value;
                switch (_state)
                {
                    case IUploaderState.Completed:
                        UploadingCompletedEvent?.Invoke(this);
                        break;
                    case IUploaderState.Failed:
                        if (_lastError?.code == ErrorCode.ERROR_CODE_EXPIRED ||
                            _lastError?.code == ErrorCode.ERROR_ACCESS_TOKEN_NOT_FOUND)
                        {
                            ApiClient?.ExpireSession(_lastError);
                        }
                        UploadingFailedEvent?.Invoke(this, _lastError);
                        break;
                }
            }
            get => _state;
        }

        public ApiClient ApiClient
        { 
            get
            {
                ApiClient client;
                if (_apiClient.TryGetTarget(out client))
                {
                    return client;
                }
                return null;
            }
        }

        public event UploadingStarted UploadingStartedEvent;
        public event UploadingProgress UploadingProgressEvent;
        public event UploadingCompleted UploadingCompletedEvent;
        public event UploadingFailed UploadingFailedEvent;
        public event UploadingDataRecived UploadingDataRecivedEvent;
        public event UploadingFileIdRecived UploadingFileIdRecivedEvent;

        public async Task Cancel()
        {
            if (_state == IUploaderState.Uploading)
            {
                _cancellationTokenSource.Cancel();
                await RemoveFileRequest(_itemInfo.Group);
                _uploadingFiles.file_ids.Clear();
            }
        }

        public async Task UpdateFileInfo(string name, string description)
        {
            if (State == IUploaderState.Canceled || State == IUploaderState.Failed ||
                   State == IUploaderState.Initialized) return;
            await UpdateNameAndDescriptionRequest(name, description, _itemInfo.Group);
        }

        public async Task Upload(TouchcastInfo wrapper)
        {
            if (_state == IUploaderState.Initialized)
            {
                _itemInfo = wrapper.CompressedInfo;
                State = IUploaderState.Uploading;
                _cancellationTokenSource = new CancellationTokenSource();
                var fileStartUploadResponse = await StartUploadingRequest();
                if (fileStartUploadResponse is FileUploadStartResponse)
                {
                    string fileId = (fileStartUploadResponse as FileUploadStartResponse).file_id;
                    _uploadingFiles.file_ids.Add(fileId);
                    UploadingFileIdRecivedEvent?.Invoke(this, fileId);

                    var result = await PermissionsShareRequest(fileStartUploadResponse as FileUploadStartResponse);

                    if (!result)
                    {
                        State = _cancellationTokenSource.IsCancellationRequested
                            ? IUploaderState.Canceled
                            : IUploaderState.Failed;
                        return;
                    }

                    var getUploadedData = await FilesGetListRequest(fileStartUploadResponse as FileUploadStartResponse);
                    if (getUploadedData is FilesList)
                    {
                        var uploadResult = getUploadedData as FilesList;
                        if (uploadResult?.files != null)
                        {
                            foreach (var file in uploadResult.files)
                            {
                                UploadingDataRecivedEvent?.Invoke(this, IUploaderConstants.LinkKey, file.external_permissions.url);
                                UploadingDataRecivedEvent?.Invoke(this, IUploaderConstants.EmbedCodeKey, String.Format(EmbedCodePattern, file.external_permissions.url));
                            }
                        }
                    }
                    else
                    {
                        State = _cancellationTokenSource.IsCancellationRequested
                            ? IUploaderState.Canceled
                            : IUploaderState.Failed;
                        return;
                    }

                    var awss3Response = await StartUploadToAwsS3(fileStartUploadResponse as FileUploadStartResponse, (fileStartUploadResponse as FileUploadStartResponse).upload_id);
                    if (awss3Response != null)
                    {
                        var payload = PreparePayload(fileStartUploadResponse as FileUploadStartResponse);
                        var fabricFinalizeReponce = await FinalizeUploadingRequest(payload);

                        if (fabricFinalizeReponce is FileUploadedResponse)
                        {
                            State = IUploaderState.Completed;
                        }
                        else
                        {
                            if (fabricFinalizeReponce is Error)
                            {
                                _lastError = fabricFinalizeReponce as Error;
                            }
                            State = IUploaderState.Failed;
                        }
                    }
                    else
                    {
                        State = _cancellationTokenSource.IsCancellationRequested
                            ? IUploaderState.Canceled
                            : IUploaderState.Failed;
                    }
                }
                else
                {
                    if (fileStartUploadResponse is Error)
                    {
                        _lastError = fileStartUploadResponse as Error;
                    }
                    State = IUploaderState.Failed;
                }
            }
        }

        public List<string> GetSharableOptions()
        {
            throw new NotImplementedException();
        }
    }
}
