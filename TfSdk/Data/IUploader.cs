﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TfSdk.Data
{
    public delegate void UploadingStarted(object sender);

    public delegate void UploadingFileIdRecived(object sender, string fileId);

    public delegate void UploadingFailed(object sender, object reason);

    public delegate void UploadingProgress(object sender, int persentsOfProgress);
    //Move embeded code to uploader!
    public delegate void UploadingDataRecived(object sender, string sharableOption, string sharableLink);

    public delegate void UploadingCompleted(object sender);

    public static class IUploaderConstants
    {
        public const string LinkKey = "link";
        public const string EmbedCodeKey = "embedcode";
    }

    public interface IUploader
    {
        ApiClient ApiClient { get; } 
        IUploaderState State { get; }
        Task Upload(TouchcastInfo wrapper);
        Task UpdateFileInfo(string name, string description);
        Task Cancel();
        List<string> GetSharableOptions();
        event UploadingStarted UploadingStartedEvent;
        event UploadingFileIdRecived UploadingFileIdRecivedEvent;
        event UploadingProgress UploadingProgressEvent;
        event UploadingCompleted UploadingCompletedEvent;
        event UploadingFailed UploadingFailedEvent;
        event UploadingDataRecived UploadingDataRecivedEvent;
    }
}
