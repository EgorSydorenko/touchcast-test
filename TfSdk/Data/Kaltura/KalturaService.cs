﻿namespace TfSdk.Data.Kaltura
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using Windows.Storage;
    using Newtonsoft.Json;
    using Data;
    using System.Xml;
    using Windows.Web.Http;
    using Windows.Storage.Streams;
    using TfSdk.Data.Fabric.Models;
    using TfSdk.Data.Kaltura.Models;
    using Windows.Web.Http.Filters;
    using System.Collections.Generic;

    internal class KalturaService
    {
        public delegate void KalturaUploaderShareUrlReady(object sender, string url);

        #region constants
        //#define K_KALTURA_URL @"https://www.kaltura.com/api_v3/"
        private const string ServiceBaseUrl = "https://www.kaltura.com/api_v3";
        
        // public const string ViewBaseUrl = "https://kaltura-dev.touchcast.com/";
        //public const string ViewBaseUrl = "https://mediaexchange.accenture.com/";
        public const string ExternallyShareURL = "https://accenture.touchcast.com/client_api/kaltura-import-media-entry?apikey=123&client=ipad";

        //public static readonly string LoginUrl = $"{ViewBaseUrl}touchcast/index/auth/context/?device=ios-auth";
        //public static readonly string LogoutUrl = $"{ViewBaseUrl}user/logout";
        // private const string SettingName = "LoginResponse";
        public static List<string> ImageExtensions = new List<string>
        {
            ".jpg"
            , ".jpeg"
            ,".png"
        };
        public static List<string> VideoExtensions = new List<string>
        {
            ".mp4"
        };
        public static List<string> DocumentExtensions = new List<string>
        {
            ".pdf"
        };
        #endregion

        #region private fileds
        private bool _isFirstFileUploaded = false;
        private Action<int> _uploadProgressHandler;
        private ulong _totalUploadingSize = 0;
        //private SessionInfoModel _sessionInfo;
        private IProgress<HttpProgress> _progress;
        private ExtraAuthToken _kalturaInfo;
        #endregion

        #region public Interface
        public event KalturaUploaderShareUrlReady KalturaUploaderShareUrlReadyEvent;
        public KalturaService(ExtraAuthToken kalturaInfo)
        {
            _kalturaInfo = kalturaInfo;
            _progress = new Progress<Windows.Web.Http.HttpProgress>(ProgressHandler);
        }

        public async Task<bool> IsSessionValidAsync()
        {
            bool result = false;

            if (!string.IsNullOrEmpty(_kalturaInfo.token))
            {
                using (var httpClient = new HttpClient())
                {

                    string requestUrl =($"{ServiceBaseUrl}/service/user/action/get?format=1&ks={_kalturaInfo.token}");
                    var httpResponseMessage = await httpClient.GetAsync(new Uri(requestUrl)).AsTask();
                    if (await IsValidKalturaResponse(httpResponseMessage))
                    {
                        //sample error response {"code":"INVALID_KS","message":"Invalid KS \"EXPIRED\", Error \"-1,INVALID_STR\"","objectType":"KalturaAPIException","args":{"KSID":"EXPIRED","ERR_CODE":"-1","ERR_DESC":"INVALID_STR"}}
                        var responseString = await httpResponseMessage.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(responseString) && !responseString.Contains("KalturaAPIException"))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }
        ulong videoFileSize;
        ulong thumbnailSize;
        public async Task<KalturaMediaEntryModel> UploadVideoAsync(bool isNeedShareExternal, IUncompressedTouchcastInfo uploadData, Action<int> progressHandler, CancellationToken cancellationToken)
        {
            videoFileSize = (await uploadData.VideoFile.GetBasicPropertiesAsync()).Size;
            thumbnailSize = (await uploadData.VideoThumbnailFile.GetBasicPropertiesAsync()).Size;
            _totalUploadingSize = videoFileSize + thumbnailSize;
            var files = await uploadData.PrepareUploadingFiles();
            foreach (var dynamicObj in files)
            {
                var fileSize = (await dynamicObj.Item2.GetBasicPropertiesAsync()).Size;
                _totalUploadingSize += fileSize;
            }
            _uploadProgressHandler = progressHandler;
            if (uploadData.VideoFile != null && !string.IsNullOrWhiteSpace(uploadData.Title) && uploadData.VideoThumbnailFile != null && uploadData != null)
            {
                if (!string.IsNullOrEmpty(_kalturaInfo.token))
                {
                    int currentProgress = 0;
                    progressHandler.Invoke(currentProgress);

                    // Uploading video
                    var mainMediaEntry = await UploadMainVideoAsync(uploadData.VideoFile, uploadData.Title, uploadData.Description, cancellationToken);
                    KalturaUploaderShareUrlReadyEvent?.Invoke(this, $"{_kalturaInfo.extra.endpoint}/media/{mainMediaEntry.Id}");
                    // Uploading thunbnail
                    _isFirstFileUploaded = true;
                    var mainThumbAsset = await UploadMainThumbFromStreamAsync(await uploadData.VideoThumbnailFile.OpenReadAsync(), uploadData.VideoThumbnailFile.Name, mainMediaEntry.Id, cancellationToken);
                    if (files != null && files.Count > 0)
                    {
                        string relatedFilesXml = null;

                        foreach (var dynamicObj in files)
                        {
                            if(dynamicObj.Item1 == "thumb")
                            {
                                var resourceThumbAssetModel = await UploadResourceThumbAsync(dynamicObj.Item2, mainMediaEntry.Id, cancellationToken);
                                var resourceThumbUrl = $"kaltura-asset://id={resourceThumbAssetModel.Id}&type=related_file";
                                uploadData.UpdateCommandByUrl(dynamicObj.Item2.Name, resourceThumbUrl);
                            }
                            else
                            {

                                //if (resource.Type != DraggableRecordableItemType.WebView)
                                //{

                                //    //todo: add the audio
                                //    var isMedia = resource.Type == DraggableRecordableItemType.Video || resource.Type == DraggableRecordableItemType.Image;
                                //    var resourceModel = isMedia
                                //        ? (KalturaBaseModel)await UploadMediaResourceAsync(resource, mainMediaEntry.Id, resource.Type == DraggableRecordableItemType.Video ? KalturaMediaType.Video : KalturaMediaType.Image, cancellationToken)
                                //        : await UploadDocumentResourceAsync(resource, mainMediaEntry.Id, cancellationToken);

                                //    relatedFilesXml += $"<RelatedFiles>{resourceModel.Id}:{(isMedia ? "media" : "related_file")}</RelatedFiles>";
                                //    resourceUrl = $"kaltura-asset://id={resourceModel.Id}&type={(isMedia ? "media" : "related_file")}";
                                //}

                                //if (resource.Thumbnail != null)
                                //{
                                //    var resourceThumbAssetModel = await UploadResourceThumbAsync(resource.Thumbnail, mainMediaEntry.Id, cancellationToken);
                                //    resourceThumbUrl = $"kaltura-asset://id={resourceThumbAssetModel.Id}&type=related_file";
                                //}

                                var extension = Path.GetExtension(dynamicObj.Item2.Name);
                                KalturaBaseModel resourceModel = null;
                                bool isMedia = false;
                                if(ImageExtensions.Contains(extension.ToLower()))
                                {
                                    isMedia = true;
                                    resourceModel =  await UploadMediaResourceAsync(dynamicObj.Item2, mainMediaEntry.Id, KalturaMediaType.Image, cancellationToken);
                                }
                               else if(VideoExtensions.Contains(extension.ToLower()))
                                {
                                    isMedia = true;
                                    resourceModel =  await UploadMediaResourceAsync(dynamicObj.Item2, mainMediaEntry.Id, KalturaMediaType.Video, cancellationToken);
                                }
                                else if(DocumentExtensions.Contains(extension.ToLower()))
                                {
                                    resourceModel = await UploadDocumentResourceAsync(dynamicObj.Item2, mainMediaEntry.Id, cancellationToken);
                                }
                                relatedFilesXml += $"<RelatedFiles>{resourceModel.Id}:{(isMedia ? "media" : "related_file")}</RelatedFiles>";
                                var resourceUrl = $"kaltura-asset://id={resourceModel.Id}&type={(isMedia ? "media" : "related_file")}";
                                uploadData.UpdateCommandByUrl(dynamicObj.Item2.Name, resourceUrl);
                            }
                        }
                        var kalturaMetadataModel = await GetMetadataProfileAsync(mainMediaEntry.Id, $"<metadata><IsTouchCast>true</IsTouchCast>{relatedFilesXml}</metadata>", cancellationToken);
                    }


                    // Uploading actions.xml
                    var actionsXmlUploadToken = await GetUploadTokenAsync("actions.xml", cancellationToken);
                    var stream = GetStreamFromString(uploadData.ActionsXmlToString()).AsRandomAccessStream();
                    _totalUploadingSize += stream.Size;
                    actionsXmlUploadToken = await UploadFileFromStreamAsync(stream, "actions.xml", actionsXmlUploadToken.Id, cancellationToken);

                    var actionsXmlAttachmentAsset = await GetAttachmentAssetAsync(mainMediaEntry.Id, "actions.xml", KalturaAttachmentType.Text, cancellationToken);
                    actionsXmlAttachmentAsset = await AttachContentToAttachmentAssetAsync(actionsXmlAttachmentAsset.Id, actionsXmlUploadToken.Id, cancellationToken);

                    //After Check Uploading Fix ShareExternal
                    if (isNeedShareExternal)
                    {
                        dynamic SharedValues = await ShareToExternalLink(mainMediaEntry.Id, _kalturaInfo.token);
                        mainMediaEntry.ExternalURL = SharedValues.URL;
                        mainMediaEntry.EmbedCode = SharedValues.EmbedCode;
                    }

                    //Analytics.GoogleAnalyticsManager.TrySend(Analytics.AnalyticCommandType.AnalyticCommandUpload);
                    progressHandler.Invoke(100);

                    return mainMediaEntry;
                }
                var kalturaApiException = new KalturaApiException { Code = "MISSING_KS" };
                throw kalturaApiException;
            }
            throw new InvalidOperationException("Upload could not be finished. Invalid arguments");
            #region Old Uploading
            /*foreach(var data in uploadData.Resources)
            {
                if (data.Resource != null)
                    totalUploadingSize += (await data.Resource.GetBasicPropertiesAsync()).Size;


                if (data.Thumbnail != null)
                    totalUploadingSize += (await data.Thumbnail.GetBasicPropertiesAsync()).Size;
            }

            uploadProgressHandler = progressHandler;
            if (uploadData.VideoFile != null && !string.IsNullOrWhiteSpace(uploadData.Title) && uploadData.VideoThumbnailFile != null && uploadData != null)
            {
                if (!string.IsNullOrEmpty(SessionInfo?.SessionId))
                {
                    int currentProgress = 0;
                    progressHandler.Invoke(currentProgress);

                    // Uploading video
                    var mainMediaEntry = await UploadMainVideoAsync(uploadData.VideoFile, uploadData.Title, uploadData.Description, cancellationToken);

                    // Uploading thunbnail
                    var mainThumbAsset = await UploadMainThumbFromStreamAsync(await uploadData.VideoThumbnailFile.OpenReadAsync(), uploadData.VideoThumbnailFile.Name, mainMediaEntry.Id, cancellationToken);

                    // Uploading media resources
                    if (uploadData.Resources != null && uploadData.Resources.Count > 0)
                    {
                        string relatedFilesXml = null;

                        foreach (var resource in uploadData.Resources.Where(r => r.Resource != null || r.Thumbnail != null))
                        {
                            string resourceUrl = string.Empty;
                            string resourceThumbUrl = string.Empty;
                            if (resource.Type != DraggableRecordableItemType.WebView)
                            {

                                //todo: add the audio
                                var isMedia = resource.Type == DraggableRecordableItemType.Video || resource.Type == DraggableRecordableItemType.Image;
                                var resourceModel = isMedia
                                    ? (KalturaBaseModel)await UploadMediaResourceAsync(resource, mainMediaEntry.Id, resource.Type == DraggableRecordableItemType.Video ? KalturaMediaType.Video : KalturaMediaType.Image, cancellationToken)
                                    : await UploadDocumentResourceAsync(resource, mainMediaEntry.Id, cancellationToken);

                                relatedFilesXml += $"<RelatedFiles>{resourceModel.Id}:{(isMedia ? "media" : "related_file")}</RelatedFiles>";
                                resourceUrl = $"kaltura-asset://id={resourceModel.Id}&type={(isMedia ? "media" : "related_file")}";
                            }

                            if (resource.Thumbnail != null)
                            {
                                var resourceThumbAssetModel = await UploadResourceThumbAsync(resource.Thumbnail, mainMediaEntry.Id, cancellationToken);
                                resourceThumbUrl = $"kaltura-asset://id={resourceThumbAssetModel.Id}&type=related_file";
                            }

                            uploadData.UpdateCommandByRecourceId(resource.ID, resourceThumbUrl, resourceUrl);
                        }

                        var kalturaMetadataModel = await GetMetadataProfileAsync(mainMediaEntry.Id, $"<metadata><IsTouchCast>true</IsTouchCast>{relatedFilesXml}</metadata>", cancellationToken);
                    }

                    // Uploading actions.xml
                    var actionsXmlUploadToken = await GetUploadTokenAsync("actions.xml", cancellationToken);
                    actionsXmlUploadToken = await UploadFileFromStreamAsync(GetStreamFromString(uploadData.ActionsXMLString()).AsRandomAccessStream(), "actions.xml", actionsXmlUploadToken.Id, cancellationToken);

                    var actionsXmlAttachmentAsset = await GetAttachmentAssetAsync(mainMediaEntry.Id, "actions.xml", KalturaAttachmentType.Text, cancellationToken);
                    actionsXmlAttachmentAsset = await AttachContentToAttachmentAssetAsync(actionsXmlAttachmentAsset.Id, actionsXmlUploadToken.Id, cancellationToken);

                    //After Check Uploading Fix ShareExternal
                    if (isNeedShareExternal)
                    {
                        dynamic SharedValues = await ShareToExternalLink(mainMediaEntry.Id, SessionInfo.SessionId);
                        mainMediaEntry.ExternalURL = SharedValues.URL;
                        mainMediaEntry.EmbedCode = SharedValues.EmbedCode;
                    }

                    Analytics.GoogleAnalyticsManager.TrySend(Analytics.AnalyticCommandType.AnalyticCommandUpload);
                    progressHandler.Invoke(100);

                    return mainMediaEntry;
                }

                var kalturaApiException = new KalturaApiException { Code = "MISSING_KS" };
                throw kalturaApiException;
            }
            throw new InvalidOperationException("Upload could not be finished. Invalid arguments");*/
            #endregion
        }
        #endregion

        #region private Interface
        ulong requestProgress = 0;
        private void ProgressHandler(Windows.Web.Http.HttpProgress progress)
        {
            ulong totalBytesToSend = 0;
            if (progress.TotalBytesToSend.HasValue)
            {
                totalBytesToSend = progress.TotalBytesToSend.Value;
            }

            //ulong totalBytesToReceive = 0;
            //if (progress.TotalBytesToReceive.HasValue)
            //{
            //    totalBytesToReceive = progress.TotalBytesToReceive.Value;
            //}

            
            if (progress.Stage == HttpProgressStage.SendingContent && totalBytesToSend > 0)
            {
                if (!_isFirstFileUploaded)
                {
                    requestProgress = (progress.BytesSent * 100 / _totalUploadingSize);
                }
                else
                {
                    requestProgress += (progress.BytesSent * 100 / _totalUploadingSize);
                }
            }
            //else if (progress.Stage == HttpProgressStage.ReceivingContent)
            //{
            //    if (totalBytesToReceive > 0)
            //    {
            //        requestProgress += progress.BytesReceived * 100 / totalBytesToReceive;
            //    }
            //}
            else
            {
                return;
            }
            if (requestProgress <= 100)
            {
                _uploadProgressHandler?.Invoke((int)requestProgress);
            }
        }

        private async Task<KalturaAttachmentAssetModel> UploadResourceThumbAsync(IStorageFile thumbFile, string mainMediaEntryId, CancellationToken cancellationToken)
        {
            var uploadToken = await GetUploadTokenAsync(thumbFile.Name, cancellationToken);
            uploadToken = await UploadFileFromStreamAsync(await thumbFile.OpenReadAsync(), thumbFile.Name, uploadToken.Id, cancellationToken);
            var attachmentAsset = await GetAttachmentAssetAsync(mainMediaEntryId, thumbFile.Name, KalturaAttachmentType.Text, cancellationToken);
            attachmentAsset = await AttachContentToAttachmentAssetAsync(attachmentAsset.Id, uploadToken.Id, cancellationToken);

            return attachmentAsset;
        }

        private async Task<object> ShareToExternalLink(string mediaEntryID, string kalturaSession)
        {
            String resultURL = String.Empty;
            String embedCode = String.Empty;
            try
            {
                var url = $"{ExternallyShareURL}&kaltura_partner_id={_kalturaInfo.extra.partner_id}&kaltura_media_id={mediaEntryID}&kaltura_ks={kalturaSession}";
                var httpClient = new HttpClient();
                var httpResponseMessage = await httpClient.GetAsync(new Uri(url));
                    if (await IsValidKalturaResponse(httpResponseMessage))
                    {
                        using (var xmlStream = await httpResponseMessage.Content.ReadAsInputStreamAsync())
                        {
                            XmlReaderSettings settings = new XmlReaderSettings();
                            settings.Async = true;
                            var reader = XmlReader.Create(xmlStream.AsStreamForRead(), settings);
                            reader.ReadToFollowing("share_url");
                            reader.Read();
                            resultURL = reader.Value;
                            reader.ReadToFollowing("embed_code");
                            reader.Read();
                            embedCode = reader.Value;
                        }
                    }
            }
            catch /*(Exception ex)*/
            { }
            return new { URL = resultURL, EmbedCode = embedCode };
        }

        private async Task<KalturaAttachmentAssetModel> UploadDocumentResourceAsync(StorageFile documentResource, string mainMediaEntryId, CancellationToken cancellationToken)
        {
            var uploadToken = await GetUploadTokenAsync(documentResource.Name, cancellationToken);
            uploadToken = await UploadFileFromStreamAsync(await documentResource.OpenReadAsync(), documentResource.Name, uploadToken.Id, cancellationToken);
            var attachmentAsset = await GetAttachmentAssetAsync(mainMediaEntryId, documentResource.Name, KalturaAttachmentType.Text, cancellationToken);
            attachmentAsset = await AttachContentToAttachmentAssetAsync(attachmentAsset.Id, uploadToken.Id, cancellationToken);

            return attachmentAsset;
        }

        private async Task<KalturaMediaEntryModel> UploadMediaResourceAsync(StorageFile mediaResource, string mainMediaEntryId, KalturaMediaType type, CancellationToken cancellationToken)
        {
            var uploadToken = await GetUploadTokenAsync(mediaResource.Name, cancellationToken);
            uploadToken = await UploadFileFromStreamAsync(await mediaResource.OpenReadAsync(), mediaResource.Name, uploadToken.Id, cancellationToken);
            var mediaEntry = await GetMediaEntryAsync(mediaResource.Name ?? mediaResource.Name, String.Empty, type, cancellationToken, mainMediaEntryId);
            mediaEntry = await AttachVideoAsync(uploadToken.Id, mediaEntry.Id, cancellationToken);

            return mediaEntry;
        }

        private async Task<KalturaMediaEntryModel> UploadMainVideoAsync(IStorageFile videoFile, string mediaEntryName, string mediaEntryDescription, CancellationToken cancellationToken)
        {
            var uploadToken = await GetUploadTokenAsync(videoFile.Name, cancellationToken);
            uploadToken = await UploadFileFromStreamAsync(await videoFile.OpenReadAsync(), videoFile.Name, uploadToken.Id, cancellationToken);
            var mediaEntry = await GetMediaEntryAsync(mediaEntryName, mediaEntryDescription, KalturaMediaType.Video, cancellationToken, tags: "CreatedWithTouchCastUniqueTag");
            mediaEntry = await AttachVideoAsync(uploadToken.Id, mediaEntry.Id, cancellationToken);

            return mediaEntry;
        }

        //========================================================

        private async Task<KalturaThumbAssetModel> UploadMainThumbFromStreamAsync(IRandomAccessStream fileStream, string fileName, string mediaEntryId, CancellationToken cancellationToken)
        {
            if (fileStream != null && !string.IsNullOrEmpty(mediaEntryId))
            {
                var httpClient = new HttpClient();

                string requestUrl = $"{ServiceBaseUrl}/service/thumbasset/action/addFromImage?format=1";

                var fileContent = new HttpMultipartFormDataContent("---------------------------" + DateTime.Now.Ticks.ToString("x"));

                fileContent.Add(new HttpStringContent(_kalturaInfo.token), "ks");
                fileContent.Add(new HttpStringContent(mediaEntryId), "entryId");
                fileContent.Add(new HttpStreamContent(fileStream), "fileData", fileName);

                var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), fileContent).AsTask(cancellationToken, _progress);
                if (await IsValidKalturaResponse(httpResponseMessage))
                {
                    var result = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result.Contains("KalturaAPIException"))
                        {
                            var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                            throw kalturaAPIException;
                        }

                        var kalturaUploadToken = JsonConvert.DeserializeObject<KalturaThumbAssetModel>(result);
                        return kalturaUploadToken;
                    }
                    throw new InvalidOperationException("Main thumb upload could not be finished. Response is empty");
                }
                throw new InvalidOperationException($"Main thumb upload could not be finished. {httpResponseMessage.ReasonPhrase}");
            }
            throw new InvalidOperationException("Main thumb upload could not be finished. No file");
        }

        private async Task<KalturaMetadataModel> GetMetadataProfileAsync(string videoMediaEntryId, string xmlData, CancellationToken cancellationToken)
        {
            var httpClient = new HttpClient();
            string requestUrl = $"{ServiceBaseUrl}/service/metadata_metadata/action/add?format=1&ks={_kalturaInfo.token}&metadataProfileId={_kalturaInfo.extra.profile_id}&objectType=1&objectId={videoMediaEntryId}&xmlData={xmlData}";

            var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), new HttpStringContent("")).AsTask(cancellationToken);
            if (await IsValidKalturaResponse(httpResponseMessage))
            {
                var result = await httpResponseMessage.Content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(result))
                {
                    if (result.Contains("KalturaAPIException"))
                    {
                        var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                        throw kalturaAPIException;
                    }

                    var kalturaAttachmentAsset = JsonConvert.DeserializeObject<KalturaMetadataModel>(result);

                    return kalturaAttachmentAsset;
                }
                throw new InvalidOperationException("Metadata could not be updated. Response is empty");
            }
            throw new InvalidOperationException($"Metadata could not be updated. {httpResponseMessage.ReasonPhrase}");
        }

        private async Task<KalturaAttachmentAssetModel> AttachContentToAttachmentAssetAsync(string attachmentAssetId, string uploadTokeId, CancellationToken cancellationToken)
        {
            var httpClient = new HttpClient();

            string requestUrl = $"{ServiceBaseUrl}/service/attachment_attachmentAsset/action/setContent?format=1&ks={_kalturaInfo.token}&id={attachmentAssetId}&contentResource:objectType=KalturaUploadedFileTokenResource&contentResource:token={uploadTokeId}";

            var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), new HttpStringContent("")).AsTask(cancellationToken);
            if (await IsValidKalturaResponse(httpResponseMessage))
            {
                var result = await httpResponseMessage.Content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(result))
                {
                    if (result.Contains("KalturaAPIException"))
                    {
                        var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                        throw kalturaAPIException;
                    }

                    var kalturaAttachmentAsset = JsonConvert.DeserializeObject<KalturaAttachmentAssetModel>(result);

                    return kalturaAttachmentAsset;
                }
                throw new InvalidOperationException("Content could not be attached to Attachment Asset. Response is empty");
            }
            throw new InvalidOperationException($"Content could not be attached to Attachment Asset. {httpResponseMessage.ReasonPhrase}");
        }

        private async Task<KalturaAttachmentAssetModel> GetAttachmentAssetAsync(string mediaEntryId, string attachmentFileName, KalturaAttachmentType type, CancellationToken cancellationToken)
        {
            var httpClient = new HttpClient();
            string requestUrl = $"{ServiceBaseUrl}/service/attachment_attachmentAsset/action/add?format=1&ks={_kalturaInfo.token}&entryId={mediaEntryId}&attachmentAsset:filename={attachmentFileName}&attachmentAsset:format={(int)type}";

            var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), new HttpStringContent("")).AsTask(cancellationToken);
            if (await IsValidKalturaResponse(httpResponseMessage))
            {
                var result = await httpResponseMessage.Content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(result))
                {
                    if (result.Contains("KalturaAPIException"))
                    {
                        var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                        throw kalturaAPIException;
                    }

                    var kalturaAttachmentAsset = JsonConvert.DeserializeObject<KalturaAttachmentAssetModel>(result);

                    return kalturaAttachmentAsset;
                }
                throw new InvalidOperationException("Attachment asset could not be created. Response is empty");
            }
            throw new InvalidOperationException($"Attachment asset could not be created. {httpResponseMessage.ReasonPhrase}");
        }

        private async Task<KalturaUploadTokenModel> GetUploadTokenAsync(string fileName, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                var httpClient = new HttpClient();
                
                string requestUrl = $"{ServiceBaseUrl}/service/uploadToken/action/add?format=1&ks={_kalturaInfo.token}&fileName={fileName}&partnerId={_kalturaInfo.extra.partner_id}";

                var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), new HttpStringContent("")).AsTask(cancellationToken);
                if (await IsValidKalturaResponse(httpResponseMessage))
                {
                    var result = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result.Contains("KalturaAPIException"))
                        {
                            var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                            throw kalturaAPIException;
                        }

                        var kalturaUploadToken = JsonConvert.DeserializeObject<KalturaUploadTokenModel>(result);

                        return kalturaUploadToken;
                    }
                    throw new InvalidOperationException("Upload token could not be created. Upload token is empty");
                }
                throw new InvalidOperationException($"Upload token could not be created. {httpResponseMessage.ReasonPhrase}");
            }
            throw new InvalidOperationException("Upload token could not be created. File name is empty");
        }

        private async Task<KalturaUploadTokenModel> UploadFileFromStreamAsync(IRandomAccessStream fileStream, string fileName, string uploadTokenId, CancellationToken cancellationToken)
        {
            if (fileStream != null && !string.IsNullOrEmpty(fileName))
            {
                var httpClient = new HttpClient();

                string requestUrl = $"{ServiceBaseUrl}/service/uploadToken/action/upload?format=1";

                var fileContent = new HttpMultipartFormDataContent("---------------------------" + DateTime.Now.Ticks.ToString("x"));

                fileContent.Add(new HttpStringContent(_kalturaInfo.token), "ks");
                fileContent.Add(new HttpStringContent(uploadTokenId), "uploadTokenId");
                fileContent.Add(new HttpStreamContent(fileStream), "fileData", fileName);

                var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), fileContent).AsTask(cancellationToken, _progress);
                if (await IsValidKalturaResponse(httpResponseMessage))
                {
                    var result = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result.Contains("KalturaAPIException"))
                        {
                            var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                            throw kalturaAPIException;
                        }

                        var kalturaUploadToken = JsonConvert.DeserializeObject<KalturaUploadTokenModel>(result);
                        return kalturaUploadToken;
                    }
                    throw new InvalidOperationException("Upload could not be finished. Response is empty");
                }
                throw new InvalidOperationException($"Upload could not be finished. {httpResponseMessage.ReasonPhrase}");
            }
            throw new InvalidOperationException("Upload could not be finished. No file");
        }

        private async Task<KalturaMediaEntryModel> GetMediaEntryAsync(string name, string description, KalturaMediaType type, CancellationToken cancellationToken, string parentMediaEntryId = null, string tags = null)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                var httpClient = new HttpClient();

                string requestUrl = $"{ServiceBaseUrl}/service/media/action/add?format=1&ks={_kalturaInfo.token}&entry:name={Uri.EscapeDataString(name)}&entry:description={Uri.EscapeDataString(description)}&entry:mediaType={(int)type}";
                if (!string.IsNullOrEmpty(parentMediaEntryId))
                {
                    requestUrl += $"&entry:parentEntryId={parentMediaEntryId}";
                }

                if (!string.IsNullOrEmpty(tags))
                {
                    requestUrl += $"&entry:tags={tags}";
                }

                var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), new HttpStringContent("")).AsTask(cancellationToken);
                if (await IsValidKalturaResponse(httpResponseMessage))
                {
                    var result = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result.Contains("KalturaAPIException"))
                        {
                            var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                            throw kalturaAPIException;
                        }

                        var kalturaMediaEntry = JsonConvert.DeserializeObject<KalturaMediaEntryModel>(result);

                        return kalturaMediaEntry;
                    }
                    throw new InvalidOperationException("Media entry could not be created. Response is empty");
                }
                throw new InvalidOperationException($"Media entry could not be created. {httpResponseMessage.ReasonPhrase}");
            }
            throw new InvalidOperationException("Media entry could not be created. Name or Description is empty");
        }

        private async Task<KalturaMediaEntryModel> AttachVideoAsync(string uploadTokenId, string mediaEntryId, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrWhiteSpace(uploadTokenId) && !string.IsNullOrWhiteSpace(mediaEntryId))
            {
                var httpClient = new HttpClient();
                string requestUrl = $"{ServiceBaseUrl}/service/media/action/addContent?format=1&ks={_kalturaInfo.token}&resource:objectType=KalturaUploadedFileTokenResource&resource:token={uploadTokenId}&entryId={mediaEntryId}";

                var httpResponseMessage = await httpClient.PostAsync(new Uri(requestUrl), new HttpStringContent("")).AsTask(cancellationToken);
                if (await IsValidKalturaResponse(httpResponseMessage))
                {
                    var result = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result.Contains("KalturaAPIException"))
                        {
                            var kalturaAPIException = JsonConvert.DeserializeObject<KalturaApiException>(result);
                            throw kalturaAPIException;
                        }

                        var kalturaMediaEntry = JsonConvert.DeserializeObject<KalturaMediaEntryModel>(result);

                        return kalturaMediaEntry;
                    }
                    throw new InvalidOperationException("Video could not be attached to Media entry. Response is empty");
                }
                throw new InvalidOperationException($"Video could not be attached to Media entry. {httpResponseMessage.ReasonPhrase}");
            }
            throw new InvalidOperationException("Video could not be attached to Media entry. Upload token or Media entry is empty");
        }

        private Stream GetStreamFromString(string sourceString)
        {
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);
            streamWriter.Write(sourceString);
            streamWriter.Flush();
            memoryStream.Position = 0;

            return memoryStream;
        }

        private async Task<bool> IsValidKalturaResponse(HttpResponseMessage response)
        {
            string responseString = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                KalturaApiException errorData = ApiResponseError(responseString);
                if (errorData == null)
                {
                    return true;
                }
                else if (!String.IsNullOrEmpty(errorData.Message) &&
                         !String.IsNullOrEmpty(errorData.Code))
                {
                        throw errorData;
                }
            }
            else
            {
                //    throw new KalturaApiException("Error Code" +
                //        response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
            return true;
        }

        private KalturaApiException ApiResponseError(String response)
        {
            KalturaApiException errorData = null;
            try
            {
                errorData = JsonConvert.DeserializeObject<KalturaApiException>(response);
            }
            catch (Exception)
            {

            }
            return errorData;
        }
        #endregion
    }
}
