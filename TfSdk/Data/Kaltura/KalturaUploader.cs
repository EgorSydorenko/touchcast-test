﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TfSdk.Data.Kaltura
{
    public class KalturaUploader : IUploader
    {
        private const string InvalidSessionExceptionCode = "INVALID_KS";
        
        private IUploaderState _state;
        private Exception _lastError;
        //private IUncompressedTouchcastInfo _itemInfo;
        private WeakReference<ApiClient> _apiClient;
        private KalturaService _kalturaService;
        private CancellationTokenSource _cancellationTokenSource;
        public ApiClient ApiClient
        {
            get
            {
                ApiClient client;
                if (_apiClient.TryGetTarget(out client))
                {
                    return client;
                }
                return null;
            }
        }

        public IUploaderState State
        {
            private set
            {
                _state = value;
                switch (_state)
                {
                    case IUploaderState.Completed:
                        UploadingCompletedEvent?.Invoke(this);
                        break;
                    case IUploaderState.Failed:
                        if (_lastError is Models.KalturaApiException)
                        {
                            var lastError = _lastError as Models.KalturaApiException;
                            if (lastError.Code == InvalidSessionExceptionCode)
                            {
                                ApiClient?.ExpireSession(null);
                            }
                        }
                        UploadingFailedEvent?.Invoke(this, _lastError);
                        break;
                }
            }
            get => _state;
        }

        public event UploadingStarted UploadingStartedEvent;
        public event UploadingProgress UploadingProgressEvent;
        public event UploadingCompleted UploadingCompletedEvent;
        public event UploadingFailed UploadingFailedEvent;
        public event UploadingDataRecived UploadingDataRecivedEvent;
        public event UploadingFileIdRecived UploadingFileIdRecivedEvent;

        public KalturaUploader(ApiClient client)
        {
            _apiClient = new WeakReference<ApiClient>(client);
            _kalturaService = new KalturaService(client.UserInfo.kalturaTokenInfo);
        }

        public Task Cancel()
        {
            State = IUploaderState.Canceled;
            _cancellationTokenSource.Cancel();

            return Task.CompletedTask;
        }

        public Task UpdateFileInfo(string name, string description)
        {
            // Do nothing for kaltura
            return Task.CompletedTask;
        }

        public async Task Upload(TouchcastInfo wrapper)
        {
            if (wrapper.UncompressedInfo != null)
            {
                try
                {
                    _kalturaService.KalturaUploaderShareUrlReadyEvent += KalturaService_KalturaUploaderShareUrlReadyEvent;
                    _cancellationTokenSource = new CancellationTokenSource();
                    UploadingStartedEvent?.Invoke(this);
                    State = IUploaderState.Uploading;
                    await _kalturaService.UploadVideoAsync(false, wrapper.UncompressedInfo, (int value)
                        => 
                    {
                        UploadingProgressEvent?.Invoke(this, value);
                    }, _cancellationTokenSource.Token);
                    State = IUploaderState.Completed;
                }
                catch(Exception ex)
                {
                    _lastError = ex;
                    State = IUploaderState.Failed;
                }
                _kalturaService.KalturaUploaderShareUrlReadyEvent -= KalturaService_KalturaUploaderShareUrlReadyEvent;
            }
            else
            {
                throw new Exception("Wrong Wrapper Type");
            }
        }

        private void KalturaService_KalturaUploaderShareUrlReadyEvent(object sender, string url)
        {
            UploadingDataRecivedEvent?.Invoke(this, IUploaderConstants.LinkKey, url);
        }

        public List<string> GetSharableOptions()
        {
            throw new NotImplementedException();
        }
    }
}
