namespace TfSdk.Data.Kaltura.Models
{
    public class KalturaAssetBaseModel: KalturaBaseModel
    {
        public string EntryId { get; set; }
        public int Size { get; set; }
        public string Tags { get; set; }
        public string FileExt { get; set; }
        public string Description { get; set; }
    }
}