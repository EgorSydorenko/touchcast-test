﻿namespace TfSdk.Data.Kaltura.Models
{
    public abstract class KalturaBaseModel
    {
        public string Id { get; set; }
    }
}