﻿namespace TfSdk.Data.Kaltura.Models
{
    public class PerformanceSearchResultResponse : BaseInnerResponse
    {
        public Performances performances { get; set; }
    }

    public class Performances
    {
        public Attributes _attrs { get; set; }
        public Performance performance { get; set; }
    }

    public class Attributes
    {
        public int offset { get; set; }
        public int count { get; set; }
        public string total { get; set; }
    }

    public class Performance
    {
        public Attributes1 _attrs { get; set; }
        public string performance_url { get; set; }
        public Video video { get; set; }
        public object[] formats { get; set; }
        public string base_url { get; set; }
        public string actions_stream { get; set; }
        public int _private { get; set; }
        public Meta meta { get; set; }
    }

    public class Attributes1
    {
        public int id { get; set; }
    }

    public class Video
    {
        public Attributes2 _attrs { get; set; }
        public string value { get; set; }
    }

    public class Attributes2
    {
        public string src { get; set; }
    }

    public class Meta
    {
        public int timestamp { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public object[] aspect { get; set; }
        public object[] type { get; set; }
        public object[] tags { get; set; }
        public string number_of_views { get; set; }
        public string number_of_shares { get; set; }
        public float duration { get; set; }
        public string times_bookmarked { get; set; }
        public string times_recasted { get; set; }
        public int comments_amount { get; set; }
        public User user { get; set; }
        public string thumbnail { get; set; }
        public string cropped_thumbnail { get; set; }
        public string bookmarked { get; set; }
        public string flagged_as_inapproproate { get; set; }
        public string recasted { get; set; }
    }
}
