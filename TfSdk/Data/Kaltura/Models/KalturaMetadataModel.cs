namespace TfSdk.Data.Kaltura.Models
{

    public class KalturaMetadataModel : KalturaBaseModel
    {
		public string ObjectId { get; set; }
		public int Version { get; set; }
		public KalturaMetadataStatus Status { get; set; }
		public string Xml { get; set; }
	}
}

