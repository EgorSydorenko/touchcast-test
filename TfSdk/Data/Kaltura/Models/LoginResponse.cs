﻿namespace TfSdk.Data.Kaltura.Models
{
    public class LoginResponse:BaseInnerResponse
    {
        public Data data { get; set; }
    }

    public class Data
    {
        public string session_id { get; set; }
        public User user { get; set; }
    }

    public class User
    {
        public _Attrs _attrs { get; set; }
        public string homepage { get; set; }
        public object url { get; set; }
        public string channel_name { get; set; }
        public object description { get; set; }
        public object twitter { get; set; }
        public string image_url { get; set; }
        public int subscribed { get; set; }
        public string performances_uploaded { get; set; }
        public int performances_recasted { get; set; }
        public int subscribers { get; set; }
        public string total_subscribed { get; set; }
        public string email { get; set; }
        public int total_time_available { get; set; }
        public object time_used { get; set; }
    }

    public class _Attrs
    {
        public int user_id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }
}
