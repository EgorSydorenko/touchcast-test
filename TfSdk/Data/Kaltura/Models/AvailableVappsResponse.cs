﻿namespace TfSdk.Data.Kaltura.Models
{
    public class AvailableVappsResponse : BaseInnerResponse
    {
        public Response1 response { get; set; }
    }

    public class Response1
    {
        public Widgets widgets { get; set; }
    }

    public class Widgets
    {
        public Widget[] widget { get; set; }
    }

    public class Widget
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string theme { get; set; }
        public string url { get; set; }
        public Initial_Size initial_size { get; set; }
        public string preparation_required { get; set; }
        public string thumbnail { get; set; }
        public string thumbnail_2x { get; set; }
        public int shadow_size { get; set; }
        public string active_on_playback { get; set; }
    }

    public class Initial_Size
    {
        public string width { get; set; }
        public string height { get; set; }
    }
}
