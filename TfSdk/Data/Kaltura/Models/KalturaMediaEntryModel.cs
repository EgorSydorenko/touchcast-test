﻿namespace TfSdk.Data.Kaltura.Models
{
    public class KalturaMediaEntryModel: KalturaBaseModel
    {
        public KalturaMediaType MediaType { get; set; }
        public string SourceType { get; set; }
        public string DataUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public KalturaEntryModerationStatus ModerationStatus { get; set; }
        public string Type { get; set; }
        public string DownloadUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public int AccessControlId { get; set; }
        public KalturaEntryReplacementStatus ReplacementStatus { get; set; }
        public string ExternalURL { set; get; }
        public string EmbedCode { set; get; }
    }
}
