﻿namespace TfSdk.Data.Kaltura.Models
{
    public class AwsAccessResponse : BaseInnerResponse
    {
        public AwsCredentials credentials { get; set; }
        public string bucket { get; set; }
        public string prefix { get; set; }
        public long timestamp { get; set; }
    }

    public class AwsCredentials
    {
        public string access_key { get; set; }
        public string access_secret { get; set; }
        public string session_token { get; set; }
    }
}
