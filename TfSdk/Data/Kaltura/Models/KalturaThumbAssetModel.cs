namespace TfSdk.Data.Kaltura.Models
{
    public class KalturaThumbAssetModel : KalturaAssetBaseModel
    {
		public KalturaThumbAssetStatus Status { get; set; }
	}
}

