﻿namespace TfSdk.Data.Kaltura.Models
{
    public class RootResponse<T>
        where T : BaseInnerResponse
    {
        public T response { get; set; }
    }

    public class BaseInnerResponse
    {
        public Failure failure { get; set; }
        public object[] success { get; set; }
    }

    public class Failure
    {
        public int code { get; set; }
        public string description { get; set; }
    }
}
