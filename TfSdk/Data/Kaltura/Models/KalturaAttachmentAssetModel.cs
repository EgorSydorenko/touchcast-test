namespace TfSdk.Data.Kaltura.Models
{
    public class KalturaAttachmentAssetModel : KalturaAssetBaseModel
    {
        public string FileName { get; set; }
		public KalturaAttachmentFormat Format { get; set; }
		public KalturaAttachmentAssetStatus Status { get; set; }
    }
}

