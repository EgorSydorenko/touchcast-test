﻿namespace TfSdk.Data.Kaltura.Models
{
    public class PerformanceResponse : BaseInnerResponse
    {
        public int performance_id { get; set; }
    }
}
