namespace TfSdk.Data.Kaltura.Models
{
    public enum KalturaEntryReplacementStatus
	{
		None = 0,
		ApprovedButNotReady = 1,
		ReadyButNotApproved = 2, 
		NotReadyAndNotApproved = 3,
		Failed = 4
	}
}
