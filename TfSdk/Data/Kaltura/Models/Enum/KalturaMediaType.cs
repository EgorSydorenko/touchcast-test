﻿namespace TfSdk.Data.Kaltura.Models
{
    public enum KalturaMediaType
    {
        Video = 1,
        Image = 2,
        Audio = 5,
        LiveStreamFlash = 201,
        LiveStreamWindowsMedia = 202,
        LiveStreamRealMedia = 203,
        LiveStreamQuicktime = 204
    }
}