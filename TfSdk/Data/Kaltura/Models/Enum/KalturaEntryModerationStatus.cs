namespace TfSdk.Data.Kaltura.Models
{
	public enum KalturaEntryModerationStatus
	{
		PendingModeration = 1,
		Approved = 2,
		Rejected = 3,
		FlaggedForReview = 5,
		AutoApproved = 6
	}
}
