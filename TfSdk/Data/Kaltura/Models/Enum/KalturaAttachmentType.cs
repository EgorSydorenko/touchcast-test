namespace TfSdk.Data.Kaltura.Models
{
	public enum KalturaAttachmentType
	{
		Text = 1,
		Media = 2,
		Document = 3
	}
}
