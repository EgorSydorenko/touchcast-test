﻿namespace TfSdk.Data.Kaltura.Models
{
    public enum KalturaUploadTokenStatus
    {
        Pending = 0,
        PartialUpload = 1,
        FullUpload = 2,
        Closed = 3,
        TimedOut = 4,
        Deleted = 5
    }
}