namespace TfSdk.Data.Kaltura.Models
{
	public sealed class KalturaSourceType
	{
		public const string LimelightLive = "limeLight.LIVE_STREAM";
		public const string VelocixLive = "velocix.VELOCIX_LIVE";
		public const string File = "1";
		public const string Webcam = "2";
		public const string Url = "5";
		public const string SearchProvider = "6";
		public const string AkamaiLive = "29";
		public const string ManualLiveStream = "30";
		public const string AkamaiUniversalLive = "31";
		public const string LiveStream = "32";
		public const string LiveChannel = "33";
		public const string RecordedLive = "34";
		public const string Clip = "35";
		public const string LiveStreamOntextdataCaptions = "42";
	}
}
