namespace TfSdk.Data.Kaltura.Models
{
    public sealed class KalturaEntryStatus
    {
        public const string ErrorImporting = "-2";
        public const string ErrorConverting = "-1";
        public const string ScanFailure = "virusScan.ScanFailure";
        public const string Import = "0";
        public const string Infected = "virusScan.Infected";
        public const string Preconvert = "1";
        public const string Ready = "2";
        public const string Deleted = "3";
        public const string Pending = "4";
        public const string Moderate = "5";
        public const string Blocked = "6";
        public const string NoContent = "7";
    }
}
