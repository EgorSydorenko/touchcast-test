namespace TfSdk.Data.Kaltura.Models
{
	public enum KalturaAttachmentAssetStatus
	{
		Error = -1,
		Queued = 0,
		Ready = 2,
		Deleted = 3,
		Importing = 7,
		Exporting = 9
	}
}
