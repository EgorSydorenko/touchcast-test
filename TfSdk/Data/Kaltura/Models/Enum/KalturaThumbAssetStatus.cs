namespace TfSdk.Data.Kaltura.Models
{
	public enum KalturaThumbAssetStatus
	{
		Error = -1,
		Queued = 0,
		Capturing = 1,
		Ready = 2,
		Deleted = 3,
		Importing = 7,
		Exporting = 9
	}
}
