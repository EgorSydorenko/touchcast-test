namespace TfSdk.Data.Kaltura.Models
{
    public sealed class KalturaEntryType
	{
		public const string Automatic = "-1";
		public const string ExternalMedia = "externalMedia.externalMedia";
		public const string MediaClip = "1";
		public const string Mix = "2";
		public const string Playlist = "5";
		public const string Data = "6";
		public const string LiveStream = "7";
		public const string LiveChannel = "8";
		public const string Document = "10";
	}
}
