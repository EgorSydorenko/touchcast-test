﻿namespace TfSdk.Data.Kaltura.Models
{
    public enum KalturaAttachmentFormat
    {
        Text = 1,
        Media = 2,
        Document = 3
    }
}