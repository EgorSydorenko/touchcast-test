namespace TfSdk.Data.Kaltura.Models
{
	public enum KalturaMetadataStatus
	{
		Valid = 1,
		Invalid = 2,
		Deleted = 3
	}
}
