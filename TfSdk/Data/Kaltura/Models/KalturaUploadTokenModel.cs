﻿
 namespace TfSdk.Data.Kaltura.Models
{
    public class KalturaUploadTokenModel: KalturaBaseModel
    {
        public KalturaUploadTokenStatus Status { get; set; }
        public int UploadedFileSize { get; set; }
        public string FileName { get; set; }
    }
}
