﻿namespace TfSdk.Data.Kaltura
{
    using System;
    using System.Linq;

    public class SessionInfoModel
    {
        public SessionInfoModel() {}

        public SessionInfoModel(string queryString)
        {
            if (!string.IsNullOrEmpty(queryString))
            {
                foreach (var param in queryString.Split('&').Select(p => p.Split('=')))
                {
                    switch (param[0])
                    {
                        case "ks":
                            //var t = System.Net.WebUtility.UrlEncode(param[1]);
                            //var t2 = System.Net.WebUtility.UrlDecode(param[1]);

                            SessionId = Uri.UnescapeDataString(param[1]);
                            break;
                        case "partner_id":
                            PartnerId = param[1];
                            break;
                        case "user_id":
                            UserId = Uri.UnescapeDataString(param[1]);
                            break;
                        case "profile_id":
                            ProfileId = param[1];
                            break;
                        case "tc_partner_id":
                            TcPartnerId = param[1];
                            break;
                        default:
                            throw new ArgumentException($"Login query string contains unexpected parameter: {param[0]}");
                    }
                }

                if (string.IsNullOrEmpty(SessionId) || string.IsNullOrEmpty(PartnerId) || string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(ProfileId) || string.IsNullOrEmpty(TcPartnerId))
                {
                    throw new ArgumentException("Login query string doesn't contain requred parameters");
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(queryString));
            }
        }

        public string SessionId { get; set; }
        public string PartnerId { get; set; }
        public string UserId { get; set; }
        public string ProfileId { get; set; }
        public string TcPartnerId { get; set; }
    }
}