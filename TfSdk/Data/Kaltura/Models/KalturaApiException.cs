﻿using System;
using System.Collections.Generic;

namespace TfSdk.Data.Kaltura.Models
{
    using Newtonsoft.Json;

    public class KalturaApiException: Exception
    {
        [JsonProperty("message")]
#pragma warning disable 649
        private string _message;
#pragma warning restore 649

        public override string Message => _message;
        public string Code { get; set; }
        public string ObjectType { get; set; }
        public Dictionary<string, string> Args { get; set; }
    }
}
