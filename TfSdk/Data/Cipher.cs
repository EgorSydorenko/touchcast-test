﻿using System;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace TfSdk.Data
{
    internal class Cipher
    {
        private const string Pswd = "#$^@*$)@_$*^__tcp";
        private static CryptographicKey _key;
        private static IBuffer _iv;

        static Cipher()
        {
            //
            // Summary:
            //     Create a cryptographic key and an initialization vector.
            //
            SymmetricKeyAlgorithmProvider algorithmProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);

            // An initialization vector must be the same size as an algorithm provider BlockLength
            _iv = CryptographicBuffer.ConvertStringToBinary(new String('$', (int) algorithmProvider.BlockLength), BinaryStringEncoding.Utf8);

            var buffUtf8 = CryptographicBuffer.ConvertStringToBinary(Pswd, BinaryStringEncoding.Utf8);
            var buffHash = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5).HashData(buffUtf8);
            _key = algorithmProvider.CreateSymmetricKey(buffHash);
        }

        public static string Encryption(string toEncrypt)
        {
            IBuffer buffUtf8 = CryptographicBuffer.ConvertStringToBinary(toEncrypt, BinaryStringEncoding.Utf8);
            var buffEncrypt = CryptographicEngine.Encrypt(_key, buffUtf8, _iv);
            return CryptographicBuffer.EncodeToBase64String(buffEncrypt);
        }

        public static string Decryption(string str)
        {
            string result = null;
            if (!String.IsNullOrEmpty(str))
            {
                try
                {
                    var buffDecrypted = CryptographicEngine.Decrypt(_key, CryptographicBuffer.DecodeFromBase64String(str), _iv);
                    result = CryptographicBuffer.ConvertBinaryToString(BinaryStringEncoding.Utf8, buffDecrypted);
                }
                catch
                {
                    // possible CRC check exception
                }
            }
            return result;
        }
    }
}
