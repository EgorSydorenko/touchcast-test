﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TfSdk.UI
{
    public sealed partial class SignInDialog : ContentDialog
    {
        public delegate void SignInCanceled();
        public event SignInCanceled SignInCanceledEvent;

        public bool CanBeClosed
        {
            get; private set;
        }

        #region Life Cycle
        
        public SignInDialog(Uri loginEndPoint, Uri redirectEndpoint, bool canBeClosed)
        {
            InitializeComponent();
            CanBeClosed = canBeClosed;
            DataContext = this;
            Loaded += SiginDialog_Loaded;

            if (Resources["FabricContentDialogStyle"] is Style fabricContentDialogStyle)
            {
                Style = fabricContentDialogStyle;
            }
            SignIn.UpdateConfiguration(loginEndPoint, redirectEndpoint);
        }

#if DEBUG
        ~SignInDialog()
        {
            System.Diagnostics.Debug.WriteLine("****************** SignInDialog Destructor *******************");
        }
#endif
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Colors.Transparent);
                }
            }
        }

        private void SiginDialog_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += SignInDialog_Unloaded;
            SignIn.SignIn();
        }

        private void SignInDialog_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= SiginDialog_Loaded;
            Unloaded -= SignInDialog_Unloaded;
            DataContext = null;
        }
        #endregion

        #region Callbacks
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SignInCanceledEvent?.Invoke();
        }
        #endregion
    }
}
