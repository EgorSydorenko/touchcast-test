﻿using CloudUpload.UI.Helper;
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace TfSdk.UI
{
    public sealed partial class SignInControl : UserControl
    {
        #region Constants
        private const int LoadingStartValue = 5;
        private const int LoadingFinalizeValue = 60;
        private const int Tick = 1;
        private const string AccessTokenKeyWork = "access_token";
        private const string GroupUniquePath = "group_unique_path";
        private const string Error = "error";
        private const string _redirectParam = "redirect_uri";
        #endregion

        #region Private Fields

        private Uri _signInUrl;
        private Uri _redirectUrl;

        #region Progress
        private Storyboard _progressStoryboard = new Storyboard();
        private DoubleAnimation _progressAnimation = new DoubleAnimation();
        private Duration _progressDuration = new Duration(TimeSpan.FromSeconds(60));
        private bool _animationStarted;
        #endregion

        #endregion

        #region Events

        public delegate Task SignInComplete(string token, string groupUniquePath, string error);
        public delegate void ReadyToLogin();
        public delegate void NoInternetConnection();
        public event SignInComplete SignInCompletedEvent;
        public event ReadyToLogin ReadyToLoginEvent;
        public event NoInternetConnection NoInternetConnectionEvent;

        #endregion

        #region Life Cycle
        public SignInControl()
        {
            InitializeComponent();
            Loaded += SignInControl_Loaded;

            LoadProgressBar.Minimum = 0;
            LoadProgressBar.Maximum = LoadingFinalizeValue;
            LoadProgressBar.Value = 0;

            Storyboard.SetTarget(_progressAnimation, LoadProgressBar);
            Storyboard.SetTargetProperty(_progressAnimation, "ProgressBar.Value");
            _progressAnimation.From = LoadingStartValue;
            _progressAnimation.To = LoadingFinalizeValue;
            _progressAnimation.Duration = _progressDuration;
            _progressAnimation.EnableDependentAnimation = true;

            _progressStoryboard.Children.Add(_progressAnimation);
            _progressStoryboard.FillBehavior = FillBehavior.HoldEnd;

            ApiClient.Shared.UpdateUIControl(this);
        }

#if DEBUG
        ~SignInControl()
        {
            System.Diagnostics.Debug.WriteLine("****************** SignInControl Destructor *******************");
        }
#endif
        private void SignInControl_Loaded(object sender, RoutedEventArgs e)
        {
            HttpHelper.ClearCookies(_signInUrl);

            Unloaded += SignInControl_Unloaded;
            _progressStoryboard.Completed += ProgressStoryboard_Completed;
        }

        private void SignInControl_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= SignInControl_Loaded;
            Unloaded -= SignInControl_Unloaded;
            UnsubscribeFromWebView();
            _progressStoryboard.Completed -= ProgressStoryboard_Completed;
        }

        private void SignInView_Loaded(object sender, RoutedEventArgs e)
        {
            //SignIn();
        }
        #endregion

        #region Public Methods
        public void UpdateConfiguration(Uri loginEndPointUrl, Uri redirectUrl)
        {
            _signInUrl = loginEndPointUrl;
            _redirectUrl = redirectUrl;
        }

        public void SignIn()
        {
            ApiClient.Shared.AuthorizingCondition();
            SubscribeToWebView();
            SignInView.Navigate(new Uri(String.Format(_signInUrl.ToString())));
        }

        public void Cancel()
        {
            ApiClient.Shared.FailedCondition();
        }

        #endregion

        #region Callbacks
        private void SignInView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            EndProgress(args.Uri);
        }

        private void SignInView_NavigationFailed(object sender, WebViewNavigationFailedEventArgs e)
        {
            UnsubscribeFromWebView();
            NoInternetConnectionEvent?.Invoke();
        }

        private void SignInView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            BeginProgress();
        }

        //Implement redirect event
        private async void SignInView_UnsupportedUriSchemeIdentified(WebView sender, WebViewUnsupportedUriSchemeIdentifiedEventArgs args)
        {
            if (args.Uri != null)
            {
                var str = Uri.EscapeUriString(args.Uri.ToString());
                if (str.StartsWith(_redirectUrl.OriginalString))
                {
                    args.Handled = true;
                    await CheckRedirection(args.Uri);
                }
            }
        }

        private async void ProgressStoryboard_Completed(object sender, object e)
        {
            await LoadProgressBar.Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(Tick));
                LoadProgressBar.Visibility = Visibility.Collapsed;
            });
        }

        #endregion

        #region Private Methods

        private async Task CheckRedirection(Uri url)
        {
            try
            {
                var groupPath = String.Empty;
                var token = String.Empty;
                var error = String.Empty;
                foreach (var param in url.Query.Split(new char[] { '&', '?' }).Select(p => p.Split('=')))
                {
                    switch (param[0])
                    {
                        case AccessTokenKeyWork:
                            token = param[1];
                            break;
                        case GroupUniquePath:
                            groupPath = param[1];
                            break;
                        case Error:
                            error = param[1];
                            break;

                    }
                }
                UnsubscribeFromWebView();
                if (SignInCompletedEvent != null)
                {
                    await SignInCompletedEvent.Invoke(token, groupPath, error);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        private void BeginProgress()
        {
            if (_animationStarted == false)
            {
                LoadProgressBar.Visibility = Visibility.Visible;
                _progressStoryboard.Begin();
                _animationStarted = true;
            }
        }

        private void EndProgress(Uri uri)
        {
            _progressStoryboard.SkipToFill();
            _animationStarted = false;
        }

        private void SubscribeToWebView()
        {
            SignInView.UnsupportedUriSchemeIdentified += SignInView_UnsupportedUriSchemeIdentified;
            SignInView.NavigationCompleted += SignInView_NavigationCompleted;
            SignInView.NavigationStarting += SignInView_NavigationStarting;
            SignInView.NavigationFailed += SignInView_NavigationFailed;
            SignInView.Loaded += SignInView_Loaded;
        }
        private void UnsubscribeFromWebView()
        {
            SignInView.UnsupportedUriSchemeIdentified -= SignInView_UnsupportedUriSchemeIdentified;
            SignInView.NavigationCompleted -= SignInView_NavigationCompleted;
            SignInView.NavigationStarting -= SignInView_NavigationStarting;
            SignInView.NavigationFailed -= SignInView_NavigationFailed;
            SignInView.Loaded -= SignInView_Loaded;
        }

        #endregion
    }
}
