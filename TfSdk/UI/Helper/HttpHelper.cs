﻿using System;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.Web.Http.Filters;

namespace CloudUpload.UI.Helper
{
    public static class HttpHelper
    {
        public static void ClearCookies(Uri uri)
        {
            var myFilter = new HttpBaseProtocolFilter();
            var cookieManager = myFilter.CookieManager;
            var myCookieJar = cookieManager.GetCookies(uri);
            foreach (var cookie in myCookieJar)
            {
                cookieManager.DeleteCookie(cookie);
            }
        }
        public static async Task ClearCache()
        {
            await WebView.ClearTemporaryWebDataAsync();
        }

    }
}
