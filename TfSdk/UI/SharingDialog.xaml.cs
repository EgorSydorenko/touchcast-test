﻿using CloudUpload.UI.Helper;
using System;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TfSdk.UI
{
    public sealed partial class SharingDialog : ContentDialog
    {
        #region Constants
        private const int LoadingStartValue = 5;
        private const int LoadingFinalizeValue = 60;
        private const int Tick = 1;
        #endregion

        #region Private Fields

        private Uri _sharingUrl;
        private string _fileId;
        private string _groupId;

        #region Progress
        private Storyboard _progressStoryboard = new Storyboard();
        private DoubleAnimation _progressAnimation = new DoubleAnimation();
        private Duration _progressDuration = new Duration(TimeSpan.FromSeconds(60));
        private bool _animationStarted;
        #endregion

        #endregion

        #region Life Cycle

        public SharingDialog(string sharingEndPoint, string fileId, string groupId)
        {
            InitializeComponent();
            _fileId = fileId;
            _groupId = groupId;

            var accessToken = ApiClient.Shared.UserInfo.token;
            var sharingUrl = $"{sharingEndPoint}&access_token={accessToken}&group={_groupId}&file_id={_fileId}";

            _sharingUrl = new Uri(sharingUrl);

            if (Resources["FabricContentDialogStyle"] is Style fabricContentDialogStyle)
            {
                Style = fabricContentDialogStyle;
            }

            Loaded += SharingDialog_Loaded;

            LoadProgressBar.Minimum = 0;
            LoadProgressBar.Maximum = LoadingFinalizeValue;
            LoadProgressBar.Value = 0;

            Storyboard.SetTarget(_progressAnimation, LoadProgressBar);
            Storyboard.SetTargetProperty(_progressAnimation, "ProgressBar.Value");
            _progressAnimation.From = LoadingStartValue;
            _progressAnimation.To = LoadingFinalizeValue;
            _progressAnimation.Duration = _progressDuration;
            _progressAnimation.EnableDependentAnimation = true;

            _progressStoryboard.Children.Add(_progressAnimation);
            _progressStoryboard.FillBehavior = FillBehavior.HoldEnd;
        }

#if DEBUG
        ~SharingDialog()
        {
            System.Diagnostics.Debug.WriteLine("****************** SharingDialog Destructor *******************");
        }
#endif

        private void SharingDialog_Loaded(object sender, RoutedEventArgs e)
        {
            HttpHelper.ClearCookies(_sharingUrl);

            Unloaded += SharingDialog_Unloaded;

            SharingView.Loaded += SharedView_Loaded;
            SharingView.NavigationStarting += SharingView_NavigationStarting;
            SharingView.NavigationCompleted += SharingView_NavigationCompleted;
            SharingView.NavigationFailed += SharingView_NavigationFailed;
            SharingView.UnsupportedUriSchemeIdentified += SharingView_UnsupportedUriSchemeIdentified;

            _progressStoryboard.Completed += _progressStoryboard_Completed;
        }

        private void SharingDialog_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= SharingDialog_Loaded;
            Unloaded -= SharingDialog_Unloaded;

            SharingView.Loaded -= SharedView_Loaded;
            SharingView.NavigationStarting -= SharingView_NavigationStarting;
            SharingView.NavigationCompleted -= SharingView_NavigationCompleted;
            SharingView.NavigationFailed -= SharingView_NavigationFailed;
            SharingView.UnsupportedUriSchemeIdentified -= SharingView_UnsupportedUriSchemeIdentified;
        }

        #endregion

        #region Override Methods

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Colors.Transparent);
                }
            }
        }

        #endregion

        #region Callbacks

        private void SharedView_Loaded(object sender, RoutedEventArgs e)
        {
            SharingView.Navigate(_sharingUrl);
        }

        private void SharingView_NavigationFailed(object sender, WebViewNavigationFailedEventArgs e)
        {
            SharingView.Navigate(new Uri("ms-appx-web:///CloudUpload/UI/Html Pages/ErrorPage.html"));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void SharingView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            BeginProgress();
        }

        private void SharingView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            EndProgress();
        }

        private void SharingView_UnsupportedUriSchemeIdentified(WebView sender, WebViewUnsupportedUriSchemeIdentifiedEventArgs args)
        {
            Hide();
            args.Handled = true;
        }

        private async void _progressStoryboard_Completed(object sender, object e)
        {
            await LoadProgressBar.Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(Tick));
                LoadProgressBar.Visibility = Visibility.Collapsed;
            });
        }

        #endregion

        #region Private Methods

        private void BeginProgress()
        {
            if (_animationStarted == false)
            {
                LoadProgressBar.Visibility = Visibility.Visible;
                _progressStoryboard.Begin();
                _animationStarted = true;
            }
        }

        private void EndProgress()
        {
            _progressStoryboard.SkipToFill();
            _animationStarted = false;
        }

        #endregion
    }
}
