﻿using CloudUpload.UI.Helper;
using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TfSdk.UI
{
    public sealed partial class UserProfileDialog : ContentDialog
    {
        private Uri _profileViewUrl;
        private bool _isLoadingError;

        #region Life Cycle

        public UserProfileDialog(string viewProfileEndpoint)
        {
            InitializeComponent();

            string accessToken = ApiClient.Shared.UserInfo.token;
            string userId = ApiClient.Shared.UserInfo.info.user_id;
            string profileUrl = $"{viewProfileEndpoint}&access_token={accessToken}&user_id={userId}";
            _profileViewUrl = new Uri(profileUrl);

            if (Resources["FabricContentDialogStyle"] is Style fabricContentDialogStyle)
            {
                Style = fabricContentDialogStyle;
            }
            Loaded += UserProfileDialog_Loaded;
            Unloaded += UserProfileDialog_Unloaded;
        }

#if DEBUG
        ~UserProfileDialog()
        {
            System.Diagnostics.Debug.WriteLine("****************** UserProfileDialog Destructor *******************");
        }
#endif

        private void UserProfileDialog_Loaded(object sender, RoutedEventArgs e)
        {
            HttpHelper.ClearCookies(_profileViewUrl);

            ProfileView.Loaded += ProfileView_Loaded;
            ProfileView.NavigationCompleted += ProfileView_NavigationCompleted;
            ProfileView.NavigationFailed += ProfileView_NavigationFailed;
        }

        private void UserProfileDialog_Unloaded(object sender, RoutedEventArgs e)
        {
            ProfileView.Loaded -= ProfileView_Loaded;
            ProfileView.NavigationCompleted -= ProfileView_NavigationCompleted;
            ProfileView.NavigationFailed -= ProfileView_NavigationFailed;
            Loaded -= UserProfileDialog_Loaded;
            Unloaded -= UserProfileDialog_Unloaded;
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Colors.Transparent);
                }
            }
        }

        #endregion

        #region Callbacks

        private void ProfileView_Loaded(object sender, RoutedEventArgs e)
        {
            ProfileView.Navigate(_profileViewUrl);
        }

        private void ProfileView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            if (_isLoadingError)
            {
                HideLoadingErrorMessage();
                _isLoadingError = false;
            }
        }

        private void ProfileView_NavigationFailed(object sender, WebViewNavigationFailedEventArgs e)
        {
            _isLoadingError = true;
            ShowLoadingErrorMessage();
        }

        private void NoConnection_TryToConnectEvent()
        {
            ProfileView.Refresh();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        #endregion

        #region Private Methods

        private void ShowLoadingErrorMessage()
        {
            WebViewPanel.Visibility = Visibility.Collapsed;
            NoConnection.Visibility = Visibility.Visible;
        }

        private void HideLoadingErrorMessage()
        {
            WebViewPanel.Visibility = Visibility.Visible;
            NoConnection.Visibility = Visibility.Collapsed;
        }

        #endregion
    }
}
