﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Pitch.Models;
using Windows.Storage;

namespace Pitch.Models
{
    public class FabricUploadingModel : TfSdk.Data.ICompressedTouchcastInfo
    {
        private StorageFile _file;
        private string _title;
        private string _descritpion;
        private string _extra;
        public StorageFile TctFile => _file;

        public string Title => _title;

        public string Description => _descritpion;

        public string Group => _extra;

        public FabricUploadingModel(StorageFile file, string title, string description, string extra = "")
        {
            _file = file;
            _title = title;
            _descritpion = description;
            _extra = extra;
        }
    }
    [XmlType(TypeName = "composition")]
    public class KalturaUploadingModel : TfSdk.Data.IUncompressedTouchcastInfo
    {
        //[XmlIgnore]
        private ActionsStream _actionStream;
        [XmlArray("commands")]
        [XmlArrayItem("cmd")]
        public List<Command> Commands => _actionStream.Commands;

        [XmlArray("events")]
        [XmlArrayItem("event")]
        public List<Event> Events => _actionStream.Events;

        [XmlIgnore]
        public StorageFile VideoFile { private set; get; }
        [XmlIgnore]
        public string Title { set; get; }
        [XmlIgnore]
        public string Description { set; get; }
        [XmlIgnore]
        public StorageFile VideoThumbnailFile { private set; get; }
        public KalturaUploadingModel() { }
        public KalturaUploadingModel(Touchcast touchcast)
        {
            _actionStream = touchcast.ActionsStream.Clone();
            VideoFile = touchcast.VideoFile;
            VideoThumbnailFile = touchcast.VideoThumbnailFile;
        }

        public string ActionsXmlToString()
        { 
            try
            {
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                var serializer = new XmlSerializer(typeof(KalturaUploadingModel));
                var settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.OmitXmlDeclaration = true;
                var stringxML = String.Empty;
                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    serializer.Serialize(writer, this, emptyNamepsaces);
                    stringxML = stream.ToString();
                }
                return stringxML;
            }
            catch(Exception)
            {

            }
            return String.Empty;
        }

        public async Task<List<Tuple<string, StorageFile>>> PrepareUploadingFiles()
        {
            var commands = _actionStream.Commands.Where(x => x.Type == CommandTypes.Close).ToList();
            var urls = new List<Tuple<string, string>>();
            foreach (var command in commands)
            {
                if (command.ThumbnailUrl != null && !urls.Where(t => t.Item2 == command.ThumbnailUrl).Any())
                {
                    urls.Add(new Tuple<string, string>("thumb", command.ThumbnailUrl));
                }
                if (command.Url != null && !urls.Where(t => t.Item2 == command.Url).Any())
                {
                    urls.Add(new Tuple<string, string>("data", command.Url));
                }
            }

            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Pitch.DataLayer.AuthoringSession>();
            var result = new List<Tuple<string, StorageFile>>();
            if (session != null)
            {
                var workingFolder = session.Project.WorkingFolder;
                foreach (var dynamicObj in urls)
                {
                    StorageFile file = null;
                    try
                    {
                        var fileName = dynamicObj.Item2.Replace($"{Touchcast.PerformancePlaceHolder}/", String.Empty).ToString();
                        file = await workingFolder.GetFileAsync(fileName);
                        result.Add(new Tuple<string, StorageFile>(dynamicObj.Item1, file));
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return result;
        }

        public void UpdateCommandByRecourceId(string id, string thumb, string url = "")
        {
            //throw new NotImplementedException();
        }

        public void UpdateCommandByUrl(string fileUrl, string url = "")
        {
            var commands = _actionStream.Commands.Where(c => (c.ThumbnailUrl != null && c.ThumbnailUrl.Contains(fileUrl))
            || (c.Url != null && c.Url.Contains(fileUrl))).ToList();
            foreach (var cmd in commands)
            {
                if (cmd.ThumbnailUrl != null && cmd.ThumbnailUrl.Contains(fileUrl))
                {
                    cmd.ThumbnailUrl = url;
                }

                if (cmd.Url != null && cmd.Url.Contains(fileUrl))
                {
                    cmd.Url = url;
                }
            }
        }
    }
}
