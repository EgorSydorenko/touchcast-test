﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Pitch.Models
{
    using Pitch.Helpers.Extensions;
    using Windows.Data.Xml.Dom;

    [XmlType(TypeName = "performance")]
    public class Performance
    {
        private XmlDocument _document = new XmlDocument();
        [XmlElement(ElementName = "meta")]
        public PerformanceMeta Metadata { get; set; }

        [XmlAttribute("id")]
        public string PerformanceId { get; set; }

        private XmlCDataSection _thumbnail;
        [XmlElement(ElementName = "thumbnail")]
        public String Thumbnail
        {
            get
            {
                return _thumbnail.GetXml();
            }
            set
            {
                string str = value.GetCdataContent();
                _thumbnail = _document.CreateCDataSection(str);
            }
        }

        [XmlIgnore]
        public String ThumbnailData
        {
            get { return _thumbnail.Data; }
        }

        /*XmlCDataSection*/
        private XmlCDataSection _thumbnailCropped;
        [XmlElement(ElementName = "thumbnail-cropped")]
        public String ThumbnailCropped
        {
            get
            {
                return _thumbnailCropped.GetXml();
            }
            set
            {
                string str = value.GetCdataContent();
                _thumbnailCropped = _document.CreateCDataSection(str);
            }
        }

        [XmlIgnore]
        public String ThumbnailCroppedData
        {
            get { return _thumbnailCropped.Data; }
        }

        private XmlCDataSection _actions;
        [XmlElement(ElementName = "actions")] 
        public String Actions
        {
            get
            {
                return _actions.GetXml();
            }
            set
            {
                string str = value.GetCdataContent();
                _actions = _document.CreateCDataSection(str);
            }
        }

        [XmlIgnore]
        public String ActionsData
        {
            get { return _actions.Data; }
        }

        [XmlAttribute("user")]
        public string UserId { get; set; }

        [XmlIgnore]
        private Video _video;

        [XmlElement("video")]
        public Video Video
        {
            get
            {
                return this._video;
            }
            set { this._video = value; }
        }

        [XmlArray("components")]
        [XmlArrayItem("component")]
        public List<Component> Components = new List<Component>();

        public Performance()
        {
            _thumbnail = _document.CreateCDataSection("($PERFORMANCE_BASE_URL)/thumbnail.png");
            _thumbnailCropped = _document.CreateCDataSection("($PERFORMANCE_BASE_URL)/thumbnail.png");
            _actions = _document.CreateCDataSection("($PERFORMANCE_BASE_URL)/actions.xml");
        }

    }

    public class Component
    {
        private XmlCDataSection _resource;
        [XmlText]
        public string Resource
        {
            set
            {
                string str = value.GetCdataContent();
                _resource = new XmlDocument().CreateCDataSection($"{str}");
            }
            get
            {
                if (_resource != null)
                    return _resource.GetXml();
                else
                    return null;
            }
        }

        [XmlIgnore]
        public string ResourceData
        {
            get
            {
                return _resource.Data;
            }
        }

        [XmlAttribute("type")]
        public string AppType { set; get; }
    }

    public class PerformanceMeta
    {
        private XmlCDataSection _description;
        [XmlElement(ElementName = "description")] 
        public String Description
        {
            get
            {
                return _description.GetXml();
            }

            set
            {
                string str = value.GetCdataContent();
                _description = new XmlDocument().CreateCDataSection($"{str}");
            }
        }

        [XmlIgnore]
        public string DescriptionData
        {
            get
            {
                return _description.Data;
            }
        }

        private XmlCDataSection _name;

        [XmlElement(ElementName = "name")]
        public String Name
        {
            get
            {
                return _name.GetXml();
            }

            set
            {
                string str = value.GetCdataContent();
                _name = new XmlDocument().CreateCDataSection($"{str}");
            }
        }

        [XmlIgnore]
        public string NameData
        {
            get
            {
                return _name.Data;
            }
        }

        public void SetMeta(string name, string description)
        {
            this.Description = description;
            this.Name = name;
        }
    }

    public class Video
    {
        [XmlAttribute("duration")]
        public string Duration { get; set; }

        [XmlIgnore]
        private XmlCDataSection _name;

        [XmlText]
        public string Name
        {
            get
            {
                return this._name.GetXml();
            }
            set
            {
                string str = value.GetCdataContent();
                this._name = new XmlDocument().CreateCDataSection(str);
            }
        }

        [XmlIgnore]
        public string NameData
        {
            get { return _name.Data; }
        }
    }
}
