﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Windows.Storage;
using Windows.UI.Xaml;
using Pitch.Helpers.Exceptions;
using Pitch.Helpers.Frames;
using Pitch.Helpers.Logger;

namespace Pitch.Models
{
    [XmlType(TypeName = "composition")]
    public class Touchcast
    {
        [XmlIgnore]  public const string PerformancePlaceHolder = "($PERFORMANCE_BASE_URL)";
        //[XmlIgnore]  private List<Command> _copyCommands { set; get; }
        [XmlIgnore]
        public string Title { set; get; }
        [XmlAttribute(AttributeName = "version")]
        public string version
        {
            set => ActionsStream.Version = value;
            get => ActionsStream.Version;
        }
        [XmlElement(ElementName = "original_video_size")]
        public VideoSize OriginalVideoSize
        {
            set => ActionsStream.OriginalVideoSize = value;
            get => ActionsStream.OriginalVideoSize;
        }
        [XmlArray("commands")]
        [XmlArrayItem("cmd")]
        public List<Command> Commands
        {
            set => ActionsStream.Commands = value;
            get => ActionsStream.Commands;
        }
        [XmlArray("events")]
        [XmlArrayItem("event")]
        public List<Event> Events
        {
            set => ActionsStream.Events = value;
            get => ActionsStream.Events;
        }

        [XmlIgnore]
        public string Description { set; get; }

        [XmlIgnore]
        public static Dictionary<string, Touchcast> Instances = new Dictionary<string, Touchcast>();
        [XmlIgnore]
        public static Touchcast Instance => Instances[(Window.Current.Content as TouchcastFrame).Id];
        [XmlIgnore]
        public StorageFile TouchcastFile { get; set; }
        [XmlIgnore]
        public StorageFile VideoFile { get; set; }

        [XmlIgnore]
        public StorageFile VideoThumbnailFile { get; set; }
        public string Extra => String.Empty;

        [XmlIgnore]
        public ActionsStream ActionsStream { get; set; }

        [XmlIgnore]
        public Performance Performance;

        [XmlIgnore]
        public bool isWithVideo { set; get; }

        public Touchcast()
        {
            ActionsStream = new ActionsStream();
            Performance = new Performance();
        }

        private string FromXml<T>(T instance)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(typeof(T));
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            var stringxML = String.Empty;
            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, instance, emptyNamepsaces);
                stringxML = stream.ToString();
                stringxML = stringxML.Replace("&lt;![CDATA", "<![CDATA").Replace("]&gt;", "]>");
            }
            return stringxML;
        }

        public void SetVideoInformation(StorageFile videoFile, StorageFile thumb, string Duration)
        {
            VideoFile = videoFile;
            VideoThumbnailFile = thumb;
            Performance.Video = new Video
            {
                Name = videoFile.Name,
                Duration = Duration
            };
        }

        public void Reset()
        {
            ActionsStream = new ActionsStream();
        }

        //public void UpdateCommandByRecourceId(string id, string thumb, string url = "")
        //{
        //    var commands = _copyCommands.Where(c => c.ResourceId == id).ToList();

        //    foreach (var command in commands)
        //    {
        //        if (!string.IsNullOrEmpty(url))
        //        {
        //            command.Url = url;
        //        }
        //        command.ThumbnailUrl = thumb;
        //    }
        //}

        public string ActionsXmlToString()
        {
            return FromXml(this);
        }
        
        public async Task<StorageFile> SaveArchive()
        {
            LogQueue.WriteToFile("SaveArchive");
            var guid = Guid.NewGuid().ToString();
            var globalArchiveFolder = await App.TemporaryFolder.CreateFolderAsync("archive", CreationCollisionOption.OpenIfExists);
            var archiveFolder = await globalArchiveFolder.CreateFolderAsync(guid, CreationCollisionOption.ReplaceExisting);
            var workFolder = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<DataLayer.AuthoringSession>().Project.WorkingFolder;
            var commands = ActionsStream.Commands.Where(x => x.Type == CommandTypes.Close).ToList();
            foreach (var command in commands)
            {
                var thumbnailUrl = command.ThumbnailUrl;
                var sourceUrl = command.Url;
                var thumbnailComponent = new Component();
                StorageFile thumbnailFile = null;
                if (!String.IsNullOrEmpty(thumbnailUrl))
                {
                    //thumbnailUrl = thumbnailUrl.Replace(_performancePlaceHolder, $"{workFolder.Path}");
                    thumbnailComponent.AppType = "thumbnail";
                    thumbnailComponent.Resource = thumbnailUrl;
                    try
                    {
                        thumbnailFile = await workFolder.GetFileAsync(thumbnailUrl.Replace($"{PerformancePlaceHolder}/", String.Empty));
                    }
                    catch(Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Touchcast.SaveArchive : {ex.Message}");
                    }
                }

                var sourceComponent = new Component();
                StorageFile sourceFile = null;
                if (!String.IsNullOrEmpty(sourceUrl))
                {
                    //thumbnailUrl = sourceUrl.Replace(_performancePlaceHolder, $"{workFolder.Path}");
                    thumbnailComponent.AppType = "app";
                    thumbnailComponent.Resource = sourceUrl;
                    try
                    {
                        sourceFile = await workFolder.GetFileAsync(sourceUrl.Replace($"{PerformancePlaceHolder}/", String.Empty));
                    }
                    catch { }

                }
                if (thumbnailFile != null)
                {
                    try
                    {
                        await thumbnailFile.CopyAsync(archiveFolder);
                    }
                    catch { }
                }

                if (sourceFile != null)
                {
                    try
                    {
                        await sourceFile.CopyAsync(archiveFolder);
                    }
                    catch { }
                }
                if (sourceComponent.Resource != null)
                {
                    Performance.Components.Add(sourceComponent);
                }
                if (thumbnailComponent.Resource != null)
                {
                    Performance.Components.Add(thumbnailComponent);
                }
            }

            var actionsXML = ActionsXmlToString();
            var serializer = new XmlSerializer(typeof(Performance));
            var stringWriter = new StringWriter();
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            var infoXmlString = String.Empty;
            Performance.Metadata = new PerformanceMeta();

            Performance.Metadata.SetMeta(String.IsNullOrEmpty(Title)? String.Empty : Title, String.IsNullOrEmpty(Description) ? String.Empty : Description);
            using (var writer = XmlWriter.Create(stringWriter, settings))
            {
                serializer.Serialize(writer, Performance);
                infoXmlString = stringWriter.ToString().Replace("&lt;![CDATA", "<![CDATA").Replace("]&gt;", "]>");
            }

            var infoFile = await archiveFolder.CreateFileAsync("info.xml");
            var actionsFile = await archiveFolder.CreateFileAsync("actions.xml");

            await FileIO.WriteTextAsync(actionsFile, actionsXML);
            await FileIO.WriteTextAsync(infoFile, infoXmlString);

            //if (isWithVideo)
            {
                await VideoFile.CopyAsync(archiveFolder);
                await VideoThumbnailFile.CopyAsync(archiveFolder);
            }
            try
            {
                var fileToRemove = await App.TemporaryFolder.GetFileAsync("archive.tct");

                if(fileToRemove != null)
                {
                    await fileToRemove.DeleteAsync();
                }
            }
            catch
            {

            }
            ZipFile.CreateFromDirectory(archiveFolder.Path, $"{App.TemporaryFolder.Path}/archive.tct", CompressionLevel.Fastest, false);
            var zip = await App.TemporaryFolder.GetFileAsync("archive.tct");

            await archiveFolder.DeleteAsync(StorageDeleteOption.PermanentDelete);

            return zip;
        }

        /// <summary>
        /// <?xml version="1.0" encoding="UTF-8"?>
        ///-<performance user = "" id="25668">
        ///-    <meta>
        ///         <ios_app_version>1.18</ios_app_version>
        ///-        <name>
        ///-            <![CDATA[Hnjnn]]>
        ///         </name>
        ///-        <description>
        ///-            <![CDATA[]]>
        ///         </description>
        ///         <type>regular</type>
        ///-        <aspect>
        ///-            <![CDATA[16:9]]>
        ///         </aspect>
        ///     </meta>
        ///-    <thumbnail>
        ///-        <![CDATA[($PERFORMANCE_BASE_URL)/thumbnail_25668.png]]>
        ///     </thumbnail>
        ///-    <thumbnail-cropped>
        ///-        <![CDATA[($PERFORMANCE_BASE_URL)/thumbnail-cropped_25668.png]]>
        ///     </thumbnail-cropped>
        ///-    <video duration = "11.000000" >
        ///-        <![CDATA[($PERFORMANCE_BASE_URL)/4E364D71-8304-4D41-99FC-A923954768B0.mp4]]>
        ///     </video>
        ///     -<actions>
        ///-        <![CDATA[($PERFORMANCE_BASE_URL)/actions.xml]]>
        ///     </actions>
        ///-    <components>
        ///-        <component>
        ///-            <![CDATA[($PERFORMANCE_BASE_URL)/444A4C6E-63BF-49CB-A9A4-7B606CA6641C.png]]>
        ///         </component>
        ///-        <component>
        ///-            <![CDATA[($PERFORMANCE_BASE_URL)/16D11AE6-7296-47E1-BC51-BB0E1577ED96.png]]>
        ///         </component>
        ///     </components>
        ///</performance>
        /// </summary>
        /// <returns></returns>

        //public static async Task<Tuple<Touchcast,string>> OpenArchive(StorageFile archive, string  exceptionMessage)
        public static async Task<Tuple<Touchcast, TouchcastException>> OpenArchive(StorageFile archive)
        {
            LogQueue.WriteToFile("OpenArchive");
            Touchcast instance = null;
            var globalTouchcastsFolder = await App.TemporaryFolder.CreateFolderAsync("Touchcasts", CreationCollisionOption.OpenIfExists);
            var touchcastFolder = await globalTouchcastsFolder.CreateFolderAsync(Guid.NewGuid().ToString());
            try
            {
                using (var stream = await archive.OpenReadAsync())
                {
                    ZipArchive zipArchive = new ZipArchive(stream.AsStream());
                    zipArchive.ExtractToDirectory(touchcastFolder.Path);
                }
            }
            catch (Exception exception)
            {
                return new Tuple<Touchcast, TouchcastException>(instance,
                    new TouchcastException(TouchcastExceptionType.TouchcastOpenFileException, exception));
            }

            var touchccastSerializer = new XmlSerializer(typeof(Touchcast));
            var performanceSerializer = new XmlSerializer(typeof(Performance));
            Performance performance;
            using (var infoStream = await (await touchcastFolder.GetFileAsync("info.xml")).OpenReadAsync())
            {
                performance = performanceSerializer.Deserialize(infoStream.AsStream()) as Performance;
            }
            try
            {
                using (var infoStream = await (await touchcastFolder.GetFileAsync(ReplacePerformancePlaceholderString(performance.ActionsData))).OpenReadAsync())
                {
                    instance = touchccastSerializer.Deserialize(infoStream.AsStream()) as Touchcast;
                }
                instance.Performance = performance;
                instance.Title = String.IsNullOrEmpty(instance.Performance.Metadata.NameData) ? archive.DisplayName : instance.Performance.Metadata.NameData;
                try
                {
                    instance.VideoFile = await touchcastFolder.GetFileAsync(ReplacePerformancePlaceholderString(performance.Video.NameData));
                    instance.VideoThumbnailFile = await touchcastFolder.GetFileAsync(ReplacePerformancePlaceholderString(performance.ThumbnailData));
                    instance.isWithVideo = true;
                }
                catch
                {
                    instance.isWithVideo = false;
                }
                var commandsToSerf = instance.ActionsStream.Commands.Where(x => x.Type == CommandTypes.Close).ToList();
            }
            catch(Exception exception)
            {
                return new Tuple<Touchcast, TouchcastException>(instance,
                   new TouchcastException(TouchcastExceptionType.TouchcastOpenFileException, exception));
            }
            return new Tuple<Touchcast, TouchcastException>(instance, null);
        }

        private static string ReplacePerformancePlaceholderString(string str)
        {
            return str.Replace($"{PerformancePlaceHolder}/", String.Empty);
        }
    }
}
