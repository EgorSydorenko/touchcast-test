﻿using Windows.Foundation;

namespace Pitch.Models
{
    public class InkSize
    {
        public string TextPartOne { set; get; }

        public string TextPartTwo { set; get; }

        public Size Size { get; set; }
    }
}
