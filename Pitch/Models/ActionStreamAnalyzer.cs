﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Foundation;
using Pitch.Helpers;

namespace Pitch.Models
{

    class ComandComparer : IEqualityComparer<Command>
    {
        public bool Equals(Command x, Command y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(Command obj)
        {
            return obj.Id.GetHashCode();
        }
    } 

    public class ActionStreamAnalyzer
    {
        #region Private Fields

        private readonly object _lockObject = new object();
        private List<Command> _actionStream;
        private double _currentTime;
        private List<Command> _currentlyOpenedVapps = new List<Command>();
        private List<Command> _allUniqueVapps;

        #endregion

        #region Public properties

        public double CurrentTime
        {
            set
            {
                _currentTime = value;
                RefreshCurrentlyOpenedVapps();

            }
            get { return _currentTime; }
        }

        public List<Command> ActionStream
        {
            set
            {
                _actionStream = value;
                _allUniqueVapps = _actionStream.Where(x => x.Type == CommandTypes.Create).Distinct(new ComandComparer()).ToList();
            }
            get
            {
                return _actionStream;
            }
        }

        public List<Command> CurrentlyOpenedVapps
        {
            get
            {
                lock (_lockObject)
                {
                    return _currentlyOpenedVapps;
                }
            }
        }

        public List<Command> AllUniqueVapps => _allUniqueVapps;

        #endregion

        #region Life Cycle

        public ActionStreamAnalyzer(List<Command> actionStream)
        {
            _actionStream = actionStream;
            _allUniqueVapps = _actionStream.Where(x=>x.Type == CommandTypes.Create).Distinct(new ComandComparer()).ToList();
        }

        public Command GetCommandByTimeAndPosition(TimeSpan time, Point position, Size size)
        {
            CurrentTime = time.TotalSeconds;
             var activeCommandVapps = from command in CurrentlyOpenedVapps orderby command.Time descending, command.Index descending select command;
            foreach (var vappCmd in activeCommandVapps)
            {
                var rect = TouchcastTransformHelper.CreateRectangleFromTransformString(vappCmd.Transform.Substring(1, vappCmd.Transform.Length - 2), size);
                if (rect.Contains(position))
                {
                    return vappCmd;
                }
            }
            return null;
        }

        #endregion

        #region Private properties

        private void RefreshCurrentlyOpenedVapps()
        {
            var currentlyOpenedVapps = new List<Command>();
            foreach (var cmd in _allUniqueVapps)
            {
                var currentVappCommands =
                    _actionStream.Where(c => c.Id == cmd.Id).OrderByDescending(com => com.Time).ToList();
                var createCommand = currentVappCommands.FirstOrDefault(c => c.Type == CommandTypes.Create);
                var closeCommand = currentVappCommands.FirstOrDefault(c => c.Type == CommandTypes.Close);
                
                if (createCommand != null && closeCommand != null && createCommand.Time <= _currentTime && closeCommand.Time >= _currentTime)
                {
                    var commands = from command in currentVappCommands where command.Time<= _currentTime orderby command.Time descending, command.Index descending select command;
                   if(commands.Any())
                    {
                        var lastActiveCommand = commands.FirstOrDefault();

                        if(lastActiveCommand != null)
                            currentlyOpenedVapps.Add(lastActiveCommand);
                    }
                }
                
            }

            lock (_lockObject)
            {
                _currentlyOpenedVapps.Clear();
                _currentlyOpenedVapps.AddRange(currentlyOpenedVapps.OrderByDescending(x=>x.Time));
            }
        }

        private bool CheckOpenedVappType(string type)
        {
            return (type == CommandTypes.Navigate || type == CommandTypes.Move);
        }

        #endregion
    }
}
