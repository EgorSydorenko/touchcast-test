﻿namespace Pitch.Models
{
    public enum InkShapeType
    {
        None,
        Highlite,
        Line,
        Arrow,
        Rectangle,
        Circle,
        Text
    }
}
