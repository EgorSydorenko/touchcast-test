﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pitch.Models
{
    public class AsyncLock
    {
        private readonly static Task SCompleted = Task.FromResult(true);
        private readonly Queue<TaskCompletionSource<bool>> _aWaiters = new Queue<TaskCompletionSource<bool>>();
        private int _count;

        public AsyncLock(int initialCount)
        {
            if (initialCount < 0) throw new Exception();
            _count = initialCount;
        }

        public Task WaitAsync()
        {
            lock (_aWaiters)
            {
                if (_count > 0)
                {
                    --_count;
                    return SCompleted;
                }
                else
                {
                    var waiter = new TaskCompletionSource<bool>();
                    _aWaiters.Enqueue(waiter);
                    return waiter.Task;
                }
            }
        }
        public void Release()
        {
            TaskCompletionSource<bool> toRelease = null;
            lock (_aWaiters)
            {
                if (_aWaiters.Count > 0)
                    toRelease = _aWaiters.Dequeue();
                else
                    ++_count;
            }
            if (toRelease != null)
                toRelease.SetResult(true);
        }
    }
}
