﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Pitch.Helpers.Extensions;
using Windows.Data.Xml.Dom;

namespace Pitch.Models
{
    public class VideoSize
    {
        [XmlElement(ElementName = "width")]
        public double Width { get; set; }

        [XmlElement(ElementName = "height")]
        public double Height { get; set; }
    }

    public class Command
    {
        public Command() { }
        private Command(Command c)
        {
            Type = c.Type;
            Id = c.Id;
            Time = c.Time;
            Widget = c.Widget;
            Url = c.Url;
            ThumbnailUrl = c.ThumbnailUrl;
            Transform = c.Transform;
            ContentType = c.ContentType;
            Title = c.Title;
            PauseOnInteration = c.PauseOnInteration;
            Index = c.Index;
            ResourceId = c.ResourceId;
            ClipId = c.ClipId;
            UrlProperties = c.UrlProperties;
        }

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public int Id { get; set; }

        [XmlAttribute(AttributeName = "time")]
        public double Time { get; set; }

        [XmlAttribute(AttributeName = "widget")]
        public string Widget { get; set; }

        [XmlAttribute(AttributeName = "url")]
        public string Url { get; set; }

        [XmlAttribute(AttributeName = "thumb")]
        public string ThumbnailUrl { get; set; }

        [XmlAttribute(AttributeName = "transform")]
        public string Transform { get; set; }


        [XmlAttribute(AttributeName = "content_type")]
        public string ContentType { get; set; }

        [XmlAttribute(AttributeName = "title")]
        public string Title { get; set; }

        [XmlAttribute(AttributeName = "url_properties")]
        public string UrlProperties { set; get; }
        // Skipping these for now

        //[XmlElement(ElementName = "thumb")]
        //public string ThumbnailUrl { get; set; }

        //[XmlElement(ElementName = "draw_system_border")]
        //public bool DrawSystemBorder { get; set; }

        //[XmlElement(ElementName = "live")]
        //public bool Live { get; set; }

        [XmlAttribute(AttributeName = "pause_video_on_interaction")]
        public string PauseOnInteration { get; set; }

        [XmlIgnore]
        public long Index { set; get; }
        [XmlIgnore]
        public string ResourceId { set; get; }

        [XmlIgnore]
        public string ClipId { set; get; }

        public Command Clone()
        {
            return new Command(this);
        }
    }

    public static class CommandTypes
    {
        public static string Create = "create";
        public static string Close = "close";
        public static string Navigate = "navigate";
        public static string Move = "move";
        public static string BringToFront = "bring_to_front";
        public static string PageChanged = "page_changed";
    }

    public class Summary
    {
        private XmlDocument _document = new XmlDocument();
        [XmlAttribute(AttributeName = "human_readable")]
        public string HumanReadable { get; set; }
        private XmlCDataSection _summary;

        [XmlText]
        public string Data
        {
            get => _summary?.GetXml();
            set => _summary = _document.CreateCDataSection(value.GetCdataContent());
        }
    }

    public class Event
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlAttribute(AttributeName = "time")]
        public double Time { get; set; }

        [XmlElement(ElementName = "summary")]
        public Summary Summary { get; set; }

        [XmlElement(ElementName = "contents")]
        public object Contents { get; set; }
        public Event Clone()
        {
            return new Event(this);
        }
        public Event()
        {

        }
        private Event(Event orig)
        {
            Type = orig.Type;
            Time = orig.Time;
            if (orig.Summary != null)
            {
                Summary = new Summary
                {
                    HumanReadable = orig.Summary.HumanReadable,
                    Data = orig.Summary.Data,
                };
            }
            Contents = orig.Contents;
        }
    }
    public static class EventTypes
    {
        public static string SceneChanged = "scene_changed";
        public static string TextAvailable = "text_available";
        public static string UserTalking = "user_talking";
        public static string UserStoppedTalking = "user_stopped_talking";
        public static string ObjectDetected = "object_detected";
    }

    public class ActionsStream
    {
        public string Version { set; get; }
        public VideoSize OriginalVideoSize { get; set; }
        public List<Command> Commands { get; set; }
        public List<Event> Events { get; set; }
        public ActionsStream()
        {
            Version = "1.3";
            Commands = new List<Command>();
            Events = new List<Event>();
        }
        private ActionsStream(ActionsStream orig)
        {
            Version = "1.3";

            Commands = new List<Command>();
            foreach (var com in orig.Commands)
            {
                Commands.Add(com.Clone());
            }

            Events = new List<Event>(orig.Events);
            foreach (var ev in orig.Events)
            {
                Events.Add(ev.Clone());
            }
        }

        public void Clear()
        {
            Commands.Clear();
            Events.Clear();
        }

        public ActionsStream Clone()
        {
            return new ActionsStream(this);
        }
    }
}
