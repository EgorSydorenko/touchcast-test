﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Media.Editing;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Pitch.DataLayer;
using Pitch.Helpers;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Player.Views
{
    public class TcCompositionSeekEventArgs
    {
        public TimeSpan Position { get; }

        public TcCompositionSeekEventArgs(TimeSpan position)
        {
            Position = position;
        }
    }

    public class ClipViewVisibleRegion
    {
        public VideoFragmentView View { get; set; }
        public Rect VisibleArea { get; set; }
    }

    public delegate void TcCompositionSeek(TcCompositionSeekEventArgs args);

    public sealed partial class MediaCompositionView
    {
        #region Static and constant values

        public static readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 1);
        public static readonly CoreCursor SizeWestEastCursor = new CoreCursor(CoreCursorType.SizeWestEast, 1);

        private const double ScrollBarHeight = 12;
        private readonly Point ZeroPoint = new Point(0, 0);
        #endregion

        #region private fields

        private Point _lastPoint;
        private TimeSpan _position = TimeSpan.Zero;
        private bool _isPressed;
        private CancellationToken _cancellationToken;
        private double _pixelPerSeconds = 0.0;
        private VideoFragmentView _selectedView;
        private ScrollViewer _scrollViewer;
        private GeneralTransform _currentViewGeneralTransform = null;
        private List<VideoFragmentView> _takeViews;
        private MediaComposition _mediaComposition;

        #endregion

        public event TcCompositionSeek TcCompositionSeekEvent;

        #region Life Cycle

        public MediaCompositionView(MediaComposition mediaComposition, CancellationToken cancellationToken, double pixelsPerSecond)
        {
            InitializeComponent();
            _pixelPerSeconds = pixelsPerSecond;
            Loaded += TcCompositionItemsView_Loaded;
            Unloaded += TcCompositionItemsView_Unloaded;
            _cancellationToken = cancellationToken;
            _mediaComposition = mediaComposition;
        }

#if DEBUG
        ~MediaCompositionView()
        {
            System.Diagnostics.Debug.WriteLine("******************** MediaCompositionView Destructor********************");
        }
#endif

        public async Task Cleanup()
        {
            if (_takeViews != null)
            {
                foreach (var item in _takeViews)
                {
                    await item.Clean();
                }
                _takeViews.Clear();
            }
            
            _selectedView = null;
            Thumbnails.Items.Clear();
        }

        #endregion

        #region CallBacks

        #region TouchcastMediaCompositionView

        private async void TcCompositionItemsView_Loaded(object sender, RoutedEventArgs e)
        {
            _takeViews = new List<VideoFragmentView>();

            CreateAndInsertViews();
            UpdateClipViewsList();

            _scrollViewer = UIHelper.FindChild<ScrollViewer>(Thumbnails, null);
            _scrollViewer.ViewChanged += Scroll_ViewChanged;
            Thumbnails.SizeChanged += Thumbnails_SizeChanged;

            Playhead.Height = ActualHeight - ScrollBarHeight;
            PlayheadTransform.X = Thumbnails.Padding.Left;

            _currentViewGeneralTransform = null;
            //_currentView = null;
            await UpdateClipViewsVisibleRegions();

            DefaultSelection();
        }

        private void TcCompositionItemsView_Unloaded(object sender, RoutedEventArgs e)
        {
            Thumbnails.SizeChanged -= Thumbnails_SizeChanged;
            _scrollViewer.ViewChanged -= Scroll_ViewChanged;
            Thumbnails.Items.Clear();
        }

        #endregion

        #region Playhead

        private void Playhead_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeWestEastCursor;

            if (_isPressed)
            {
                var currentPoint = e.GetCurrentPoint(this);
                var transformX = PlayheadTransform.X + currentPoint.Position.X - _lastPoint.X;
                //var transformX = currentPoint.Position.X;
                _lastPoint = currentPoint.Position;

                if (_selectedView == null)
                {
                    UpdateCurrentView();
                }

                if (_selectedView != null)
                {
                    InvokeSeekEvent(transformX);
                }
            }
        }

        private void Playhead_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private void Playhead_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var point = e.GetCurrentPoint(this);
            _lastPoint = point.Position;
            Playhead.CapturePointer(e.Pointer);
            _isPressed = true;
        }

        private void Playhead_PointerCaptureLost(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            _isPressed = false;
        }

        private void Playhead_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            Playhead.ReleasePointerCapture(e.Pointer);
            _isPressed = false;
        }

        #endregion

        #region ScrollView


        #region New methods
        private async void Scroll_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (_selectedView != null)
            {
                UpdateCurrentView(_selectedView);
                UpdatePlayheadTransform(_position);
            }
            if (!e.IsIntermediate)
            {
                await UpdateClipViewsVisibleRegions();
            }
        }
        private List<ClipViewVisibleRegion> ClipViewsVisibleRegions()
        {
            var result = new List<ClipViewVisibleRegion>();

            if (_scrollViewer != null)
            {
                var viewportStartX = _scrollViewer.HorizontalOffset - VideoFragmentView.FrameSize.Width;
                if (viewportStartX < 0) viewportStartX = 0;
                var viewportEndX = _scrollViewer.HorizontalOffset + _scrollViewer.ViewportWidth;

                var clipStartX = Thumbnails.Padding.Left - _scrollViewer.HorizontalOffset;
                if (clipStartX < 0) clipStartX = 0;

                foreach (FrameworkElement view in Thumbnails.Items)
                {
                    double viewWidth;
                    if (view is VideoFragmentView)
                    {
                        viewWidth = view.Width;

                        var clipEndX = clipStartX + viewWidth;
                        if (viewportStartX < clipEndX && clipStartX < viewportEndX)
                        {
                            var regionX = (clipStartX < viewportStartX) ? viewportStartX - clipStartX : 0;

                            var regionWidth = (clipStartX + viewWidth > viewportEndX) ?
                                viewportEndX - clipStartX - regionX : viewWidth - regionX;

                            result.Add(new ClipViewVisibleRegion()
                            {
                                View = view as VideoFragmentView,
                                VisibleArea = new Rect(regionX, 0, regionWidth, view.Height)
                            });
                        }
                    }
                    else
                    {
                        viewWidth = 16;
                    }

                    clipStartX += viewWidth;
                    if (clipStartX > viewportEndX)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        #endregion

        #endregion

        private async void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Playhead.Height = ActualHeight - ScrollBarHeight;

            await UpdateClipViewsVisibleRegions();
        }

        #region TouchcastCompositionClipView

        private void ClipView_TouchcastCompositionClipViewWillChangeEvent(Take take)
        {
            //var clone = new Take(take.Recording, take.StartTrimTime, take.EndTrimTime);

            //authoringSession.Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, clone);
        }

        #endregion

        #region ListView

        private void Thumbnails_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            var point = e.GetPosition(this);
            _lastPoint = point;

            point = e.GetPosition(null);
            var selectedView = VisualTreeHelper.FindElementsInHostCoordinates(point, Thumbnails).FirstOrDefault(c => c is VideoFragmentView);

            if (selectedView != null)
            {
                UpdateCurrentView(selectedView as VideoFragmentView);
                InvokeSeekEvent(_lastPoint.X);
                SelectCurrentView();
            }
        }

        private void Thumbnails_SizeChanged(object sender, SizeChangedEventArgs e)
        {
             UpdatePlayheadTransform(_position);
        }

        #endregion

        #endregion

        #region Public interface
        public void UpdatePlayheadPositionIfNeeded(TimeSpan position)
        {
            if(_currentViewGeneralTransform != null)
            {
                UpdatePlayheadPosition(position);
            }
        }
        public void UpdatePlayheadPosition(TimeSpan position)
        {
            if (_takeViews.Count == 0)
            {
                Playhead.Visibility = Visibility.Collapsed;
                return;
            }
            Playhead.Visibility = Visibility.Visible;

            _position = position;

            if (_selectedView == null || _selectedView.MediaClip.StartTimeInComposition > position || _selectedView.MediaClip.EndTimeInComposition < position)
            {
                var view = _takeViews.Where(v => v.MediaClip.StartTimeInComposition <= position && v.MediaClip.EndTimeInComposition >= position).FirstOrDefault();
                if (view == null && _selectedView == null)
                {
                    view = _takeViews.FirstOrDefault();
                    _position = TimeSpan.Zero;
                }
                UpdateCurrentView(view);
            }
            
            UpdatePlayheadTransform(_position);
        }

        public async Task UpdateThumbnailsByNewZoom(Double pixelsPerSecond, CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            if (cancellationToken.IsCancellationRequested) return;

            foreach (var item in _takeViews)
            {
                if (cancellationToken.IsCancellationRequested) return;

                item.CalculateNewRanges(pixelsPerSecond);
            }

            if (cancellationToken.IsCancellationRequested) return;

            Thumbnails.UpdateLayout();
            var regions = ClipViewsVisibleRegions();
            foreach (var item in regions)
            {
                if (cancellationToken.IsCancellationRequested) return;
                await item.View.UpdateThumbnailsByNewZoom(pixelsPerSecond, item.VisibleArea, cancellationToken);
            }

            if (cancellationToken.IsCancellationRequested) return;

            UpdateMatrixTransformIfNeeded(pixelsPerSecond);
            UpdatePlayheadTransform(_position);
        }

        public void IncreasePosition(TimeSpan deltaTime)
        {
            var position = TimeInComposition(_position + deltaTime);
            SeekAndUpdatePlayheadPosition(position);
        }

        #endregion

        #region private

        private void DefaultSelection()
        {
            bool isNeedToSelect = !_takeViews.Any(v => v.IsSelected);
            if (isNeedToSelect)
            {
                _selectedView = _takeViews.FirstOrDefault(v => v.MediaClip.StartTimeInComposition <= _position && v.MediaClip.EndTimeInComposition >= _position);
                if (_selectedView != null)
                {
                    _selectedView.IsSelected = true;
                }
            }
        }
        private void UpdateClipViewsList()
        {
            _takeViews = Thumbnails.Items.Cast<VideoFragmentView>().ToList();
        }

        private void ChangePositionToClipStartTime(VideoFragmentView view, bool isScrolled = false)
        {
            var prevPositionX = CalculateXOffset();
            UpdateCurrentView(view);

            if (isScrolled)
            {
                var currentPositionX = CalculateXOffset();
                if (currentPositionX > _scrollViewer.ViewportWidth || currentPositionX < 0)
                {
                    _scrollViewer.ChangeView(_scrollViewer.HorizontalOffset + currentPositionX - prevPositionX, null, null);
                }
            }

            _position = _selectedView.MediaClip.StartTimeInComposition + TimeSpan.FromMilliseconds(1);
            SeekAndUpdatePlayheadPosition(_position);
        }

        private void UpdateCurrentView(VideoFragmentView newView)
        {
            if (newView != null)
            {
                if (_selectedView != null)
                {
                    _selectedView.IsSelected = false;
                }
                _selectedView = newView;
                _selectedView.IsSelected = true;
                UpdateMatrixTransform();
            }
        }

        private void SelectCurrentView()
        {
            var selectedView = _takeViews.FirstOrDefault(c => c.IsSelected);
            if (selectedView != null && selectedView != _selectedView) selectedView.IsSelected = false;
        }

        private void UpdatePlayheadTransform(TimeSpan position)
        {
            PlayheadTransform.X = CalculateXOffset() + (_selectedView.Width / _selectedView.MediaClip.TrimmedDuration.TotalSeconds)
                * (position - _selectedView.MediaClip.StartTimeInComposition).TotalSeconds;
            Playhead.Visibility = (PlayheadTransform.X < 0) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void UpdateMatrixTransformIfNeeded(double newPixelsPerSecodn)
        {
            if (newPixelsPerSecodn != _pixelPerSeconds)
            {
                UpdateMatrixTransform();
                _pixelPerSeconds = newPixelsPerSecodn;
            }
        }
        private void UpdateMatrixTransform()
        {
            if(_selectedView == null)
            {
                UpdateCurrentView();
            }

            var transform = _selectedView.TransformToVisual(Thumbnails);
            if (transform != null)
            {
                _currentViewGeneralTransform = transform;
            }
        }

        private double CalculateXOffset()
        {
            if(_currentViewGeneralTransform == null)
            {
                UpdateMatrixTransform();
            }
            var pt = _currentViewGeneralTransform.TransformPoint(ZeroPoint);
            return pt.X;
        }

        private void InvokeSeekEvent(double pointerPositionX)
        {
            var timeInComposition = CalculatePositionInComposition(pointerPositionX);
            SeekAndUpdatePlayheadPosition(timeInComposition);
        }

        private TimeSpan CalculatePositionInComposition(double pointerPositionX)
        {
            var rate = (pointerPositionX - CalculateXOffset()) / _selectedView.Width;

            TimeSpan timeInComposition = TimeSpan.FromSeconds(
                _selectedView.MediaClip.StartTimeInComposition.TotalSeconds + _selectedView.MediaClip.TrimmedDuration.TotalSeconds * rate);

            return TimeInComposition(timeInComposition);
        }

        private TimeSpan TimeInComposition(TimeSpan time)
        {
            if (time < TimeSpan.Zero)
            {
                time = TimeSpan.Zero;
            }
            if (time > _selectedView.MediaClip.OriginalDuration)
            {
                time = _selectedView.MediaClip.OriginalDuration;
            }
            return time;
        }

        private void CreateAndInsertViews()
        {
            foreach (var clip in _mediaComposition.Clips)
            {
                var videoFragmentView = new VideoFragmentView(clip, _pixelPerSeconds, _cancellationToken);
                Thumbnails.Items.Add(videoFragmentView);
            }
        }

        private void SeekAndUpdatePlayheadPosition(TimeSpan time)
        {
            TcCompositionSeekEvent?.Invoke(new TcCompositionSeekEventArgs(time));
            UpdatePlayheadPosition(time);
        }

        private async Task UpdateClipViewsVisibleRegions()
        {
            var regions = ClipViewsVisibleRegions();
            foreach (var item in regions)
            {
                await item.View.UpdateVisibleRegion(item.VisibleArea, _cancellationToken);
            }
        }

        private void UpdateCurrentView()
        {
            var view = _takeViews.Where(v => v.MediaClip.StartTimeInComposition <= _position && v.MediaClip.EndTimeInComposition >= _position).FirstOrDefault();
            UpdateCurrentView(view);
        }

        #endregion
    }
}
