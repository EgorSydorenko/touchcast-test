﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pitch.Helpers.Extensions;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Media.Editing;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.DataLayer;
using Pitch.Models;
using Pitch.Helpers.Logger;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Player.Views
{
    public delegate void VideoFragmentViewChanged(VideoFragment videoFragment);

    public sealed partial class VideoFragmentView
    {
        #region Static props and Constants

        public static readonly Size FrameSize = new Size(128, 72);
        public static readonly int MarkersPanelHeight = 10;

        private static readonly Thickness LayoutRootBorderThickness = new Thickness(1, 0, 1, 0);
        private static readonly TimeSpan MinClipTrimmedDuration = TimeSpan.FromSeconds(0.5);

        public static readonly DependencyProperty IsSelectedProperty =
                    DependencyProperty.Register(
                        "IsSelected", typeof(bool), typeof(VideoFragmentView),
                        new PropertyMetadata(false));

        #endregion

        #region Private fields and Properties

        private double _maxWidth;
        private AsyncLock _asynclock = new AsyncLock(1);
        private readonly MediaClip _mediaClip;

        private CancellationToken _cancellationToken;
        private MediaComposition _composition;
        private double _pixelsPerSecond;
        private Rect _visibleRegion;
        #endregion

        #region Public Properties

        public MediaClip MediaClip => _mediaClip;

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set
            {
                SetValue(IsSelectedProperty, value);
                if (value == false)
                {
                    LeftBorderToolTip.Opacity = 0;
                    RightBorderToolTip.Opacity = 0;
                }
            }
        }
        #endregion

        #region Life Cycle
        public VideoFragmentView(MediaClip mediaClip, double pixelsPerSecond, CancellationToken cancellationToken)
        {
            InitializeComponent();
            _cancellationToken = cancellationToken;
            _pixelsPerSecond = pixelsPerSecond;
            _mediaClip = mediaClip.Clone();
            _mediaClip.TrimTimeFromStart = _mediaClip.TrimTimeFromEnd = TimeSpan.Zero;
            _composition = new MediaComposition();
            _composition.Clips.Add(_mediaClip);
            CalculateNewRanges(_pixelsPerSecond);
            Loaded += TouchcastCompositionClipView_Loaded;
            Unloaded += TouchcastCompositionClipView_Unloaded;
        }

#if DEBUG
        ~VideoFragmentView()
        {
            System.Diagnostics.Debug.WriteLine("******************** VideoFragmentView Destructor********************");
        }
#endif
        #endregion

        #region Callbacks

        #region TouchcastCompositionClipView

        private void TouchcastCompositionClipView_Loaded(object sender, RoutedEventArgs e)
        {
            PointerEntered += ClipView_PointerEntered;
            PointerExited += ClipView_PointerExited;

            UpdateBorderToolTips();
        }

        private void TouchcastCompositionClipView_Unloaded(object sender, RoutedEventArgs e)
        {
            PointerEntered -= ClipView_PointerEntered;
            PointerExited -= ClipView_PointerExited;
        }

        #endregion

        #region Border

        private async void ClipView_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (RightBorderToolTip.Opacity != 1)
            {
                await ShowTooltipAnimation.BeginAsync();
            }
        }

        private async void ClipView_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (!IsSelected)
            {
                await HideTooltipAnimation.BeginAsync();
            }
        }

        #endregion

        #endregion

        #region Private

        private void UpdateBorderToolTips()
        {
            LeftBorderToolTip.Text = TimeToText(TimeSpan.Zero);
            RightBorderToolTip.Text = TimeToText(_mediaClip.OriginalDuration);
        }

        private string TimeToText(TimeSpan time)
        {
            if (time == TimeSpan.Zero) return "0";

            var toolTipFormat = (time.Hours == 0 && time.Minutes == 0) ? @"mm\:ss" : @"hh\:mm\:ss";
            return time.ToString(toolTipFormat);
        }

        private List<TimeSpan> CreateTimes(double x, double width)
        {
            List<TimeSpan> times = new List<TimeSpan>();

            int startRenderingFrame = (int)(x / FrameSize.Width);
            int endRenderingFrame = (int)Math.Ceiling((x + width) / FrameSize.Width); ;

            double msTimeStep = FrameSize.Width / _pixelsPerSecond;
            int step;
            for (step = startRenderingFrame; step < endRenderingFrame; ++step)
            {
                times.Add(TimeSpan.FromSeconds(msTimeStep * step));
            }

            return times;
        }

        private async Task CreateThumbnails(Rect visibleRegion, CancellationToken token)
        {
            _visibleRegion = visibleRegion;
            if (token.IsCancellationRequested) return;

            var times = CreateTimes(visibleRegion.X, visibleRegion.Width);

            if (token.IsCancellationRequested) return;

            int index = (int)(visibleRegion.X / FrameSize.Width);
            while (index > Frames.Children.Count)
            {
                Frames.Children.Add(new ImageView() { Width = FrameSize.Width });
            }

            foreach (var time in times)
            {
                if (token.IsCancellationRequested) return;

                var imageView = (index < Frames.Children.Count) ? 
                    Frames.Children[index] as ImageView : 
                    new ImageView() { Width = FrameSize.Width };

                if (!imageView.UpdateImageForZoom(_pixelsPerSecond))
                {
                    var bitmapImage = await CreateThumbnailImageAsync(time, token);
                    if (bitmapImage != null)
                    {
                        imageView.ImageSourceForZoom(bitmapImage, _pixelsPerSecond);
                    }

                    if (index >= Frames.Children.Count)
                    {
                        Frames.Children.Add(imageView);
                    }
                }
                ++index;
            }
        }

        private async Task<BitmapImage> CreateThumbnailImageAsync(TimeSpan time, CancellationToken token)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (token.IsCancellationRequested) return null;

                ImageStream stream =
                    await _composition.GetThumbnailAsync(time, (int)FrameSize.Width, (int)FrameSize.Height, VideoFramePrecision.NearestKeyFrame);

                if (token.IsCancellationRequested) return null;

                if (stream != null)
                {
                    bitmapImage = new BitmapImage()
                    {
                        DecodePixelHeight = (int)FrameSize.Height
                    };
                    bitmapImage.SetSource(stream);
                }
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.CreateThumbnailImageAsync({time}, token) : {ex.Message}");
            }
            return bitmapImage;
        }

        private TimeSpan WidthToDuration(double width)
        {
            return TimeSpan.FromSeconds(width / _pixelsPerSecond);
        }

        private void ResetTrimVariables()
        {
            LeftBorderTransform.X = 0;
            RightBorderTransform.X = Width;
        }

        private double CalculateMarkerPositionForTime(double time)
        {
            return _pixelsPerSecond * (time - _mediaClip.StartTimeInComposition.TotalSeconds);
        }

        #endregion

        #region Public

        public Task Clean()
        {
            return Task.CompletedTask;
        }

        public void ClipViewForCurrentTrimTime()
        {
            Width = _maxWidth;
            Frames.Width = _maxWidth;
            RightBorderTransform.X = Width;
            UpdateBorderToolTips();
        }

        public async Task UpdateThumbnailsByNewZoom(double pixelsPerSecond, Rect visibleRegion, CancellationToken token)
        {
            _pixelsPerSecond = pixelsPerSecond;
            await _asynclock.WaitAsync();
            try
            {
                await CreateThumbnails(visibleRegion, token);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.UpdateThumbnailsByNewZoom({pixelsPerSecond}, {visibleRegion}, token) : {ex.Message}");
            }
            finally
            {
                _asynclock.Release();
                _cancellationToken = token;
            }
        }

        public void CalculateNewRanges(double pixelsPerSecond)
        {
            try
            {
                _pixelsPerSecond = pixelsPerSecond;
                //Height = FrameSize.Height + MarkersPanelHeight + FramesContainer.BorderThickness.Top + FramesContainer.BorderThickness.Bottom + 14;

                _maxWidth = _mediaClip.OriginalDuration.TotalSeconds * _pixelsPerSecond;

                ClipViewForCurrentTrimTime();
                ResetTrimVariables();
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.CalculateNewRanges({pixelsPerSecond}) : {ex.Message}");
            }
        }

        public async Task UpdateVisibleRegion(Rect visibleRegion, CancellationToken token)
        {
            await _asynclock.WaitAsync();
            try
            {
                await CreateThumbnails(visibleRegion, token);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.UpdateVisibleRegion({visibleRegion}, token) : {ex.Message}");
            }
            finally
            {
                _asynclock.Release();
                _cancellationToken = token;
            }
        }

        #endregion
    }
}
