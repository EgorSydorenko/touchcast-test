﻿using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Player.Views
{
    public sealed partial class ImageView : UserControl
    {
        private readonly Dictionary<double, ImageSource> _imageCache = new Dictionary<double, ImageSource>();

        public void Reset()
        {

        }

        public void ImageSourceForZoom(ImageSource source, double pixelsPerSecond)
        {
            image.Source = source;
            _imageCache[pixelsPerSecond] = source;
        }

        public bool UpdateImageForZoom(double pixelsPerSecond)
        {
            if (_imageCache.ContainsKey(pixelsPerSecond))
            {
                image.Source = _imageCache[pixelsPerSecond];
                return true;
            }
            return false;
        }

        public ImageView()
        {
            InitializeComponent();
            Reset();
        }
    }
}
