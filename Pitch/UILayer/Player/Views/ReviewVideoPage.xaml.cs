﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Pitch.UILayer.Player.Views
{
    public sealed partial class ReviewVideoPage
    {

        #region Life Cycle

        public ReviewVideoPage()
        {
            InitializeComponent();
            App.SetupTitleBarColorScheme();
            Loaded += ReviewVideoPage_Loaded;
        }

        private void ReviewVideoPage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandTCTOpened);
            Windows.ApplicationModel.Core.CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Windows.UI.Xaml.Window.Current.SetTitleBar(TitleBar.MovementContainer);
            TitleBar.UpdateTitle(Models.Touchcast.Instance.Title);
            Unloaded += ReviewVideoPage_Unloaded;
        }

        private void ReviewVideoPage_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= ReviewVideoPage_Loaded;
            Unloaded -= ReviewVideoPage_Unloaded;
        }

#if DEBUG
        ~ReviewVideoPage()
        {
            Debug.WriteLine("********************ReviewVideoPage Destructor********************");
        }
#endif

        #endregion

        public void Clean()
        {
            LayoutRoot.Children.Clear();
        }
    }
}
