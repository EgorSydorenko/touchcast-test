﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pitch.Helpers.Extensions;
using Windows.Foundation;
using Windows.Media.Editing;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Pitch.Models;
using Pitch.Helpers;
using Pitch.UILayer.Helpers.Messages;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Player.Views
{
    public sealed partial class PlayerView
    {
        #region Static and constant values
        private readonly TimeSpan MediaPlayerStatePollingInterval = TimeSpan.FromMilliseconds(500);
        private readonly Duration _duration = new Duration(TimeSpan.FromMilliseconds(150));
        private readonly Duration _opacityAnimationDuration = new Duration(TimeSpan.FromMilliseconds(500));
        private readonly Size CompactPlayerSize = new Size(320, 180);
        private readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 0);
        private readonly CoreCursor SizeAllCursor = new CoreCursor(CoreCursorType.SizeAll, 0);
        #endregion

        #region Private Fields
        private bool _playing;
        private bool _isPlayerLayoutCompact;
        private double _bottomPanelHeight;
        private Point _previousPosition = new Point();
        private CompositeTransform _transform;
        private DispatcherTimer _timer;
        private MediaCompositionView _compositionView;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private ActionStreamAnalyzer _analyzer;
        private readonly Size _videoSize;

        private bool _isAnimationStarted;
        private bool _isNeedAnimate;
        private bool _isButtonPressed;
        private bool _isAfterLoading = false;
        private DoubleAnimation _xScaleAnimation = new DoubleAnimation();
        private DoubleAnimation _yScaleAnimation = new DoubleAnimation();
        private DoubleAnimation _xPositionAnimation = new DoubleAnimation();
        private DoubleAnimation _yPositionAnimation = new DoubleAnimation();
        private DoubleAnimation _bottomPanelAnimation = new DoubleAnimation();

        private Storyboard _playerModeAnimation = new Storyboard();

        private DoubleAnimation _opacityAnimation = new DoubleAnimation();
        private Storyboard _fadeInAnimation = new Storyboard();
        private Point _lastPoint;
        private object _lockObject = new object();

        private MediaComposition _mediaComposition;
        private string _touchcastPath;

        private double _millisecondsPerFrame = 33.3333;

        private TimeSpan _positionIncreaseStep;
        private double _framesPerSecond = 30;
        private MediaElementState _currentState;
        #endregion

        #region Life Cycle

        public PlayerView()
        {
            InitializeComponent();

            Loaded += TcMediaCompositionView_Loaded;
            Unloaded += TcMediaCompositionView_Unloaded;

            var bitmapSize = ServiceLocator.Current.GetInstance<Services.IMediaMixerService>().ResultVideoSize;
            _videoSize = new Size(bitmapSize.Width, bitmapSize.Height);

            Storyboard.SetTarget(_xPositionAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_xPositionAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateX)");
            _xPositionAnimation.Duration = _duration;
            _xPositionAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_yPositionAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_yPositionAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateY)");
            _yPositionAnimation.Duration = _duration;
            _yPositionAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_xScaleAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_xScaleAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleX)");
            _xScaleAnimation.Duration = _duration;
            _xScaleAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_yScaleAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_yScaleAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleY)");
            _yScaleAnimation.Duration = _duration;
            _yScaleAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_bottomPanelAnimation, PlayerBottomPanel);
            Storyboard.SetTargetProperty(_bottomPanelAnimation, "(UIElement.Height)");
            _bottomPanelAnimation.Duration = _duration;
            _bottomPanelAnimation.EnableDependentAnimation = false;

            _playerModeAnimation.Children.Add(_xPositionAnimation);
            _playerModeAnimation.Children.Add(_yPositionAnimation);
            _playerModeAnimation.Children.Add(_xScaleAnimation);
            _playerModeAnimation.Children.Add(_yScaleAnimation);
            _playerModeAnimation.Children.Add(_bottomPanelAnimation);
            _playerModeAnimation.FillBehavior = FillBehavior.HoldEnd;

            Storyboard.SetTarget(_opacityAnimation, PlayerViewBorder);
            Storyboard.SetTargetProperty(_opacityAnimation, "UIElement.Opacity");
            _opacityAnimation.Duration = _opacityAnimationDuration;
            _opacityAnimation.EnableDependentAnimation = false;
            _fadeInAnimation.Children.Add(_opacityAnimation);
            _fadeInAnimation.FillBehavior = FillBehavior.HoldEnd;
        }

#if DEBUG
        ~PlayerView()
        {
            System.Diagnostics.Debug.WriteLine("******************** PlayerView Destructor********************");
        }
#endif
        public async Task Cleanup()
        {
            await _compositionView.Cleanup();
        }

        public void Hide()
        {
            MediaPlayerElement.Stop();
            Visibility = Visibility.Collapsed;
        }
        public void Show()
        {
            Visibility = Visibility.Visible;
        }

        #endregion

        #region Callbacks

        #region TouchcastEditorView

        private async void TcMediaCompositionView_Loaded(object sender, RoutedEventArgs e)
        {
            MediaPlayerElement.CurrentStateChanged += MediaPlayerElement_CurrentStateChanged;
            MediaPlayerElement.SeekCompleted += MediaPlayerElement_SeekCompleted;
            MediaPlayerElement.MediaFailed += MediaPlayerElement_MediaFailed;
            MediaPlayerElement.MediaOpened += MediaPlayerElement_MediaOpened;

            PlayerLayout.PointerPressed += PlayerLayout_PointerPressed;
            PlayerLayout.PointerReleased += PlayerLayout_PointerReleased;
            PlayerLayout.PointerMoved += PlayerLayout_PointerMoved;
            PlayerLayout.PointerExited += PlayerLayout_PointerExited;
            PlayerLayout.DoubleTapped += PlayerLayout_DoubleTapped;
            
            VappContainer.PointerMoved += VappContainer_PointerMoved;

            _timer = new DispatcherTimer();
            _timer.Interval = MediaPlayerStatePollingInterval;
            _timer.Tick += Timer_Tick;

            await CalculateFrameRate();

            var timeValue = ZoomSlider.Value == 0 ? 1 : ZoomSlider.Value;
            var pixselsPerSecond = VideoFragmentView.FrameSize.Width / timeValue;
            _compositionView = new MediaCompositionView(_mediaComposition, _cancellationTokenSource.Token, pixselsPerSecond);
            CompositionItemContainer.Children.Add(_compositionView);
            _compositionView.TcCompositionSeekEvent += MediaItems_TcCompositionSeekEvent;

            ControlPanelEnabled(true);

            _touchcastPath = Touchcast.Instance.VideoFile.Path;
            _touchcastPath = _touchcastPath.Remove(_touchcastPath.LastIndexOf('\\') + 1);
            MediaPlayerElement.Source = new Uri(Touchcast.Instance.VideoFile.Path, UriKind.Absolute);
            
            if (_analyzer == null)
            {
                _analyzer = new ActionStreamAnalyzer(Touchcast.Instance.ActionsStream.Commands);
            }
            else
            {
                _analyzer.ActionStream = Touchcast.Instance.ActionsStream.Commands;
            }

            VideoDurationTextBlock.Text = TimeWithFrames(_mediaComposition.Duration);

            _transform = new CompositeTransform();
            _transform.TranslateX = 0;
            _transform.TranslateY = 0;
            _transform.ScaleX = 1;
            _transform.ScaleY = 1;
            PlayerLayout.RenderTransform = _transform;
            Windows.UI.Xaml.Controls.Canvas.SetZIndex(PlayerLayout, 300);
            Windows.UI.Xaml.Controls.Canvas.SetZIndex(VappContainer, 200);

            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            VappContainer.Visibility = Visibility.Collapsed;
            _compositionView.IsEnabled = true;
            _isPlayerLayoutCompact = false;
            _isAfterLoading = true;
            ShowNoClipsMessage(false);

            Shortcut.Manager.Instance.EditShortcutsManager.PlayMediaShortcutEvent += EditShortcutsManager_PlayMediaShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomInShortcutEvent += EditShortcutsManager_ZoomInShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomOutShortcutEvent += EditShortcutsManager_ZoomOutShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.PrevFrameShortcutEvent += EditShortcutsManager_PrevFrameShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NextFrameShortcutEvent += EditShortcutsManager_NextFrameShortcutEvent;
        }

        private void TcMediaCompositionView_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_isPlayerLayoutCompact)
            {
                _isPlayerLayoutCompact = false;
                _transform.TranslateX = 0;
                _transform.TranslateY = 0;
                _transform.ScaleX = 1;
                _transform.ScaleY = 1;
                PlaybackControlPanel.Visibility = Visibility.Visible;
                _compositionView.Visibility = Visibility.Visible;
                VappContainer.Children.Clear();
            }

            Messenger.Default.Unregister(this);
            _timer.Stop();
            _timer.Tick -= Timer_Tick;

            Shortcut.Manager.Instance.EditShortcutsManager.PlayMediaShortcutEvent -= EditShortcutsManager_PlayMediaShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomInShortcutEvent -= EditShortcutsManager_ZoomInShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomOutShortcutEvent -= EditShortcutsManager_ZoomOutShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.PrevFrameShortcutEvent -= EditShortcutsManager_PrevFrameShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NextFrameShortcutEvent -= EditShortcutsManager_NextFrameShortcutEvent;

            MediaPlayerElement.CurrentStateChanged -= MediaPlayerElement_CurrentStateChanged;
            MediaPlayerElement.SeekCompleted -= MediaPlayerElement_SeekCompleted;
            MediaPlayerElement.MediaFailed -= MediaPlayerElement_MediaFailed;
            MediaPlayerElement.MediaOpened -= MediaPlayerElement_MediaOpened;

            PlayerLayout.PointerPressed -= PlayerLayout_PointerPressed;
            PlayerLayout.PointerReleased -= PlayerLayout_PointerReleased;
            PlayerLayout.PointerMoved -= PlayerLayout_PointerMoved;
            PlayerLayout.PointerExited -= PlayerLayout_PointerExited;
            PlayerLayout.DoubleTapped -= PlayerLayout_DoubleTapped;

            _compositionView.TcCompositionSeekEvent -= MediaItems_TcCompositionSeekEvent;
            VappContainer.PointerMoved -= VappContainer_PointerMoved;
            VappContainer.Children.Clear();
        }

        #endregion

        #region MediaPlayerElement

        private void MediaPlayerElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            ControlPanelEnabled(true);
            UpdateTimeTexts();
            if(_isAfterLoading)
            {
                _isAfterLoading = false;
                MediaPlayerElement.Play();
            }
        }

        private void MediaPlayerElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            ControlPanelEnabled(false);
        }

        
        private void MediaItems_TcCompositionSeekEvent(TcCompositionSeekEventArgs args)
        {
            _currentState = MediaPlayerElement.CurrentState;
            MediaPlayerElement.Pause();
            MediaPlayerElement.Position = args.Position;
        }

        private void MediaPlayerElement_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (MediaPlayerElement.CurrentState)
            {
                case MediaElementState.Opening:
                    Messenger.Default.Send(new PausePlayerMessage());
                    break;
                case MediaElementState.Playing:
                    _timer.Start();
                    if (!_isPlayerLayoutCompact)
                    {
                        Messenger.Default.Send(new PlayPlayerMessage());
                    }
                    PauseContent.Visibility = Visibility.Visible;
                    PlayContent.Visibility = Visibility.Collapsed;
                    break;
                case MediaElementState.Paused:
                    _timer.Stop();
                    UpdatePlayheadPositionIfNeeded();
                    if (!_isPlayerLayoutCompact)
                    {
                        Messenger.Default.Send(new PausePlayerMessage());
                    }
                    PlayContent.Visibility = Visibility.Visible;
                    PauseContent.Visibility = Visibility.Collapsed;
                    break;
                case MediaElementState.Stopped:
                    _timer.Stop();
                    Messenger.Default.Send(new PausePlayerMessage());
                    PlayContent.Visibility = Visibility.Visible;
                    PauseContent.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void MediaPlayerElement_SeekCompleted(object sender, RoutedEventArgs e)
        {
            UpdateTimeTexts();
            if (_currentState == MediaElementState.Playing)
            {
                MediaPlayerElement.Play();
            }
        }

        #endregion

        #region PlayerLayout Pointer

        private void PlayerLayout_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            _isButtonPressed = true;
            _isNeedAnimate = true;
            MediaPlayerElement.CapturePointer(e.Pointer);

            if (_isPlayerLayoutCompact)
            {
                var point = e.GetCurrentPoint(this);
                _lastPoint = point.Position;
            }
        }

        private void PlayerLayout_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            lock (_lockObject)
            {
                if (_isPlayerLayoutCompact)
                {
                    if (_isButtonPressed)
                    {
                        _isNeedAnimate = false;

                        Window.Current.CoreWindow.PointerCursor = SizeAllCursor;
                        var currentPoint = e.GetCurrentPoint(this);

                        var maxX = RootLayout.ActualWidth - PlayerLayout.ActualWidth * _transform.ScaleX;
                        var x = _transform.TranslateX + currentPoint.Position.X - _lastPoint.X;
                        if (x < 0) x = 0;
                        if (x > maxX) x = maxX;

                        var maxY = RootLayout.ActualHeight - PlayerLayout.ActualHeight * _transform.ScaleY;
                        var y = _transform.TranslateY + currentPoint.Position.Y - _lastPoint.Y;
                        if (y < 0) y = 0;
                        if (y > maxY) y = maxY;

                        _transform.TranslateX = x;
                        _transform.TranslateY = y;

                        _lastPoint = currentPoint.Position;
                    }
                }
            }
        }

        private async void PlayerLayout_DoubleTapped(object sender, Windows.UI.Xaml.Input.DoubleTappedRoutedEventArgs e)
        {
            if (e.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Touch || e.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Pen)
            {
                if (_isPlayerLayoutCompact)
                {
                    if (_isAnimationStarted || !_isNeedAnimate)
                    {
                        return;
                    }
                    _isAnimationStarted = true;

                    await RestorePlayerFromCompactMode();
                    _isAnimationStarted = false;
                }
            }
        }

        private async Task RestorePlayerFromCompactMode()
        {
            _isPlayerLayoutCompact = false;
            _previousPosition.X = _transform.TranslateX;
            _previousPosition.Y = _transform.TranslateY;

            PlaybackControlPanel.Visibility = Visibility.Visible;
            _compositionView.Visibility = Visibility.Visible;

            await UpdateUi(false);

            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            if (MediaPlayerElement.CurrentState == MediaElementState.Paused && _playing)
            {
                MediaPlayerElement.Play();
                _playing = false;
            }

            VappContainer.Children.Clear();
        }

        private async void PlayerLayout_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            lock (_lockObject)
            {
                _isButtonPressed = false;

                if (_isAnimationStarted || !_isNeedAnimate)
                {
                    return;
                }
                _isAnimationStarted = true;
            }

            if (_isPlayerLayoutCompact)
            {
                await RestorePlayerFromCompactMode();
                _isAnimationStarted = false;
                return;
            }

            var point = e.GetCurrentPoint(MediaPlayerElement).Position;
            var cmd = _analyzer.GetCommandByTimeAndPosition(MediaPlayerElement.Position, point, _videoSize);
            if (cmd != null)
            {
                UIElement element = null;
                if (!String.IsNullOrEmpty(cmd.Url))
                {
                    var extension = System.IO.Path.GetExtension(cmd.Url);
                    if (!String.IsNullOrEmpty(extension) && !(cmd.Url.Contains("http://") || cmd.Url.Contains("https://")))
                    {
                        extension = extension.ToLower();
                        var url = cmd.Url.Replace("($PERFORMANCE_BASE_URL)/", _touchcastPath);
                        if (FileToAppHelper.DocumentExtensions.Contains(extension))
                        {
                            element = new Editing.Views.PreviewViews.PreviewDocumentView(url);
                        }
                        else if (FileToAppHelper.ImageExtensions.Contains(extension))
                        {
                            element = new Editing.Views.PreviewViews.PreviewImageView(url);
                        }
                        else if (FileToAppHelper.VideoExtensions.Contains(extension))
                        {
                            element = new Editing.Views.PreviewViews.PreviewVideoView(url);
                        }
                    }
                    else
                    {
                        element = new Editing.Views.PreviewViews.PreviewWebView(cmd.Url);
                    }

                    if (element != null)
                    {
                        _isPlayerLayoutCompact = true;

                        if (cmd.PauseOnInteration.Equals("YES") && MediaPlayerElement.CurrentState == MediaElementState.Playing)
                        {
                            MediaPlayerElement.Pause();
                            _playing = true;
                        }
                        VappContainer.Children.Add(element);
                        await UpdateUi(true);
                    }
                }


            }
            _isAnimationStarted = false;
        }

        private void VappContainer_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private void PlayerLayout_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        #endregion

        #region Playback

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            PlayPause();
        }

        private void PlayPause()
        {
            if (MediaPlayerElement.CurrentState == MediaElementState.Playing)
            {
                MediaPlayerElement.Pause();
            }
            else
            {
                MediaPlayerElement.Play();
            }
        }

        private void FastForwardButton_Click(object sender, RoutedEventArgs e)
        {
            IncreasePosition(_positionIncreaseStep);
        }

        private void FastBackwardButton_Click(object sender, RoutedEventArgs e)
        {
            IncreasePosition(-_positionIncreaseStep);
        }
        #endregion

        #region Timer

        private void Timer_Tick(object sender, object e)
        {
            UpdatePlayheadPosition();
        }

        #endregion

        #region Shortcuts callbacks

        private Task<bool> EditShortcutsManager_PlayMediaShortcutEvent()
        {
            PlayPause();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_ZoomInShortcutEvent()
        {
            ZoomIn();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_ZoomOutShortcutEvent()
        {
            ZoomOut();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_NextFrameShortcutEvent()
        {
            IncreasePosition(_positionIncreaseStep);
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_PrevFrameShortcutEvent()
        {
            IncreasePosition(-_positionIncreaseStep);
            return Task.FromResult(true);
        }

        #endregion

        private async Task AuthoringSession_AuthoringSessionDidResetEvent()
        {
            await _compositionView.Cleanup();
            _analyzer = null;
        }

        private void RootLayout_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_isPlayerLayoutCompact == true && _transform != null)
            {
                CompositeTransform transform = CalculateTransform();
                _transform.TranslateX = transform.TranslateX;
                _transform.TranslateY = transform.TranslateY;
                _transform.ScaleX = transform.ScaleX;
                _transform.ScaleY = transform.ScaleY;
            }
        }

        private void IncreazeZoom_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ZoomIn();
        }

        private void DecreaseZoom_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ZoomOut();
        }

        private async void RangeBase_OnValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (_compositionView != null)
            {
                CancelUpdateThumbnails();
                var timeValue = ZoomSlider.Value == 0 ? 1 : ZoomSlider.Value;
                var pixselsPerSecond = VideoFragmentView.FrameSize.Width / timeValue;
                await Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal,
                    async () =>
                    {
                        if (_cancellationTokenSource.Token.IsCancellationRequested) return;

                        await _compositionView.UpdateThumbnailsByNewZoom(pixselsPerSecond, _cancellationTokenSource.Token);
                    });
            }
        }

        #endregion

        #region Private Methods

        private async Task CalculateFrameRate()
        {
            var mediaClip = await MediaClip.CreateFromFileAsync(Touchcast.Instance.VideoFile);
            _mediaComposition = new MediaComposition();
            _mediaComposition.Clips.Add(mediaClip);
            var profile = _mediaComposition.CreateDefaultEncodingProfile();
            var frameRate = profile.Video.FrameRate;
            _framesPerSecond = (double)frameRate.Numerator / frameRate.Denominator;
            _millisecondsPerFrame = (double)frameRate.Denominator / frameRate.Numerator * 1000f;
            _positionIncreaseStep = TimeSpan.FromTicks((long)(_millisecondsPerFrame * TimeSpan.TicksPerMillisecond));
        }

        private void CancelUpdateThumbnails()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource = new CancellationTokenSource();
        }

        private void ShowNoClipsMessage(bool value)
        {
            PlayerViewBox.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
            NoClipsPanel.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ZoomIn()
        {
            ZoomSlider.Value -= ZoomSlider.StepFrequency;
        }

        private void ZoomOut()
        {
            ZoomSlider.Value += ZoomSlider.StepFrequency;
        }

        private void UpdatePlayheadPosition()
        {
                var position = MediaPlayerElement.Position;
                if (position > _mediaComposition.Duration)
                {
                    position = _mediaComposition.Duration;
                    _timer.Stop();
                }
                VideoTimeTextBlock.Text = TimeWithFrames(position);
                _compositionView.UpdatePlayheadPosition(position);
        }

        private void UpdatePlayheadPositionIfNeeded()
        {  
            VideoTimeTextBlock.Text = TimeWithFrames(MediaPlayerElement.Position);
            _compositionView.UpdatePlayheadPositionIfNeeded(MediaPlayerElement.Position);
        }

        private void IncreasePosition(TimeSpan deltaTime)
        {
            _compositionView.IncreasePosition(deltaTime);
        }

        private void ControlPanelEnabled(bool isEnabled)
        {
            ZoomControl.IsEnabled = isEnabled;
            PlaybackControl.IsEnabled = isEnabled;
        }

        private void UpdateTimeTexts()
        {
            VideoTimeTextBlock.Text = TimeWithFrames(MediaPlayerElement.Position);
        }

        private string TimeWithFrames(TimeSpan time)
        {
            int frames = (int)(time.Milliseconds / _millisecondsPerFrame); //-1;
            return time.ToString(@"mm\:ss") + $":{frames:00}";
        }

        private async Task UpdateUi(bool isVappShown)
        {
            if (!_playing)
            {
                if (isVappShown)
                {
                    Messenger.Default.Send(new PlayPlayerMessage());
                }
                else
                {
                    Messenger.Default.Send(new PausePlayerMessage());
                }
            }
            
            if (isVappShown)
            {
                Messenger.Default.Send(new DisableExportButtonMessage());
            }
            else
            {
                PlayerViewBorder.Opacity = 0;
            }

            VappContainer.Visibility = isVappShown ? Visibility.Visible : Visibility.Collapsed;
            ControlPanelEnabled(!isVappShown);
            _compositionView.IsEnabled = !isVappShown;

            CompositeTransform transform = CalculateTransform();

            _xPositionAnimation.From = isVappShown ? 0 : transform.TranslateX;
            _xPositionAnimation.To = isVappShown ? transform.TranslateX : 0;
            _yPositionAnimation.From = isVappShown ? 0 : transform.TranslateY;
            _yPositionAnimation.To = isVappShown ? transform.TranslateY : 0;

            _xScaleAnimation.From = isVappShown ? 1 : transform.ScaleX;
            _xScaleAnimation.To = isVappShown ? transform.ScaleX : 1;
            _yScaleAnimation.From = isVappShown ? 1 : transform.ScaleY;
            _yScaleAnimation.To = isVappShown ? transform.ScaleY : 1;

            if (!isVappShown)
            {
                _bottomPanelHeight = PlayerBottomPanel.ActualHeight;
            }

            _bottomPanelAnimation.From = isVappShown ? 1 : _bottomPanelHeight;
            _bottomPanelAnimation.To = isVappShown ? _bottomPanelHeight : 1;

            await _playerModeAnimation.BeginAsync();
            _transform = PlayerLayout.RenderTransform as CompositeTransform;

            if (isVappShown)
            {
                _isPlayerLayoutCompact = true;
                _compositionView.Visibility = Visibility.Collapsed;
                PlaybackControlPanel.Visibility = Visibility.Collapsed;

                _opacityAnimation.From = 0;
                _opacityAnimation.To = 1;
                await _fadeInAnimation.BeginAsync();
            }
        }

        private CompositeTransform CalculateTransform()
        {
            CompositeTransform transform = new CompositeTransform();

            var aspect = PlayerLayout.ActualWidth / PlayerLayout.ActualHeight;
            var _xAspectValue = CompactPlayerSize.Height * aspect / PlayerLayout.ActualWidth;

            _previousPosition.X = PlayerLayout.ActualWidth - CompactPlayerSize.Height * aspect - 10;
            _previousPosition.Y = PlayerLayout.ActualHeight - CompactPlayerSize.Height - 48;

            transform.TranslateX = _previousPosition.X;
            transform.TranslateY = _previousPosition.Y;

            transform.ScaleX = _xAspectValue;
            transform.ScaleY = CompactPlayerSize.Height / PlayerLayout.ActualHeight;

            return transform;
        }

        #endregion
    }
}
