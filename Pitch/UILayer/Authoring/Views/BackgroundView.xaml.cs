﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.Shots;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views
{
    public sealed partial class BackgroundView : UserControl
    {
        private Shot _shot;
        private string _clipId = String.Empty;
        private WeakReference<AuthoringSession> _authoringSession;
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();


        #region Life cycle
        public BackgroundView()
        {
            InitializeComponent();

            Loaded += BackgroundView_Loaded;
        }

#if DEBUG
        ~BackgroundView()
        {
            System.Diagnostics.Debug.WriteLine("************* BackgroundView Destructor **********************");
        }
#endif
        private async void BackgroundView_Loaded(object sender, RoutedEventArgs e)
        {
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                _authoringSession = new WeakReference<AuthoringSession>(session);
                if (session.ShotManager == null)
                {
                    session.AuthroingSessionWasInitialized += Session_AuthroingSessionWasInitialized;
                }
                else
                {
                    session.AuthoringSeesionWillOpenProject += Session_AuthoringSeesionWillOpenProject;
                    session.AuthoringSeesionDidOpenProject += Session_AuthoringSeesionDidOpenProject;
                    session.ShotManager.ShotWillBeDisplayed += ShotManager_ShotWillBeDisplayed;
                    await Update(session.Project.ActiveShot);
                }
            }
        }

        private Task Session_AuthoringSeesionWillOpenProject()
        {
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                session.ShotManager.ShotWillBeDisplayed -= ShotManager_ShotWillBeDisplayed;
            }
            return Task.CompletedTask;
        }

        private Task Session_AuthoringSeesionDidOpenProject()
        {
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                session.ShotManager.ShotWillBeDisplayed += ShotManager_ShotWillBeDisplayed;
            }
            return Task.CompletedTask;
        }

        public void Unsubscribe()
        {
            try
            {
                var session = AuthoringSession;
                if (session != null)
                {
                    session.AuthoringSeesionWillOpenProject -= Session_AuthoringSeesionWillOpenProject;
                    session.AuthoringSeesionDidOpenProject -= Session_AuthoringSeesionDidOpenProject;
                    session.ShotManager.ShotWillBeDisplayed -= ShotManager_ShotWillBeDisplayed;
                    session.AuthroingSessionWasInitialized -= Session_AuthroingSessionWasInitialized;
                }
                Loaded -= BackgroundView_Loaded;
                Player.Stop();
                Player.Source = null;

                UnsubscribeFromShot();
                StoprenderingIfNeeded(_shot);
                _shot = null;
            }
            catch(Exception e)
            {
                Pitch.Helpers.Logger.LogQueue.WriteToFile($"backgroundView Unsubscribe:  {e.Message}");
            }
        }
        #endregion

        #region Callbacks
        private async Task ShotManager_ShotWillBeDisplayed(Shot model)
        {
            await Update(model);
        }

        private async Task Session_AuthroingSessionWasInitialized()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.ShotManager.ShotWillBeDisplayed += ShotManager_ShotWillBeDisplayed;
                await Update(session.Project.ActiveShot);
            }
        }
        private async void Shot_UpdateShotViewEvent()
        {
            await Update(_shot);
        }
        private async void Shot_UpdateBackgroundLayerEvent(LayerLayout newBackgroundLayer, LayerLayout currentBackgroundLayer)
        {
            await Update(_shot);
        }
        #endregion

        #region Private methods
        private async Task Update(Shot model)
        {

            UnsubscribeFromShot();
            var prevShot = _shot;
            _shot = model;
            SubscribeForShot();
            UpdateBackgroundColor();
            if (!App.Locator.IsLightMode)
            {


                var backgroundLayer = model.FindVideoBackground();
                if (backgroundLayer == null)
                {
                    model.Project.BackgroundRenderableObject.IsBackgroundEnabled = false;
                    Player.Stop();
                    Player.Visibility = Visibility.Collapsed;
                    _clipId = String.Empty;
                    StoprenderingIfNeeded(prevShot);
                }
                else if (_clipId != backgroundLayer.File.Path)
                {
                    if (_clipId != backgroundLayer.File.Path)
                    {
                        StoprenderingIfNeeded(prevShot);
                        if (App.Locator.AppState == ApplicationState.Recording)
                        {
                            backgroundLayer.UpdateVideoCompositor(model.Project.BackgroundRenderableObject.BackgroundContextElement.RecordableItemID);
                        }
                        var stream = backgroundLayer.Composition.GenerateMediaStreamSource();
                        //composition.Clips.Add(mediaClip);
                        Player.SetMediaStreamSource(stream);
                        Player.Play();
                        Player.Visibility = Visibility.Visible;
                        backgroundLayer.RefreshRelativePosition();
                        await backgroundLayer.UpdateThumbnail();
                    }
                    model.Project.BackgroundRenderableObject.IsBackgroundEnabled = true;
                    _clipId = backgroundLayer.File.Path;
                }
            }
        }

        private void UpdateBackgroundColor()
        {
            LayoutRoot.Background = new SolidColorBrush(_shot.BackgroundColor);
        }
        private void SubscribeForShot()
        {
            if (_shot != null)
            {
                _shot.UpdateShotViewEvent += Shot_UpdateShotViewEvent;
                _shot.UpdateBackgroundLayerEvent += Shot_UpdateBackgroundLayerEvent;
            }
        }
        private void UnsubscribeFromShot()
        {
            if (_shot != null)
            {
                _shot.UpdateShotViewEvent -= Shot_UpdateShotViewEvent;
                _shot.UpdateBackgroundLayerEvent -= Shot_UpdateBackgroundLayerEvent;
            }
        }
        private void StoprenderingIfNeeded(Shot shot)
        {
            var background = shot?.FindVideoBackground();
            if (background != null)
            {
                background.ClearVideoCompositor();
            }
        }
        #endregion
    }
}
