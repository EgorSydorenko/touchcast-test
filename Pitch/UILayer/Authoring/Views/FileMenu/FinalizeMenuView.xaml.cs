﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Linq;
using Pitch.DataLayer;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.FileMenu
{
    public sealed partial class FinalizeMenuView : UserControl
    {
        #region Private Fields
        private FinalizeMenuViewModel _viewModel;
        private UploaderControl _fabricPanel;
        private UploaderControl _kalturaPanel;
        private UploaderControl _exchangePanel;
        private UploaderControl _desktopPanel;
        #endregion

        public FinalizeMenuViewModel ViewModel => _viewModel;

        public delegate void FinalizeProcess();
        public event FinalizeProcess FinalizeProcessStartedEvent;
        public event FinalizeProcess FinalizeProcessStoppedEvent;

        #region Life Cycle
        public FinalizeMenuView()
        {
            _viewModel = new FinalizeMenuViewModel();
            InitializeComponent();
            Loaded += FinalizeMenuView_Loaded;
        }

#if DEBUG
        ~FinalizeMenuView()
        {
            System.Diagnostics.Debug.WriteLine("********************FinalizeMenuView Destructor********************");
        }
#endif
        
        private async void FinalizeMenuView_Loaded(object sender, RoutedEventArgs e)
        {
            var isShowFabricUploader = !TfSdk.ApiClient.Shared.UserInfo.IsKalturaUser || TfSdk.ApiClient.Shared.UserInfo.IsAccentureUser;
            var isShowKalturaUploader = TfSdk.ApiClient.Shared.UserInfo.IsKalturaUser && !TfSdk.ApiClient.Shared.UserInfo.IsAccentureUser;

            if (isShowFabricUploader && _fabricPanel == null)
            {
                _fabricPanel = new UploaderControl(UploaderType.Fabric);
                _fabricPanel.UploadProcessStartedEvent += UploadProcessStartedEvent;
                _fabricPanel.UploadProcessStoppedEvent += UploadProcessStoppedEvent;
                Grid.SetColumn(_fabricPanel, 2);
                LayoutRoot.Children.Add(_fabricPanel);
                _fabricPanel.Visibility = Visibility.Collapsed;
                if (TfSdk.ApiClient.Shared.UserInfo.IsAccentureUser)
                {
                    FabricCloudItem.Text += " (external)";
                }
            }
            if (isShowKalturaUploader && _kalturaPanel == null)
            {
                _kalturaPanel = new UploaderControl(UploaderType.Kaltura);
                _kalturaPanel.UploadProcessStartedEvent += UploadProcessStartedEvent;
                _kalturaPanel.UploadProcessStoppedEvent += UploadProcessStoppedEvent;
                Grid.SetColumn(_kalturaPanel, 2);
                LayoutRoot.Children.Add(_kalturaPanel);
                _kalturaPanel.Visibility = Visibility.Collapsed;
            }
            if (TfSdk.ApiClient.Shared.UserInfo.IsAccentureUser && _exchangePanel == null)
            {
                _exchangePanel = new UploaderControl(UploaderType.Exchange);
                _exchangePanel.UploadProcessStartedEvent += UploadProcessStartedEvent;
                _exchangePanel.UploadProcessStoppedEvent += UploadProcessStoppedEvent;
                Grid.SetColumn(_exchangePanel, 2);
                LayoutRoot.Children.Add(_exchangePanel);
                _exchangePanel.Visibility = Visibility.Collapsed;
            }

            if (_desktopPanel == null)
            {
                _desktopPanel = new UploaderControl(UploaderType.Desktop);
                _desktopPanel.UploadProcessStartedEvent += UploadProcessStartedEvent;
                _desktopPanel.UploadProcessStoppedEvent += UploadProcessStoppedEvent;
                Grid.SetColumn(_desktopPanel, 2);
                LayoutRoot.Children.Add(_desktopPanel);
                _desktopPanel.Visibility = Visibility.Collapsed;
            }

            var item = Finalizers.Items.OfType<ListViewItem>().FirstOrDefault(i => i.Tag.Equals("Fabric"));
            item.Visibility = isShowFabricUploader ? Visibility.Visible : Visibility.Collapsed;

            item = Finalizers.Items.OfType<ListViewItem>().FirstOrDefault(i => i.Tag.Equals("Kaltura"));
            item.Visibility = isShowKalturaUploader ? Visibility.Visible : Visibility.Collapsed;

            item = Finalizers.Items.OfType<ListViewItem>().FirstOrDefault(i => i.Tag.Equals("Exchange"));
            item.Visibility = TfSdk.ApiClient.Shared.UserInfo.IsAccentureUser ? Visibility.Visible : Visibility.Collapsed;

            Finalizers.SelectedItem = Finalizers.Items.OfType<ListViewItem>().FirstOrDefault(i => i.Visibility == Visibility.Visible);

            //FinalizeProcessStartedEvent?.Invoke();
            var exporter = SimpleIoc.Default.GetInstance<AuthoringSession>()?.Export();
            _viewModel.Exporter = exporter;
            if (_fabricPanel != null) _fabricPanel.ViewModel.Exporter = exporter;
            if (_kalturaPanel != null) _kalturaPanel.ViewModel.Exporter = exporter;
            if (_exchangePanel != null) _exchangePanel.ViewModel.Exporter = exporter;
            if (_desktopPanel != null) _desktopPanel.ViewModel.Exporter = exporter;

            _viewModel.Export();
        }
        #endregion

        public void CancelExport()
        {
            _viewModel.CancelExport();
            _fabricPanel?.Cancel();
            _kalturaPanel?.Cancel();
            _exchangePanel?.Cancel();
            _desktopPanel?.Cancel();
        }

        public void Clean()
        {
            CancelExport();
            Loaded -= FinalizeMenuView_Loaded;
            if (_fabricPanel != null)
            {
                _fabricPanel.UploadProcessStartedEvent -= UploadProcessStartedEvent;
                _fabricPanel.UploadProcessStoppedEvent -= UploadProcessStoppedEvent;
                _fabricPanel.Clean();
            }
            if (_kalturaPanel != null)
            {
                _kalturaPanel.UploadProcessStartedEvent -= UploadProcessStartedEvent;
                _kalturaPanel.UploadProcessStoppedEvent -= UploadProcessStoppedEvent;
                _kalturaPanel.Clean();
            }
            if (_exchangePanel != null)
            {
                _exchangePanel.UploadProcessStartedEvent -= UploadProcessStartedEvent;
                _exchangePanel.UploadProcessStoppedEvent -= UploadProcessStoppedEvent;
                _exchangePanel.Clean();
            }

            LayoutRoot.Children.Clear();

            _viewModel.Cleanup();
            _viewModel = null;
        }

        #region Callbacks

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_fabricPanel != null)  _fabricPanel.Visibility = Visibility.Collapsed;
            if (_kalturaPanel != null) _kalturaPanel.Visibility = Visibility.Collapsed;
            if (_exchangePanel != null) _exchangePanel.Visibility = Visibility.Collapsed;
            _desktopPanel.Visibility = Visibility.Collapsed;
            
            var selectedItem = (Finalizers.SelectedItem as ListViewItem)?.Tag as String;
            if (selectedItem == "Fabric")
            {
                _fabricPanel.Visibility = Visibility.Visible;
            }
            else if (selectedItem == "Kaltura")
            {
                _kalturaPanel.Visibility = Visibility.Visible;
            }
            else if (selectedItem == "Exchange")
            {
                _exchangePanel.Visibility = Visibility.Visible;
            }
            else if (selectedItem == "Desktop")
            {
                _desktopPanel.Visibility = Visibility.Visible;
            }
        }

        private void UploadProcessStoppedEvent()
        {
            FinalizeProcessStoppedEvent?.Invoke();
        }

        private void UploadProcessStartedEvent()
        {
            FinalizeProcessStartedEvent?.Invoke();
        }
        #endregion
    }
}
