﻿using System;
using TfSdk;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.FileMenu
{
    public sealed partial class SettingsMenuView : UserControl
    {
        private const string SettingsMenuMessageTitle = "SettingsMenuMessageTitle";
        private const string SettingsMenuMessageContent = "SettingsMenuMessageContent";

        private readonly string MessageTitle;
        private readonly string MessageContent;

        public event EventHandler LogoutMenuEvent;

        #region Life cycle
        public SettingsMenuView()
        {
            InitializeComponent();

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            MessageTitle = loader.GetString(SettingsMenuMessageTitle);
            MessageContent = loader.GetString(SettingsMenuMessageContent);

            var settingsService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Services.ISettingsService>();
            var isLightModeOn = (settingsService.LightModeUserPermission == Services.ModeUserPermission.Undefined) ? (!settingsService.IsDeviceStrong)
                : (settingsService.LightModeUserPermission == Services.ModeUserPermission.LightModeEnabled);
            LightModeSwitcher.IsOn = isLightModeOn;
            LightModeSwitcher.Toggled += LightModeSwitcher_Toggled;

            Loaded += SettingsMenuView_Loaded;
        }
#if DEBUG
        ~SettingsMenuView()
        {
            System.Diagnostics.Debug.WriteLine("****************SettingsMenuView Destructor****************");
        }
#endif

        private void SettingsMenuView_Loaded(object sender, RoutedEventArgs e)
        {
            var uri = ApiClient.Shared.UserInfo?.info?.data?.avatar_small;
            if (!String.IsNullOrEmpty(uri) && Uri.IsWellFormedUriString(uri, UriKind.Absolute))
            {
                var bitmap = new BitmapImage(new Uri(uri));
                var imageBrush = new ImageBrush();
                imageBrush.Stretch = Stretch.Uniform;
                imageBrush.ImageSource = bitmap;
                Avatar.Fill = imageBrush;
            }
            UserName.Text = (ApiClient.Shared.UserInfo?.info?.data?.first_name ?? String.Empty) + " " + (ApiClient.Shared.UserInfo?.info?.data?.last_name ?? String.Empty);
            Email.Text = ApiClient.Shared.UserInfo?.info?.data?.email ?? String.Empty;
        }
        #endregion

        #region Callbacks
        private async void ViewLogsButton_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchFolderAsync(App.LogsFolder);
        }
        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {
            LogoutMenuEvent?.Invoke(this, EventArgs.Empty);
        }
        private async void LightModeSwitcher_Toggled(object sender, RoutedEventArgs e)
        {
            var settingsService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Services.ISettingsService>();

            if (LightModeSwitcher.IsOn != App.Locator.IsLightMode)
            {
                var dialog = new MessageDialog(MessageContent, MessageTitle);
                dialog.Commands.Add(new UICommand("OK"));
                await dialog.ShowAsync();
            }
            
            settingsService.LightModeUserPermission = LightModeSwitcher.IsOn ? Services.ModeUserPermission.LightModeEnabled : Services.ModeUserPermission.LightModeDisabled;
            Pitch.Helpers.Logger.LogQueue.WriteToFile($"User light mode permission: {settingsService.LightModeUserPermission}");
        }
        private async void EditProfileButton_Click(object sender, RoutedEventArgs e)
        {
            
            await ApiClient.Shared.ViewUserProfileDialog(App.FabricEndpointData.profile_endpoint);
        }
        #endregion
    }
}
