﻿using System;
using Pitch.Helpers;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.FileMenu
{
    public sealed partial class UploaderControl : UserControl
    {
        private UploaderViewModel _viewModel;
        public UploaderViewModel ViewModel => _viewModel;

        public delegate void UploadProcess();
        public event UploadProcess UploadProcessStartedEvent;
        public event UploadProcess UploadProcessStoppedEvent;

        #region Life Cycle
        public UploaderControl(UploaderType uploaderType)
        {
            _viewModel = new UploaderViewModel(uploaderType);
            InitializeComponent();

            Loaded += UploaderControl_Loaded;
            if (uploaderType == UploaderType.Desktop)
            {
                UploadPanel.Visibility = Visibility.Collapsed;
                DesktopPanel.Visibility = Visibility.Visible;
                ExportProgress.Visibility = Visibility.Collapsed;
            }
        }
#if DEBUG
        ~UploaderControl()
        {
            System.Diagnostics.Debug.WriteLine("********************UploaderControl Destructor********************");
        }
#endif

        private void UploaderControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel.IsKaltura)
            {
                LinkSharePanel.Margin = new Thickness(0, -60, 0, 0);
            }
            ViewModel.Update();
            SharingPanel.Visibility = Visibility.Collapsed;
            ProgressPanel.Visibility = Visibility.Collapsed;
            UploadButton.Visibility = Visibility.Visible;
            ViewModel.Reset();
            Title.Text = ViewModel.Title;
            Tct.IsChecked = true;
        }
        #endregion
        public void Cancel()
        {
            ViewModel.Cancel();
        }
        public void Clean()
        {
            Loaded -= UploaderControl_Loaded;

            Bindings.StopTracking();
            ViewModel.Cleanup();
            _viewModel = null;
        }

        #region Callbacks
        private void Timer_Tick(object sender, object e)
        {
            if (_viewModel.Progress >= 100)
            {
                ExportProgress.Visibility = Visibility.Collapsed;
            }
        }

        private async void UploadButton_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Touchcast == null || ViewModel.IsKalturaNameError)
            {
                return;
            }

            UploadProcessStartedEvent?.Invoke();
            Progress.Visibility = Visibility.Visible;
            CancelButton.Visibility = Visibility.Visible;
            UploadButton.Visibility = Visibility.Collapsed;
            ProgressPanel.Visibility = Visibility.Visible;

            SharingPanel.Visibility = ViewModel.IsKaltura ? Visibility.Collapsed : Visibility.Visible;

            await ViewModel.Uploading();
            if (ViewModel.IsUploadingCanceled)
            {
                UploadButton.Visibility = Visibility.Visible;
                ProgressPanel.Visibility = Visibility.Collapsed;
            }
            else if (!ViewModel.IsUploadingFailed)
            {
                CancelButton.Visibility = Visibility.Collapsed;
                Progress.Visibility = Visibility.Collapsed;
            }

            UploadProcessStoppedEvent?.Invoke();
        }
        private void LinkCopyButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(LinkText.Text))
            {
                return;
            }

            ShowCopiedMessage(LinkCopiedText);
        }

        private void LinkEmbedCopyButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(LinkEmbedText.Text))
            {
                return;
            }

            ShowCopiedMessage(EmbedLinkCopiedText);
        }
        private void Title_TextChanged(object sender, TextChangedEventArgs e)
        {
            ViewModel.IsKalturaNameError = ViewModel.IsKaltura && string.IsNullOrEmpty(Title.Text);
        }
        private void TextBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            _viewModel.TryUpdateTouchcastData();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.CancelUploading();
            UploadButton.Visibility = Visibility.Visible;
            ProgressPanel.Visibility = Visibility.Collapsed;
        }
        private async void DesktopExport_Click(object sender, RoutedEventArgs e)
        {
            UploadProcessStartedEvent?.Invoke();

            var fileName = ViewModel.Touchcast.Title;
            if (String.IsNullOrEmpty(fileName))
            {
                fileName = Mp4.IsChecked.Value ? "New recording" : "New Touchcast";
            }

            var ext = (Mp4.IsChecked.Value) ? ".mp4" : FileToAppHelper.TouchcastExtension;
            var description = (Mp4.IsChecked.Value) ? "Video file": "Touchcast File";

            var file = await FileToAppHelper.SaveFileDialog(ext, description, fileName);
            if (file != null)
            {
                DesktopExport.IsEnabled = false;
                ExportProgress.Visibility = Visibility.Visible;
                await ViewModel.SaveFileLocally(file);
                Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticCommandTouchcastExported, Mp4.IsChecked.Value ? "video" : "tct");
            }

            ExportProgress.Visibility = Visibility.Collapsed;
            DesktopExport.IsEnabled = true;
            UploadProcessStoppedEvent?.Invoke();
        }
        #endregion

        #region Private Methods
        private void ShowCopiedMessage(UIElement messageControl)
        {
            messageControl.IsHitTestVisible = true;

            var fadeInAnimation = new DoubleAnimation
            {
                From = 0d,
                To = 1,
                Duration = new Duration(TimeSpan.FromMilliseconds(500)),
            };

            var storyboard = new Storyboard
            {
                RepeatBehavior = new RepeatBehavior(1),
                AutoReverse = true,
                Duration = new Duration(TimeSpan.FromSeconds(1.2)),
            };

            storyboard.Completed += (s, arg) =>
            {
                messageControl.IsHitTestVisible = false;
            };

            storyboard.Children.Add(fadeInAnimation);

            Storyboard.SetTarget(fadeInAnimation, messageControl);
            Storyboard.SetTargetProperty(fadeInAnimation, "Opacity");

            storyboard.Begin();
        }
        #endregion
    }
}
