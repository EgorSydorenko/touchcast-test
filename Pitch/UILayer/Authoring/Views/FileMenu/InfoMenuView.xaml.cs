﻿using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.FileMenu
{
    public sealed partial class InfoMenuView : UserControl
    {
        private InfoMenuViewModel _viewModel;
        public InfoMenuViewModel ViewModel => _viewModel;

        #region Life Cycle
        public InfoMenuView()
        {
            _viewModel = new InfoMenuViewModel();
            InitializeComponent();
        }

#if DEBUG
        ~InfoMenuView()
        {
            System.Diagnostics.Debug.WriteLine("********************* InfoMenuView Destructor *****************");
        }
#endif

        private void InfoMenuView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += InfoMenuView_Unloaded;
            _viewModel.UpdateInfo();
        }

        private void InfoMenuView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unloaded -= InfoMenuView_Unloaded;
            Bindings?.StopTracking();
            _viewModel?.Cleanup();
        }
        #endregion
    }
}
