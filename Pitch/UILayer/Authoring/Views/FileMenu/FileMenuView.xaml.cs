﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pitch.UILayer.Authoring.Views.FileMenu;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.FileMenu
{
    public sealed partial class FileMenuView : UserControl
    {
        #region Events

        public event EventHandler CloseProjectEvent;
        public event EventHandler NewProjectEvent;
        public event EventHandler OpenProjectEvent;
        public event EventHandler CloseMenuEvent;
        public event EventHandler LogoutMenuEvent;

        #endregion

        #region Fields

        private FileMenuViewModel _viewModel;
        private IDictionary<FileMenuKey, UIElement> _menuControls;
        private int _previousSelectedIndex;
        private SettingsMenuView _settingsMenuView;
        private FinalizeMenuView _finalizeMenuView;

        #endregion

        #region Properties

        public FileMenuViewModel ViewModel => _viewModel;
        public IDictionary<FileMenuKey, UIElement> MenuControls => _menuControls;
        public int PreviusSelectedIndex => _previousSelectedIndex;
        public bool IsOpened => Visibility == Visibility.Visible;

        #endregion

        #region Life Cycle

        public FileMenuView()
        {
            _viewModel = new FileMenuViewModel();
            _menuControls = new Dictionary<FileMenuKey, UIElement>();
            InitializeComponent();
            Loaded += FileMenuView_Loaded;
        }

#if DEBUG
        ~FileMenuView()
        {
            System.Diagnostics.Debug.WriteLine("****************FileMenuView Destructor****************");
        }
#endif

        private void FileMenuView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += FileMenuView_Unloaded;

            InitializeMenuControls();
            MenuListView.ItemsSource = ViewModel.MenuItems;
            MenuListView.SelectedIndex = 0;
            _previousSelectedIndex = 0;
        }

        private void FileMenuView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= FileMenuView_Loaded;
            Unloaded -= FileMenuView_Unloaded;
            _settingsMenuView.LogoutMenuEvent -= SettingsMenuView_LogoutMenuEvent;
            _settingsMenuView = null;

            _finalizeMenuView.FinalizeProcessStartedEvent -= FinalizeMenuView_FinalizeProcessStartedEvent;
            _finalizeMenuView.FinalizeProcessStoppedEvent -= FinalizeMenuView_FinalizeProcessStoppedEvent;
            _finalizeMenuView.Clean();
            _finalizeMenuView = null;
            MenuListView.ItemsSource = null;
            MenuControls.Clear();
            ViewModel?.Cleanup();
        }

        #endregion

        #region Public Methods

        public void PrepareBeforeShow(FileMenuKey menuKey)
        {
            var tmpList = ViewModel.MenuItems.ToList();
            var project = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Pitch.DataLayer.AuthoringSession>()?.Project;
            if (project != null && !project.TcMediaComposition.MediaClips.Any())
            {
                tmpList.Remove(tmpList.FirstOrDefault(i => i.Key == FileMenuKey.FinalizeMenu));
            }

            MenuListView.ItemsSource = tmpList;
            MenuListView.SelectedIndex = 0;
            _previousSelectedIndex = 0;
            MenuListView.SelectedItem = ViewModel.MenuItems.OfType<FileMenuItem>().FirstOrDefault(i => i.Key == menuKey);
        }

        public void PrepareBeforeHiding()
        {
            _finalizeMenuView.CancelExport();
            MenuListView.ItemsSource = null;
        }

        public void SwitchToPreviousMenu()
        {
            MenuListView.SelectedIndex = PreviusSelectedIndex;
        }

        #endregion

        #region Private Methods

        private void InitializeMenuControls()
        {
            MenuControls.Add(FileMenuKey.InfoMenu, new InfoMenuView());
            _settingsMenuView = new SettingsMenuView();
            _settingsMenuView.LogoutMenuEvent += SettingsMenuView_LogoutMenuEvent;
            MenuControls.Add(FileMenuKey.SettingsMenu, _settingsMenuView);
            _finalizeMenuView = new FinalizeMenuView();
            _finalizeMenuView.FinalizeProcessStartedEvent += FinalizeMenuView_FinalizeProcessStartedEvent;
            _finalizeMenuView.FinalizeProcessStoppedEvent += FinalizeMenuView_FinalizeProcessStoppedEvent;
            MenuControls.Add(FileMenuKey.FinalizeMenu, _finalizeMenuView);
        }
        #endregion

        #region Callbacks
        private void FinalizeMenuView_FinalizeProcessStoppedEvent()
        {
            BackButton.IsEnabled = true;
        }
        private void FinalizeMenuView_FinalizeProcessStartedEvent()
        {
            BackButton.IsEnabled = false;
        }
        private async void MenuListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _finalizeMenuView?.CancelExport();
            if (MenuListView.SelectedItem is FileMenuItem selectedItem)
            {
                if (MenuControls.TryGetValue(selectedItem.Key, out UIElement menuControl))
                {
                    MenuCaption.Text = selectedItem.Caption;
                    LayoutContent.Children.Clear();
                    LayoutContent.Children.Add(menuControl);
                }
                switch (selectedItem.Key)
                {
                    case FileMenuKey.NewMenu:
                        NewProjectEvent?.Invoke(this, EventArgs.Empty);
                        break;
                    case FileMenuKey.OpenMenu:
                        OpenProjectEvent?.Invoke(this, EventArgs.Empty);
                        break;
                    case FileMenuKey.CloseMenu:
                        CloseProjectEvent?.Invoke(this, EventArgs.Empty);
                        break;
                    case FileMenuKey.SaveMenu:
                    case FileMenuKey.SaveAsMenu:
                        bool result = await ViewModel.ExecuteMethodByKey(selectedItem.Key);
                        if (result == true)
                        {
                            CloseMenuEvent?.Invoke(this, EventArgs.Empty);
                        }
                        else
                        {
                            MenuListView.SelectedIndex = PreviusSelectedIndex;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            CloseMenuEvent?.Invoke(this, EventArgs.Empty);
        }

        private void SettingsMenuView_LogoutMenuEvent(object sender, EventArgs e)
        {
            LogoutMenuEvent?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
