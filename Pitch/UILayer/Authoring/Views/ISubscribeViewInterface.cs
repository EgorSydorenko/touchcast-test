﻿using Windows.UI.Xaml.Controls;

namespace Pitch.UILayer.Authoring.Views
{
    public abstract class ISubscribeView : UserControl
    {
        public abstract void Subscribe();
        public abstract void Unsubscribe();
        public abstract void Clean();
    }
}
