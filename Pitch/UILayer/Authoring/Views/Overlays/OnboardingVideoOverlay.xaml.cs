﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Overlays
{
    public sealed partial class OnboardingVideoOverlay : UserControl
    {
        #region Constants

        private readonly int _updateIntervalInMilliseconds = 100;
        private readonly double  _introOverlayScale = 0.25;

        #endregion

        #region Fields

        private bool _isMinimized;
        private bool _isUpdatingByTimer;
        private bool _isUserTimeChanged;
        private DispatcherTimer _updateTimer;
        private MediaElementState _previousPlayerState;
        private Panel _parentPanel;

        #endregion

        #region Life Cycle

        public OnboardingVideoOverlay(Panel parent)
        {
            _parentPanel = parent ?? throw new ArgumentNullException(nameof(parent));

            InitializeComponent();
            _updateTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(_updateIntervalInMilliseconds)
            };
            _updateTimer.Tick += UpdateTimer_Tick;
        }

#if DEBUG
        ~OnboardingVideoOverlay()
        {
            System.Diagnostics.Debug.WriteLine("****************OnboardingVideoOverlay Destructor****************");
        }
#endif

        private void OnboardingVideoOverlay_Loaded(object sender, RoutedEventArgs e)
        {
            _parentPanel.SizeChanged += ParentPanel_SizeChanged;
        }

        private void OnboardingVideoOverlay_Unloaded(object sender, RoutedEventArgs e)
        {
            _parentPanel.SizeChanged -= ParentPanel_SizeChanged;
            _updateTimer.Stop();
            Player.Stop();
        }

        #endregion

        #region Callbacks

        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            Minimize();
        }

        private void RestoreButton_Click(object sender, RoutedEventArgs e)
        {
            Restore();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void VolumeSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Player.Volume = VolumeSlider.Value / 100.0;
        }

        private void TimelineSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (_isUpdatingByTimer)
            {
                return;
            }
            if (Player.CanSeek)
            {
                if (_isUserTimeChanged == false)
                {
                    _previousPlayerState = Player.CurrentState;
                }
                _isUserTimeChanged = true;

                Player.Pause();
                Player.Position = new TimeSpan(0, 0, 0, 0, (int)TimelineSlider.Value);
            }
            UpdateTimeLine();
        }

        private void TimelineSlider_PointerCaptureLost(object sender, PointerRoutedEventArgs e)
        {
            if (_previousPlayerState == MediaElementState.Playing)
            {
                Player.Play();
            }
            _isUserTimeChanged = false;
        }

        private void PlayPauseButton_Click(object sender, RoutedEventArgs e)
        {
            PlayPause();
        }

        private void Player_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            if (_isMinimized == false)
            {
                PlayPause();
            }
        }

        private void Player_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            ControlsRoot.Visibility = (!_isMinimized 
                                       && Player.CurrentState != MediaElementState.Buffering 
                                       && Player.CurrentState != MediaElementState.Opening) ? Visibility.Visible : Visibility.Collapsed; 
            switch (Player.CurrentState)
            {
                case MediaElementState.Playing:
                    PlayIcon.Visibility = Visibility.Collapsed;
                    PauseIcon.Visibility = Visibility.Visible;
                    break;
                case MediaElementState.Paused:
                    PlayIcon.Visibility = Visibility.Visible;
                    PauseIcon.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void Player_MediaEnded(object sender, RoutedEventArgs e)
        {
            PlayIcon.Visibility = Visibility.Visible;
            PauseIcon.Visibility = Visibility.Collapsed;
        }

        private void Player_MediaOpened(object sender, RoutedEventArgs e)
        {
            TimelineSlider.Maximum = Player.NaturalDuration.TimeSpan.TotalMilliseconds;
            _updateTimer.Start();
        }

        private void UpdateTimer_Tick(object sender, object e)
        {
            if (_isUserTimeChanged)
            {
                return;
            }
            _isUpdatingByTimer = true;
            try
            { 
                UpdateTimeLine();
            }
            finally
            {
                _isUpdatingByTimer = false;
            }
        }

        private void Grid_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            if (_isMinimized)
            {
                Restore();
            }
        }

        private void ParentPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            CalculateTransform();
        }

        #endregion

        #region Private Methods

        private void PlayPause()
        {
            if (Player.CurrentState == MediaElementState.Playing && Player.CanPause)
            {
                Player.Pause();
            }
            else
            {
                if (Player.Position + TimeSpan.FromMilliseconds(10) >= Player.NaturalDuration)
                {
                    Player.Position = TimeSpan.FromSeconds(0);
                }
                Player.Play();
            }
        }

        private void UpdateTimeLine()
        {
            TimeElapsedBlock.Text = Player.Position.ToString(@"mm\:ss");
            TimeRemainingBlock.Text = (Player.NaturalDuration.TimeSpan - Player.Position).ToString(@"mm\:ss");
            TimelineSlider.Value = Player.Position.TotalMilliseconds;
        }

        private void Hide()
        {
            _parentPanel.Children.Remove(this);
            ResetTranform();
            SetNormalMode();
        }

        private void Minimize()
        {
            SetMinimizedMode();
            CalculateTransform();
            if (Player.CanPause)
            {
                Player.Pause();
            }
        }

        private void Restore()
        {
            SetNormalMode();
            ResetTranform();
            Player.Play();
        }

        private void CalculateTransform()
        {
            if (_isMinimized == false)
            {
                return;
            }
            if (RenderTransform is CompositeTransform transform)
            {
                transform.ScaleX = _introOverlayScale;
                transform.ScaleY = _introOverlayScale;
                transform.CenterX = _parentPanel.ActualWidth;
                transform.CenterY = _parentPanel.ActualHeight;
            }
        }

        private void ResetTranform()
        {
            if (RenderTransform is CompositeTransform transform)
            {
                transform.ScaleX = 1.0;
                transform.ScaleY = 1.0;
                transform.CenterX = 0;
                transform.CenterY = 0;
            }
        }

        private void SetMinimizedMode()
        {
            _isMinimized = true;
            ControlsRoot.Visibility = Visibility.Collapsed;
            ControlPanel.Visibility = Visibility.Collapsed;
            RestoreButton.Visibility = Visibility.Visible;
            BigCloseButton.Visibility = Visibility.Visible;
        }

        private void SetNormalMode()
        {
            _isMinimized = false;
            ControlsRoot.Visibility = Visibility.Visible;
            ControlPanel.Visibility = Visibility.Visible;
            RestoreButton.Visibility = Visibility.Collapsed;
            BigCloseButton.Visibility = Visibility.Collapsed;
        }

        #endregion
    }
}
