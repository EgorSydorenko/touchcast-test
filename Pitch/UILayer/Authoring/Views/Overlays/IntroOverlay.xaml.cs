﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.UILayer.Helpers.Messages;
using System.Collections.Generic;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Overlays
{
    public sealed partial class IntroOverlay : UserControl
    {
        private PointerEventHandler handler;
        private readonly List<String> _paths;

        public IntroOverlay()
        {
            InitializeComponent();

            _paths = new List<String>()
            {
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-01.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-02.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-03.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-04.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-05.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-06.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-07.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-08.png",
                "ms-appx:///Assets/SlideShow/AllClients/tcp-onboarding-09.png",
            };

            if (TfSdk.ApiClient.Shared.UserInfo.kalturaTokenInfo != null)
            {

            }
           
            Loaded += IntroOverlay_Loaded;
        }

        private void IntroOverlay_Loaded(object sender, RoutedEventArgs e)
        {
            foreach(var path in _paths)
            {
                var image = new Image
                {
                    Source = new BitmapImage(new Uri(path)),
                    Stretch = Windows.UI.Xaml.Media.Stretch.Uniform,
                    Margin = new Thickness(10, 10, 10, 10)
                };
                FlipView.Items.Add(image);
            }

            handler = new PointerEventHandler(StartWithIButton_PointerReleased);
            CloseButton.AddHandler(PointerReleasedEvent, handler, true);
            Unloaded += IntroOverlay_Unloaded;
        }

        private void IntroOverlay_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= IntroOverlay_Loaded;
            Unloaded -= IntroOverlay_Unloaded;
            CloseButton.RemoveHandler(PointerReleasedEvent, handler);
        }

        private void StartWithIButton_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(new HideIntroOverlayMessage());
        }
    }
}
