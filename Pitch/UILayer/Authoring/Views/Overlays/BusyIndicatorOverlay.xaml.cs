﻿using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Overlays
{
    public sealed partial class BusyIndicatorOverlay : UserControl
    {
        public string Message { get; set; }

        public string Title { get; set; }

        public BusyIndicatorOverlay(string message = null, string title = null)
        {
            Message = message;
            Title = title;
            InitializeComponent();
        }
    }
}
