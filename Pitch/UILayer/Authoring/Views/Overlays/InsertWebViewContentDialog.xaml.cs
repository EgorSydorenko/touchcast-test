﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Pitch.Shortcut;
using Pitch.DataLayer;
using Windows.System;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Authoring.Views.Overlays
{
    public enum InsertWebViewContentDialogResult
    {
        WebViewCreated,
        CreationCanceled
    }

    public sealed partial class InsertWebViewContentDialog : ContentDialog
    {
        #region Constants
        const int LoadingStartValue = 10;
        const int LoadingFinalyseValue = 60;
        const int Tick = 1;
        #endregion

        #region Private Fields
        private bool _canGoBack;
        private bool _canGoForward;
        private long _canGoBackToken;
        private long _canGoForwardToken;
        private WebView _webPreview;
        private bool _isFailOrEmptyPage;
        private readonly string googleSearchFormat = "https://www.google.com/search?q={0}";
        private string _navbarText;
        private string _url;
        private bool _isSetMode;
        private const string _regex = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
        private readonly Uri EmptyPageUrl = new Uri("ms-appx-web:///Resources/Styles/Html Pages/EmptyPage.html");
        #endregion

        #region Public Properties

        public InsertWebViewContentDialogResult Result { private set; get; }

        public bool IsSetMode
        {
            get
            {
                return _isSetMode;
            }
            set
            {
                _isSetMode = value;
            }
        }

        //public string NavbarText
        //{
        //    set
        //    {
        //        _navbarText = value;
        //    }
        //    get
        //    {
        //        return _navbarText;
        //    }
        //}

        public bool CanGoBack
        {
            get { return _canGoBack; }
            set
            {
                _canGoBack = value;
                BackButton.IsEnabled = _canGoBack;
            }
        }

        public bool CanGoForward
        {
            get { return _canGoForward; }
            set
            {
                _canGoForward = value;
                ForwardButton.IsEnabled = _canGoForward;
            }
        }

        #endregion

        #region Life Cycle

        public InsertWebViewContentDialog()
        {
            InitializeComponent();
            Loaded += WebViewPopupOverlay_Loaded;
        }

        public InsertWebViewContentDialog(string url) : this()
        {
            _url = url;
        }

#if DEBUG
        ~InsertWebViewContentDialog()
        {
            System.Diagnostics.Debug.WriteLine("******************* InsertWebViewContentDialog Destructor ********************");
        }
#endif
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Windows.UI.Colors.Transparent);
                }
            }
        }

        private void WebViewPopupOverlay_Loaded(object sender, RoutedEventArgs e)
        {
            Shortcut.Manager.Instance.IsEnabled = false;
            if (_isSetMode)
            {
                AcceptButton.Visibility = Visibility.Collapsed;
                SetButton.Visibility = Visibility.Visible;
            }
            _webPreview = new WebView();
            Grid.SetRow(_webPreview, 1);
            Grid.SetRowSpan(_webPreview, 2);
            LayoutGrid.Children.Add(_webPreview);
            Result = InsertWebViewContentDialogResult.CreationCanceled;
            SearchTextBox.Text = "";
            SearchTextBox.Focus(FocusState.Keyboard | FocusState.Pointer);
            BackButton.IsEnabled = false;
            ForwardButton.IsEnabled = false;
            ReloadButton.IsEnabled = false;
            AcceptButton.IsEnabled = false;
            _isFailOrEmptyPage = false;
            _webPreview.NavigationStarting += WebPreview_NavigationStarting;
            _webPreview.NavigationCompleted += WebPreview_NavigationCompleted;
            _webPreview.NavigationFailed += WebPreview_NavigationFailed;

            _canGoBackToken = _webPreview.RegisterPropertyChangedCallback(WebView.CanGoBackProperty, CanGoBackChanged);
            _canGoForwardToken = _webPreview.RegisterPropertyChangedCallback(WebView.CanGoForwardProperty, CanGoForwardChanged);

            LoadProgressBar.Maximum = LoadingFinalyseValue;
            LoadProgressBar.Value = 0;

            if (!String.IsNullOrEmpty(_url))
            {
                _webPreview.Navigate(new Uri(_url));
            }

            Unloaded += WebViewPopupOverlay_Unloaded;
        }

        private void WebViewPopupOverlay_Unloaded(object sender, RoutedEventArgs e)
        {
            _webPreview.UnregisterPropertyChangedCallback(WebView.CanGoBackProperty, _canGoBackToken);
            _webPreview.UnregisterPropertyChangedCallback(WebView.CanGoForwardProperty, _canGoForwardToken);
            _webPreview.NavigationCompleted -= WebPreview_NavigationCompleted;
            _webPreview.NavigationFailed -= WebPreview_NavigationFailed;
            _webPreview.NavigationStarting -= WebPreview_NavigationStarting;
            _webPreview.Navigate(EmptyPageUrl);
            LayoutGrid.Children.Remove(_webPreview);
            Unloaded -= WebViewPopupOverlay_Unloaded;
            Shortcut.Manager.Instance.IsEnabled = true;
        }

        #endregion

        #region Callbacks

        private void WebPreview_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            if (!_isFailOrEmptyPage)
            {
                SearchTextBox.Text = args.Uri.OriginalString;

                _navbarText = args.Uri.OriginalString;
            }
            LoadProgressBar.Value = LoadingStartValue;
            LoadProgressBar.Visibility = Visibility.Visible;
        }

        private void WebPreview_NavigationFailed(object sender, WebViewNavigationFailedEventArgs e)
        {
            if (e.Uri != null && e.Uri.ToString().Contains("https"))
            {
                var newUrl = e.Uri.ToString().Replace("https", "http");
                _webPreview.Navigate(new Uri(newUrl));
            }
            else
            {
                AcceptButton.IsEnabled = false;
                _isFailOrEmptyPage = true;
                ReloadButton.IsEnabled = false;
                NoConnection.Visibility = Visibility.Visible;
                _webPreview.Visibility = Visibility.Collapsed;
            }
        }

        private async void WebPreview_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            if (sender.Source != null && !_isFailOrEmptyPage)
            {
                AcceptButton.IsEnabled = true;
                SearchTextBox.Text = sender.Source.ToString();
                _navbarText = SearchTextBox.Text;
                ReloadButton.IsEnabled = true;
            }

            _isFailOrEmptyPage = false;

            LoadProgressBar.Value = LoadingFinalyseValue;
            await LoadProgressBar.Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(Tick));
                LoadProgressBar.Visibility = Visibility.Collapsed;
            });
        }

        private void NoConnection_TryToConnectEvent()
        {
            NoConnection.Visibility = Visibility.Collapsed;
            _webPreview.Visibility = Visibility.Visible;

            _webPreview.Refresh();
        }

        private void SearchTextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                string finalUri;
                var text = SearchTextBox.Text;

                if(Regex.IsMatch(text, _regex))
                {
                    if (!text.Contains("://"))
                    {
                        text = $"https://{text}";
                    }
                    SearchTextBox.Text = text;
                    finalUri = SearchTextBox.Text;
                }
                else
                {
                    finalUri = string.Format(googleSearchFormat, SearchTextBox.Text);
                }

                _navbarText = finalUri;
                try
                {
                    var uri = new Uri(finalUri);
                    _webPreview.Navigate(uri);
                }
                catch(Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }

            }
        }

        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            if (_webPreview.CanGoBack)
            {
                _webPreview.GoBack();
            }
        }

        private void Button_Forward_Click(object sender, RoutedEventArgs e)
        {
            if (_webPreview.CanGoForward)
            {
                _webPreview.GoForward();
            }
        }

        private void Button_Reload_Click(object sender, RoutedEventArgs e)
        {
            _webPreview.Refresh();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            Result = InsertWebViewContentDialogResult.WebViewCreated;
            Hide();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Result = InsertWebViewContentDialogResult.CreationCanceled;
            Hide();
        }

        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            _navbarText = null;
            Uri result;
            if (!String.IsNullOrEmpty(SearchTextBox.Text) && Uri.TryCreate(SearchTextBox.Text, UriKind.Absolute, out result) == true)
            {
                _navbarText = result.AbsoluteUri;
            }

            Result = InsertWebViewContentDialogResult.WebViewCreated;
            Hide();
        }

        private void AcceptButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Brush textForeground = Application.Current.Resources["AcceptTextBrush"] as SolidColorBrush;
            AcceptTextBox.Foreground = (AcceptButton.IsEnabled) ? textForeground : new SolidColorBrush(Colors.White);
        }

        #endregion

        #region Public Methods

        public async Task<UrlWithProperties> ProcessUrlWithProperties()
        {
            if (Uri.TryCreate(_navbarText, UriKind.Absolute, out Uri uri))
            {
                return await UrlWithProperties.Check(uri);
            }
            return null;
        }

        public void Destroy()
        {
            Loaded -= WebViewPopupOverlay_Loaded;
            WebViewPopupContainer.Children.Clear();
            DataContext = null;
        }

        #endregion

        #region Private Methods

        private void CanGoBackChanged(DependencyObject sender, DependencyProperty property)
        {
            var newValue = (bool)_webPreview.GetValue(property);
            CanGoBack = newValue;
        }

        private void CanGoForwardChanged(DependencyObject sender, DependencyProperty property)
        {
            var newValue = (bool)_webPreview.GetValue(property);
            CanGoForward = newValue;
        }

        #endregion
    }
}
