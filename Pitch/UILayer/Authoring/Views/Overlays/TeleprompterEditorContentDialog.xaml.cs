﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Linq;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Authoring.Views.Overlays
{
    public sealed partial class TeleprompterEditorContentDialog : ContentDialog
    {
        #region Private Fields
        private readonly WeakReference<AuthoringSession> _authoringSession;
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();
        #endregion

        #region Life Сycle 
        public TeleprompterEditorContentDialog()
        {
            InitializeComponent();
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(session);

            Opened += TeleprompterEditorContentDialog_Opened;
        }

#if DEBUG
        ~TeleprompterEditorContentDialog()
        {
            System.Diagnostics.Debug.WriteLine("******************* TeleprompterEditorContentDialog Destructor ********************");
        }
#endif
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Windows.UI.Colors.Transparent);
                }
            }
        }
        private void TeleprompterEditorContentDialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            PopulateShotsList();
        }
        #endregion

        #region Callbacks 

        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            Hide();
            ClearShotsList();
        }
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            ClearShotsList();
        }

        private void SaveButton_Clicked(object sender, RoutedEventArgs e)
        {
            foreach (TeleprompterItemView teleprompter in RootListView.Items)
            {
                teleprompter.SaveTeleprompterText();
            }
            Hide();
        }
        #endregion

        #region Private Methods

        private void PopulateShotsList()
        {
            if (AuthoringSession == null)
            {
                return;
            }

            foreach (var shot in AuthoringSession.Project.Shots)
            {
                RootListView.Items.Add(new TeleprompterItemView(shot));
            }
            var selectedView = RootListView.Items.FirstOrDefault(view => (view as TeleprompterItemView)?.Shot.Id == AuthoringSession.Project.ActiveShot.Id);
            if (selectedView is TeleprompterItemView)
            {
                (selectedView as TeleprompterItemView).IsNeedActivatedAfterLoading = true;
                RootListView.SelectedItem = selectedView;
            }
        }

        private void ClearShotsList()
        {
            RootListView.Items.Clear();
        }
        #endregion
    }
}
