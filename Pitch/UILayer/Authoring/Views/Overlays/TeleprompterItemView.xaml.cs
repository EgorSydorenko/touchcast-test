﻿using System;
using Pitch.DataLayer.Shots;
using Pitch.UILayer.Authoring.Views.Shots;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Overlays
{
    public sealed partial class TeleprompterItemView : UserControl
    {
        #region Constants

        private const string TeleprompterItemPlaceHolder = "TeleprompterItemPlaceHolder";
        private readonly string TelepromterPlaceHolder;

        #endregion

        #region Private Fields

        private bool _isNeedActivateAfterLoading;
        private WeakReference<Shot> _shot;
        private ShotPreviewItem _shotPreview;

        #endregion

        #region Public Properties

        public Shot Shot => _shot?.TryGetObject();


        public bool IsNeedActivatedAfterLoading
        {
            get
            {
                return _isNeedActivateAfterLoading;
            }
            set
            {
                _isNeedActivateAfterLoading = value;
            }
        }

		#endregion

		#region Life Cycle

		public TeleprompterItemView(Shot shot)
		{
			InitializeComponent();
            _shot = new WeakReference<Shot>(shot);
			Loaded += TeleprompterEditorOverlay_Loaded;

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            TelepromterPlaceHolder = loader.GetString(TeleprompterItemPlaceHolder);
        }

#if DEBUG
        ~TeleprompterItemView()
        {
            System.Diagnostics.Debug.WriteLine("****************TeleprompterItemView Destructor****************");
        }
#endif

        private void TeleprompterEditorOverlay_Loaded(object sender, RoutedEventArgs e)
		{
			InsertShotPreview();
            if (!String.IsNullOrEmpty(Shot?.TeleprompterText))
            {
                TeleprompterTextBox.Text = Shot?.TeleprompterText;
            }
            TeleprompterTextBox.PlaceholderText = TelepromterPlaceHolder;
            if (_isNeedActivateAfterLoading)
            {
                ActivateShotPreview();
            }

            Unloaded += TeleprompterEditorOverlay_Unloaded;
		}

		private void TeleprompterEditorOverlay_Unloaded(object sender, RoutedEventArgs e)
		{
			_shot = null;
            _shotPreview?.Clean();
            _shotPreview = null;
            Loaded -= TeleprompterEditorOverlay_Loaded;
            Unloaded -= TeleprompterEditorOverlay_Unloaded;
		}

		#endregion

		#region Callbacks

		private void TeleprompterTextBoxGotFocus(object sender, RoutedEventArgs e)
		{
            if (sender is TextBox teleprompter)
            {
                teleprompter.PlaceholderText = String.Empty;
            }
            if (_shotPreview != null)
            {
                _shotPreview.IsSelected = true;
            }
		}

		private void TeleprompterTextBoxLostFocus(object sender, RoutedEventArgs e)
		{
            if (sender is TextBox teleprompter)
            {
                teleprompter.PlaceholderText = TelepromterPlaceHolder;
            }
            if (_shotPreview != null)
            {
                _shotPreview.IsSelected = false;
            }
        }

		#endregion

        #region Private Methods

        private void InsertShotPreview()
        {
            Shot shot = Shot;
            if (shot != null)
            {
                _shotPreview = new ShotPreviewItem(Shot,false);
                _shotPreview.DisableContextMenu();
                Grid.SetColumn(_shotPreview, 0);
                LayoutGrid.Children.Add(_shotPreview);
            }
		}

        private void ActivateShotPreview()
        {
            _shotPreview.IsSelected = true;
            TeleprompterTextBox.Focus(FocusState.Programmatic);
            TeleprompterTextBox.SelectionStart = TeleprompterTextBox.Text.Length;
        }

        #endregion

        #region Public Methods

        public void SaveTeleprompterText()
		{
            Shot shot = Shot;
            if (shot != null)
            {
                shot.TeleprompterText = TeleprompterTextBox.Text.Trim(); //TODO Undo/Redo
            }
        }
        
		#endregion
	}
}
