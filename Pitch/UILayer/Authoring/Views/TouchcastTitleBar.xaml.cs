﻿using System;
using TfSdk;
using Pitch.DataLayer;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Authoring.Views
{
    public enum TitlebarBackgroundColorMode
    {
        Default,
        Dark,
        White,
        SignIn
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public delegate void ChangeMode(ApplicationState futureState);
    public sealed partial class TouchcastTitleBar
    {
        #region Constants

        private readonly string PitchHelpUrl = @"https://touchcastllc.atlassian.net/servicedesk/customer/portal/2";

        #endregion

        #region Private Field

        private double _textWidth;

        #endregion

        #region Properties
        object _navigationLock = new object();
        bool _navigationInprocess = false;
        public static readonly DependencyProperty BackButtonVisibilityProperty = DependencyProperty.Register(
            nameof(BackButtonVisibility), typeof(Visibility), typeof(TouchcastTitleBar), new PropertyMetadata(Visibility.Collapsed));

        public Visibility BackButtonVisibility
        {
            get
            {
                return (Visibility)GetValue(BackButtonVisibilityProperty);
            }
            set
            {
                SetValue(BackButtonVisibilityProperty, value);
            }
        }
        public static readonly DependencyProperty UndoRedoControlsVisibilityProperty = DependencyProperty.Register(
            nameof(UndoRedoControlsVisibility), typeof(Visibility), typeof(TouchcastTitleBar), new PropertyMetadata(Visibility.Collapsed));

        public Visibility UndoRedoControlsVisibility
        {
            get
            {
                return (Visibility)GetValue(UndoRedoControlsVisibilityProperty);
            }
            set
            {
                SetValue(UndoRedoControlsVisibilityProperty, value);
            }
        }
        public static readonly DependencyProperty UserDataVisibilityProperty = DependencyProperty.Register(
        nameof(UserDataVisibility), typeof(Visibility), typeof(TouchcastTitleBar), new PropertyMetadata(Visibility.Collapsed));

        public Visibility UserDataVisibility
        {
            get
            {
                return (Visibility)GetValue(UserDataVisibilityProperty);
            }
            set
            {
                SetValue(UserDataVisibilityProperty, value);
            }
        }
        public static readonly DependencyProperty ModeSwitcherVisibilityProperty = DependencyProperty.Register(
            nameof(ModeSwitcherVisibility), typeof(Visibility), typeof(TouchcastTitleBar), new PropertyMetadata(Visibility.Collapsed));
        public Visibility ModeSwitcherVisibility
        {
            get
            {
                return (Visibility)GetValue(ModeSwitcherVisibilityProperty);
            }
            set
            {
                SetValue(ModeSwitcherVisibilityProperty, value);
            }
        }

        public static readonly DependencyProperty HelpButtonVisibilityProperty = DependencyProperty.Register(
            nameof(HelpButtonVisibility), typeof(Visibility), typeof(TouchcastTitleBar), new PropertyMetadata(Visibility.Visible));
        public Visibility HelpButtonVisibility
        {
            get
            {
                return (Visibility)GetValue(HelpButtonVisibilityProperty);
            }
            set
            {
                SetValue(HelpButtonVisibilityProperty, value);
            }
        }

        public static readonly DependencyProperty IsPreparetionProperty = DependencyProperty.Register(
            nameof(IsPreparetion), typeof(bool), typeof(TouchcastTitleBar), new PropertyMetadata(false));
        public bool IsPreparetion
        {
            get
            {
                return (bool)GetValue(IsPreparetionProperty);
            }
            set
            {
                SetValue(IsPreparetionProperty, value);
            }
        }

        public static readonly DependencyProperty IsPitchProperty = DependencyProperty.Register(
            nameof(IsPitch), typeof(bool), typeof(TouchcastTitleBar), new PropertyMetadata(false));
        public bool IsPitch
        {
            get
            {
                return (bool)GetValue(IsPitchProperty);
            }
            set
            {
                SetValue(IsPitchProperty, value);
            }
        }

        public static readonly DependencyProperty IsEditingProperty = DependencyProperty.Register(
            nameof(IsEditing), typeof(bool), typeof(TouchcastTitleBar), new PropertyMetadata(false));
        public bool IsEditing
        {
            get
            {
                return (bool)GetValue(IsEditingProperty);
            }
            set
            {
                SetValue(IsEditingProperty, value);
            }
        }

        public UIElement MovementContainer => movementContainer;

        #endregion

        #region Events

        public delegate void ButtonPressed();
        public event ButtonPressed SignOutPressedEvent;
        public event ButtonPressed BackRequested;
        public event ButtonPressed UndoButtonPressedEvent;
        public event ButtonPressed RedoButtonPressedEvent;
        public event ChangeMode ChangeModeEvent;
        public event ButtonPressed ShowTrainingPressedEvent;

        #endregion

        #region Life Cycle
        public TouchcastTitleBar()
        {
            InitializeComponent();
            Loaded += TouchcastTitleBar_Loaded;
            Unloaded += TouchcastTitleBar_Unloaded;
        }

#if DEBUG
        ~TouchcastTitleBar()
        {
            System.Diagnostics.Debug.WriteLine("******************* TouchcastTitlebar Destructor **************************");
        }
#endif
        private void TouchcastTitleBar_Loaded(object sender, RoutedEventArgs e)
        {
            UserName.Text = String.Format("{0} {1}", ApiClient.Shared.UserInfo?.info?.data?.first_name ?? String.Empty, ApiClient.Shared.UserInfo?.info?.data?.last_name ?? String.Empty);
            var uri = ApiClient.Shared.UserInfo?.info?.data?.avatar_small;
            if (!String.IsNullOrEmpty(uri) && Uri.IsWellFormedUriString(uri, UriKind.Absolute))
            {
                var bitmap = new BitmapImage(new Uri(uri));
                UserAvatar.Source = bitmap;
                UserAvatar.Visibility = Visibility.Visible;
            }
            Preparetion.Click += Preparetion_Click;
            Pitch.Click += Pitch_Click;
            Editing.Click += Editing_Click;
            SizeChanged += TouchcastTitleBar_SizeChanged;
            Title.SizeChanged += Title_SizeChanged;
        }

        private void TouchcastTitleBar_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= TouchcastTitleBar_Loaded;
            Unloaded -= TouchcastTitleBar_Unloaded;
            Unsubscribe();
        }

        #endregion

        #region Public Methods

        public void Unsubscribe()
        {
            Preparetion.Click -= Preparetion_Click;
            Pitch.Click -= Pitch_Click;
            Editing.Click -= Editing_Click;
            SizeChanged -= TouchcastTitleBar_SizeChanged;
            Title.SizeChanged -= Title_SizeChanged;
        }

        public void UpdateTitle(string title = null)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (!string.IsNullOrEmpty(title) && Title.Text != title)
                {
                    Title.Text = title;
                }
                UpdateLayout();
                var offset = ButtonsContainer.ActualWidth;
                var x = (ActualWidth - _textWidth) / 2;
                Title.Margin = new Thickness(x - offset, 0, 0, 0);
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        public void UpdateUndoRedoButtons(bool isUndoEnabled, bool isRedoEnabled)
        {
            UndoButton.IsEnabled = isUndoEnabled;
            RedoButton.IsEnabled = isRedoEnabled;
        }

        public void UpdateModeSwitcherState(bool isPitchEnabled, bool isEditEnabled)
        {
            if (App.Locator.AppState == ApplicationState.Preparation)
            {
                IsPreparetion = true;
                IsPitch = false;
                IsEditing = false;
            }
            else if (App.Locator.AppState == ApplicationState.Editing)
            {
                IsPreparetion = false;
                IsPitch = false;
                IsEditing = true;
            }
            else if (App.Locator.AppState == ApplicationState.Recording)
            {
                IsPreparetion = false;
                IsPitch = true;
                IsEditing = false;
            }

            Pitch.IsEnabled = isPitchEnabled;
            Editing.IsEnabled = isEditEnabled;
        }

        public void SetDarkMode()
        {
            App.SetupTitleBarColorScheme(TitlebarBackgroundColorMode.Dark);
            if (Application.Current.Resources["TitlebarDarkBackgroundBrush"] is Brush darkBrush)
            {
                RootGrid.Background = darkBrush;
                MovementRectangle.Fill = darkBrush;
            }
        }

        public void SetDefaultMode()
        {
            App.SetupTitleBarColorScheme(TitlebarBackgroundColorMode.Default);
            if (Application.Current.Resources["TitlebarBackgroundBrush"] is Brush defaultBrush)
            {
                RootGrid.Background = defaultBrush;
                MovementRectangle.Fill = defaultBrush;
            }
        }

        public void SetSignInMode()
        {
            App.SetupTitleBarColorScheme(TitlebarBackgroundColorMode.SignIn);
            if (Application.Current.Resources["SignInBackgroundBrush"] is Brush darkBrush)
            {
                RootGrid.Background = darkBrush;
                MovementRectangle.Fill = darkBrush;
                Title.Foreground = darkBrush;
                BackButton.Foreground = new SolidColorBrush(Colors.White);
            }
        }

        public void SetLoginMode()
        {
            App.SetupTitleBarColorScheme(TitlebarBackgroundColorMode.White);
            if (Application.Current.Resources["SignInBackgroundBrush"] is Brush darkBrush)
            {
                RootGrid.Background = new SolidColorBrush(Colors.White);
                MovementRectangle.Fill = new SolidColorBrush(Colors.White);
                Title.Foreground = darkBrush;
                BackButton.Foreground = darkBrush;
            }
        }

        public void SetShowTrainingButtonVisibility(bool isVisible)
        {
            ShowTrainingButton.Visibility = isVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion

        #region Callbacks

        private void Title_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _textWidth = e.NewSize.Width;
        }

        private void TouchcastTitleBar_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateTitle();
        }

        private void SignOutButton_Click(object sender, RoutedEventArgs e)
        {
            SignOutPressedEvent?.Invoke();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            BackRequested?.Invoke();
        }

        private void UndoButton_OnClick(object sender, RoutedEventArgs e)
        {
            UndoButtonPressedEvent?.Invoke();
        }

        private void RedoButton_OnClick(object sender, RoutedEventArgs e)
        {
            RedoButtonPressedEvent?.Invoke();
        }

        private void Editing_Click(object sender, RoutedEventArgs e)
        {
            lock (_navigationLock)
            {
                if (App.Locator.AppState != ApplicationState.Editing)
                {
                    if (_navigationInprocess) return;
                    _navigationInprocess = true;

                    ChangeModeEvent?.Invoke(ApplicationState.Editing);
                }
            }
        }

        private void Pitch_Click(object sender, RoutedEventArgs e)
        {
            lock (_navigationLock)
            {
                if (App.Locator.AppState != ApplicationState.Recording)
                {
                    if (_navigationInprocess) return;
                    _navigationInprocess = true;

                    ChangeModeEvent?.Invoke(ApplicationState.Recording);
                }
            }
        }

        private void Preparetion_Click(object sender, RoutedEventArgs e)
        {
            lock (_navigationLock)
            {
                if (App.Locator.AppState != ApplicationState.Preparation)
                {
                    if (_navigationInprocess) return;
                    _navigationInprocess = true;

                    ChangeModeEvent?.Invoke(ApplicationState.Preparation);
                }
            }
        }

        private async void GetHelpItem_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri(PitchHelpUrl));
            GetHelpFlyout.Hide();
        }

        private void ShowTrainingButton_Click(object sender, RoutedEventArgs e)
        {
            ShowTrainingPressedEvent?.Invoke();
            GetHelpFlyout.Hide();
        }

        #endregion
    }
}
