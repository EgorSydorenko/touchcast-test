﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views
{
    public sealed partial class TeleprompterView
	{
        #region Constants

        private const string TeleprompterPlaceholderText = "TeleprompterPlaceholderText";
        private readonly string PlaceholderText;

        #endregion

        #region Private Fields

        private readonly WeakReference<AuthoringSession> _authoringSession;
		private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();


        #endregion

        #region Life Cycle

        public TeleprompterView()
		{
			var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
			_authoringSession = new WeakReference<AuthoringSession>(session);
			InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            PlaceholderText = loader.GetString(TeleprompterPlaceholderText);
            Loaded += TeleprompterView_Loaded;
		}

#if DEBUG
		~TeleprompterView()
		{
			System.Diagnostics.Debug.WriteLine("******************** TeleprompterView Destructor********************");
		}
#endif

		private void TeleprompterView_Loaded(object sender, RoutedEventArgs e)
		{
			TeleprompterTextBox.Text = GetTeleprompterText();
            TeleprompterTextBox.PlaceholderText = PlaceholderText;
			Unloaded += TeleprompterView_Unloaded;
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.AuthroingSessionWasInitialized += AuthoringSession_AuthroingSessionWasInitialized;
				authoringSession.AuthoringSessionWillBeCleaned += AuthoringSession_AuthoringSessionWillBeCleaned;
			}
		}

		private void TeleprompterView_Unloaded(object sender, RoutedEventArgs routedEventArgs)
		{
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.AuthroingSessionWasInitialized -= AuthoringSession_AuthroingSessionWasInitialized;
				authoringSession.AuthoringSessionWillBeCleaned -= AuthoringSession_AuthoringSessionWillBeCleaned;
			}
			Unsubscribe();

			Loaded -= TeleprompterView_Loaded;
			Unloaded -= TeleprompterView_Unloaded;
			TeleprompterTextBox.LostFocus -= TeleprompterTextBox_LostFocus;
		}

		#endregion

		#region Callbacks

		private Task AuthoringSession_AuthoringSessionWillBeCleaned()
		{
			Unsubscribe();
            return Task.CompletedTask;
		}

		private Task AuthoringSession_AuthroingSessionWasInitialized()
		{
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
                if (authoringSession.Project != null)
                {
                    authoringSession.Project.ActiveShotWillBeChanged += Project_ActiveShotWillBeChanged;
                    authoringSession.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                    authoringSession.Project.ActiveShot.TeleprompterTextWasChanged += ActiveShot_TeleprompterTextWasChanged;
                    TeleprompterTextBox.Text = GetTeleprompterText();
                }
            }

            return Task.CompletedTask;
        }

        private void Project_ActiveShotWillBeChanged()
		{
            SaveTeleprompterText(TeleprompterTextBox.Text);
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.Project.ActiveShot.TeleprompterTextWasChanged -= ActiveShot_TeleprompterTextWasChanged;
			}
		}

		private void Project_ActiveShotWasChanged()
		{
			TeleprompterTextBox.Text = GetTeleprompterText();
            TeleprompterTextBox.PlaceholderText = PlaceholderText;
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.Project.ActiveShot.TeleprompterTextWasChanged += ActiveShot_TeleprompterTextWasChanged;
			}
		}

		private void ActiveShot_TeleprompterTextWasChanged()
		{
			TeleprompterTextBox.Text = GetTeleprompterText();
		}

        private void TeleprompterTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TeleprompterTextBox.PlaceholderText = String.Empty;
        }

        private void TeleprompterTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (authoringSession.Project.ActiveShot.TeleprompterText != TeleprompterTextBox.Text)
                {
                    authoringSession.Project.ActiveShot.SetTeleprompterTextWithUndoRedo(TeleprompterTextBox.Text);
                }
            }
            TeleprompterTextBox.PlaceholderText = String.IsNullOrEmpty(TeleprompterTextBox.Text) ? PlaceholderText : String.Empty;
        }
		
		#endregion

		#region Private Methods

		private void SaveTeleprompterText(string teleprompterText)
		{
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.Project.ActiveShot.TeleprompterText = teleprompterText;
			}
		}

		private string GetTeleprompterText()
		{
            string text = null;
			var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                text = authoringSession.Project?.ActiveShot?.TeleprompterText;
            }

            return text ?? String.Empty;
		}

		private void Unsubscribe()
		{
			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
                if (authoringSession.Project != null)
                {
                    authoringSession.Project.ActiveShotWillBeChanged -= Project_ActiveShotWillBeChanged;
                    authoringSession.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                    if (authoringSession.Project.ActiveShot != null)
                    {
                        authoringSession.Project.ActiveShot.TeleprompterTextWasChanged -= ActiveShot_TeleprompterTextWasChanged;
                    }
                }
			}
            TeleprompterTextBox.Text = String.Empty;
        }

        #endregion
    }
}
