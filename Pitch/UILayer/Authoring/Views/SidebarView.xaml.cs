﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Shots;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Pitch.UILayer.Authoring.Views.Shots;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views
{
    public delegate Task ShowSideBar(double from, double to);

    public sealed partial class SidebarView : UserControl
    {
        public static readonly DependencyProperty IsAuthoringModeProperty = DependencyProperty.Register(
            nameof(IsAuthoringMode), typeof(bool), typeof(SidebarView), new PropertyMetadata(false));

        public event ShowSideBar ShowSideBarEvent;

        public bool IsAuthoringMode
        {
            get { return (bool)GetValue(IsAuthoringModeProperty); }
            set
            {
                SetValue(IsAuthoringModeProperty, value);
                Background = new SolidColorBrush(
                    value ? Color.FromArgb(0xff, 0x3d, 0x3d, 0x3d)
                    : Color.FromArgb(0xff, 0x50, 0x50, 0x50));
                ImagePanel.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        #region Private Fields
        private WeakReference<AuthoringSession> _authoringSession;
        private bool _isDragStarted;
        private double _hidedShift;
        private readonly Models.AsyncLock _asyncLock = new Models.AsyncLock(1);

        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();


        #endregion

        #region Life Cycle

        public SidebarView()
        {
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(session);
            InitializeComponent();

            Loaded += SidebarView_Loaded;
        }

#if DEBUG
        ~SidebarView()
        {
            System.Diagnostics.Debug.WriteLine("******************** SidebarView Destructor********************");
        }
#endif

        private void SidebarView_Loaded(object sender, RoutedEventArgs e)
        {
            if (!IsAuthoringMode)
            {
                _hidedShift = ImagePanel.ActualWidth - ActualWidth;
                RenderTransform = new CompositeTransform()
                {
                    TranslateX = _hidedShift,
                    TranslateY = 0,
                    ScaleX = 1,
                    ScaleY = 1
                };
                Shortcut.Manager.Instance.RecordingShortcutManager.MoveDownShortcutEvent += ShortcutsManager_NextSceneShortcutEvent;
                Shortcut.Manager.Instance.RecordingShortcutManager.MoveRightShortcutEvent += ShortcutsManager_NextSceneShortcutEvent;
                Shortcut.Manager.Instance.RecordingShortcutManager.MoveUpShortcutEvent += ShortcutsManager_PrevSceneShortcutEvent;
                Shortcut.Manager.Instance.RecordingShortcutManager.MoveLeftShortcutEvent += ShortcutsManager_PrevSceneShortcutEvent;
            }
            else
            {
                Shortcut.Manager.Instance.VappShortcutsManager.MoveDownShortcutEvent += ShortcutsManager_NextSceneShortcutEvent;
                Shortcut.Manager.Instance.VappShortcutsManager.MoveRightShortcutEvent += ShortcutsManager_NextSceneShortcutEvent;
                Shortcut.Manager.Instance.VappShortcutsManager.MoveUpShortcutEvent += ShortcutsManager_PrevSceneShortcutEvent;
                Shortcut.Manager.Instance.VappShortcutsManager.MoveLeftShortcutEvent += ShortcutsManager_PrevSceneShortcutEvent;
                Shortcut.Manager.Instance.FormatShortcutsManager.MoveDownShortcutEvent += ShortcutsManager_NextSceneShortcutEvent;
                Shortcut.Manager.Instance.FormatShortcutsManager.MoveRightShortcutEvent += ShortcutsManager_NextSceneShortcutEvent;
                Shortcut.Manager.Instance.FormatShortcutsManager.MoveUpShortcutEvent += ShortcutsManager_PrevSceneShortcutEvent;
                Shortcut.Manager.Instance.FormatShortcutsManager.MoveLeftShortcutEvent += ShortcutsManager_PrevSceneShortcutEvent;
            }
            Unloaded += SidebarView_Unloaded;
            ShotListView.IsItemClickEnabled = true;
            ShotListView.ItemClick += ShotListView_ItemClick;
            var session = AuthoringSession;
            if (session != null)
            {
                if (IsAuthoringMode)
                {
                    session.AuthroingSessionWasInitialized += AuthoringSession_AuthroingSessionWasInitialized;
                    session.AuthoringSessionWillBeCleaned += AuthoringSession_AuthoringSessionWillBeCleaned;
                }
                if (session.ShotManager != null && !IsAuthoringMode)
                {
                    session.ShotManager.ShotWasDisplayed += ShotManager_ShotWasDisplayed;
                }
            }
        }

        private async void ShotListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(e.ClickedItem is ShotPreviewItem item)
            {
                item.Focus(FocusState.Programmatic);
                var index = ShotListView.Items.IndexOf(item);
                var session = AuthoringSession;
                if(session != null)
                {
                    var shot = session.Project.ShotAt(index);
                    if(shot != null)
                    {
                        session.ShotManager.ShowShot(shot);
                    }
                }
                    
            }
        }

        private void SidebarView_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
            Shortcut.Manager.Instance.VappShortcutsManager.MoveDownShortcutEvent -= ShortcutsManager_NextSceneShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveRightShortcutEvent -= ShortcutsManager_NextSceneShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveUpShortcutEvent -= ShortcutsManager_PrevSceneShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveLeftShortcutEvent -= ShortcutsManager_PrevSceneShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveDownShortcutEvent -= ShortcutsManager_NextSceneShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveRightShortcutEvent -= ShortcutsManager_NextSceneShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveUpShortcutEvent -= ShortcutsManager_PrevSceneShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveLeftShortcutEvent -= ShortcutsManager_PrevSceneShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveDownShortcutEvent -= ShortcutsManager_NextSceneShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveRightShortcutEvent -= ShortcutsManager_NextSceneShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveUpShortcutEvent -= ShortcutsManager_PrevSceneShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveLeftShortcutEvent -= ShortcutsManager_PrevSceneShortcutEvent;
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized -= AuthoringSession_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned -= AuthoringSession_AuthoringSessionWillBeCleaned;
            }
            ShotListView.ItemClick -= ShotListView_ItemClick;
            Clear();

            Loaded -= SidebarView_Loaded;
            Unloaded -= SidebarView_Unloaded;
        }

        #endregion

        #region Public Methods

        public void CheckReadyShots()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                ShotPreviewItem selectedView = null;
                foreach (var shot in session.Project.Shots)
                {
                    var view = new ShotPreviewItem(shot, true);
                    ShotListView.Items.Add(view);
                    if (view.Shot.Id == session.Project.ActiveShot.Id)
                    {
                        selectedView = view;
                        view.IsSelected = true;
                    }
                    else
                    {
                        view.ShotWillBeDeativated(false);
                    }
                }

                ShotListView.SelectedItem = selectedView;
                UpdateShotsIndexes();
            }
        }

        public void DisableEditingScenes()
        {
            ShotListView.CanDrag = false;
            ShotListView.CanDragItems = false;
            ShotListView.CanReorderItems = false;
            ShotListView.AllowDrop = false;
            foreach (var item in ShotListView.Items)
            {
                (item as ShotPreviewItem)?.DisableContextMenu();
            }
        }

        #endregion

        #region Callbacks

        private Task AuthoringSession_AuthoringSessionWillBeCleaned()
        {
            Clear();
            return Task.CompletedTask;
        }

        private Task AuthoringSession_AuthroingSessionWasInitialized()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project != null)
                {
                    session.Project.ShotWasCreated += Project_ShotWasCreated;
                    session.Project.ShotWasRemoved += Project_ShotWasRemoved;
                    session.Project.ShotWasMoved += Project_ShotWasMoved;
                    session.ShotManager.ShotWasDisplayed += ShotManager_ShotWasDisplayed;
                }
            }

            return Task.CompletedTask;
        }

        private Task Project_ShotWasCreated(Shot shot)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                var shotPreview = new ShotPreviewItem(shot, true);
                if(session.Project.ActiveShot != shot)
                {
                    shotPreview.ShotWillBeDeativated(false);
                }
                int insertIndex = 0;
                while (insertIndex < ShotListView.Items.Count)
                {
                    var model = (ShotListView.Items[insertIndex] as ShotPreviewItem).Shot;
                    if (session.Project.Shots.IndexOf(model) > session.Project.Shots.IndexOf(shot))
                    {
                        break;
                    }
                    ++insertIndex;
                }
                
                ShotListView.Items.Insert(insertIndex, shotPreview);

                UpdateShotsIndexes();
            }

            return Task.CompletedTask;
        }

        private Task Project_ShotWasRemoved(Shot shot)
        {
            var shotPreview = ShotListView.Items.OfType<ShotPreviewItem>().FirstOrDefault(x => x.Shot == shot);
            if (shotPreview != null)
            {
                shotPreview.Clean();
                ShotListView.Items.Remove(shotPreview);
                UpdateShotsIndexes();
            }
            return Task.CompletedTask;
        }

        private Task Project_ShotWasMoved(Shot shot)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                ShotPreviewItem movedShotPreview = ShotListView.Items.OfType<ShotPreviewItem>().FirstOrDefault(x => x.Shot == shot);
                if (movedShotPreview == null)
                {
                    return Task.CompletedTask;
                }
                int oldPreviewIndex = ShotListView.Items.IndexOf(movedShotPreview);
                int newPreviewIndex = session.Project.Shots.IndexOf(shot);
                if (oldPreviewIndex == newPreviewIndex || oldPreviewIndex == -1 || newPreviewIndex == -1)
                {
                    return Task.CompletedTask;
                }
                ShotListView.Items.Remove(movedShotPreview);
                ShotListView.Items.Insert(newPreviewIndex, movedShotPreview);
                UpdateShotsIndexes();
            }

            return Task.CompletedTask;
        }

        private void UpdateShotsIndexes()
        {
            foreach (var item in ShotListView.Items)
            {
                (item as ShotPreviewItem)?.CheckIndex();
            }
        }

        private async Task ShotManager_ShotWasDisplayed(Shot shot)
        {
            await _asyncLock.WaitAsync();
            try
            {
                var shotPreview = ShotListView.Items.FirstOrDefault(x => (x is ShotPreviewItem) && (x as ShotPreviewItem).Shot == shot);
                if (shotPreview != null && !(shotPreview as ShotPreviewItem).IsSelected)
                {

                    await UnselectShotPreview();
                    ShotListView.SelectedItem = shotPreview;
                    (shotPreview as ShotPreviewItem).IsSelected = true;
                    await (shotPreview as ShotPreviewItem).ShotWillBeActivated();

                }
            }
            finally
            {
                _asyncLock.Release();
            }
        }

        private async void ShotListView_DragItemsCompleted(ListViewBase sender, DragItemsCompletedEventArgs args)
        {
            var movedView = (ShotPreviewItem)args.Items.First();

            var newItemIndex = ShotListView.Items.IndexOf(movedView);
            var session = AuthoringSession;
            if (session != null)
            {
                var oldIndex = session.Project.Shots.IndexOf(movedView.Shot);
                await session.Project.MoveShot(oldIndex, newItemIndex);
                UpdateShotsIndexes();
            }
            _isDragStarted = false;
        }

        private void ShotListView_DragItemsStarting(object sender, DragItemsStartingEventArgs e)
        {
            _isDragStarted = true;
        }

        private async void ImagePanel_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var isHideList = (RenderTransform as CompositeTransform).TranslateX == 0;
            HideListImage.Visibility = isHideList ? Visibility.Collapsed : Visibility.Visible;
            ShowListImage.Visibility = isHideList ? Visibility.Visible : Visibility.Collapsed;

            var from = isHideList ? 0 : _hidedShift;
            var to = isHideList ? _hidedShift : 0;

            await ShowSideBarEvent?.Invoke(from, to);
        }

        private async Task<bool> ShortcutsManager_NextSceneShortcutEvent()
        {
            return await ShowShot(true);
        }

        private async Task<bool> ShortcutsManager_PrevSceneShortcutEvent()
        {
            return await ShowShot(false);
        }

        private async Task<bool> ShowShot(bool isNext)
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (authoringSession.ShotManager.ActiveShotView.IsAnyVappSelected())
                    return await Task.FromResult(false);

                var shot = isNext ? authoringSession.Project.NextShot() : authoringSession.Project.PrevShot();
                if (shot != null)
                {
                    var result = await authoringSession.ShotManager.ShowShot(shot);
                    if(result)
                    {
                        return await Task.FromResult(true);
                    }
                }
            }
            
            return await Task.FromResult(false);
        }

        public void UpdateToCurrentAppState()
        {
            ShotListView.Visibility = App.Locator.AppState == ApplicationState.Preparation ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion

        #region Private Methods

        private void Clear()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project != null)
                {
                    session.Project.ShotWasCreated -= Project_ShotWasCreated;
                    session.Project.ShotWasRemoved -= Project_ShotWasRemoved;
                    session.Project.ShotWasMoved -= Project_ShotWasMoved;
                }
                if (session.ShotManager != null)
                {
                    session.ShotManager.ShotWasDisplayed -= ShotManager_ShotWasDisplayed;
                }
            }
            var shotPreviews = ShotListView.Items.OfType<ShotPreviewItem>();
            foreach (var preview in shotPreviews)
            {
                preview.Clean();
            }
            ShotListView.Items.Clear();
        }

        private async Task UnselectShotPreview()
        {
            var selectedPreview = ShotListView.Items.OfType<ShotPreviewItem>().FirstOrDefault(s => s.IsSelected);

            if (selectedPreview != null)
            {
                selectedPreview.IsSelected = false;
                await selectedPreview.ShotWillBeDeativated(true);
            }
        }

        private bool IsSelectedVapp()
        {
            var result = false;
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                result = authoringSession.ShotManager.ActiveShotView.IsAnyVappSelected();
            }
            return result;
        }

        #endregion
    }
}
