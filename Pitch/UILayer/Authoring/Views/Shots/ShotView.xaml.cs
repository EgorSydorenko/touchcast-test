﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.Shots;
using Pitch.Helpers.Logger;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views
{
    class Inking
    {
        private InkCanvas _inkCanvas;
        private FrameworkElement _inkToolbar;

        public InkCanvas InkCanvas => _inkCanvas;
        public FrameworkElement InkToolbar => _inkToolbar;

        public Inking()
        {
            _inkCanvas = new InkCanvas
            {
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
            };
            Canvas.SetZIndex(_inkCanvas, 10000);

            _inkCanvas.InkPresenter.InputDeviceTypes =
                   Windows.UI.Core.CoreInputDeviceTypes.Mouse |
                   Windows.UI.Core.CoreInputDeviceTypes.Pen |
                   Windows.UI.Core.CoreInputDeviceTypes.Touch;

            var backgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Transparent);
            var inkToolbar = new InkToolbar();
            inkToolbar.Orientation = Orientation.Vertical;
            inkToolbar.Background = backgroundBrush;
            inkToolbar.Children.Clear();

            var ballPointBtn = new InkToolbarBallpointPenButton();
            ballPointBtn.Background = backgroundBrush;
            //hacky approach to select bright color
            ballPointBtn.SelectedBrushIndex = 8;
            inkToolbar.Children.Add(ballPointBtn);
            var pencilBtn = new InkToolbarPencilButton();
            pencilBtn.Background = backgroundBrush;
            //hacky approach to select bright color
            pencilBtn.SelectedBrushIndex = 8;
            inkToolbar.Children.Add(pencilBtn);
            var higlLiterBtn = new InkToolbarHighlighterButton();
            higlLiterBtn.Background = backgroundBrush;
            inkToolbar.Children.Add(higlLiterBtn);
            var eraseBtn = new InkToolbarEraserButton();
            eraseBtn.Background = backgroundBrush;
            inkToolbar.Children.Add(eraseBtn);
            var rulerBtn = new InkToolbarStencilButton();
            rulerBtn.Background = backgroundBrush;
            inkToolbar.Children.Add(rulerBtn);

            inkToolbar.TargetInkCanvas = _inkCanvas;
            _inkToolbar = inkToolbar;
        }

#if DEBUG
        ~Inking()
        {
            System.Diagnostics.Debug.WriteLine("******************** Inking Destructor********************");
        }
#endif

        public void Clean()
        {
            if (_inkToolbar is InkToolbar inkTlbr)
            {
                inkTlbr.Children.Clear();
            }
            _inkCanvas.InkPresenter.IsInputEnabled = false;
            _inkCanvas.InkPresenter.InputDeviceTypes = Windows.UI.Core.CoreInputDeviceTypes.None;
            _inkCanvas = null;
            _inkToolbar = null;
        }
    }

    public sealed partial class ShotView : ISubscribeView
    {
        public delegate void BaseLayoutViewEvent(BaseLayoutView view);

        #region Private Fields
        private Shot _shot;
        private List<BaseLayoutView> _layoutViews;
        private WeakReference<ComposingManager> _composingManager;
        private bool _isShowGuideLines = true;
        private Inking _inking;
        #endregion

        #region Events

        public event BaseLayoutViewEvent LayoutViewSelectedEvent;
        public event BaseLayoutViewEvent LayoutViewWasCreatedEvent;

        #endregion

        #region Properies

        public Shot Shot => _shot;

        public List<BaseLayoutView> LayoutViews => _layoutViews;

        public bool IsShowGuideLines
        {
            get => _isShowGuideLines;
            set => _isShowGuideLines = value;
        }

        private ComposingManager ComposingManager =>_composingManager?.TryGetObject();

        #endregion

        #region Life Cycle

        public ShotView(Shot shot)
        {
            _shot = shot;
            _layoutViews = new List<BaseLayoutView>();
            var composingManager = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<ComposingManager>();
            _composingManager = new WeakReference<ComposingManager>(composingManager);
            InitializeComponent();
            Loaded += ShotView_Loaded;

            HelperLines.LayoutApps = LayoutRoot;
        }

#if DEBUG
        ~ShotView()
        {
            System.Diagnostics.Debug.WriteLine("******************** ShotView Destructor********************");
        }
#endif

        private void ShotView_Loaded(object sender, RoutedEventArgs e)
        {
            Clip = new RectangleGeometry()
            {
                Rect = new Windows.Foundation.Rect(0, 0, Services.MediaMixerService.VideoSize.Width, Services.MediaMixerService.VideoSize.Height)
            };
            Subscribe();
        }

        private void ShotView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ShotView_Loaded;
            _inking?.Clean();
            _inking = null;
            Unsubscribe();
        }
        #endregion

        #region Private Methods

        private async void OutterLayout_DragEnter(object sender, DragEventArgs e)
        {
            if (App.Locator.AppState == ApplicationState.Preparation)
            {
                if(e.DataView.Contains(StandardDataFormats.StorageItems))
                {
                    var def = e.GetDeferral();
                    try
                    {
                        var storageItems = await e.DataView.GetStorageItemsAsync();
                        bool isSituatedSupportedTypes = false;
                        bool isSituatedUnSupportedTypes = false;
                        foreach (var item in storageItems)
                        {
                            var extension = Path.GetExtension(item.Name);
                            if (Pitch.Helpers.FileToAppHelper.DocumentExtensions.Contains(extension)
                                || Pitch.Helpers.FileToAppHelper.VideoExtensions.Contains(extension)
                                || Pitch.Helpers.FileToAppHelper.ImageExtensions.Contains(extension))
                            {
                                isSituatedSupportedTypes = true;
                            }
                            else
                            {
                                isSituatedUnSupportedTypes = true;
                            }
                        }
                        if(isSituatedSupportedTypes && isSituatedUnSupportedTypes)
                        {
                            //e.DragUIOverride.Caption =  "One or more files will be not added because of not supported file types";
                            e.AcceptedOperation = DataPackageOperation.Copy;
                        }
                        else if(isSituatedUnSupportedTypes && !isSituatedSupportedTypes)
                        {
                           // e.DragUIOverride.Caption = "File types are not supported";
                            e.AcceptedOperation = DataPackageOperation.None;
                        }
                        else if(isSituatedSupportedTypes && !isSituatedUnSupportedTypes)
                        {
                           // e.DragUIOverride.Caption = "Drop the files to show it in this area";
                            e.AcceptedOperation = DataPackageOperation.Copy;
                        }
                        //e.DragUIOverride.
                    }
                    catch
                    {

                    }
                    def.Complete();
                }
                    
                // e.AcceptedOperation = (App.Locator.AppState == ApplicationState.Preparation) ? DataPackageOperation.Copy : DataPackageOperation.None;
            }
        }

        private async void ShotView_Drop(object sender, DragEventArgs e)
        {
            if (e.DataView.Contains(StandardDataFormats.StorageItems))
            {
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(new Helpers.Messages.BusyIndicatorShowMessage());
                var def = e.GetDeferral();
                e.AcceptedOperation = DataPackageOperation.None;
                var items = await e.DataView.GetStorageItemsAsync();
                await _shot.CreateLayers(items);
                e.AcceptedOperation = DataPackageOperation.Copy;
                e.DragUIOverride.Caption = "Drop the image to show it in this area";
                e.Handled = true;
                // Notify XAML that now we are done
                def.Complete();

                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(new Helpers.Messages.BusyIndicatorHideMessage());
            }
        }

        private void ShotView_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            UpdateAllLayerViewToNormalStyle(true);
        }
        #region Events

        private Task Shot_LayoutWasReplaced(LayerLayout oldModel, LayerLayout newModel)
        {
            RemoveView(oldModel);
            CreateView(newModel);
            return Task.CompletedTask;
        }

        private Task Shot_LayoutWasRemoved(LayerLayout model)
        {
            RemoveView(model);
            return Task.CompletedTask;
        }

        private Task Shot_LayoutWasCreated(LayerLayout model)
        {
            CreateView(model);
            return Task.CompletedTask;
        }

        private void Shot_UpdateBackgroundLayerEvent(LayerLayout newBackgroundLayer, LayerLayout currentBackgroundLayer)
        {
            if (currentBackgroundLayer != null)
            {
                var currentBackgroundView = LayoutViews.FirstOrDefault(v => v.ViewModel.Id == currentBackgroundLayer.Id);
                if (currentBackgroundLayer is VideoLayerLayout videoLayer)
                {
                    if (currentBackgroundView == null)
                    {
                        currentBackgroundView = CreateNewLayerView(currentBackgroundLayer);
                    }

                    if (!LayoutRoot.Children.Contains(currentBackgroundView))
                    {
                        currentBackgroundView.PrepareViewBeforeDisplaying(this);
                        LayoutRoot.Children.Add(currentBackgroundView);
                    }
                }
                Canvas.SetZIndex(currentBackgroundView, currentBackgroundLayer.ZIndex);
                currentBackgroundView.UpdateSizeAndPosition();
                (currentBackgroundView as VideoLayerLayoutView)?.DisableBackgroundMode();
                currentBackgroundView.ViewModel.UpdateThumbnail();
            }
            if (newBackgroundLayer != null)
            {
                var newBackgroundView = LayoutViews.FirstOrDefault(v => v.ViewModel.Id == newBackgroundLayer.Id);
                if (newBackgroundLayer is VideoLayerLayout videoLayer && !App.Locator.IsLightMode)
                {
                    if (LayoutRoot.Children.Contains(newBackgroundView))
                    {
                        LayoutRoot.Children.Remove(newBackgroundView);
                        newBackgroundView.Unsubscribe();
                    }
                }
                else
                {
                    Canvas.SetZIndex(newBackgroundView, newBackgroundLayer.ZIndex);
                    newBackgroundView.LayoutViewState = LayoutViewState.SelectedState;
                    newBackgroundView.UpdateSizeAndPosition();
                    (newBackgroundView as VideoLayerLayoutView)?.EnableBackgroundMode();
                }
                newBackgroundView.ViewModel.UpdateThumbnail();
            }
        }

        private void CreateView(LayerLayout model)
        {
            var view = CreateNewLayerView(model);
            UpdateAllLayerViewToNormalStyle();

            AddLayerView(view);
            LayoutViewWasCreatedEvent?.Invoke(view);
        }
        private void RemoveView(LayerLayout model)
        {
            var view = _layoutViews.FirstOrDefault(x => x.ViewModel == model);
            if (view != null)
            {
                view.Clean();
                _layoutViews.Remove(view);
                LayoutRoot.Children.Remove(view);
            }
        }

        private BaseLayoutView CreateNewLayerView(LayerLayout layer)
        {
            LogQueue.WriteToFile($"ShotView CreateNewLayerView");
            BaseLayoutView view = null;
            if (layer is MediaMixerLayerLayout)
            {
                view = new MediaMixerLayerLayoutView(layer as MediaMixerLayerLayout);
            }
            else if (layer is ImageLayerLayout)
            {
                view = new ImageLayerLayoutView(layer as ImageLayerLayout);
            }
            else if (layer is DocumentLayerLayout)
            {
                view = new DocumentLayerLayoutView(layer as DocumentLayerLayout);
            }
            else if (layer is WebLayerLayout)
            {
                view = new WebLayerLayoutView(layer as WebLayerLayout);
            }
            else if (layer is VideoLayerLayout)
            {
                view = new VideoLayerLayoutView(layer as VideoLayerLayout);
            }
            else if (layer is ShapeLayerLayout)
            {
                var model = layer as ShapeLayerLayout;
                if (model.ShapeType == Models.InkShapeType.Line || model.ShapeType == Models.InkShapeType.Arrow)
                {
                    view = new LineLayerLayoutView(model);
                }
                else
                {
                    view = new ShapeLayerLayoutView(model);
                }
            }
            else if (layer is TextLayerLayout)
            {
                view = new TextLayerLayoutView(layer as TextLayerLayout);
            }
            else if(layer is PlaceholderLayerLayout)
            {
                view = new LayerLayoutPlaceHolderView(layer);
            }

            if (view != null)
            {
                _layoutViews.Add(view);
            }
            return view;
        }

        private void AddLayerView(BaseLayoutView view)
        {
            if (view != null)
            {
                view.PrepareViewBeforeDisplaying(this);
                if (!LayoutRoot.Children.Contains(view))
                {
                    LayoutRoot.Children.Add(view);
                }
                RecordCommand(view.Id, AppMessageType.AddApp, view.ViewModel);
                RecordCommand(view.Id, AppMessageType.BringToFrontApp, view.ViewModel);
            }
        }

        #endregion

        #endregion

        #region Public Methods

        public void RecordCommand(int id, AppMessageType type, LayerLayout layer)
        {
            if (App.Locator.AppState == ApplicationState.Recording && layer.IsLiveOnPlayback)
            {
                var composingManager = ComposingManager;
                if (composingManager != null)
                {
                    composingManager.RecordCommand(id, type, layer);
                }
            }
        }
        public void RecordEvent(TouchcastEventType type, object contents = null)
        {
            if (App.Locator.AppState == ApplicationState.Recording)
            {
                var composingManager = ComposingManager;
                if (composingManager != null)
                {
                    composingManager.RecordEvent(type, contents);
                }
            }
        }

        public void RemovePlaceholder(LayerLayoutPlaceHolderView view)
        {
            if (LayoutRoot.Children.Contains(view))
            {
                LayoutRoot.Children.Remove(view);
            }
        }

        public void StartRendering()
        {
            foreach (var item in _layoutViews)
            {
                item.StartRenderView();
            }
        }

        public void StopRendering()
        {
            foreach (var item in _layoutViews)
            {
                item.StopRenderView();
            }
        }

        public override void Subscribe()
        {
            PointerPressed += ShotView_PointerPressed;
            Unloaded += ShotView_Unloaded;
            _shot.LayoutWasCreated += Shot_LayoutWasCreated;
            _shot.LayoutWasRemoved += Shot_LayoutWasRemoved;
            _shot.LayoutWasReplaced += Shot_LayoutWasReplaced;
            _shot.UpdateBackgroundLayerEvent += Shot_UpdateBackgroundLayerEvent;
            OutterLayout.DragEnter += OutterLayout_DragEnter;
            OutterLayout.Drop += ShotView_Drop;
        }

        public override void Unsubscribe()
        {
            Unloaded -= ShotView_Unloaded;
            OutterLayout.Drop -= ShotView_Drop;
            OutterLayout.DragEnter -= OutterLayout_DragEnter;
            PointerPressed -= ShotView_PointerPressed;
            _shot.LayoutWasCreated -= Shot_LayoutWasCreated;
            _shot.LayoutWasRemoved -= Shot_LayoutWasRemoved;
            _shot.LayoutWasReplaced -= Shot_LayoutWasReplaced;
            _shot.UpdateBackgroundLayerEvent -= Shot_UpdateBackgroundLayerEvent;
        }
        public FrameworkElement TurnOnInkingIfNeeded(bool isNeedTurnOn)
        {
            if (isNeedTurnOn)
            {
                UpdateAllLayerViewToNormalStyle();
                if (_inking == null)
                {
                    _inking = new Inking();
                    OutterLayout.Children.Add(_inking.InkCanvas);
                }
                _inking.InkCanvas.InkPresenter.StrokesCollected += InkPresenter_StrokesCollected;
                _inking.InkCanvas.InkPresenter.StrokesErased += InkPresenter_StrokesErased;
                (_inking.InkToolbar as InkToolbar).EraseAllClicked += InkToolbar_EraseAllClicked;
                _inking.InkCanvas.Visibility = Visibility.Visible;
            }
            else
            {
                if (_inking != null)
                {
                    _inking.InkCanvas.InkPresenter.StrokesCollected -= InkPresenter_StrokesCollected;
                    _inking.InkCanvas.InkPresenter.StrokesErased -= InkPresenter_StrokesErased;
                    (_inking.InkToolbar as InkToolbar).EraseAllClicked -= InkToolbar_EraseAllClicked;
                    _inking.InkCanvas.Visibility = Visibility.Collapsed;
                }
            }

            return _inking?.InkToolbar;
        }

        private void InkPresenter_StrokesErased(Windows.UI.Input.Inking.InkPresenter sender, Windows.UI.Input.Inking.InkStrokesErasedEventArgs args)
        {
            _shot.StrokesComposingElement.RemoveStrokes(args.Strokes.ToList().ToList());
        }

        private void InkPresenter_StrokesCollected(Windows.UI.Input.Inking.InkPresenter sender, Windows.UI.Input.Inking.InkStrokesCollectedEventArgs args)
        {
            _shot.StrokesComposingElement.AppendStrokes(args.Strokes);
        }

        private void InkToolbar_EraseAllClicked(InkToolbar sender, object args)
        {
            _shot.StrokesComposingElement.RemoveStrokes(_inking.InkCanvas.InkPresenter.StrokeContainer.GetStrokes());
        }

        public override void Clean()
        {
            Unsubscribe();
            Loaded -= ShotView_Loaded;
            try
            {
                foreach (var item in _layoutViews)
                {
                    item.Clean();
                }
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception in ShotView.Clean : {ex.Message}");
            }
            LayoutRoot.Children.Clear();
            _layoutViews.Clear();
            _shot = null;
        }

        public void PrepareShotBeforeDisplaying()
        {
            if(App.Locator.AppState == ApplicationState.Preparation)
            {
                TurnOnInkingIfNeeded(false);
            }
            var layersToIterate = Shot.Layers.OrderBy(l => l.ZIndex).ToList();
            var videoBackgroundLayer = layersToIterate.FirstOrDefault(l => l is VideoLayerLayout videoLayout && videoLayout.IsBackground && !App.Locator.IsLightMode);
            if (videoBackgroundLayer != null)
            {
                layersToIterate.Remove(videoBackgroundLayer);
            }
            foreach (var layer in layersToIterate)
            {
                var view = _layoutViews.FirstOrDefault(v => v.ViewModel == layer);
                if (view == null)
                {
                   view = CreateNewLayerView(layer);
                }

                if (layer is PlaceholderLayerLayout && App.Locator.AppState != ApplicationState.Preparation)
                {
                    continue;
                }

                AddLayerView(view);
            }
            RecordEvent(TouchcastEventType.SceneChanged);
        }

        public async Task PrepareShotBeforeHiding()
        {
            foreach (var view in _layoutViews)
            {
                await view.PrepareViewBeforeHiding();
                RecordCommand(view.Id, AppMessageType.CloseApp, view.ViewModel);
            }
        }
        public bool IsAnyVappSelected()
        {
            return LayoutViews.Any(v => v.LayoutViewState == LayoutViewState.EditState
                    || v.LayoutViewState == LayoutViewState.SelectedState);
        }
        public void PrepareToRecordingNewActionStream()
        {
            foreach (var view in _layoutViews)
            {
                RecordCommand(view.Id, AppMessageType.AddApp, view.ViewModel);
                RecordCommand(view.Id, AppMessageType.BringToFrontApp, view.ViewModel);
            }
        }

        public void UpdateAllLayerViewToNormalStyle(bool IsNeedToFireEvent = false)
        {
            foreach (var view in LayoutViews)
            {
                view.LayoutViewState = LayoutViewState.NormalState;
            }
            (App.Frame.Content as Control)?.Focus(FocusState.Programmatic);
            FireLayoutViewSelectionEvent(null, IsNeedToFireEvent);
        }

        public void ChangeZIndex(BaseLayoutView view, ZIndexPosition position)
        {
            var layers = Shot.ChangeZIndex(view.ViewModel, position);
            foreach (var layer in layers)
            {
                var layerView = _layoutViews.FirstOrDefault(v => v.ViewModel == layer);
                if (layerView != null)
                {
                    Canvas.SetZIndex(layerView, layer.ZIndex);
                }
            }
            Canvas.SetZIndex(view, view.ViewModel.ZIndex);
        }

        public void FireLayoutViewSelectionEvent(BaseLayoutView view, bool IsNeedToFireEvent = false)
        {
            if (IsNeedToFireEvent)
            {
                LayoutViewSelectedEvent?.Invoke(view);
            }
        }

        public void CleanLayoutLayerContainer()
        {
            foreach (var view in _layoutViews)
            {
                view.Unsubscribe();
                RecordCommand(view.Id, AppMessageType.CloseApp, view.ViewModel);
            }
            Unsubscribe();
            LayoutRoot.Children.Clear();
        }

        public async Task UpdateLayerViews()
        {
            /*foreach(var view in _layoutViews)
            {
                await view.UpdateContextBitmap();
            }*/
        }

        public Rect HelperLinesCheck(BaseLayoutView layoutView, Rect futurePosition, SenderType senderType)
        {
            if (IsShowGuideLines == true)
            {
               return HelperLines.CheckGuideLines(layoutView, futurePosition, senderType);
            }
            return futurePosition;
        }
        
        public void HideHelperLines()
        {
            HelperLines.HideLines();
        }
        #endregion
    }
}
