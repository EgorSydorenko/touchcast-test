﻿using Microsoft.Graphics.Canvas;
using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace TouchCast.UILayer.Authoring.Views.Shots
{
    public enum ClipSide
    {
        None,
        Left,
        Right
    }

    public sealed partial class NewShotItemView : UserControl
    {
        private Rect _rect;
        private ClipSide _clipSide;
        private bool _isCameraVisible;
        BitmapImage _image;

        public NewShotItemView(string prefix, Rect rect, bool isCameraVisible, ClipSide clipSide = ClipSide.None)
        {
            InitializeComponent();
            _isCameraVisible = isCameraVisible;
            _rect = rect;
            _clipSide = clipSide;
            Image.Source = new BitmapImage(new Uri($"ms-appx:///Assets/NewScenePopup/{prefix}_scene@3x.png"));

            Loaded += NewShotItemView_Loaded;
        }

        private void NewShotItemView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Image.Source == null)
            {
                Image.Source = _image;
            }
            Unloaded += NewShotItemView_Unloaded;
            if (_isCameraVisible)
            {
                if (SwapChainePanel.SwapChain == null || SwapChainePanel.SwapChain.Device != TouchCastComposingEngine.ComposingContext.Shared().CanvasSwapChain?.Device)
                {
                    SwapChainePanel.SwapChain = (CanvasSwapChain)TouchCastComposingEngine.ComposingContext.Shared().CanvasSwapChain;

                    var imageHeight = Image.Width * SwapChainePanel.SwapChain.SizeInPixels.Height / SwapChainePanel.SwapChain.SizeInPixels.Width;

                    SwapChaineContainer.Margin = new Thickness((LayoutRoot.Width - Image.Width) / 2, (LayoutRoot.Height - imageHeight) / 2, 0, 0);

                    if (_clipSide != ClipSide.None)
                    {
                        SwapChainePanel.RenderTransform = new CompositeTransform
                        {
                            TranslateX = (Image.Width / 4) * -1,
                            TranslateY = _rect.Top * imageHeight,
                            ScaleX = (Image.Width / SwapChainePanel.SwapChain.SizeInPixels.Width),
                            ScaleY = (imageHeight / SwapChainePanel.SwapChain.SizeInPixels.Height)
                        };
                        SwapChaineContainer.Clip = new RectangleGeometry()
                        {
                            Rect = new Rect(0, 0, Image.Width / 2, imageHeight)
                        };
                        SwapChaineContainer.RenderTransform = new CompositeTransform
                        {
                            TranslateX = _clipSide == ClipSide.Left ? _rect.Left * Image.Width : Image.Width / 2,
                            TranslateY = 0,
                            ScaleX = 1,
                            ScaleY = 1
                        };
                    }
                    else
                    {
                        SwapChainePanel.RenderTransform = new CompositeTransform
                        {
                            TranslateX = _rect.Left * Image.Width,
                            TranslateY = _rect.Top * imageHeight,
                            ScaleX = (Image.Width / SwapChainePanel.SwapChain.SizeInPixels.Width) * _rect.Width,
                            ScaleY = (imageHeight / SwapChainePanel.SwapChain.SizeInPixels.Height) * _rect.Height
                        };
                    }
                }
            }
        }

        private void NewShotItemView_Unloaded(object sender, RoutedEventArgs e)
        {
            //Loaded -= NewShotItemView_Loaded;
            //Unloaded -= NewShotItemView_Unloaded;
        }
    }
}
