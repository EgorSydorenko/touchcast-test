﻿using System.Collections.Generic;
using System.Linq;
using Pitch.DataLayer;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Shots
{
    public delegate void SelectionChanged(int index);
    public sealed partial class NewShotPopup : UserControl
    {
        private List<ShotPreviewCell> _previews;
#pragma warning disable CS0067
        public event SelectionChanged SelectionChangedEvent;
#pragma warning restore CS0067
        public Flyout parentFlyout;
        //public NewShotViewModel ViewModel => DataContext as NewShotViewModel;

        #region Life Cycle

        public NewShotPopup()
        {
            InitializeComponent();
        }
#if DEBUG
        ~NewShotPopup()
        {
            System.Diagnostics.Debug.WriteLine("***************** NewShotPopup Destructor **********************");
        }
#endif       
        #endregion

        public void Clean()
        {
            if (_previews != null)
            {
                foreach (var item in _previews)
                {
                    item.Clean();
                }
                _previews.Clear();
                _previews = null;
            }

            ShotsList?.Items?.Clear();
        }

        public void Update()
        {
            Clean();

            _previews = new List<ShotPreviewCell>();

            var project = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>()?.Project;
            if (project != null)
            {
                foreach (var item in project.Theme.Shots)
                {
                    var view = new ShotPreviewCell(item, false);
                    view.Width = 160;
                    view.Height = 90;
                    _previews.Add(view);
                }
            }
        }

        #region Callbacks

        private async void List_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ShotsList.SelectedItem is ShotPreviewCell previewCell)
            {
                var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
                if (session?.Project != null)
                {
                    var shot = session.Project.Theme.Shots.FirstOrDefault(s => s.Id == previewCell.Shot?.Id);
                    if (shot != null)
                    {
                        session.Project.Theme.ActiveShot = shot;
                        var newShot = await session.Project.CreateShot(shot);
                        await session.ShotManager.ShowShot(newShot);
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandSceneAdded);
                        parentFlyout?.Hide();
                    }
                }
            }
        }

        private void List_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (_previews == null)
            {
                Update();
            }
            ShotsList.SelectionChanged += List_SelectionChanged;
            foreach (var item in _previews)
            {
                ShotsList.Items.Add(item);
            }
        }

        private void List_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Clean();
            ShotsList.SelectionChanged -= List_SelectionChanged;
            ShotsList.SelectedIndex = -1;
        }

        #endregion
    }
}
