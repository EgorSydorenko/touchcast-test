﻿using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.Shots;
using Pitch.UILayer.Authoring.Views.LayoutViews.Previews;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Shots
{
    public sealed partial class ShotPreviewCell : UserControl
    {
        #region Private Fields
        private Shot _shot;
        private bool _isNeedObserveShotLayouts;
        #endregion

        public Shot Shot => _shot;

        #region Life Cycle
        public ShotPreviewCell(Shot shot, bool IsNeedObserveShotLayouts)
        {
            _shot = shot;
            _isNeedObserveShotLayouts = IsNeedObserveShotLayouts;

            InitializeComponent();

            Loaded += ShotPreviewCell_Loaded;
            Unloaded += ShotPreviewCell_Unloaded;

            if (_isNeedObserveShotLayouts)
            {
                _shot.LayoutWasCreated += Shot_LayoutWasCreated;
                _shot.LayoutWasRemoved += Shot_LayoutWasRemoved;
                _shot.LayoutWasReplaced += Shot_LayoutWasReplaced;
                _shot.UpdateShotViewEvent += Shot_UpdateShotViewEvent;
            }

            LayoutRoot.Clip = new RectangleGeometry()
            {
                Rect = new Windows.Foundation.Rect(0, 0, Services.MediaMixerService.VideoSize.Width, Services.MediaMixerService.VideoSize.Height)
            };

            var orderedItems = _shot.Layers.OrderBy(l => l.ZIndex);
            foreach (var layer in orderedItems)
            {
                var previewView = CreateFromModel(layer);
                if (previewView != null)
                {
                    LayoutRoot.Children.Add(previewView);
                }
            }
        }

#if DEBUG
        ~ShotPreviewCell()
        {
            System.Diagnostics.Debug.WriteLine("******************** ShotPreviewCell Destructor ***********************");
        }
#endif
        private void ShotPreviewCell_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateBackgroundColor();
        }

        private void ShotPreviewCell_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ShotPreviewCell_Loaded;
            Unloaded -= ShotPreviewCell_Unloaded;
            //Release();
        }

        #endregion

        #region Callbacks
        private async Task Shot_LayoutWasRemoved(LayerLayout model)
        {
            RemovePreview(model);
        }

        private async Task Shot_LayoutWasCreated(LayerLayout model)
        {
            var previewView = CreateFromModel(model);
            if (previewView != null)
            {
                LayoutRoot.Children.Add(previewView);
            }
        }

        private async Task Shot_LayoutWasReplaced(LayerLayout oldModel, LayerLayout newModel)
        {
            RemovePreview(oldModel);
            var previewView = CreateFromModel(newModel);
            if (previewView != null)
            {
                LayoutRoot.Children.Add(previewView);
            }
        }

        private void Shot_UpdateShotViewEvent()
        {
            UpdateBackgroundColor();
        }
        #endregion

        #region Private
        private void RemovePreview(LayerLayout model)
        {
            ILayoutPreview itemToRemove = null;
            foreach (var item in LayoutRoot.Children)
            {
                if (item is ILayoutPreview)
                {
                    var preview = item as ILayoutPreview;
                    if (preview.ViewModel?.Id == model.Id)
                    {
                        itemToRemove = preview;
                    }
                }
            }
            if (itemToRemove != null)
            {
                LayoutRoot.Children.Remove(itemToRemove);
            }
        }

        private ILayoutPreview CreateFromModel(LayerLayout model)
        {
            ILayoutPreview view = null;
            if (model is ImageLayerLayout)
            {
                view = new ImageLayerLayoutPreview(model as ImageLayerLayout);
            }
            else if (model is MediaMixerLayerLayout)
            {
                view = new MediaMixerLayerLayoutPreview(model as MediaMixerLayerLayout);
            }
            else if (model is WebLayerLayout)
            {
                view = new WebLayerLayoutPreview(model as WebLayerLayout);
            }
            else if (model is VideoLayerLayout)
            {
                view = new VideoLayerLayoutPreview(model as VideoLayerLayout);
            }
            else if (model is TextLayerLayout)
            {
                view = new TextLayerLayoutPreview(model as TextLayerLayout);
            }
            else if (model is DocumentLayerLayout)
            {
                view = new DocumentLayerLayoutPreview(model as DocumentLayerLayout);
            }
            else if(model is PlaceholderLayerLayout)
            {
                view = new PlaceholderLayerLayoutPreview(model as PlaceholderLayerLayout);
            }
            else if(model is ShapeLayerLayout)
            {
                view = new ShapeLayerLayoutPreview(model as ShapeLayerLayout);
            }

            return view;
        }

        private void UpdateBackgroundColor()
        {
            LayoutRoot.Background = new SolidColorBrush(_shot.BackgroundColor);
        }
        #endregion

        #region Public

        public async Task ShotWillBeActivated()
        {
            // var items = LayoutRoot.Children.Where(v => v is MediaMixerLayerLayoutPreview).ToList<MediaMixerLayerLayoutPreview>();
            var items = LayoutRoot.Children.Where(v => v is ILayoutPreview);
            foreach (var item in items)
            {
                if (item is ILayoutPreview)
                {
                    await (item as ILayoutPreview).ShotWillBeActivated(true);
                }
            }
        }
        public async Task ShotWillBeDeativated(bool isNeedUpdateThumbnail)
        {
            var items = LayoutRoot.Children.Where(v => v is ILayoutPreview);
            foreach (var item in items)
            {
                if(item is ILayoutPreview)
                {
                    await (item as ILayoutPreview).ShotWillBeDeativated(isNeedUpdateThumbnail);
                }
            }
        }
        public void Clean()
        {
            _shot.LayoutWasCreated -= Shot_LayoutWasCreated;
            _shot.LayoutWasRemoved -= Shot_LayoutWasRemoved;
            _shot.LayoutWasReplaced -= Shot_LayoutWasReplaced;
            _shot.UpdateShotViewEvent -= Shot_UpdateShotViewEvent;
            //_shot = null;
            var items = LayoutRoot.Children.Where(v => v is ILayoutPreview);
            foreach (ILayoutPreview item in items)
            {
                item.Clean();
            }
        }
        #endregion
    }
}
