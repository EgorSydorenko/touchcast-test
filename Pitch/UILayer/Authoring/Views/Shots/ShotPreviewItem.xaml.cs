﻿using GalaSoft.MvvmLight.Ioc;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Shots;
using Pitch.Models;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Shots
{
    public sealed partial class ShotPreviewItem : UserControl
    {
        #region Constants
        private static readonly Size _preaparationSize = new Size(288, 162);
        private static readonly Brush ActiveIndexLabelColor = new SolidColorBrush(Colors.White);
        private static readonly Brush DefaultIndexLabelColor = new SolidColorBrush(Color.FromArgb(0xff, 0x9d, 0x9d, 0x9d));
        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
               nameof(IsSelected), typeof(bool),
               typeof(ShotPreviewItem), new PropertyMetadata(false));

        #endregion

        #region Private Fields
        private bool _isCreated;
        private bool _hasContextMenu = true;
        private bool _isNeedToObserveShotChanges;
        private ShotPreviewCell _previewCell;
        private readonly AsyncLock _lock = new AsyncLock(1);
        #endregion

        #region Properties

        public Shot Shot => _previewCell?.Shot;

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set
            {
                SetValue(IsSelectedProperty, value);
                IndexTextBlock.Foreground = IsSelected == true ? ActiveIndexLabelColor : DefaultIndexLabelColor;
            }
        }

        #endregion

        #region Life Cycle

        public ShotPreviewItem(Shot shot, bool isNeedToObserveShotChanges)
        {
            InitializeComponent();

            _isNeedToObserveShotChanges = isNeedToObserveShotChanges;
            if (_previewCell == null)
            {
                _previewCell = new ShotPreviewCell(shot, _isNeedToObserveShotChanges);
                PreviewContainer.Children.Add(_previewCell);
            }
        }

#if DEBUG
        ~ShotPreviewItem()
        {
            System.Diagnostics.Debug.WriteLine("****************** ShotPreviewItem Destructor *********************");
        }
#endif

        private async void ShotPreviewItem_Loaded(object sender, RoutedEventArgs e)
        {
            if (_isCreated == false)
            {
                CheckIndex();
                Width = _preaparationSize.Width;
                Height = _preaparationSize.Height;
                IndexTextBlock.Foreground = IsSelected == true ? ActiveIndexLabelColor : DefaultIndexLabelColor;
                _isCreated = true;
            }
            if (IsSelected)
            {
                await ShotWillBeActivated();
            }
            else
            {
                await ShotWillBeDeativated(false);
            }
        }
        #endregion

        #region Callbacks

        private void ShotPreviewItem_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            if (App.Locator.AppState == ApplicationState.Preparation && _hasContextMenu)
            {
                MenuFlyout contextMenu = FlyoutBase.GetAttachedFlyout(this) as MenuFlyout;
                contextMenu?.ShowAt(this, e.GetPosition(this));
            }
        }

        private void ContextMenu_Opening(object sender, object e)
        {
            try
            {
                var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
                if (session != null)
                {
                    DeleteMenu.IsEnabled = session.Project.Shots.Count > 1;
                }
            }
            catch(System.Exception ex)
            {

            }
        }

        private async void MenuFlyoutDuplicate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
                var shot = _previewCell.Shot;
                if (session != null && shot != null)
                {
                    var duplicateShot = await session.Project.Duplicate(shot);
                    await session.ShotManager.ShowShotWithoutUndoRedo(duplicateShot);
                }
            }
            catch(System.Exception ex)
            {

            }
        }

        private async void MenuFlyoutDelete_Click(object sender, RoutedEventArgs e)
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            var shot = _previewCell.Shot;
            if (session != null && shot != null)
            {
                await session.Project.RemoveShot(shot);
            }
        }

        #endregion

        #region Public Methods

        public void DisableContextMenu()
        {
            _hasContextMenu = false;
        }

        public async Task ShotWillBeActivated()
        {
            await _lock.WaitAsync();
            try
            {
                await _previewCell.ShotWillBeActivated();
            }
            finally
            {
                _lock.Release();
            }
        }

        public async Task ShotWillBeDeativated(bool isNeedUpdateThumbnail)
        {
            await _lock.WaitAsync();
            try
            {
                await _previewCell.ShotWillBeDeativated(isNeedUpdateThumbnail);
            }
            finally
            {
                _lock.Release();
            }
        }

        public void CheckIndex()
        {
            AuthoringSession session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            Shot shot = _previewCell.Shot;
            if (session != null && shot != null)
            {
                int index = session.Project.Shots.IndexOf(shot) + 1;
                IndexTextBlock.Text = index.ToString();
            }
        }

        public void Clean()
        {
            _previewCell?.Clean();
        }

        #endregion
    }
}
