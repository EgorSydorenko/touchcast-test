﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;
using TfSdk;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Pitch.Services;
using Pitch.UILayer.Authoring.Views.Overlays;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Pitch.UILayer.Authoring.Views.FileMenu;
using Pitch.UILayer.Editing.Views;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Resources;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Authoring.Views
{
    public enum NavigateReason
    {
        None,
        CreateNewProject,
        OpenProjectFromFile,
        OpenProjectByToken,
        CloseProject,
    }

    public class NavigateRequest
    {
        public NavigateReason Reason { get; private set; }
        public object Parameter { get; private set; }

        public NavigateRequest(NavigateReason reason, object parameter = null)
        {
            Reason = reason;
            Parameter = parameter;
        }
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public enum OpenProjecFileErrorType
    {
        ProjectFileOldVersion,
        ProjectFileCorrupted,
    }

    public sealed partial class AuthoringPage : Page
    {
        #region Constants

        private const string AuthoringPageSaveProjectDialogContent = "SaveProjectDialogContent";
        private const string AuthoringPageSaveProjectDialogTitle = "SaveProjectDialogTitle";
        private const string AuthoringPageSaveProjectDialogSaveLabel = "SaveProjectDialogSaveLabel";
        private const string AuthoringPageSaveProjectDialogDoNotSaveLabel = "SaveProjectDialogDoNotSaveLabel";
        private const string AuthoringPageSaveProjectDialogCancelLabel = "SaveProjectDialogCancelLabel";
        private const string AuthoringPageInsertUnsupportedFormatTitle = "AuthoringPageInsertUnsupportedFormatTitle";
        private const string AuthoringPageInsertUnsupportedFormatContent = "AuthoringPageInsertUnsupportedFormatContent";
        private const string AuthoringPageProjectCorruptedDialogVersionTitle = "AuthoringPageProjectCorruptedDialogVersionTitle";
        private const string AuthoringPageProjectCorruptedDialogVersionContent = "AuthoringPageProjectCorruptedDialogVersionContent";
        private const string AuthoringPageProjectCorruptedDialogDismissLabel = "AuthoringPageProjectCorruptedDialogDismissLabel";
        private const string AuthoringPageProjectCorruptedDialogStoreLabel = "AuthoringPageProjectCorruptedDialogStoreLabel";
        private const string AuthoringPageProjectCorruptedDialogFileTitle = "AuthoringPageProjectCorruptedDialogFileTitle";
        private const string AuthoringPageProjectCorruptedDialogFileContent = "AuthoringPageProjectCorruptedDialogFileContent";
        private const string AuthoringPageProjectCorruptedDialogOkLabel = "AuthoringPageProjectCorruptedDialogOkLabel";
        private const string AuthoringPagePerformanceWarningContent = "PerformanceWarningContent";
        private const string AuthoringPagePerformanceWarningTitle = "PerformanceWarningTitle";
        private const string AuthoringPagePerformanceWarningOk = "PerformanceWarningOk";
        private const string AuthoringPagePerformanceWarningDontShow = "PerformanceWarningDontShow";
        private const string SettingsServicePerformanceKeyName = "IsHidePerformanceWarning";

        private readonly string SaveProjectDialogContent;
        private readonly string SaveProjectDialogTitle;
        private readonly string SaveProjectDialogSaveLabel;
        private readonly string SaveProjectDialogDoNotSaveLabel;
        private readonly string SaveProjectDialogCancelLabel;
        private readonly string InsertUnsupportedFormatTitle;
        private readonly string InsertUnsupportedFormatContent;
        private readonly string ProjectCorruptedDialogVersionTitle;
        private readonly string ProjectCorruptedDialogVersionContent;
        private readonly string ProjectCorruptedDialogDismissLabel;
        private readonly string ProjectCorruptedDialogStoreLabel;
        private readonly string ProjectCorruptedDialogFileTitle;
        private readonly string ProjectCorruptedDialogFileContent;
        private readonly string ProjectCorruptedDialogOkLabel;

        private readonly string PerformanceWarningContent;
        private readonly string PerformanceWarningTitle;
        private readonly string PerformanceWarningOk;
        private readonly string PerformanceWarningDontShow;

        private readonly Uri StoreUrl = new Uri("https://www.microsoft.com/en-us/store/p/tc-pitch/9p1bvglf68hw?rtc=1");
        #endregion

        #region Private Fields
        private WeakReference<AuthoringSession> _authoringSession;
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private FileMenuView FileMenuView;
        private bool _isWindowWasHidden = false;
        private AsyncLock _lockObject = new AsyncLock(1);
        private AsyncLock _visibilityChangedLock = new AsyncLock(1);
        private NavigateRequest _navigateRequest;

        #region Popups

        private BusyIndicatorOverlay _busyOverlay;
        private OnboardingVideoOverlay _introOverlay;
        private double _introOverlayScale = 0.25;
        private MessageDialog _saveProjectDialog;

        #endregion

        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        private IMediaMixerService MediaMixerService => _mediaMixerService?.TryGetObject();

        #endregion

        #region Life Cycle

        public AuthoringPage()
        {
            App.Locator.AppState = ApplicationState.Preparation;
            InitializeComponent();
            ResourceLoader loader = new ResourceLoader();
            SaveProjectDialogContent = loader.GetString(AuthoringPageSaveProjectDialogContent);
            SaveProjectDialogTitle = loader.GetString(AuthoringPageSaveProjectDialogTitle);
            SaveProjectDialogSaveLabel = loader.GetString(AuthoringPageSaveProjectDialogSaveLabel);
            SaveProjectDialogDoNotSaveLabel = loader.GetString(AuthoringPageSaveProjectDialogDoNotSaveLabel);
            SaveProjectDialogCancelLabel = loader.GetString(AuthoringPageSaveProjectDialogCancelLabel);
            InsertUnsupportedFormatTitle = loader.GetString(AuthoringPageInsertUnsupportedFormatTitle);
            InsertUnsupportedFormatContent = loader.GetString(AuthoringPageInsertUnsupportedFormatContent);
            ProjectCorruptedDialogVersionTitle = loader.GetString(AuthoringPageProjectCorruptedDialogVersionTitle);
            ProjectCorruptedDialogVersionContent = loader.GetString(AuthoringPageProjectCorruptedDialogVersionContent);
            ProjectCorruptedDialogDismissLabel = loader.GetString(AuthoringPageProjectCorruptedDialogDismissLabel);
            ProjectCorruptedDialogStoreLabel = loader.GetString(AuthoringPageProjectCorruptedDialogStoreLabel);
            ProjectCorruptedDialogFileTitle = loader.GetString(AuthoringPageProjectCorruptedDialogFileTitle);
            ProjectCorruptedDialogFileContent = loader.GetString(AuthoringPageProjectCorruptedDialogFileContent);
            ProjectCorruptedDialogOkLabel = loader.GetString(AuthoringPageProjectCorruptedDialogOkLabel);
            PerformanceWarningContent = loader.GetString(AuthoringPagePerformanceWarningContent);
            PerformanceWarningTitle = loader.GetString(AuthoringPagePerformanceWarningTitle);
            PerformanceWarningDontShow = loader.GetString(AuthoringPagePerformanceWarningDontShow);
            PerformanceWarningOk = loader.GetString(AuthoringPagePerformanceWarningOk);

            InitializeFileMenu();
            InitializeSaveMessageDialog();

            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                _authoringSession = new WeakReference<AuthoringSession>(session);
            }

            var mediaMixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            if (mediaMixer != null)
            {
                _mediaMixerService = new WeakReference<IMediaMixerService>(mediaMixer);
            }
            Loaded += AuthoringPage_Loaded;
        }

#if DEBUG
        ~AuthoringPage()
        {
            System.Diagnostics.Debug.WriteLine("****************AuthoringPage Destructor****************");
        }
#endif

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.Parameter == null) return;

            if (e.Parameter is NavigateRequest request)
            {
                _navigateRequest = request;
            }
        }

        protected override async void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            TitleBar.Unsubscribe();

            Messenger.Default.Unregister(this);
            base.OnNavigatingFrom(e);

            var mediaMixer = MediaMixerService;
            if (mediaMixer != null)
            {
                mediaMixer.StateChangedEvent -= MediaMixer_StateChangedEvent;
            }

            var session = AuthoringSession;
            if (session != null)
            {
                if (session.ShotManager != null)
                {
                    foreach (var item in session.ShotManager.ShotViews)
                    {
                        item.CleanLayoutLayerContainer();
                    }
                }
                session.AuthoringSessionWillBeCleaned -= Session_AuthoringSessionWillBeCleaned;
                session.AuthoringSeesionDidOpenProject -= Session_AuthoringSeesionDidOpenProject;
            }
            TitleBar.BackRequested -= TitleBar_BackRequested;
            TitleBar.UndoButtonPressedEvent -= TitleBar_UndoButtonPressedEvent;
            TitleBar.RedoButtonPressedEvent -= TitleBar_RedoButtonPressedEvent;
            TitleBar.SignOutPressedEvent -= TitleBar_SignOutPressedEvent;
            TitleBar.ChangeModeEvent -= ModeSwitcher_ChangeModeEvent;
            TitleBar.ShowTrainingPressedEvent -= TitleBar_ShowTrainingPressedEvent;
            Window.Current.CoreWindow.VisibilityChanged -= WindowVisibilityChanged;
            PreparingToolbar.OpenProjectEvent -= PreparingToolbar_OpenProjectEvent;
            PreparingToolbar.SaveProjectEvent -= PreparingToolbar_SaveProjectEvent;
            PreparingToolbar.SwitchToFileMenuEvent -= PreparingToolbar_SwitchToFileMenuEvent;
            FileMenuView.NewProjectEvent -= FileMenuView_NewProjectEvent;
            FileMenuView.OpenProjectEvent -= FileMenuView_OpenProjectEvent;
            FileMenuView.CloseProjectEvent -= FileMenuView_CloseProjectEvent;
            FileMenuView.CloseMenuEvent -= FileMenuView_CloseMenuEvent;
            FileMenuView.LogoutMenuEvent -= FileMenuView_LogoutMenuEvent;
            _saveProjectDialog = null;
            Container.Children.Clear();
            AuthoringPanel.Children.Clear();
            LayoutRoot.Children.Clear();
            if (e.SourcePageType != typeof(Recording.Views.RecordingScreenPage) &&
               e.SourcePageType != typeof(EditingPage))
            {
                await MediaMixerService.Reset();
                TouchCastComposingEngine.ComposingContext.Shared().ResetContext();
                await session?.Clean();
                session?.ShotManager?.Clean();
                session?.Reset();
            }
            else if (e.SourcePageType == typeof(EditingPage))
            {
                await MediaMixerService.Reset();
                TouchCastComposingEngine.ComposingContext.Shared().ResetContext();
            }
            
        }

        private async void AuthoringPage_Loaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("AuthoringPage_Loaded");

            Unloaded += AuthoringPage_Unloaded;
            SizeChanged += AuthoringPage_SizeChanged;
            App.SetupTitleBarColorScheme();

            await ShowPerformanceWarningIfNeeded();

            _busyOverlay = new BusyIndicatorOverlay();
            InitOverlayPosition(_busyOverlay);
            LayoutRoot.Children.Add(_busyOverlay);

            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(TitleBar.MovementContainer);

            PreparingToolbar.OpenProjectEvent += PreparingToolbar_OpenProjectEvent;
            PreparingToolbar.SaveProjectEvent += PreparingToolbar_SaveProjectEvent;

            PreparingToolbar.SwitchToFileMenuEvent += PreparingToolbar_SwitchToFileMenuEvent;
            FileMenuView.NewProjectEvent += FileMenuView_NewProjectEvent;
            FileMenuView.OpenProjectEvent += FileMenuView_OpenProjectEvent;
            FileMenuView.CloseProjectEvent += FileMenuView_CloseProjectEvent;
            FileMenuView.CloseMenuEvent += FileMenuView_CloseMenuEvent;
            FileMenuView.LogoutMenuEvent += FileMenuView_LogoutMenuEvent;
            Subscribe();

            var mediaMixer = MediaMixerService;
            if (mediaMixer != null)
            {
                mediaMixer.StateChangedEvent += MediaMixer_StateChangedEvent;
            }

            var session = AuthoringSession;
            if (session != null)
            {
                var isEditEnabled = (session.Project == null) ? false : session.Project.TcMediaComposition.MediaClips.Any(i => i.Takes.Any());
                TitleBar.UpdateModeSwitcherState(true, isEditEnabled);

                session.AuthoringSessionWillBeCleaned += Session_AuthoringSessionWillBeCleaned;
                session.AuthoringSeesionWillOpenProject += Session_AuthoringSeesionWillOpenProject;
                session.AuthoringSeesionDidOpenProject += Session_AuthoringSeesionDidOpenProject;
                if (_navigateRequest == null)
                {
                    if (session.Project == null)
                    {
                        if (App.Locator.ProjectFile == null)
                        {
                            try
                            {
                                await session.OpenNew(ThemeManager.Instance.SelectedTheme.theme_foler_path);
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                await session.Open(App.Locator.ProjectFile);
                            }
                            catch (Exception ex)
                            {
                                LogQueue.WriteToFile($"Exception {ex.Message}");
                            }
                        }
                    }
                    else
                    {
                        if (mediaMixer != null)
                        {
                            if (mediaMixer.State == TouchcastComposerServiceState.Idle)
                            {
                                await InitWithCamera();
                            }
                        }
                        await session.Init(Container);
                        SideBar.CheckReadyShots();
                    }
                }
                else
                {
                    await InvokeNavigateRequest();
                }
                if (session.Project != null)
                {
                    session.Project.UndoRedoOriginator.UndoRedoStacksChangedEvent += UndoRedoOriginator_UndoRedoStacksChangedEvent;
                    session.Project.ProjectWasSaved += Project_ProjectWasSaved;
                }
                UpdateTitleBar();
            }

            ISettingsService settingsService = SimpleIoc.Default.GetInstance<ISettingsService>();
            bool isFirstTimeLaunched = !settingsService.LoadFromSettings<bool>("IsFirstTimeLaunched");
            if (isFirstTimeLaunched == true)
            {
                settingsService.SaveToSettings("IsFirstTimeLaunched", true);
                ShowTrainingOverlay();
            }

            SideBar.Focus(FocusState.Programmatic);

            Window.Current.CoreWindow.VisibilityChanged += WindowVisibilityChanged;

            Messenger.Default.Send(new SubscribeUndoRedoButtonsMessage());
            TitleBar.SetShowTrainingButtonVisibility(true);
            TitleBar.BackRequested += TitleBar_BackRequested;
            TitleBar.UndoButtonPressedEvent += TitleBar_UndoButtonPressedEvent;
            TitleBar.RedoButtonPressedEvent += TitleBar_RedoButtonPressedEvent;
            TitleBar.SignOutPressedEvent += TitleBar_SignOutPressedEvent;
            TitleBar.ChangeModeEvent += ModeSwitcher_ChangeModeEvent;
            TitleBar.ShowTrainingPressedEvent += TitleBar_ShowTrainingPressedEvent;
            LayoutRoot.Children.Remove(_busyOverlay);
            PreparingToolbar.SubscribeToShortcutManager();
            Shortcut.Manager.Instance.Activate(Shortcut.ShortcutsManagerId.Vapp);
            StockMediaSdk.ApiClient.Shared.UpdateSize(ActualWidth, ActualHeight);
        }
        private void AuthoringPage_Unloaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("AuthoringPage_Unloaded");
            Loaded -= AuthoringPage_Loaded;
            Unloaded -= AuthoringPage_Unloaded;
            SizeChanged -= AuthoringPage_SizeChanged;
            TitleBar.SetShowTrainingButtonVisibility(false);
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthoringSeesionDidOpenProject -= Session_AuthoringSeesionDidOpenProject;
                session.AuthoringSeesionWillOpenProject -= Session_AuthoringSeesionWillOpenProject;
                if (session.Project != null)
                {
                    session.Project.UndoRedoOriginator.UndoRedoStacksChangedEvent -= UndoRedoOriginator_UndoRedoStacksChangedEvent;
                    session.Project.ProjectWasSaved -= Project_ProjectWasSaved;
                }
            }

            //TODO Need Close? 
            //await NativeServiceHelper.CloseNativeService();

            _introOverlay = null;
        }

        #endregion

        #region Callbacks

        private void MediaMixer_StateChangedEvent(object sender)
        {
            if (sender is IMediaMixerService)
            {
                var mixer = sender as IMediaMixerService;
                if (mixer.State == TouchcastComposerServiceState.NoDevices
                    || mixer.State == TouchcastComposerServiceState.DoNotHasAccess
                    || mixer.State == TouchcastComposerServiceState.DeviceLock
                    || mixer.State == TouchcastComposerServiceState.UnhandledFail)
                {
                    NavigateToPage(typeof(IntroPage.Views.NewThemePage), mixer.State);
                }
            }
        }

        private async void TitleBar_BackRequested()
        {
            var saveIfneededResult = await SaveProjectIfNeeded();
            if (saveIfneededResult == true)
            {
                NavigateToPage(typeof(IntroPage.Views.NewThemePage));
            }
        }

        private async void TitleBar_UndoButtonPressedEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.UndoRedoOriginator.Undo();
            }
        }

        private async void TitleBar_RedoButtonPressedEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.UndoRedoOriginator.Redo();
            }
        }

        private async void TitleBar_SignOutPressedEvent()
        {
            await SignOut();
        }

        private void TitleBar_ShowTrainingPressedEvent()
        {
            ShowTrainingOverlay();
        }

        private void Shared_ApiClientChangedEvent()
        {
            ApiClient.Shared.ApiClientStateChangedEvent -= Shared_ApiClientChangedEvent;
            if (ApiClient.Shared.Condition == ApiClient.State.NotAuthorized)
            {
                App.Frame.Navigate(typeof(IntroPage.Views.SignInPage), null, new DrillInNavigationTransitionInfo());
                App.Locator.UnRegisterAuthoringSession();
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }
        }

        private Task Session_AuthoringSeesionWillOpenProject()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.UndoRedoOriginator.UndoRedoStacksChangedEvent -= UndoRedoOriginator_UndoRedoStacksChangedEvent;
            }

            return Task.CompletedTask;
        }

        private async Task Session_AuthoringSeesionDidOpenProject()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Init(Container);
                PreparingToolbar.UpdateShotsPopup();
                SideBar.CheckReadyShots();
                session.Project.UndoRedoOriginator.UndoRedoStacksChangedEvent += UndoRedoOriginator_UndoRedoStacksChangedEvent;
                UpdateTitleBar();
            }
        }

        private void UndoRedoOriginator_UndoRedoStacksChangedEvent(DataLayer.UndoRedo.UndoRedoStacksChangedEventArgs args)
        {
            TitleBar.UpdateUndoRedoButtons(!args.IsUndoStackEmpty, !args.IsRedoStackEmpty);
        }

        private Task Session_AuthoringSessionWillBeCleaned()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.ShotManager != null)
                {
                    foreach (var shotView in session.ShotManager.ShotViews)
                    {
                        shotView.Clean();
                    }
                }
                Container.Children.Clear();
            }
            TitleBar.UpdateUndoRedoButtons(false, false);

            return Task.CompletedTask;
        }

        private void AuthoringPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            StockMediaSdk.ApiClient.Shared.UpdateSize(ActualWidth, ActualHeight);
        }

        private async void WindowVisibilityChanged(CoreWindow sender, VisibilityChangedEventArgs args)
        {
            await _visibilityChangedLock.WaitAsync();
            LogQueue.WriteToFile($"WindowVisibilityChanged {args.Visible}");
            var mediaMixer = MediaMixerService;
            if (mediaMixer != null)
            {
                if (args.Visible)
                {
                    if (_isWindowWasHidden)
                    {
                        _isWindowWasHidden = false;
                        await InitWithCamera();
                    }
                }
                else
                {
                    _isWindowWasHidden = true;
                    await mediaMixer.Reset();
                }
            }
            _visibilityChangedLock.Release();
        }

        private void Project_ProjectWasSaved()
        {
            UpdateTitleBar();
        }

        private void ModeSwitcher_ChangeModeEvent(ApplicationState futureState)
        {
            if (futureState == ApplicationState.Editing)
            {
                App.Frame.Navigate(typeof(EditingPage));
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }

            if (futureState == ApplicationState.Recording)
            {
                App.Locator.RegisterRecordingManager();
                App.Frame.Navigate(typeof(Recording.Views.RecordingScreenPage));
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }
        }

        private void PreparingToolbar_SwitchToFileMenuEvent(FileMenuKey menuKey)
        {
            SwitchToFileMenu(menuKey);
        }

        private async void PreparingToolbar_SaveProjectEvent(object sender, EventArgs e)
        {
            await FileMenuView.ViewModel.ExecuteMethodByKey(FileMenuKey.SaveMenu);
        }

        private async void PreparingToolbar_OpenProjectEvent(object sender, EventArgs e)
        {
            bool saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult != false)
            {
                await OpenProjectFromFile();
            }
        }

        private async void FileMenuView_NewProjectEvent(object sender, EventArgs e)
        {
            bool saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult == true)
            {
                NavigateToPage(typeof(IntroPage.Views.NewThemePage));
            }
            else
            {
                FileMenuView.SwitchToPreviousMenu();
            }
        }

        private async void FileMenuView_OpenProjectEvent(object sender, EventArgs e)
        {
            bool saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult != false)
            {
                bool result = await OpenProjectFromFile();
                if (result == true)
                {
                    CloseFileMenu();
                    return;
                }
            }
            FileMenuView.SwitchToPreviousMenu();
        }

        private async void FileMenuView_CloseProjectEvent(object sender, EventArgs e)
        {
            bool saveIfneededResult = await SaveProjectIfNeeded();
            if (saveIfneededResult)
            {
                NavigateToPage(typeof(IntroPage.Views.NewThemePage));
            }
            else
            {
                FileMenuView.SwitchToPreviousMenu();
            }
        }

        private void FileMenuView_CloseMenuEvent(object sender, EventArgs e)
        {
            CloseFileMenu();
        }

        private async void FileMenuView_LogoutMenuEvent(object sender, EventArgs e)
        {
            await SignOut();
        }

        #endregion

        #region Private Methods

        private void Subscribe()
        {
            Messenger.Default.Register<ProjectCorruptedMessage>(this, async (m) =>
            {
                var projectCorruptedDialog = new MessageDialog(String.Empty);
                switch (m.Type)
                {
                    case OpenProjecFileErrorType.ProjectFileOldVersion:
                        projectCorruptedDialog.Title = ProjectCorruptedDialogVersionTitle;
                        projectCorruptedDialog.Content = ProjectCorruptedDialogVersionContent;
                        projectCorruptedDialog.Commands.Add(new UICommand(ProjectCorruptedDialogDismissLabel));
                        projectCorruptedDialog.Commands.Add(new UICommand(ProjectCorruptedDialogStoreLabel));
                        break;
                    case OpenProjecFileErrorType.ProjectFileCorrupted:
                        projectCorruptedDialog.Title = ProjectCorruptedDialogFileTitle;
                        projectCorruptedDialog.Content = ProjectCorruptedDialogFileContent;
                        projectCorruptedDialog.Commands.Add(new UICommand(ProjectCorruptedDialogOkLabel));
                        break;
                    default:
                        break;
                }
                IUICommand dialogResult = await projectCorruptedDialog.ShowAsync();
                if (dialogResult.Label == ProjectCorruptedDialogStoreLabel)
                {
                    await Windows.System.Launcher.LaunchUriAsync(StoreUrl);
                }
            });

            Messenger.Default.Register<TeleprompterEditorPopupShowMessage>(this, async (m) =>
            {
                var teleprompterEditorDialog = new TeleprompterEditorContentDialog();
                await teleprompterEditorDialog.ShowAsync();
            });

            Messenger.Default.Register<BusyIndicatorShowMessage>(this, (m) =>
            {
                if (!LayoutRoot.Children.Contains(_busyOverlay))
                {
                    LayoutRoot.Children.Add(_busyOverlay);
                }
            });

            Messenger.Default.Register<BusyIndicatorHideMessage>(this, (m) =>
            {
                LayoutRoot.Children.Remove(_busyOverlay);
            });

            Messenger.Default.Register<OpenNewProjectMessage>(this, async (m) =>
            {
                if (await SaveProjectIfNeeded())
                {
                    await OpenProjectByFileToken(m.FileTocken);
                }
            });

            Messenger.Default.Register<ShowUnsupportedFormatMessage>(this, async (m) =>
            {
                await ShowUnsupportedFormatMessage();
            });
        }

        private void InitOverlayPosition(Control overlay)
        {
            Grid.SetRow(overlay, 0);
            Grid.SetRowSpan(overlay, 4);
            Canvas.SetZIndex(overlay, 1000);
        }

        private void UpdateTitleBar()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                var isEditEnabled = (session.Project == null) ? false : session.Project.TcMediaComposition.MediaClips.Any(i => i.Takes.Any());
                TitleBar.UpdateModeSwitcherState(true, isEditEnabled);

                var title = session.Project?.Name;
                if (!String.IsNullOrEmpty(title))
                {
                    title = " - " + title;
                }
                title = String.Format("TouchCast Pitch{0}", title);
                TitleBar.UpdateTitle(title);

                if (session.Project != null)
                {
                    var state = session.Project.UndoRedoOriginator.State;
                    TitleBar.UpdateUndoRedoButtons(!state.IsUndoStackEmpty, !state.IsRedoStackEmpty);
                }
            }
        }

        private void NavigateToPage(Type target, object parameter = null)
        {
            App.Locator.UnRegisterAuthoringSession();
            App.Frame.Navigate(target, parameter);
            App.Frame.BackStack.Clear();
            App.Frame.ForwardStack.Clear();
            App.Frame.CacheSize = 0;
        }

        private async Task InitWithCamera()
        {
            var mediaMixer = MediaMixerService;
            var session = AuthoringSession;
            if (session != null && mediaMixer != null)
            {
                mediaMixer.InitWithCamera();
                await mediaMixer.StartPreviewingAsync();
                await session.UpdateScreenshotsForNewRecordingDevice();
            }
        }

        private async Task InvokeNavigateRequest()
        {
            switch (_navigateRequest.Reason)
            {
                case NavigateReason.CreateNewProject:
                    await OpenNewProject();
                    break;
                case NavigateReason.OpenProjectFromFile:
                    await OpenProjectFromFile(true);
                    break;
                case NavigateReason.OpenProjectByToken:
                    if (_navigateRequest.Parameter is string token && !String.IsNullOrEmpty(token))
                    {
                        await OpenProjectByFileToken(token);
                    }
                    break;
                case NavigateReason.CloseProject:
                    NavigateToPage(typeof(IntroPage.Views.NewThemePage));
                    break;
            }
        }

        private async Task ShowUnsupportedFormatMessage()
        {
            MessageDialog dlg = new MessageDialog(InsertUnsupportedFormatContent)
            {
                Title = InsertUnsupportedFormatTitle
            };
            dlg.Commands.Add(new UICommand(ProjectCorruptedDialogOkLabel) { Id = 1 });

            await dlg.ShowAsync();
        }

        private void ShowTrainingOverlay()
        {
            if (_introOverlay == null)
            {
                _introOverlay = new OnboardingVideoOverlay(LayoutRoot);
                InitOverlayPosition(_introOverlay);
            }
            LayoutRoot.Children.Add(_introOverlay);
        }
       
        #region File Menu

        private void InitializeFileMenu()
        {
            FileMenuView = new FileMenuView()
            {
                Visibility = Visibility.Collapsed,
            };

            Grid.SetRow(FileMenuView, 1);
            Grid.SetRowSpan(FileMenuView, 3);
            Canvas.SetZIndex(FileMenuView, 500);

            if (!LayoutRoot.Children.Contains(FileMenuView))
            {
                LayoutRoot.Children.Add(FileMenuView);
            }
        }

        private void SwitchToFileMenu(FileMenuKey menuKey)
        {
            FileMenuView.PrepareBeforeShow(menuKey);
            FileMenuView.Visibility = Visibility.Visible;
            TitleBar.BackButtonVisibility = Visibility.Collapsed;
            TitleBar.UndoRedoControlsVisibility = Visibility.Collapsed;
            TitleBar.ModeSwitcherVisibility = Visibility.Collapsed;
            TitleBar.UpdateTitle();
            TitleBar.SetDarkMode();
        }

        private void CloseFileMenu()
        {
            FileMenuView.PrepareBeforeHiding();
            FileMenuView.Visibility = Visibility.Collapsed;
            TitleBar.BackButtonVisibility = Visibility.Visible;
            TitleBar.UndoRedoControlsVisibility = Visibility.Visible;
            TitleBar.ModeSwitcherVisibility = Visibility.Visible;
            TitleBar.UpdateTitle();
            TitleBar.SetDefaultMode();
            PreparingToolbar.SwitchToPreviousPivotItem();
        }

        #endregion

        #region Project

        private async Task<bool> OpenNewProject()
        {
            var result = false;
            try
            {
                var session = AuthoringSession;
                if (session != null)
                {
                    Messenger.Default.Send(new BusyIndicatorShowMessage());
                    await session.OpenNew(ThemeManager.Instance.DefaultTheme.theme_foler_path);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in AuthoringPage.OpenNewProject : {ex.Message}");
            }
            finally
            {
                Messenger.Default.Send(new BusyIndicatorHideMessage());
            }

            return result;
        }

        private async Task<bool> OpenProjectFromFile(bool isFromEditing = false)
        {
            var result = false;


            try
            {
                var session = AuthoringSession;

                Func<Task> initIfNeeded = async () => 
                {
                    if (isFromEditing)
                    {
                        await InitWithCamera();
                        await session.Init(Container);
                        SideBar.CheckReadyShots();
                    }
                };

                if (session != null)
                {
                    StorageFile file = await FileToAppHelper.SelectFile(FileToAppHelper.PitchExtensions.ToArray());

                    if (file != null)
                    {
                        if (file.FileType == FileToAppHelper.TouchcastExtension)
                        {
                            await (Application.Current as App)?.OpenTctInAnotherWindow(file);
                            await initIfNeeded();
                            return true;
                        }
                        else
                        {
                            Messenger.Default.Send(new BusyIndicatorShowMessage());
                            await session.Open(file);
                            result = true;
                        }
                    }
                    else
                    {
                        await initIfNeeded();
                    }
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in AuthoringPage.OpenProjectFromFile : {ex.Message}");
            }
            finally
            {
                Messenger.Default.Send(new BusyIndicatorHideMessage());
            }

            return result;
        }

        private async Task OpenProjectByFileToken(string token)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                Messenger.Default.Send(new BusyIndicatorShowMessage());
                try
                {
                    var file = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(token);
                    await session.Open(file);
                    if (FileMenuView.IsOpened == true)
                    {
                        CloseFileMenu();
                    }
                }
                catch (Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in AuthoringPage.OpenProjectByFileToken : {ex.Message}");
                }
                finally
                {
                    Messenger.Default.Send(new BusyIndicatorHideMessage());
                }
            }
        }

        private async Task ShowPerformanceWarningIfNeeded()
        {
            if (App.Locator.IsShowPerformanceWarning)
            {
                App.Locator.IsShowPerformanceWarning = false;
                var settingsService = SimpleIoc.Default.GetInstance<ISettingsService>();
                var isHide = settingsService.LoadFromSettings<bool>(SettingsServicePerformanceKeyName);
                isHide |= settingsService.IsDeviceStrong | (settingsService.LightModeUserPermission != ModeUserPermission.Undefined);
                if (!isHide)
                {
                    var messageDialog = new MessageDialog(PerformanceWarningContent, PerformanceWarningTitle);
                    messageDialog.Commands.Add(new UICommand(PerformanceWarningDontShow, (c) => { settingsService.SaveToSettings(SettingsServicePerformanceKeyName, true); }));
                    messageDialog.Commands.Add(new UICommand(PerformanceWarningOk));
                    var result = await messageDialog.ShowAsync();
                }
            }
        }

        //TODO Duplicate Methods (see EditingPage)
        private void InitializeSaveMessageDialog()
        {
            _saveProjectDialog = new MessageDialog(SaveProjectDialogContent, SaveProjectDialogTitle);
            _saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogSaveLabel));
            _saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogDoNotSaveLabel));
            _saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogCancelLabel));
        }

        //TODO Duplicate Methods (see EditingPage)
        private async Task<bool> SaveProjectIfNeeded()
        {
            var result = true;
            var session = AuthoringSession;
            if (session?.Project != null && session.Project.HasUnsavedChanges)
            {
                result = await ShowSaveProjectDialog();
            }

            return result;
        }

        //TODO Duplicate Methods (see EditingPage)
        private async Task<bool> ShowSaveProjectDialog()
        {
            var result = true;
            var session = AuthoringSession;
            if (session?.Project != null)
            {
                IUICommand saveProjectDialogResult = await _saveProjectDialog.ShowAsync();
                if (saveProjectDialogResult.Label == SaveProjectDialogSaveLabel)
                {
                    try
                    {
                        StorageFile fileToSave = await FileToAppHelper.SaveFileDialog(FileToAppHelper.TouchcastProjectExtension, "Touchcast project", session.Project.Name);
                        if (fileToSave != null)
                        {
                            Messenger.Default.Send(new BusyIndicatorShowMessage());
                            await session.Project.SaveAs(fileToSave);
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        LogQueue.WriteToFile($"Exception in AuthoringPage.ShowSaveProjectDialog : {ex.Message}");
                    }
                    finally
                    {
                        Messenger.Default.Send(new BusyIndicatorHideMessage());
                    }
                }
                if (saveProjectDialogResult.Label == SaveProjectDialogCancelLabel)
                {
                    result = false;
                }
            }

            return result;
        }

        private async Task SignOut()
        {
            var saveIfneededResult = await SaveProjectIfNeeded();
            if (saveIfneededResult)
            {
                ApiClient.Shared.ApiClientStateChangedEvent += Shared_ApiClientChangedEvent;
                await ApiClient.Shared.SignOut();
            }
        }

        #endregion

        #endregion
    }
}