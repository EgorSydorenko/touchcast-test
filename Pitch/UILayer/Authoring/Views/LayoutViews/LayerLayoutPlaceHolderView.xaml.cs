﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using Pitch.DataLayer;
using Pitch.DataLayer.Layouts;
using Pitch.Helpers;
using Pitch.UILayer.Authoring.Views.Overlays;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;
using System.IO;
using GalaSoft.MvvmLight.Messaging;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class LayerLayoutPlaceHolderView : BaseLayoutView
    {
        #region Private Fields
        private bool _isInProcess;
        private WeakReference<AuthoringSession> _weakSession;
        private new PlaceholderLayerLayout ViewModel => _viewModel as PlaceholderLayerLayout;
        #endregion

        private AuthoringSession Session => _weakSession?.TryGetObject();
 
        #region Life cycle

        public LayerLayoutPlaceHolderView(LayerLayout model) : base(model)
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _weakSession = new WeakReference<AuthoringSession>(session);

            InitializeComponent();

            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
            RenderTransform = ViewModel.Transform;

            Loaded += LayerLayoutPlaceHolderView_Loaded;
            Unloaded += LayerLayoutPlaceHolderView_Unloaded;
        }

        private void LayerLayoutPlaceHolderView_Loaded(object sender, RoutedEventArgs e)
        {
        }
        private void LayerLayoutPlaceHolderView_Unloaded(object sender, RoutedEventArgs e)
        {
        }
        public override void Subscribe()
        {
            base.Subscribe();

            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Add(_layoutViewBorder);
            }
        }

        public override void Unsubscribe()
        {
            LayoutRoot.Children.Remove(_layoutViewBorder);

            base.Unsubscribe();
        }
#if DEBUG
        ~LayerLayoutPlaceHolderView()
        {
            System.Diagnostics.Debug.WriteLine("****************** LayerLayoutPlaceHolderView Destructor ***********************");
        }
#endif
        #endregion

        public override void PrepareViewBeforeDisplaying(ShotView parentView)
        {
            _parentView = new WeakReference<ShotView>(parentView);
            Canvas.SetZIndex(this, ViewModel.ZIndex);
        }
        public override void StartRenderView() { }

        public override void StopRenderView() { }

        public override Task UpdateContextBitmap()
        {
            return Task.CompletedTask;
        }

        #region private Methods
        private async void FileVappButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isInProcess) return;
            _isInProcess = true;

            UpdateSelection();
            var session = Session;
            if (session != null)
            {
                var fileInsertResult = await FileToAppHelper.InsertFile(() => Messenger.Default.Send(new BusyIndicatorShowMessage()));
                if (fileInsertResult.File != null)
                {
                    try
                    {
                        if (fileInsertResult.Result == FileImportResult.FileAsIs)
                        {
                            await session.Project.ActiveShot.ReplaceLayerInsteadOfPlaysholder(fileInsertResult.File, ViewModel);
                        }
                        else
                        {
                            await session.Project.CreateShotsFromDocumentPages(fileInsertResult.File, ViewModel);
                        }
                        session.ShotManager.ActiveShotView.RemovePlaceholder(this);
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandLocalFileAdded,
                                                                                    fileInsertResult.File.FileType);
                    }
                    finally
                    {
                        
                    }
                }
                Messenger.Default.Send(new BusyIndicatorHideMessage());
            }
            _isInProcess = false;
        }

        private async void WebVappButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateSelection();
            InsertWebViewContentDialog dlg = new InsertWebViewContentDialog();
            await dlg.ShowAsync();
            if(dlg.Result == InsertWebViewContentDialogResult.WebViewCreated)
            {
                var session = Session;
                if (session != null)
                {
                    Pitch.DataLayer.UrlWithProperties urlWithProps = null;
                    try
                    {
                        urlWithProps = await dlg.ProcessUrlWithProperties();
                    }
                    catch (Exception ex)
                    {
                        urlWithProps = null;
                    }
                    if (urlWithProps?.Properties != null && urlWithProps?.UrlString != null)
                    {
                        await session.Project.ActiveShot.ReplaceLayerInsteadOfPlaysholder(urlWithProps, ViewModel);
                        session.ShotManager.ActiveShotView.RemovePlaceholder(this);
                    }

                }
            }
            else if(dlg.Result == InsertWebViewContentDialogResult.CreationCanceled)
            {

            }
            dlg.Destroy();
        }

        private async void TextVappButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isInProcess) return;
            _isInProcess = true;
            UpdateSelection();
            var session = Session;
            if (session != null)
            {
                await session.Project.ActiveShot.ReplaceLayerInsteadOfPlaysholder(ViewModel);
                session.ShotManager.ActiveShotView.RemovePlaceholder(this);
                Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandTextAdded);
            }
            _isInProcess = false;
        }

        private void DrawVappButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void MediaButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isInProcess) return;
            _isInProcess = true;
            UpdateSelection();
            var session = Session;
            if (session != null)
            {
                StockMediaSdk.ApiClient.Initialize(App.AppSettings.GetString(App.VideoBlocksPublicKey), App.AppSettings.GetString(App.VideoBlocksPrivateKey));
                var downloadedFiles = await StockMediaSdk.ApiClient.Shared.ShowSearchDialogAsync(
                    StockMediaSdk.Data.Model.MediaType.Video | StockMediaSdk.Data.Model.MediaType.Photo,
                    Windows.Storage.ApplicationData.Current.LocalCacheFolder, true);
                var file = downloadedFiles.FirstOrDefault();
                if (file != null)
                {
                    var extension = Path.GetExtension(file.Path);
                    if (FileToAppHelper.ImageExtensions.Contains(extension) || FileToAppHelper.VideoExtensions.Contains(extension))
                    {
                        Messenger.Default.Send(new BusyIndicatorShowMessage());
                        await session.Project.ActiveShot.ReplaceLayerInsteadOfPlaysholder(file, ViewModel);
                        Messenger.Default.Send(new BusyIndicatorHideMessage());
                        session.ShotManager.ActiveShotView.RemovePlaceholder(this);
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandTCStockAdded, file.FileType);
                    }
                    else
                    {
                        Messenger.Default.Send(new ShowUnsupportedFormatMessage());
                    }
                }
            }
            _isInProcess = false;
        }

        private void UpdateSelection()
        {
            if (LayoutViewState == LayoutViewState.NormalState)
            {
                var parentView = ParentView;
                if (parentView != null)
                {
                    parentView.UpdateAllLayerViewToNormalStyle();
                    LayoutViewState = LayoutViewState.SelectedState;
                    parentView.FireLayoutViewSelectionEvent(this, true);
                }
            }
        }

        #endregion
    }
}
