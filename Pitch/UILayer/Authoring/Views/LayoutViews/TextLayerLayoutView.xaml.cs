﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.Helpers.Logger;
using TouchCastComposingEngine;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class TextLayerLayoutView : BaseLayoutView
    {
        public static readonly DependencyProperty BorderVisibilityProperty = DependencyProperty.Register(
            nameof(BorderVisibility), typeof(Visibility), typeof(TextLayerLayoutView), new PropertyMetadata(Visibility.Visible));

        #region Private Fields

        private bool _isAfterLoading = false;
        private bool _isLoaded = false;
        private bool _isEnterPressed;
        private bool _isKeyPressed;
        private KeyEventHandler _handler;
        #endregion

        #region Properties

        public RichEditBox RichEditBox => _richTextBox;
        public ITextRange DocumentSelection => _richTextBox.Document.Selection.GetClone();

        private new TextLayerLayout ViewModel => _viewModel as TextLayerLayout;

        public override LayoutViewState LayoutViewState
        {
            get => _layoutViewState;
            set
            {
                if (_layoutViewBorder != null && _isLoaded)
                {
                    _layoutViewState = value;
                    HorizontalPanel.Visibility = VerticalPanel.Visibility = (_layoutViewState == LayoutViewState.EditState) ? Visibility.Collapsed : Visibility.Visible;
                    _layoutViewBorder.IsHitTestVisible = !(_layoutViewState == LayoutViewState.EditState);

                    _layoutViewBorder?.UpdateBorderStyle(_layoutViewState, ViewModel.IsStatic);
                    if (LayoutRoot.Children.Contains(_richTextBox))
                    {
                        if (_layoutViewState == LayoutViewState.EditState)
                        {
                            ViewModel.HidePlaceholder();
                            _richTextBox.IsEnabled = true;
                            _richTextBox.IsHitTestVisible = true;
                            _richTextBox.Document.Selection.SetRange(0, 0);
                            _richTextBox.Focus(FocusState.Programmatic);
                        }
                        else if (_layoutViewState == LayoutViewState.NormalState)
                        {
                            ViewModel.ShowPlaceholder();
                            _richTextBox.IsHitTestVisible = false;
                            _richTextBox.IsEnabled = false;
                        }
                        else
                        {
                            _richTextBox.IsEnabled = false;
                            _richTextBox.IsHitTestVisible = false;
                            var outString = String.Empty;
                            _richTextBox.Document.GetText(TextGetOptions.FormatRtf, out outString);
                            if (outString?.Length > 0)
                            {
                                _richTextBox.Document.Selection.SetRange(0, outString.Length);
                            }
                        }
                        UpdateMoreTextIcon();
                    }
                }
            }
        }

        public Visibility BorderVisibility
        {
            get => (Visibility)GetValue(BorderVisibilityProperty);
            set => SetValue(BorderVisibilityProperty, value);
        }

        #endregion

        #region Life Cycle

        public TextLayerLayoutView(TextLayerLayout model) : base(model)
        {
            InitializeComponent();
            RenderTransform = ViewModel.Transform;
            _richTextBox.ApplyTemplate();
            ViewModel.Document = _richTextBox.Document;
            ViewModel.LoadData();
            _richTextBox.Style = (Style)Application.Current.Resources["CustomTextBox"];
            _richTextBox.IsHitTestVisible = true;
            _richTextBox.IsSpellCheckEnabled = false;
            if (model.IsEmptyDocumentData)
            {
                _richTextBox.FontSize = 48;
            }

            _richTextBox.IsEnabled = false;
            _richTextBox.FontFamily = new Windows.UI.Xaml.Media.FontFamily("Segoe UI");

            _stopwatch = new Stopwatch();

            Loaded += TextContentPresenter_Loaded;
            _renderableItem = _richTextBox;
        }

#if DEBUG
        ~TextLayerLayoutView()
        {
            Debug.WriteLine("*******************TextLayerLayoutView Destructor ********************");
        }
#endif
        private void TextContentPresenter_Loaded(object sender, RoutedEventArgs e)
        {
            _isAfterLoading = true;

            ViewModel.LoadData();
            if (_layoutViewBorder != null)
            {
                _layoutViewBorder.IsHitTestVisible = true;
                LayoutRoot.Children.Add(_layoutViewBorder);
            }
            var mediaMixerService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Services.IMediaMixerService>();
            if (mediaMixerService.State == Services.TouchcastComposerServiceState.Recording)
            {
                StartRenderView();
            }
            CheckViewHotSpot();
            _isLoaded = true;
        }

        private void TextContentPresenter_Unloaded(object sender, RoutedEventArgs e)
        {
            //TODO Empty?
        }

        #endregion

        #region Callbacks
        private void RichTextBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            _isEnterPressed = e.Key == Windows.System.VirtualKey.Enter;
            _isKeyPressed = true;
        }

        private async void RichTextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            if (_isAfterLoading)
            {
                _isAfterLoading = false;
                if (ViewModel.IsNeedUpdateThumbnail)
                {
                    await UpdateThumbnail();
                    ViewModel.IsNeedUpdateThumbnail = false;
                }
            }
            if (!_isAfterLoading)
            {
                ViewModel.SaveDocumentData();
            }
            if (_richTextBox.FocusState != FocusState.Unfocused)
            {
                UpdateHeight();
                UpdateMoreTextIcon();
            }
            if (!_isKeyPressed)
            {
                return;
            }
            _isKeyPressed = false;

            if (_isEnterPressed)
            {
                _isEnterPressed = false;
                ViewModel.UpdateMarkerList();
            }
        }

        private async void RichTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            await UpdateThumbnail();
        }

        #endregion

        #region Public Methods

        public override void StartRenderView()
        {
            if (_isRenderStarted || _cancelationTokenSource == null) return;
            _isRenderStarted = true;
            if (_cancelationTokenSource.IsCancellationRequested)
            {
                _cancelationTokenSource?.Dispose();
                _cancelationTokenSource = new CancellationTokenSource();
            }
            _stopwatch.Start();
            Task.Run(async () =>
            {
                while (!_cancelationTokenSource.IsCancellationRequested)
                {
                    var startTime = _stopwatch.Elapsed;
                    if (!_isScreenshotRuning)
                    {
                        _isScreenshotRuning = true;
                        await Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                        {
                            try
                            {
                                await RenderView();
                            }
                            catch (Exception ex)
                            {
                                LogQueue.WriteToFile($"Exception in TextLayerLayoutView.StartRenderView : {ex.Message}");
                                Debug.Write($"Exception is raised when trying to get sanpshot. Exception message: {ex.Message}");
                            }
                            finally
                            {
                                _isScreenshotRuning = false;
                            }
                        });
                    }
                    var duration = (_stopwatch.Elapsed - startTime).TotalMilliseconds;

                    if (duration < 200)
                    {
                        duration = 200;
                    }
                    else
                    {
                        duration *= 2;
                    }
                    await Task.Delay(TimeSpan.FromMilliseconds(duration));
                }
                _cancelationTokenSource.Dispose();
                _cancelationTokenSource = new CancellationTokenSource();
                _isRenderStarted = false;
                _stopwatch.Stop();
            }, _cancelationTokenSource.Token);
        }

        public override void StopRenderView()
        {
            _cancelationTokenSource.Cancel();
        }

        public override async Task UpdateContextBitmap()
        {
            await RenderView();
        }

        public override void Subscribe()
        {
            _cancelationTokenSource = new CancellationTokenSource();
            _richTextBox.LostFocus += RichTextBox_LostFocus;
            _richTextBox.TextChanged += RichTextBox_TextChanged;
            _handler = new KeyEventHandler(RichTextBox_KeyDown);
            _richTextBox.AddHandler(KeyDownEvent, _handler, true);
            Unloaded += TextContentPresenter_Unloaded;
            base.Subscribe();
        }

        public override void Unsubscribe()
        {
            ViewModel.SaveDocumentData();
            StopRenderView();
            ViewModel.HidePlaceholder();
            if (_richTextBox != null)
            {
                _richTextBox.LostFocus -= RichTextBox_LostFocus;
            }
            Unloaded -= TextContentPresenter_Unloaded;
            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Remove(_layoutViewBorder);
            }
            //_isLoaded = false;
            base.Unsubscribe();
        }

        public override void Clean()
        {
            StopRenderView();

            if (_richTextBox != null)
            {
                LayoutRoot.Children.Remove(_richTextBox);
            }
            if (_richTextBox != null)
            {
                _richTextBox.LostFocus -= RichTextBox_LostFocus;
                _richTextBox.TextChanged -= RichTextBox_TextChanged;
                _richTextBox.RemoveHandler(KeyDownEvent, _handler);
                _richTextBox = null;
            }
            Loaded -= TextContentPresenter_Loaded;
            ViewModel.Document = null;
            ViewModel.Cleanup();
            base.Clean();
        }

        public override async Task PrepareViewBeforeHiding()
        {
            if (App.Locator.AppState != DataLayer.ApplicationState.Recording)
            {
                await RenderView();
            }
            await base.PrepareViewBeforeHiding();
        }

        public override void SetHotSpot(bool isHotSpot)
        {
            base.SetHotSpot(isHotSpot);
            _richTextBox.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
        }

        public void UpdateDocumentStyle(ITextRange textRange)
        {
            ViewModel.UpdateDocumentStyle(textRange);
            UpdateThumbnail();
        }

        #endregion

        #region Protected Methods

        protected override void ManipulateView(ILayoutViewManipulationArgs args)
        {
            base.ManipulateView(args);
            UpdateMoreTextIcon();
        }

        #endregion

        #region Private Methods
        private void UpdateHeight()
        {
            _richTextBox.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
            if (ViewModel.IsStatic == false && _richTextBox.DesiredSize.Height > ViewModel.Position.Height)
            {
                if (ViewModel.Transform.TranslateY + _richTextBox.DesiredSize.Height <= ParentView?.ActualHeight)
                {
                    ViewModel.UpdatePosition(Width, _richTextBox.DesiredSize.Height);
                    UpdateSizeAndPosition();
                }
            }
        }

        private void UpdateMoreTextIcon()
        {
            if (_layoutViewState == LayoutViewState.NormalState)
            {
                MoreTextIcon.Visibility = Visibility.Collapsed;
            }
            else
            {
                _richTextBox.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
                MoreTextIcon.Visibility = ViewModel.Position.Height < _richTextBox.DesiredSize.Height ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        #endregion
    }
}
