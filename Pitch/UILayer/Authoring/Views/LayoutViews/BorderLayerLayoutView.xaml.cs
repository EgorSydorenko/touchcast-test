﻿using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public delegate void PinMoved(PointerRoutedEventArgs e);
    public delegate void PinPressed();
    public delegate void PinReleased();
    public delegate void PinManipulationDelta();
    public enum RegionTapped
    {
        NotTapped,
        BodyTapped,
        LeftTopCornerTapped,
        RightTopCornerTapped,
        LeftBottomCornerTapped,
        RightBottomCornerTapped,
        TopMiddle,
        LeftMiddle,
        BottomMiddle,
        RightMiddle
    }

    public sealed partial class BorderLayerLayoutView : UserControl
    {
        #region Constants

        public static readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 0);
        public static readonly CoreCursor SizeAllCursor = new CoreCursor(CoreCursorType.SizeAll, 1);
        public static readonly CoreCursor SizeNorthwestSoutheastCursor = new CoreCursor(CoreCursorType.SizeNorthwestSoutheast, 2);
        public static readonly CoreCursor SizeNortheastSouthwestCursor = new CoreCursor(CoreCursorType.SizeNortheastSouthwest, 3);
        public static readonly CoreCursor SizeWestEastCursor = new CoreCursor(CoreCursorType.SizeWestEast, 4);
        public static readonly CoreCursor SizeNorthSouthCursor = new CoreCursor(CoreCursorType.SizeNorthSouth, 5);

        private const double _selectedStrokeThink = 2.0;
        private const double _editedStrokeThink = 8.0;
        private readonly DoubleCollection _selectedStrokeDashArray = new DoubleCollection() { 2, 2 };
        private readonly Brush _strokeSelectBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x96, 0x96, 0x96));
        private readonly Brush _strokeEditBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0, 0, 0));

        #endregion

        #region Events

        public event PinPressed PinPressedEvent;
        public event PinReleased PinReleasedEvent;
        public event PinMoved PinMovedEvent;
        public event PinManipulationDelta LeftTopPinManipulationEvent;
        public event PinManipulationDelta RightTopPinManipulationEvent;
        public event PinManipulationDelta RightBottomPinManipulationEvent;
        public event PinManipulationDelta LeftBottomPinManipulationEvent;

        public event PinManipulationDelta LeftPointPinManipulationEvent;
        public event PinManipulationDelta RightPointPinManipulationEvent;

        #endregion

        #region Private Fields

        private bool _isPointerPressed;

        #endregion
        
        #region Life Cycle

        public BorderLayerLayoutView()
        {
            InitializeComponent();
            Visibility = Visibility.Collapsed;
        }

#if DEBUG
        ~BorderLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("******************* BorderLayerLayoutView Destructor ********************");
        }
#endif

        #endregion

        #region Public Methods

        public void UpdateBorderStyle(LayoutViewState state, bool isStatic)
        {
            switch (state)
            {
                case LayoutViewState.NormalState:
                    Visibility = Visibility.Collapsed;
                    break;
                case LayoutViewState.SelectedState:
                    Visibility = Visibility.Visible;
                    LayoutBorder.BorderBrush = _strokeSelectBrush;
                    Pins.Visibility = isStatic ? Visibility.Collapsed : Visibility.Visible;
                    break;
                case LayoutViewState.EditState:
                    LayoutBorder.BorderBrush = _strokeEditBrush;
                    Pins.Visibility = Visibility.Collapsed;

                    break;
            }
        }

        #endregion
        
        #region Cursors pointer

        private void LeftTopRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNorthwestSoutheastCursor;
        }

        private void CenterTopRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNorthSouthCursor;
        }

        private void RightTopRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNortheastSouthwestCursor;
        }

        private void LeftCenterRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeWestEastCursor;
        }

        private void RightCenterRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeWestEastCursor;
        }

        private void LeftBottomRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNortheastSouthwestCursor;
        }

        private void CenterBottomRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNorthSouthCursor;
        }

        private void RightBottomRectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNorthwestSoutheastCursor;
        }

        private void Rectangle_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            if (!_isPointerPressed)
            {
                Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            }
        }
        #endregion

        #region Callbacks

        private void LeftTopRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            LeftTopPinManipulationEvent?.Invoke();
            e.Handled = true;
        }

        private void RightTopRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            RightTopPinManipulationEvent?.Invoke();
            e.Handled = true;
        }

        private void RightBottomRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            RightBottomPinManipulationEvent?.Invoke();
            e.Handled = true;
        }

        private void LeftBottomRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            LeftBottomPinManipulationEvent?.Invoke();
            e.Handled = true;
        }

        #region Shape-Line Callbacks

        private void LeftPointRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            LeftPointPinManipulationEvent?.Invoke();
            e.Handled = true;
        }

        private void RightPointRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            RightPointPinManipulationEvent?.Invoke();
            e.Handled = true;
        }

        #endregion

        private void Rectangle_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            if (sender is Rectangle)
            {
                _isPointerPressed = true;
                (sender as Rectangle).CapturePointer(e.Pointer);
                PinPressedEvent?.Invoke();
            }
            e.Handled = true;
        }

        private void Rectangle_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            ReleasePointerCapture(e.Pointer);
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            PinReleasedEvent?.Invoke();
            _isPointerPressed = false;
            e.Handled = true;
        }

        private void Rectangle_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            PinMovedEvent?.Invoke(e);
        }

        private void ResizeButton_OnPointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeNorthwestSoutheastCursor;
        }

        private void ResizeButton_OnPointerExited(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        #endregion
    }
}
