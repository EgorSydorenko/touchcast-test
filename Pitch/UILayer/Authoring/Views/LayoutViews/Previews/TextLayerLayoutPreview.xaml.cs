﻿using Pitch.DataLayer.Layouts;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class TextLayerLayoutPreview
    {
        public new TextLayerLayout ViewModel => _viewModel as TextLayerLayout;

        #region Life Cycle
        public TextLayerLayoutPreview(TextLayerLayout model):base(model)
        {
            InitializeComponent();
            Loaded += TextLayerLayoutPreview_Loaded;
            Unloaded += TextLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~TextLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** TextLayerLayoutPreview Destructor ***********************");
        }
#endif
        private void TextLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void TextLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= TextLayerLayoutPreview_Loaded;
            Unloaded -= TextLayerLayoutPreview_Unloaded;
        }
        #endregion

        public override void Update()
        {
            base.Update();
            if (ViewModel.ThumbnailImage != null)
            {
                try
                {
                    PreviewImage.Source = ViewModel.ThumbnailImage;
                }
                catch { }
            }
        }
    }
}
