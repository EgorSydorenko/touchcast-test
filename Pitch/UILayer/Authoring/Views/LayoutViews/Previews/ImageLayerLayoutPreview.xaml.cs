﻿using System;
using Pitch.DataLayer.Layouts;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class ImageLayerLayoutPreview
    {
        public new ImageLayerLayout ViewModel => _viewModel as ImageLayerLayout;
        #region Life cycle
        public ImageLayerLayoutPreview(ImageLayerLayout model):base(model)
        {
            InitializeComponent();
            Loaded += ImageLayerLayoutPreview_Loaded;
            Unloaded += ImageLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~ImageLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** ImageLayerLayoutPreview Destructor ***********************");
        }
#endif
        private void ImageLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void ImageLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= ImageLayerLayoutPreview_Loaded;
            Unloaded -= ImageLayerLayoutPreview_Unloaded;
        }

        #endregion

        public override void Update()
        {
            base.Update();
            if (PreviewImage.Source == null)
            {
                if (ViewModel.ThumbnailImage != null)
                {
                    PreviewImage.Source = ViewModel.ThumbnailImage;
                }
                else if (ViewModel.ThumbnailFile != null)
                {
                    PreviewImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(ViewModel.ThumbnailFile.Path));
                }
            }
        }
    }
}
