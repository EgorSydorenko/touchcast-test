﻿using Microsoft.Graphics.Canvas;
using Pitch.DataLayer.Layouts;
using TouchCastComposingEngine;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using System.Threading.Tasks;
using System;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.Helpers.Logger;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class MediaMixerLayerLayoutPreview
    {
        public new MediaMixerLayerLayout ViewModel => _viewModel as MediaMixerLayerLayout;
        private Services.IMediaMixerService _mediaMixerService;
        #region Life cycle
        public MediaMixerLayerLayoutPreview(MediaMixerLayerLayout model):base(model)
        {
            InitializeComponent();
            Loaded += MediaMixerLayerLayoutPreview_Loaded;
            Unloaded += MediaMixerLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~MediaMixerLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** MediaMixerLayerLayoutPreview Destructor ***********************");
        }
#endif

        private void MediaMixerLayerLayoutPreview_Loaded(object sender, RoutedEventArgs e)
        {
            var panelTransform = new CompositeTransform()
            {
                TranslateX = 0,
                TranslateY = 0,
                ScaleX = ViewModel.Position.Width / ComposingContext.VideoSize.Width,
                ScaleY = ViewModel.Position.Height / ComposingContext.VideoSize.Height
            };
            PreviewPanel.RenderTransform = panelTransform;
            InitTransforms(ViewModel);

            LayoutRoot.Clip = new RectangleGeometry()
            {
                Rect = new Rect(0, 0, ViewModel.Position.Width, ViewModel.Position.Height)
            };
            _mediaMixerService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Services.IMediaMixerService>();
            PreviewPanel.SwapChain = (CanvasSwapChain)ComposingContext.Shared().CanvasSwapChain;

            _mediaMixerService.CanvasSwapChainReadyEvent += _mediaMixerService_CanvasSwapChainReadyEvent;
            Update();
        }

        private void _mediaMixerService_CanvasSwapChainReadyEvent(object sender, CanvasSwapChain swpchain)
        {
            Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                PreviewPanel.SwapChain = swpchain;
                ImagePanel.Visibility = Visibility.Collapsed;
            });
        }

        private void MediaMixerLayerLayoutPreview_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= MediaMixerLayerLayoutPreview_Loaded;
            Unloaded -= MediaMixerLayerLayoutPreview_Unloaded;
            if (_mediaMixerService != null)
            {
                _mediaMixerService.CanvasSwapChainReadyEvent -= _mediaMixerService_CanvasSwapChainReadyEvent;
            }

        }
        #endregion

        public override async Task ShotWillBeDeativated(bool isNeedUpdateThumbnailFile)
        {
            LogQueue.WriteToFile("MediaMixerLayerLayoutPreview.ShotWillBeDeativated");
            await UpdateThumbnail(isNeedUpdateThumbnailFile, true);

            ImagePanel.Visibility = Visibility.Visible;
            PreviewPanel.Visibility = Visibility.Collapsed;
        }
        public async override Task ShotWillBeActivated(bool isNeedUpdateThumbnailFile)
        {
            LogQueue.WriteToFile("MediaMixerLayerLayoutPreview.ShotWillBeActivated");
            ImagePanel.Visibility = Visibility.Collapsed;
            PreviewPanel.Visibility = Visibility.Visible;
            
            await UpdateThumbnail(isNeedUpdateThumbnailFile, false);
        }
        public override void Update()
        {
            base.Update();
            InitTransforms(ViewModel);
            LayoutRoot.Clip = new RectangleGeometry()
            {
                Rect = new Rect(0, 0, ViewModel.Position.Width, ViewModel.Position.Height)
            };
        }

        private void InitTransforms(MediaMixerLayerLayout layer)
        {
            var panelTransform = PreviewPanel.RenderTransform as CompositeTransform;
            var aspectRatio = (double)Services.MediaMixerService.VideoSize.Width / Services.MediaMixerService.VideoSize.Height;
            if (layer.KeepAspectRatio)
            {

                var neededWidth = Height * aspectRatio;
                panelTransform.ScaleX = neededWidth / Services.MediaMixerService.VideoSize.Width;
                panelTransform.ScaleY = Height / Services.MediaMixerService.VideoSize.Height;
                panelTransform.TranslateX = -((neededWidth - Width) / 2);
            }
            else
            {
                if (Width <= Height * aspectRatio)
                {
                    var neededWidth = Height * aspectRatio;
                    panelTransform.ScaleX = neededWidth / Services.MediaMixerService.VideoSize.Width;
                    panelTransform.ScaleY = Height / Services.MediaMixerService.VideoSize.Height;
                    panelTransform.TranslateX = -(neededWidth - ViewModel.Position.Width) / 2;
                    panelTransform.TranslateY = 0;
                }
                else
                {
                    var neededHeight = Width / aspectRatio;
                    panelTransform.ScaleX = Width / Services.MediaMixerService.VideoSize.Width;
                    panelTransform.ScaleY = neededHeight / Services.MediaMixerService.VideoSize.Height;
                    panelTransform.TranslateY = -(neededHeight - ViewModel.Position.Height) / 2;
                    panelTransform.TranslateX = 0;
                }
            }
        }

        private async Task UpdateThumbnail(bool isNeedUpdateFile, bool IsNeedToShowThumbnail)
        {
            var bitmapImage = new BitmapImage();
            try
            {
                if (isNeedUpdateFile)
                {
                    await ViewModel.UpdateMediaMixerThumbnail();
                }
                if (IsNeedToShowThumbnail)
                {
                    if (ViewModel.ThumbnailImage != null)
                    {
                        PreviewImage.Source = ViewModel.ThumbnailImage;
                        DefaultImage.Visibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in MediaMixerLayerLayoutPreview.UpdateThumbnailFileIfNeeded : {ex.Message}");
            }
        }
    }
}
