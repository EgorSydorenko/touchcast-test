﻿using Pitch.DataLayer.Layouts;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class WebLayerLayoutPreview
    {
        public new WebLayerLayout ViewModel => _viewModel as WebLayerLayout;
        #region Life Cycle
        public WebLayerLayoutPreview(WebLayerLayout model) :base(model)
        {
            InitializeComponent();
            Loaded += WebLayerLayoutPreview_Loaded;
            Unloaded += WebLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~WebLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** WebLayerLayoutPreview Destructor ***********************");
        }
#endif

        private void WebLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void WebLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= WebLayerLayoutPreview_Loaded;
            Unloaded -= WebLayerLayoutPreview_Unloaded;
        }

        #endregion
        public override void Update()
        {
            base.Update();
            if (ViewModel.ThumbnailImage != null)
            {
                try
                {
                   PreviewImage.Source = ViewModel.ThumbnailImage;
                }
                catch { }
            }
        }
    }
}
