﻿using System;
using Pitch.DataLayer.Layouts;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class VideoLayerLayoutPreview
    {
        public new VideoLayerLayout ViewModel => _viewModel as VideoLayerLayout;

        #region Life Cycle
        public VideoLayerLayoutPreview(VideoLayerLayout model):base(model)
        {
            InitializeComponent();
            Loaded += VideoLayerLayoutPreview_Loaded;
            Unloaded += VideoLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~VideoLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** VideoLayerLayoutPreview Destructor ***********************");
        }
#endif
        private void VideoLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void VideoLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= VideoLayerLayoutPreview_Loaded;
            Unloaded -= VideoLayerLayoutPreview_Unloaded;
        }
        #endregion

        public override void Update()
        {
            base.Update();
            if (PreviewImage.Source == null)
            {
                if (ViewModel.ThumbnailImage != null)
                {
                    PreviewImage.Source = ViewModel.ThumbnailImage;
                }
                else if (ViewModel.ThumbnailFile != null)
                {
                    PreviewImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(ViewModel.ThumbnailFile.Path));
                }
            }
        }
    }
}
