﻿using System;
using Pitch.DataLayer.Layouts;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class DocumentLayerLayoutPreview
    {
        public new DocumentLayerLayout ViewModel => _viewModel as DocumentLayerLayout;

        #region Life cycle
        public DocumentLayerLayoutPreview(DocumentLayerLayout model) : base(model)
        {
            InitializeComponent();
            Loaded += DocumentLayerLayoutPreview_Loaded;
            Unloaded += DocumentLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~DocumentLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** DocumentLayerLayoutPreview Destructor ***********************");
        }
#endif
        private void DocumentLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void DocumentLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= DocumentLayerLayoutPreview_Loaded;
            Unloaded -= DocumentLayerLayoutPreview_Unloaded;
        }
        #endregion

        public override void Update()
        {
            base.Update();
            //if (PreviewImage.Source == null)
            {
                if (ViewModel.ThumbnailImage != null)
                {
                    PreviewImage.Source = ViewModel.ThumbnailImage;
                }
                else if (ViewModel.ThumbnailFile != null)
                {
                    PreviewImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(ViewModel.ThumbnailFile.Path));
                }
            }
        }
    }
}
