﻿using Pitch.DataLayer.Layouts;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class ShapeLayerLayoutPreview
    {
        private const int _minHeight = 6;//Because of not good previewwing of thick lines
        public new ShapeLayerLayout ViewModel => _viewModel as ShapeLayerLayout;

        #region Life Cycle
        public ShapeLayerLayoutPreview(ShapeLayerLayout model) : base(model)
        {
            InitializeComponent();
            _transform.CenterX = 0;
            _transform.CenterY = 0;
            Loaded += ShapeLayerLayoutPreview_Loaded;
            Unloaded += ShapeLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~ShapeLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** ShapeLayerLayoutPreview Destructor ***********************");
        }
#endif
        private void ShapeLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void ShapeLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= ShapeLayerLayoutPreview_Loaded;
            Unloaded -= ShapeLayerLayoutPreview_Unloaded;
        }

        #endregion

        public override void Update()
        {
            if (ViewModel.ThumbnailImage != null)
            {
                PreviewImage.Source = ViewModel.ThumbnailImage;
            }

            base.Update();
            if (ViewModel.ShapeType == Models.InkShapeType.Line || ViewModel.ShapeType == Models.InkShapeType.Arrow)
            {
                Height = ViewModel.StrokeSize < _minHeight ? _minHeight : ViewModel.StrokeSize;
            }
            _transform.TranslateX = ViewModel.Transform.TranslateX;
            _transform.TranslateY = ViewModel.Transform.TranslateY;
             _transform.CenterX = ViewModel.Transform.CenterX;
            _transform.Rotation = ViewModel.Transform.Rotation;
        }
    }
}
