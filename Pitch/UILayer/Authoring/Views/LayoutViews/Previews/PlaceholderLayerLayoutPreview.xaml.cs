﻿using Pitch.DataLayer.Layouts;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public sealed partial class PlaceholderLayerLayoutPreview
    {
        #region Life Cycle
        public PlaceholderLayerLayoutPreview(PlaceholderLayerLayout model): base(model)
        {
            InitializeComponent();
            Loaded += PlaceholderLayerLayoutPreview_Loaded;
            Unloaded += PlaceholderLayerLayoutPreview_Unloaded;
        }

#if DEBUG
        ~PlaceholderLayerLayoutPreview()
        {
            System.Diagnostics.Debug.WriteLine("******************** PlaceholderLayerLayoutPreview Destructor ***********************");
        }
#endif
        private void PlaceholderLayerLayoutPreview_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Update();
        }
        private void PlaceholderLayerLayoutPreview_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Loaded -= PlaceholderLayerLayoutPreview_Loaded;
            Unloaded -= PlaceholderLayerLayoutPreview_Unloaded;
        }
        #endregion
    }
}
