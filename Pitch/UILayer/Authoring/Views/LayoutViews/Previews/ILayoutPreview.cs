﻿using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Pitch.UILayer.Authoring.Views.LayoutViews.Previews
{
    public abstract class ILayoutPreview : UserControl
    {
        protected CompositeTransform _transform;
        protected LayerLayout _viewModel;

        public LayerLayout ViewModel => _viewModel;

        public ILayoutPreview(LayerLayout model)
        {
            _viewModel = model;
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
             
            VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;

            _transform = new CompositeTransform();
            _transform.TranslateX = ViewModel.Position.X;
            _transform.TranslateY = ViewModel.Position.Y;
            RenderTransform = _transform;

            ViewModel.LayoutViewPreviewChangedEvent += ViewModel_LayoutViewPreviewChangedEvent;
        }
        public virtual Task ShotWillBeDeativated(bool isNeedUpdateThumbnailFile)
        {
            return Task.CompletedTask;
        }

        public virtual Task ShotWillBeActivated(bool isNeedUpdateThumbnailFile)
        {
            return Task.CompletedTask;
        }
        public virtual void Update()
        {
            Canvas.SetZIndex(this, ViewModel.ZIndex);

            _transform.TranslateX = ViewModel.Position.X;
            _transform.TranslateY = ViewModel.Position.Y;
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
        }
        public void Clean()
        {
            ViewModel.LayoutViewPreviewChangedEvent -= ViewModel_LayoutViewPreviewChangedEvent;
            _viewModel = null;
        }

        private void ViewModel_LayoutViewPreviewChangedEvent(object sender)
        {
            Update();
        }
    }
}
