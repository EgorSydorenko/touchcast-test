﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.UILayer.Helpers.Controls;
using Pitch.UILayer.Helpers.Messages;
using TouchCastComposingEngine;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Pitch.Helpers.Logger;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class VideoLayerLayoutView : BaseLayoutView
    {
        #region Private Fields
        private Windows.UI.Xaml.Media.SolidColorBrush _normalBackground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);
        private Windows.UI.Xaml.Media.SolidColorBrush _hotSpotBackground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Transparent);
        private MediaElement _player;
        private Image _imageView;
        private bool _isNeedToStartPlay = false;
        #endregion

        #region Properties

        private new VideoLayerLayout ViewModel => _viewModel as VideoLayerLayout;
        private bool IsDeviceStrong => !App.Locator.IsLightMode;
        private MediaElement Player
        {
            get
            {
                if (_player == null && IsDeviceStrong)
                {
                    _player = new MediaElement();
                    _player.Volume = ViewModel.Volume;
                    _player.AutoPlay = ViewModel.IsPlayAutomatically;
                }
                return _player;
            }
        }

        public override LayoutViewState LayoutViewState
        {
            get
            {
                return _layoutViewState;
            }
            set
            {
                _layoutViewState = value == LayoutViewState.EditState ? LayoutViewState.SelectedState : value;
                _layoutViewBorder?.UpdateBorderStyle(_layoutViewState, ViewModel.IsStatic);
            }
        }

        #endregion

        #region Life Cycle

        public VideoLayerLayoutView(VideoLayerLayout model) : base(model)
        {
            InitializeComponent();
            RenderTransform = ViewModel.Transform;
            Loaded += VideoLayerLayoutView_Loaded;
            Unloaded += VideoLayerLayoutView_Unloaded;
        }

#if DEBUG
        ~VideoLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("************************* VideoLayerLayoutView Desctructor **************************");
        }
#endif

        private async void VideoLayerLayoutView_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsDeviceStrong)
            {
                if (ViewModel.ThumbnailImage != null)
                {
                    Player.PosterSource = ViewModel.ThumbnailImage;
                }
                if (ViewModel.IsBackground)
                {
                    Player.TransportControls.Visibility = Visibility.Collapsed;
                    Player.AreTransportControlsEnabled = false;
                    _isNeedToStartPlay = true;
                }
                Player.AutoPlay = _isNeedToStartPlay;
                if (ViewModel.Composition == null)
                {
                    await ViewModel.Initialize();
                }
                await ViewModel.AddVideoVappBitmap();
                Player.SetMediaStreamSource(ViewModel.Composition.GenerateMediaStreamSource());
                Player.AreTransportControlsEnabled = true;
                if (!_isNeedToStartPlay)
                {
                    Player.Pause();
                }
            }
            else
            {
                _imageView.Source = ViewModel.HighDefinitionThumbnailImage;
            }
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;

            if (_isNeedToStartPlay)
            {
                _isNeedToStartPlay = false;
            }
            UpdateSizeForCurrentAspectRatioIfNeeded();
        }

        private void VideoLayerLayoutView_Unloaded(object sender, RoutedEventArgs e)
        {
            Player?.Stop();
        }

        #endregion

        #region Public Methods

        public override void Subscribe()
        {
            base.Subscribe();

            if (IsDeviceStrong)
            {
                var touchcastTransportControl = new TouchcastTransportControl
                {
                    IsStopButtonVisible = true,
                    IsStopEnabled = true,
                    IsPlaybackRateEnabled = false,
                    IsPlaybackRateButtonVisible = false,
                    IsFastForwardButtonVisible = false,
                    IsFastForwardEnabled = false,
                    IsFastRewindButtonVisible = false,
                    IsFastRewindEnabled = false,
                    IsFullWindowButtonVisible = false,
                    IsZoomButtonVisible = false,
                    IsVolumeButtonVisible = false,
                    IsVolumeEnabled = false,
                    IsCompact = true,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    TouchcastVolumeVisibility = Visibility.Visible
                };
                touchcastTransportControl.Volume = ViewModel.Volume;
                touchcastTransportControl.VolumeChanged += TouchcastTransportControl_VolumeChanged; //TODO Unsubscribe?
                Player.TransportControls = touchcastTransportControl;
                Player.Volume = ViewModel.Volume;
                Player.MediaEnded += Player_MediaEnded;
                if (App.Locator.AppState == DataLayer.ApplicationState.Recording)
                {
                    ViewModel.UpdateVideoCompositor();
                }

                LayoutRoot.Children.Add(Player);
            }
            else
            {
                _imageView = new Image();
                LayoutRoot.Children.Add(_imageView);
            }

            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Add(_layoutViewBorder);
            }

            Messenger.Default.Register<NewTakeInitializedMessage>(this, message =>
            {
                RestartVideo();
            });
        }

        public override void Unsubscribe()
        {
            Messenger.Default.Unregister(this);
            if (IsDeviceStrong)
            {
                try
                {
                    if (_player != null)
                    {
                        _player.Stop();
                        ViewModel.Volume = _player.Volume;
                        _player.MediaEnded -= Player_MediaEnded;
                        _player.RemoveAllEffects();
                        _player.AreTransportControlsEnabled = false;
                        _player.Source = null;
                        _player.RemoveAllEffects();
                    }
                }
                catch (Exception ex)
                { }
            }
            LayoutRoot.Children.Clear();
            _player = null;
            base.Unsubscribe();
        }

        public override void Clean()
        {
            Loaded -= VideoLayerLayoutView_Loaded;
            Unloaded -= VideoLayerLayoutView_Unloaded;
            base.Clean();
        }

        public async override void StartRenderView()
        {
            if (Player != null)
            {
                if (IsDeviceStrong)
                {
                    ViewModel.UpdateVideoCompositor();

                    if (_player != null)
                    {
                        if (Player.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Opening
                            || Player.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Closed)
                        {
                            _player.CurrentStateChanged += Player_CurrentStateChanged;
                        }
                        else
                        {
                            if (ViewModel.IsPlayAutomatically)
                            {
                                _player.Play();
                            }
                        }
                    }
                    else
                    {
                        _isNeedToStartPlay = true;
                    }
                }
                else
                {
                    if (ViewModel.ComposingContextElement.Bitmap == null)
                    {
                        using (var fileStream = await ViewModel.ThumbnailFile.OpenReadAsync())
                        {
                            ViewModel.ComposingContextElement.Bitmap = await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(ComposingContext.Shared().PreviewingDevice, fileStream);
                        }
                    }
                }
            }
        }

        public override void StopRenderView()
        {
            if (IsDeviceStrong)
            {
                ViewModel.ClearVideoCompositor();
                Player.Pause();
            }
        }

        public override void PrepareViewBeforeDisplaying(ShotView parentView)
        {
            base.PrepareViewBeforeDisplaying(parentView);
            if (ViewModel.IsBackground)
            {
                EnableBackgroundMode();
            }
        }

        public override Task PrepareViewBeforeHiding()
        {
            Player?.Pause();
            return base.PrepareViewBeforeHiding();
        }
        protected override async Task UpdateThumbnail()
        {
            await ViewModel.UpdateThumbnail();
        }

        public void EnableBackgroundMode()
        {
            if (Player == null) return;

            if (Player.CurrentState != Windows.UI.Xaml.Media.MediaElementState.Playing)
            {
                Player.Play();
            }
            Player.TransportControls.Visibility = Visibility.Collapsed;
            Player.AreTransportControlsEnabled = false;
        }

        public void DisableBackgroundMode()
        {
            if (Player == null) return;

            if (Player.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Playing)
            {
                Player.Stop();
            }
            Player.TransportControls.Visibility = Visibility.Visible;
            Player.AreTransportControlsEnabled = true;
        }

        public override Task UpdateContextBitmap()
        {
            return Task.CompletedTask;
        }

        public override void SetHotSpot(bool isHotSpot)
        {
            try
            {
                base.SetHotSpot(isHotSpot);
                if (LayoutRoot != null)
                {
                    LayoutRoot.Background = isHotSpot ? _hotSpotBackground : _normalBackground;
                }
                if (IsDeviceStrong)
                {
                    if (Player != null)
                    {
                        Player.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
                    }
                }
                else
                {
                    if (_imageView != null)
                    {
                        _imageView.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
                    }
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile(ex.Message);
            }
        }

        public override void Reconfigure()
        {
            ViewModel.IsPlayAutomatically = true;
            base.Reconfigure();
            UpdateSizeWithAspectRatio(ViewModel.AspectRatio);
        }

        #endregion

        #region Callbacks
        private void TouchcastTransportControl_VolumeChanged(object sender, VolumeChangedEventArgs e)
        {
            Player.Volume = e.NewVolume;
        }

        private void Player_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (ComposingContext.Shared().IsRecording || ViewModel.IsBackground == true)
            {
                Player.Play();
            }
        }

        private void Player_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            if (_player.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Paused
                || _player.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Stopped)
            {
                if (ViewModel.IsPlayAutomatically ||
                    ViewModel.IsBackground)
                {
                    _player.Play();
                }
                _player.CurrentStateChanged -= Player_CurrentStateChanged;
            }
        }

        #endregion

        #region Private Methods

        private void RestartVideo()
        {
            if (_player == null) return;

            if (Player.CanSeek == true)
            {
                Player.Position = TimeSpan.Zero;
            }
        }
        private void UpdateSizeForCurrentAspectRatioIfNeeded()
        {
            if (ViewModel.KeepAspectRatio)
            {
                UpdateSizeWithAspectRatio(ViewModel.AspectRatio);
            }
        }

        #endregion
    }
}
