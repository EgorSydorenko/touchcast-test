﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Threading.Tasks;
using TouchCastComposingEngine;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Pitch.DataLayer.Layouts;
using Pitch.Services;
using Pitch.DataLayer.Helpers.Extensions;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class MediaMixerLayerLayoutView : BaseLayoutView
    {
        public static readonly CoreCursor ColorPickerCursor = new CoreCursor(CoreCursorType.Custom, 101);

        #region Private fields
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private TranslateTransform _viewTransform;
        private CompositeTransform _panelTransform;
        private bool _isNeedTogetFocus;
        private IMediaMixerService MediaMixerService => _mediaMixerService?.TryGetObject();

        #endregion

        public new MediaMixerLayerLayout ViewModel => _viewModel as MediaMixerLayerLayout;

        #region Life cycle
        public MediaMixerLayerLayoutView(MediaMixerLayerLayout model) : base(model)
        {
            InitializeComponent();
            UpdateSizeForCurrentAspectRatioIfNeeded();
            InitTransforms();

            Width = model.Position.Width;
            Height = model.Position.Height;

            var mediaMixerService = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            if (mediaMixerService != null)
            {
                _mediaMixerService = new WeakReference<IMediaMixerService>(mediaMixerService);
            }
            Loaded += MediaMixerLayerLayoutView_Loaded;
        }

#if DEBUG
        ~MediaMixerLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("************************* MediaMixerLayerLayoutView Desctructor **************************");
        }
#endif

        private void MediaMixerLayerLayoutView_Loaded(object sender, RoutedEventArgs e)
        {
            var mediaMixerService = MediaMixerService;
            if (mediaMixerService != null)
            {
                if (mediaMixerService.SwapChain != null)
                {
                    SwapchainPanel.SwapChain = mediaMixerService.SwapChain;
                }
                SwapchainPanel.RenderTransform = _panelTransform;
            }
        }

        public override void Subscribe()
        {
            base.Subscribe();

            PointerPressed += MediaMixerLayerLayoutView_PointerPressed;
            PointerReleased += MediaMixerLayerLayoutView_PointerReleased;
            PointerEntered += MediaMixerLayerLayoutView_PointerEntered;
            PointerExited += MediaMixerLayerLayoutView_PointerExited;
            ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent += ChromaKeyEffect_ColorChangedEvent;
            var mediaMixer = MediaMixerService;
            if (mediaMixer != null)
            {
                mediaMixer.CanvasSwapChainReadyEvent += MediaMixerService_SwapChainReadyEvent;
            }

            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Add(_layoutViewBorder);
            }
        }

        public override void Unsubscribe()
        {
            PointerPressed -= MediaMixerLayerLayoutView_PointerPressed;
            PointerReleased -= MediaMixerLayerLayoutView_PointerReleased;
            PointerEntered -= MediaMixerLayerLayoutView_PointerEntered;
            PointerExited -= MediaMixerLayerLayoutView_PointerExited;
            ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent -= ChromaKeyEffect_ColorChangedEvent;
            LayoutRoot.Children.Remove(_layoutViewBorder);
            var mediaMixer = MediaMixerService;
            if (mediaMixer != null)
            {
                mediaMixer.CanvasSwapChainReadyEvent -= MediaMixerService_SwapChainReadyEvent;
            }

            base.Unsubscribe();
        }

        public override void Clean()
        {
            Loaded -= MediaMixerLayerLayoutView_Loaded;
            Unsubscribe();
            base.Clean();
        }
        #endregion

        #region Callbacks
        private void MediaMixerLayerLayoutView_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            var isNeedToProcess = CheckIsColorPickerTurned();
            Window.Current.CoreWindow.PointerCursor = BorderLayerLayoutView.ArrowCursor;
            e.Handled = isNeedToProcess;
        }

        private void MediaMixerLayerLayoutView_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            var isNeedToProcess = CheckIsColorPickerTurned();
            if (isNeedToProcess)
            {
                Window.Current.CoreWindow.PointerCursor = ColorPickerCursor;
                e.Handled = isNeedToProcess;
            }
        }

        private void MediaMixerLayerLayoutView_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var isNeedToProcess = CheckIsColorPickerTurned();
            e.Handled = isNeedToProcess;
        }

        private void MediaMixerLayerLayoutView_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var isNeedToProcess = CheckIsColorPickerTurned();
            
            if(isNeedToProcess)
            {
                var mousePointerAbsolutePosition = e.GetCurrentPoint(this).Position;
                var mousePointerRelativePosition = new Point(mousePointerAbsolutePosition.X / ActualWidth, mousePointerAbsolutePosition.Y / ActualHeight);

                var cameraAspect = (double)Services.MediaMixerService.VideoSize.Width / Services.MediaMixerService.VideoSize.Height;
                var viewportAspect = (double)ActualWidth / ActualHeight;

                //video is displayed in scale aspect fill mode
                //so we need to tweak mousePointerRelativePosition to take this into account
                Rect visibleCameraRelativeRect = new Rect();

                if(cameraAspect > viewportAspect)
                {
                    visibleCameraRelativeRect.Height = 1;
                    visibleCameraRelativeRect.Width = viewportAspect / cameraAspect;
                }
                else
                {
                    visibleCameraRelativeRect.Width = 1;
                    visibleCameraRelativeRect.Height = cameraAspect / viewportAspect;
                }

                visibleCameraRelativeRect.X = (1.0f - visibleCameraRelativeRect.Width) / 2.0f;
                visibleCameraRelativeRect.Y = (1.0f - visibleCameraRelativeRect.Height) / 2.0f;

                var convertedToVideoMousePosition = new Point(visibleCameraRelativeRect.Width * mousePointerRelativePosition.X + visibleCameraRelativeRect.X,
                    visibleCameraRelativeRect.Height * mousePointerRelativePosition.Y + visibleCameraRelativeRect.Y);
                _isNeedTogetFocus = true;
                ComposingContext.Shared().ChromaKeyEffect.ChromakeyColorPoint = convertedToVideoMousePosition;
                Window.Current.CoreWindow.PointerCursor = BorderLayerLayoutView.ArrowCursor;
            }
           
            //Focus();
            e.Handled = true;
            
        }
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        private void ChromaKeyEffect_ColorChangedEvent(Windows.UI.Color color)
        {
            if (_isNeedTogetFocus)
            {
                Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    ParentView.Focus(FocusState.Pointer);
                });
                _isNeedTogetFocus = false;
            }
        }
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
 

        private void MediaMixerService_SwapChainReadyEvent(object sender, Microsoft.Graphics.Canvas.CanvasSwapChain swpchain)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            SwapchainPanel.Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, () =>
            {
                SwapchainPanel.SwapChain = swpchain;
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        #endregion

        protected override void ManipulateView(ILayoutViewManipulationArgs args)
        {
            base.ManipulateView(args);
            _viewTransform.X = ViewModel.Transform.TranslateX;
            _viewTransform.Y = ViewModel.Transform.TranslateY;
            var aspectRatio = (double)Services.MediaMixerService.VideoSize.Width / Services.MediaMixerService.VideoSize.Height;
            {
                if (ViewModel.KeepAspectRatio)
                {
                    
                    var neededWidth = Height * aspectRatio;
                    ViewModel.Transform.ScaleX = neededWidth / Services.MediaMixerService.VideoSize.Width;
                    ViewModel.Transform.ScaleY = Height / Services.MediaMixerService.VideoSize.Height;
                    _panelTransform.TranslateX = -((neededWidth - Width) / 2);
                }
                else
                {
                    if (Width <= Height * aspectRatio)
                    {
                        var neededWidth = Height * aspectRatio;
                        ViewModel.Transform.ScaleX = neededWidth / Services.MediaMixerService.VideoSize.Width;
                        ViewModel.Transform.ScaleY = Height / Services.MediaMixerService.VideoSize.Height;
                        _panelTransform.TranslateX = -(neededWidth - ViewModel.Position.Width) / 2;
                        _panelTransform.TranslateY = 0;
                    }
                    else
                    {
                        var neededHeight = Width / aspectRatio;
                        ViewModel.Transform.ScaleX = Width / Services.MediaMixerService.VideoSize.Width;
                        ViewModel.Transform.ScaleY = neededHeight / Services.MediaMixerService.VideoSize.Height;
                        _panelTransform.TranslateY = -(neededHeight - ViewModel.Position.Height) / 2;
                        _panelTransform.TranslateX = 0;
                    }
                }
            }
            _panelTransform.ScaleX = ViewModel.Transform.ScaleX;
            _panelTransform.ScaleY = ViewModel.Transform.ScaleY;
            CanvasSwapchainContainer.Clip = new RectangleGeometry()
            {
                Rect = new Rect(0, 0, ViewModel.Position.Width, ViewModel.Position.Height)
            };
            ParentView?.RecordCommand(_id, DataLayer.AppMessageType.MoveApp, _viewModel);
        }

        public bool CheckIsColorPickerTurned()
        {
            bool result = false;
            var shot = ViewModel.Shot;
            if (shot != null && shot.GreenScreenSettings.IsColorPickerOn)
            {
                result = true;
            }
            return result;
        }

        public override void StartRenderView() { }

        public override void StopRenderView() { }

        public override void Reconfigure()
        {
            base.Reconfigure();
            UpdateSizeForCurrentAspectRatioIfNeeded();
            InitTransforms();
            CanvasSwapchainContainer.Clip = new RectangleGeometry()
            {
                Rect = new Rect(0, 0, ViewModel.Position.Width , ViewModel.Position.Height)
            };
            SwapchainPanel.RenderTransform = _panelTransform;
        }

        public override void UpdateSizeAndPosition()
        {
            base.UpdateSizeAndPosition();

            UpdateSizeForCurrentAspectRatioIfNeeded();
            InitTransforms();
            CanvasSwapchainContainer.Clip = new RectangleGeometry()
            {
                Rect = new Rect(0, 0, ViewModel.Position.Width, ViewModel.Position.Height)
            };
            SwapchainPanel.RenderTransform = _panelTransform;
        }
        public override Task UpdateContextBitmap()
        {
            return Task.CompletedTask;
        }

        protected override void UpdateUiFromMementoModel(LayerLayoutMementoModel model)
        {
            Width = model.PreviousPosition.Width;
            Height = model.PreviousPosition.Height;
            var manipulationArgs = new ILayoutViewManipulationArgs
            {
                TranslateX = model.PreviousTransform.TranslateX - ViewModel.Transform.TranslateX,
                TranslateY = model.PreviousTransform.TranslateY - ViewModel.Transform.TranslateY,
                ScaleX = 1,
                ScaleY = 1
            };

            ManipulateView(manipulationArgs);
        }
        private bool _isFullSizeScaleCalculationNeeded = false;
        private void UpdateSizeForCurrentAspectRatioIfNeeded()
        {
                var aspectRatioOfPreview = ViewModel.Position.Width / ViewModel.Position.Height;
                var aspectRatioOfCamera = (double)ComposingContext.VideoSize.Width / ComposingContext.VideoSize.Height;
                {
                    CanvasSwapchainContainer.Clip = new RectangleGeometry()
                    {
                        Rect = new Rect(0, 0, ViewModel.Position.Width , ViewModel.Position.Height)
                    };
                    _isFullSizeScaleCalculationNeeded = true;
                }
        }

        private void InitTransforms()
        {
            _viewTransform = new TranslateTransform()
            {
                X = ViewModel.Transform.TranslateX,
                Y = ViewModel.Transform.TranslateY
            };
            RenderTransform = _viewTransform;
            var aspectRatio = (double)Services.MediaMixerService.VideoSize.Width / Services.MediaMixerService.VideoSize.Height;
            if (ViewModel.KeepAspectRatio)
            {
                
                var neededWidth = ViewModel.Position.Height * aspectRatio;
                _panelTransform = new CompositeTransform()
                {
                    ScaleX = neededWidth / ComposingContext.VideoSize.Width,
                    ScaleY = ViewModel.Position.Height / ComposingContext.VideoSize.Height,
                    TranslateX = _isFullSizeScaleCalculationNeeded ? -(neededWidth - ViewModel.Position.Width) / 2 : 0
                };
            }
            else
            {
                if (ViewModel.Position.Width <= ViewModel.Position.Height * aspectRatio)
                {
                    var neededWidth = ViewModel.Position.Height * aspectRatio;
                    _panelTransform = new CompositeTransform()
                    {
                        ScaleX = neededWidth / Services.MediaMixerService.VideoSize.Width,
                        ScaleY = ViewModel.Position.Height / Services.MediaMixerService.VideoSize.Height,
                        TranslateX = -(neededWidth - ViewModel.Position.Width) / 2,
                        TranslateY = 0
                    };
                }
                else
                {
                    var neededHeight = ViewModel.Position.Width / aspectRatio;
                    _panelTransform = new CompositeTransform()
                    {
                        ScaleX = ViewModel.Position.Width / Services.MediaMixerService.VideoSize.Width,
                        ScaleY = neededHeight / Services.MediaMixerService.VideoSize.Height,
                        TranslateX = 0,
                        TranslateY = -(neededHeight - ViewModel.Position.Height) / 2
                    };

                }
            }
            ViewModel.Transform.ScaleX = _panelTransform.ScaleX;
            ViewModel.Transform.ScaleY = _panelTransform.ScaleY;
            ViewModel.PreviousTransform.ScaleX = _panelTransform.ScaleX;
            ViewModel.PreviousTransform.ScaleY = _panelTransform.ScaleY;
        }
    }
}
