﻿using Microsoft.Graphics.Canvas;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.Helpers.Logger;
using Pitch.Services;
using TouchCastComposingEngine;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class WebLayerLayoutView : BaseLayoutView
    {
        public static readonly DependencyProperty BorderVisibilityProperty = DependencyProperty.Register(
            nameof(BorderVisibility), typeof(Visibility), typeof(WebLayerLayoutView), new PropertyMetadata(Visibility.Visible));

        #region Constants

        private const string pauseScript = @"
    var videos = document.querySelectorAll('video'),
    audios = document.querySelectorAll('audio');
    [].forEach.call(videos, function(video) { if(!video.paused) video.pause(); });
    [].forEach.call(audios, function(audio) { if(!audio.paused) audio.pause(); }); ";

        private const string playScript = @"
    var videos = document.querySelectorAll('video'),
    audios = document.querySelectorAll('audio');
    [].forEach.call(videos, function(video) { if(video.paused) video.play(); });
    [].forEach.call(audios, function(audio) { if(audio.paused) audio.play(); }); ";

        private const string scrollScript = @"window.pageYOffset.toString();";

        private readonly static TimeSpan ScrollPollingTime = TimeSpan.FromSeconds(1);
        #endregion

        #region Navigation Fields
        private const string localUrl = "ms-appx-web:///Resources/Styles/Html Pages/TransparentPage.html";
        private string _baseUrl;
        private bool _isNavigating;
        #endregion

        #region Fields
        private WebView _webView;
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private readonly DispatcherTimer _scrollPollingTimer = new DispatcherTimer();
        private double _scrollOffset;
        private double _prevOffset;
        private bool _isScrolling;
        private bool _isScrollProcessing;
        #endregion

        #region Properties

        private IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();


        public override LayoutViewState LayoutViewState
        {
            get => _layoutViewState;
            set
            {
                if (_layoutViewBorder != null)
                {
                    _layoutViewState = value;

                    HorizontalPanel.Visibility = VerticalPanel.Visibility = (_layoutViewState == LayoutViewState.EditState) ? Visibility.Collapsed : Visibility.Visible;
                    _webView.IsHitTestVisible = _layoutViewBorder.IsHitTestVisible = !(_layoutViewState == LayoutViewState.EditState);
                    _layoutViewBorder?.UpdateBorderStyle(_layoutViewState, ViewModel.IsStatic);

                    if (_layoutViewState == LayoutViewState.EditState)
                    {
                        _scrollPollingTimer.Interval = ScrollPollingTime;
                        _scrollPollingTimer.Start();
                    }
                    else
                    {
                        _scrollPollingTimer.Stop();
                    }
                }
            }
        }

        private new WebLayerLayout ViewModel => _viewModel as WebLayerLayout;

        public Visibility BorderVisibility
        {
            get => (Visibility)GetValue(BorderVisibilityProperty);
            set => SetValue(BorderVisibilityProperty, value);
        }

        #endregion

        #region Life Cycle

        public WebLayerLayoutView(WebLayerLayout model) : base(model)
        {
            _baseUrl = model.Url.UrlString;
            InitializeComponent();
            var mixer = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<IMediaMixerService>();
            _mediaMixerService = new WeakReference<IMediaMixerService>(mixer);

            Loaded += WebContentPresenter_Loaded;
            Unloaded += WebContentPresenter_Unloaded;
            _webView = new WebView(WebViewExecutionMode.SeparateThread);
            _webView.Source = new Uri(ViewModel.Url.UrlString);
            _stopwatch = new Stopwatch();
            RenderTransform = ViewModel.Transform;

            _scrollPollingTimer.Tick += ScrollPollingTimer_Tick;
            _renderableItem = _webView;
            _colorIndex = 0;
        }

#if DEBUG
        ~WebLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("*******************WebDraggableRecordableItemView Destructor ********************");
        }
#endif

        private void WebContentPresenter_Loaded(object sender, RoutedEventArgs e)
        {
            if (!LayoutRoot.Children.Contains(_webView))
            {
                LayoutRoot.Children.Add(_webView);
            }

            _webView.IsHitTestVisible = true;

            if (_layoutViewBorder != null)
            {
                _layoutViewBorder.IsHitTestVisible = true;
                LayoutRoot.Children.Add(_layoutViewBorder);
            }

            if (MediaMixer.State == TouchcastComposerServiceState.Recording)
            {
                StartRenderView();
            }

            CheckViewHotSpot();
        }

        private void WebContentPresenter_Unloaded(object sender, RoutedEventArgs e)
        {
            PauseMultimedia();
        }

        #endregion

        #region Public Methods

        public override void StartRenderView()
        {
            if (_isRenderStarted || _cancelationTokenSource == null) return;
            _isRenderStarted = true;

            if (_cancelationTokenSource.IsCancellationRequested)
            {
                _cancelationTokenSource?.Dispose();
                _cancelationTokenSource = new CancellationTokenSource();
            }
            _stopwatch.Start();
            PlayMultimedia();
            Task.Run(async () =>
            {
                while (!_cancelationTokenSource.IsCancellationRequested)
                {
                    var startTime = _stopwatch.Elapsed;
                    if (!_isScreenshotRuning && !_isNavigating)
                    {
                        _isScreenshotRuning = true;
                        await Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, async () =>
                        {
                            try
                            {
                                await RenderView();
                            }
                            catch (Exception ex)
                            {
                                LogQueue.WriteToFile($"Exception in WebLayerLayoutView.StartRenderView : {ex.Message}");
                                Debug.Write($"Exception is raised when trying to get sanpshot. Exception message: {ex.Message}");
                            }
                            finally
                            {
                                _isScreenshotRuning = false;
                            }
                        });

                    }
                    var duration = (_stopwatch.Elapsed - startTime).TotalMilliseconds;

                    if (duration < 200)
                    {
                        duration = 200;
                    }
                    else
                    {
                        duration *= 2;
                    }
                    await Task.Delay(TimeSpan.FromMilliseconds(duration));
                }
                _cancelationTokenSource.Dispose();
                _cancelationTokenSource = new CancellationTokenSource();
                _isRenderStarted = false;
                _stopwatch.Stop();
            }, _cancelationTokenSource.Token);
        }

        public override void StopRenderView()
        {
            _cancelationTokenSource.Cancel();
            PauseMultimedia();
        }

        public override Task PrepareViewBeforeHiding()
        {
            PauseMultimedia();
            return base.PrepareViewBeforeHiding();
        }

        public override async Task UpdateContextBitmap()
        {
            try
            {
                await RenderView();
            }
            catch (Exception e)
            {
                LogQueue.WriteToFile(e.Message);
            }
        }

        public override void SetHotSpot(bool isHotSpot)
        {
            base.SetHotSpot(isHotSpot);
            if (_webView != null)
            {
                _webView.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public override void Subscribe()
        {
            _cancelationTokenSource = new CancellationTokenSource();
            _webView.NavigationStarting += WebView_NavigationStarting;
            _webView.NavigationCompleted += WebView_NavigationCompleted;
            base.Subscribe();
        }

        public override void Unsubscribe()
        {
            StopRenderView();
            _webView.NavigationStarting -= WebView_NavigationStarting;
            _webView.NavigationCompleted -= WebView_NavigationCompleted;
            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Remove(_layoutViewBorder);
            }
            base.Unsubscribe();
        }

        public override void Clean()
        {
            StopRenderView();
            _mediaMixerService = null;
            if (_webView != null)
            {
                LayoutRoot.Children.Remove(_webView);
            }
            Loaded -= WebContentPresenter_Loaded;
            Unloaded -= WebContentPresenter_Unloaded;
            base.Clean();
        }

        protected override async Task RenderView()
        {
            if (_webView != null && _webView.Visibility == Visibility.Visible && !_isNavigating)
            {
                await base.RenderView();
            }
        }

        #endregion

        #region Private Methods

        private async void PlayMultimedia()
        {
            if (_webView != null)
            {
                await _webView.InvokeScriptAsync("eval", new string[] { playScript });
            }
        }

        private async void PauseMultimedia()
        {
            if (_webView != null)
            {
                await _webView.InvokeScriptAsync("eval", new string[] { pauseScript });
            }
        }

        private async Task<double> GetScrollOffset()
        {
            var result = await _webView.InvokeScriptAsync("eval", new string[] { scrollScript });
            var offset = 0.0;
            Double.TryParse(result, out offset);
            return offset;
        }

        #endregion

        #region Callbacks
        private async void WebView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            PauseMultimedia();
            _isNavigating = false;
            if (ViewModel.Url.UrlString != @"http://demo.touchcast.com/inclusionstartswithi/vapp.html")
            {
                await UpdateThumbnail();
            }
            _prevOffset = await GetScrollOffset();
        }

        private async void WebView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            _isNavigating = true;

            await ViewModel.UpdateUrlWithProperties(args.Uri);
            if (App.Locator.AppState == DataLayer.ApplicationState.Recording)
            {
                ParentView?.RecordCommand(_id, DataLayer.AppMessageType.NavigateApp, ViewModel);
            }
        }
        private async void ScrollPollingTimer_Tick(object sender, object e)
        {
            if (_isScrollProcessing) return;
            _isScrollProcessing = true;

            if (_webView != null)
            {
                var offset = await GetScrollOffset();

                if (offset != _prevOffset) _isScrolling = true;
                else
                {
                    if (_isScrolling)
                    {
                        _isScrolling = false;
                        if (Math.Abs(offset - _scrollOffset) > ActualHeight)
                        {
                            ParentView?.RecordCommand(_id, DataLayer.AppMessageType.PageChanged, _viewModel);
                            _scrollOffset = offset;
                        }
                    }
                }
                _prevOffset = offset;
            }
            _isScrollProcessing = false;
        }

        #endregion
    }
}
