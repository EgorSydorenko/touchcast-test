﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.Helpers.Logger;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class LineLayerLayoutView
    {
        public static readonly CoreCursor HandCursor = new CoreCursor(CoreCursorType.Hand, 2);
        public static readonly CoreCursor SizeAllCursor = new CoreCursor(CoreCursorType.SizeAll, 1);
        public static readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 0);

        #region Private Fields
        private const double arrowAngle = 40 * Math.PI / 180;
        private const double arrowLength = 15;

        private bool _isPressed = false;
        private Point _lastPosition;
        private bool _isLastRectLeft;
        private Shape _shape;
        private double _xProjection;
        private double _yProjection;
        #endregion

        public new ShapeLayerLayout ViewModel => _viewModel as ShapeLayerLayout;

        #region Life Cycle
        public LineLayerLayoutView(ShapeLayerLayout model) : base(model)
        {
            InitializeComponent();
            Loaded += LineLayerLayoutView_Loaded;
            ViewModel.StyleChanged += ViewModel_StyleChanged;
        }

#if DEBUG
        ~LineLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("************************* LineLayerLayoutView Desctructor **************************");
        }
#endif
        private void LineLayerLayoutView_Loaded(object sender, RoutedEventArgs e)
        {
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
            _xProjection = ViewModel.Position.Width;
            _yProjection = ViewModel.StrokeSize;

            _shape = LayoutRoot.Children.OfType<Shape>().FirstOrDefault(i => i is Shape);
            if (_shape == null)
            {
                if (ViewModel.ShapeType == Models.InkShapeType.Line)
                {
                    _shape = new Line
                    {
                        X1 = 1,
                        Y1 = 1,
                        Y2 = 1,
                        Stretch = Stretch.UniformToFill,
                    };
                }

                if (ViewModel.ShapeType == Models.InkShapeType.Arrow)
                {
                    var path = new Path();
                    _shape = path;
                    DrawArrow();
                }

                _shape.VerticalAlignment = VerticalAlignment.Top;

                LayoutRoot.Children.Add(_shape);
            }
            _renderableItem = _shape;

            ViewModel.UpdatePreviousPosition();
            UpdateStyle(ViewModel.IsNeedUpdateThumbnail);
            Calculate();
            RenderTransform = ViewModel.Transform;
        }

        #endregion

        public override void PrepareViewBeforeDisplaying(ShotView parentView)
        {
            _id = IdCounter++;
            VerticalAlignment = VerticalAlignment.Top;
            HorizontalAlignment = HorizontalAlignment.Left;
            _parentView = new WeakReference<ShotView>(parentView);

            CheckViewHotSpot();

            Canvas.SetZIndex(this, ViewModel.ZIndex);
        }

        protected override void ManipulateView(ILayoutViewManipulationArgs args)
        {
            var parentView = ParentView;
            if (parentView != null)
            {
                var translateX = ViewModel.Transform.TranslateX + args.TranslateX;
                var translateY = ViewModel.Transform.TranslateY + args.TranslateY;

                var minSize = ViewModel.SizeExtension;

                var translateLowX = (_xProjection >= 0) ? minSize.Width : minSize.Width - _xProjection;
                var translateHighX = (_xProjection >= 0) ? parentView.ActualWidth - minSize.Width - _xProjection : parentView.ActualWidth - minSize.Width;
                var translateLowY = (_yProjection >= 0) ? minSize.Height : minSize.Height - _yProjection;
                var translateHighY = (_yProjection >= 0) ? parentView.ActualHeight - minSize.Height - _yProjection : parentView.ActualHeight - minSize.Height;

                if (translateX < translateLowX) translateX = translateLowX;
                if (translateX > translateHighX)
                {
                    translateX = translateHighX;
                }
                if (translateY < translateLowY) translateY = translateLowY;
                if (translateY > translateHighY)
                {
                    translateY = translateHighY;
                }

                var point = HelperLineCheck(translateX, translateY, args.SenderType);

                var deltaX = point.X - ViewModel.Transform.TranslateX;
                var deltaY = point.Y - ViewModel.Transform.TranslateY;

                ViewModel.Transform.TranslateX = point.X;
                ViewModel.Transform.TranslateY = point.Y;
                parentView.RecordCommand(_id, DataLayer.AppMessageType.MoveApp, _viewModel);

                ViewModel.LineFirstPoint = CheckBorder(ViewModel.LineFirstPoint, deltaX, deltaY, ViewModel.LineSecondPoint);
                ViewModel.LineSecondPoint = CheckBorder(ViewModel.LineSecondPoint, deltaX, deltaY, ViewModel.LineFirstPoint);
            }

            ViewModel.UpdatePosition(ActualWidth, ActualHeight);
        }

        private Point HelperLineCheck(double posX, double posY, SenderType senderType)
        {
            var x = 0.0;
            var y = 0.0;
            var parentView = ParentView;
            if (parentView != null)
            {
                _yProjection = ViewModel.LineSecondPoint.Y - ViewModel.LineFirstPoint.Y;
                _xProjection = ViewModel.LineSecondPoint.X - ViewModel.LineFirstPoint.X;

                x = (_xProjection >= 0) ? posX : posX + _xProjection;
                y = (_yProjection >= 0) ? posY : posY + _yProjection;
                var newPosition = parentView.HelperLinesCheck(this,
                    new Rect { X = x, Y = y, Width = Math.Abs(_xProjection), Height = Math.Abs(_yProjection) },
                    senderType);

                x = (_xProjection >= 0) ? newPosition.X : newPosition.X - _xProjection;
                y = (_yProjection >= 0) ? newPosition.Y : newPosition.Y - _yProjection;
            }
            return new Point(x, y);
        }

        public override LayoutViewState LayoutViewState
        {
            get => _layoutViewState;
            set
            {
                _layoutViewState = value;
                switch (_layoutViewState)
                {
                    case LayoutViewState.NormalState:
                        Pins.Visibility = Visibility.Collapsed;
                        break;
                    case LayoutViewState.SelectedState:
                        Pins.Visibility = ViewModel.IsStatic ? Visibility.Collapsed : Visibility.Visible;
                        break;
                }
            }
        }

        public override void StartRenderView() { }

        public override void StopRenderView() { }

        public override Task UpdateContextBitmap()
        {
            return Task.CompletedTask;
        }

        public override void SetHotSpot(bool isHotSpot)
        {
            base.SetHotSpot(isHotSpot);
            if (_shape != null)
            {
                _shape.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public override void Reconfigure()
        {
            ViewModel.UpdatePreviousLinePoints();
            base.Reconfigure();
            UpdateStyle();
            Calculate();
            DrawArrow();
        }

        protected override void UpdateUiFromMementoModel(LayerLayoutMementoModel model)
        {
            UpdateStyle();
            Calculate();
            DrawArrow();
            RenderTransform = ViewModel.Transform;
        }

        #region Callbacks

        private void ViewModel_StyleChanged()
        {
            if (_shape != null)
            {
                DrawArrow();
                UpdateStyle();
            }
        }

        private void LayoutRoot_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = HandCursor;
        }

        private void LayoutRoot_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private void Rectangle_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeAllCursor;
            e.Handled = true;
        }

        private void Rectangle_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private void LeftRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void RightRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void LeftRectangle_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (_isPressed)
            {
                var currentPosition = e.GetCurrentPoint(ParentView);

                var dY = currentPosition.Position.Y - _lastPosition.Y;
                var dX = currentPosition.Position.X - _lastPosition.X;

                ViewModel.LineFirstPoint = CheckBorder(ViewModel.LineFirstPoint, dX, dY, ViewModel.LineSecondPoint);
                ViewModel.LineFirstPoint = HelperLineCheck(ViewModel.LineFirstPoint.X, ViewModel.LineFirstPoint.Y, SenderType.Mouse);

                var width = Width;
                Calculate();

                ViewModel.Transform.TranslateX -= (Width - width);
                ViewModel.Transform.CenterX = Width;

                DrawArrow();
                ViewModel.UpdatePosition(ActualWidth, ActualHeight);
                _lastPosition = currentPosition.Position;

                e.Handled = true;
            }
        }

        private void RightRectangle_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (_isPressed)
            {
                var currentPosition = e.GetCurrentPoint(ParentView);
                var dY = currentPosition.Position.Y - _lastPosition.Y;
                var dX = currentPosition.Position.X - _lastPosition.X;

                ViewModel.LineSecondPoint = CheckBorder(ViewModel.LineSecondPoint, dX, dY, ViewModel.LineFirstPoint);
                ViewModel.LineSecondPoint = HelperLineCheck(ViewModel.LineSecondPoint.X, ViewModel.LineSecondPoint.Y, SenderType.Mouse);

                Calculate();
                DrawArrow();
                ViewModel.UpdatePosition(ActualWidth, ActualHeight);

                _lastPosition = currentPosition.Position;

                e.Handled = true;
            }
        }

        private void LeftRectangle_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            _isPressed = true;
            var currentPosition = e.GetCurrentPoint(ParentView);
            LeftRect.CapturePointer(e.Pointer);

            ViewModel.UpdatePreviousPosition();

            if (!_isLastRectLeft)
            {
                ViewModel.Transform.CenterX = ActualWidth;
                ViewModel.Transform.TranslateX -= ActualWidth * (1 - Math.Cos(ViewModel.Transform.Rotation * Math.PI / 180));
                ViewModel.Transform.TranslateY += ActualWidth * Math.Sin(ViewModel.Transform.Rotation * Math.PI / 180);
            }
            _isLastRectLeft = true;
            _lastPosition = currentPosition.Position;

            e.Handled = true;
        }

        private async void LeftRectangle_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            (sender as Rectangle).ReleasePointerCapture(e.Pointer);
            ParentView?.HideHelperLines();
            if (_isLastRectLeft)
            {
                ViewModel.Transform.CenterX = 0;
                ViewModel.Transform.TranslateX += ActualWidth * (1 - Math.Cos(ViewModel.Transform.Rotation * Math.PI / 180));
                ViewModel.Transform.TranslateY -= ActualWidth * Math.Sin(ViewModel.Transform.Rotation * Math.PI / 180);
            }
            _isLastRectLeft = false;
            _isPressed = false;
            e.Handled = true;
            ViewModel.Shot.MovementChanged(ViewModel.GenerateMementoModel());
            await UpdateThumbnail();
        }

        private void RightRectangle_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            _isPressed = true;
            var currentPosition = e.GetCurrentPoint(ParentView);
            RightRect.CapturePointer(e.Pointer);
            if (_isLastRectLeft)
            {
                ViewModel.Transform.CenterX = 0;
                ViewModel.Transform.TranslateX += ActualWidth * (1 - Math.Cos(ViewModel.Transform.Rotation * Math.PI / 180));
                ViewModel.Transform.TranslateY -= ActualWidth * Math.Sin(ViewModel.Transform.Rotation * Math.PI / 180);
            }
            _isLastRectLeft = false;
            _lastPosition = currentPosition.Position;

            ViewModel.UpdatePreviousPosition();
            e.Handled = true;
        }

        private async void RightRectangle_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            (sender as Rectangle).ReleasePointerCapture(e.Pointer);
            ParentView?.HideHelperLines();
            _isPressed = false;
            ViewModel.Shot.MovementChanged(ViewModel.GenerateMementoModel());
            e.Handled = true;
            await UpdateThumbnail();
        }

        #endregion

        #region Private Methods

        private void Calculate()
        {
            var dY = ViewModel.LineSecondPoint.Y - ViewModel.LineFirstPoint.Y;
            var dX = ViewModel.LineSecondPoint.X - ViewModel.LineFirstPoint.X;
            _yProjection = dY;
            _xProjection = dX;

            ViewModel.CalculateLineWidthAndRotation();
            Width = ViewModel.Position.Width;
        }

        private Point CheckBorder(Point oldPosition, double dx, double dy, Point checkPoint)
        {
            var parentView = ParentView;
            if (parentView != null)
            {
                var newX = oldPosition.X + dx;
                var newY = oldPosition.Y + dy;

                var minSize = ViewModel.SizeExtension;

                if (newX < minSize.Width) newX = minSize.Width;
                if (newX > parentView.ActualWidth - minSize.Width)
                {
                    newX = parentView.ActualWidth - minSize.Width;
                }
                if (newY < minSize.Height) newY = minSize.Height;
                if (newY > parentView.ActualHeight - minSize.Height)
                {
                    newY = parentView.ActualHeight - minSize.Height;
                }

                var dY1 = newY - checkPoint.Y;
                var dX1 = newX - checkPoint.X;
                var width1 = Math.Sqrt(Math.Pow(dX1, 2) + Math.Pow(dY1, 2));
                if (width1 > parentView.ActualWidth)
                {
                    newY = checkPoint.Y + (dY1 / width1) * parentView.ActualWidth;
                    newX = checkPoint.X + (dX1 / width1) * parentView.ActualWidth;
                }
                return new Point(newX, newY);
            }
            return oldPosition;
        }

        private void DrawArrow()
        {
            if (ViewModel.ShapeType == Models.InkShapeType.Arrow)
            {
                var heigh = arrowLength * Math.Sin(arrowAngle / 2);
                var tmp = (ViewModel.StrokeSize / 2) / Math.Sin((Math.PI - arrowAngle) / 2);
                var shift = tmp / Math.Tan(arrowAngle / 2);

                var pathGeometry = new PathGeometry();
                var pathFigureCollection = new PathFigureCollection();
                var pathFigure1 = new PathFigure();
                pathFigure1.IsClosed = true;
                pathFigure1.StartPoint = new Point(1 + shift, 1);
                pathFigureCollection.Add(pathFigure1);
                pathGeometry.Figures = pathFigureCollection;

                var pathSegmentCollection = new PathSegmentCollection();
                var lineSeg = new LineSegment();
                lineSeg.Point = new Point(arrowLength + shift, heigh + 1);
                pathSegmentCollection.Add(lineSeg);
                lineSeg = new LineSegment();
                lineSeg.Point = new Point(arrowLength + shift, 1 - heigh);
                pathSegmentCollection.Add(lineSeg);

                pathFigure1.Segments = pathSegmentCollection;

                var lineGeometry = new LineGeometry();
                lineGeometry.StartPoint = new Point(shift, 1);
                lineGeometry.EndPoint = new Point(Width - 12, 1);

                var geometryGroup1 = new GeometryGroup();
                geometryGroup1.Children.Add(pathGeometry);
                geometryGroup1.Children.Add(lineGeometry);

                (_shape as Path).Data = geometryGroup1;
            }
        }

        private async void UpdateStyle(bool isNeedToUpdate = true)
        {
            _shape.Stroke = new SolidColorBrush(ViewModel.StrokeColor);
            _shape.Fill = new SolidColorBrush(ViewModel.StrokeColor);
            _shape.StrokeThickness = ViewModel.StrokeSize;
            _shape.Margin = ViewModel.ShapeType == Models.InkShapeType.Line ? new Thickness(-ViewModel.StrokeSize / 2) : new Thickness(0);
            if (isNeedToUpdate)
            {
                await UpdateThumbnail();
            }
        }
        #endregion
    }
}
