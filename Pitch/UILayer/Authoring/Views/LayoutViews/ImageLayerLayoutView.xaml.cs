﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using TouchCastComposingEngine;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class ImageLayerLayoutView : BaseLayoutView
    {
        public new ImageLayerLayout ViewModel => _viewModel as ImageLayerLayout;

        #region Life Cycle
        public ImageLayerLayoutView(ImageLayerLayout model) : base(model)
        {
            InitializeComponent();
            Loaded += ImageLayoutLayerView_Loaded;
            RenderTransform = ViewModel.Transform;
            ViewModel.ComposingContextElement.Type = ComposingContextElementType.Bitmap;
        }

#if DEBUG
        ~ImageLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("************************* ImageLayerLayoutView Desctructor **************************");
        }
#endif

        private async void ImageLayoutLayerView_Loaded(object sender, RoutedEventArgs e)
        {
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
            BitmapImage bmp = new BitmapImage(new Uri(ViewModel.File.Path));
            ImageView.Source = bmp;
            if (ViewModel.ComposingContextElement.Bitmap == null && App.Locator.AppState == DataLayer.ApplicationState.Recording)
            {
                await UpdateContextBitmap();
            }
            await UpdateSizeForCurrentAspectRatioIfNeeded();
        }

        private void ImageLayoutLayerView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ImageLayoutLayerView_Loaded;
            Unloaded -= ImageLayoutLayerView_Unloaded;
        }

        public override void Subscribe()
        {
            base.Subscribe();
            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Add(_layoutViewBorder);
            }
        }

        public override async Task UpdateContextBitmap()
        {
            if (ViewModel.ComposingContextElement.Bitmap == null)
            {
                using (var fileStream = await ViewModel.File.OpenReadAsync())
                {
                    ViewModel.ComposingContextElement.Bitmap = await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(ComposingContext.Shared().PreviewingDevice, fileStream);
                }
            }
        }

        public override void Unsubscribe()
        {
            LayoutRoot.Children.Remove(_layoutViewBorder);

            base.Unsubscribe();
        }

        public override void Clean()
        {
            Loaded -= ImageLayoutLayerView_Loaded;
            Unloaded -= ImageLayoutLayerView_Unloaded;
            base.Clean();
        }

        protected override async Task UpdateThumbnail()
        {
            await ViewModel.UpdateThumbnail();
        }
        #endregion

        private async Task UpdateSizeForCurrentAspectRatioIfNeeded()
        {
            if (ViewModel.KeepAspectRatio || ViewModel.IsBackground)
            {
                var imageProps = await ViewModel.File.Properties.GetImagePropertiesAsync();
                _aspectRatio = (double)imageProps.Width / imageProps.Height;

                UpdateSizeWithAspectRatio(_aspectRatio);
            }
        }

        public async override void StartRenderView()
        {
            await UpdateContextBitmap();
        }

        public override void StopRenderView() { }

        public override void SetHotSpot(bool isHotSpot)
        {
            base.SetHotSpot(isHotSpot);
            ImageView.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
        }

        public override void Reconfigure()
        {
            base.Reconfigure();
            UpdateSizeWithAspectRatio(_aspectRatio);
        }
    }
}
