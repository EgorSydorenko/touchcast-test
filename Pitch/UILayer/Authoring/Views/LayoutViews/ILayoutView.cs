﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.Helpers;
using TouchCastComposingEngine;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.Storage.Streams;
using Windows.Graphics.Imaging;
using Pitch.Helpers.Logger;
using System.Linq;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public enum SenderType
    {
        Mouse,
        Keyboard
    }

    public class ILayoutViewManipulationArgs
    {
        public double TranslateX { get; set; }
        public double TranslateY { get; set; }
        public double ScaleX { get; set; }
        public double ScaleY { get; set; }
        public SenderType SenderType { get; set; } = SenderType.Mouse;
    }

    public enum LayoutViewState
    {
        NormalState,
        SelectedState,
        EditState
    }

    public delegate void LayoutViewUpdated(object sender, object data);

    public abstract class ILayoutView : ISubscribeView
    {
        protected static int IdCounter = 1;
        protected LayerLayout _viewModel;
        protected WeakReference<ShotView> _parentView;
        protected LayoutViewState _layoutViewState;
        protected int _id;

        protected ShotView ParentView => _parentView?.TryGetObject();

        public ILayoutView(LayerLayout model)
        {
            _viewModel = model;
        }

        public abstract void PrepareViewBeforeDisplaying(ShotView _parent);

        public abstract Task PrepareViewBeforeHiding();

        protected abstract void ManipulateView(ILayoutViewManipulationArgs args);
    }

    public class BaseLayoutView : ILayoutView
    {
        #region Fields

        private bool _isFirstShow;
        protected double _canvasScaleX;
        protected double _canvasScaleY;
        protected BorderLayerLayoutView _layoutViewBorder;
        protected double _aspectRatio = 1.0;
        private RegionTapped _regionTapped;
        private Point _lastPoint;
        private bool _isReadyToChangeStyle;
        private bool _isManipulationDelta;
        private Rect _position;
        private CompositeTransform _transform;

        private RenderTargetBitmap _renderTargetBitmap = new RenderTargetBitmap();
        private Models.AsyncLock _renderViewLock = new Models.AsyncLock(1);

        protected CancellationTokenSource _cancelationTokenSource;
        protected FrameworkElement _renderableItem;
        protected int _colorIndex;
        protected bool _isRenderStarted;
        protected bool _isScreenshotRuning;
        protected Stopwatch _stopwatch;
        #endregion

        #region Properties

        public bool IsManipulationDelta
        {
            get { return _isManipulationDelta; }
            set { _isManipulationDelta = value; }
        }

        public LayerLayout ViewModel => _viewModel;

        public int Id => _id;

        public virtual LayoutViewState LayoutViewState
        {
            get => _layoutViewState;
            set
            {
                _layoutViewState = value;
                _layoutViewBorder?.UpdateBorderStyle(_layoutViewState, ViewModel.IsStatic);
            }
        }

        public double AspectRatio => _aspectRatio;
        #endregion

        #region Life Cycle

        public BaseLayoutView(LayerLayout model) : base(model)
        {
            ManipulationMode = ManipulationModes.TranslateX | ManipulationModes.TranslateY;
            model.LayerLayoutWasChangedEvent += Model_LayerLayoutWasChangedEvent;
            model.LayerLayoutRaiseThumbnailGenerationEvent += Model_LayerLayoutRaiseThumbnailGenerationEvent;
            Loaded += ILayoutView_Loaded;
            Unloaded += ILayoutViewView_Unloaded;
            _isFirstShow = ViewModel.IsSelectedAfterCreation;
            AllowDrop = true;
        }

#if DEBUG
        ~BaseLayoutView()
        {
            Debug.WriteLine("************* BaseLayoutView Destructor ***************");
        }
#endif

        private void ILayoutView_Loaded(object sender, RoutedEventArgs e)
        {
            Subscribe();

            UpdateScales();

            LayoutViewState = _isFirstShow ? LayoutViewState.SelectedState : LayoutViewState.NormalState;
            SetHotSpot(ViewModel.IsHotSpot);
            _isFirstShow = false;
        }

        private void ILayoutViewView_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        public override void Subscribe()
        {
            if (_layoutViewBorder == null)
            {
                _layoutViewBorder = new BorderLayerLayoutView
                {
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch
                };

                _layoutViewBorder.PinMovedEvent += LayoutViewBorder_PinMovedEvent;
                _layoutViewBorder.PinPressedEvent += LayoutViewBorder_PinPressedEvent;
                _layoutViewBorder.PinReleasedEvent += LayoutViewBorder_PinReleasedEvent;
                _layoutViewBorder.LeftBottomPinManipulationEvent += LayoutViewBorder_LeftBottomPinManipulationEvent;
                _layoutViewBorder.LeftTopPinManipulationEvent += LayoutViewBorder_LeftTopPinManipulationEvent;
                _layoutViewBorder.RightBottomPinManipulationEvent += LayoutViewBorder_RightBottomPinManipulationEvent;
                _layoutViewBorder.RightTopPinManipulationEvent += LayoutViewBorder_RightTopPinManipulationEvent;
            }

            PointerPressed += BaseLayoutView_PointerPressed;
            PointerReleased += BaseLayoutView_PointerReleased;
            ManipulationDelta += ILayoutViewView_ManipulationDelta;
            Window.Current.CoreWindow.SizeChanged += CoreWindow_SizeChanged;
        }

        public override void Unsubscribe()
        {
            if (_layoutViewBorder != null)
            {
                _layoutViewBorder.PinMovedEvent -= LayoutViewBorder_PinMovedEvent;
                _layoutViewBorder.PinPressedEvent -= LayoutViewBorder_PinPressedEvent;
                _layoutViewBorder.PinReleasedEvent -= LayoutViewBorder_PinReleasedEvent;
                _layoutViewBorder.LeftBottomPinManipulationEvent -= LayoutViewBorder_LeftBottomPinManipulationEvent;
                _layoutViewBorder.LeftTopPinManipulationEvent -= LayoutViewBorder_LeftTopPinManipulationEvent;
                _layoutViewBorder.RightBottomPinManipulationEvent -= LayoutViewBorder_RightBottomPinManipulationEvent;
                _layoutViewBorder.RightTopPinManipulationEvent -= LayoutViewBorder_RightTopPinManipulationEvent;
                _layoutViewBorder = null;
            }
            PointerPressed -= BaseLayoutView_PointerPressed;
            PointerReleased -= BaseLayoutView_PointerReleased;
            ManipulationDelta -= ILayoutViewView_ManipulationDelta;
            Window.Current.CoreWindow.SizeChanged -= CoreWindow_SizeChanged;
        }

        public virtual void UpdateFocusIfNeeded(bool isNeedUpdateFocus)
        {
            if (isNeedUpdateFocus)
            {
                Focus(FocusState.Programmatic);
            }
        }

        public override void Clean()
        {
            Unsubscribe();
            ViewModel.LayerLayoutWasChangedEvent -= Model_LayerLayoutWasChangedEvent;
            ViewModel.LayerLayoutRaiseThumbnailGenerationEvent -= Model_LayerLayoutRaiseThumbnailGenerationEvent;
            Unloaded -= ILayoutViewView_Unloaded;
            Loaded -= ILayoutView_Loaded;
        }

        #endregion

        #region Public Methods

        public virtual void StartRenderView() { throw new Exception("Need to Override method"); }

        public virtual void StopRenderView() { throw new Exception("Need to Override method"); }

        public virtual Task UpdateContextBitmap() { throw new Exception("Need to Override method"); }


        private bool _isUpdateThumbnailInProcess;
        protected async virtual Task UpdateThumbnail()
        {
            if (_isUpdateThumbnailInProcess || ComposingContext.Shared().IsRecording || _renderableItem == null)
            {
                return;
            }
            _isUpdateThumbnailInProcess = true;
            try
            {
                byte[] pixels = null;
                for (int i = 0; i < 3; i++)
                {
                    await _renderTargetBitmap.RenderAsync(_renderableItem);
                    var pixelBuffer = await _renderTargetBitmap.GetPixelsAsync();
                    pixels = pixelBuffer.ToArray();
                    if (pixels.Any(x => x != _colorIndex))
                    {
                        break;
                    }
                }

                using (var stream = new InMemoryRandomAccessStream())
                {
                    await EncodeBufferToStream(pixels, stream);
                    await ViewModel.UpdateThumbnailFile(stream, ActualWidth, ActualHeight);
                    if (ViewModel.ComposingContextElement.Bitmap == null)
                    {
                        ViewModel.ComposingContextElement.Bitmap = await CanvasBitmapHelper.FromFile(ViewModel.ThumbnailFile);
                    }
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in UpdateThumbnail : {ex.Message}");
            }
            finally
            {
                _isUpdateThumbnailInProcess = false;
            }
        }
        protected virtual async Task RenderView()
        {
            await _renderViewLock.WaitAsync();
            try
            {
                if (_renderableItem != null && _renderableItem.Visibility == Visibility.Visible)
                {
                    await _renderTargetBitmap.RenderAsync(_renderableItem);
                    var pixelBuffer = await _renderTargetBitmap.GetPixelsAsync();
                    var pixels = pixelBuffer.ToArray();
                    if (pixels.Any(p => p != _colorIndex))
                    {
                        using (var stream = new InMemoryRandomAccessStream())
                        {
                            await EncodeBufferToStream(pixels, stream);
                            var device = ComposingContext.Shared().PreviewingDevice;
                            if (device?.Device != null)
                            {
                                _viewModel.ComposingContextElement.Bitmap = await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(ComposingContext.Shared().PreviewingDevice, stream);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (_parentView != null && ParentView.Visibility == Visibility.Collapsed)
                {
                    _cancelationTokenSource.Cancel();
                }
                LogQueue.WriteToFile($"Exception in RenderView : {ex.Message}");
            }
            finally
            {
                _renderViewLock.Release();
            }
        }
        public virtual void SetHotSpot(bool isHotSpot)
        {
            ViewModel.IsHotSpot = isHotSpot;
        }

        public override void PrepareViewBeforeDisplaying(ShotView parentView)
        {
            _id = IdCounter++;
            VerticalAlignment = VerticalAlignment.Top;
            HorizontalAlignment = HorizontalAlignment.Left;
            _parentView = new WeakReference<ShotView>(parentView);

            CheckViewHotSpot();

            Canvas.SetZIndex(this, ViewModel.ZIndex);

            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;

            UpdatePosition();
        }

        public override Task PrepareViewBeforeHiding()
        {
            LayoutViewState = LayoutViewState.NormalState;
            return Task.CompletedTask;
        }

        protected virtual void UpdateUiFromMementoModel(LayerLayoutMementoModel model)
        {
            RenderTransform = model.PreviousTransform;
            Width = model.PreviousPosition.Width;
            Height = model.PreviousPosition.Height;
        }

        protected void CheckViewHotSpot()
        {
        }

        public virtual void UpdateSizeAndPosition()
        {
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
        }

        public async void ChangePositionBy(double x, double y)
        {
            if (ViewModel.IsStatic) return;
            ILayoutViewManipulationArgs manipulationArgs = new ILayoutViewManipulationArgs
            {
                TranslateX = x,
                TranslateY = y,
                ScaleX = 1,
                ScaleY = 1,
                SenderType = SenderType.Keyboard
            };
            ManipulateView(manipulationArgs);
            await ViewModel.UpdateThumbnail();
        }

        public async Task ChangeSizeBy(double width, double height)
        {
            if (ViewModel.IsStatic) return;
            ILayoutViewManipulationArgs manipulationArgs = new ILayoutViewManipulationArgs
            {
                TranslateX = 0,
                TranslateY = 0,
                ScaleX = width / Width,
                ScaleY = height / Height,
                SenderType = SenderType.Keyboard,
            };
            ManipulateView(manipulationArgs);
            await ViewModel.UpdateThumbnail();
        }

        public virtual void Reconfigure()
        {
            if (ViewModel.IsInitializedAsBackground)
            {
                ViewModel.Shot?.SetAsBackground(ViewModel);
                return;
            }
            CompositeTransform previousTransform = new CompositeTransform
            {
                TranslateX = ViewModel.Transform.TranslateX,
                TranslateY = ViewModel.Transform.TranslateY,
                ScaleX = ViewModel.Transform.ScaleX,
                ScaleY = ViewModel.Transform.ScaleY,
                Rotation = ViewModel.Transform.Rotation,
            };
            Rect previousPosition = ViewModel.Position;

            LayerLayoutMementoModel mementoModel = ViewModel.GenerateMementoModel();
            ViewModel.UpdateToInitialData();

            SetHotSpot(ViewModel.IsHotSpot);

            mementoModel.PreviousTransform = previousTransform;
            mementoModel.PreviousPosition = previousPosition;

            ViewModel.Shot?.Project?.UndoRedoOriginator.PushNewUndoAction(DataLayer.UndoRedo.UndoRedoMementoType.Change, mementoModel);

            RenderTransform = ViewModel.Transform;
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
            ViewModel.UpdateThumbnail();
        }

        #endregion

        #region Protected Methods

        protected override void ManipulateView(ILayoutViewManipulationArgs args)
        {
            var parentView = ParentView;
            if (parentView != null)
            {
                var translateX = ViewModel.Transform.TranslateX + args.TranslateX;
                var translateY = ViewModel.Transform.TranslateY + args.TranslateY;

                var newWidth = Width * args.ScaleX;
                var newHeight = Height * args.ScaleY;

                if (translateX < 0) translateX = 0;
                if (translateY < 0) translateY = 0;

                if (translateX + newWidth > parentView.ActualWidth)
                {
                    translateX = parentView.ActualWidth - newWidth;
                }

                if (translateY + newHeight > parentView.ActualHeight)
                {
                    translateY = parentView.ActualHeight - newHeight;
                }
                Rect newPosition = parentView.HelperLinesCheck(this,
                    new Rect() { X = translateX, Y = translateY, Width = newWidth, Height = newHeight },
                    args.SenderType);

                Width = newPosition.Width;
                Height = newPosition.Height;

                ViewModel.Transform.TranslateX = newPosition.X;
                ViewModel.Transform.TranslateY = newPosition.Y;

                UpdatePosition();

                parentView.RecordCommand(_id, DataLayer.AppMessageType.MoveApp, _viewModel);
            }
        }

        protected void UpdateSizeWithAspectRatio(double aspectRatio)
        {
            var newWidth = (aspectRatio <= 1) ? ViewModel.Position.Height * aspectRatio : ViewModel.Position.Width;
            var newHeight = (aspectRatio <= 1) ? ViewModel.Position.Height : ViewModel.Position.Width / aspectRatio;

            if (newHeight > ViewModel.Position.Height)
            {
                newWidth *= ViewModel.Position.Height / newHeight;
                newHeight = ViewModel.Position.Height;
            }
            if (newWidth > ViewModel.Position.Width)
            {
                newHeight *= ViewModel.Position.Width / newWidth;
                newWidth = ViewModel.Position.Width;
            }

            if (ViewModel.IsBackground)
            {
                if (newWidth > Services.MediaMixerService.VideoSize.Width)
                {
                    newWidth = Services.MediaMixerService.VideoSize.Width;
                }
                else if (newHeight > Services.MediaMixerService.VideoSize.Height)
                {
                    newHeight = Services.MediaMixerService.VideoSize.Height;
                }
            }
            Width = newWidth;
            Height = newHeight;

            var x = (ViewModel.Position.Width - Width) / 2;
            var y = (ViewModel.Position.Height - Height) / 2;
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            ViewModel.UpdatePosition(Width, Height, x, y);
            ViewModel.Transform.TranslateX = ViewModel.Position.X;
            ViewModel.Transform.TranslateY = ViewModel.Position.Y;
            ViewModel.UpdateThumbnail();
        }

        #endregion

        #region Callbacks

        private async Task Model_LayerLayoutRaiseThumbnailGenerationEvent(object sender)
        {
            await UpdateThumbnail();
        }

        private void Model_LayerLayoutWasChangedEvent(LayerLayoutMementoModel model)
        {
            UpdateUiFromMementoModel(model);
            SetHotSpot(model.IsHotSpot);
            Canvas.SetZIndex(this, model.ZIndex);
            UpdateLayout();
        }

        protected void LeftTopRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            _regionTapped = RegionTapped.LeftTopCornerTapped;
            CalculateTransformations();
            e.Handled = true;
        }

        protected void RightTopRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            _regionTapped = RegionTapped.RightTopCornerTapped;
            CalculateTransformations();
            e.Handled = true;
        }

        protected void RightBottomRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            _regionTapped = RegionTapped.RightBottomCornerTapped;
            CalculateTransformations();
            e.Handled = true;
        }

        protected void LeftBottomRectangle_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            _regionTapped = RegionTapped.LeftBottomCornerTapped;
            CalculateTransformations();
            e.Handled = true;
        }

        private void LayoutViewBorder_RightTopPinManipulationEvent()
        {
            _regionTapped = RegionTapped.RightTopCornerTapped;
            CalculateTransformations();
        }

        private void LayoutViewBorder_RightBottomPinManipulationEvent()
        {
            _regionTapped = RegionTapped.RightBottomCornerTapped;
            CalculateTransformations();
        }

        private void LayoutViewBorder_LeftTopPinManipulationEvent()
        {
            _regionTapped = RegionTapped.LeftTopCornerTapped;
            CalculateTransformations();
        }

        private void LayoutViewBorder_LeftBottomPinManipulationEvent()
        {
            _regionTapped = RegionTapped.LeftBottomCornerTapped;
            CalculateTransformations();
        }

        private async void BaseLayoutView_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            ReleasePointerCapture(e.Pointer);
            ParentView?.HideHelperLines();
            if (_isReadyToChangeStyle && !_isManipulationDelta)
            {
                LayoutViewState = (ViewModel.IsEditable) ? LayoutViewState.EditState : LayoutViewState.NormalState;

                var parentView = ParentView;
                if (parentView != null && LayoutViewState == LayoutViewState.NormalState)
                {
                    parentView.FireLayoutViewSelectionEvent(null, true);
                }
            }
            if (_isManipulationDelta)
            {
                LayerLayoutMementoModel mementoModel = ViewModel.GenerateMementoModel();
                mementoModel.PreviousPosition = ViewModel.Position;
                ViewModel.Shot.MovementChanged(mementoModel);
                await UpdateThumbnail();
            }
            _isReadyToChangeStyle = false;
        }

        private void BaseLayoutView_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            CapturePointer(e.Pointer);
            bool isMediaMixerView = this is MediaMixerLayerLayoutView;
            if (isMediaMixerView)
            {
                if ((this as MediaMixerLayerLayoutView).CheckIsColorPickerTurned())
                {
                    return;
                }
            }
            _isManipulationDelta = false;
            if (LayoutViewState == LayoutViewState.NormalState)
            {
                var parentView = ParentView;
                if (parentView != null)
                {
                    parentView.UpdateAllLayerViewToNormalStyle();
                    LayoutViewState = LayoutViewState.SelectedState;
                    parentView.FireLayoutViewSelectionEvent(this, true);
                }
            }
            else if (LayoutViewState == LayoutViewState.SelectedState)
            {
                _isReadyToChangeStyle = true;
            }

            ViewModel.UpdatePreviousPosition();

            if (!isMediaMixerView)
            {
                e.Handled = true;
            }
        }

        private void CoreWindow_SizeChanged(CoreWindow sender, WindowSizeChangedEventArgs args)
        {
            UpdateScales();
        }

        private void LayoutViewBorder_PinMovedEvent(PointerRoutedEventArgs e)
        {
            var parentView = ParentView;
            if (parentView != null)
            {
                var currentPosition = e.GetCurrentPoint(parentView);
                _lastPoint = currentPosition.Position;
            }
        }

        private async void LayoutViewBorder_PinReleasedEvent()
        {
            ParentView?.HideHelperLines();
            LayerLayoutMementoModel mementoModel = ViewModel.GenerateMementoModel();
            if (ViewModel.Position.Width != _position.Width || ViewModel.Position.Height != _position.Height)
            {
                mementoModel.PreviousPosition = _position;
                mementoModel.PreviousTransform = _transform;
                ViewModel.Shot.MovementChanged(mementoModel);
                await UpdateThumbnail();
            }
        }

        private void LayoutViewBorder_PinPressedEvent()
        {
            _position = ViewModel.Position;
            _transform = new CompositeTransform
            {
                TranslateX = ViewModel.Transform.TranslateX,
                TranslateY = ViewModel.Transform.TranslateY,
                ScaleX = ViewModel.Transform.ScaleX,
                ScaleY = ViewModel.Transform.ScaleY,
                Rotation = ViewModel.Transform.Rotation,
            };
        }

        protected void ILayoutViewView_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (!ViewModel.IsStatic && LayoutViewState != LayoutViewState.EditState)
            {
                _isManipulationDelta = true;
                ILayoutViewManipulationArgs manipulationArgs = new ILayoutViewManipulationArgs
                {
                    TranslateX = e.Delta.Translation.X * _canvasScaleX,
                    TranslateY = e.Delta.Translation.Y * _canvasScaleY,
                    ScaleX = 1,
                    ScaleY = 1
                };

                ManipulateView(manipulationArgs);
            }
        }

        #endregion

        #region Private Methods

        private void UpdatePosition()
        {
            ViewModel.UpdatePosition(Width, Height);
        }

        private void UpdateScales()
        {
            var parentView = ParentView;
            if (parentView != null)
            {
                var viewBoxContainer = UIHelper.FindVisualParent<Viewbox>(parentView);
                if (viewBoxContainer != null)
                {
                    _canvasScaleX = Services.MediaMixerService.VideoSize.Width / viewBoxContainer.ActualWidth;
                    _canvasScaleY = Services.MediaMixerService.VideoSize.Height / viewBoxContainer.ActualHeight;
                }
            }
        }

        private void CalculateTransformations()
        {
            var xPos = _lastPoint.X > ComposingContext.VideoSize.Width ? ComposingContext.VideoSize.Width : _lastPoint.X < 0 ? 0 : _lastPoint.X;
            var newWidth = (_regionTapped == RegionTapped.LeftTopCornerTapped || _regionTapped == RegionTapped.LeftBottomCornerTapped)
                ? ViewModel.Transform.TranslateX - xPos + ViewModel.Position.Width
                : xPos - ViewModel.Transform.TranslateX;

            var yPos = _lastPoint.Y > ComposingContext.VideoSize.Height ? ComposingContext.VideoSize.Height : _lastPoint.Y < 0 ? 0 : _lastPoint.Y;
            var newHeight = (_regionTapped == RegionTapped.LeftTopCornerTapped || _regionTapped == RegionTapped.RightTopCornerTapped)
                ? ViewModel.Transform.TranslateY - yPos + ViewModel.Position.Height
                : yPos - ViewModel.Transform.TranslateY;

            newHeight = newHeight > ComposingContext.VideoSize.Height ? ComposingContext.VideoSize.Height : newHeight;
            newWidth = newWidth > ComposingContext.VideoSize.Width ? ComposingContext.VideoSize.Width : newWidth;

            if (ViewModel.KeepAspectRatio)
            {
                if (_aspectRatio > 1)
                {
                    newWidth = newWidth < LayerLayoutTemplate.MinWidth ? LayerLayoutTemplate.MinWidth : newWidth;
                    newHeight = Height * newWidth / Width;
                    if (newHeight > ComposingContext.VideoSize.Height)
                    {
                        newHeight = ComposingContext.VideoSize.Height;
                        newWidth = newHeight * Width / Height;
                    }
                }
                else
                {
                    newHeight = newHeight < LayerLayoutTemplate.MinHeight ? LayerLayoutTemplate.MinHeight : newHeight;
                    newWidth = newHeight * Width / Height;
                }
            }
            else
            {
                newWidth = newWidth < LayerLayoutTemplate.MinWidth ? LayerLayoutTemplate.MinWidth : newWidth;
                newHeight = newHeight < LayerLayoutTemplate.MinHeight ? LayerLayoutTemplate.MinHeight : newHeight;
            }

            double translateX = 0;
            double translateY = 0;
            switch (_regionTapped)
            {
                case RegionTapped.LeftTopCornerTapped:
                    translateX = ViewModel.Position.Width - newWidth;
                    translateY = ViewModel.Position.Height - newHeight;
                    break;
                case RegionTapped.LeftBottomCornerTapped:
                    translateX = ViewModel.Position.Width - newWidth;
                    break;
                case RegionTapped.RightTopCornerTapped:
                    translateY = ViewModel.Position.Height - newHeight;
                    break;
            }

            ILayoutViewManipulationArgs manipulationArgs = new ILayoutViewManipulationArgs
            {
                TranslateX = translateX,
                TranslateY = translateY,
                ScaleX = newWidth / Width,
                ScaleY = newHeight / Height,
            };

            ManipulateView(manipulationArgs);
        }
        private async Task<IRandomAccessStream> EncodeBufferToStream(byte[] pixels, IRandomAccessStream stream)
        {
            var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);
            encoder.SetPixelData(BitmapPixelFormat.Bgra8,
                                 BitmapAlphaMode.Premultiplied,
                                 (uint)_renderTargetBitmap.PixelWidth,
                                 (uint)_renderTargetBitmap.PixelHeight,
                                 96,
                                 96,
                                 pixels);

            await encoder.FlushAsync();
            stream.Seek(0);
            return stream;
        }

        #endregion
    }
}
