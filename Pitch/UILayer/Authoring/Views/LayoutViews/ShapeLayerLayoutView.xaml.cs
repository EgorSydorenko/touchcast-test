﻿using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Pitch.Helpers.Logger;
using System.Linq;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class ShapeLayerLayoutView : BaseLayoutView
    {
        #region Fields

        private Shape _shape;

        #endregion

        #region Properties

        public new ShapeLayerLayout ViewModel => _viewModel as ShapeLayerLayout;

        public override LayoutViewState LayoutViewState
        {
            get => _layoutViewState;
            set
            {
                if (_layoutViewBorder != null)
                {
                    _layoutViewState = value;
                    _layoutViewBorder?.UpdateBorderStyle(_layoutViewState, ViewModel.IsStatic);
                }
            }
        }

        #endregion

        #region Life Cycle

        public ShapeLayerLayoutView(ShapeLayerLayout model) : base(model)
        {
            InitializeComponent();

            Loaded += ShapeLayerLayoutView_Loaded;
            RenderTransform = ViewModel.Transform;

            ViewModel.StyleChanged += ViewModel_StyleChanged;
        }

#if DEBUG
        ~ShapeLayerLayoutView()
        {
            System.Diagnostics.Debug.WriteLine("************************* ShapeLayerLayoutView Desctructor **************************");
        }
#endif
        private async void ShapeLayerLayoutView_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.UpdatePreviousPosition();
            Width = ViewModel.Position.Width;
            Height = ViewModel.Position.Height;
            CreateShapeFromModel();
            if (ViewModel.IsNeedUpdateThumbnail)
            {
                await UpdateThumbnail();
                ViewModel.IsNeedUpdateThumbnail = false;
            }
        }

        private void ShapeLayerLayoutView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ShapeLayerLayoutView_Loaded;
            Unloaded -= ShapeLayerLayoutView_Unloaded;
        }

        public override void Subscribe()
        {
            base.Subscribe();
            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Add(_layoutViewBorder);
                Canvas.SetZIndex(_layoutViewBorder, 1000);
            }
        }

        public override void Unsubscribe()
        {
            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Remove(_layoutViewBorder);
            }
            base.Unsubscribe();
        }

        public override void Clean()
        {
            Loaded -= ShapeLayerLayoutView_Loaded;
            base.Clean();
        }

        #endregion

        #region Public Methods

        public override void StartRenderView() { }

        public override void StopRenderView() { }

        public override Task UpdateContextBitmap()
        {
            return Task.CompletedTask;
        }

        public override void SetHotSpot(bool isHotSpot)
        {
            base.SetHotSpot(isHotSpot);
            if (_shape != null)
            {
                _shape.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        #endregion

        #region Callbacks

        private async void ViewModel_StyleChanged()
        {
            if (_shape != null)
            {
                _shape.Stroke = new SolidColorBrush(ViewModel.StrokeColor);
                _shape.Fill = new SolidColorBrush(ViewModel.FillColor);
                _shape.StrokeThickness = ViewModel.StrokeSize;
                await UpdateThumbnail();
            }
        }

        private void ShapeLayoutView_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (!ViewModel.IsStatic && LayoutViewState != LayoutViewState.EditState)
            {
                IsManipulationDelta = true;
                var manipulationArgs = new ILayoutViewManipulationArgs
                {
                    TranslateX = e.Delta.Translation.X * _canvasScaleX,
                    TranslateY = e.Delta.Translation.Y * _canvasScaleY,
                    ScaleX = 1,
                    ScaleY = 1
                };

                ManipulateView(manipulationArgs);
            }
        }

        #endregion

        #region Private Methods
        private void CreateShapeFromModel()
        {
            if (LayoutRoot.Children.OfType<Shape>().FirstOrDefault(i => i is Shape) != null)
            {
                return;
            }
            switch (ViewModel.ShapeType)
            {
                case InkShapeType.Circle:
                    _shape = new Ellipse();
                    break;
                case InkShapeType.Rectangle:
                    _shape = new Rectangle();
                    break;
                default:
                    _shape = new Ellipse();
                    break;
            }

            _shape.Stroke = new SolidColorBrush(ViewModel.StrokeColor);
            _shape.Fill = new SolidColorBrush(ViewModel.FillColor);
            _shape.StrokeThickness = ViewModel.StrokeSize;
            LayoutRoot.Children.Add(_shape);
            _renderableItem = _shape;
        }

        #endregion
    }
}
