﻿using Microsoft.Graphics.Canvas;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using TouchCastComposingEngine;
using Windows.Data.Pdf;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.DataLayer.Layouts;
using Pitch.Models;
using Pitch.Services;
using Pitch.Helpers.Logger;
using System.Linq;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.LayoutViews
{
    public sealed partial class DocumentLayerLayoutView : BaseLayoutView
    {
        #region Private Constants
        private const uint PagesPerLoad = 4;
        private const float MinZoomFactor = 1;
        private const float MaxZoomFactor = 10f;
        private static readonly TimeSpan InactivityTime = TimeSpan.FromSeconds(2);
        #endregion

        #region Private Fields
        private StorageFolder _localCacheFolder;
        private PdfPageRenderOptions _pdfPageRenderOptions = new PdfPageRenderOptions();
        private uint _currentPage = 0;
        private AsyncLock _loadNextPagesLock = new AsyncLock(1);
        private object _scrollLock = new object();
        private double _pageHeight;
        private bool _isLoading;
        private readonly DispatcherTimer _inactivityTimer = new DispatcherTimer();
        private double _scrollPosition;
        #endregion

        public new DocumentLayerLayout ViewModel => _viewModel as DocumentLayerLayout;

        public override LayoutViewState LayoutViewState
        {
            get => _layoutViewState;
            set
            {
                if (_layoutViewBorder != null)
                {
                    _layoutViewState = value;
                    HorizontalPanel.Visibility = VerticalPanel.Visibility = (_layoutViewState == LayoutViewState.EditState) ? Visibility.Collapsed : Visibility.Visible;
                    _layoutViewBorder?.UpdateBorderStyle(_layoutViewState, ViewModel.IsStatic);
                }
            }
        }

        #region Life Cycle
        public DocumentLayerLayoutView(DocumentLayerLayout model) : base(model)
        {
            InitializeComponent();

            Loaded += DocumentLayerLayoutView_Loaded;
            Unloaded += DocumentLayerLayoutView_Unloaded;
            if (ViewModel.PdfDoc != null)
            {
                var page = ViewModel.PdfDoc.GetPage(0);
                _pageHeight = page.Size.Height;
            }
            RenderTransform = ViewModel.Transform;
            _inactivityTimer.Tick += InactivityTimer_Tick;
            _stopwatch = new Stopwatch();
        }

#if DEBUG
        ~DocumentLayerLayoutView()
        {
            Debug.WriteLine("************************* DocumentLayerLayoutView Desctructor **************************");
        }
#endif

        private async void DocumentLayerLayoutView_Loaded(object sender, RoutedEventArgs e)
        {
            //UpdateSizeForCurrentAspectRatioIfNeeded();

            if (ScrollView != null)
            {
                ScrollContent.Width = Width;
                ScrollView.MinZoomFactor = MinZoomFactor;
                ScrollView.MaxZoomFactor = MaxZoomFactor;
                ScrollView.ZoomMode = ZoomMode.Enabled;
            }
            _renderableItem = ScrollView;
            _colorIndex = 255;

            if (_localCacheFolder == null)
            {
                _localCacheFolder = await ApplicationData.Current.LocalCacheFolder.CreateFolderAsync(_viewModel.Id.ToString(), CreationCollisionOption.OpenIfExists);
            }

            if (_currentPage == 0)
            {
                await LoadNextPages();
            }

            var mediaMixerService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<IMediaMixerService>();
            if (mediaMixerService.State == TouchcastComposerServiceState.Recording)
            {
                StartRenderView();
            }
            else
            {
                await UpdateThumbnail();
            }
        }

        private void DocumentLayerLayoutView_Unloaded(object sender, RoutedEventArgs e)
        {
            StopRenderView();
            _inactivityTimer.Stop();
            _inactivityTimer.Tick -= InactivityTimer_Tick;
        }

        public override void Subscribe()
        {
            base.Subscribe();
            if (_layoutViewBorder != null)
            {
                LayoutRoot.Children.Add(_layoutViewBorder);
            }

            SizeChanged += DocumentLayerLayoutView_SizeChanged;

            if (ScrollView != null)
            {
                ScrollView.ViewChanged += ScrollViewer_ViewChanged;
            }
            _cancelationTokenSource = new CancellationTokenSource();
        }

        public override void Unsubscribe()
        {
            SizeChanged -= DocumentLayerLayoutView_SizeChanged;
            if (ScrollView != null)
            {
                ScrollView.ViewChanged -= ScrollViewer_ViewChanged;
            }

            LayoutRoot.Children.Remove(_layoutViewBorder);

            base.Unsubscribe();
        }

        public override void Clean()
        {
            Loaded -= DocumentLayerLayoutView_Loaded;
            Unloaded -= DocumentLayerLayoutView_Unloaded;
            base.Clean();
        }

        public override async Task UpdateContextBitmap()
        {
            await RenderView();
        }
        #endregion

        #region Callbacks
        private void DocumentLayerLayoutView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            lock (_scrollLock)
            {
                if (e.PreviousSize.Height != 0)
                {
                    var newOffset = (ScrollView.VerticalOffset / e.PreviousSize.Height) * e.NewSize.Height;
                    if (!Double.IsNaN(newOffset))
                    {
                        ScrollView.ChangeView(null, newOffset, null, true);
                    }
                }
                ScrollContent.Width = Width;
            }
        }

        private async void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (_isLoading) return;
            _inactivityTimer.Stop();
            _isLoading = true;
            if (ScrollView.ExtentHeight - ScrollView.VerticalOffset < _pageHeight * 2)
            {
                await LoadNextPages();
            }

            _inactivityTimer.Interval = InactivityTime;
            _inactivityTimer.Start();
            _isLoading = false;
        }
        private void InactivityTimer_Tick(object sender, object e)
        {
            _inactivityTimer.Stop();
            if (Math.Abs(_scrollPosition - ScrollView.VerticalOffset) > _pageHeight)
            {
                ParentView?.RecordCommand(_id, DataLayer.AppMessageType.PageChanged, _viewModel);
                _scrollPosition = ScrollView.VerticalOffset;
            }
        }

        private void ContentVisibility(bool isShow)
        {
            if (ViewModel.PdfDoc != null)
            {
                LayoutRoot.Visibility = isShow ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        #endregion

        #region Public Methods

        public override void StartRenderView()
        {
            if (_isRenderStarted || _cancelationTokenSource == null) return;

            _isRenderStarted = true;
            if (!_cancelationTokenSource.IsCancellationRequested)
            {
                _stopwatch.Start();
                Task.Run(async () =>
                {
                    while (!_cancelationTokenSource.IsCancellationRequested)
                    {
                        var startTime = _stopwatch.Elapsed;
                        if (!_isScreenshotRuning)
                        {
                            _isScreenshotRuning = true;
                            await Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                            {
                                try
                                {
                                    await RenderView();
                                }
                                catch (Exception ex)
                                {
                                    LogQueue.WriteToFile($"Exception in DocumentLayerLayoutView.StartRenderView : {ex.Message}");
                                    Debug.Write($"Exception is raised when trying to get sanpshot. Exception message: {ex.Message}");
                                }
                                finally
                                {
                                    _isScreenshotRuning = false;
                                }
                            });
                        }
                        var duration = (_stopwatch.Elapsed - startTime).TotalMilliseconds;

                        if (duration < 100)
                        {
                            duration = 100;
                        }
                        else
                        {
                            duration *= 2;
                        }
                        await Task.Delay(TimeSpan.FromMilliseconds(duration));
                    }
                    _cancelationTokenSource.Dispose();
                    _cancelationTokenSource = new CancellationTokenSource();
                    _isRenderStarted = false;
                    _stopwatch.Stop();
                }, _cancelationTokenSource.Token);
            }
        }

        public override void StopRenderView()
        {
            if (_stopwatch.IsRunning)
            {
                _cancelationTokenSource.Cancel();
            }
        }

        public override void SetHotSpot(bool isHotSpot)
        {
            base.SetHotSpot(isHotSpot);
            ScrollView.Visibility = isHotSpot ? Visibility.Collapsed : Visibility.Visible;
        }

        public override void Reconfigure()
        {
            base.Reconfigure();
            UpdateSizeForCurrentAspectRatioIfNeeded();
        }

        #endregion

        #region Private Methods
        private async Task LoadNextPages()
        {
            await _loadNextPagesLock.WaitAsync();
            try
            {
                var delta = _currentPage + PagesPerLoad > ViewModel.PdfDoc.PageCount ? ViewModel.PdfDoc.PageCount - _currentPage : PagesPerLoad;
                for (uint i = _currentPage; i < ViewModel.PdfDoc.PageCount && i < _currentPage + delta; i++)
                {
                    string fileName = $"{i}.jpg";
                    var file = await _localCacheFolder.TryGetItemAsync(fileName);
                    if (file == null)
                    {
                        file = await _localCacheFolder.CreateFileAsync(fileName);
                        using (var page = ViewModel.PdfDoc.GetPage(i))
                        {
                            _pdfPageRenderOptions.DestinationWidth = (uint)page.Size.Width;
                            _pdfPageRenderOptions.DestinationHeight = (uint)page.Size.Height;

                            using (IRandomAccessStream randomStream = await (file as StorageFile).OpenAsync(FileAccessMode.ReadWrite))
                            {
                                await page.RenderToStreamAsync(randomStream, _pdfPageRenderOptions);
                                await randomStream.FlushAsync();
                            }
                        }
                    }
                    if (file is StorageFile)
                    {
                        ScrollContent.Children.Add(new Image()
                        {
                            Stretch = Stretch.UniformToFill,
                            Source = new BitmapImage(new Uri(file.Path))
                        });
                    }
                }
                _currentPage += delta;
            }
            finally
            {
                _loadNextPagesLock.Release();
            }
        }
        private void UpdateSizeForCurrentAspectRatioIfNeeded()
        {
            //if (!ViewModel.KeepAspectRatio) return;

            var page = ViewModel.PdfDoc.GetPage(0);
            _pageHeight = page.Size.Height;
            _aspectRatio = page.Size.Width / page.Size.Height;


            var newWidth = (_aspectRatio <= 1) ? ViewModel.Position.Height * _aspectRatio : ViewModel.Position.Width;
            var newHeight = (_aspectRatio <= 1) ? ViewModel.Position.Height : ViewModel.Position.Width / _aspectRatio;

            if (newHeight > ViewModel.Position.Height)
            {
                newWidth *= ViewModel.Position.Height / newHeight;
                newHeight = ViewModel.Position.Height;
            }
            if (newWidth > ViewModel.Position.Width)
            {
                newHeight *= ViewModel.Position.Width / newWidth;
                newWidth = ViewModel.Position.Width;
            }

            Width = newWidth;
            Height = newHeight;


            //if (Width == LayerLayoutTemplate.DefaultWidth && Height == LayerLayoutTemplate.DefaultHeight)
            //{
            //    Width = LayerLayoutTemplate.DefaultWidth;
            //    Height = LayerLayoutTemplate.DefaultWidth / _aspectRatio;

            //    ViewModel.UpdatePosition(Width, Height);
            //}
            ScrollContent.Width = Width;
        }
        #endregion
    }
}
