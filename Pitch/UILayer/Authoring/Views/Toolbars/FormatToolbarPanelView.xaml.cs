﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.Shots;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.UILayer.Authoring.Views.Overlays;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class FormatToolbarPanelView : UserControl
    {
        #region Private Fields
        private object _lockObject = new object();
        private WeakReference<AuthoringSession> _authoringSession;
        private BaseLayoutView _view;
        private bool _isUpdateInPorcess;
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        #endregion

        #region Life Cycle
        public FormatToolbarPanelView()
        {
            InitializeComponent();
            _authoringSession = new WeakReference<AuthoringSession>(GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>());
            Loaded += FormatToolbarPanelView_Loaded;
            Unloaded += FormatToolbarPanelView_Unloaded;
        }

#if DEBUG
        ~FormatToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("**************** PropertyToolbarPanelView Destructor****************");
        }
#endif
        private void FormatToolbarPanelView_Loaded(object sender, RoutedEventArgs e)
        {
            Shortcut.Manager.Instance.FormatShortcutsManager.StepObjectBackShortcutEvent += DrawShortcutsManager_StepObjectBackShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.StepObjectForwardShortcutEvent += DrawShortcutsManager_StepObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.SendObjectBackShortcutEvent += FormatShortcutsManager_SendObjectBackShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.SendObjectForwardShortcutEvent += FormatShortcutsManager_SendObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.DeleteShortcutEvent += DrawShortcutsManager_DeleteShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.BackspaceShortcutEvent += FormatShortcutsManager_BackspaceShortcutEvent;
        }
        private void FormatToolbarPanelView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unsubscribe();
        }
        #endregion

        #region Public Methods

        public void Unsubscribe()
        {
            Loaded -= FormatToolbarPanelView_Loaded;
            Unloaded -= FormatToolbarPanelView_Unloaded;
            Shortcut.Manager.Instance.FormatShortcutsManager.StepObjectBackShortcutEvent -= DrawShortcutsManager_StepObjectBackShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.StepObjectForwardShortcutEvent -= DrawShortcutsManager_StepObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.SendObjectBackShortcutEvent -= FormatShortcutsManager_SendObjectBackShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.SendObjectForwardShortcutEvent -= FormatShortcutsManager_SendObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.DeleteShortcutEvent -= DrawShortcutsManager_DeleteShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.BackspaceShortcutEvent -= FormatShortcutsManager_BackspaceShortcutEvent;
            shapeToolbarView.Unsubscribe();
            textToolbarView.Unsubscribe();
            if (_view != null && _view.ViewModel != null)
            {
                _view.ViewModel.LayerLayoutWasChangedEvent -= ViewModel_LayoutViewPreviewChangedEvent;
            }
        }

        public void Update(BaseLayoutView view)
        {
            lock (_lockObject)
            {
                Clear();

                _isUpdateInPorcess = true;

                SetSelectedView(view);

                LiveOnPlaybackBox.IsEnabled = !(view is LayerLayoutPlaceHolderView);
                LiveOnPlaybackBox.IsChecked = view.ViewModel.IsLiveOnPlayback;

                PauseOnPlaybackBox.IsEnabled = !(view is LayerLayoutPlaceHolderView);
                PauseOnPlaybackBox.IsChecked = view.ViewModel.IsPauseOnPlayback;

                HotSpotBox.IsEnabled = !(view is LayerLayoutPlaceHolderView);
                HotSpotBox.IsChecked = view.ViewModel.IsHotSpot;

                PlayAutomaticallyBox.IsEnabled = view is VideoLayerLayoutView;
                PlayAutomaticallyBox.IsChecked = (view.ViewModel is VideoLayerLayout && (view.ViewModel as VideoLayerLayout).IsPlayAutomatically);

                ChangeButton.IsEnabled = !(view is LayerLayoutPlaceHolderView || view is MediaMixerLayerLayoutView || view is LineLayerLayoutView || _view.ViewModel.IsBackground);

                UrlButton.IsEnabled = !(view is MediaMixerLayerLayoutView || view is LayerLayoutPlaceHolderView);

                textToolbarView.Visibility = Visibility.Collapsed;
                shapeToolbarView.Visibility = Visibility.Collapsed;
                textToolbarView.UnsubscribeFromShortcutManager();

                Canvas.SetZIndex(view, view.ViewModel.ZIndex);

                if (view is ShapeLayerLayoutView)
                {
                    shapeToolbarView.Visibility = Visibility.Visible;
                    shapeToolbarView.ShapeModel = (view as ShapeLayerLayoutView).ViewModel;
                    shapeToolbarView.SetNoColorButtonsVisivility(true);
                }
                else if (view is LineLayerLayoutView)
                {
                    shapeToolbarView.Visibility = Visibility.Visible;
                    shapeToolbarView.ShapeModel = (view as LineLayerLayoutView).ViewModel;
                    shapeToolbarView.SetNoColorButtonsVisivility(false);
                }
                else if (view is TextLayerLayoutView)
                {
                    textToolbarView.Visibility = Visibility.Visible;
                    textToolbarView.CurrentTextLayerLayoutView = view as TextLayerLayoutView;
                    textToolbarView.SubscribeToShortcutManager();
                }

                PositionControl.Visibility = !(view is LineLayerLayoutView) ? Visibility.Visible : Visibility.Collapsed;
                PositionControl.CurrentView = _view;
               
                UpdateUIIsStatic();
                _isUpdateInPorcess = false;
            }
        }
        public void Clear()
        {
            if(textToolbarView != null)
                textToolbarView.CurrentTextLayerLayoutView = null;
            if(shapeToolbarView != null)
            shapeToolbarView.ShapeModel = null;
        }
        #endregion

        #region Private Methods

        private void UpdatePlayAutomatically(bool isPlayAutomatically)
        {
            if (_view.ViewModel is VideoLayerLayout videoLayerLayout)
            {
                videoLayerLayout.Shot.InteractivityParametersChanged(CreateMememtoModelForCurrentView());
                videoLayerLayout.IsPlayAutomatically = isPlayAutomatically;
            }
        }

        private void UpdateHotSpot(bool isHotSpot)
        {
            _view.ViewModel.Shot.InteractivityParametersChanged(CreateMememtoModelForCurrentView());
            _view.SetHotSpot(isHotSpot);
        }

        private void UpdateIsPauseOnPlayback(bool isPauseOnPlayback)
        {
            _view.ViewModel.Shot.InteractivityParametersChanged(CreateMememtoModelForCurrentView());
            _view.ViewModel.IsPauseOnPlayback = isPauseOnPlayback;
        }

        private void UpdateLiveOnPlayback(bool isLiveOnPlayback)
        {
            _view.ViewModel.Shot.InteractivityParametersChanged(CreateMememtoModelForCurrentView());
            _view.ViewModel.IsLiveOnPlayback = isLiveOnPlayback;
        }

        private void UpdateInteracriveUrl(UrlWithProperties url)
        {
            _view.ViewModel.Shot.InteractivityParametersChanged(CreateMememtoModelForCurrentView());
            _view.ViewModel.UpdateUrlWithProperties(url);
        }

        private void UpdateIsStatic(bool isStatic)
        {
            _view.ViewModel.Shot.InteractivityParametersChanged(CreateMememtoModelForCurrentView());
            _view.ViewModel.IsStatic = isStatic;
            UpdateUIIsStatic();
        }
        private void UpdateUIIsStatic()
        {
            _view.LayoutViewState = LayoutViewState.SelectedState;
            LockButton.UpdateContent(_view.ViewModel.IsStatic);
            PositionControl.IsEnabled = !_view.ViewModel.IsStatic;
            UpdateZIndexButtons();
        }

        private void UpdateStateForInteractiveUrl(bool state)
        {
            _view.ViewModel.IsLiveOnPlayback = state;
            _view.ViewModel.IsPauseOnPlayback = state;
            LiveOnPlaybackBox.IsChecked = state;
            PauseOnPlaybackBox.IsChecked = state;
        }

        private void IncreaseZIndex()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_view, ZIndexPosition.Next);
            }
            UpdateZIndexButtons();
        }

        private void DecreaseZIndex()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_view, ZIndexPosition.Previous);
            }
            UpdateZIndexButtons();
        }

        private void BringToFront()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_view, ZIndexPosition.Top);
            }
            UpdateZIndexButtons();
        }

        private void SendToBack()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_view, ZIndexPosition.Bottom);
            }
            UpdateZIndexButtons();
        }

        private void UpdateZIndexButtons()
        {
            ForwardButton.IsEnabled = false;
            BackwardButton.IsEnabled = false;

            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                var zIndexes = authoringSession.Project.ActiveShot.GetZIndexRange();
                if (zIndexes != null)
                {
                    ForwardButton.IsEnabled = !_view.ViewModel.IsStatic && _view.ViewModel.ZIndex < zIndexes.MaxIndex;
                    BackwardButton.IsEnabled = !_view.ViewModel.IsStatic && _view.ViewModel.ZIndex > zIndexes.MinIndex;
                }
            }
        }

        private LayerLayoutMementoModel CreateMememtoModelForCurrentView()
        {
            var mementoModel = _view.ViewModel.GenerateMementoModel();
            mementoModel.PreviousPosition = _view.ViewModel.Position;
            mementoModel.PreviousTransform = _view.ViewModel.Transform;
            return mementoModel;
        }

        private void SetSelectedView(BaseLayoutView view)
        {
            if (_view != null && _view.ViewModel != null)
            {
                _view.ViewModel.LayerLayoutWasChangedEvent -= ViewModel_LayoutViewPreviewChangedEvent;
            }
            _view = view;
            if (_view.ViewModel != null)
            {
                _view.ViewModel.LayerLayoutWasChangedEvent += ViewModel_LayoutViewPreviewChangedEvent;
            }
        }
        
        #endregion

        #region Callbacks

        private Task<bool> DrawShortcutsManager_StepObjectForwardShortcutEvent()
        {
            IncreaseZIndex();
            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_StepObjectBackShortcutEvent()
        {
            DecreaseZIndex();
            return Task.FromResult(true);
        }

        private void ForwardButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            IncreaseZIndex();
        }

        private void BackwardButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            DecreaseZIndex();
        }

        private Task<bool> FormatShortcutsManager_SendObjectForwardShortcutEvent()
        {
            BringToFront();
            return Task.FromResult(true);
        }

        private Task<bool> FormatShortcutsManager_SendObjectBackShortcutEvent()
        {
            SendToBack();
            return Task.FromResult(true);
        }

        private async void RemoveButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                await authoringSession.Project.ActiveShot.RemoveLayer(_view.ViewModel);
            }
        }

        private void ReconfigureButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            _view.Reconfigure();
            Update(_view);
        }

        private void HotSpotBox_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdateHotSpot(true);
        }

        private void HotSpotBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdateHotSpot(false);
        }

        private void PauseOnPlaybackBox_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdateIsPauseOnPlayback(true);
        }

        private void PauseOnPlaybackBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdateIsPauseOnPlayback(false);
        }

        private void LockButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateIsStatic(!_view.ViewModel.IsStatic);
        }

        private async void LiveOnPlaybackBox_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            if (LiveOnPlaybackBox.IsChecked.HasValue 
                && LiveOnPlaybackBox.IsChecked.Value == true
                && (_view.ViewModel is TextLayerLayout || _view.ViewModel is ShapeLayerLayout || _view.ViewModel is MediaMixerLayerLayout)
                && String.IsNullOrEmpty(_view.ViewModel.InteractivityUrl?.UrlString))
            {
                InsertWebViewContentDialog dlg = new InsertWebViewContentDialog(_view.ViewModel.InteractivityUrl?.UrlString)
                {
                    IsSetMode = true,
                };
                await dlg.ShowAsync();
                _isUpdateInPorcess = true;
                if (dlg.Result == InsertWebViewContentDialogResult.WebViewCreated)
                {
                    UrlWithProperties urlWithProps = null;
                    try
                    {
                        urlWithProps = await dlg.ProcessUrlWithProperties();
                    }
                    catch (Exception ex)
                    {
                        urlWithProps = null;
                    }
                    if (urlWithProps?.Properties != null
                        && urlWithProps?.UrlString != null)
                    {
                        UpdateInteracriveUrl(urlWithProps);
                        UpdateStateForInteractiveUrl(true);
                    }
                }
                dlg.Destroy();

                if (dlg.Result == InsertWebViewContentDialogResult.CreationCanceled || _view.ViewModel.InteractivityUrl?.UrlString == null)
                {
                    UpdateStateForInteractiveUrl(false);
                }
                else
                {
                    _view.ViewModel.IsPauseOnPlayback = true;
                    PauseOnPlaybackBox.IsChecked = true;
                }
                _isUpdateInPorcess = false;
            }
            else
            {
                UpdateLiveOnPlayback(true);
            }
        }

        private void LiveOnPlaybackBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdateLiveOnPlayback(false);
        }

        private async Task<bool> DrawShortcutsManager_DeleteShortcutEvent()
        {
            return await RemoveSelectedView();
        }

        private async Task<bool> FormatShortcutsManager_BackspaceShortcutEvent()
        {
            return await RemoveSelectedView();
        }

        private async Task<bool> RemoveSelectedView()
        {
            var result = false;
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (_view?.ViewModel != null)
                {
                    if (_view.LayoutViewState != LayoutViewState.EditState)
                    {
                        await authoringSession.Project.ActiveShot.RemoveLayer(_view?.ViewModel);
                        result = true;
                    }
                }
                else
                {
                    if (authoringSession.Project.Shots.Count > 1)
                    {
                        await authoringSession.Project.RemoveShot(authoringSession.Project.ActiveShot);
                        result = true;
                    }
                }
            }
            return result;
        }

        private async void UrlButton_Click(object sender, RoutedEventArgs e)
        {
            InsertWebViewContentDialog dlg = new InsertWebViewContentDialog(_view.ViewModel.InteractivityUrl?.UrlString)
            {
                IsSetMode = true,
            };
            await dlg.ShowAsync();
            if (dlg.Result == InsertWebViewContentDialogResult.WebViewCreated)
            {
                _isUpdateInPorcess = true;
                UrlWithProperties urlWithProps = null;
                try
                {
                    urlWithProps = await dlg.ProcessUrlWithProperties();
                }
                catch(Exception ex)
                {
                    urlWithProps = null;
                }
                if (urlWithProps?.Properties != null
                    && urlWithProps.UrlString != null)
                {
                    UpdateInteracriveUrl(urlWithProps);
                    UpdateStateForInteractiveUrl(true);
                    _isUpdateInPorcess = false;
                }
            }
            dlg.Destroy();
        }

        private async void ChangeButton_Click(object sender, RoutedEventArgs e)
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                await authoringSession.Project.ActiveShot.ReplaceLayerByPlaceholder(_view.ViewModel);
            }
        }

        private void PlayAutomaticallyBox_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdatePlayAutomatically(true);
        }

        private void PlayAutomaticallyBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            UpdatePlayAutomatically(false);
        }

        private void ViewModel_LayoutViewPreviewChangedEvent(object model)
        {
            lock (_lockObject)
            {
                _isUpdateInPorcess = true;

                LayerLayoutMementoModel mementoModel = model as LayerLayoutMementoModel;
                HotSpotBox.IsChecked = mementoModel.IsHotSpot;
                PauseOnPlaybackBox.IsChecked = mementoModel.IsPauseOnPlayback;
                LiveOnPlaybackBox.IsChecked = mementoModel.IsLiveOnPlayback;
                UpdateZIndexButtons();
                LockButton.UpdateContent(mementoModel.IsStatic);
                if (model is VideoLayerLayoutMementoModel videoModel)
                {
                    PlayAutomaticallyBox.IsChecked = videoModel.IsPlayAutomatically;
                }
                if (mementoModel.Id == _view.ViewModel.Id)
                {
                    _view.LayoutViewState = LayoutViewState.SelectedState;
                }
                _isUpdateInPorcess = false;
            }
        }

        private void ForwardList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem = (ForwardList.SelectedItem as ListViewItem)?.Tag as String;
            if (selectedItem == "Forward")
            {
                IncreaseZIndex();
            }
            else if (selectedItem == "Front")
            {
                BringToFront();
            }
            ForwardFlyout.Hide();
            ForwardList.SelectedItem = null;
        }

        private void BackwardList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem = (BackwardList.SelectedItem as ListViewItem)?.Tag as String;
            if (selectedItem == "Backward")
            {
                DecreaseZIndex();
            }
            else if (selectedItem == "Back")
            {
                SendToBack();
            }
            BackwardFlyout.Hide();
            BackwardList.SelectedItem = null;
        }

        #endregion
    }
}
