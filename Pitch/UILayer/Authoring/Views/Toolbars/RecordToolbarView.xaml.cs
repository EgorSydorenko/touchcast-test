﻿using Windows.UI.Xaml.Controls;
using Pitch.UILayer.Authoring.ViewModels.Toolbars;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class RecordToolbarView : UserControl
    {
        private RecordToolbarViewModel ViewModel;
        public RecordToolbarView()
        {
            InitializeComponent();
            ViewModel = new RecordToolbarViewModel();
            Loaded += RecordToolbarView_Loaded;
        }
#if DEBUG
        ~RecordToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("************** RecordToolbarView Destructor **************");
        }
#endif
        private void RecordToolbarView_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Unloaded += RecordToolbarView_Unloaded;
        }

        private void RecordToolbarView_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Unloaded -= RecordToolbarView_Unloaded;
            //Bindings.StopTracking();
            ViewModel?.Cleanup();
        }
    }
}
