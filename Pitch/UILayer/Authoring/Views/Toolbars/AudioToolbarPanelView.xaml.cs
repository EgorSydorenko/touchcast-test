﻿using GalaSoft.MvvmLight.Ioc;
using System.Threading.Tasks;
using Pitch.Services;
using Pitch.UILayer.Authoring.ViewModels.Toolbars;
using Pitch.Helpers.Logger;
using Windows.Devices.Enumeration;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class AudioToolbarPanelView : UserControl
    {
        #region Private Fields

        private AudioToolbarPanelViewModel _viewModel;
        private bool _settingSaveInProcess;

        #endregion

        #region Public Properties

        public AudioToolbarPanelViewModel ViewModel => _viewModel;

        #endregion

        #region Life Cycle

        public AudioToolbarPanelView()
        {
            _viewModel = new AudioToolbarPanelViewModel();
            InitializeComponent();
            Loaded += AudioToolbarPanelView_Loaded;
        }

#if DEBUG
        ~AudioToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("****************AudioToolbarPanelView Destructor****************");
        }
#endif
        private void AudioToolbarPanelView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += AudioToolbarPanelView_Unloaded;
            ViewModel.UpdateVolumeLevel();
        }

        private void AudioToolbarPanelView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unsubscribe();
        }

        private void Unsubscribe()
        {
            Loaded -= AudioToolbarPanelView_Loaded;
            Unloaded -= AudioToolbarPanelView_Unloaded;

            ViewModel?.Cleanup();
            _viewModel = null;
        }

        #endregion

        #region Private Methods

        private void FillMicrophonesList()
        {
            MicrophonesList.SelectionChanged -= Microphones_SelectionChanged;
            MicrophonesList.Items.Clear();
            var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            foreach (var microphone in mixer.AvailableMicrophones)
            {
                var item = new ListViewItem()
                {
                    Content = microphone.Name,
                    Tag = microphone,
                    IsSelected = (microphone.Id == mixer.SelectedMicrophone?.Id),
                };
                MicrophonesList.Items.Add(item);
            }
            MicrophonesList.SelectionChanged += Microphones_SelectionChanged;
        }

        private async Task SaveMicrophone(DeviceInformation microphone)
        {
            if (_settingSaveInProcess) return;
            _settingSaveInProcess = true;

            var mixrer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            
            bool composerRestartRequired = false;
            if (microphone != null && microphone != mixrer.SelectedMicrophone)
            {
                LogQueue.WriteToFile($"SaveMicrophone {microphone.Name}");
                mixrer.SelectedMicrophone = microphone;
                composerRestartRequired = true;
            }

            if (composerRestartRequired)
            {
                await mixrer.Reset();
                if (mixrer.ComposerMode == TouchcastComposerServiceMode.CameraComposingMode)
                {
                    mixrer.InitWithCamera();
                }
                await mixrer.StartPreviewingAsync();
                var session = SimpleIoc.Default.GetInstance<DataLayer.AuthoringSession>();
                if (session != null)
                {
                    foreach (var shot in session.Project.Shots)
                    {
                        await shot.PrepareForRecording();
                    }
                }
            }

            _settingSaveInProcess = false;
        }

        #endregion

        #region Call Backs

        private async void Microphones_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MicrophonesList.SelectedItem is ListViewItem item)
            {
                if (item.Tag is DeviceInformation microphone)
                {
                    MicrophoneSelectFlyout.Hide();
                    await SaveMicrophone(microphone);
                }
            }
        }

        private void Microphones_ItemClick(object sender, ItemClickEventArgs e)
        {
            MicrophoneSelectFlyout.Hide();
        }

        private void Microphones_Loaded(object sender, RoutedEventArgs e)
        {
            FillMicrophonesList();
        }

        #endregion
    }
}
