﻿<UserControl
    x:Class="Pitch.UILayer.Authoring.Views.Toolbars.CameraToolbarPanelView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:controls="using:Pitch.UILayer.Helpers.Controls"
    mc:Ignorable="d"
    d:DesignHeight="300"
    d:DesignWidth="400">

    <Grid HorizontalAlignment="Stretch" Margin="10,0,10,0">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="Auto"/>
        </Grid.ColumnDefinitions>
        <StackPanel Grid.Column="0" Orientation="Vertical">
            <Button Style="{StaticResource MenuFlatButtonStyle}"
                    Width="70" Height="70" Margin="0,10,0,0">
                <Button.Flyout>
                    <Flyout x:Name="CameraSelectFlyout" Placement="Bottom"
                            FlyoutPresenterStyle="{StaticResource ToolBarSelectFlyoutStyle}">
                        <ListView x:Name="CamerasList" ItemContainerStyle="{StaticResource ToolBarSelectListViewItemStyle}"
                                  CanDrag="False" CanDragItems="False" SelectionMode="Single" IsItemClickEnabled="True"
                                  Loaded="Cameras_Loaded" ItemClick="Cameras_ItemClick">
                            <ListView.ItemTemplate>
                                <DataTemplate>
                                    <TextBlock TextTrimming="CharacterEllipsis"/>
                                </DataTemplate>
                            </ListView.ItemTemplate>
                        </ListView>
                    </Flyout>
                </Button.Flyout>
                <StackPanel Orientation="Vertical">
                    <Image Source="/Assets/Pivot/Camera/select-cam.png" Height="28"/>
                    <TextBlock x:Uid="ToolbarCameraSelectCam" TextWrapping="WrapWholeWords" Width="70"
                               Margin="0,6,0,6" TextAlignment="Center"/>
                    <Image Source="/Assets/Icons/menu-arrow.png" Height="8"/>
                </StackPanel>
            </Button>
            <TextBlock x:Uid="ToolbarCameraLabel" Style="{StaticResource ToolbarLabelStyle}" 
                       Margin="0,2,0,0"/>
        </StackPanel>

        <Image Grid.Column="1" Style="{StaticResource PivotVerticalDividerStyle}"/>
        
        <StackPanel Grid.Column="2" Orientation="Vertical">
            <ToggleSwitch Name="TurnChromaKeyButton" Foreground="White" Margin="10,10,0,0" 
                          Style="{StaticResource TouchcastToggleSwitch}"
                          IsOn="{x:Bind ViewModel.IsGreenScreenOn, Mode=TwoWay}"
                          Height="36" MinWidth="50" MinHeight="30"
                          HorizontalAlignment="Center">
                <ToggleSwitch.OffContent>
                    <TextBlock Text="Off" VerticalAlignment="Top" Margin="-7 4 0 0"
                               FontSize="12" Foreground="#FF8B8B8B" FontWeight="SemiBold" 
                               TextAlignment="Center" TextWrapping="WrapWholeWords" TextLineBounds="TrimToCapHeight"/>
                </ToggleSwitch.OffContent>
                <ToggleSwitch.OnContent>
                    <TextBlock Text="On" VerticalAlignment="Top" Margin="-7 4 0 0"
                               FontSize="12" Foreground="#FF8B8B8B" FontWeight="SemiBold"
                               TextAlignment="Center" TextWrapping="WrapWholeWords" TextLineBounds="TrimToCapHeight"/>
                </ToggleSwitch.OnContent>
                <ToolTipService.ToolTip>
                    <TextBlock x:Uid="ToolbarSwitchGreenScreenToolTip"/>
                </ToolTipService.ToolTip>
            </ToggleSwitch>
            <TextBlock x:Uid="ToolbarSwitchGreenScreen" 
                       Foreground="White" FontSize="12" FontWeight="SemiBold"
                       TextAlignment="Center" TextWrapping="WrapWholeWords" TextLineBounds="TrimToCapHeight"
                       Margin="0,4,0,0"/>
        </StackPanel>

        <Image Grid.Column="3" Style="{StaticResource PivotVerticalDividerStyle}"/>
        
        <StackPanel Grid.Column="4" Margin="10,0,0,0" Orientation="Vertical">
            <StackPanel Orientation="Horizontal">
                <StackPanel VerticalAlignment="Top" Margin="20,12,20,0">
                    <TextBlock x:Name="SensitivitySliderLabel" x:Uid="ToolbarSwitchGreenScreenSensitivity" 
                               FontSize="12" Margin="0"
                               HorizontalAlignment="Left" Foreground="{x:Bind ViewModel.LabelColor, Mode=OneWay}"/>
                    <Slider x:Name="SensitivitySlider" Style="{StaticResource TouchCastSliderStyle}"
                            Margin="0" Width="120" Height="10"
                            IsEnabled="{x:Bind ViewModel.IsGreenScreenOn, Mode=TwoWay}" Orientation="Horizontal"
                            Minimum="0" Maximum="500" StepFrequency="1" IsThumbToolTipEnabled="False"
                            Value="{x:Bind ViewModel.Sensitivity, Mode=TwoWay}"/>

                    <TextBlock x:Name="SmoothSliderLabel" x:Uid="ToolbarSwitchGreenScreenSmoothness" FontSize="12" 
                               Margin="0,2,0,0" 
                               HorizontalAlignment="Left" Foreground="{x:Bind ViewModel.LabelColor, Mode=OneWay}"/>
                    <Slider x:Name="SmoothSlider" Style="{StaticResource TouchCastSliderStyle}"
                            Margin="0" Width="120" Height="10"
                            IsEnabled="{x:Bind ViewModel.IsGreenScreenOn, Mode=TwoWay}" Orientation="Horizontal" 
                            Minimum="0" Maximum="500" StepFrequency="1" IsThumbToolTipEnabled="False"
                            Value="{x:Bind ViewModel.Smoothness, Mode=TwoWay}"/>
                </StackPanel>
                <controls:ExtendedToggleButton x:Name="ColorPickerToggleButton" 
                                               Style="{StaticResource ExtendedToggleButtonStyle}"
                                               Width="70" Height="70" Margin="0,8,0,20"
                                               UserMargin="20,-48,0,0"
                                               IsEnabled="{Binding ElementName=TurnChromaKeyButton, Path=IsOn}"
                                               LostFocus="ColorPickerToggleButton_LostFocus"
                                               Checked="ColorPickerToggleButton_Checked"
                                               CommandParameter="{Binding Path=IsChecked, RelativeSource={RelativeSource Self}}">
                    <StackPanel Orientation="Vertical">
                        <Image Source="/Assets/Pivot/GreenScreen/Color_Picker.png" Height="28"/>
                        <TextBlock x:Uid="ToolbarSwitchGreenScreenColorPicker" Margin="0,5,0,0" TextWrapping="WrapWholeWords" Width="50" 
                                   TextLineBounds="TrimToCapHeight" TextAlignment="Center"/>
                    </StackPanel>
                </controls:ExtendedToggleButton>
            </StackPanel>
            <TextBlock x:Uid="ToolbarSwitchGreenScreenLabel" Style="{StaticResource ToolbarLabelStyle}"
                       Margin="0,-16,0,4"/>
        </StackPanel>
    </Grid>
</UserControl>
