﻿using GalaSoft.MvvmLight.Messaging;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Pitch.DataLayer;
using Pitch.UILayer.Helpers.Messages;
using System.Threading.Tasks;
using Pitch.DataLayer.Helpers.Extensions;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{

    public sealed partial class StatusToolbarView : UserControl
    {
        private const string SceneTextFromaterresourceKey = "SceneStringFormater";
        private WeakReference<AuthoringSession> _authoringSession;
        private string _sceneLabeltextFromater;
        AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();


        public StatusToolbarView()
        {
            InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            _sceneLabeltextFromater = loader.GetString(SceneTextFromaterresourceKey);
            
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(session);
            Loaded += StatusToolbarView_Loaded;
        }

        private void StatusToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += StatusToolbarView_Unloaded;
            var session = AuthoringSession;
            if(session != null)
            {
                session.AuthroingSessionWasInitialized += Session_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned += Session_AuthoringSessionWillBeCleaned;
            }
        }

        private Task Session_AuthoringSessionWillBeCleaned()
        {
            Unsubscribe();

            return Task.CompletedTask;
        }

        private Task Session_AuthroingSessionWasInitialized()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                session.Project.ShotWasRemoved += Project_ShotWasRemoved;
                session.Project.ShotWasCreated += Project_ShotWasCreated;
                session.Project.ShotWasMoved += Project_ShotWasMoved;
            }
            UpdateSceneCounterText();

            return Task.CompletedTask;
        }

        private void StatusToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= StatusToolbarView_Loaded;
            Unloaded -= StatusToolbarView_Unloaded;
            Unsubscribe();
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized -= Session_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned -= Session_AuthoringSessionWillBeCleaned;
            }
        }

        private void Unsubscribe()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project != null)
                {
                    session.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                    session.Project.ShotWasRemoved -= Project_ShotWasRemoved;
                    session.Project.ShotWasCreated -= Project_ShotWasCreated;
                    session.Project.ShotWasMoved -= Project_ShotWasMoved;
                }
            }
        }

        private Task Project_ShotWasCreated(DataLayer.Shots.Shot shot)
        {
            UpdateSceneCounterText();

            return Task.CompletedTask;
        }

        private void Project_ActiveShotWasChanged()
        {
            UpdateSceneCounterText();
        }
        
        private Task Project_ShotWasRemoved(DataLayer.Shots.Shot shot)
        {
            UpdateSceneCounterText();

            return Task.CompletedTask;
        }
        private Task Project_ShotWasMoved(DataLayer.Shots.Shot shot)
        {
            UpdateSceneCounterText();

            return Task.CompletedTask;
        }
        public void UpdateSceneCounterText()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                var index = session.Project.Shots.IndexOf(session.Project.ActiveShot) + 1;
                
               SceneCounterTextBlock.Text = String.Format(_sceneLabeltextFromater, index, session.Project.Shots.Count);
            }
        }

        private void TeleprompterEditorButton_Clicked(object sender, RoutedEventArgs e)
		{
			Messenger.Default.Send(new TeleprompterEditorPopupShowMessage());
		}
	}
}
