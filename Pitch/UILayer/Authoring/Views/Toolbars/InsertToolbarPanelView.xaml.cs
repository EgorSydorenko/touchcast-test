﻿using System;
using System.Collections.Generic;
using Pitch.Models;
using Pitch.UILayer.Authoring.ViewModels.Toolbars;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public class ShapeItem
    {
        public InkShapeType ShapeType { get; private set; }
        public Uri Url { get; private set; }
        public ShapeItem(InkShapeType shapeType, Uri url)
        {
            ShapeType = shapeType;
            Url = url;
        }
    }

    public sealed partial class InsertToolbarPanelView : UserControl
    {
        #region Fields

        public InsertToolbarPanelViewModel _viewModel;

        #endregion

        #region Properties

        public InsertToolbarPanelViewModel ViewModel => _viewModel;
        public List<ShapeItem> Shapes { get; private set; }

        #endregion

        #region Life Cycle

        public InsertToolbarPanelView()
        {
            _viewModel = new InsertToolbarPanelViewModel();
            InitializeComponent();

            Loaded += InsertToolbarPanelView_Loaded;
            ShotsTemplateList.parentFlyout = Flyout;

            Shapes = new List<ShapeItem>();
            foreach (var shapeItem in _viewModel.Shapes)
            {
                Shapes.Add(new ShapeItem(shapeItem, ShapeTypeToUriConverter(shapeItem)));
            }

            ShapeList.ItemsSource = Shapes;
        }

#if DEBUG
        ~InsertToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("************* InsertToolbarPanelView destructor *************");
        }
#endif

        private void InsertToolbarPanelView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += InsertToolbarPanelView_Unloaded;
            UpperImage.Source = new BitmapImage(ShapeTypeToUriConverter(ViewModel.LastShapeType));
        }

        private void InsertToolbarPanelView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unsubscribe();
        }

        #endregion

        #region Public Methods

        public void Unsubscribe()
        {
            ShotsTemplateList.Clean();
            Loaded -= InsertToolbarPanelView_Loaded;
            Unloaded -= InsertToolbarPanelView_Unloaded;
            Bindings.StopTracking();
            ViewModel?.Cleanup();
            _viewModel = null;
        }

        public void UpdateShotsPopup()
        {
            ShotsTemplateList.Update();
        }

        public void SetCutCopyButtonsState(bool isEnabled)
        {
            CutButton.IsEnabled = isEnabled;
            CopyButton.IsEnabled = isEnabled;
        }

        #endregion

        #region Callbacks

        private void ColorsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ShapeList.SelectedItem is ShapeItem item)
            {
                UpperImage.Source = new BitmapImage(ShapeTypeToUriConverter(item.ShapeType));
            }
            ShapesFlyout.Hide();
        }

        #endregion

        #region Private Methods

        private Uri ShapeTypeToUriConverter(InkShapeType type)
        {
            Uri result = null;
            switch (type)
            {
                case InkShapeType.Arrow:
                    result = new Uri("ms-appx:///Assets/Pivot/Insert/shape-arrow@3x.png");
                    break;
                case InkShapeType.Line:
                    result = new Uri("ms-appx:///Assets/Pivot/Insert/shape-line@3x.png");
                    break;
                case InkShapeType.Rectangle:
                    result = new Uri("ms-appx:///Assets/Pivot/Insert/shape-rectangle@3x.png");
                    break;
                case InkShapeType.Circle:
                    result = new Uri("ms-appx:///Assets/Pivot/Insert/shape-circle@3x.png");
                    break;
            }
            return result;
        }

        #endregion
    }
}
