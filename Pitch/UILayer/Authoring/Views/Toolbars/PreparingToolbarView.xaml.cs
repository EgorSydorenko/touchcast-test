﻿using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.Shots;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class PreparingToolbarView : UserControl
    {
        public delegate void SwitchToFileMenu(FileMenuKey menuKey);

        #region Events
        public event SwitchToFileMenu SwitchToFileMenuEvent;
        public event EventHandler OpenProjectEvent;
        public event EventHandler SaveProjectEvent;
        #endregion

        #region Constants

        private const int VappMoveStep = 10;

        #endregion

        #region Private Fields
        private WeakReference<AuthoringSession> _authoringSession;
        private PivotHeaderItem _propertyPivotHeaderItem;
        private object _previousPanelItem;
        private BaseLayoutView _selectedView;
        #endregion

        #region Properties
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        #endregion

        #region Life Cycle

        public PreparingToolbarView()
        {
            InitializeComponent();
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(session);
            Loaded += AuthoringToolbarView_Loaded;
        }

#if DEBUG
        ~PreparingToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("**************** PreparingToolbarView Destructor****************");
        }
#endif
        private void AuthoringToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("AuthoringToolbarView_Loaded");

            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized += Session_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned += Session_AuthoringSessionWillBeCleaned;
            }
            Unloaded += AuthoringToolbarView_Unloaded;

            var pivotHeaderPanel = UIHelper.FindChild<PivotHeaderPanel>(Pivot, null);
            _propertyPivotHeaderItem = pivotHeaderPanel?.Children.Cast<PivotHeaderItem>().FirstOrDefault(i => i.Content.Equals(PropertyPivotItem.Header));
            if (_propertyPivotHeaderItem != null)
            {
                _propertyPivotHeaderItem.Visibility = Visibility.Collapsed;
            }

            Pivot.SelectedItem = InsertPivotItem;
            Pivot.SelectionChanged += Pivot_SelectionChanged;
            _previousPanelItem = Pivot.SelectedItem;

            RecordToolbarView _recordToolbarView = new RecordToolbarView();
            _recordToolbarView.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetColumn(_recordToolbarView, 2);
            LayoutRoot.Children.Add(_recordToolbarView);
            Pivot.PivotItemLoaded += Pivot_PivoItemLoaded;
        }

        private void AuthoringToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("AuthoringToolbarView_Unloaded");

            Pivot.PivotItemLoaded -= Pivot_PivoItemLoaded;

            Shortcut.Manager.Instance.VappShortcutsManager.OpenShortcutEvent -= ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.SaveShortcutEvent -= ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.NewShortcutEvent -= ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.ExportShortcutEvent -= VappShortcutsManager_ExportShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.InsertTextShortcutEvent -= VappShortcutsManager_InsertTextShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.NewShotShortcutEvent -= VappShortcutsManager_NewShotShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveRightShortcutEvent -= VappShortcutsManager_MoveRightShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveLeftShortcutEvent -= VappShortcutsManager_MoveLeftShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveUpShortcutEvent -= VappShortcutsManager_MoveUpShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveDownShortcutEvent -= VappShortcutsManager_MoveDownShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveRightShortcutEvent -= VappShortcutsManager_VappMoveRightShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveLeftShortcutEvent -= VappShortcutsManager_VappMoveLeftShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveUpShortcutEvent -= VappShortcutsManager_VappMoveUpShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveDownShortcutEvent -= VappShortcutsManager_VappMoveDownShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.UndoShortcutEvent -= ShortcutsManager_UndoShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.RedoShortcutEvent -= ShortcutsManager_RedoShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.StepObjectBackShortcutEvent -= VappShortcutsManager_StepObjectBackShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.StepObjectForwardShortcutEvent -= VappShortcutsManager_StepObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.SendObjectBackShortcutEvent -= VappShortcutsManager_SendObjectBackShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.SendObjectForwardShortcutEvent -= VappShortcutsManager_SendObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.CopyShortcutEvent -= ShortcutsManager_CopyShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.PasteShortcutEvent -= ShortcutsManager_PasteShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.CutShortcutEvent -= ShortcutsManager_CutShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.DeleteShortcutEvent -= VappShortcutsManager_DeleteShortcatEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.BackspaceShortcutEvent -= VappShortcutsManager_BackspaceShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.DuplicateClipShortcutEvent -= ShortcutsManager_DuplicateClipShortcutEvent;

            Shortcut.Manager.Instance.EditShortcutsManager.ExportShortcutEvent -= VappShortcutsManager_ExportShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.OpenShortcutEvent -= ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.SaveShortcutEvent -= ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NewShortcutEvent -= ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.DuplicateClipShortcutEvent -= ShortcutsManager_DuplicateClipShortcutEvent;

            Shortcut.Manager.Instance.FormatShortcutsManager.InsertTextShortcutEvent -= VappShortcutsManager_InsertTextShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveRightShortcutEvent -= VappShortcutsManager_MoveRightShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveLeftShortcutEvent -= VappShortcutsManager_MoveLeftShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveUpShortcutEvent -= VappShortcutsManager_MoveUpShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveDownShortcutEvent -= VappShortcutsManager_MoveDownShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveRightShortcutEvent -= VappShortcutsManager_VappMoveRightShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveLeftShortcutEvent -= VappShortcutsManager_VappMoveLeftShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveUpShortcutEvent -= VappShortcutsManager_VappMoveUpShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveDownShortcutEvent -= VappShortcutsManager_VappMoveDownShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.NewShotShortcutEvent -= VappShortcutsManager_NewShotShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.UndoShortcutEvent -= ShortcutsManager_UndoShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.RedoShortcutEvent -= ShortcutsManager_RedoShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.NewShortcutEvent -= ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.OpenShortcutEvent -= ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.SaveShortcutEvent -= ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.CopyShortcutEvent -= ShortcutsManager_CopyShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.PasteShortcutEvent -= ShortcutsManager_PasteShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.CutShortcutEvent -= ShortcutsManager_CutShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.DuplicateClipShortcutEvent -= ShortcutsManager_DuplicateClipShortcutEvent;

            Pivot.SelectionChanged -= Pivot_SelectionChanged;

            CameraPanel.Unsubscribe();
            InsertPanel.Unsubscribe();
            PropertyPanel.Unsubscribe();

            Pivot.Items.Clear();
            Loaded -= AuthoringToolbarView_Loaded;
            Unloaded -= AuthoringToolbarView_Unloaded;
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized -= Session_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned -= Session_AuthoringSessionWillBeCleaned;
            }
            Unsubscribe();
        }

        #endregion

        #region Public Methods

        public void SubscribeToShortcutManager()
        {
            Shortcut.Manager.Instance.VappShortcutsManager.OpenShortcutEvent += ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.SaveShortcutEvent += ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.NewShortcutEvent += ShortcutsManager_NewShortcutEvent;

            Shortcut.Manager.Instance.VappShortcutsManager.ExportShortcutEvent += VappShortcutsManager_ExportShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.InsertTextShortcutEvent += VappShortcutsManager_InsertTextShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.NewShotShortcutEvent += VappShortcutsManager_NewShotShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveRightShortcutEvent += VappShortcutsManager_MoveRightShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveLeftShortcutEvent += VappShortcutsManager_MoveLeftShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveUpShortcutEvent += VappShortcutsManager_MoveUpShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.MoveDownShortcutEvent += VappShortcutsManager_MoveDownShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveRightShortcutEvent += VappShortcutsManager_VappMoveRightShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveLeftShortcutEvent += VappShortcutsManager_VappMoveLeftShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveUpShortcutEvent += VappShortcutsManager_VappMoveUpShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.VappMoveDownShortcutEvent += VappShortcutsManager_VappMoveDownShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.UndoShortcutEvent += ShortcutsManager_UndoShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.RedoShortcutEvent += ShortcutsManager_RedoShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.StepObjectBackShortcutEvent += VappShortcutsManager_StepObjectBackShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.StepObjectForwardShortcutEvent += VappShortcutsManager_StepObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.SendObjectBackShortcutEvent += VappShortcutsManager_SendObjectBackShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.SendObjectForwardShortcutEvent += VappShortcutsManager_SendObjectForwardShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.CopyShortcutEvent += ShortcutsManager_CopyShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.PasteShortcutEvent += ShortcutsManager_PasteShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.CutShortcutEvent += ShortcutsManager_CutShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.DeleteShortcutEvent += VappShortcutsManager_DeleteShortcatEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.BackspaceShortcutEvent += VappShortcutsManager_BackspaceShortcutEvent;
            Shortcut.Manager.Instance.VappShortcutsManager.DuplicateClipShortcutEvent += ShortcutsManager_DuplicateClipShortcutEvent;

            Shortcut.Manager.Instance.EditShortcutsManager.ExportShortcutEvent += VappShortcutsManager_ExportShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.OpenShortcutEvent += ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.SaveShortcutEvent += ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NewShortcutEvent += ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.DuplicateClipShortcutEvent += ShortcutsManager_DuplicateClipShortcutEvent;

            Shortcut.Manager.Instance.FormatShortcutsManager.InsertTextShortcutEvent += VappShortcutsManager_InsertTextShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveRightShortcutEvent += VappShortcutsManager_MoveRightShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveLeftShortcutEvent += VappShortcutsManager_MoveLeftShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveUpShortcutEvent += VappShortcutsManager_MoveUpShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.MoveDownShortcutEvent += VappShortcutsManager_MoveDownShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveRightShortcutEvent += VappShortcutsManager_VappMoveRightShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveLeftShortcutEvent += VappShortcutsManager_VappMoveLeftShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveUpShortcutEvent += VappShortcutsManager_VappMoveUpShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.VappMoveDownShortcutEvent += VappShortcutsManager_VappMoveDownShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.NewShotShortcutEvent += VappShortcutsManager_NewShotShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.UndoShortcutEvent += ShortcutsManager_UndoShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.RedoShortcutEvent += ShortcutsManager_RedoShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.NewShortcutEvent += ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.OpenShortcutEvent += ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.SaveShortcutEvent += ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.CopyShortcutEvent += ShortcutsManager_CopyShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.PasteShortcutEvent += ShortcutsManager_PasteShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.CutShortcutEvent += ShortcutsManager_CutShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.DuplicateClipShortcutEvent += ShortcutsManager_DuplicateClipShortcutEvent;
        }

        public void UpdateActiveShortcutManager()
        {
            switch ((Pivot.SelectedItem as PivotItem)?.TabIndex)
            {
                case 5:
                    Shortcut.Manager.Instance.Activate(Shortcut.ShortcutsManagerId.Format);
                    break;
                default:
                    Shortcut.Manager.Instance.Activate(Shortcut.ShortcutsManagerId.Vapp);
                    break;
            }
        }

        public void UpdateShotsPopup()
        {
            InsertPanel.UpdateShotsPopup();
        }

        public void SwitchToPreviousPivotItem()
        {
            Pivot.SelectedItem = _previousPanelItem;
        }

        #endregion

        #region Callbacks

        private Task Session_AuthoringSessionWillBeCleaned()
        {
            Unsubscribe();
            ResetPivot();

            return Task.CompletedTask;
        }

        private Task Session_AuthroingSessionWasInitialized()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                session.Project.ActiveShotWillBeChanged += Project_ActiveShotWillBeChanged;
                session.ShotManager.ActiveShotView.LayoutViewWasCreatedEvent += ActiveShotView_LayoutViewWasCreatedEvent;
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent += ActiveShotView_LayoutViewSelectedEvent;
                session.Project.ActiveShot.LayoutWasRemoved += ActiveShot_LayoutWasRemoved;
                session.Project.ActiveShot.UpdateBackgroundLayerEvent += ActiveShot_UpdateBackgroundLayerEvent;
            }

            return Task.CompletedTask;
        }

        private Task ActiveShot_LayoutWasRemoved(LayerLayout model)
        {
            PrepareInsertPivotItem();

            PrepareFormatPivotItemForHidding();

            if (_selectedView?.ViewModel == model)
            {
                _selectedView = null;
                PrepareDesignPivotItemForShow(_selectedView);
            }

            return Task.CompletedTask;
        }

        private void ActiveShot_UpdateBackgroundLayerEvent(LayerLayout newBackgroundLayer, LayerLayout currentBackgroundLayer)
        {
            if (PropertyPivotItem.Content is FormatToolbarPanelView)
            {
                var pivotItem = PropertyPivotItem.Content as FormatToolbarPanelView;
                var session = AuthoringSession;
                if (session != null)
                {

                    var view = session.ShotManager.ActiveShotView.LayoutViews.FirstOrDefault(v => v.ViewModel.Id == newBackgroundLayer?.Id);
                    if (view != null)
                    {
                        pivotItem.Update(view);
                    }
                }
            }
        }

        private void Project_ActiveShotWillBeChanged()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.ShotManager.ActiveShotView.LayoutViewWasCreatedEvent -= ActiveShotView_LayoutViewWasCreatedEvent;
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent -= ActiveShotView_LayoutViewSelectedEvent;
                session.Project.ActiveShot.LayoutWasRemoved -= ActiveShot_LayoutWasRemoved;
                session.Project.ActiveShot.UpdateBackgroundLayerEvent -= ActiveShot_UpdateBackgroundLayerEvent;
            }
        }

        private void ActiveShotView_LayoutViewWasCreatedEvent(BaseLayoutView view)
        {
            PrepareInsertPivotItem(view);

            _selectedView = view;
            PrepareDesignPivotItemForShow(view);
            if (view != null)
            {
                PrepareFormatPivotItemForShow(view);
                Pivot.SelectedItem = PropertyPivotItem;
            }
        }

        private void ActiveShotView_LayoutViewSelectedEvent(BaseLayoutView view)
        {
            PrepareInsertPivotItem(view);

            _selectedView = view;
            PrepareDesignPivotItemForShow(view);
            if (view != null)
            {
                PrepareFormatPivotItemForShow(view);
            }
            else
            {
                PrepareFormatPivotItemForHidding();
            }
        }

        private void Project_ActiveShotWasChanged()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.ShotManager.ActiveShotView.LayoutViewWasCreatedEvent += ActiveShotView_LayoutViewWasCreatedEvent;
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent += ActiveShotView_LayoutViewSelectedEvent;
                session.Project.ActiveShot.LayoutWasRemoved += ActiveShot_LayoutWasRemoved;
                session.Project.ActiveShot.UpdateBackgroundLayerEvent += ActiveShot_UpdateBackgroundLayerEvent;

                var selectedView = session.ShotManager.ActiveShotView.LayoutViews.FirstOrDefault(v => v.LayoutViewState != LayoutViews.LayoutViewState.NormalState);
                ActiveShotView_LayoutViewSelectedEvent(selectedView);
            }
        }

        private void Pivot_PivoItemLoaded(Pivot sender, PivotItemEventArgs args)
        {
            if (Pivot.SelectedItem != PropertyPivotItem && Pivot.SelectedItem != FilePivotItem)
            {
                _previousPanelItem = Pivot.SelectedItem;
            }
            UpdateActiveShortcutManager();
        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Pivot.SelectedIndex == 0)
            {
                SwitchToFileMenuEvent?.Invoke(FileMenuKey.InfoMenu);
            }
            else if (_selectedView != null)
            {
                _selectedView.UpdateFocusIfNeeded(_selectedView is TextLayerLayoutView);
            }
        }

        #region Shortcut manager's callbacks
        private Task<bool> VappShortcutsManager_ExportShortcutEvent()
        {
            var session = AuthoringSession;
            if (session != null && session.Project != null)
            {
                if (session.Project.TcMediaComposition.MediaClips.Any(i => i.Takes.Any(j => j.VideoFragments.Any())))
                {
                    SwitchToFileMenuEvent?.Invoke(FileMenuKey.FinalizeMenu);
                }
            }
            return Task.FromResult(true);
        }

        private async Task<bool> ShortcutsManager_RedoShortcutEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.UndoRedoOriginator.Redo();
            }
            return true;
        }

        private async Task<bool> ShortcutsManager_UndoShortcutEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.UndoRedoOriginator.Undo();
            }
            return true;
        }

        private Task<bool> ShortcutsManager_OpenShortcutEvent()
        {
            OpenProjectEvent?.Invoke(this, EventArgs.Empty);
            return Task.FromResult(true);
        }

        private Task<bool> ShortcutsManager_SaveShortcutEvent()
        {
            SaveProjectEvent?.Invoke(this, EventArgs.Empty);
            return Task.FromResult(true);
        }

        private Task<bool> ShortcutsManager_NewShortcutEvent()
        {
            SwitchToFileMenuEvent?.Invoke(FileMenuKey.NewMenu);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_InsertTextShortcutEvent()
        {
            InsertPanel.ViewModel.InsertTextToolCommand.Execute(null);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_NewShotShortcutEvent()
        {
            InsertPanel.ViewModel.InsertLastAddedScene.Execute(null);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_MoveRightShortcutEvent()
        {
            _selectedView?.ChangePositionBy(1, 0);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_MoveDownShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, 1);
            return Task.FromResult(true);
        }
        private Task<bool> VappShortcutsManager_MoveUpShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, -1);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_MoveLeftShortcutEvent()
        {
            _selectedView?.ChangePositionBy(-1, 0);
            return Task.FromResult(true);
        }
        private Task<bool> VappShortcutsManager_VappMoveRightShortcutEvent()
        {
            _selectedView?.ChangePositionBy(VappMoveStep, 0);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_VappMoveDownShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, VappMoveStep);
            return Task.FromResult(true);
        }
        private Task<bool> VappShortcutsManager_VappMoveUpShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, -VappMoveStep);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_VappMoveLeftShortcutEvent()
        {
            _selectedView?.ChangePositionBy(-VappMoveStep, 0);
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_StepObjectForwardShortcutEvent()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_selectedView, ZIndexPosition.Next);
            }
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_StepObjectBackShortcutEvent()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_selectedView, ZIndexPosition.Previous);
            }
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_SendObjectBackShortcutEvent()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_selectedView, ZIndexPosition.Bottom);
            }
            return Task.FromResult(true);
        }

        private Task<bool> VappShortcutsManager_SendObjectForwardShortcutEvent()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ActiveShotView.ChangeZIndex(_selectedView, ZIndexPosition.Top);
            }
            return Task.FromResult(true);
        }

        private async Task<bool> VappShortcutsManager_DeleteShortcatEvent()
        {
            return await RemoveSelectedView();
        }

        private async Task<bool> VappShortcutsManager_BackspaceShortcutEvent()
        {
            return await RemoveSelectedView();
        }

        private async Task<bool> ShortcutsManager_PasteShortcutEvent()
        {
            var result = true;
            if (_selectedView?.ViewModel == null || _selectedView.LayoutViewState != LayoutViewState.EditState)
            {
                result = await InsertPanel.ViewModel.PasteLayerLayoutView();
            }
            return result;
        }

        private Task<bool> ShortcutsManager_CutShortcutEvent()
        {
            bool result = InsertPanel.ViewModel.CutLayerLayoutView();
            return Task.FromResult(result);
        }

        private Task<bool> ShortcutsManager_CopyShortcutEvent()
        {
            bool result = InsertPanel.ViewModel.CopyLayerLayoutView();
            return Task.FromResult(result);
        }

        private async Task<bool> ShortcutsManager_DuplicateClipShortcutEvent()
        {
            return await DuplicateSelectedView();
        }

        #endregion

        #endregion

        #region Private Methods

        private void Unsubscribe()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project != null)
                {
                    session.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                    session.Project.ActiveShotWillBeChanged -= Project_ActiveShotWillBeChanged;
                }
                if (session.ShotManager != null)
                {
                    foreach (var view in session.ShotManager.ShotViews)
                    {
                        view.LayoutViewWasCreatedEvent -= ActiveShotView_LayoutViewWasCreatedEvent;
                        view.LayoutViewSelectedEvent -= ActiveShotView_LayoutViewSelectedEvent;
                    }
                    foreach (var shot in session.Project.Shots)
                    {
                        shot.LayoutWasRemoved -= ActiveShot_LayoutWasRemoved;
                        session.Project.ActiveShot.UpdateBackgroundLayerEvent -= ActiveShot_UpdateBackgroundLayerEvent;
                    }
                }
            }
        }

        private void PrepareFormatPivotItemForShow(BaseLayoutView view)
        {
            if (PropertyPivotItem.Content is FormatToolbarPanelView)
            {
                var pivotItem = PropertyPivotItem.Content as FormatToolbarPanelView;
                pivotItem.Update(view);
            }
            if (Pivot.SelectedItem != PropertyPivotItem && Pivot.SelectedItem != FilePivotItem)
            {
                _previousPanelItem = Pivot.SelectedItem;
            }
            if(_propertyPivotHeaderItem != null)
                _propertyPivotHeaderItem.Visibility = Visibility.Visible;
        }

        private void PrepareFormatPivotItemForHidding()
        {
            PropertyPanel?.Clear();
            
            if (Pivot.SelectedItem == PropertyPivotItem)
            {
                Pivot.SelectedItem = _previousPanelItem;
            }
            if (_propertyPivotHeaderItem != null)
                _propertyPivotHeaderItem.Visibility = Visibility.Collapsed;
        }

        private void PrepareDesignPivotItemForShow(BaseLayoutView view)
        {
            DesignPanel?.ViewModel?.Update(view);
        }

        private void PrepareInsertPivotItem(BaseLayoutView view = null)
        {
            if (InsertPivotItem.Content is InsertToolbarPanelView insertToolbar)
            {
                bool isEnabledButtons = view != null && !view.ViewModel.IsStatic;
                insertToolbar.SetCutCopyButtonsState(isEnabledButtons);
            }
        }

        private void ResetPivot()
        {
            if (InsertPivotItem.Content is InsertToolbarPanelView insertToolBar)
            {
                insertToolBar.ViewModel.CanPaste = false;
                insertToolBar.SetCutCopyButtonsState(false);
            }

            Pivot.SelectedItem = InsertPivotItem;
            if (_propertyPivotHeaderItem != null)
            {
                _propertyPivotHeaderItem.Visibility = Visibility.Collapsed;
            }
        }

        private async Task<bool> RemoveSelectedView()
        {
            bool result = false;
            AuthoringSession session = AuthoringSession;
            if (session != null)
            {
                if (_selectedView?.ViewModel != null)
                {
                    if (_selectedView.LayoutViewState != LayoutViewState.EditState)
                    {
                        await session.Project.ActiveShot.RemoveLayer(_selectedView?.ViewModel);
                        result = true;
                    }
                }
                else
                {
                    if (session.Project.Shots.Count > 1)
                    {
                        await session.Project.RemoveShot(session.Project.ActiveShot);
                        result = true;
                    }
                }
            }
            return result;
        }

        private async Task<bool> DuplicateSelectedView()
        {
            bool result = false;
            AuthoringSession session = AuthoringSession;
            if (session != null)
            {
                if (_selectedView?.ViewModel != null)
                {
                    if (_selectedView.ViewModel.IsBackground == false && _selectedView.ViewModel.IsStatic == false)
                    {
                        await session.Project.ActiveShot.DuplicateLayer(_selectedView.ViewModel);
                        result = true;
                    }
                }
                else if (session.Project.ActiveShot != null)
                {
                    await session.ShotManager.ShowShotWithoutUndoRedo(await session.Project.Duplicate(session.Project.ActiveShot));
                    result = true;
                }
            }

            return result;
        }

        #endregion
    }
}