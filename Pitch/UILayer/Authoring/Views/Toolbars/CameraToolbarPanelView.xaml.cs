﻿using GalaSoft.MvvmLight.Ioc;
using System.Threading.Tasks;
using Pitch.Services;
using Pitch.UILayer.Authoring.ViewModels.Toolbars;
using Pitch.Helpers.Logger;
using Windows.Devices.Enumeration;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class CameraToolbarPanelView : UserControl
    {
        private CameraToolbarPanelViewModel _viewModel;
        public CameraToolbarPanelViewModel ViewModel => _viewModel;
        private bool _settingSaveInProcess;

        #region Life Cycle

        public CameraToolbarPanelView()
        {
            _viewModel = new CameraToolbarPanelViewModel();
            InitializeComponent();
            Loaded += CameraToolbarPanelView_Loaded;
        }
#if DEBUG
        ~CameraToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("****************CameraToolbarPanelView Destructor****************");
        }
#endif
        private void CameraToolbarPanelView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += CameraToolbarPanelView_Unloaded;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent += ChromaKeyEffect_ColorChangedEvent;
        }

        private void CameraToolbarPanelView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unsubscribe();
        }

        public void Unsubscribe()
        {
            Loaded -= CameraToolbarPanelView_Loaded;
            Unloaded -= CameraToolbarPanelView_Unloaded;
            ColorPickerToggleButton.Unchecked -= ColorPickerToggleButton_Unchecked;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent -= ChromaKeyEffect_ColorChangedEvent;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent -= ChromaKeyEffect_ColorChangedByUserEvent;
            Bindings.StopTracking();
            _viewModel?.Cleanup();
            _viewModel = null;
        }

        #endregion

        #region Call Backs

        private void ChromaKeyEffect_ColorChangedEvent(Windows.UI.Color color)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            ColorPickerToggleButton.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                ColorPickerToggleButton.UserBrush = new SolidColorBrush(color);
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private void ChromaKeyEffect_ColorChangedByUserEvent(Windows.UI.Color color)
        {
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent -= ChromaKeyEffect_ColorChangedByUserEvent;
            LogQueue.WriteToFile("ChromaKeyEffect_ColorChangedByUserEvent");
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            ColorPickerToggleButton.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                ColorPickerToggleButton.Unchecked -= ColorPickerToggleButton_Unchecked;
                ColorPickerToggleButton.IsChecked = false;
                _viewModel.GreenScreenColor = color;
                _viewModel.DisableColorPicker();
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private void ColorPickerToggleButton_LostFocus(object sender, RoutedEventArgs e)
        {
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent -= ChromaKeyEffect_ColorChangedByUserEvent;
            ColorPickerToggleButton.Unchecked -= ColorPickerToggleButton_Unchecked;
            ColorPickerToggleButton.IsChecked = false;
            _viewModel?.DisableColorPicker();
        }

        private void ColorPickerToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("ColorPickerToggleButton_Checked");
            _viewModel.CameraToolBarSwithChromaKeyCommand.Execute(null);
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent += ChromaKeyEffect_ColorChangedByUserEvent;
            ColorPickerToggleButton.Unchecked += ColorPickerToggleButton_Unchecked;
        }

        private void ColorPickerToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("ColorPickerToggleButton_Unchecked");
            ColorPickerToggleButton.Unchecked -= ColorPickerToggleButton_Unchecked;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ColorChangedEvent -= ChromaKeyEffect_ColorChangedByUserEvent;
            _viewModel.CameraToolBarSwithChromaKeyCommand.Execute(null);
        }

        private void Cameras_Loaded(object sender, RoutedEventArgs e)
        {
            FillCamerasList();
        }

        private async void Cameras_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CamerasList.SelectedItem is ListViewItem item)
            {
                if (item.Tag is DeviceInformation camera)
                {
                    CameraSelectFlyout.Hide();
                    await SaveCamera(camera);
                }
            }
        }

        private void Cameras_ItemClick(object sender, ItemClickEventArgs e)
        {
            CameraSelectFlyout.Hide();
        }

        #endregion

        #region Private Methods

        private void FillCamerasList()
        {
            CamerasList.SelectionChanged -= Cameras_SelectionChanged;
            CamerasList.Items.Clear();
            var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            foreach (var camera in mixer.AvailableCameras)
            {
                var item = new ListViewItem()
                {
                    Content = camera.Name,
                    Tag = camera,
                    IsSelected = (camera.Id == mixer.SelectedCamera?.Id),
                };
                CamerasList.Items.Add(item);
            }
            CamerasList.SelectionChanged += Cameras_SelectionChanged;
        }

        private async Task SaveCamera(DeviceInformation camera)
        {
            if (_settingSaveInProcess) return;
            _settingSaveInProcess = true;

            var mixrer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            
            bool composerRestartRequired = false;
            if (camera != null && camera != mixrer.SelectedCamera)
            {
                LogQueue.WriteToFile($"SaveMicrophone {camera.Name}");
                mixrer.SelectedCamera = camera;
                composerRestartRequired = true;
            }

            if (composerRestartRequired)
            {
                await mixrer.Reset();
                if (mixrer.ComposerMode == TouchcastComposerServiceMode.CameraComposingMode)
                {
                    mixrer.InitWithCamera();
                }

                await mixrer.StartPreviewingAsync();

                var session = SimpleIoc.Default.GetInstance<DataLayer.AuthoringSession>();
                if (session != null)
                {
                    foreach (var shot in session.Project.Shots)
                    {
                       await  shot.PrepareForRecording();
                    }
                }
                
            }

            _settingSaveInProcess = false;
        }

        #endregion
    }
}
