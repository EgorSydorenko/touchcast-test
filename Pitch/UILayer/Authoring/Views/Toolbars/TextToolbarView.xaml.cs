﻿using Microsoft.Graphics.Canvas.Text;
using System;
using System.Threading.Tasks;
using Pitch.UILayer.Authoring.ViewModels.Toolbars;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Pitch.UILayer._Helpers.Custom_Controls;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class TextToolbarView : UserControl
    {
        #region Private Fields

        private TextLayerLayoutView _textView;
        private TextToolbarViewModel _viewModel;
        private bool _isUpdatingPanel;
        private bool _isUpdatingAligmentPanel;

        #endregion

        #region Properties

        public TextToolbarViewModel ViewModel => _viewModel;

        #endregion

        #region Life Сycle

        public TextToolbarView()
        {
            _viewModel = new TextToolbarViewModel();

            InitializeComponent();
            InitializeMarkersListButtons();
            InitializeColorPickers();

            Loaded += TextToolbarView_Loaded;
            Unloaded += TextToolbarView_Unloaded;
        }

        private void TextToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            FontFamilyBox.ItemsSource = _viewModel.FontFamilyList;
            FontSizeBox.ItemsSource = _viewModel.FontSizeList;

            UpdateStyle();
        }

        private void TextToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unsubscribe();
        }

#if DEBUG
        ~TextToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("************* TextToolbarView Destructor *************");
        }
#endif
        #endregion

        #region Public Methods

        public void Unsubscribe()
        {
            Loaded -= TextToolbarView_Loaded;
            Unloaded -= TextToolbarView_Unloaded;

            if (_textView?.RichEditBox != null)
            {
                _textView.RichEditBox.SelectionChanged -= CurrentRichEditBox_SelectionChanged;
                _textView = null;
            }
        }

        public TextLayerLayoutView CurrentTextLayerLayoutView
        {
            get { return _textView; }
            set
            {
                if (_textView != value)
                {
                    if (_textView?.RichEditBox != null)
                    {
                        _textView.RichEditBox.SelectionChanged -= CurrentRichEditBox_SelectionChanged;
                    }

                    _textView = value;

                    if (_textView?.RichEditBox != null)
                    {
                        _textView.RichEditBox.SelectionChanged += CurrentRichEditBox_SelectionChanged;
                    }
                    UpdateStyle();
                }
            }
        }

        public void SubscribeToShortcutManager()
        {
            Shortcut.Manager.Instance.FormatShortcutsManager.ApplyBoldShortcutEvent += DrawShortcutsManager_ApplyBoldShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.ApplyItalicShortcutEvent += DrawShortcutsManager_ApplyItalicShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.IncreaseFontSizeShortcutEvent += DrawShortcutsManager_IncreaseFontSizeShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.DecreaseFontSizeShortcutEvent += DrawShortcutsManager_DecreaseFontSizeShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.JustifyParagraphShortcutEvent += DrawShortcutsManager_JustifyParagraphShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.CenterParagraphShortcutEvent += DrawShortcutsManager_CenterParagraphShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.LeftAlignShortcutEvent += DrawShortcutsManager_LeftAlignShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.RightAlignShortcutEvent += DrawShortcutsManager_RightAlignShortcutEvent;
        }

        public void UnsubscribeFromShortcutManager()
        {
            Shortcut.Manager.Instance.FormatShortcutsManager.ApplyBoldShortcutEvent -= DrawShortcutsManager_ApplyBoldShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.ApplyItalicShortcutEvent -= DrawShortcutsManager_ApplyItalicShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.IncreaseFontSizeShortcutEvent -= DrawShortcutsManager_IncreaseFontSizeShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.DecreaseFontSizeShortcutEvent -= DrawShortcutsManager_DecreaseFontSizeShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.JustifyParagraphShortcutEvent -= DrawShortcutsManager_JustifyParagraphShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.CenterParagraphShortcutEvent -= DrawShortcutsManager_CenterParagraphShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.LeftAlignShortcutEvent -= DrawShortcutsManager_LeftAlignShortcutEvent;
            Shortcut.Manager.Instance.FormatShortcutsManager.RightAlignShortcutEvent -= DrawShortcutsManager_RightAlignShortcutEvent;
        }

        #endregion

        #region Callbacks

        #region Controls

        private void AligmentToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdatingPanel || _isUpdatingAligmentPanel) return;

            if (Enum.TryParse((sender as ToggleButton).Tag.ToString(), out ParagraphAlignment aligment))
            {
                UpdateDocumentStyle(Element.TextAlignment, aligment);
            }
        }

        private void AligmentToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_textView?.RichEditBox != null)
            {
                UpdateAligmentToggleButtons(_textView.DocumentSelection.ParagraphFormat.Alignment);
                _textView.RichEditBox.Focus(FocusState.Programmatic);
            }
        }

        private void FontFamilyBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_isUpdatingPanel) return;

            if (FontFamilyBox.SelectedItem != null)
            {
                UpdateDocumentStyle(Element.FontFamily, FontFamilyBox.SelectedItem.ToString());
            }
        }

        private void FontSizeBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_isUpdatingPanel) return;

            if (FontSizeBox.SelectedItem != null)
            {
                UpdateDocumentStyle(Element.FontSize, Int32.Parse(FontSizeBox.SelectedItem.ToString()));
            }
        }

        private void IsBold_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdatingPanel) return;

            UpdateDocumentStyle(Element.Bold, IsBold.IsChecked);
        }

        private void IsItalic_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdatingPanel) return;

            UpdateDocumentStyle(Element.Italic, IsItalic.IsChecked);
        }

        private void IsUnderline_Checked(object sender, RoutedEventArgs e)
        {
            if (_isUpdatingPanel) return;

            UpdateDocumentStyle(Element.Underline, IsUnderline.IsChecked);
        }

        private void ForegroundColor_ColorSelectedEvent(Color color)
        {
            if (_isUpdatingPanel) return;

            UpdateDocumentStyle(Element.Foreground, color);
        }

        private void BackgroundColor_ColorSelectedEvent(Color color)
        {
            if (_isUpdatingPanel) return;

            UpdateDocumentStyle(Element.Background, color);
        }

        private void ListMarkerButton_ListTypeChangedEvent(object sender, ListTypeChangedEventArgs args)
        {
            if (_isUpdatingPanel) return;

            UpdateDocumentStyle(Element.List, args.ListMarkerType);
        }

        #endregion

        #region ShortCuts

        private Task<bool> DrawShortcutsManager_ApplyBoldShortcutEvent()
        {
            IsBold.IsChecked = !IsBold.IsChecked;
            UpdateDocumentStyle(Element.Bold, IsBold.IsChecked);

            return Task.FromResult<bool>(true);
        }

        private Task<bool> DrawShortcutsManager_ApplyItalicShortcutEvent()
        {
            IsItalic.IsChecked = !IsItalic.IsChecked;
            UpdateDocumentStyle(Element.Italic, IsItalic.IsChecked);

            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_DecreaseFontSizeShortcutEvent()
        {
            if (FontSizeBox.SelectedIndex > 0)
            {
                FontSizeBox.SelectedIndex = FontSizeBox.SelectedIndex - 1;
            }
            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_IncreaseFontSizeShortcutEvent()
        {
            if (FontSizeBox.SelectedIndex < FontSizeBox.Items.Count - 1)
            {
                FontSizeBox.SelectedIndex = FontSizeBox.SelectedIndex + 1;
            }
            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_JustifyParagraphShortcutEvent()
        {
            UpdateDocumentStyle(Element.TextAlignment, CanvasHorizontalAlignment.Justified);

            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_RightAlignShortcutEvent()
        {
            UpdateDocumentStyle(Element.TextAlignment, CanvasHorizontalAlignment.Right);

            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_LeftAlignShortcutEvent()
        {
            UpdateDocumentStyle(Element.TextAlignment, CanvasHorizontalAlignment.Left);

            return Task.FromResult(true);
        }

        private Task<bool> DrawShortcutsManager_CenterParagraphShortcutEvent()
        {
            UpdateDocumentStyle(Element.TextAlignment, CanvasHorizontalAlignment.Center);

            return Task.FromResult(true);
        }

        #endregion

        private void CurrentRichEditBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            UpdateStyle();
        }

        #endregion

        #region Private Methods

        private void UpdateStyle()
        {
            if (_textView?.RichEditBox != null)
            {
                var textRange = _textView.DocumentSelection;
                var range = textRange.Length;
                var str =  textRange.Text;
                var currentListType = new ListMarkType(textRange.ParagraphFormat.ListType, textRange.ParagraphFormat.ListStyle);
                var currentTextStyle = new TextStyle
                {
                    IsBold = (textRange.CharacterFormat.Bold == FormatEffect.On),
                    IsItalic = (textRange.CharacterFormat.Italic == FormatEffect.On),
                    IsUnderline = (textRange.CharacterFormat.Underline == UnderlineType.Single),
                    IsStrikethrough = (textRange.CharacterFormat.Strikethrough == FormatEffect.On),
                    FontFamily = textRange.CharacterFormat.Name,
                    FontSize = (int)textRange.CharacterFormat.Size,
                    Foreground = textRange.CharacterFormat.ForegroundColor,
                    Background = textRange.CharacterFormat.BackgroundColor,
                    ListMarkType = currentListType,
                    TextAlignment = textRange.ParagraphFormat.Alignment,
                };

                UpdatePanel(currentTextStyle);
            }
        }

        private void UpdatePanel(TextStyle style)
        {
            _isUpdatingPanel = true;

            UpdateAligmentToggleButtons(style.TextAlignment);

            IsItalic.IsChecked = style.IsItalic;
            IsBold.IsChecked = style.IsBold;
            IsUnderline.IsChecked = style.IsUnderline;

            FontSizeBox.SelectedItem = style.FontSize.ToString();
            FontFamilyBox.SelectedItem = style.FontFamily;

            ForegroundColor.SetColor(style.Foreground);
            BackgroundColor.SetColor(style.Background);

            UpdateMarkersListButtonsSelection(style.ListMarkType);

            _isUpdatingPanel = false;
        }

        private void UpdateDocumentStyle(Element element, object value)
        {
            if (_textView?.RichEditBox == null)
            {
                return;
            }
            var textRange = _textView.DocumentSelection;
            
            switch (element)
            {
                case (Element.Bold):
                    if ((bool)value)
                        textRange.CharacterFormat.Bold = FormatEffect.On;
                    else
                        textRange.CharacterFormat.Bold = FormatEffect.Off;
                    break;

                case (Element.Italic):
                    if ((bool)value)
                        textRange.CharacterFormat.Italic = FormatEffect.On;
                    else
                        textRange.CharacterFormat.Italic = FormatEffect.Off;
                    break;

                case (Element.Underline):
                    textRange.CharacterFormat.Underline = ((bool)value) ? UnderlineType.Single : UnderlineType.None;
                    break;

                case (Element.Strikethrough):
                    if ((bool)value)
                        textRange.CharacterFormat.Strikethrough = FormatEffect.On;
                    else
                        textRange.CharacterFormat.Strikethrough = FormatEffect.Off;
                    break;

                case (Element.FontFamily):
                    if (value != null)
                        textRange.CharacterFormat.Name = (string)value;
                    break;

                case (Element.TextAlignment):
                    textRange.ParagraphFormat.Alignment = (ParagraphAlignment)value;
                    UpdateAligmentToggleButtons((ParagraphAlignment)value);
                    break;

                case (Element.Foreground):
                    if (value is Color foreColor)
                        textRange.CharacterFormat.ForegroundColor = foreColor;
                    break;

                case (Element.Background):
                    if (value is Color backColor)
                        textRange.CharacterFormat.BackgroundColor = backColor;
                    break;

                case (Element.FontSize):
                    if (value != null)
                        textRange.CharacterFormat.Size = (int)value;
                    break;

                case (Element.List):
                    if (value is ListMarkType listMarkType)
                    {
                        textRange.ParagraphFormat.ListStart = 1;
                        textRange.ParagraphFormat.ListType = listMarkType.Type;
                        textRange.ParagraphFormat.ListStyle = listMarkType.Style;

                        UpdateMarkersListButtonsSelection(listMarkType);
                    }
                    break;
                default:
                    break;
            }
            _textView.UpdateDocumentStyle(textRange);

            _textView.RichEditBox.Focus(FocusState.Programmatic);
        }

        private void InitializeMarkersListButtons()
        {
            BulltedListButton.InitializeMarkersButtons(ViewModel.BulletedListMarkers);
            BulltedListButton.DefaultMarker = ViewModel.BulletedListMarkers[0];
            NumberedListButton.InitializeMarkersButtons(ViewModel.NumberedListMarkers);
            NumberedListButton.DefaultMarker = ViewModel.NumberedListMarkers[0];
        }

        private void InitializeColorPickers()
        {
            ForegroundColor.DefaultColor = Colors.White;
            BackgroundColor.DefaultColor = Color.FromArgb(0x0, 0x0, 0x0, 0x1); //Default RichEditBox Background Color
            ForegroundColor.OpacityVisibile = false;
            BackgroundColor.OpacityVisibile = false;
        }

        private void UpdateMarkersListButtonsSelection(ListMarkType markerType)
        {
            BulltedListButton.SelectButtonByMarkerType(markerType);
            NumberedListButton.SelectButtonByMarkerType(markerType);
        }

        private void UpdateAligmentToggleButtons(ParagraphAlignment aligment)
        {
            _isUpdatingAligmentPanel = true;

            LeftAligmentToggleButton.IsChecked = aligment == ParagraphAlignment.Left;
            CenterAligmentToggleButton.IsChecked = aligment == ParagraphAlignment.Center;
            RightAligmentToggleButton.IsChecked = aligment == ParagraphAlignment.Right;
            JustifiedAligmentToggleButton.IsChecked = aligment == ParagraphAlignment.Justify;

            _isUpdatingAligmentPanel = false;
        }

        #endregion
    }
}
