﻿using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class ToolbarColorItemControl : UserControl
    {
        public ToolbarColorItemControl(Color color)
        {
            InitializeComponent();
            Color.Background = new SolidColorBrush(color);
        }
    }
}
