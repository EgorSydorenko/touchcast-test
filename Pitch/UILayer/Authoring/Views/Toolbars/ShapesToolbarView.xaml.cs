﻿using System.Collections.Generic;
using System.Linq;
using Pitch.DataLayer.Layouts;
using Pitch.Models;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class ShapesToolbarView : UserControl
    {
        #region Fields

        private ShapeLayerLayout _model;
        private bool _isUpdateInPorcess;
        private IEnumerable<InkSize> _sizes;

        #endregion

        #region Properties

        public ShapeLayerLayout ShapeModel
        {
            get
            {
                return _model;
            }
            set
            {
                if (_model != null)
                {
                    _model.LayerLayoutWasChangedEvent -= Model_LayerLayoutWasChangedEvent;
                }
                _model = value;
                if (_model != null)
                {
                    FillColorButton.Visibility = (ShapeModel.ShapeType == InkShapeType.Arrow || ShapeModel.ShapeType == InkShapeType.Line) ? Visibility.Collapsed : Visibility.Visible;
                    Update();
                    _model.LayerLayoutWasChangedEvent += Model_LayerLayoutWasChangedEvent;
                }
            }
        }

        public IEnumerable<InkSize> Sizes => _sizes;

        #endregion

        #region Life Cycle

        public ShapesToolbarView()
        {
            _sizes = new[] {
                new InkSize { Size = new Size(2, 2), TextPartOne = "1/4" },
                new InkSize { Size = new Size(4, 4), TextPartOne = "1/2" },
                new InkSize { Size = new Size(6, 6), TextPartOne = "3/4" },
                new InkSize { Size = new Size(8, 8), TextPartOne = "1" },
                new InkSize { Size = new Size(10, 10), TextPartOne = "1", TextPartTwo="1/2" },
                new InkSize { Size = new Size(12, 12), TextPartOne = "2",TextPartTwo="1/4" },
                new InkSize { Size = new Size(16, 16), TextPartOne = "3" },
                new InkSize { Size = new Size(24, 24), TextPartOne = "4", TextPartTwo = "1/2" },
            };
            InitializeComponent();
            InitializeColorPickers();
            Loaded += ShapesToolbarView_Loaded;
        }

#if DEBUG
        ~ShapesToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("****** ShapesToolbarView Destructor *******");
        }
#endif

        private void ShapesToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += ShapesToolbarView_Unloaded;

            _isUpdateInPorcess = true;
            try
            {
                StrokeSizeComboBox.ItemsSource = Sizes;
                if (_model != null)
                {
                    StrokeSizeComboBox.SelectedItem = Sizes.FirstOrDefault(s => s.Size.Height == _model.StrokeSize);
                }
            }
            finally
            {
                _isUpdateInPorcess = false;
            }
        }

        private void ShapesToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            Unsubscribe();
        }

        #endregion

        #region Public Methods

        public void Unsubscribe()
        {
            if (ShapeModel != null)
            {
                ShapeModel.LayerLayoutWasChangedEvent -= Model_LayerLayoutWasChangedEvent;
            }
            Loaded -= ShapesToolbarView_Loaded;
            Unloaded -= ShapesToolbarView_Unloaded;
        }

        public void SetNoColorButtonsVisivility(bool isVisible)
        {
            StrokeColorButton.NoColorVisible = isVisible;
            FillColorButton.NoColorVisible = isVisible;
        }

        #endregion

        #region Private Methods

        private void InitializeColorPickers()
        {
            StrokeColorButton.DefaultColor = Colors.Transparent;
            StrokeColorButton.OpacityVisibile = true;
            FillColorButton.DefaultColor = Colors.Transparent;
            FillColorButton.OpacityVisibile = true;
        }

        private void Update()
        {
            _isUpdateInPorcess = true;
            try
            {
                StrokeSizeComboBox.SelectedItem = Sizes.FirstOrDefault(s => s.Size.Height == ShapeModel.StrokeSize);
                StrokeColorButton.UserColor = ShapeModel.StrokeColor;
                FillColorButton.UserColor = ShapeModel.FillColor;
            }
            finally
            {
                _isUpdateInPorcess = false;
            }
        }

        private void PushMementoModel()
        {
            ShapeModel.Shot.UpdateProjectHasUnsavedChanges();
            ShapeModel.Shot.Project.UndoRedoOriginator.PushNewUndoAction(DataLayer.UndoRedo.UndoRedoMementoType.Change, CreateMementoModelForCurrentShape());
        }

        private LayerLayoutMementoModel CreateMementoModelForCurrentShape()
        {
            ShapeModel.UpdatePreviousPosition();
            var mementoModel = ShapeModel.GenerateMementoModel();
            mementoModel.PreviousPosition = ShapeModel.Position;

            return mementoModel;
        }

        #endregion

        #region Callbacks

        private void StrokeSizeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_isUpdateInPorcess) return;
            PushMementoModel();
            ShapeModel.StrokeSize = (StrokeSizeComboBox.SelectedItem as InkSize).Size.Height;
        }

        private void StrokeColorButton_ColorSelectedEvent(Color color)
        {
            if (_isUpdateInPorcess) return;
            PushMementoModel();
            ShapeModel.StrokeColor = StrokeColorButton.UserColor;
        }

        private void FillColorButton_ColorSelectedEvent(Color color)
        {
            if (_isUpdateInPorcess) return;
            PushMementoModel();
            ShapeModel.FillColor = FillColorButton.UserColor;
        }

        private void Model_LayerLayoutWasChangedEvent(LayerLayoutMementoModel model)
        {
            Update();
        }

        #endregion
    }
}