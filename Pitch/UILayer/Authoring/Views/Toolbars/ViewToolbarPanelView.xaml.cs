﻿using Pitch.UILayer.Authoring.ViewModels.Toolbars;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class ViewToolbarPanelView : UserControl
    {
        private ViewToolbarPanelViewModel _viewModel;

        public ViewToolbarPanelViewModel ViewModel => _viewModel;

        public ViewToolbarPanelView()
        {
            InitializeComponent();
            _viewModel = new ViewToolbarPanelViewModel();
            Loaded += ViewToolbarPanelView_Loaded;
        }

#if DEBUG
        ~ViewToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("************* ViewToolbarPanelView destructor *************");
        }
#endif

        private void ViewToolbarPanelView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += ViewToolbarPanelView_Unloaded;
        }

        private void ViewToolbarPanelView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ViewToolbarPanelView_Loaded;
            Unloaded -= ViewToolbarPanelView_Unloaded;
            Bindings?.StopTracking();
            ViewModel?.Cleanup();
            _viewModel = null;
        }
    }
}
