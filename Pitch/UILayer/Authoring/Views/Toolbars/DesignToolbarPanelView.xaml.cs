﻿using Pitch.UILayer.Authoring.ViewModels.Toolbars;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Authoring.Views.Toolbars
{
    public sealed partial class DesignToolbarPanelView : UserControl
    {
        public DesignToolbarPanelViewModel ViewModel;
        
        #region Life Cycle

        public DesignToolbarPanelView()
        {
            InitializeComponent();
            ViewModel = new DesignToolbarPanelViewModel();
        }

#if DEBUG
        ~DesignToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("**************** DesignToolbarPanelView Destructor****************");
        }
#endif
        #endregion

        private void ColorSelectorCompositeButton_ColorSelectedEvent(Windows.UI.Color color)
        {
            ViewModel.SolidColorBackgroundCommand.Execute(color);
        }
    }
}
