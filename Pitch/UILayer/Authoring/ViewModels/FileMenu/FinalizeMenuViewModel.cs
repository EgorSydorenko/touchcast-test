﻿using GalaSoft.MvvmLight;
using System.Threading;
using System.Threading.Tasks;
using Pitch.DataLayer;

namespace Pitch.UILayer.Authoring.ViewModels.FileMenu
{
    public class FinalizeMenuViewModel : ViewModelBase
    {
        #region Private Fields
        private Exporter _exporter;
        private double _percent;
        private bool _isExportCompleted;
        #endregion
        public Exporter Exporter
        {
            get => _exporter;
            set
            {
                UnsubscribeFromExporter();

                _exporter = value;
                Percent = 0;
                if (_exporter != null)
                {
                    _exporter.CompositionExporterProgressChangedEvent += Exporter_CompositionExporterProgressChangedEvent;
                    _exporter.CompositionExporterCompletedEvent += Exporter_CompositionExporterCompletedEvent;
                }
            }
        }

        public double Percent
        {
            get => _percent;
            set => Set(ref _percent, value);
        }
        public bool IsExportCompleted
        {
            get => _isExportCompleted;
            set => Set(ref _isExportCompleted, value);
        }

        #region Life cycle
        public FinalizeMenuViewModel()
        {
        }
        public override void Cleanup()
        {
            CancelExport();
            UnsubscribeFromExporter();
            _exporter = null;
        }

#if DEBUG
        ~FinalizeMenuViewModel()
        {
            System.Diagnostics.Debug.WriteLine("********************FinalizeMenuViewModel Destructor********************");
        }
#endif
        #endregion

        #region Public methods
        public async Task Export()
        {
            await Task.Run(async () => {
                try
                {
                    if (_exporter != null)
                    {
                        await _exporter.Export();
                    }
                }
                catch(System.Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                }
            });
        }

        public void CancelExport()
        {
            IsExportCompleted = false;
            _exporter?.Cancel();
        }

        #endregion

        #region Callbacks
        private void Exporter_CompositionExporterProgressChangedEvent(double percent)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () => Percent = percent);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed        
        }
        private void Exporter_CompositionExporterCompletedEvent(Models.Touchcast touchcast)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () => IsExportCompleted = true);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed        
        }
        #endregion

        #region Private methods
        private void UnsubscribeFromExporter()
        {
            if (_exporter != null)
            {
                CancelExport();
                _exporter.CompositionExporterProgressChangedEvent -= Exporter_CompositionExporterProgressChangedEvent;
                _exporter.CompositionExporterCompletedEvent -= Exporter_CompositionExporterCompletedEvent;
            }
        }
        #endregion
    }
}
