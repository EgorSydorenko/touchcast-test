﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using TfSdk;
using Pitch.DataLayer;
using Pitch.Helpers.Logger;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml.Media.Imaging;

namespace Pitch.UILayer.Authoring.ViewModels.FileMenu
{
    public class InfoMenuViewModel : ViewModelBase
    {
        #region Constants
        private const string InfoMenuNotSaveMessage = "InfoMenuNotSaveMessage";
        private readonly string EmptyUserImagePath = "ms-appx:///Assets/Icons/cam-off-1-scene@3x.png";
        private readonly string NotSaveMessage;
        #endregion

        #region Fields

        private WeakReference<AuthoringSession> _session;
        private string _displayedSize;
        private int _slides;
        private string _title;
        private string _lastModified;
        private string _created;
        private string _userImagePath;
        private BitmapImage _userImage;
        private string _pitchVersion;
        private string _author;

        #endregion

        #region Properties

        private AuthoringSession AuthoringSession => _session?.TryGetObject();



        public string DisplayedSize
        {
            get
            {
                return _displayedSize;
            }
            set
            {
                Set(ref _displayedSize, value);
            }
        }

        public int Slides
        {
            get
            {
                return _slides;
            }
            set
            {
                Set(ref _slides, value);
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                Set(ref _title, value);
            }
        }

        public string LastModified
        {
            get
            {
                return _lastModified;
            }
            set
            {
                Set(ref _lastModified, value);
            }
        }

        public string Created
        {
            get
            {
                return _created;
            }
            set
            {
                Set(ref _created, value);
            }
        }

        public string UserImagePath
        {
            get
            {
                return _userImagePath;
            }
            set
            {
                Set(ref _userImagePath, value);
                if (_userImagePath != null && Uri.TryCreate(_userImagePath, UriKind.Absolute, out Uri result))
                {
                    UserImage = new BitmapImage(result);
                }
            }
        }

        public BitmapImage UserImage
        {
            get
            {
                return _userImage;
            }
            set
            {
                Set(ref _userImage, value);
            }
        }
        
        public string PitchVersion
        {
            get
            {
                return _pitchVersion;
            }
            set
            {
                Set(ref _pitchVersion, value);
            }
        }

        public string Author
        {
            get
            {
                return _author;
            }
            set
            {
                Set(ref _author, value);
            }
        }

        #endregion

        #region Life Cycle

        public InfoMenuViewModel()
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                _session = new WeakReference<AuthoringSession>(session);
            }
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            NotSaveMessage = loader.GetString(InfoMenuNotSaveMessage);
        }

#if DEBUG
        ~InfoMenuViewModel()
        {
            System.Diagnostics.Debug.WriteLine("****************InfoMenuViewModel Destructor****************");
        }
#endif
        #endregion

        #region Public Methods

        public void UpdateInfo()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project != null)
                {
                    Slides = session.Project.Shots.Count;
                    Title = session.Project.Name;
                    SetDisplayedProjectSize(session.Project.Size);
                    if (session.Project.LastModifyTime != null)
                    {
                        LastModified = session.Project.LastModifyTime.ToString();
                    }
                    if (session.Project.CreationTime != null)
                    {
                        Created = session.Project.CreationTime.ToString();
                    }
                }
            }
            PitchVersion = Pitch.App.ApplicationVersion;
            Author = String.Format("{0} {1}", ApiClient.Shared.UserInfo?.info?.data?.first_name ?? String.Empty, ApiClient.Shared.UserInfo?.info?.data?.last_name ?? String.Empty);
            var tmp = ApiClient.Shared.UserInfo?.info?.data?.avatar_small;
            UserImagePath = ApiClient.Shared.UserInfo?.info?.data?.avatar_small ?? EmptyUserImagePath;
        }

        public override void Cleanup()
        {
            base.Cleanup();
            UserImage = null;
        }

        #endregion

        #region Private Methods

        private void SetDisplayedProjectSize(ulong size)
        {
            if (0 < size && size < 1024)
            {
                DisplayedSize = $"{size} B";
            }
            else if (1024 <= size && size < (1024 * 1024))
            {
                DisplayedSize = $"{size / 1024} KB";
            }
            else if (size >= 1024 * 1024)
            {
                DisplayedSize = $"{size / (1024 * 1024)} MB";
            }
            else
            {
                DisplayedSize = NotSaveMessage;
            }
        }

        #endregion
    }
}
