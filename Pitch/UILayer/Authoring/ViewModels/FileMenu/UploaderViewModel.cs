﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using TfSdk;
using TfSdk.Data;
using TfSdk.Data.Fabric.Models;
using Pitch.DataLayer;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Pitch.UILayer.Authoring.ViewModels.FileMenu
{
    public enum UploaderType
    {
        Fabric,
        Kaltura,
        Exchange,
        Desktop
    }
    public class UploaderViewModel : ViewModelBase
    {
        #region Constants
        private const string ExportOverlayOnlyMe = "ExportOverlayOnlyMe";
        private const string ExportOverlayGroup = "ExportOverlayGroup";
        private const string ExportOverlayLink = "ExportOverlayLink";
        private const string ExportOverlayPassword = "ExportOverlayPassword";
        private const string ExportOverlaySpecificPeople = "ExportOverlaySpecificPeople";

        private const string ExportOverlayProcessingCanceled = "ExportOverlayProcessingCanceled";
        private const string ExportOverlayProcessingFailed = "ExportOverlayProcessingFailed";
        private const string ExportOverlayProcessingProcessing = "ExportOverlayProcessingProcessing";
        private const string ExportOverlayProcessingPercent = "ExportOverlayProcessingPercent";
        private const string ExportOverlayProcessingSuccessMediaExch = "ExportOverlayProcessingSuccessMediaExch";
        private const string ExportOverlayProcessingSuccessFabric = "ExportOverlayProcessingSuccessFabric";
        private const string ExportOverlayProcessingSuccessKaltura = "ExportOverlayProcessingSuccessKaltura";

        private readonly string ProcessingCanceled;
        private readonly string ProcessingFailed;
        private readonly string ProcessingProcessing;
        private readonly string ProcessingPercent;
        private readonly string ProcessingSuccessMediaExch;
        private readonly string ProcessingSuccessFabric;
        private readonly string ProcessingSuccessKaltura;
        #endregion

        #region Private Fields
        private Exporter _exporter;
        private Touchcast _touchcast;
        private StorageFile _thumbnail;
        private UploaderType _uploaderType;
        private BitmapImage _thumbImage;
        private string _title = String.Empty;
        private string _description = String.Empty;
        private string _progressMessage;
        private Brush _progressBarBrush;
        private bool _isUploadingFailed;
        private bool _isIndeterminate;
        private int _percent;
        private string _sharedText;
        private BitmapImage _sharedIconSource;
        private SharedState _fileSharedState;
        private string _sharedUrl = String.Empty;
        private string _embedCode = String.Empty;
        private bool _isVisibleSharedUrl;
        private bool _isKaltura;
        private bool _isKalturaNameError;
        private bool _isVisibleEmbedCode;
        private bool _isShowUploadingProgress;
        private string _selectedGroup;
        private IUploader _uploader;
        private bool _isUploadingInProcess;
        private string _uploadedFileId;
        private bool _isExportCompleted;
        private readonly Dictionary<SharedState, string> _sharedMessages = new Dictionary<SharedState, string>();
        private Dictionary<SharedState, string> _sharedIconsSources = new Dictionary<SharedState, string>()
        {
            { SharedState.Group, "ms-appx:///Assets/Upload/save-upload-icon-anyone-in-tc.png" },
            { SharedState.Link, "ms-appx:///Assets/Upload/save-upload-icon-link.png" },
            { SharedState.OnlyMe, "ms-appx:///Assets/Upload/save-upload-icon-lock.png" },
            { SharedState.Password, "ms-appx:///Assets/Upload/save-upload-icon-password.png" },
            { SharedState.SpecificPeople, "ms-appx:///Assets/Upload/save-upload-icon-specific-people.png" },
        };

        private ICommand _shareDialogCommand;
        private ICommand _copyLinkCommand;
        private ICommand _copyEmbedCodeCommand;

        private SharedState FileSharedState
        {
            get => _fileSharedState;
            set
            {
                Set(ref _fileSharedState, value);
                SharedText = value == SharedState.Group ? String.Format(_sharedMessages[value], SelectedGroupName) : SharedText = _sharedMessages[value];
                SharedIcon = new BitmapImage(new Uri(_sharedIconsSources[value]));
                //IsVisibleSharedUrl = (_fileSharedState != SharedState.OnlyMe);
                //IsVisibleEmbedCode = (_fileSharedState == SharedState.Link);
            }
        }
        #endregion

        public ICommand SharedDialogCommand => _shareDialogCommand;
        public ICommand CopyLinkCommand => _copyLinkCommand;
        public ICommand CopyEmbedCodeCommand => _copyEmbedCodeCommand;

        #region Public properties
        public double Progress => _exporter.Progress;
        public Touchcast Touchcast => _touchcast;
        public string Title
        {
            get => _title;
            set
            {
                Set(ref _title, value);
                if (_touchcast != null)
                {
                    _touchcast.Title = _title;
                }
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                Set(ref _description, value);
                if (_touchcast != null)
                {
                    _touchcast.Description = _description;
                }
            }
        }

        public StorageFile Thumbnail
        {
            get => _thumbnail;
            set => Set(ref _thumbnail, value);
        }

        public BitmapImage ThumbImage
        {
            get => _thumbImage;
            set => Set(ref _thumbImage, value);
        }

        public string ProgressMessage
        {
            get => _progressMessage;
            set => Set(ref _progressMessage, value);
        }
        public Brush ProgressBarBrush
        {
            get => _progressBarBrush;
            set => Set(ref _progressBarBrush, value);
        }

        public bool IsUploadingFailed
        {
            get => _isUploadingFailed;
            set
            {
                Set(ref _isUploadingFailed, value);
                if (_isUploadingFailed)
                {
                    ProgressBarBrush = new SolidColorBrush(Colors.Red);
                    ProgressMessage = ProcessingFailed;
                }
                else
                {
                    ProgressBarBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0xad, 0x4c));
                }
            }
        }

        public bool IsUploadingCanceled { get; private set; }
        public bool IsIndeterminate
        {
            get => _isIndeterminate;
            set => Set(ref _isIndeterminate, value);
        }

        public int Percent
        {
            get => _percent;
            set => Set(ref _percent, value);
        }

        public string SharedText
        {
            get => _sharedText;
            set => Set(ref _sharedText, value);
        }

        public BitmapImage SharedIcon
        {
            get => _sharedIconSource;
            set => Set(ref _sharedIconSource, value);
        }
        public bool IsVisibleSharedUrl
        {
            get => _isVisibleSharedUrl;
            set => Set(ref _isVisibleSharedUrl, value);
        }
        public bool IsVisibleEmbedCode
        {
            get => _isVisibleEmbedCode;
            set => Set(ref _isVisibleEmbedCode, value);
        }
        public bool IsShowUploadingProgress
        {
            get => _isShowUploadingProgress;
            set => Set(ref _isShowUploadingProgress, value);
        }
        public string SelectedGroupName
        {
            get => _selectedGroup;
            set => Set(ref _selectedGroup, value);
        }
        public bool IsUploadingInProcess
        {
            get => _isUploadingInProcess;
            set => Set(ref _isUploadingInProcess, value);
        }
        public string SharedUrl
        {
            get => _sharedUrl;
            set => Set(ref _sharedUrl, value);
        }
        public string EmbedCode
        {
            get => _embedCode;
            set => Set(ref _embedCode, value);
        }

        public bool IsKaltura
        {
            get => _isKaltura;
            set
            {
                if (_isKaltura != value)
                {
                    if (value)
                    {
                        if (_touchcast != null)
                        {
                            IsKalturaNameError = string.IsNullOrEmpty(_title);
                        }
                    }
                    else
                    {
                        IsKalturaNameError = false;
                    }
                    Set(ref _isKaltura, value);
                }
            }
        }
        public bool IsKalturaNameError
        {
            get => _isKalturaNameError;
            set => Set(ref _isKalturaNameError, value);
        }
        public bool IsExportCompleted
        {
            get => _isExportCompleted;
            set => Set(ref _isExportCompleted, value);
        }

        public Exporter Exporter
        {
            get => _exporter;
            set
            {
                Cancel();

                _exporter = value;

                if (_exporter != null)
                {
                    _exporter.CompositionExporterCompletedEvent += Exporter_CompositionExporterCompletedEvent;
                    _exporter.CompositionExporterCanceledEvent += Exporter_CompositionExporterCanceledEvent;
                }
            }
        }
        #endregion

        #region Life cycle
        public UploaderViewModel(UploaderType uploaderType)
        {
            IsKaltura = (uploaderType == UploaderType.Kaltura || uploaderType == UploaderType.Exchange);
            _uploaderType = uploaderType;

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            _sharedMessages[SharedState.OnlyMe] = loader.GetString(ExportOverlayOnlyMe);
            _sharedMessages[SharedState.Group] = loader.GetString(ExportOverlayGroup);
            _sharedMessages[SharedState.Link] = loader.GetString(ExportOverlayLink);
            _sharedMessages[SharedState.Password] = loader.GetString(ExportOverlayPassword);
            _sharedMessages[SharedState.SpecificPeople] = loader.GetString(ExportOverlaySpecificPeople);
            ProcessingCanceled = loader.GetString(ExportOverlayProcessingCanceled);
            ProcessingFailed = loader.GetString(ExportOverlayProcessingFailed);
            ProcessingProcessing = loader.GetString(ExportOverlayProcessingProcessing);
            ProcessingPercent = loader.GetString(ExportOverlayProcessingPercent);
            ProcessingSuccessMediaExch = loader.GetString(ExportOverlayProcessingSuccessMediaExch);
            ProcessingSuccessFabric = loader.GetString(ExportOverlayProcessingSuccessFabric);
            ProcessingSuccessKaltura = loader.GetString(ExportOverlayProcessingSuccessKaltura);

            _shareDialogCommand = new RelayCommand(async () =>
            {
                try
                {
                    await ShowSharingDialog();
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
            _copyLinkCommand = new RelayCommand(() =>
            {
                try
                {
                    CopyTextToClipboard(_sharedUrl);
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
            _copyEmbedCodeCommand = new RelayCommand(() =>
            {
                try
                {
                    CopyTextToClipboard(_embedCode);
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
        }

        public override void Cleanup()
        {
            _shareDialogCommand = null;
            _copyLinkCommand = null;
            _copyEmbedCodeCommand = null;
            Cancel();
        }
        public void Reset()
        {
            IsVisibleSharedUrl = false;
            IsVisibleEmbedCode = false;
            _embedCode = String.Empty;
            _sharedUrl = String.Empty;
            IsShowUploadingProgress = false;
        }
#if DEBUG
        ~UploaderViewModel()
        {
            System.Diagnostics.Debug.WriteLine("********************UploaderViewModel Destructor********************");
        }
#endif
        #endregion

        #region Public methods
        public void Update()
        {
            IsExportCompleted = false;//_touchcast != null;

            if (_touchcast != null)
            {
                Thumbnail = _touchcast.VideoThumbnailFile;
                Title = string.IsNullOrEmpty(_touchcast.Title) ? _touchcast.TouchcastFile.DisplayName : _touchcast.Title;
                Description = _touchcast.Description;
            }
        }

        public void Cancel()
        {
            if (_exporter != null)
            {
                _exporter.CompositionExporterCompletedEvent -= Exporter_CompositionExporterCompletedEvent;
                _exporter.CompositionExporterCanceledEvent -= Exporter_CompositionExporterCanceledEvent;
                _exporter = null;
            }
            _touchcast = null;
        }

        public async Task Uploading()
        {
            IsUploadingCanceled = false;
            if (_uploaderType == UploaderType.Fabric)
            {
                FileSharedState = SharedState.OnlyMe;
            }
            if (ApiClient.Shared.UserInfo.info == null)
            {
                //IsShowPopup = true;
                //IsFabric = false;
                //IsKaltura = false;
                return;
            }

            var groupPath = ApiClient.Shared.UserInfo.groupUniquePath;
            if (groupPath != null)
            {
                IsUploadingFailed = false;
                
                var uploadInfo = new TouchcastInfo();
                if (_uploaderType == UploaderType.Fabric)
                {
                    uploadInfo.CompressedInfo = new Pitch.Models.FabricUploadingModel(_touchcast.TouchcastFile, _title, _description, groupPath);
                    _uploader = ApiClient.Shared.CreateFabricUploader();
                }
                else
                {
                    var data = new Pitch.Models.KalturaUploadingModel(_touchcast);
                    data.Title = _title;
                    data.Description = _description;
                    uploadInfo.UncompressedInfo = data;
                    _uploader = ApiClient.Shared.CreateKalturaUploader();
                }
                _uploader.UploadingCompletedEvent += Uploader_UploadingCompletedEvent;
                _uploader.UploadingFailedEvent += Uploader_UploadingFailedEvent;
                _uploader.UploadingProgressEvent += Uploader_UploadingProgressEvent;
                _uploader.UploadingStartedEvent += Uploader_UploadingStartedEvent;
                _uploader.UploadingDataRecivedEvent += Uploader_UploadingDataRecivedEvent;
                _uploader.UploadingFileIdRecivedEvent += Uploader_UploadingFileIdRecivedEvent;
                if (uploadInfo != null)
                {
                    try
                    {
                        _uploadedFileId = String.Empty;
                        await _uploader.Upload(uploadInfo);
                        await _uploader.UpdateFileInfo(Title, Description);
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticCommandTouchcastExported, ApiClient.Shared.UserInfo.kalturaTokenInfo == null ? "fabric" : "kaltura");
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                        LogQueue.WriteToFile($"Exception in ExportTouchcastOverlayViewModel.Uploading : {e.Message}");
                    }
                    finally
                    {
                        _uploader.UploadingCompletedEvent -= Uploader_UploadingCompletedEvent;
                        _uploader.UploadingFailedEvent -= Uploader_UploadingFailedEvent;
                        _uploader.UploadingProgressEvent -= Uploader_UploadingProgressEvent;
                        _uploader.UploadingStartedEvent -= Uploader_UploadingStartedEvent;
                        _uploader.UploadingDataRecivedEvent -= Uploader_UploadingDataRecivedEvent;
                        _uploader.UploadingFileIdRecivedEvent -= Uploader_UploadingFileIdRecivedEvent;
                    }
                }
            }
        }

        public void CancelUploading()
        {
            if (_uploader != null && IsUploadingInProcess)
            {
                IsUploadingInProcess = false;
                _uploader.Cancel();
                ProgressMessage = ProcessingCanceled;
                IsUploadingCanceled = true;
            }
        }
        public void TryUpdateTouchcastData()
        {
            try
            {
                //_touchcast.Title = Title;
                //_touchcast.Description = Description;
                if (_uploader != null)
                {
                    _uploader.UpdateFileInfo(Title, Description);
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in ExportTouchcastOverlayViewModel.TryUpdateTouchcastData : {ex.Message}");
            }
        }
        #endregion

        #region Callbacks
        private void Uploader_UploadingStartedEvent(object sender)
        {
            Percent = 0;
            IsUploadingInProcess = true;
            IsShowUploadingProgress = true;
            IsIndeterminate = true;
            ProgressMessage = ProcessingProcessing;
            SharedUrl = String.Empty;
            EmbedCode = String.Empty;
        }

        private void Uploader_UploadingFileIdRecivedEvent(object sender, string fileId)
        {
            _uploadedFileId = fileId;
        }

        private void Uploader_UploadingProgressEvent(object sender, int persentsOfProgress)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () =>
                {
                    IsIndeterminate = false;
                    Percent = persentsOfProgress;
                    if (_isUploadingInProcess)
                    {
                        ProgressMessage = String.Format(ProcessingPercent, persentsOfProgress);
                    }
                });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private void Uploader_UploadingFailedEvent(object sender, object reason)
        {
            IsUploadingInProcess = false;
            IsUploadingFailed = true;
            _uploadedFileId = String.Empty;
        }

        private void Uploader_UploadingDataRecivedEvent(object sender, string key, string data)
        {
            if (key == IUploaderConstants.LinkKey)
            {
                SharedUrl = data;
                IsVisibleSharedUrl = !String.IsNullOrEmpty(SharedUrl) && (_isKaltura || (!_isKaltura && _fileSharedState != SharedState.OnlyMe));
            }
            else if (key == IUploaderConstants.EmbedCodeKey)
            {
                EmbedCode = data;
                IsVisibleEmbedCode = !String.IsNullOrEmpty(EmbedCode) && (!_isKaltura && _fileSharedState == SharedState.Link);
            }
        }
        private void Uploader_UploadingCompletedEvent(object sender)
        {
            if (_uploaderType == UploaderType.Exchange)
            {
                ProgressMessage = ProcessingSuccessMediaExch;
            }
            else if (_uploaderType == UploaderType.Kaltura)
            {
                ProgressMessage = ProcessingSuccessKaltura;
            }
            else
            {
                ProgressMessage = ProcessingSuccessFabric;
            }

            IsUploadingInProcess = false;
            IsShowUploadingProgress = false;
        }

        private void Exporter_CompositionExporterCanceledEvent()
        {
            //MessengerInstance.Send(new HideExportOverlay() { Touchcast = null });
        }

        private void Exporter_CompositionExporterCompletedEvent(Models.Touchcast touchcast)
        {
            _touchcast = touchcast;
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                async () =>
                {
                    Thumbnail = _touchcast.VideoThumbnailFile;
                    if (String.IsNullOrEmpty(Title))
                    {
                        Title = _touchcast.TouchcastFile.DisplayName;
                    }
                    else
                    {
                        _touchcast.Title = Title;
                    }
                    if (!String.IsNullOrEmpty(Description))
                    {
                        _touchcast.Description = Description;
                    }

                    IsKalturaNameError = _isKaltura && string.IsNullOrEmpty(_title);

                    IsExportCompleted = true;
                    using (var thumbStream = await Thumbnail.OpenAsync(FileAccessMode.Read))
                    {
                        var thumbImage = new BitmapImage();
                        thumbImage.SetSource(thumbStream);
                        ThumbImage = thumbImage;
                    }
                });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        #endregion

        #region Private methods
        private async Task ShowSharingDialog()
        {
            if (String.IsNullOrEmpty(_uploadedFileId)) return;

            var redirectEndPoint = App.FabricEndpointData.redirect_endpoint;
            var appId = App.AppSettings.GetString(App.AppId);
            var product = App.AppSettings.GetString(App.Product);
            var sharedUrl = App.FabricEndpointData.change_privacy_endpoint;
            sharedUrl = String.Format(sharedUrl, redirectEndPoint, product, appId, App.DeviceName, App.ApplicationVersion);
            var groupId = ApiClient.Shared.UserInfo.groupUniquePath;

            await ApiClient.Shared.SharingFileDialog(sharedUrl, _uploadedFileId, groupId);

            var response = await ApiClient.Shared.GetFileSharedState(_uploadedFileId, groupId);
            FileSharedState = response.State;
            SharedUrl = response.Url;
            IsVisibleSharedUrl = !String.IsNullOrEmpty(SharedUrl);
            EmbedCode = response.EmbedCode;
            IsVisibleEmbedCode = !String.IsNullOrEmpty(EmbedCode);
        }
        private void CopyTextToClipboard(string text)
        {
            if (String.IsNullOrEmpty(text)) return;

            var package = new DataPackage();
            package.SetText(text);
            try
            {
                Clipboard.SetContent(package);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in ExportTouchcastOverlayViewModel.CopyTextToClipboard : {ex.Message}");
            }
        }
        public async Task SaveFileLocally(StorageFile file)
        {
            if (file.Name.Contains(".mp4"))
            {
                await _touchcast.VideoFile.CopyAndReplaceAsync(file);
            }
            else
            {
                _touchcast.TouchcastFile = await _touchcast.SaveArchive();
                await _touchcast.TouchcastFile.CopyAndReplaceAsync(file);
            }
        }
        #endregion
    }
}
