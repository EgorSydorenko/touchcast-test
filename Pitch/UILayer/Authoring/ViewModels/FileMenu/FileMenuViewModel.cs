﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.UILayer.Authoring.ViewModels.FileMenu
{
    public enum FileMenuKey
    {
        InfoMenu,
        NewMenu,
        OpenMenu,
        SaveMenu,
        SaveAsMenu,
        FinalizeMenu,
        ExportMenu,
        SettingsMenu,
        CloseMenu
    }

    public class FileMenuViewModel : ViewModelBase
    {
        #region Constants
        private const string FileMenuInfoLabel = "FileMenuInfoLabel";
        private const string FileMenuNewLabel = "FileMenuNewLabel";
        private const string FileMenuOpenLabel = "FileMenuOpenLabel";
        private const string FileMenuSaveLabel = "FileMenuSaveLabel";
        private const string FileMenuSaveAsLabel = "FileMenuSaveAsLabel";
        private const string FileMenuFinalizeLabel = "FileMenuFinalizeLabel";
        private const string FileMenuSettingsLabel = "FileMenuSettingsLabel";
        private const string FileMenuCloseLabel = "FileMenuCloseLabel";
        #endregion

        #region Fields

        private WeakReference<AuthoringSession> _session;
        private IList<FileMenuItem> _menuItems;
        private bool _isCommandExecuted;

        #endregion

        #region Properties

        private AuthoringSession AuthoringSession => _session?.TryGetObject();

        public IEnumerable<FileMenuItem> MenuItems => _menuItems;

        #endregion

        #region Life Cycle

        public FileMenuViewModel()
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                _session = new WeakReference<AuthoringSession>(session);
            }
            InitializeMenuItems();
        }

#if DEBUG
        ~FileMenuViewModel()
        {
            System.Diagnostics.Debug.WriteLine("****************FileMenuViewModel Destructor****************");
        }
#endif

        #endregion

        #region Public Methods

        public async Task<bool> ExecuteMethodByKey(FileMenuKey key)
        {
            bool result = false;
            switch (key)
            {
                case FileMenuKey.SaveMenu:
                    result = await SaveProject();
                    break;
                case FileMenuKey.SaveAsMenu:
                    result = await SaveAsProject();
                    break;
                default:
                    break;
            }

            return result;
        }

        public override void Cleanup()
        {
            _session = null;
            _menuItems.Clear();
            base.Cleanup();
        }

        #endregion

        #region Private Methods

        private void InitializeMenuItems()
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var info = loader.GetString(FileMenuInfoLabel);
            var newProject = loader.GetString(FileMenuNewLabel);
            var open = loader.GetString(FileMenuOpenLabel);
            var save = loader.GetString(FileMenuSaveLabel);
            var saveAs = loader.GetString(FileMenuSaveAsLabel);
            var finalize = loader.GetString(FileMenuFinalizeLabel);
            var settings = loader.GetString(FileMenuSettingsLabel);
            var close = loader.GetString(FileMenuCloseLabel);

            _menuItems = new List<FileMenuItem>()
            {
                new FileMenuItem(FileMenuKey.InfoMenu, info),
                new FileMenuItem(FileMenuKey.NewMenu, newProject),
                new FileMenuItem(FileMenuKey.OpenMenu, open),
                new FileMenuItem(FileMenuKey.SaveMenu, save),
                new FileMenuItem(FileMenuKey.SaveAsMenu, saveAs),
                new FileMenuItem(FileMenuKey.FinalizeMenu, finalize),
                new FileMenuItem(FileMenuKey.SettingsMenu, settings),
                new FileMenuItem(FileMenuKey.CloseMenu, close),
            };
        }

        private async Task<bool> SaveProject()
        {
            if (_isCommandExecuted) return false;

            var session = AuthoringSession;
            if (session != null)
            {
                if (!session.Project.IsTemporary)
                {
                    try
                    {
                        _isCommandExecuted = true;
                        MessengerInstance.Send(new BusyIndicatorShowMessage());
                        await session.Project.Save();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        Pitch.Helpers.Logger.LogQueue.WriteToFile($"Exception in FileMenuViewModel.SaveProject : {ex.Message}");
                    }
                    finally
                    {
                        MessengerInstance.Send(new BusyIndicatorHideMessage());
                        _isCommandExecuted = false;
                    }
                }
                else
                {
                    return await SaveAsProject();
                }
            }

            return false;
        }

        private async Task<bool> SaveAsProject()
        {
            if (_isCommandExecuted) return false;

            _isCommandExecuted = true;

            try
            {
                var session = AuthoringSession;
                if (session != null)
                {
                    var file = await FileToAppHelper.SaveFileDialog(FileToAppHelper.TouchcastProjectExtension, "Touchcast Project File", Project.DefaultProjectName);
                    if (file != null)
                    {
                        MessengerInstance.Send(new BusyIndicatorShowMessage());
                        await session.Project.SaveAs(file);

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Pitch.Helpers.Logger.LogQueue.WriteToFile($"Exception in FileMenuViewModel.SaveAsProject : {ex.Message}");
            }
            finally
            {
                MessengerInstance.Send(new BusyIndicatorHideMessage());
                _isCommandExecuted = false;
            }

            return false;
        }

        #endregion
    }

    public class FileMenuItem
    {
        #region Private Fields

        private string _caption;
        private FileMenuKey _key;

        #endregion

        #region Properties

        public string Caption => _caption;

        public FileMenuKey Key => _key;

        #endregion

        #region Life Cycle

        public FileMenuItem(FileMenuKey key, string caption)
        {
            _caption = caption;
            _key = key;
        }

        #endregion

        #region Piblic Methods
     
        public override string ToString()
        {
            return Caption;
        }

        #endregion
    }
}
