﻿using GalaSoft.MvvmLight;
using Microsoft.Graphics.Canvas.Text;
using System.Collections.Generic;
using Pitch.UILayer._Helpers.Custom_Controls;
using Windows.UI;
using Windows.UI.Text;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public enum Element
    {
        Bold,
        Italic,
        Underline,
        Strikethrough,
        TextAlignment,
        FontFamily,
        FontSize,
        Foreground,
        Background,
        List,
    };

    public class TextToolbarViewModel : ViewModelBase
    {
        #region Private Fields

        private List<string> _fontSizeList;
        private List<string> _fontFamilyList;
        private IList<ListMarkType> _bulletedListMarkers;
        private IList<ListMarkType> _numberedListMarkers;

        #endregion

        #region Public Properties

        public List<string> FontFamilyList => _fontFamilyList;
        public List<string> FontSizeList => _fontSizeList;
        public IList<ListMarkType> BulletedListMarkers => _bulletedListMarkers;
        public IList<ListMarkType> NumberedListMarkers => _numberedListMarkers;

        #endregion

        #region Life Cycle

        public TextToolbarViewModel()
        {
            
            _fontFamilyList = new List<string>(CanvasTextFormat.GetSystemFontFamilies());
            _fontFamilyList.Sort();
            _fontSizeList = new List<string>
            {
                "8",
                "9",
                "10",
                "11",
                "12",
                "14",
                "16",
                "18",
                "20",
                "22",
                "24",
                "26",
                "28",
                "36",
                "42",
                "48",
                "52",
                "56",
                "64",
                "72",
                "80",
                "88",
                "96",
            };
            _bulletedListMarkers = new List<ListMarkType>()
            {
                 new ListMarkType(MarkerType.Bullet, MarkerStyle.Parenthesis, "ms-appx:///Assets/Pivot/TextTool/bullet-icon-1@3x.png"),
            };
            _numberedListMarkers = new List<ListMarkType>()
            {
                new ListMarkType(MarkerType.Arabic, MarkerStyle.Period,                         "ms-appx:///Assets/Pivot/TextTool/numbering-icon-1@3x.png"),
                new ListMarkType(MarkerType.Arabic, MarkerStyle.Parenthesis,                    "ms-appx:///Assets/Pivot/TextTool/numbering-icon-2@3x.png"),
                new ListMarkType(MarkerType.UppercaseRoman, MarkerStyle.Period,                 "ms-appx:///Assets/Pivot/TextTool/numbering-icon-3@3x.png"),
                new ListMarkType(MarkerType.UppercaseEnglishLetter, MarkerStyle.Period,         "ms-appx:///Assets/Pivot/TextTool/numbering-icon-4@3x.png"),
                new ListMarkType(MarkerType.LowercaseEnglishLetter, MarkerStyle.Period,         "ms-appx:///Assets/Pivot/TextTool/numbering-icon-5@3x.png"),
                new ListMarkType(MarkerType.LowercaseEnglishLetter, MarkerStyle.Parenthesis,    "ms-appx:///Assets/Pivot/TextTool/numbering-icon-6@3x.png"),
            };
        }

#if DEBUG
        ~TextToolbarViewModel()
        {
            System.Diagnostics.Debug.WriteLine("********** TextToolbarViewModel Destructor **********");
        }
#endif
        #endregion
    }

    public class TextStyle
    {
        public bool IsBold { set; get; }
        public bool IsItalic { set; get; }
        public bool IsUnderline { set; get; }
        public bool IsStrikethrough { set; get; }
        public string FontFamily { set; get; }
        public bool AlignLeft { set; get; }
        public bool AlignCenter { set; get; }
        public bool AlignRight { set; get; }
        public int FontSize { set; get; }
        public ParagraphAlignment TextAlignment { set; get; }
        public Color Foreground { set; get; }
        public Color Background { set; get; }
        public ListMarkType ListMarkType { set; get; }

        public TextStyle()
        {
            Foreground = Colors.Black;
            Background = Colors.White;
        }
    }
}
