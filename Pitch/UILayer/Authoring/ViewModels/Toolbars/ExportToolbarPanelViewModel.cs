﻿using GalaSoft.MvvmLight;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class ExportToolbarPanelViewModel : ViewModelBase
    {
        #region Private fields
        private bool _isExportButtonEnabled;
        #endregion

        #region Life cycle
        public ExportToolbarPanelViewModel()
        {
            _isExportButtonEnabled = true;
        }
#if DEBUG
        ~ExportToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("********** ExportToolbarPanelViewModel Destructor ********");
        }
#endif
        public override void Cleanup()
        {
            base.Cleanup();
        }
        #endregion

        public bool IsExportButtonEnabled => _isExportButtonEnabled;
    }
}
