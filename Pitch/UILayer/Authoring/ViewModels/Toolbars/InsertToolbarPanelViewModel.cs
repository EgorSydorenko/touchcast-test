﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.UILayer.Authoring.Views.Overlays;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.Foundation.Collections;
using System.IO;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class InsertToolbarPanelViewModel : ViewModelBase
    {
        #region Constants

        private const double NewVappShiftXPos = 15;
        private const double NewVappShiftYPos = 15;

        #endregion

        #region Private Fields

        private WeakReference<AuthoringSession> _session;
        private bool _isCommandExecuted;
        private bool _canPaste;
        private int _selectedShapeIndex;

        #endregion

        #region Commands

        private ICommand _insertMediaCommand;
        private ICommand _insertWebViewCommand;
        private ICommand _insertTextToolCommand;
        private ICommand _importDataFromTouchcastCommand;
        private ICommand _insertLastAddedScene;
        private ICommand _insertLastAddedShape;
        private ICommand _insertShapeCommand;
        private ICommand _insertFileCommand;
        private ICommand _copyLayerCommand;
        private ICommand _cutLayerCommand;
        private ICommand _pasteLayerCommand;

        #endregion

        #region Properties

        private AuthoringSession AuthoringSession => _session?.TryGetObject();

        public bool CanPaste
        {
            get
            {
                return _canPaste;
            }
            set
            {
                Set(ref _canPaste, value);
            }
        }

        public List<InkShapeType> Shapes {get; private set;}

        public InkShapeType LastSelectedShape { get; private set; }

        public int SelectedShapeIndex
        {
            get
            {
                return _selectedShapeIndex;
            }
            set
            {
                Set(ref _selectedShapeIndex, value);
                InsertShapeCommand.Execute(Shapes[value]);
            }
        }

        public InkShapeType LastShapeType
        {
            get
            {
                if (SelectedShapeIndex < Shapes.Count)
                {
                    return Shapes[SelectedShapeIndex];
                }
                return InkShapeType.Line;
            }
        }

        #region Commands

        public ICommand InsertMediaCommand => _insertMediaCommand;
        public ICommand InsertWebViewCommand => _insertWebViewCommand;
        public ICommand InsertFileCommand => _insertFileCommand;
        public ICommand InsertTextToolCommand => _insertTextToolCommand;
        public ICommand ImportDataFromTouchcastCommand => _importDataFromTouchcastCommand;
        public ICommand InsertLastAddedScene => _insertLastAddedScene;
        public ICommand InsertLastAddedShape => _insertLastAddedShape;
        public ICommand InsertShapeCommand => _insertShapeCommand;
        public ICommand CopyLayerCommand => _copyLayerCommand;
        public ICommand CutLayerCommand => _cutLayerCommand;
        public ICommand PasteLayerCommand => _pasteLayerCommand;

        #endregion

        #endregion

        #region Life Cycle

        public InsertToolbarPanelViewModel()
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _session = new WeakReference<AuthoringSession>(session);

            _insertMediaCommand = new RelayCommand(async () => 
            {
                try
                {
                    if (_isCommandExecuted) return;
                    _isCommandExecuted = true;

                    StockMediaSdk.ApiClient.Initialize(App.AppSettings.GetString(App.VideoBlocksPublicKey), App.AppSettings.GetString(App.VideoBlocksPrivateKey));
                    var downloadedFiles = await StockMediaSdk.ApiClient.Shared.ShowSearchDialogAsync(
                        StockMediaSdk.Data.Model.MediaType.Video | StockMediaSdk.Data.Model.MediaType.Photo,
                        ApplicationData.Current.LocalCacheFolder);

                    MessengerInstance.Send(new BusyIndicatorShowMessage());
                    bool isShowMessage = await AuthoringSession.Project.ActiveShot.CreateLayers(downloadedFiles);
                    MessengerInstance.Send(new BusyIndicatorHideMessage());
                    foreach (var file in downloadedFiles)
                    {
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandTCStockAdded,
                                                                                   file.FileType);
                    }

                    if (isShowMessage) MessengerInstance.Send(new ShowUnsupportedFormatMessage());
                }
                catch (System.Exception ex)
                { 
                    LogQueue.WriteToFile(ex.Message);
                }
                finally
                {
                    _isCommandExecuted = false;
                }
            });

            _insertWebViewCommand = new RelayCommand(async () =>
            {
                try
                {
                    InsertWebViewContentDialog dlg = new InsertWebViewContentDialog();
                    await dlg.ShowAsync();
                    if (dlg.Result == InsertWebViewContentDialogResult.CreationCanceled)
                    {
                        dlg.Destroy();
                    }
                    else
                    {
                        var authoringSession = AuthoringSession;
                        if (authoringSession != null)
                        {
                            Pitch.DataLayer.UrlWithProperties urlWitProps = null;
                            try
                            {
                                urlWitProps = await dlg.ProcessUrlWithProperties();
                            }
                            catch
                            {
                                urlWitProps = null;
                            }
                            if (urlWitProps?.Properties != null && urlWitProps?.UrlString != null)
                            {
                                await authoringSession.Project.ActiveShot.CreateLayer(urlWitProps);
                            }
                        }
                        dlg.Destroy();
                    }
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });

            _insertFileCommand = new RelayCommand(async () => 
            {
                if (_isCommandExecuted) return;
                try
                {
                    _isCommandExecuted = true;
                    var fileInsertResult = await FileToAppHelper.InsertFile(() => MessengerInstance.Send(new BusyIndicatorShowMessage()));
                    if (fileInsertResult.File != null)
                    {
                        if (fileInsertResult.Result == FileImportResult.FileAsIs)
                        {
                            await AuthoringSession.Project.ActiveShot.CreateLayer(fileInsertResult.File);
                        }
                        else
                        {
                            await AuthoringSession.Project.CreateShotsFromDocumentPages(fileInsertResult.File);
                        }
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandLocalFileAdded, fileInsertResult.File.FileType);
                    }
                    MessengerInstance.Send(new BusyIndicatorHideMessage());
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
                finally
                {
                    _isCommandExecuted = false;
                }
            });

            _insertTextToolCommand = new RelayCommand(async () =>
            {
                if (_isCommandExecuted) return;
                try
                {
                    _isCommandExecuted = true;
                    await AuthoringSession.Project.ActiveShot.CreateTextLayer();
                    Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandTextAdded);
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
                finally
                {
                    _isCommandExecuted = false;
                }
            });
            
            _insertLastAddedScene = new RelayCommand(async () =>
            {
                if (_isCommandExecuted) return;
                try
                {
                    _isCommandExecuted = true;
                    var newShot = await AuthoringSession.Project.CreateShot();
                    await AuthoringSession.ShotManager.ShowShot(newShot);
                    Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandSceneAdded);
                }

                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
                finally
                {
                    _isCommandExecuted = false;
                }
            });

            _insertLastAddedShape = new RelayCommand(async () =>
            {
                try
                {
                    await AuthoringSession?.Project?.ActiveShot?.CreateShapeLayer(LastShapeType);
                    Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandShapeAdded, LastShapeType.ToString());
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });

            _insertShapeCommand = new RelayCommand<InkShapeType>(async t => 
            {
                try
                {
                    await AuthoringSession?.Project?.ActiveShot?.CreateShapeLayer(t);
                    Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandShapeAdded, t.ToString());
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });

            _importDataFromTouchcastCommand = new RelayCommand(() =>
            {
                //Write Code to insert data from touchcast
            });

            _copyLayerCommand = new RelayCommand(() =>
            {
                try
                {
                    CopyLayerLayoutView();
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });


            _cutLayerCommand = new RelayCommand(() =>
            {
                try
                {
                    CutLayerLayoutView();
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });

            _pasteLayerCommand = new RelayCommand(async () =>
            {
                try
                {
                    await PasteLayerLayoutView();
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });

            Shapes = new List<InkShapeType>()
            {
                InkShapeType.Line, 
                InkShapeType.Arrow,
                InkShapeType.Rectangle,
                InkShapeType.Circle,
            };
        }

#if DEBUG
        ~InsertToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("**************** InsertToolbarPanelViewModel Destructor****************");
        }
#endif
        #endregion

        #region Public Methods

        public override void Cleanup()
        {
            _insertMediaCommand = null;
            _insertWebViewCommand = null;
            _insertTextToolCommand = null;
            _insertShapeCommand = null;
            _importDataFromTouchcastCommand = null;
            _insertLastAddedScene = null;
            _insertLastAddedShape = null;
            _copyLayerCommand = null;
            _cutLayerCommand = null;
            _pasteLayerCommand = null;
            _insertFileCommand = null;
            base.Cleanup();
        }

        public bool CopyLayerLayoutView()
        {
            var result = false;
            AuthoringSession authoringSession = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (authoringSession != null)
            {
                BaseLayoutView currentLayer = authoringSession.ShotManager.ActiveShotView.LayoutViews.FirstOrDefault(v => v.LayoutViewState == LayoutViewState.SelectedState);
                bool canCopyLayout = currentLayer != null && !currentLayer.ViewModel.IsStatic;
                if (canCopyLayout)
                {
                    AuthoringSession.Project.CopyLayer(currentLayer.ViewModel);
                    CanPaste = true;
                    result = true;
                }
            }
            return result;
        }

        public bool CutLayerLayoutView()
        {
            var result = false;
            AuthoringSession authoringSession = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (authoringSession != null)
            {
                BaseLayoutView currentLayer = authoringSession.ShotManager.ActiveShotView.LayoutViews.FirstOrDefault(v => v.LayoutViewState == LayoutViewState.SelectedState);
                bool canCutLayout = currentLayer != null && !currentLayer.ViewModel.IsStatic;
                if (canCutLayout)
                {
                    AuthoringSession.Project.CutLayer(currentLayer.ViewModel);
                    CanPaste = true;
                    result = true;
                }
            }
            return result;
        }

        public async Task<bool> PasteLayerLayoutView()
        {
            var result = false;
            if (AuthoringSession.Project.BufferedLayer != null)
            {
                DataLayer.Layouts.LayerLayout layer = AuthoringSession.Project.BufferedLayer.Clone();
                await layer.CopyDataToProjectFolder();
                AuthoringSession.Project.PasteLayer(layer);
                result = true;
            }
            return result;
        }

        #endregion
    }
}
