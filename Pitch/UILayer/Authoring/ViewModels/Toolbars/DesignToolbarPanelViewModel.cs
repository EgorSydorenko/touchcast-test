﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class DesignToolbarPanelViewModel : ViewModelBase
    {
        #region Private Fields
        private RelayCommand _setAsBackgroundCommand;
        private RelayCommand<Color> _solidColorBackgroundCommand;
        private RelayCommand _fileAsBackgroundCommand;
        private RelayCommand _mediaAsBackgroundCommand;
        private WeakReference<AuthoringSession> _session;
        private BaseLayoutView _view;
        private object _lockObject = new object();
        private bool _isSetAsBackgroundEnabled;
        private bool _isCommandExecuted;
        private Brush _selectedColor;
        private AuthoringSession AuthoringSession => _session?.TryGetObject();

        #endregion

        #region Properties

        public ICommand SetAsBackgroundCommand => _setAsBackgroundCommand;
        public ICommand SolidColorBackgroundCommand => _solidColorBackgroundCommand;
        public ICommand FileAsBackgroundCommand => _fileAsBackgroundCommand;
        public ICommand MediaAsBackgroundCommand => _mediaAsBackgroundCommand;

        public bool IsSetAsBackgroundEnabled
        {
            get => _isSetAsBackgroundEnabled;
            set => Set(ref _isSetAsBackgroundEnabled, value);
        }

        public Brush SelectedColor
        {
            get => _selectedColor;
            set => Set(ref _selectedColor, value);
        }

        #endregion

        #region Life Cycle
        public DesignToolbarPanelViewModel()
        {
            var authoringSession = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (authoringSession != null)
            {
                _session = new WeakReference<AuthoringSession>(authoringSession);
                authoringSession.AuthroingSessionWasInitialized += AuthoringSession_AuthroingSessionWasInitialized;
                authoringSession.AuthoringSessionWillBeCleaned += Session_AuthoringSessionWillBeCleaned;

                if (authoringSession.Project?.ActiveShot?.BackgroundColor != null)
                {
                    SelectedColor = new SolidColorBrush(authoringSession.Project.ActiveShot.BackgroundColor);
                }
            }
            _setAsBackgroundCommand = new RelayCommand(() =>
            {
                try
                {
                    SetAsBackground();
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
            _solidColorBackgroundCommand = new RelayCommand<Color>(c =>
            {
                try
                {
                    SolidColorAsBackground(c);
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
            _fileAsBackgroundCommand = new RelayCommand(async () =>
            {
                try
                { 
                    if (_isCommandExecuted) return;

                    _isCommandExecuted = true;
                    var extensions = new List<string>(FileToAppHelper.ImageExtensions);
                    extensions.AddRange(FileToAppHelper.VideoExtensions);
                    extensions.AddRange(FileToAppHelper.DocumentExtensions);
                    var file = await FileToAppHelper.SelectFile(extensions.ToArray());
                    await SetFileAsBackground(file);

                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
                finally
                {
                    _isCommandExecuted = false;
                }
            });

            _mediaAsBackgroundCommand = new RelayCommand(async () =>
            {
                try
                {
                    if (_isCommandExecuted) return;
                    _isCommandExecuted = true;

                    StockMediaSdk.ApiClient.Initialize(App.AppSettings.GetString(App.VideoBlocksPublicKey), App.AppSettings.GetString(App.VideoBlocksPrivateKey));
                    var downloadedFiles = await StockMediaSdk.ApiClient.Shared.ShowSearchDialogAsync(
                        StockMediaSdk.Data.Model.MediaType.Video | StockMediaSdk.Data.Model.MediaType.Photo,
                        ApplicationData.Current.LocalCacheFolder, true);

                    await SetFileAsBackground(downloadedFiles.FirstOrDefault());
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
                finally
                {
                    _isCommandExecuted = false;
                }
            });
        }

        public override void Cleanup()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized -= AuthoringSession_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned -= Session_AuthoringSessionWillBeCleaned;

                session.ShotManager.ShotWasDisplayed -= ShotManager_ShotWasDisplayed;
                session.ShotManager.ShotWillBeHidden -= ShotManager_ShotWillBeHidden;
                session.Project.ActiveShot.UpdateShotViewEvent -= Shot_UpdateShotViewEvent;
            }

            _setAsBackgroundCommand = null;
            _solidColorBackgroundCommand = null;
            _fileAsBackgroundCommand = null;
            _mediaAsBackgroundCommand = null;
            base.Cleanup();
        }

#if DEBUG
        ~DesignToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("**************** DesignToolbarPanelViewModel Destructor****************");
        }
#endif
        #endregion

        #region Callbacks

        private async Task AuthoringSession_AuthroingSessionWasInitialized()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.ShotManager != null)
                {
                    session.ShotManager.ShotWasDisplayed += ShotManager_ShotWasDisplayed;
                    session.ShotManager.ShotWillBeHidden += ShotManager_ShotWillBeHidden;
                }

                if (session.Project?.ActiveShot != null)
                {
                    session.Project.ActiveShot.UpdateShotViewEvent += Shot_UpdateShotViewEvent;
                }
            }
        }

        private async Task Session_AuthoringSessionWillBeCleaned()
        {
            var session = AuthoringSession;
            if (session.Project?.ActiveShot != null)
            {
                session.Project.ActiveShot.UpdateShotViewEvent -= Shot_UpdateShotViewEvent;
            }
        }

        private async Task ShotManager_ShotWillBeHidden(DataLayer.Shots.Shot model)
        {
            model.UpdateShotViewEvent -= Shot_UpdateShotViewEvent;
        }

        private async Task ShotManager_ShotWasDisplayed(DataLayer.Shots.Shot model)
        {
            UpdateColorSelector(model.BackgroundColor);
            model.UpdateShotViewEvent += Shot_UpdateShotViewEvent;
        }

        #endregion

        #region Private Methods

        private void Shot_UpdateShotViewEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                UpdateColorSelector(session.Project.ActiveShot.BackgroundColor);
            }
        }

        public void Update(BaseLayoutView view)
        {
            lock (_lockObject)
            {
                _view = view;
                UpdateButtonEnable();
            }
        }

        private void SetAsBackground()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.ActiveShot.SetAsBackground(_view.ViewModel);
                UpdateButtonEnable();
            }
        }

        private async Task SolidColorAsBackground(Color color)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.ActiveShot.UpdateBackgroundColor(color);
                UpdateButtonEnable();
            }
        }

        private async Task SetFileAsBackground(StorageFile file)
        {
            var session = AuthoringSession;
            if (session != null && file != null)
            {
                MessengerInstance.Send(new BusyIndicatorShowMessage());
                bool isShowMessage = await session.Project.ActiveShot.CreateLayerAsBackground(file);
                UpdateButtonEnable();
                MessengerInstance.Send(new BusyIndicatorHideMessage());

                if (isShowMessage) MessengerInstance.Send(new ShowUnsupportedFormatMessage());
            }
        }

        private void UpdateButtonEnable()
        {
            IsSetAsBackgroundEnabled = (_view != null && !(_view is LineLayerLayoutView)
                && !_view.ViewModel.IsBackground);
        }

        private void UpdateColorSelector(Color color)
        {
            SelectedColor = new SolidColorBrush(color);
        }

        #endregion
    }
}
