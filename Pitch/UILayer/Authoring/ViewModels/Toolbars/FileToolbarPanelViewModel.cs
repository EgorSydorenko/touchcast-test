﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TouchCast.DataLayer;
using TouchCast.Helpers;
using TouchCast.UILayer.Helpers.Messages;
using Windows.UI.Popups;

namespace TouchCast.UILayer.Authoring.ViewModels.Toolbars
{
    public class FileToolbarPanelViewModel : ViewModelBase
    {
        #region Private Fields
        private const string SaveProjectDialogContent = "Would you like to save existing project before creating the new one?";
        private const string SaveProjectDialogTitle = "The current project is not saved";
        private const string SaveProjectDialogSaveLabel = "Save";
        private const string SaveProjectDialogDoNotSaveLabel = "Don't Save";
        private const string SaveProjectDialogCancelLabel = "Cancel";

        private bool _isSaveButtonEnabled = true;
        private bool _isCommandExecuted;
        private ICommand _startRecordWithCameraCommand;
        private ICommand _startRecordWithVideoCommand;
        private ICommand _saveVappsCommand;
        private ICommand _saveAsVappsCommand;
        private ICommand _exportCommand;
        private ICommand _newProjectCommand;
        private WeakReference<AuthoringSession> _authoringSession;

        private AuthoringSession AuthoringSession
        {
            get
            {
                AuthoringSession session;
                if (_authoringSession.TryGetTarget(out session))
                {
                    return session;
                }
                return null;
            }
        }
        #endregion

        public bool IsSaveButtonEnabled
        {
            get
            {
                return _isSaveButtonEnabled;
            }
            set
            {

            }
        }

        public ICommand StartRecordWithCameraCommand => _startRecordWithCameraCommand;
        public ICommand StartRecordWithVideoCommand => _startRecordWithVideoCommand;
        public ICommand OpenProjectCommand { get; private set; }
        public ICommand SaveVappsCommand => _saveVappsCommand;
        public ICommand SaveAsVappsCommand => _saveAsVappsCommand;
        public ICommand ExportCommand => _exportCommand;
        public ICommand NewProjectCommand => _newProjectCommand;

        #region Life Cycle
        public FileToolbarPanelViewModel()
        {
            var sesion = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(sesion);

            _startRecordWithCameraCommand = new RelayCommand(() => { 
            });

            _startRecordWithVideoCommand = new RelayCommand(() => {
            });

            OpenProjectCommand = new RelayCommand(async () =>
            {
                await OpenProject();
            });

            _saveVappsCommand = new RelayCommand(async () => 
            {
                await SaveProject();
            });

            _saveAsVappsCommand = new RelayCommand(async () => 
            {
                await SaveAsProject();
            });

            _exportCommand = new RelayCommand(() =>
            {

            });

            _newProjectCommand = new RelayCommand(async () =>
            {
                if (await ShowSaveProjectDialog())
                {
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        await session.OpenNew(ProjectTemplate.DefaultTemplate());
                    }
                }
            });
        }

#if DEBUG
        ~FileToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("****************FileToolbarPanelViewModel Destructor****************");
        }
#endif

        public void Subscribe()
        {
            Messenger.Default.Register<OpenNewProjectMessage>(this, async (m) =>
            {
                if (await ShowSaveProjectDialog())
                {
                    //App.Locator.ProjectFile = m.FileProject;
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        MessengerInstance.Send(new BusyIndicatorShowMessage());
                        var file = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(m.FileTocken);
                        await session.Open(file);
                        //await Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                        //{
                        //    await session.Open(App.Locator.ProjectFile);
                        //});
                        MessengerInstance.Send(new BusyIndicatorHideMessage());
                    }
                }
            });
            //Messenger.Default.Register<SaveProjectIfNeededMessage>(this, async msg => //TODO
            //{
            //    await SaveProjectIfNeeded();
            //});
        }

        public override void Cleanup()
        {
            Messenger.Default.Unregister(this);
            _startRecordWithCameraCommand = null;
            _startRecordWithVideoCommand = null;
            OpenProjectCommand = null;
            _saveVappsCommand = null;
            _saveAsVappsCommand = null;
            _exportCommand = null;
            _newProjectCommand = null;
            base.Cleanup();
        }
        #endregion

        #region Private Methods
        private async Task OpenProject()
        {
            if (_isCommandExecuted) return;
            try
            {
                _isCommandExecuted = true;

                if (await ShowSaveProjectDialog())
                {
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        var file = await FileToAppHelper.SelectFile(FileToAppHelper.TouchcastProjectExtension);
                        if (file != null)
                        {
                            MessengerInstance.Send(new BusyIndicatorShowMessage());
                            await session.Open(file);
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TouchCast.Helpers.Logger.LogQueue.WriteToFile($"Exception in FileToolbarPanelViewModel.OpenProject : {ex.Message}");
            }
            finally
            {
                MessengerInstance.Send(new BusyIndicatorHideMessage());
                _isCommandExecuted = false;
            }
            
        }

        private async Task SaveProject()
        {
            if (_isCommandExecuted) return;
            
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project.IsTemporary)
                {
                    await SaveAsProject();
                }
                else
                {
                    try
                    {
                        _isCommandExecuted = true;
                        MessengerInstance.Send(new BusyIndicatorShowMessage());
                        await session.Project.Save();
                    }
                    catch(Exception ex)
                    {
                        TouchCast.Helpers.Logger.LogQueue.WriteToFile($"Exception in FileToolbarPanelViewModel.SaveProject : {ex.Message}");
                    }
                    finally
                    {
                        MessengerInstance.Send(new BusyIndicatorHideMessage());
                        _isCommandExecuted = false;
                    }
                }
            }
            
        }

        private async Task SaveAsProject()
        {
            if (_isCommandExecuted) return;
            try
            {
                _isCommandExecuted = true;

                var session = AuthoringSession;
                if (session != null)
                {
                    var file = await FileToAppHelper.SaveFileDialog(FileToAppHelper.TouchcastProjectExtension, "touchcast project file", Project.DefaultProjectName);
                    if (file != null)
                    {
                        MessengerInstance.Send(new BusyIndicatorShowMessage());
                        await session.Project.SaveAs(file);
                    }
                }
            }
            catch (Exception ex)
            {
                TouchCast.Helpers.Logger.LogQueue.WriteToFile($"Exception in FileToolbarPanelViewModel.SaveAsProject : {ex.Message}");
            }
            finally
            {
                MessengerInstance.Send(new BusyIndicatorHideMessage());
                _isCommandExecuted = false;
            }
        }

        private async Task SaveProjectIfNeeded()
        {
            bool result = true;
            var session = AuthoringSession;
            if (session != null &&  session.Project.HasUnsavedChanges)
            {
                result = await ShowSaveProjectDialog();
            }
            //Messenger.Default.Send(new SaveProjectIfNeededDone(result)); //TODO
        }

        private async Task<bool> ShowSaveProjectDialog()
        {
            var authSession = AuthoringSession;
            if (authSession != null)
            {
                if (authSession.Project.HasUnsavedChanges)
                {
                    var saveProjectDialog = new MessageDialog(SaveProjectDialogContent, SaveProjectDialogTitle);
                    saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogSaveLabel));
                    saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogDoNotSaveLabel));
                    saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogCancelLabel));
                    var result = await saveProjectDialog.ShowAsync();
                    if (result.Label == SaveProjectDialogSaveLabel)
                    {
                        var fileToSave = await FileToAppHelper.SaveFileDialog(FileToAppHelper.TouchcastProjectExtension, "Touchcast project", authSession.Project.Name);
                        if (fileToSave != null)
                        {
                            await authSession.Project.SaveAs(fileToSave);
                        }
                    }
                    if (result.Label == SaveProjectDialogCancelLabel)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion
    }
}
