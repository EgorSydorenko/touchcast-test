﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Diagnostics;
using System.Windows.Input;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class RecordToolbarViewModel : ViewModelBase
    {
        public RecordToolbarViewModel()
        {
            StartRecordingCommand = new RelayCommand(() =>
            {
                try
                {
                    App.Locator.RegisterRecordingManager();
                    App.Frame.Navigate(typeof(Recording.Views.RecordingScreenPage));
                    App.Frame.BackStack.Clear();
                    App.Frame.ForwardStack.Clear();
                    App.Frame.CacheSize = 0;
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
        });

            NavigateToEditCommand = new RelayCommand(() =>
            {
            });
        }

#if DEBUG
        ~RecordToolbarViewModel()
        {
            Debug.WriteLine("********************RecordToolbarViewModel Destructor********************");
        }
#endif

        public override void Cleanup()
        {
            StartRecordingCommand = null;
            NavigateToEditCommand = null;
            base.Cleanup();
        }

        public ICommand StartRecordingCommand { get; private set; }

        public ICommand NavigateToEditCommand { set; get; }
    }
}
