﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class ViewToolbarPanelViewModel : ViewModelBase
    {
        #region Private Fields

        private WeakReference<AuthoringSession> _authoringSession;
        private bool _isShowGuideLines;
        #endregion

        #region Properties

        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        public bool IsShowGuideLines
        {
            get
            {
                return _isShowGuideLines;
            }
            set
            {
                Set(ref _isShowGuideLines, value);
                var session = AuthoringSession;
                if (session != null)
                {
                    session.ShotManager.ActiveShotView.IsShowGuideLines = value;
                }
            }
        }

        #endregion

        #region Life Cycle

        public ViewToolbarPanelViewModel()
        {
            _authoringSession = new WeakReference<AuthoringSession>(SimpleIoc.Default.GetInstance<AuthoringSession>());
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized += Session_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned += Session_AuthoringSessionWillBeCleaned;
            }
        }

#if DEBUG
        ~ViewToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("****************ViewToolbarPanelViewModel Destructor****************");
        }
#endif
        #endregion

        #region Public Methods

        public override void Cleanup()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.AuthroingSessionWasInitialized -= Session_AuthroingSessionWasInitialized;
                session.AuthoringSessionWillBeCleaned -= Session_AuthoringSessionWillBeCleaned;
                if (session.Project != null)
                {
                    session.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                }
            }
            _authoringSession = null;
            base.Cleanup();
        }

        #endregion

        #region Callbacks

        private Task Session_AuthroingSessionWasInitialized()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (authoringSession.Project != null)
                {
                    authoringSession.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                    IsShowGuideLines = authoringSession.ShotManager.ActiveShotView.IsShowGuideLines;
                }
            }

            return Task.CompletedTask;
        }

        private Task Session_AuthoringSessionWillBeCleaned()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (authoringSession.Project != null)
                {
                    authoringSession.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                }
            }

            return Task.CompletedTask;
        }

        private void Project_ActiveShotWasChanged()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                IsShowGuideLines = session.ShotManager.ActiveShotView.IsShowGuideLines;
            }
        }

        #endregion
    }
}
