﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Pitch.DataLayer;
using Pitch.Services;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class CameraToolbarPanelViewModel : ViewModelBase
    {
        #region Private Fields
        private bool _isGreenScreenOn;
        private string _greenScreenText;
        private double _sensitivity;
        private double _smoothness;
        private ICommand _cameraToolBarSwithChromaKeyCommand;
        private WeakReference<AuthoringSession> _authoringSession;
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        private WeakReference<IMediaMixerService> _mediaMixerService;
        private IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();
        private Brush _labelColor;

        #endregion

        #region Life Cycle

        public CameraToolbarPanelViewModel()
        {
            var authoringSession = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(authoringSession);
            authoringSession.AuthoringSessionWillBeCleaned += AuthoringSession_AuthoringSessionWillBeCleaned;
            authoringSession.AuthroingSessionWasInitialized += AuthoringSession_AuthroingSessionWasInitialized;

            UpdateChromaKeyValues();

            _cameraToolBarSwithChromaKeyCommand = new RelayCommand(() =>
            {
                try
                {
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        session.Project.ActiveShot.GreenScreenSettings.IsColorPickerOn = !session.Project.ActiveShot.GreenScreenSettings.IsColorPickerOn;
                        session.Project.ActiveShot.GreenScreenSettings.IsOn = !session.Project.ActiveShot.GreenScreenSettings.IsOn;
                    }
                }
                catch (Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
           
            var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            _mediaMixerService = new WeakReference<IMediaMixerService>(mixer);
        }

        private Task AuthoringSession_AuthroingSessionWasInitialized()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                UpdateChromaKeyValues();
            }

            return Task.CompletedTask;
        }

        private Task AuthoringSession_AuthoringSessionWillBeCleaned()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (authoringSession.Project != null)
                {
                    authoringSession.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                }
            }

            return Task.CompletedTask;
        }

        private void Project_ActiveShotWasChanged()
        {
            UpdateChromaKeyValues();
        }

#if DEBUG
        ~CameraToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("****************CameraToolbarPanelViewModel Destructor****************");
        }
#endif
        public override void Cleanup()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.AuthoringSessionWillBeCleaned -= AuthoringSession_AuthoringSessionWillBeCleaned;
                authoringSession.AuthroingSessionWasInitialized -= AuthoringSession_AuthroingSessionWasInitialized;
                if (authoringSession.Project != null)
                {
                    authoringSession.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                }
            }

            base.Cleanup();
            _cameraToolBarSwithChromaKeyCommand = null;
            _mediaMixerService = null;
            _authoringSession = null;
        }

        #endregion

        #region Public Properties

        public bool IsGreenScreenOn
        {
            set
            {
                if(_isGreenScreenOn != value)
                {
                    Set(ref _isGreenScreenOn, value);
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        session.Project.ActiveShot.GreenScreenSettings.IsOn = _isGreenScreenOn;
                    }
                }
                LabelColor = new SolidColorBrush(_isGreenScreenOn ? Colors.White : (Color)Application.Current.Resources["DisableButtonForegroundColor"]);
                GreenScreenText = _isGreenScreenOn ? "On" : "Off";
            }
            get
            {
                return _isGreenScreenOn;
            }
        }

        public string GreenScreenText
        {
            get
            {
                return _greenScreenText;
            }
            set
            {
                Set(ref _greenScreenText, value);
            }
        }

        public double Sensitivity
        {
            set
            {
                if (_sensitivity != value)
                {
                    Set(ref _sensitivity, value);
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        session.Project.ActiveShot.GreenScreenSettings.Sensetivity = _sensitivity / 1000;
                    }
                }
            }
            get
            {
                return _sensitivity;
            }
        }

        public double Smoothness
        {
            set
            {
                if (_smoothness != value)
                {
                    Set(ref _smoothness, value);
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        session.Project.ActiveShot.GreenScreenSettings.Smoothness = _smoothness / 1000;
                    }
                }
            }
            get
            {
                return _smoothness;
            }
        }

        public Color GreenScreenColor
        {
            get
            {
                Color result;
                var session = AuthoringSession;
                if (session != null && session.Project?.ActiveShot != null)
                {
                    result = session.Project.ActiveShot.GreenScreenSettings.Color;
                }
                return result;
            }
            set
            {
                var session = AuthoringSession;
                if (session != null && session.Project?.ActiveShot != null)
                {
                    session.Project.ActiveShot.GreenScreenSettings.Color = value;
                }
            }
        }

        public Brush LabelColor
        {
            get
            {
                return _labelColor;
            }
            set
            {
                Set(ref _labelColor, value);
            }
        }

        #region Commands
       
        public ICommand CameraToolBarSwithChromaKeyCommand => _cameraToolBarSwithChromaKeyCommand;

        #endregion

        #endregion

        #region Public Methods

        public void DisableColorPicker()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.ActiveShot.GreenScreenSettings.IsColorPickerOn = false;
                session.Project.ActiveShot.GreenScreenSettings.IsOn = true;
            }
        }

        #endregion

        #region Private Methods

        private void UpdateChromaKeyValues()
        {
            var session = AuthoringSession;
            if (session != null && session.Project?.ActiveShot != null)
            {
                Sensitivity = session.Project.ActiveShot.GreenScreenSettings.Sensetivity * 1000;
                Smoothness = session.Project.ActiveShot.GreenScreenSettings.Smoothness * 1000;
                IsGreenScreenOn = session.Project.ActiveShot.GreenScreenSettings.IsOn;
            }
        }
        #endregion
    }
}
