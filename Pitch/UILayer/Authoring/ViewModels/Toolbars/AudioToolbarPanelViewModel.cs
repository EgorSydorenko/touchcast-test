﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.ComponentModel;
using Pitch.Services;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.UILayer.Authoring.ViewModels.Toolbars
{
    public class AudioToolbarPanelViewModel : ViewModelBase, INotifyPropertyChanged
    {
        #region Private Fields

        private WeakReference<IMediaMixerService> _mediaMixerService;
        private IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();

        private double _volumeLevel;

        #endregion

        #region Public Properties
       
        public double VolumeLevel
        {
            get
            {
                return _volumeLevel;
            }
            set
            {
                if (_volumeLevel != value)
                {
                    Set(ref _volumeLevel, value);
                    MediaMixer.MicrophoneVolumeLevel = value;
                }
            }
        }

        #endregion

        #region Life Cycle

        public AudioToolbarPanelViewModel()
        {
            var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            _mediaMixerService = new WeakReference<IMediaMixerService>(mixer);
        }

#if DEBUG
        ~AudioToolbarPanelViewModel()
        {
            System.Diagnostics.Debug.WriteLine("****************AudioToolbarPanelViewModel Destructor****************");
        }
#endif

        public override void Cleanup()
        {
            base.Cleanup();
            _mediaMixerService = null;
        }

        #endregion

        #region Public Methods
        
        public void UpdateVolumeLevel()
        {
            if (MediaMixer != null)
            {
                VolumeLevel = MediaMixer.MicrophoneVolumeLevel;
            }
        }

        #endregion
    }
}
