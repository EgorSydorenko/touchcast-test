﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using TouchCast.DataLayer;
using TouchCast.DataLayer.Layouts;
using TouchCast.DataLayer.Shots;
using TouchCast.Services;
using Windows.Foundation;

namespace TouchCastUWP.UILayer.Authoring.ViewModels.Shots
{
    public class NewShotViewModel : ViewModelBase
    {
        #region Private Static
        private readonly static Rect FullScreenRectangle = new Rect(0, 0, MediaMixerService.VideoSize.Width, MediaMixerService.VideoSize.Height);
        private readonly static Rect LeftHalfScreenRectangle = new Rect(0, 0, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height);
        private readonly static Rect RightHalfScreenRectangle = new Rect(MediaMixerService.VideoSize.Width / 2, 0, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height);
        private readonly static Rect LeftTopCornerRectangle = new Rect(0, 0, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2);
        private readonly static Rect RightTopCornerRectangle = new Rect(MediaMixerService.VideoSize.Width / 2, 0, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2);
        private readonly static Rect LeftBottomCornerRectangle = new Rect(0, MediaMixerService.VideoSize.Height / 2, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2);
        private readonly static Rect RightBottomCornerRectangle = new Rect(MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2);
        private readonly static Rect CenterBottomRectangle = new Rect(MediaMixerService.VideoSize.Width / 4, MediaMixerService.VideoSize.Height / 2, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2);
        private readonly static Rect LeftBottomCornerSmallRectangle = new Rect(0, MediaMixerService.VideoSize.Height * 2 / 3, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 3);
        private readonly static Rect RightTopCornerSmallRectangle = new Rect(MediaMixerService.VideoSize.Width / 2, 20, MediaMixerService.VideoSize.Width / 2 - 20, MediaMixerService.VideoSize.Height / 3);
        private readonly static Rect RightTopCornerOffsetRectangle = new Rect(MediaMixerService.VideoSize.Width / 2 - 20, 20, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 2);
        private readonly static Rect RightBottomCornerTextLabelRectantle = new Rect(MediaMixerService.VideoSize.Width / 2 - 20, MediaMixerService.VideoSize.Height / 2 + 50, MediaMixerService.VideoSize.Width / 2, MediaMixerService.VideoSize.Height / 4);
        private readonly static Rect CenterRectangle = new Rect(MediaMixerService.VideoSize.Width / 2.0 - MediaMixerService.VideoSize.Width / 3.0, MediaMixerService.VideoSize.Height / 2 - MediaMixerService.VideoSize.Height / 6.0, 2.0 * MediaMixerService.VideoSize.Width / 3, MediaMixerService.VideoSize.Height / 3.0);

        private readonly static LayerLayoutTemplate FullScreenStaticCamera = LayerLayoutTemplate.GenerateCustomTemplate(
            LayerLayoutTemplate.MixerType,
            FullScreenRectangle, true, false, false, false, false, true
        );

        private readonly static LayerLayoutTemplate LeftHalfScreenStaticCamera = LayerLayoutTemplate.GenerateCustomTemplate(
            LayerLayoutTemplate.MixerType,
            LeftHalfScreenRectangle, true, false, false, false, false, true
        );

        private readonly static LayerLayoutTemplate RightHalfScreenStaticCamera = LayerLayoutTemplate.GenerateCustomTemplate(
            LayerLayoutTemplate.MixerType,
            RightHalfScreenRectangle, true, false, false, false, false, true
        );

        private readonly List<ShotTemplate> _shotTemplates = new List<ShotTemplate>()
        {
            new ShotTemplate(new List<LayerLayoutTemplate>(){ FullScreenStaticCamera } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ FullScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftTopCornerRectangle, false, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ FullScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightTopCornerRectangle, false, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ FullScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftBottomCornerRectangle, false , false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ FullScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightBottomCornerRectangle, false, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ LeftHalfScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightTopCornerRectangle, true, false, false, true, true, false ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightBottomCornerRectangle, true, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ RightHalfScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftTopCornerRectangle, true, false, false, true, true, false ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftBottomCornerRectangle, true, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ RightHalfScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftHalfScreenRectangle, true, false, false,  true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ LeftHalfScreenStaticCamera,
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightHalfScreenRectangle, true, false, false,  true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){
                LayerLayoutTemplate.GenerateCustomTemplate(
                    LayerLayoutTemplate.PlaceHolderType, FullScreenRectangle, true, false, false, true, true, true ),
                LayerLayoutTemplate.GenerateCustomTemplate(
                    LayerLayoutTemplate.MixerType, LeftBottomCornerRectangle, false, false, false, false, false, false)}),
            new ShotTemplate(new List<LayerLayoutTemplate>(){ 
                LayerLayoutTemplate.GenerateCustomTemplate(
                    LayerLayoutTemplate.PlaceHolderType, FullScreenRectangle, true, false, false, true, true, true ),
                LayerLayoutTemplate.GenerateCustomTemplate(
                    LayerLayoutTemplate.MixerType, RightBottomCornerRectangle, false, false, false, false, false, false)
            }),
            new ShotTemplate(new List<LayerLayoutTemplate>(){LayerLayoutTemplate.GenerateCustomTemplate(
                    LayerLayoutTemplate.PlaceHolderType, FullScreenRectangle, true, false, false, true, true, true )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftTopCornerRectangle, true, false, false, true, true, true ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftBottomCornerRectangle, true, false, false, true, true, false ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightTopCornerRectangle, true, false, false, true, true, false),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightBottomCornerRectangle, true, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftHalfScreenRectangle, true, false, false, true, true, true ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightTopCornerRectangle, true, false, false, true, true, false ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightBottomCornerRectangle, true, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftTopCornerRectangle, true, false, false, true, true, true ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftBottomCornerRectangle, true, false, false, true, true, false ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightHalfScreenRectangle, true, false, false, true, true, false )
            } ),
            new ShotTemplate(new List<LayerLayoutTemplate>(){
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, LeftHalfScreenRectangle, true, false, false, true, true, true ),
                    LayerLayoutTemplate.GenerateCustomTemplate(
                        LayerLayoutTemplate.PlaceHolderType, RightHalfScreenRectangle, true, false, false, true, true, false ),
                LayerLayoutTemplate.GenerateCustomTemplate(
                    LayerLayoutTemplate.MixerType, CenterBottomRectangle, false, false, false, true, true, false)
            }),
        };

        #endregion

        #region Private Fields
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private WeakReference<AuthoringSession> _session;
        private IMediaMixerService MediaMixer
        {
            get
            {
                IMediaMixerService mixer;
                if (_mediaMixerService.TryGetTarget(out mixer))
                {
                    return mixer;
                }
                return null;
            }
        }
        private AuthoringSession AuthoringSession
        {
            get
            {
                AuthoringSession session;
                if (_session.TryGetTarget(out session))
                {
                    return session;
                }
                return null;
            }
        }
        private ICommand _insertNewLayerCommand;
        #endregion

        #region Public Properties

        public ICommand InsertNewLayerCommand => _insertNewLayerCommand;

        #endregion

        #region Life Cycle

        public NewShotViewModel()
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _session = new WeakReference<AuthoringSession>(session);

            var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            if (mixer != null)
            {
                _mediaMixerService = new WeakReference<IMediaMixerService>(mixer);
            }

            InitTextTemplates();

            _insertNewLayerCommand = new RelayCommand<int>(async (i) =>
            {
                if (i > -1 && i < 22)
                {
                    await AuthoringSession.Project.CreateShot(_shotTemplates[i]);
                    await AuthoringSession.ShotManager.ShowShot(AuthoringSession.Project.Shots.Last());
                }
            });
        }

        public override void Cleanup()
        {
            _insertNewLayerCommand = null;
            base.Cleanup();
        }

        #endregion

        #region Private Methods

        private void InitTextTemplates()
        {
            LayerLayoutTemplate FullScreenText = LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.TextType, CenterRectangle, false, false, false, false, false, false);
            FullScreenText.Text = @"{\rtf1\fbidis\ansi\ansicpg1252\deff0\nouicompat\deflang1033\deflangfe2052{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}{\f1\fnil\fcharset0 Segoe UI;}}
{\colortbl ;\red255\green255\blue255;\red128\green128\blue128;}
{\*\generator Riched20 10.0.16299}{\*\mmathPr\mdispDef1\mwrapIndent1440 }\viewkind4\uc1 
\pard\widctlpar\sl276\slmult1\qc\cf1\f0\fs72 Lorem Ipsum \f1 Dolor\par
\cf2\highlight1 Lorem Ipsum Dolor\par
}
";
            _shotTemplates.Add(new ShotTemplate(new List<LayerLayoutTemplate>() { FullScreenText }));

            LayerLayoutTemplate LeftBottomText = LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.TextType, LeftBottomCornerSmallRectangle, false, false, false, false, false, false);
            LeftBottomText.Text = @"{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang1033\deflangfe2052{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}}
{\colortbl ;\red255\green255\blue255;\red128\green128\blue128;\red0\green0\blue0;}
{\*\generator Riched20 10.0.16299}{\*\mmathPr\mdispDef1\mwrapIndent1440 }\viewkind4\uc1 
\pard\widctlpar\sl276\slmult1\cf1\f0\fs72 Lorem Ipsum Dolor\par
\cf2 Lorem Ipsum Dolor\par
\cf3\par
}";
            _shotTemplates.Add(new ShotTemplate(new List<LayerLayoutTemplate>() { FullScreenStaticCamera, LeftBottomText }));

            LayerLayoutTemplate RightTopCornerText = LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.TextType, RightTopCornerRectangle, false, false, false, false, false, false);
            RightTopCornerText.Text = @"{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang1033\deflangfe2052{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}{\f1\fswiss\fprq2\fcharset0 Arial;}{\f2\fnil\fcharset2 Symbol;}}
{\colortbl ;\red255\green255\blue255;\red0\green0\blue0;}
{\*\generator Riched20 10.0.16299}{\*\mmathPr\mdispDef1\mwrapIndent1440 }\viewkind4\uc1 
\pard\widctlpar\sl276\slmult1\cf1\f0\fs72 Lorem Ipsum Dolor\par

\pard{\pntext\f2\'B7\tab}{\*\pn\pnlvlblt\pnf2\pnindent360{\pntxtb\'B7}}\fi-360\li720\sl276\slmult1\tx360\tx720 Lorem Ipsum\par
{\pntext\f2\'B7\tab}Lorem Ipsum\par
{\pntext\f2\'B7\tab}Lorem Ipsum\par

\pard\widctlpar\sl276\slmult1\cf2\f1\par
}";
            _shotTemplates.Add(new ShotTemplate(new List<LayerLayoutTemplate>() { FullScreenStaticCamera, RightTopCornerText}));

            LayerLayoutTemplate RightTopSmallCornetText = LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.TextType, RightTopCornerSmallRectangle, false, false, false, false, false, false);
            RightTopSmallCornetText.Text = @"{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang1033\deflangfe2052{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}{\f1\fnil\fcharset2 Symbol;}}
{\colortbl ;\red255\green255\blue255;}
{\*\generator Riched20 10.0.16299}{\*\mmathPr\mdispDef1\mwrapIndent1440 }\viewkind4\uc1 
\pard{\pntext\f1\'B7\tab}{\*\pn\pnlvlblt\pnf1\pnindent360{\pntxtb\'B7}}\fi-360\li720\sl276\slmult1\tx360\tx720\cf1\f0\fs72 Lorem Ipsum\par
{\pntext\f1\'B7\tab}Lorem Ipsum\par
{\pntext\f1\'B7\tab}Lorem Ipsum\par
}";
            _shotTemplates.Add(new ShotTemplate(new List<LayerLayoutTemplate>() { FullScreenStaticCamera, RightTopSmallCornetText }));

            LayerLayoutTemplate BottomLayerText = LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.TextType, RightBottomCornerTextLabelRectantle, false, false, false, false, false, false);
            BottomLayerText.Text = @"{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang1033\deflangfe2052{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}}
{\colortbl ;\red255\green255\blue255;}
{\*\generator Riched20 10.0.16299}{\*\mmathPr\mdispDef1\mwrapIndent1440 }\viewkind4\uc1 
\pard\widctlpar\sl276\slmult1\qc\cf1\f0\fs72 Lorem Ipsum Dolor\par
}";
            LayerLayoutTemplate PlaceHolder = LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.PlaceHolderType, RightTopCornerOffsetRectangle, false, true, false, true, true, false);
            _shotTemplates.Add(new ShotTemplate(new List<LayerLayoutTemplate>() { FullScreenStaticCamera, PlaceHolder, BottomLayerText}));
        }

        #endregion
    }
}
