﻿using System;
using Windows.UI.Xaml.Data;

namespace Pitch.UILayer.Helpers.Converters
{
    public class TextAligmentToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value.ToString().Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }

    }
}
