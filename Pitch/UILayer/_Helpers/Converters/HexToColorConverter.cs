﻿using System;

namespace Pitch.UILayer.Helpers.Converters
{
    public class HexToColorConverter
    {
        public static Windows.UI.Color ConvertHexToArgbColor(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            return Windows.UI.Color.FromArgb(a, r, g, b);
        }
        public static Windows.UI.Color ConvertHexToRgbColor(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte r = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));

            return Windows.UI.Color.FromArgb(255, r, g, b);
        }
        public static string ConvertColorToHex(Windows.UI.Color color)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}",
                     color.A,
                     color.R,
                     color.G,
                     color.B);
        }
    }
}
