﻿using System;
using Windows.UI.Xaml.Data;

namespace Pitch.UILayer.Helpers.Converters
{
    public class PageOffsetConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            long num;
            if (value != null && long.TryParse(value.ToString(), out num))
                return (num + 1).ToString();
            return (value ?? string.Empty).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new System.NotImplementedException();
        }
    }
}
