﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Pitch.UILayer.IntroPage.Views;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Helpers.CustomControls
{
    public sealed partial class RoundaboutControl : UserControl
    {
        public delegate void SelectionChanged(ThemePreviewView sender);

        #region Private Fields
        private AsyncLock _lockObject = new AsyncLock(1);
        private double _itemWidth = 500;
        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private readonly Point ZeroPoint = new Point(0, 0);
        private readonly TimeSpan ScrollInterval = TimeSpan.FromSeconds(2);
        private const int MinimalInternalListSize = 4;
        private bool _isResizeByBorder;
        private List<ThemeManager.Theme> _dataModels;
        private double _horizontalOffset;
        private bool _isSecondResizedEvent;
        #endregion

        public event SelectionChanged SelectionChangedEvent;

        #region Life Cycle
        public RoundaboutControl()
        {
            InitializeComponent();
            _timer.Interval = ScrollInterval;

            Loaded += RoundaboutControl_Loaded;
            Unloaded += RoundaboutControl_Unloaded;
        }

        private void RoundaboutControl_Loaded(object sender, RoutedEventArgs e)
        {
            _timer.Tick += Timer_Tick;

            UpdateViewList();
            UpdateLayout();
            UpdateItemsSizes();
            UpdateCenteredItemPosition();
            SizeChanged += RoundaboutControl_SizeChanged;
            StartTimer();
        }

        private void RoundaboutControl_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= RoundaboutControl_Loaded;
            Unloaded -= RoundaboutControl_Unloaded;
            
            _timer.Stop();
            _timer.Tick -= Timer_Tick;

            foreach (ThemePreviewView view in ScrollContent.Children)
            {
                view.ViewSelectedEvent -= View_ViewSelectedEvent;
            }
            ScrollContent.Children.Clear();
        }
        #endregion

        public void UpdateData(List<ThemeManager.Theme> data)
        {
            _dataModels = data;
        }

        #region Callbacks

        private async void Timer_Tick(object sender, object e)
        {
            await _lockObject.WaitAsync();
            LogQueue.WriteToFile($"Timer_Tick HorizontalOffset = {ScrollViewer.HorizontalOffset} : {_horizontalOffset}");

            ScrollViewer.ChangeView(_horizontalOffset + _itemWidth, null, null, false);
            await UpdateListTail();
            _horizontalOffset += _itemWidth;
            _lockObject.Release();
        }

        private async void Previouse_Click(object sender, RoutedEventArgs e)
        {
            await _lockObject.WaitAsync();
            ScrollViewer.ChangeView(_horizontalOffset - _itemWidth, null, null, false);
            await UpdateListHead();
            _horizontalOffset -= _itemWidth;
            _lockObject.Release();
        }

        private async void Next_Click(object sender, RoutedEventArgs e)
        {
            await _lockObject.WaitAsync();
            ScrollViewer.ChangeView(_horizontalOffset + _itemWidth, null, null, false);
            await UpdateListTail();
            _horizontalOffset += _itemWidth;
            _lockObject.Release();
        }
        private void Children_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Controls.Visibility = Visibility.Visible;
            _timer.Stop();
        }
        private void Children_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            Controls.Visibility = Visibility.Collapsed;
            StartTimer();
        }
        private void View_ViewSelectedEvent(ThemePreviewView sender)
        {
            SelectionChangedEvent?.Invoke(sender);
        }
        private void RoundaboutControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateItemsSizes();
        }
        #endregion

        #region Private methods
        private async Task UpdateListHead()
        {
            var item = ScrollContent.Children.LastOrDefault();
            if (item != null)
            {
                var x = ItemPosition(item);
                if (x > ScrollViewer.ActualWidth)
                {
                    await UpdateView(item, 0);
                    LogQueue.WriteToFile($"UpdateListHead HorizontalOffset = {ScrollViewer.HorizontalOffset} : {_horizontalOffset}");
                    _horizontalOffset += _itemWidth;
                    ScrollViewer.ChangeView(_horizontalOffset, null, null, true);
                }
            }
        }
        private async Task UpdateListTail()
        {
            var item = ScrollContent.Children.FirstOrDefault();
            if (item != null)
            {
                var x = ItemPosition(item);
                if (-x > _itemWidth)
                {
                    LogQueue.WriteToFile($"UpdateListTail HorizontalOffset = {ScrollViewer.HorizontalOffset} : {_horizontalOffset}");
                    _horizontalOffset -= _itemWidth;
                    ScrollViewer.ChangeView(_horizontalOffset, null, null, true);
                    await UpdateView(item, ScrollContent.Children.Count - 1);
                }
            }
        }

        private double ItemPosition(UIElement item)
        {
            var result = -Double.NaN;
            var transform = item.TransformToVisual(ScrollViewer);
            if (transform != null)
            {
                var pt = transform.TransformPoint(ZeroPoint);
                result = pt.X;
            }
            return result;
        }

        private void UpdateCenteredItemPosition()
        {
            var centerPosition = (ScrollViewer.ActualWidth - _itemWidth) / 2;

            FrameworkElement element = ScrollContent.Children.OfType<FrameworkElement>().FirstOrDefault(e => ItemPosition(e) >= centerPosition);
            if (element != null)
            {
                var x = ItemPosition(element);
                _horizontalOffset += (x - centerPosition);
                ScrollViewer.ChangeView(_horizontalOffset, null, null, true);
            }
        }
        private void UpdateItemsSizes()
        {
            var itemHeight = ActualHeight - Padding.Top - Padding.Bottom - 20;
            _itemWidth = itemHeight * 16 / 9;
            foreach (ThemePreviewView view in ScrollContent.Children)
            {
                view.Height = itemHeight;
                view.Width = _itemWidth;
            }
        }
        private void UpdateViewList()
        {
            var itemsNumber = (int)(ScrollViewer.ActualWidth / _itemWidth) + 4;

            if (itemsNumber < MinimalInternalListSize)
            {
                itemsNumber = MinimalInternalListSize;
            }

            if (ScrollContent.Children.Count >= itemsNumber)
            {
                return;
            }

            ScrollContent.Children.Clear();
            foreach (var theme in _dataModels)
            {
                var view = new ThemePreviewView(theme, true)
                {
                    IsTitleVisible = false,
                    Width = _itemWidth
                };
                view.ViewSelectedEvent += View_ViewSelectedEvent;
                ScrollContent.Children.Add(view);
                if (--itemsNumber == 0)
                {
                    break;
                }
            }
        }

        private async Task UpdateView(UIElement item, int position)
        {
            if (item is ThemePreviewView)
            {
                var previewItem = item as ThemePreviewView;
                int swapModelIndex = -1;
                if (position == 0)
                {
                    var firstView = ScrollContent.Children.FirstOrDefault();
                    if (firstView is ThemePreviewView)
                    {
                        var firstModelIndex = GetModelIndexForView(firstView as ThemePreviewView);
                        swapModelIndex = firstModelIndex - 1;
                    }
                }
                else
                {
                    var lastView = ScrollContent.Children.LastOrDefault();
                    if (lastView is ThemePreviewView)
                    {
                        var lastModelIndex = GetModelIndexForView(lastView as ThemePreviewView);
                        swapModelIndex = lastModelIndex + 1;
                    }
                }

                if (swapModelIndex < 0)
                {
                    swapModelIndex = _dataModels.Count + swapModelIndex;
                }
                if (swapModelIndex > _dataModels.Count - 1)
                {
                    swapModelIndex -= _dataModels.Count;
                }

                ScrollContent.Children.Remove(item);
                ScrollContent.Children.Insert(position, item);
                await (item as ThemePreviewView).UpdateModel(_dataModels[swapModelIndex]);
            }
        }

        private int GetModelIndexForView(ThemePreviewView view)
        {
            int result = -1;
            if (view != null)
            {
                var model = _dataModels.FirstOrDefault(m => m == view.ViewModel);
                if (model != null)
                {
                    result = _dataModels.IndexOf(model);
                }
            }
            return result;
        }

        private void StartTimer()
        {
            _timer.Interval = ScrollInterval;
            _timer.Start();
        }
        #endregion
    }
}
