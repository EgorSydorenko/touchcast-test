﻿using Pitch.DataLayer.Layouts;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using TouchCastComposingEngine;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Helpers.CustomControls
{
    public sealed partial class LayerPositionControl : UserControl
    {
        #region Fields

        private BaseLayoutView _view;
        private LayerLayoutMementoModel _mementoModel;
        private bool _updateInProcess;

        #endregion

        #region Properties

        public BaseLayoutView CurrentView
        {
            get
            {
                return _view;
            }
            set
            {
                if (_view != null && _view.ViewModel != null)
                {
                    _view.ViewModel.LayerLayoutPositionChangedEvent -= ViewModel_LayerLayoutPositionChangedEvent;
                }
                _view = value;
                if (_view != null && _view.ViewModel != null)
                {
                    _view.ViewModel.LayerLayoutPositionChangedEvent += ViewModel_LayerLayoutPositionChangedEvent;
                }
                Update();
            }
        }

        #endregion

        #region Life Cycle

        public LayerPositionControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Callbacks

        private void ViewModel_LayerLayoutPositionChangedEvent(Rect position)
        {
            Update();
        }

        private void Numeric_ValueWillChangedEvent(object sender, System.EventArgs e)
        {
            SaveMementoModel();
        }

        private void Numeric_ValueWasChangedEvent(object sender, System.EventArgs e)
        {
            PushMementoModel();
        }

        private void XCoordNumeric_ValueChanged(double delta)
        {
            if (_updateInProcess == true)
            {
                return;
            }
            _view.ChangePositionBy(delta, 0);
        }

        private void YCoordNumeric_ValueChanged(double delta)
        {
            if (_updateInProcess == true)
            {
                return;
            }
            _view.ChangePositionBy(0, delta);
        }

        private async void WidthNumeric_ValueChanged(double delta)
        {
            if (_updateInProcess == true)
            {
                return;
            }

            double newWidth = CurrentView.ViewModel.Position.Width + delta;
            double newHeight = CurrentView.ViewModel.Position.Height;

            if (CurrentView.ViewModel.KeepAspectRatio)
            {
                double minWidth = 0.0;
                double minHeight = 0.0;
                CalculateMinimalSizes(ref minWidth, ref minHeight);
                newWidth = NormalizeSize(newWidth, ComposingContext.VideoSize.Width - CurrentView.ViewModel.Transform.TranslateX, minWidth);
                newHeight = newWidth / (CurrentView.Width / CurrentView.Height);
                if (newHeight + CurrentView.ViewModel.Transform.TranslateY > ComposingContext.VideoSize.Height)
                {
                    newHeight = ComposingContext.VideoSize.Height - CurrentView.ViewModel.Transform.TranslateY;
                    newWidth = newHeight * (CurrentView.Width / CurrentView.Height);
                }
            }
            else
            {
                newWidth = NormalizeSize(newWidth, ComposingContext.VideoSize.Width - CurrentView.ViewModel.Transform.TranslateX, LayerLayoutTemplate.MinWidth);
            }

            await _view.ChangeSizeBy(newWidth, newHeight);
        }

        private async void HeightNumeric_ValueChanged(double delta)
        {
            if (_updateInProcess == true)
            {
                return;
            }

            double newHeight = CurrentView.ViewModel.Position.Height + delta;
            double newWidth = CurrentView.ViewModel.Position.Width;
            if (CurrentView.ViewModel.KeepAspectRatio)
            {
                double minWidth = 0.0;
                double minHeight = 0.0;
                CalculateMinimalSizes(ref minWidth, ref minHeight);
                newHeight = NormalizeSize(newHeight, ComposingContext.VideoSize.Height - CurrentView.ViewModel.Transform.TranslateY, minHeight);
                newWidth = newHeight * (CurrentView.Width / CurrentView.Height);
                if (newWidth + CurrentView.ViewModel.Transform.TranslateX > ComposingContext.VideoSize.Width)
                {
                    newWidth = ComposingContext.VideoSize.Width - CurrentView.ViewModel.Transform.TranslateX;
                    newHeight = newWidth / (CurrentView.Width / CurrentView.Height);
                }
            }
            else
            {
                newHeight = NormalizeSize(newHeight, ComposingContext.VideoSize.Height - CurrentView.ViewModel.Transform.TranslateY, LayerLayoutTemplate.MinHeight);
            }

            await _view.ChangeSizeBy(newWidth, newHeight);
        }

        private void UserControl_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool isEnabled = (bool)e.NewValue;
            UpdateLabelColors(isEnabled);
        }

        #endregion

        #region Private Methods

        private void Update()
        {
            _updateInProcess = true;
            try
            {
                XCoordNumeric.Value = _view.ViewModel.Position.Left;
                YCoordNumeric.Value = _view.ViewModel.Position.Top;
                WidthNumeric.Value = _view.ViewModel.Position.Width;
                HeightNumeric.Value = _view.ViewModel.Position.Height;
                UpdateLabelColors(!_view.ViewModel.IsStatic);
            }
            finally
            {
                _updateInProcess = false;
            }
        }

        private void UpdateLabelColors(bool isEnabled)
        {
            SolidColorBrush brush = new SolidColorBrush(isEnabled ? Colors.White : Color.FromArgb(0xff, 0x9d, 0x9d, 0x9d));
            XLabel.Foreground = brush;
            YLabel.Foreground = brush;
            WidthLabel.Foreground = brush;
            HeightLabel.Foreground = brush;
        }

        private void SaveMementoModel()
        {
            _mementoModel = CurrentView.ViewModel.GenerateMementoModel();
            _mementoModel.PreviousPosition = CurrentView.ViewModel.Position;
            _mementoModel.PreviousTransform = new CompositeTransform()
            {
                TranslateX = CurrentView.ViewModel.Transform.TranslateX,
                TranslateY = CurrentView.ViewModel.Transform.TranslateY,
                ScaleX = CurrentView.ViewModel.Transform.ScaleX,
                ScaleY = CurrentView.ViewModel.Transform.ScaleY,
                Rotation = CurrentView.ViewModel.Transform.Rotation,
            };
        }

        private void PushMementoModel()
        {
            if (_mementoModel != null)
            {
                CurrentView.ViewModel.Shot.MovementChanged(_mementoModel);
                _mementoModel = null;
            }
        }

        private void CalculateMinimalSizes(ref double minWidth, ref double minHeight)
        {
            if (CurrentView.AspectRatio > 1.0)
            {
                minWidth = LayerLayoutTemplate.MinWidth;
                minHeight = minWidth / CurrentView.AspectRatio;
            }
            else
            {
                minHeight = LayerLayoutTemplate.MinHeight;
                minWidth = minHeight * CurrentView.AspectRatio;
            }
        }

        private double NormalizeSize(double size, double max, double min)
        {
            if (size > max)
            {
                size = max;
            }
            else if (size < min)
            {
                size = min;
            }

            return size;
        }

        #endregion
    }
}
