﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Helpers.CustomControls
{
    public enum ImportPowerPointResult
    {
        Cancel,
        AllSlidesInSingleScene,
        ScenePerSlide
    }
    public sealed partial class ImportPowerPointDialog : ContentDialog
    {
        public ImportPowerPointResult Result { get; private set; }
        public ImportPowerPointDialog()
        {
            InitializeComponent();
            Opened += ImportPowerPointDialog_Opened;
        }

        private void ImportPowerPointDialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            Single.IsChecked = true;
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Windows.UI.Colors.Transparent);
                }
            }
        }
        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            Result = Single.IsChecked.Value ? ImportPowerPointResult.AllSlidesInSingleScene : ImportPowerPointResult.ScenePerSlide;
            Hide();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Result = ImportPowerPointResult.Cancel;
            Hide();
        }
    }
}
