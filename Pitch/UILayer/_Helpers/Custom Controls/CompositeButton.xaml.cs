﻿using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer._Helpers.CustomControls
{
    public sealed partial class CompositeButton : UserControl
    {
        public static readonly DependencyProperty MainContentProperty =
           DependencyProperty.Register(nameof(UpperPartContent), typeof(object),
              typeof(CompositeButton), new PropertyMetadata(default(object)));

        public static DependencyProperty CommandProperty = DependencyProperty.Register(nameof(Command), typeof(ICommand), 
            typeof(CompositeButton), new PropertyMetadata(null));
        
        public event RoutedEventHandler Click;

        public object UpperPartContent
        {
            get { return GetValue(MainContentProperty); }
            set { SetValue(MainContentProperty, value); }
        }

        public ICommand Command
        {
            get
            {
                return (ICommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
            }
        }

        #region Life Cycle
        public CompositeButton()
        {
            InitializeComponent();
            Loaded += CompositeButton_Loaded;
            Unloaded += CompositeButton_Unloaded;
        }

        private void CompositeButton_Loaded(object sender, RoutedEventArgs e)
        {
            LowerButton.Visibility = (FlyoutBase.GetAttachedFlyout(this) == null) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void CompositeButton_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= CompositeButton_Loaded;
            Unloaded -= CompositeButton_Unloaded;
            Bindings.StopTracking();
            Command = null;
        }
        #endregion

        #region Callbacks
        private void UpperButton_Click(object sender, RoutedEventArgs e)
        {
            Click?.Invoke(this, e);
        }

        private void LowerButton_Checked(object sender, RoutedEventArgs e)
        {
            var flyout = FlyoutBase.GetAttachedFlyout(this);
            if (flyout != null)
            {
                flyout.Closed += Flyout_Closed;
                FlyoutBase.ShowAttachedFlyout(this);
            }
        }

        private void LowerButton_Unchecked(object sender, RoutedEventArgs e)
        {
            var flyout = FlyoutBase.GetAttachedFlyout(this);
            if (flyout != null)
            {
                flyout.Closed -= Flyout_Closed;
                flyout.Hide();
            }
        }

        private void Flyout_Closed(object sender, object e)
        {
            LowerButton.IsChecked = false;
        }
        #endregion
    }
}
