﻿using System;
using System.Windows.Input;
using Pitch.UILayer._Helpers.Custom_Controls;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Helpers.CustomControls
{
    public class TcColorButton : Button
    {
        public static readonly DependencyProperty UserBrushProperty = 
            DependencyProperty.Register(
                "UserBrush", 
                typeof(Brush), 
                typeof(TcColorButton),
                new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));

        public Brush UserBrush
        {
            get { return (Brush)GetValue(UserBrushProperty); }
            set { SetValue(UserBrushProperty, value); }
        }

        public static readonly DependencyProperty UserMarginProperty = 
            DependencyProperty.Register(
                "UserMargin", 
                typeof(Thickness), 
                typeof(TcColorButton),
                new PropertyMetadata(new Thickness(0.0)));

        public Thickness UserMargin
        {
            get { return (Thickness)GetValue(UserMarginProperty); }
            set { SetValue(UserMarginProperty, value); }
        }
    }
    
    public sealed partial class ColorSelectorCompositeButton : UserControl
    {
        #region Depedency Properties

        public static DependencyProperty CommandProperty = DependencyProperty.Register(nameof(Command), typeof(ICommand),
            typeof(ColorSelectorCompositeButton), new PropertyMetadata(null));

        public static DependencyProperty TitleProperty = DependencyProperty.Register(nameof(Title), typeof(string),
            typeof(ColorSelectorCompositeButton), new PropertyMetadata(String.Empty));

        public static readonly DependencyProperty UserColorProperty = DependencyProperty.Register(
            nameof(UserColor), typeof(Brush), typeof(ColorSelectorCompositeButton),
            new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));

        public static readonly DependencyProperty IsAlwaysShowColorPickerProperty = DependencyProperty.Register(
            nameof(IsAlwaysShowColorPicker), typeof(Brush), typeof(ColorSelectorCompositeButton),
            new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));
        #endregion

        #region Events

        public event RoutedEventHandler Click;
        public event ColorSelection ColorSelectedEvent;

        #endregion

        #region Properties

        public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public Brush UserColor
        {
            get => (Brush)GetValue(UserColorProperty);
            set
            {
                SetValue(UserColorProperty, value);
                SelectColorInColorPicker(value);
            }
        }

        public bool IsAlwaysShowColorPicker
        {
            get => (bool)GetValue(IsAlwaysShowColorPickerProperty);
            set => SetValue(IsAlwaysShowColorPickerProperty, value);
        }

        #endregion

        #region Life Cycle

        public ColorSelectorCompositeButton()
        {
            InitializeComponent();
            InitializeColorPicker();

            Loaded += ColorSelectorCompositeButton_Loaded;
            Unloaded += ColorSelectorCompositeButton_Unloaded;
        }

#if DEBUG
        ~ColorSelectorCompositeButton()
        {
            System.Diagnostics.Debug.WriteLine("************* ColorSelectorCompositeButton Destructor *************");
        }
#endif

        private void ColorSelectorCompositeButton_Loaded(object sender, RoutedEventArgs e)
        {
            ColorPicker.ColorSelectedEvent += ColorPicker_ColorSelectedEvent;
            ColorPicker.ColorOpacityChangeEvent += ColorPicker_ColorOpacityChangeEvent;
            ColorPicker.MoreColorClickedEvent += ColorPicker_MoreColorClickedEvent;
        }

        private void ColorSelectorCompositeButton_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ColorSelectorCompositeButton_Loaded;
            Unloaded -= ColorSelectorCompositeButton_Unloaded;
            ColorPicker.ColorSelectedEvent -= ColorPicker_ColorSelectedEvent;
            ColorPicker.ColorOpacityChangeEvent -= ColorPicker_ColorOpacityChangeEvent;
            ColorPicker.MoreColorClickedEvent -= ColorPicker_MoreColorClickedEvent;
            Bindings.StopTracking();
            Command = null;
        }

        #endregion

        #region Callbacks

        private void UpperButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsAlwaysShowColorPicker)
            {
                ShowFlyout();
            }
            else
            {
                Click?.Invoke(this, e);
                var color = (UpperButton.UserBrush as SolidColorBrush)?.Color;
                if (color != null)
                {
                    ColorSelectedEvent?.Invoke(color.Value);
                }
            }
        }

        private void LowerButton_Checked(object sender, RoutedEventArgs e)
        {
            ShowFlyout();
        }

        private void LowerButton_Unchecked(object sender, RoutedEventArgs e)
        {
            var flyout = FlyoutBase.GetAttachedFlyout(UpperButton);
            if (flyout != null)
            {
                flyout.Closed -= Flyout_Closed;
                flyout.Hide();
            }
        }

        private void Flyout_Closed(object sender, object e)
        {
            //LowerButton.IsChecked = false;
        }
        
        private void ColorPicker_ColorSelectedEvent(Color color)
        {
            UpperButton.UserBrush = new SolidColorBrush(color);
            FlyoutBase.GetAttachedFlyout(UpperButton)?.Hide();
            ColorSelectedEvent?.Invoke(color);
        }

        private async void ColorPicker_MoreColorClickedEvent(Color color)
        {
            FlyoutBase.GetAttachedFlyout(UpperButton)?.Hide();
            var dialog = new ColorPickerDialog();
            dialog.ColorSelectedEvent += Dialog_ColorSelectedEvent;
            dialog.Color = color;
            await dialog.ShowAsync();
            dialog.ColorSelectedEvent -= Dialog_ColorSelectedEvent;
        }
        private void Dialog_ColorSelectedEvent(Color color)
        {
            ColorSelectedEvent?.Invoke(color);
        }

        private void ColorPicker_ColorOpacityChangeEvent(byte opacity)
        {
            Color? currentColor = (UpperButton.UserBrush as SolidColorBrush)?.Color;
            if (currentColor.HasValue)
            {
                var newColor = Color.FromArgb(opacity, currentColor.Value.R, currentColor.Value.G, currentColor.Value.B);
                UpperButton.UserBrush = new SolidColorBrush(newColor);
                ColorSelectedEvent?.Invoke(newColor);
            }
        }

        #endregion

        #region Private Methods

        private void InitializeColorPicker()
        {
            ColorPicker.OpacityVisibility = false;
            ColorPicker.NoColorVisibility = false;
        }

        private void SelectColorInColorPicker(Brush brush)
        {
            if (brush == null) return;
            Color? color = (brush as SolidColorBrush)?.Color;
            if (color.HasValue == true)
            {
                ColorPicker.SelectColorInList(color.Value);
            }
        }

        private void ShowFlyout()
        {
            var flyout = FlyoutBase.GetAttachedFlyout(UpperButton);
            if (flyout != null)
            {
                flyout.Closed += Flyout_Closed;
                FlyoutBase.ShowAttachedFlyout(UpperButton);
            }
        }
        #endregion
    }
}
