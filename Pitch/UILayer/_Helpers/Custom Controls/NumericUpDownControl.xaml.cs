﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Helpers.CustomControls
{
    public sealed partial class NumericUpDownControl : UserControl
    {
        #region Events

        public delegate void ValueChangedHandler(double delta);
        public event ValueChangedHandler ValueChangedEvent;
        public event EventHandler ValueWillChangedEvent;
        public event EventHandler ValueWasChangedEvent;

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                nameof(Value),
                typeof(double),
                typeof(NumericUpDownControl),
                new PropertyMetadata(0.0));

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set
            {
                if (value == Value)
                {
                    return;
                }
                double newValue = value;
                if (newValue > Maximum)
                {
                    newValue = Maximum;
                }
                if (newValue < Minimum)
                {
                    newValue = Minimum;
                }
                newValue = Math.Round(newValue);
                NumericTextBox.Text = newValue.ToString();
                _previousValue = NumericTextBox.Text;
                ValueChangedEvent?.Invoke(newValue - Value);
                SetValue(ValueProperty, newValue);
            }
        }

        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register(
                nameof(Minimum),
                typeof(double),
                typeof(NumericUpDownControl),
                new PropertyMetadata(0.0));

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register(
                nameof(Maximum),
                typeof(double),
                typeof(NumericUpDownControl),
                new PropertyMetadata(0.0));

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty ChangeProperty =
            DependencyProperty.Register(
                nameof(Change),
                typeof(double),
                typeof(NumericUpDownControl),
                new PropertyMetadata (1.0));
        
        public double Change
        {
            get { return (double)GetValue(ChangeProperty); }
            set { SetValue(ChangeProperty, value); }
        }

        #endregion

        #region Fields

        private bool _buttonPressed;
        private string _previousValue;

        #endregion

        #region Life Cycle

        public NumericUpDownControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Callbacks

        private void IncrementButton_Click(object sender, RoutedEventArgs e)
        {
            if (!_buttonPressed) return;
            Value += Change;
        }

        private void DecrementButton_Click(object sender, RoutedEventArgs e)
        {
            if (!_buttonPressed) return;
            Value -= Change;
        }

        private void Button_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            _buttonPressed = true;
            ValueWillChangedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void Button_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            _buttonPressed = false;
            ValueWasChangedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void NumericTextBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                if (Double.TryParse(NumericTextBox.Text, out double newValue) && Value != newValue)
                {
                    Value = newValue;
                    ValueWasChangedEvent.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private void NumericTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            _previousValue = NumericTextBox.Text;
            ValueWillChangedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void NumericTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            NumericTextBox.Text = _previousValue; 
        }

        #endregion
    }
}