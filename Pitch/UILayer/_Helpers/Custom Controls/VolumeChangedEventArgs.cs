﻿using System;

namespace Pitch.UILayer.Helpers.Controls
{
    public class VolumeChangedEventArgs : EventArgs
    {
        public double NewVolume { get; private set; }

        public VolumeChangedEventArgs()
        {

        }
        public VolumeChangedEventArgs(double volume)
        {
            NewVolume = volume;
        }
    }

    public enum MediaSeek
    {
        SeekStarted,
        SeekCompleted
    }

    public class MediaSeekEvent : EventArgs
    {
        public MediaSeek State { set; get; }
    }
}
