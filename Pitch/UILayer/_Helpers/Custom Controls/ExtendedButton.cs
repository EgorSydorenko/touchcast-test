﻿using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Pitch.UILayer.Helpers.Controls
{
    public class ExtendedButton : Button
    {
        public static readonly DependencyProperty UserLabelProperty = DependencyProperty.Register(
            "UserLabel", typeof(string), typeof(ExtendedButton),
            new PropertyMetadata(string.Empty));

        public string UserLabel
        {
            get { return (string)GetValue(UserLabelProperty); }
            set { SetValue(UserLabelProperty, value); }
        }

        public static readonly DependencyProperty UserMarginProperty = DependencyProperty.Register(
            "UserMargin", typeof(Thickness), typeof(ExtendedButton),
            new PropertyMetadata(new Thickness(0.0)));

        public Thickness UserMargin
        {
            get { return (Thickness)GetValue(UserMarginProperty); }
            set { SetValue(UserMarginProperty, value); }
        }

        public static readonly DependencyProperty LabelForegroundProperty = DependencyProperty.Register(
            "LabelForeground", typeof(Brush), typeof(ExtendedButton),
            new PropertyMetadata(new SolidColorBrush(Colors.White)));
        public Brush LabelForeground
        {
            get { return (Brush)GetValue(LabelForegroundProperty); }
            set { SetValue(LabelForegroundProperty, value); }
        }
    }
}
