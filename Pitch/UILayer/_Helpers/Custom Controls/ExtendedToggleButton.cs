﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using TouchCastComposingEngine;

namespace Pitch.UILayer.Helpers.Controls
{
    public class ExtendedToggleButton : ToggleButton
    {
        public static readonly DependencyProperty UserBrushProperty = DependencyProperty.Register(
            "UserBrush", typeof(Brush), typeof(ExtendedToggleButton), 
            new PropertyMetadata(new SolidColorBrush(ComposingContext.Shared().ChromaKeyEffect.ChromaKeyColor)));

        public Brush UserBrush
        {
            get { return (Brush)GetValue(UserBrushProperty); }
            set { SetValue(UserBrushProperty, value); }
        }

        public static readonly DependencyProperty UserMarginProperty = DependencyProperty.Register(
            "UserMargin", typeof(Thickness), typeof(ExtendedToggleButton),
            new PropertyMetadata(new Thickness(0.0)));

        public Thickness UserMargin
        {
            get { return (Thickness)GetValue(UserMarginProperty); }
            set { SetValue(UserMarginProperty, value); }
        }
    }
}
