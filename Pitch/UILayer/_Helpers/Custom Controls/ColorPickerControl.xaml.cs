﻿using System.Collections.Generic;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer._Helpers.Custom_Controls
{
    public delegate void ColorSelection(Color color);
    public delegate void ColorOpacityChanged(byte opacity);
    public delegate void MoreColorClicked(Color color);

    public sealed class TcColorItemControl : Grid
    {
        public TcColorItemControl(Color color)
        {
            Width = 20;
            Height = 20;
            Background = new SolidColorBrush(color);
        }
    }

    public sealed partial class ColorPickerControl : UserControl
    {
        #region Private Fields

        private Color _defaultColor = Colors.Transparent;
        private bool _opacityVisibility = true;
        private bool _noColorVisibility = true;
        private byte _opacity;
        private Color _selectedColor;

        private IList<Color> _colors = new List<Color> {
            Color.FromArgb(0xff, 0xca, 0x6f, 0xff),
            Color.FromArgb(0xff, 0x76, 0x38, 0x24),
            Color.FromArgb(0xff, 0xfd, 0x75, 0x33),
            Color.FromArgb(0xff, 0xf4, 0xe1, 0x00),
            Color.FromArgb(0xff, 0x07, 0xa3, 0x86),
            Color.FromArgb(0xff, 0x01, 0x74, 0xe6),
            Color.FromArgb(0xff, 0xe3, 0x1b, 0x24),
            Color.FromArgb(0xff, 0x00, 0x01, 0x01),
            Color.FromArgb(0xff, 0x73, 0x4c, 0xb8),
            Color.FromArgb(0xff, 0x97, 0xe1, 0xff),
            Color.FromArgb(0xff, 0xff, 0xff, 0xff),
            Color.FromArgb(0xff, 0x49, 0xce, 0x50),
            Color.FromArgb(0xff, 0x99, 0xac, 0xfb),
            Color.FromArgb(0xff, 0xfa, 0x75, 0xc1),
            Color.FromArgb(0xff, 0x7c, 0x7c, 0x7c),
        };

        #endregion

        #region Properties 

        public Color DefaultColor
        {
            get => _defaultColor;
            set => _defaultColor = value;
        }

        public bool OpacityVisibility
        {
            get => _opacityVisibility;
            set
            {
                _opacityVisibility = value;
                OpacityPanel.Visibility = _opacityVisibility ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool NoColorVisibility
        {
            get => _noColorVisibility;
            set
            {
                _noColorVisibility = value;
                NoColorButton.Visibility = _noColorVisibility ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public byte Opacity => _opacityVisibility ? _opacity : byte.MaxValue;

        public IList<Color> Palette => _colors;

        #endregion

        #region Events

        public event ColorSelection ColorSelectedEvent;
        public event ColorOpacityChanged ColorOpacityChangeEvent;
        public event MoreColorClicked MoreColorClickedEvent;
        #endregion

        #region Life Cycle

        public ColorPickerControl()
        {
            InitializeComponent();
            SetColors();
            Subscribe();
        }

#if DEBUG
        ~ColorPickerControl()
        {
            System.Diagnostics.Debug.WriteLine("************* ColorPickerControl Destructor *************");
        }
#endif
        #endregion

        #region Public Methods

        public void SelectColorInList(Color color)
        {
            Unsubscribe();
            _selectedColor = color;
            var colorWithoutOpacity = Color.FromArgb(0xff, color.R, color.G, color.B);
            ColorsList.SelectedIndex = Palette.IndexOf(colorWithoutOpacity);
            OpacitySlider.IsEnabled = color != DefaultColor;
            _opacity = color.A;
            if (color != DefaultColor)
            {
                OpacitySlider.Value = color.A;
            }
            Subscribe();
        }

        #endregion

        #region Private Methods

        private void Subscribe()
        {
            ColorsList.SelectionChanged += Colors_SelectionChanged;
            NoColorButton.Click += NoColorButton_Click;
            ColorsList.ItemClick += ColorsList_ItemClick;
            OpacitySlider.ValueChanged += OpacitySlider_ValueChanged;
        }

        private void Unsubscribe()
        {
            ColorsList.SelectionChanged -= Colors_SelectionChanged;
            NoColorButton.Click -= NoColorButton_Click;
            ColorsList.ItemClick -= ColorsList_ItemClick;
            OpacitySlider.ValueChanged -= OpacitySlider_ValueChanged;
        }

        private void SetColors()
        {
            ColorsList.Items.Clear();
            foreach (var item in Palette)
            {
                ColorsList.Items.Add(new TcColorItemControl(item));
            }
            MoreColorButton.Visibility = (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.Xaml.Controls.ColorPicker")) ? Visibility.Visible : Visibility.Collapsed;
        }

        private Color GetResultColor()
        {
            var colorWithoutOpacity = Palette[ColorsList.SelectedIndex];
            _opacity = (byte)OpacitySlider.Value;

            return Color.FromArgb(_opacity, colorWithoutOpacity.R, colorWithoutOpacity.G, colorWithoutOpacity.B);
        }

        #endregion

        #region Callbacks

        private void Colors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Palette.Count < ColorsList.SelectedIndex) return;
            ColorSelectedEvent?.Invoke(GetResultColor());
        }

        private void NoColorButton_Click(object sender, RoutedEventArgs e)
        {
            ColorSelectedEvent?.Invoke(DefaultColor);
        }

        private void ColorsList_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (ColorsList.SelectedItem == e.ClickedItem)
            {
                ColorSelectedEvent?.Invoke(GetResultColor());
            }
        }

        private void OpacitySlider_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            ColorOpacityChangeEvent?.Invoke((byte)OpacitySlider.Value);
        }

        private void MoreColorButton_Click(object sender, RoutedEventArgs e)
        {
            MoreColorClickedEvent?.Invoke(_selectedColor);
        }

        #endregion
    }
}
