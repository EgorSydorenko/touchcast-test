﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer._Helpers.Custom_Controls
{
    public delegate void ListTypeChanged(object sender, ListTypeChangedEventArgs args);

    public class ListTypeChangedEventArgs : EventArgs
    {
        public ListMarkType ListMarkerType { get; private set; }

        public ListTypeChangedEventArgs(ListMarkType lmt)
        {
            ListMarkerType = lmt;
        }
    }

    public class ListMarkType
    {
        private static readonly ListMarkType empty = new ListMarkType(MarkerType.None, MarkerStyle.Undefined);
        public static ListMarkType Empty => empty;

        public MarkerType Type { get; private set; }
        public MarkerStyle Style { get; private set; }
        public string IconPath { get; private set; }
        
        public ListMarkType(MarkerType type, MarkerStyle style, string iconPath = null)
        {
            Type = type;
            Style = style;
            IconPath = iconPath;
        }
    }

    public sealed partial class HorizontalCompositeButton : UserControl
    {
        #region Depedency Properties

        public static readonly DependencyProperty IconSourceProperty = 
            DependencyProperty.Register(
                "IconSource",
                typeof(ImageSource),
                typeof(HorizontalCompositeButton), 
                new PropertyMetadata(null));

        public static readonly DependencyProperty RightButtonVisibilityProperty =
            DependencyProperty.Register(
                "RightButtonVisibility",
                typeof(Visibility),
                typeof(HorizontalCompositeButton),
                new PropertyMetadata(Visibility.Visible));

        #endregion

        #region Events

        public event ListTypeChanged ListTypeChangedEvent;

        #endregion

        #region Private Fields

        private IList<ListMarkType> _markersList;

        #endregion

        #region Properties

        public ImageSource IconSource
        {
            get
            {
                return (ImageSource)GetValue(IconSourceProperty);
            }
            set
            {
                SetValue(IconSourceProperty, value);
            }
        }

        public Visibility RightButtonVisibility
        {
            get
            {
                return (Visibility)GetValue(RightButtonVisibilityProperty);
            }
            set
            {
                SetValue(RightButtonVisibilityProperty, value);
            }
        }

        public ListMarkType DefaultMarker { get; set; }
        
        #endregion

        #region Life Cycle

        public HorizontalCompositeButton()
        {
            InitializeComponent();
        }

#if DEBUG
        ~HorizontalCompositeButton()
        {
            System.Diagnostics.Debug.WriteLine("************* HorizontalCompositeButton Destructor *************");
        }
#endif
        #endregion

        #region Public

        public void InitializeMarkersButtons(IList<ListMarkType> markerList)
        {
            _markersList = markerList;
            Style radioButtonStyle = Resources["FlyoutRadioButtonInverseStyle"] as Style;
            foreach (var item in markerList)
            {
                RadioButton button = new RadioButton
                {
                    Width = 40,
                    Height = 40,
                    Tag = item,
                    GroupName = "BulletedGroup",
                };
                if (radioButtonStyle != null)
                {
                    button.Style = radioButtonStyle;
                }
                if (!String.IsNullOrEmpty(item.IconPath))
                {
                    Image buttonIcon = new Image()
                    {
                        Source = new BitmapImage(new Uri(item.IconPath)),
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        VerticalAlignment = VerticalAlignment.Stretch,
                    };
                    button.Content = buttonIcon;
                }
                RadioButtonGrid.Children.Add(button);
            }
        }

        public void SelectButtonByMarkerType(ListMarkType listMarkerType)
        {
            Unsubscribe();

            if (listMarkerType.Type != MarkerType.None 
                && _markersList.Where(m => m.Type == listMarkerType.Type && m.Style == listMarkerType.Style).Count() != 0)
            {
                LeftButton.IsChecked = true;
                CheckRadioButtonByMarkType(listMarkerType);
            }
            else
            {
                LeftButton.IsChecked = false;
                UnckeckedRadioButtons();
            }

            Subscribe();
        }

        #endregion

        #region Callbacks

        private void LeftButton_Checked(object sender, RoutedEventArgs e)
        {
            SelectButtonByMarkerType(DefaultMarker);
            ListTypeChangedEvent?.Invoke(this, new ListTypeChangedEventArgs(DefaultMarker));
        }

        private void LeftButton_Unchecked(object sender, RoutedEventArgs e)
        {
            ListFlyout.Hide();
            UnckeckedRadioButtons();
            ListTypeChangedEvent?.Invoke(this, new ListTypeChangedEventArgs(ListMarkType.Empty));
        }

        private void RightButton_Checked(object sender, RoutedEventArgs e)
        {
            ListFlyout.ShowAt(this);
        }

        private void RightButton_Unchecked(object sender, RoutedEventArgs e)
        {
            ListFlyout.Hide();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            CloseFlyout();
            if (sender is RadioButton currentRadioButton)
            {
                if (currentRadioButton.Tag != null && currentRadioButton.Tag is ListMarkType markerType)
                {
                    ListTypeChangedEvent?.Invoke(this, new ListTypeChangedEventArgs(markerType));
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender is RadioButton button && button.IsChecked == true)
            {
                CloseFlyout();
            }
        }

        private void ListFlyout_Closed(object sender, object e)
        {
            if (RightButton.IsChecked == true)
            {
                RightButton.IsChecked = false;
            }
        }

        #endregion

        #region Private Methods

        private void Subscribe()
        {
            LeftButton.Checked += LeftButton_Checked;
            LeftButton.Unchecked += LeftButton_Unchecked;
            RightButton.Checked += RightButton_Checked;
            RightButton.Unchecked += RightButton_Unchecked;
            foreach (var radioButton in RadioButtonGrid.Children)
            {
                if (radioButton is RadioButton button)
                {
                    button.Checked += RadioButton_Checked;
                    button.Click += Button_Click;
                }
            }
        }

        private void Unsubscribe()
        {
            LeftButton.Checked -= LeftButton_Checked;
            LeftButton.Unchecked -= LeftButton_Unchecked;
            RightButton.Checked -= RightButton_Checked;
            RightButton.Unchecked -= RightButton_Unchecked;
            foreach (var radioButton in RadioButtonGrid.Children)
            {
                if (radioButton is RadioButton button)
                {
                    button.Checked -= RadioButton_Checked;
                    button.Click -= Button_Click;
                }
            }
        }

        private void UnckeckedRadioButtons()
        {
            foreach (var radioButton in RadioButtonGrid.Children)
            {
                if (radioButton is RadioButton button)
                {
                    button.IsChecked = false;
                }
            }
        }

        private void CheckRadioButtonByMarkType(ListMarkType listMarkType)
        {
            foreach (var item in RadioButtonGrid.Children)
            {
                if (item is RadioButton button)
                {
                    if (button.Tag is ListMarkType buttonMarkType)
                    {
                        if (buttonMarkType.Type == listMarkType.Type && buttonMarkType.Style == listMarkType.Style)
                        {
                            button.IsChecked = true;
                            break;
                        }
                    }
                }
            }
        }

        private void CloseFlyout()
        {
            RightButton.IsChecked = false;
            LeftButton.IsChecked = true;
            ListFlyout.Hide();
        }

        #endregion
    }
}
