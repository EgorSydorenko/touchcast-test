﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Shapes;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer._Helpers.CustomControls
{
    public sealed partial class GainLevelControl : UserControl
    {
        #region Dependency Properties

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(double), typeof(GainLevelControl), new PropertyMetadata(null));

        #endregion

        #region Properties  

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
       
        private double PercentValue
        {
            get
            {
                return 100.0 * (LevelSlider.Value - LevelSlider.Minimum ) / (LevelSlider.Maximum - LevelSlider.Minimum);
            }
        }

        private double ScaleDivision
        {
            get
            {
                return 100.0 / IndicatorGrid.Children.Count;
            }
        }

        #endregion

        #region Life Cycle

        public GainLevelControl()
        {
            this.InitializeComponent();
            Loaded += GainLevelControl_Loaded;
        }

        private void GainLevelControl_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateIndicator();
        }

        #endregion

        #region Call Back

        private void LevelSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            UpdateIndicator();
        }

        #endregion

        #region Private Methods
        
        private void UpdateIndicator()
        {
            int activeRectangles = CalculateActiveRectangles();
            for (int i = 0; i < IndicatorGrid.Children.Count; i++)
            {
                if (IndicatorGrid.Children[i] is Rectangle rect)
                {
                    rect.Visibility = ((IndicatorGrid.Children.Count - i) > activeRectangles) ? Visibility.Collapsed : Visibility.Visible;
                }
            }
        }

        private int CalculateActiveRectangles()
        {
            double percent = PercentValue;
            double division = ScaleDivision;
            return (int)Math.Floor(percent / division);
        }

        #endregion
    }
}
