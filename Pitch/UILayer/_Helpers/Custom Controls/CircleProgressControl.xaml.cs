﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Helpers.AnimationControl
{
    public sealed partial class CircleProgressControl : UserControl
    {
        public static readonly DependencyProperty PercentProperty =
            DependencyProperty.Register(nameof(Percent), typeof(double),
                typeof(CircleProgressControl),
                new PropertyMetadata(0d));

        public double Percent
        {
            get => (double)GetValue(PercentProperty);
            set
            {
                PART_Label.Text = $"{(int)value}%";
                RingSlice.StartAngle = (360.0 / 100.0) * value;
                SetValue(PercentProperty, value);
            }
        }

        public CircleProgressControl()
        {
            InitializeComponent();
        }

        public void UpdateProgress(double value)
        {
            Percent = value;
        }
    }
}
