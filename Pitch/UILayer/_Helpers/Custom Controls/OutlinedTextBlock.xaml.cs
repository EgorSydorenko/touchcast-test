﻿using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.Text;
using Microsoft.Graphics.Canvas.UI.Xaml;

namespace Pitch.UILayer.Helpers.CustomControls
{
    public sealed partial class OutlinedTextBlock : UserControl
    {
        private CanvasTextLayout _textLayout;
        private CanvasGeometry _textGeometry;
        private readonly CanvasStrokeStyle _stroke = new CanvasStrokeStyle()
        {
            DashStyle = CanvasDashStyle.Solid
        };

        private bool _isNeedsResourceRecreation;

        public OutlinedTextBlock()
        {
            InitializeComponent();
            Loaded += OutlinedTextBlock_Loaded;
            Unloaded += OutlinedTextBlock_Unloaded;
        }

#if DEBUG
        ~OutlinedTextBlock()
        {
            System.Diagnostics.Debug.WriteLine("********************OutlinedTextBlock Destructor********************");
        }
#endif

        private void OutlinedTextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            canvas.Draw += Canvas_Draw;
            canvas.SizeChanged += Canvas_SizeChanged;
            canvas.CreateResources += Canvas_CreateResources;
        }

        private void OutlinedTextBlock_Unloaded(object sender, RoutedEventArgs e)
        {
            if (canvas != null)
            {
                canvas.Draw -= Canvas_Draw;
                canvas.SizeChanged -= Canvas_SizeChanged;
                canvas.CreateResources -= Canvas_CreateResources;
                // Explicitly remove references to allow the Win2D controls to get garbage collected
                //canvas.RemoveFromVisualTree();
                //canvas = null;
            }
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
                _isNeedsResourceRecreation = true;
                canvas.Invalidate();
            }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text", typeof(string), typeof(OutlinedTextBlock), new PropertyMetadata("Test")
            );

        public new double FontSize
        {
            get
            {
                return (double)GetValue(FontSizeProperty);
            }
            set
            {
                SetValue(FontSizeProperty, value);
                _isNeedsResourceRecreation = true;
                canvas.Invalidate();
            }
        }
        /// <summary>
        /// Identifies the FontSize dependency property.
        /// </summary>
        public new static readonly DependencyProperty FontSizeProperty = DependencyProperty.Register(
            "FontSize", typeof(double), typeof(OutlinedTextBlock), new PropertyMetadata(48.0f)
            );

        public new Color Foreground
        {
            get
            {
                return (Color)GetValue(ForegroundProperty);
            }
            set
            {
                SetValue(ForegroundProperty, value);
                canvas.Invalidate();
            }
        }

        public new static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register(
            "Foreground", typeof(Color), typeof(OutlinedTextBlock), new PropertyMetadata(Colors.White)
            );

        public Color OutlineColor
        {
            get
            {
                return (Color)GetValue(OutlineColorProperty);
            }
            set
            {
                SetValue(OutlineColorProperty, value);
                canvas.Invalidate();
            }
        }

        public static readonly DependencyProperty OutlineColorProperty = DependencyProperty.Register(
            "OutlineColor", typeof(Color), typeof(OutlinedTextBlock), new PropertyMetadata(Colors.Black)
            );

        public double OutlineThickness
        {
            get
            {
                return (double)GetValue(OutlineThicknessProperty);
            }
            set
            {
                SetValue(OutlineThicknessProperty, value);
                canvas.Invalidate();
            }
        }

        public static readonly DependencyProperty OutlineThicknessProperty = DependencyProperty.Register(
            "OutlineThickness", typeof(double), typeof(OutlinedTextBlock), new PropertyMetadata(3.0)
            );

        private void Canvas_Draw(CanvasControl sender, CanvasDrawEventArgs args)
        {
            EnsureResources(sender, sender.Size);

            args.DrawingSession.DrawGeometry(_textGeometry, OutlineColor, (float)OutlineThickness, _stroke);
            args.DrawingSession.DrawTextLayout(_textLayout, 0, 0, Foreground);
        }

        private void EnsureResources(ICanvasResourceCreatorWithDpi resourceCreator, Size targetSize)
        {
            if (!_isNeedsResourceRecreation)
                return;

            if (_textLayout != null)
            {
                _textLayout.Dispose();
                //_textGeometry.Dispose();
            }

            _textLayout = CreateTextLayout(resourceCreator, (float)targetSize.Width, (float)targetSize.Height);
            //_textGeometry = CanvasGeometry.CreateText(_textLayout);
            _isNeedsResourceRecreation = false;
        }

        private CanvasTextLayout CreateTextLayout(ICanvasResourceCreator resourceCreator, float canvasWidth, float canvasHeight)
        {
            CanvasTextFormat textFormat = new CanvasTextFormat()
            {
                FontSize = (float)FontSize,
                Direction = CanvasTextDirection.LeftToRightThenTopToBottom,
                HorizontalAlignment = CanvasHorizontalAlignment.Center,
                VerticalAlignment = CanvasVerticalAlignment.Center,
            };

            CanvasTextLayout textLayout = new CanvasTextLayout(resourceCreator, Text, textFormat, canvasWidth, canvasHeight);

            textLayout.TrimmingGranularity = CanvasTextTrimmingGranularity.Character;
            textLayout.TrimmingSign = CanvasTrimmingSign.Ellipsis;

            return textLayout;
        }

        private void Canvas_CreateResources(CanvasControl sender, object args)
        {
            _isNeedsResourceRecreation = true;
        }

        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _isNeedsResourceRecreation = true;
        }
    }
}
