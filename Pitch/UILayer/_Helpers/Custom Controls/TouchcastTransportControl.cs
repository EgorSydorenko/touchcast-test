﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;

namespace Pitch.UILayer.Helpers.Controls
{
    public class TouchcastTransportControl : MediaTransportControls
    {
        public static readonly DependencyProperty TouchcastVolumeVisibilityProperty =
            DependencyProperty.Register("TouchcastVolumeVisibility",
                typeof(Visibility), typeof(TouchcastTransportControl),
                new PropertyMetadata(Visibility.Collapsed));

        public Visibility TouchcastVolumeVisibility
        {
            get
            {
                return (Visibility)GetValue(TouchcastVolumeVisibilityProperty);
            }
            set
            {
                SetValue(TouchcastVolumeVisibilityProperty, value);
                if (_volumeButton != null)
                {
                    _volumeButton.Visibility = value;
                }
            }
        }

        public double Volume
        {
            set
            {
                _volume = value;
            }
        }

        public void EnableProgressSlider(bool isEnabled)
        {
            var slider = GetTemplateChild("ProgressSlider") as Slider;
            if(slider != null)
            {
                slider.IsEnabled = isEnabled;
            }
        }

        public event EventHandler<EventArgs> PlayPauseTapped;
        public event EventHandler<VolumeChangedEventArgs> VolumeChanged;

        //public event EventHandler<MediaSeekEvent> SeekEvent;

        private BitmapIcon _volumeIcon;
        private FrameworkElement _volumeButton;
        private AppBarButton _playPauseButton;
        private Slider _slider;
        private double _volume = 0;
        private static readonly double _flyoutIndentWidth = 4;

        public TouchcastTransportControl()
        {
            IsVolumeButtonVisible = true;
            IsVolumeEnabled = true;
            IsZoomButtonVisible = false;
            IsZoomEnabled = false;
            IsFullWindowEnabled = false;
            IsFullWindowButtonVisible = false;
            IsTextScaleFactorEnabled = false;
            UseSystemFocusVisuals = false;
            IsPlaybackRateButtonVisible = false;
            IsPlaybackRateEnabled = false;
            Background = new SolidColorBrush(Colors.Transparent);
            IsCompact = true;
            ManipulationMode = Windows.UI.Xaml.Input.ManipulationModes.TranslateX | Windows.UI.Xaml.Input.ManipulationModes.TranslateY;
            VerticalContentAlignment = VerticalAlignment.Center;
            if (Application.Current.Resources.ContainsKey("TouchcastTranportControlStyle"))
            {
                Style = (Style)Application.Current.Resources["TouchcastTranportControlStyle"];
            }
            Unloaded += TouchcastTranportControl_Unloaded;
        }

        private void TouchcastTranportControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_playPauseButton != null)
            {
                _playPauseButton.Tapped -= PlayPauseButton_Tapped;
            }

            if (_slider != null)
            {
                _slider.ValueChanged -= Slider_ValueChanged;
            }
        }

#if DEBUG
        ~TouchcastTransportControl()
        {
            System.Diagnostics.Debug.WriteLine("********************TouchcastTranportControl Destructor********************");
        }
#endif

        protected override void OnApplyTemplate()
        {
            _playPauseButton = GetTemplateChild("PlayPauseButtonOnLeft") as AppBarButton;
            if (PlayPauseTapped != null && _playPauseButton != null)
            {
                _playPauseButton.Tapped += PlayPauseButton_Tapped;
            }
            _slider = GetTemplateChild("TouchcastVolumeSlider") as Slider;
            if (_slider != null && VolumeChanged != null)
            {
                _slider.ValueChanged += Slider_ValueChanged;
            }

            _volumeIcon = GetTemplateChild("TouchcastVolumeIcon") as BitmapIcon;
            _volumeButton = GetTemplateChild("TouchcastVolumeButton") as FrameworkElement;

            base.OnApplyTemplate();

            var flyoutRoot = GetTemplateChild("VolumeFlyoutRoot") as Panel;
            var flyoutWidth = _volumeButton.Width + _flyoutIndentWidth * 2;
            if (flyoutRoot != null) flyoutRoot.Width = flyoutWidth;
            _volumeButton.Visibility = TouchcastVolumeVisibility;
            _slider.Value = _volume * 100;
        }

        private void PlayPauseButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            PlayPauseTapped?.Invoke(this, EventArgs.Empty);
        }

        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Uri newUri = new Uri((e.NewValue > 66) ? "ms-appx:///Assets/MediaTransportControls/normal_loud.png"
                       : (e.NewValue > 33) ? "ms-appx:///Assets/MediaTransportControls/normal_medium.png"
                       : (e.NewValue > 0)  ? "ms-appx:///Assets/MediaTransportControls/normal_low.png"
                       : "ms-appx:///Assets/MediaTransportControls/normal_mute.png");

            if (_volumeIcon.UriSource != null && newUri != _volumeIcon.UriSource)
            {
                _volumeIcon.UriSource = newUri;
            }

            VolumeChanged?.Invoke(this, new VolumeChangedEventArgs(e.NewValue / 100));
        }
    }
}
