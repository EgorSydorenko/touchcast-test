﻿using System;
using System.Threading.Tasks;
using Pitch.Helpers.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

namespace Pitch.UILayer.Helpers.AnimationControl
{
    /// <summary>
    /// A control that displays a countdown visualization
    /// and raises a CountdownComplete event when countdown is complete.
    /// Countdown duration is specified in Seconds.
    /// </summary>
    public sealed partial class CountdownControl
    {
        Guid d = Guid.NewGuid();
        private bool _isCountingDown;
        private Storyboard _storyboard;
        private readonly TimeSpan _duration = TimeSpan.FromSeconds(1d);

        public bool IsCountingDown
        {
            get
            {
                return _isCountingDown;
            }
            set
            {
                _isCountingDown = value;
                if (!_isCountingDown)
                {
                    _storyboard?.Seek(_duration);
                    Seconds = 0;
                }
            }
        }
        /// <summary>
        /// Occurs when the countdown is complete.
        /// </summary>
        public event RoutedEventHandler CountdownComplete;

        #region Seconds
        /// <summary>
        /// The seconds property.
        /// </summary>
        public static readonly DependencyProperty SecondsProperty =
            DependencyProperty.Register("Seconds", typeof(int),
                typeof(CountdownControl),  new PropertyMetadata(0, OnSecondsChanged));

        /// <summary>
        /// Gets or sets the seconds to countdown.
        /// </summary>
        /// <value>
        /// The seconds.
        /// </value>
        public int Seconds
        {
            get { return (int)GetValue(SecondsProperty); }
            set { SetValue(SecondsProperty, value); }
        }

        private static void OnSecondsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var target = (CountdownControl)sender;
            var oldSeconds = (int)e.OldValue;
            var newSeconds = (int)e.NewValue;
            target.OnSecondsChanged(oldSeconds, newSeconds);
        }

        private void OnSecondsChanged(int oldSeconds, int newSeconds)
        {
            if (!_isCountingDown && newSeconds > 0)
            {
#pragma warning disable 4014
                StartCountdownAsync(newSeconds);
#pragma warning restore 4014
            }
        }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CountdownControl" /> class.
        /// </summary>
        public CountdownControl()
        {
            InitializeComponent();
        }

#if DEBUG
        ~CountdownControl()
        {
            System.Diagnostics.Debug.WriteLine("********************CountdownControl Destructor********************");
        }
#endif

        /// <summary>
        /// Starts the countdown and completes when the countdown completes.
        /// </summary>
        /// <param name="seconds">The seconds.</param>
        /// <returns></returns>
        public async Task StartCountdownAsync(int seconds)
        {
            _isCountingDown = true;
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<Messages.CancelCountingMessage>(this,
                (m) =>
                {
                    IsCountingDown = false;
                }
            );
            Seconds = seconds;

            while (this.Seconds > 0 && _isCountingDown)
            {
                _storyboard = new Storyboard();
                var da = new DoubleAnimation
                {
                    From = 1d,
                    To = 0,
                    Duration = new Duration(_duration),
                    EnableDependentAnimation = true
                };

                _storyboard.Children.Add(da);
                _storyboard.RepeatBehavior = new RepeatBehavior(1);
                Storyboard.SetTargetProperty(da, "Opacity");
                Storyboard.SetTarget(_storyboard, PART_Label);

                PART_Label.Text = this.Seconds.ToString();

                await _storyboard.BeginAsync();

                Seconds--;
            }

            if (_isCountingDown)
            {
                PART_Label.Text = Seconds.ToString();
                CountdownComplete?.Invoke(this, new RoutedEventArgs());
            }

            _isCountingDown = false;
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Unregister(this);
        }
    }
}
