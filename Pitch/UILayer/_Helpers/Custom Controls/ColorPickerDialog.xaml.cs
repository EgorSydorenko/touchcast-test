﻿using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Helpers.CustomControls
{
    public sealed partial class ColorPickerDialog : ContentDialog
    {
        #region Private Fields
        private ColorPicker _colorPicker;
        #endregion

        public delegate void ColorSelection(Color color);
        public event ColorSelection ColorSelectedEvent;

        public Color Color {
            get => _colorPicker.Color;
            set
            {
                _colorPicker.Color = Color.FromArgb(0xff, value.R, value.G, value.B);
            }
        }

        #region Life Cycle
        public ColorPickerDialog()
        {
            InitializeComponent();

            CreateColorPicker();
            SizeChanged += ColorPickerDialog_SizeChanged;
            
        }

        private void ColorPickerDialog_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ///TODO:Check in newest version of windows.
            /// This needs to prevent crash after risezing application
            /// Don't know why after resing application color picker throwing Exception
            /// recreation of color picker help to prevent this crash
            CreateColorPicker();
        }
#if DEBUG
        ~ColorPickerDialog()
        {
            System.Diagnostics.Debug.WriteLine("******************* ColorPickerDialog Destructor ********************");
        }
#endif

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
            foreach (var popup in popups)
            {
                if (popup.Child is Rectangle)
                {
                    (popup.Child as Rectangle).Fill = new SolidColorBrush(Windows.UI.Colors.Transparent);
                }
            }
        }
        #endregion

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            ColorSelectedEvent?.Invoke(_colorPicker.Color);
            Hide();
        }
        private void CreateColorPicker()
        {
            if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.Xaml.Controls.ColorPicker"))
            {
                var colorPicker = new ColorPicker()
                {
                    ColorSpectrumShape = ColorSpectrumShape.Box,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    IsColorChannelTextInputVisible = true,
                    IsHexInputVisible = false,
                    Margin = new Thickness(10, 10, 10, 0)      
                };

                if (_colorPicker != null)
                {
                    colorPicker.Color = _colorPicker.Color;
                }

                Grid.SetRow(colorPicker, 1);
                LayoutRoot.Children.Insert(0, colorPicker);
                LayoutRoot.Children.Remove(_colorPicker);
                _colorPicker = colorPicker;
            }
        }
    }
}
