﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer._Helpers.Custom_Controls
{
    public sealed partial class ColorSelectorButton : UserControl
    {
        #region Dependency Properties

        public static readonly DependencyProperty IconSourceProperty =
           DependencyProperty.Register(
               "IconSource",
               typeof(ImageSource),
               typeof(HorizontalCompositeButton),
               new PropertyMetadata(null));

        public static readonly DependencyProperty UserColorProperty = 
            DependencyProperty.Register(
                "UserColor", 
                typeof(Color), 
                typeof(ColorSelectorButton),
                new PropertyMetadata(Colors.Transparent));

        public static readonly DependencyProperty DefaultColorProperty =
            DependencyProperty.Register(
                "DefaultColor",
                typeof(Color),
                typeof(ColorSelectorButton),
                new PropertyMetadata(Colors.Transparent));

        public static readonly DependencyProperty RectangleMarginProperty =
            DependencyProperty.Register(
                "RectangleMargin",
                typeof(Thickness),
                typeof(ColorSelectorButton),
                new PropertyMetadata(new Thickness(0)));

        public static readonly DependencyProperty RectangleWidthProperty =
            DependencyProperty.Register(
                "RectangleWidth",
                typeof(double),
                typeof(ColorSelectorButton),
                new PropertyMetadata(0));

        public static readonly DependencyProperty RectangleHeightProperty =
            DependencyProperty.Register(
                "RectangleHeight",
                typeof(double),
                typeof(ColorSelectorButton),
                new PropertyMetadata(0));

        public static readonly DependencyProperty IconMarginProperty =
            DependencyProperty.Register(
                "IconMargin",
                typeof(Thickness),
                typeof(ColorSelectorButton),
                new PropertyMetadata(new Thickness(6)));

        #endregion

        #region Private Fields
        private bool _opacityVisibile;
        private bool _noColorVisible;
        private bool _isAlreadySubscribed;
        #endregion

        #region Events

        public event ColorSelection ColorSelectedEvent;

        #endregion
        
        #region Properties

        public ImageSource IconSource
        {
            get { return (ImageSource)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        public Color UserColor
        {
            get { return (Color)GetValue(UserColorProperty); }
            set
            {
                SetValue(UserColorProperty, value);
                ColorPicker.SelectColorInList(value);
            }
        }

        public Color DefaultColor
        {
            get { return (Color)GetValue(DefaultColorProperty); }
            set { SetValue(DefaultColorProperty, value); }
        }

        public Thickness RectangleMargin
        {
            get { return (Thickness)GetValue(RectangleMarginProperty); }
            set { SetValue(RectangleMarginProperty, value); }
        }

        public double RectangleWidth
        {
            get { return (double)GetValue(RectangleWidthProperty); }
            set { SetValue(RectangleWidthProperty, value); }
        }

        public double RectangleHeight
        {
            get { return (double)GetValue(RectangleHeightProperty); }
            set { SetValue(RectangleHeightProperty, value); }
        }
        public Thickness IconMargin
        {
            get { return (Thickness)GetValue(IconMarginProperty); }
            set { SetValue(IconMarginProperty, value); }
        }

        public bool OpacityVisibile
        {
            get
            {
                return _opacityVisibile;
            }
            set
            {
                _opacityVisibile = value;
                ColorPicker.OpacityVisibility = _opacityVisibile;
            }
        }

       public bool NoColorVisible
        {
            get
            {
                return _noColorVisible;
            }
            set
            {
                _noColorVisible = value;
                ColorPicker.NoColorVisibility = _noColorVisible;
            }
        }

        #endregion

        #region Life Cycle

        public ColorSelectorButton()
        {
            InitializeComponent();
            Loaded += ColorSelectorButton_Loaded;
        }

#if DEBUG
        ~ColorSelectorButton()
        {
            System.Diagnostics.Debug.WriteLine("************* ColorSelectorButton Destructor *************");
        }
#endif
        #endregion

        #region Callbacks

        private void ColorSelectorButton_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += ColorSelectorButton_Unloaded;
            ColorPicker.MoreColorClickedEvent += ColorPicker_MoreColorClickedEvent;
            Subscribe();
            ColorPicker.DefaultColor = DefaultColor;
        }

        private void ColorSelectorButton_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ColorSelectorButton_Loaded;
            Unloaded -= ColorSelectorButton_Unloaded;
            ColorPicker.MoreColorClickedEvent -= ColorPicker_MoreColorClickedEvent;
            Unsubscribe();
        }

        private void MainButton_Checked(object sender, RoutedEventArgs e)
        {
            ColorFlyout.ShowAt(this);
        }

        private void MainButton_Unchecked(object sender, RoutedEventArgs e)
        {
            ColorFlyout.Hide();
        }

        private void ColorFlyout_Closed(object sender, object e)
        {
            if (MainButton.IsChecked == true)
            {
                MainButton.IsChecked = false;
            }
        }

        private async void ColorPicker_MoreColorClickedEvent(Color color)
        {
            ColorFlyout.Hide();
            var dialog = new Pitch.UILayer.Helpers.CustomControls.ColorPickerDialog();
            dialog.ColorSelectedEvent += Dialog_ColorSelectedEvent;
            dialog.Color = color;
            await dialog.ShowAsync();
            dialog.ColorSelectedEvent -= Dialog_ColorSelectedEvent;
        }

        private void Dialog_ColorSelectedEvent(Color color)
        {
            color.A = ColorPicker.Opacity;
            UserColor = color;
            ColorSelectedEvent?.Invoke(color);
        }

        private void ColorPicker_ColorSelectedEvent(Color color)
        {
            UserColor = color;
            ColorFlyout.Hide();
            ColorSelectedEvent?.Invoke(color);
        }

        private void ColorPicker_ColorOpacityChangeEvent(byte opacity)
        {
            UserColor = Color.FromArgb(opacity, UserColor.R, UserColor.G, UserColor.B);
            ColorSelectedEvent?.Invoke(UserColor);
        }

        #endregion

        #region Private Methods

        
        private void Subscribe()
        {
            if (!_isAlreadySubscribed)
            {
                MainButton.Checked += MainButton_Checked;
                MainButton.Unchecked += MainButton_Unchecked;
                ColorFlyout.Closed += ColorFlyout_Closed;
                ColorPicker.ColorSelectedEvent += ColorPicker_ColorSelectedEvent;
                ColorPicker.ColorOpacityChangeEvent += ColorPicker_ColorOpacityChangeEvent;

                _isAlreadySubscribed = true;
            }
        }

        private void Unsubscribe()
        {
            MainButton.Checked -= MainButton_Checked;
            MainButton.Unchecked -= MainButton_Unchecked;
            ColorFlyout.Closed -= ColorFlyout_Closed;
            ColorPicker.ColorSelectedEvent -= ColorPicker_ColorSelectedEvent;
            ColorPicker.ColorOpacityChangeEvent -= ColorPicker_ColorOpacityChangeEvent;

            _isAlreadySubscribed = false;
        }

        #endregion

        #region Public Methods

        public void SetColor(Color color)
        {
            Unsubscribe();

            UserColor = color;

            Subscribe();
        }

        #endregion
    }
}
