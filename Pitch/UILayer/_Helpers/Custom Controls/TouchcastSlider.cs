﻿namespace Pitch.UILayer.Helpers.Controls
{
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;

    public class SliderValueChangeCompletedEventArgs : RoutedEventArgs
    {
        private readonly double _value;

        public double Value { get { return _value; } }

        public SliderValueChangeCompletedEventArgs(double value)
        {
            _value = value;
        }
    }

    public delegate void SlideValueChangeCompletedEventHandler(object sender, SliderValueChangeCompletedEventArgs args);

    public class TouchcastSlider : Slider
    {
        // Slider manipulation
        public event SlideValueChangeCompletedEventHandler ThumbChangeCompleted;
        private bool dragging = false;

        // Markers
        public static DependencyProperty PlaceholderProperty = DependencyProperty.RegisterAttached(
          "Placeholder",
          typeof(Canvas),
          typeof(TouchcastSlider),
          new PropertyMetadata(new Canvas()));

        public static void SetPlaceholder(UIElement element, Canvas value)
        {
            element.SetValue(PlaceholderProperty, value);
        }

        public static Canvas GetPlaceholder(UIElement element)
        {
            return (Canvas)element.GetValue(PlaceholderProperty);
        }

        public TouchcastSlider()
        {
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var thumb = base.GetTemplateChild("HorizontalThumb") as Thumb;
            if (thumb != null)
            {
                thumb.DragStarted += ThumbOnDragStarted;
                thumb.DragCompleted += ThumbOnDragCompleted;
            }
            // we don't use vertical slider
            //thumb = base.GetTemplateChild("VerticalThumb") as Thumb;
            //if (thumb != null)
            //{
            //    thumb.DragStarted += ThumbOnDragStarted;
            //    thumb.DragCompleted += ThumbOnDragCompleted;
            //}
        }

        public void ThumbOnDragStarted(object sender, DragStartedEventArgs e)
        {
            this.dragging = true;
        }

        private void ThumbOnDragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.dragging = false;
            this.OnThumbChangeCompleted(this.Value);
        }

        protected void OnThumbChangeCompleted(double value)
        {
            this.ThumbChangeCompleted?.Invoke(this, new SliderValueChangeCompletedEventArgs(value));
        }

        protected override void OnMaximumChanged(System.Double oldMaximum, System.Double newMaximum)
        {
            base.OnMaximumChanged(oldMaximum, newMaximum);
        }

        protected override void OnMinimumChanged(System.Double oldMinimum, System.Double newMinimum)
        {
            base.OnMinimumChanged(oldMinimum, newMinimum);
        }

        protected override void OnValueChanged(double oldValue, double newValue)
        {
            // inform only if playing (automatic mode)
            if (!this.dragging)
            {
                base.OnValueChanged(oldValue, newValue);
            }
        }
    }
}
