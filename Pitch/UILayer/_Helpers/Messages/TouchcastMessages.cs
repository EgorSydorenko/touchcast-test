﻿using GalaSoft.MvvmLight.Messaging;
using System;
using Windows.UI.Xaml;
using Pitch.Models;
using Pitch.UILayer.Authoring.Views;

namespace Pitch.UILayer.Helpers.Messages
{
    #region Overlays messages

    public class BusyIndicatorShowMessage : MessageBase
    {
        public string Message { get; set; }

        public string Title { get; set; }

        public BusyIndicatorShowMessage(string message = null, string title = null)
        {
            Message = message;
            Title = title;
        }
    }

    public class BusyIndicatorHideMessage : MessageBase { }

    public class TeleprompterEditorPopupShowMessage : MessageBase { }

    public class TeleprompterEditorPopupHideMessage : MessageBase { }
   
    public class ShowTimeInOutMessage : MessageBase
    {
        public FrameworkElement Control { get; }
        public ShowTimeInOutMessage(FrameworkElement control)
        {
            Control = control;
        }
    }

    public class HideTimeInOutMessage : MessageBase
    {
        public FrameworkElement Control { get; }
        public HideTimeInOutMessage(FrameworkElement control)
        {
            Control = control;
        }
    }
    #endregion

    #region Toolbar messages

    public class HideIntroOverlayMessage : MessageBase { }

    public class StartRecordingMessage : MessageBase { }

	public class NewTakeInitializedMessage : MessageBase { }

	public class TeleprompterScriptIncreaseSpeedMessage : MessageBase { }

	public class TeleprompterScriptDecreaseSpeedMessage : MessageBase { }

	public class UpdateTimeMessage : MessageBase
    {
        public TimeSpan Duration { get; private set; }

        public UpdateTimeMessage(TimeSpan time)
        {
            Duration = time;
        }
    }

    public class OpenNewProjectMessage : MessageBase
    {
        public string FileTocken { get; }
        public OpenNewProjectMessage(string fileTocken)
        {
            FileTocken = fileTocken;
        }
    }

    public class StartCountingMessage : MessageBase { }

    public class CancelCountingMessage : MessageBase { }

    public class PlayPlayerMessage : MessageBase { }

    public class DisableExportButtonMessage : MessageBase { }

    public class PausePlayerMessage : MessageBase { }

    public class SwitchSplitButtonEnableMessage : MessageBase
    {
        public bool IsEnabled { get; }
        public SwitchSplitButtonEnableMessage(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }
    }

    public class ShowUnsupportedFormatMessage : MessageBase { }
    
    #endregion

    #region Titlebar messages

    public class SubscribeUndoRedoButtonsMessage : MessageBase { }

    #endregion

    #region Canvas messages

    public class ChromakeySwitchColorPointMessage : MessageBase
    {
        public bool IsEnabled { set; get; }
        public ChromakeySwitchColorPointMessage(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }
    }

    #endregion

    #region Open Project Errors messages

    public class ProjectOpenErrorMessage : MessageBase
    {
        public string ProjectFileName { get; }
        public ProjectOpenErrorMessage(string projectFileName)
        {
            ProjectFileName = projectFileName;
        }
    }

    public class ProjectCorruptedMessage : MessageBase
    {
        public OpenProjecFileErrorType Type { get; private set; }

        public ProjectCorruptedMessage(OpenProjecFileErrorType t)
        {
            Type = t;
        }
    }

    #endregion

    #region Power Managment messages

    public class ShowBatteryStatusPopup : MessageBase { }

    public class HideBatteryStatusPopup : MessageBase { }

    #endregion
}
