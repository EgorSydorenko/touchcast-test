﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using Pitch.UILayer.Authoring.Views;
using Pitch.UILayer.Authoring.Views.FileMenu;
using Pitch.UILayer.Authoring.Views.Overlays;
using Pitch.UILayer.Helpers.Messages;
using Pitch.UILayer.IntroPage.Views;
using Pitch.UILayer.Recording.Views;
using System;
using System.Threading.Tasks;
using TfSdk;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Editing.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditingPage : Page
    {
        #region Constants

        private const string EditingPageSaveProjectDialogContent = "SaveProjectDialogContent";
        private const string EditingPageSaveProjectDialogTitle = "SaveProjectDialogTitle";
        private const string EditingPageSaveProjectDialogSaveLabel = "SaveProjectDialogSaveLabel";
        private const string EditingPageSaveProjectDialogDoNotSaveLabel = "SaveProjectDialogDoNotSaveLabel";
        private const string EditingPageSaveProjectDialogCancelLabel = "SaveProjectDialogCancelLabel";

        private readonly string SaveProjectDialogContent;
        private readonly string SaveProjectDialogTitle;
        private readonly string SaveProjectDialogSaveLabel;
        private readonly string SaveProjectDialogDoNotSaveLabel;
        private readonly string SaveProjectDialogCancelLabel;

        #endregion

        #region Private Fields

        private FileMenuView _fileMenuView;
        private BusyIndicatorOverlay _busyOverlay;
        private MessageDialog _saveProjectDialog;
        private WeakReference<AuthoringSession> _authoringSession;
        private TouchcastEditorView _editor;

        #endregion

        #region Properties

        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();
        #endregion

        #region Life Cycle

        public EditingPage()
        {
            App.Locator.AppState = ApplicationState.Editing;
            InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            SaveProjectDialogContent = loader.GetString(EditingPageSaveProjectDialogContent);
            SaveProjectDialogTitle = loader.GetString(EditingPageSaveProjectDialogTitle);
            SaveProjectDialogSaveLabel = loader.GetString(EditingPageSaveProjectDialogSaveLabel);
            SaveProjectDialogDoNotSaveLabel = loader.GetString(EditingPageSaveProjectDialogDoNotSaveLabel);
            SaveProjectDialogCancelLabel = loader.GetString(EditingPageSaveProjectDialogCancelLabel);

            InitializeFileMenu();
            InitializeSaveMessageDialog();
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session != null)
            {
                _authoringSession = new WeakReference<AuthoringSession>(session);
            }
            Loaded += EditingPage_Loaded;
        }
#if DEBUG
        ~EditingPage()
        {
            System.Diagnostics.Debug.WriteLine("********************EditingPage Destructor********************");
        }
#endif

        private void EditingPage_Loaded(object sender, RoutedEventArgs e)
        {
            ApiClient.Shared.ApiClientStateChangedEvent += Shared_ApiClientStateChangedEvent;
            if (_editor == null)
            {
                _editor = new TouchcastEditorView();
                Grid.SetRow(_editor, 2);
                LayoutRoot.Children.Add(_editor);
            }
            _busyOverlay = new BusyIndicatorOverlay();
            Grid.SetRowSpan(_busyOverlay, 4);
            RegisterMessages();
            _editor.ActiveTcMediaClipChangedEvent += Editor_ActiveTcMediaClipChangedEvent;
            _editor.MediaPlayerStateChangedEvent += Editor_MediaPlayerStateChangedEvent;
            Windows.ApplicationModel.Core.CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(TitleBar.MovementContainer);
            EditingToolbar.RetakeShotEvent += EditingToolbar_RetakeShotEvent;
            EditingToolbar.RecordMoreAtEndEvent += EditingToolbar_RecordMoreAtEndEvent;
            Unloaded += EditingPage_Unloaded;
            StatusPanel.ZommValueChangedEvent += StatusPanel_ZommValueChangedEvent;

            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.EditorUndoRedoOriginator.UndoRedoStacksChangedEvent += EditorUndoRedoOriginator_UndoRedoStacksChangedEvent;
                var state = session.Project.EditorUndoRedoOriginator.State;
                TitleBar.UpdateUndoRedoButtons(!state.IsUndoStackEmpty, !state.IsRedoStackEmpty);
                TitleBar.UpdateModeSwitcherState(true, true);
                session.Project.TcMediaComposition.MediaCompositionEmptyEvent += TcMediaComposition_MediaCompositionEmptyEvent;
                var title = session.Project.Name;
                if (!String.IsNullOrEmpty(title))
                {
                    title = " - " + title;
                }
                title = String.Format("TouchCast Pitch{0}", title);
                TitleBar.UpdateTitle(title);
            }

            TitleBar.UndoButtonPressedEvent += TitleBar_UndoButtonPressedEvent;
            TitleBar.RedoButtonPressedEvent += TitleBar_RedoButtonPressedEvent;
            TitleBar.SignOutPressedEvent += TitleBar_SignOutPressedEvent;
            TitleBar.ChangeModeEvent += ModeSwitcher_ChangeModeEvent;
            
            EditingToolbar.SwitchToFileMenuEvent += EditingToolbar_SwitchToFileMenuEvent;
            EditingToolbar.OpenProjectEvent += EditingToolbar_OpenProjectEvent;
            EditingToolbar.SaveProjectEvent += EditingToolbar_SaveProjectEvent;

            _fileMenuView.NewProjectEvent += FileMenuView_NewProjectEvent;
            _fileMenuView.OpenProjectEvent += FileMenuView_OpenProjectEvent;
            _fileMenuView.CloseMenuEvent += FileMenuView_CloseMenuEvent;
            _fileMenuView.CloseProjectEvent += FileMenuView_CloseProjectEvent;
            _fileMenuView.LogoutMenuEvent += FileMenuView_LogoutMenuEvent;
            _editor.Show();
        }

        private void Shared_ApiClientStateChangedEvent()
        {
            try
            {
                var state = ApiClient.Shared.Condition;
                if (state == ApiClient.State.Authorized)
                {
                    Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticCommandUse);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void EditingPage_Unloaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.Children.Clear();
            EditingToolbar.SwitchToFileMenuEvent -= EditingToolbar_SwitchToFileMenuEvent;
            EditingToolbar.OpenProjectEvent -= EditingToolbar_OpenProjectEvent;
            EditingToolbar.SaveProjectEvent -= EditingToolbar_SaveProjectEvent;
            Loaded -= EditingPage_Loaded;
            Unloaded -= EditingPage_Unloaded;
        }

        protected override async void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            await _editor.Cleanup();
            ApiClient.Shared.ApiClientStateChangedEvent -= Shared_ApiClientStateChangedEvent;
            LayoutRoot.Children.Remove(_editor);
            _editor.ActiveTcMediaClipChangedEvent -= Editor_ActiveTcMediaClipChangedEvent;
            _editor.MediaPlayerStateChangedEvent -= Editor_MediaPlayerStateChangedEvent;

            Messenger.Default.Unregister(this);

            EditingToolbar.RetakeShotEvent -= EditingToolbar_RetakeShotEvent;
            EditingToolbar.RecordMoreAtEndEvent -= EditingToolbar_RecordMoreAtEndEvent;

            StatusPanel.ZommValueChangedEvent -= StatusPanel_ZommValueChangedEvent;
            TitleBar.UndoButtonPressedEvent -= TitleBar_UndoButtonPressedEvent;
            TitleBar.RedoButtonPressedEvent -= TitleBar_RedoButtonPressedEvent;
            TitleBar.SignOutPressedEvent -= TitleBar_SignOutPressedEvent;
            TitleBar.ChangeModeEvent -= ModeSwitcher_ChangeModeEvent;
            EditingToolbar.SwitchToFileMenuEvent -= EditingToolbar_SwitchToFileMenuEvent;

            _fileMenuView.NewProjectEvent -= FileMenuView_NewProjectEvent;
            _fileMenuView.OpenProjectEvent -= FileMenuView_OpenProjectEvent;
            _fileMenuView.CloseMenuEvent -= FileMenuView_CloseMenuEvent;
            _fileMenuView.CloseProjectEvent -= FileMenuView_CloseProjectEvent;
            _fileMenuView.LogoutMenuEvent -= FileMenuView_LogoutMenuEvent;

            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.EditorUndoRedoOriginator.UndoRedoStacksChangedEvent -= EditorUndoRedoOriginator_UndoRedoStacksChangedEvent;
                session.Project.TcMediaComposition.MediaCompositionEmptyEvent -= TcMediaComposition_MediaCompositionEmptyEvent;
            }
        }

        #endregion

        #region Callbacks
        private async void EditingToolbar_SaveProjectEvent(object sender, EventArgs e)
        {
            await _fileMenuView.ViewModel.ExecuteMethodByKey(FileMenuKey.SaveMenu);
        }

        private async void EditingToolbar_OpenProjectEvent(object sender, EventArgs e)
        {
            bool saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult != false)
            {
                await OpenProjectFromFile();
            }
        }

        private void Editor_ActiveTcMediaClipChangedEvent(object sender, TcMediaClip clip)
        {
            EditingToolbar.ProcessWithNewMediClip(clip);
        }

        private void Editor_MediaPlayerStateChangedEvent(bool isPlaying)
        {
            EditingToolbar.IsSceneEffectEnabled = !isPlaying;
        }

        private void EditorUndoRedoOriginator_UndoRedoStacksChangedEvent(Pitch.DataLayer.UndoRedo.UndoRedoStacksChangedEventArgs args)
        {
            TitleBar.UpdateUndoRedoButtons(!args.IsUndoStackEmpty, !args.IsRedoStackEmpty);
        }

        private void StatusPanel_ZommValueChangedEvent(object sender, double step)
        {
            _editor.UpdateZoom(step);
        }

        private async void ModeSwitcher_ChangeModeEvent(ApplicationState futureState)
        {
            if (futureState == ApplicationState.Preparation)
            {
                await SwitchToAuthoringPage();
            }
            else if (futureState == ApplicationState.Recording)
            {
                await SwitchToRecordingPage();
            }
        }

        private async void TcMediaComposition_MediaCompositionEmptyEvent()
        {
            await SwitchToAuthoringPage();
        }

        private async void TitleBar_UndoButtonPressedEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.EditorUndoRedoOriginator.Undo();
            }
        }

        private async void TitleBar_RedoButtonPressedEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.EditorUndoRedoOriginator.Redo();
            }
        }

        private async void TitleBar_SignOutPressedEvent()
        {
            await SignOut();
        }

        private async void Shared_ApiClientChangedEvent()
        {
            ApiClient.Shared.ApiClientStateChangedEvent -= Shared_ApiClientChangedEvent;
            if (ApiClient.Shared.Condition == ApiClient.State.NotAuthorized)
            {
                var session = AuthoringSession;
                if (session != null)
                {
                    await session.Clean();
                    session.ShotManager?.Clean();
                    session.Reset();
                }
                App.Frame.Navigate(typeof(SignInPage), null, new DrillInNavigationTransitionInfo());
                App.Locator.UnRegisterAuthoringSession();
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }
        }

        private async void EditingToolbar_RetakeShotEvent()
        {
            PreparingSwitchToRecordPage(false);
            await SwitchToRecordingPage(true);
            Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandRetakeSceneInEditing);
        }

        private async void EditingToolbar_RecordMoreAtEndEvent()
        {
            PreparingSwitchToRecordPage(true);
            await SwitchToRecordingPage(true);
        }

        private void EditingToolbar_SwitchToFileMenuEvent(FileMenuKey menuKey)
        {
            SwitchToFileMenu(menuKey);
        }

        private async void FileMenuView_NewProjectEvent(object sender, EventArgs e)
        {
            var saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult)
            {
                NavigateToPage(typeof(NewThemePage));
            }
            else
            {
                _fileMenuView.SwitchToPreviousMenu();
            }
        }

        private async void FileMenuView_OpenProjectEvent(object sender, EventArgs e)
        {
            await OpenProjectFromFile();
        }

        private async void FileMenuView_CloseProjectEvent(object sender, EventArgs e)
        {
            var saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult)
            {
                await SwitchToAuthoringPage(new NavigateRequest(NavigateReason.CloseProject));
            }
            else
            {
                _fileMenuView.SwitchToPreviousMenu();
            }
        }
        private async void FileMenuView_LogoutMenuEvent(object sender, EventArgs e)
        {
            await SignOut();
        }

        private void FileMenuView_CloseMenuEvent(object sender, System.EventArgs e)
        {
            CloseFileMenu();
        }

        #endregion

        #region Private Methods

        private void PreparingSwitchToRecordPage(bool isMoreAtEnd)
        {

        }

        private async Task SwitchToRecordingPage(bool isStartRecord = false)
        {
            var mediaMixer = SimpleIoc.Default.GetInstance<Pitch.Services.IMediaMixerService>();
            if (mediaMixer != null)
            {
                mediaMixer.InitWithCamera();
                await mediaMixer.StartPreviewingAsync();
            }
            App.Locator.RegisterRecordingManager();
            App.Frame.Navigate(typeof(RecordingScreenPage), isStartRecord);
            App.Frame.BackStack.Clear();
            App.Frame.ForwardStack.Clear();
            App.Frame.CacheSize = 0;
        }

        private async Task SwitchToAuthoringPage(NavigateRequest request = null)
        {
            var mediaMixer = SimpleIoc.Default.GetInstance<Pitch.Services.IMediaMixerService>();
            if (mediaMixer != null)
            {
                mediaMixer.InitWithCamera();
                await mediaMixer.StartPreviewingAsync();
            }
            App.Frame.Navigate(typeof(AuthoringPage), request);
            App.Frame.BackStack.Clear();
            App.Frame.ForwardStack.Clear();
            App.Frame.CacheSize = 0;
        }

        private void InitializeFileMenu()
        {
            _fileMenuView = new FileMenuView()
            {
                Visibility = Visibility.Collapsed,
            };

            Grid.SetRow(_fileMenuView, 1);
            Grid.SetRowSpan(_fileMenuView, 3);
            Canvas.SetZIndex(_fileMenuView, 500);

            if (!LayoutRoot.Children.Contains(_fileMenuView))
            {
                LayoutRoot.Children.Add(_fileMenuView);
            }
        }

        private void SwitchToFileMenu(FileMenuKey menuKey)
        {
            _editor?.PauseMediaContent();
            _fileMenuView.PrepareBeforeShow(menuKey);
            _fileMenuView.Visibility = Visibility.Visible;
            EditingToolbar.IsEnabled = false;
            StatusPanel.IsEnabled = false;
            _editor.IsEnabled = false;
            TitleBar.UndoRedoControlsVisibility = Visibility.Collapsed;
            TitleBar.ModeSwitcherVisibility = Visibility.Collapsed;
            TitleBar.UpdateTitle();
            TitleBar.SetDarkMode();
        }

        private void CloseFileMenu()
        {
            _fileMenuView.PrepareBeforeHiding();
            _fileMenuView.Visibility = Visibility.Collapsed;
            EditingToolbar.IsEnabled = true;
            StatusPanel.IsEnabled = true;
            _editor.IsEnabled = true;
            EditingToolbar.SwitchToEffectsItem();
            TitleBar.UndoRedoControlsVisibility = Visibility.Visible;
            TitleBar.ModeSwitcherVisibility = Visibility.Visible;
            TitleBar.UpdateTitle();
            TitleBar.SetDefaultMode();
        }

        private void NavigateToPage(Type target)
        {
            App.Locator.UnRegisterAuthoringSession();
            App.Frame.Navigate(target);
            App.Frame.BackStack.Clear();
            App.Frame.ForwardStack.Clear();
            App.Frame.CacheSize = 0;
        }

        private void RegisterMessages()
        {
            Messenger.Default.Register<BusyIndicatorShowMessage>(this, (m) =>
            {
                LayoutRoot.Children.Add(_busyOverlay);
            });
            Messenger.Default.Register<BusyIndicatorHideMessage>(this, (m) =>
            {
                LayoutRoot.Children.Remove(_busyOverlay);
            });

            Messenger.Default.Register<OpenNewProjectMessage>(this, async (m) =>
            {
                if (await SaveProjectIfNeeded())
                {
                    await SwitchToAuthoringPage(new NavigateRequest(NavigateReason.OpenProjectByToken, m.FileTocken));
                }
            });
        }

        //TODO Duplicate Methods (see AuthoringPage)
        private void InitializeSaveMessageDialog()
        {
            _saveProjectDialog = new MessageDialog(SaveProjectDialogContent, SaveProjectDialogTitle);
            _saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogSaveLabel));
            _saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogDoNotSaveLabel));
            _saveProjectDialog.Commands.Add(new UICommand(SaveProjectDialogCancelLabel));
        }

        //TODO Duplicate Methods (see AuthoringPage)
        private async Task<bool> SaveProjectIfNeeded()
        {
            var result = true;
            var session = AuthoringSession;
            if (session != null && session.Project.HasUnsavedChanges)
            {
                result = await ShowSaveProjectDialog();
            }

            return result;
        }

        //TODO Duplicate Methods (see AuthoringPage)
        private async Task<bool> ShowSaveProjectDialog()
        {
            var result = true;
            var session = AuthoringSession;
            if (session != null)
            {
                if (session.Project.HasUnsavedChanges)
                {
                    var saveProjectDialogResult = await _saveProjectDialog.ShowAsync();
                    if (saveProjectDialogResult.Label == SaveProjectDialogSaveLabel)
                    {
                        try
                        {
                            var fileToSave = await FileToAppHelper.SaveFileDialog(FileToAppHelper.TouchcastProjectExtension, "Touchcast project", session.Project.Name);
                            if (fileToSave != null)
                            {
                                Messenger.Default.Send(new BusyIndicatorShowMessage());
                                await session.Project.SaveAs(fileToSave);
                            }
                        }
                        catch (Exception ex)
                        {
                            result = false;
                            LogQueue.WriteToFile($"Exception in AuthoringPage.ShowSaveProjectDialog : {ex.Message}");
                        }
                        finally
                        {
                            Messenger.Default.Send(new BusyIndicatorHideMessage());
                        }
                    }
                    if (saveProjectDialogResult.Label == SaveProjectDialogCancelLabel)
                    {
                        result = false;
                    }
                }
            }

            return result;
        }

        private async Task OpenProjectFromFile()
        {
            var saveChangesResult = await SaveProjectIfNeeded();
            if (saveChangesResult)
            {
                await SwitchToAuthoringPage(new NavigateRequest(NavigateReason.OpenProjectFromFile));
            }
            else
            {
                _fileMenuView.SwitchToPreviousMenu();
            }
        }

        private async Task SignOut()
        {
            var saveIfneededResult = await SaveProjectIfNeeded();
            if (saveIfneededResult)
            {
                ApiClient.Shared.ApiClientStateChangedEvent += Shared_ApiClientChangedEvent;
                await ApiClient.Shared.SignOut();
            }
        }
        #endregion
    }
}
