﻿using System;
using Pitch.DataLayer;
using Windows.UI.Xaml.Controls;

namespace Pitch.UILayer.Editing.Views
{
    public enum CompositionItemTypes
    {
        Clip,
        Transition
    }

    public abstract class ICompositionItemView : UserControl
    {
        public CompositionItemTypes ItemType { get; protected set; }
        public abstract TimeSpan StartTimeInComposition { get; }
        public abstract TimeSpan EndTimeInComposition { get; }
        public abstract TimeSpan TrimmedDuration { get; }
        public abstract TcMediaClip TcMediaClip { get; }
    }
}
