﻿using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views
{
    public delegate void ZommValueChanged(object sender, double step);
    public sealed partial class EditorStatusbarView : UserControl
    {
        public event ZommValueChanged ZommValueChangedEvent;
        public static double StartZoomValue = 6;
        public double ZoomValue => ZoomSlider.Value;
        public EditorStatusbarView()
        {
            InitializeComponent();
        }

#if DEBUG
        ~EditorStatusbarView()
        {
            System.Diagnostics.Debug.WriteLine("********************EditorStatusbarView Destructor********************");
        }
#endif

        private void IncreazeZoom_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ZoomIn();
        }

        private void DecreaseZoom_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ZoomOut();
        }

        private void ZoomIn()
        {
            ZoomSlider.Value -= ZoomSlider.StepFrequency;
        }

        private void ZoomOut()
        {
            ZoomSlider.Value += ZoomSlider.StepFrequency;
        }

        private void RangeBase_OnValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            ZommValueChangedEvent?.Invoke(this, ZoomSlider.Value);
        }
    }
}
