﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views.PreviewViews
{
    public sealed partial class PreviewImageView : UserControl
    {
        #region Private fields
        private string _imageUrl;
        #endregion
        
        #region Life cycle
        public PreviewImageView(string imageUrl)
        {
            _imageUrl = imageUrl;
            InitializeComponent();
            Loaded += PreviewImageView_Loaded;
        }

#if DEBUG
        ~PreviewImageView()
        {
            System.Diagnostics.Debug.WriteLine("******************** PreviewImageView Destructor********************");
        }
#endif

        private void PreviewImageView_Loaded(object sender, RoutedEventArgs e)
        {
            if (_imageUrl != null)
            {
                try
                {
                    BitmapImage bmp = new BitmapImage(new Uri(_imageUrl));
                    ImageView.Source = bmp;
                }
                catch(Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile($"Exception in PreviewImageView.PreviewImageView_Loaded : {ex.Message}");
                }
            }
            Unloaded += PreviewImageView_Unloaded;
        }

        private void PreviewImageView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= PreviewImageView_Loaded;
            Unloaded -= PreviewImageView_Unloaded;
        }
        #endregion
    }
}
