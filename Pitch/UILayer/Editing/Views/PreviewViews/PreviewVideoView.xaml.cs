﻿using System;
using Pitch.Helpers.Logger;
using Pitch.UILayer.Helpers.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views.PreviewViews
{
    public sealed partial class PreviewVideoView : UserControl
    {
        #region Private Fields

        private string _videoFileUrl;
        private TouchcastTransportControl _touchcastTransportControl;

        #endregion

        #region Life Cycle

        public PreviewVideoView(string videoFileUrl)
        {
            _videoFileUrl = videoFileUrl;
            InitializeComponent();
            Loaded += PreviewVideoView_Loaded;

            _touchcastTransportControl = new TouchcastTransportControl
            {
                IsStopButtonVisible = true,
                IsStopEnabled = true,
                IsPlaybackRateEnabled = false,
                IsPlaybackRateButtonVisible = false,
                IsFastForwardButtonVisible = false,
                IsFastForwardEnabled = false,
                IsFastRewindButtonVisible = false,
                IsFastRewindEnabled = false,
                IsFullWindowButtonVisible = false,
                IsZoomButtonVisible = false,
                IsVolumeButtonVisible = false,
                IsVolumeEnabled = false,
                IsCompact = true,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                TouchcastVolumeVisibility = Visibility.Visible
            };
            Player.TransportControls = _touchcastTransportControl;
            Player.AreTransportControlsEnabled = true;
        }

#if DEBUG
        ~PreviewVideoView()
        {
            System.Diagnostics.Debug.WriteLine("******************** PreviewVideoView Destructor********************");
        }
#endif

        private void PreviewVideoView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += PreviewVideoView_Unloaded;
            _touchcastTransportControl.VolumeChanged += TouchcastTransportControl_VolumeChanged;
            if (_videoFileUrl != null)
            {
                try
                {
                    Player.Source = new Uri(_videoFileUrl);
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in PreviewVideoView.PreviewVideoView_Loaded : {ex.Message}");
                }
            }
        }

        private void PreviewVideoView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= PreviewVideoView_Loaded;
            Unloaded -= PreviewVideoView_Unloaded;
            _touchcastTransportControl.VolumeChanged -= TouchcastTransportControl_VolumeChanged;
        }

        #endregion

        #region Callbacks

        private void TouchcastTransportControl_VolumeChanged(object sender, VolumeChangedEventArgs e)
        {
            Player.Volume = e.NewVolume;
        }

        #endregion

        #region Public Methods

        public void PauseMultimediaContent()
        {
            if (Player.CurrentState == MediaElementState.Playing)
            {
                Player.Pause();
            }
        }

        #endregion
    }
}
