﻿using System;
using System.Threading.Tasks;
using Windows.Data.Pdf;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.Helpers.Logger;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views.PreviewViews
{
    public sealed partial class PreviewDocumentView : UserControl
    {
        #region Private fields
        private const uint PagesPerLoad = 4;
        private string _url;
        private PdfDocument _pdfDoc;
        private StorageFolder _localCacheFolder;
        private PdfPageRenderOptions _pdfPageRenderOptions = new PdfPageRenderOptions();
        private uint _currentPage;
        private double _pageHeight;
        private ScrollViewer _scroll;
        private bool _isLoading;
        #endregion

        #region Life cycle
        public PreviewDocumentView(string url)
        {
            _url = url;
            InitializeComponent();
            Loaded += PreviewDocumentView_Loaded;
        }

#if DEBUG
        ~PreviewDocumentView()
        {
            System.Diagnostics.Debug.WriteLine("******************** PreviewDocumentView Destructor********************");
        }
#endif

        private async void PreviewDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += PreviewDocumentView_Unloaded;
            if (_url != null)
            {
                try
                {
                    var storageFile = await StorageFile.GetFileFromPathAsync(_url);
                    _pdfDoc = await PdfDocument.LoadFromFileAsync(storageFile);
                    _localCacheFolder = await ApplicationData.Current.LocalCacheFolder.CreateFolderAsync(Guid.NewGuid().ToString(), CreationCollisionOption.OpenIfExists);
                    var page = _pdfDoc.GetPage(0);
                    _pageHeight = page.Size.Height;
                    await LoadNextPages();
                    ListViewControl.Visibility = Visibility.Visible;
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in PreviewDocumentView.PreviewDocumentView_Loaded : {ex.Message}");
                }
                _scroll = Pitch.Helpers.UIHelper.FindChild<ScrollViewer>(ListViewControl, null);
                if (_scroll != null)
                {
                    _scroll.MinZoomFactor = 1;
                    _scroll.MaxZoomFactor = 10;
                    _scroll.ZoomMode = ZoomMode.Enabled;
                    _scroll.ViewChanged += ScrollViewer_ViewChanged;
                }
                SizeChanged += DocumentLayerLayoutView_SizeChanged;
            }
        }

        private void PreviewDocumentView_Unloaded(object sender, RoutedEventArgs e)
        {
            SizeChanged -= DocumentLayerLayoutView_SizeChanged;
            if (_scroll != null)
            {
                _scroll.ViewChanged -= ScrollViewer_ViewChanged;
            }
        }

        #endregion

        #region Callbacks

        private void DocumentLayerLayoutView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _scroll?.ChangeView(null, null, 1);
        }
        private async void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (_isLoading) return;
            _isLoading = true;
            if (_scroll.ExtentHeight - _scroll.VerticalOffset < _pageHeight * 2)
            {
                await LoadNextPages();
            }
            _isLoading = false;
        }
        #endregion

        #region Private methods
        private async Task LoadNextPages()
        {
            for (uint i = _currentPage; i < _pdfDoc.PageCount && i < _currentPage + PagesPerLoad; i++)
            {
                string fileName = $"{i}.jpg";
                var file = await _localCacheFolder.TryGetItemAsync(fileName);
                if (file == null)
                {
                    file = await _localCacheFolder.CreateFileAsync(fileName);
                    using (var page = _pdfDoc.GetPage(i))
                    {
                        _pdfPageRenderOptions.DestinationWidth = (uint)page.Size.Width;
                        _pdfPageRenderOptions.DestinationHeight = (uint)page.Size.Height;

                        using (IRandomAccessStream randomStream = await (file as StorageFile).OpenAsync(FileAccessMode.ReadWrite))
                        {
                            await page.RenderToStreamAsync(randomStream, _pdfPageRenderOptions);
                            await randomStream.FlushAsync();
                        }
                    }
                }

                if (file is StorageFile)
                {
                    ListViewControl.Items.Add(new Image
                    {
                        Stretch = Stretch.Uniform,
                        Source = new BitmapImage(new Uri(file.Path))
                    });
                }
            }
            _currentPage += PagesPerLoad;
        }
        #endregion
    }
}
