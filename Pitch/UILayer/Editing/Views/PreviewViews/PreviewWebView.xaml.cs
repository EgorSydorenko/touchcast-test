﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views.PreviewViews
{
    public sealed partial class PreviewWebView : UserControl
    {
        #region Constants
 
        private const string pauseScript = @"
    var videos = document.querySelectorAll('video'),
    audios = document.querySelectorAll('audio');
    [].forEach.call(videos, function(video) { if(!video.paused) video.pause(); });
    [].forEach.call(audios, function(audio) { if(!audio.paused) audio.pause(); }); ";

        private const string playScript = @"
    var videos = document.querySelectorAll('video'),
    audios = document.querySelectorAll('audio');
    [].forEach.call(videos, function(video) { if(video.paused) video.play(); });
    [].forEach.call(audios, function(audio) { if(audio.paused) audio.play(); }); ";

        #endregion

        #region Private Fields
        private string _url;
        private WebView _webView;
        #endregion

        #region Life Cycle

        public PreviewWebView(string Url)
        {
            _url = Url;
            InitializeComponent();
            Loaded += PreviewWebView_Loaded;
        }

#if DEBUG
        ~PreviewWebView()
        {
            System.Diagnostics.Debug.WriteLine("******************** PreviewWebView Destructor********************");
        }
#endif

        private void PreviewWebView_Loaded(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(_url))
            {
                try
                {
                    _webView = new WebView(WebViewExecutionMode.SeparateThread)
                    {
                        Source = new Uri(_url)
                    };
                    if (_webView != null)
                    {
                        LayoutRoot.Children.Add(_webView);
                    }
                }
                catch(Exception ex)
                {

                }


            }
            Unloaded += PreviewWebView_Unloaded;
        }

        private void PreviewWebView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= PreviewWebView_Loaded;
            Unloaded -= PreviewWebView_Unloaded;
            if (_webView != null)
            {
                _webView.Navigate(new Uri("ms-appx-web:///Resources/Styles/Html Pages/EmptyPage.html"));
            }
            LayoutRoot.Children.Clear();
            _webView = null;
        }

        #endregion

        #region Public Methods

        public async void PauseMultimediaContent()
        {
            if (_webView != null)
            {
                await _webView.InvokeScriptAsync("eval", new string[] { pauseScript });
            }
        }

        #endregion
    }
}
