﻿using Pitch.UILayer.Authoring.ViewModels.Toolbars;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Editing.Views.Toolbars
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ExportToolbarPanelView
    {
        private ExportToolbarPanelViewModel _viewModel;
        public ExportToolbarPanelViewModel ViewModel => _viewModel;

        public delegate void ExportButtonPressed();
        public event ExportButtonPressed ExportButtonPressedEvent;

        public ExportToolbarPanelView()
        {
            _viewModel = new ExportToolbarPanelViewModel();
            InitializeComponent();
            Loaded += ExportToolbarPanelView_Loaded;
        }

        private void ExportToolbarPanelView_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Unloaded += ExportToolbarPanelView_Unloaded;
        }

        private void ExportToolbarPanelView_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Unloaded -= ExportToolbarPanelView_Unloaded;
            Bindings.StopTracking();
            ViewModel?.Cleanup();
            _viewModel = null;
        }

        private void ExportButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ExportButtonPressedEvent?.Invoke();
        }
    }
}
