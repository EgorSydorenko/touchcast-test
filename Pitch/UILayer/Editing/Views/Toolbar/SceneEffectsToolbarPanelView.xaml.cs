﻿using System;
using System.Linq;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views.Toolbars
{
    public delegate void RetakeShot();
    public sealed partial class SceneEffectsToolbarPanelView : UserControl
    {
        #region Private Fields
        private bool _isNavigationStarted;
        private object _navigationLock = new object();
        private WeakReference<AuthoringSession> _authoringSession;
        private TcMediaClip _currentClip;
        private object _lockObject = new object();
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        #endregion

        public event RetakeShot RetakeShotEvent;
        public event RetakeShot RecordMoreAtEndEvent;

        public void Update(TcMediaClip clip)
        {
            lock (_lockObject)
            {
                _currentClip = clip;
                UpdateTakeEffectsToolbarUI();
            }
        }

        #region Life Cycle
        public SceneEffectsToolbarPanelView()
        {
            _authoringSession = new WeakReference<AuthoringSession>(GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>());
            InitializeComponent();

            Loaded += SceneEffectsToolbarPanelView_Loaded;
            Unloaded += SceneEffectsToolbarPanelView_Unloaded;
        }

#if DEBUG
        ~SceneEffectsToolbarPanelView()
        {
            System.Diagnostics.Debug.WriteLine("****************** SceneEffectsToolbarPanelView Destructor *********************");
        }
#endif

        private void SceneEffectsToolbarPanelView_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                //session.Project.TcMediaComposition.ActiveMediaClip.ActiveTakeWasChanged += ActiveMediaClip_ActiveTakeWasChanged;
                //session.Project.ActiveShot.TakeWasCreated += ActiveShot_TakeWasCreated;
                //session.Project.ActiveShot.TakeWasRemoved += ActiveShot_TakeWasRemoved;
            }
            UpdateTakeEffectsToolbarUI();
        }

        private void SceneEffectsToolbarPanelView_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                //session.Project.TcMediaComposition.ActiveMediaClip.ActiveTakeWasChanged -= ActiveMediaClip_ActiveTakeWasChanged;
                //session.Project.ActiveShot.TakeWasCreated -= ActiveShot_TakeWasCreated;
                //session.Project.ActiveShot.TakeWasRemoved -= ActiveShot_TakeWasRemoved;
            }
            Loaded -= SceneEffectsToolbarPanelView_Loaded;
            Unloaded -= SceneEffectsToolbarPanelView_Unloaded;
        }

        #endregion

        #region Callbacks
        private void ActiveMediaClip_ActiveTakeWasChanged(TcMediaClip mediaClip)
        {
            UpdateTakeEffectsToolbarUI();
        }
        private void ActiveShot_TakeWasRemoved(Take take)
        {
            UpdateTakeEffectsToolbarUI();
        }

        private void ActiveShot_TakeWasCreated(Take take)
        {
            UpdateTakeEffectsToolbarUI();
        }

        private void NextTakeButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var sesssion = AuthoringSession;
            if(_currentClip != null && sesssion != null)
            {
                _currentClip.NextTake();
                sesssion.CompositionBuilder.Refresh();
            }
        }

        private void PrevTakeButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var sesssion = AuthoringSession;
            if (_currentClip != null && sesssion != null)
            {
                _currentClip.PreviousTake();
                sesssion.CompositionBuilder.Refresh();
            }
        }

        private void RetakeButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            lock (_navigationLock)
            {
                if (_isNavigationStarted) return;
                if (_currentClip != null)
                {
                    _isNavigationStarted = true;
                    PrepareMediaCompositionToProcessing(false);
                    RetakeShotEvent?.Invoke();
                }
            }
        }

        private void RecordMoreButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            lock (_navigationLock)
            {
                if (_isNavigationStarted) return;
                if (_currentClip != null)
                {
                    _isNavigationStarted = true;
                    PrepareMediaCompositionToProcessing(true);
                    RecordMoreAtEndEvent?.Invoke();
                }
            }
        }

        #endregion

        #region Private Methods
        private void PrepareMediaCompositionToProcessing(bool isRecordMoreAtEnd)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.TcMediaComposition.MediaClipToProcess = _currentClip;
                session.Project.TcMediaComposition.IsRecordMoreAtEnd = isRecordMoreAtEnd;
                if (session.Project.Shots.FirstOrDefault(s => s.Id == _currentClip.ActiveTake?.Shot?.Id) != null)
                {
                    session.Project.ActiveShot = _currentClip.ActiveTake.Shot;
                }
            }
        }

        private void UpdateTakeEffectsToolbarUI()
        {
            lock (_lockObject)
            {
                if (_currentClip != null)
                {
                    if (_currentClip.ActiveTake != null)
                    {
                        var totalTakes = _currentClip.Takes.Count;
                        var currentTake = _currentClip.Takes.IndexOf(_currentClip.ActiveTake);
                        TotalTakes.Text = totalTakes > 0 ? $"of {totalTakes}" : "";
                        CurrentTake.Text = totalTakes > 0 ? (currentTake + 1).ToString() : "";

                        PrevTakeButton.IsEnabled = (totalTakes > 1) && (currentTake > 0);
                        NextTakeButton.IsEnabled = (totalTakes > 1) && (currentTake < totalTakes - 1);
                        var session = AuthoringSession;
                        if (session != null)
                        {
                            RecordMoreButton.IsEnabled = session.Project.Shots.Contains(_currentClip.ActiveTake.Shot);
                            RetakeButton.IsEnabled = session.Project.Shots.Contains(_currentClip.ActiveTake.Shot);
                        }
                        //this.Rec
                    }
                }
            }
            //var totalTakes = session.Project.ActiveShot.Takes.Count;
            //var currentTake = session.Project.ActiveShot.ActiveTakeIndex;
            //TotalTakes.Text = totalTakes > 0 ? $"of {totalTakes}" : "";
            //CurrentTake.Text = totalTakes > 0 ? (currentTake + 1).ToString() : "";

            //PrevTakeButton.IsEnabled = (totalTakes > 1) && (currentTake > 0);
            //NextTakeButton.IsEnabled = (totalTakes > 1) && (currentTake < totalTakes - 1);

        }

        #endregion
    }
}
