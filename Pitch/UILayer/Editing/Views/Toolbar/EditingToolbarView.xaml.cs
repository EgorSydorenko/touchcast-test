﻿using Pitch.DataLayer;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Pitch.UILayer.Authoring.ViewModels.FileMenu;
using System.Threading.Tasks;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views.Toolbars
{
    public sealed partial class EditingToolbarView : UserControl
    {
        public delegate void SwitchToFileMenu(FileMenuKey menuKey);

        #region Events
        public event RetakeShot RetakeShotEvent;
        public event RetakeShot RecordMoreAtEndEvent;
        public event SwitchToFileMenu SwitchToFileMenuEvent;
        public event EventHandler OpenProjectEvent;
        public event EventHandler SaveProjectEvent;
        #endregion

        public bool IsSceneEffectEnabled
        {
            set
            {
                SceneEffectsPanel.IsEnabled = value;
            }
        }

        #region Life Cycle

        public EditingToolbarView()
        {
            InitializeComponent();
            Loaded += EditingToolbarView_Loaded;
        }

        private void EditingToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            Pivot.SelectedItem = SceneEffectsPivotItem;
            SceneEffectsPanel.RetakeShotEvent += SceneEffectsPanel_RetakeShotEvent;
            SceneEffectsPanel.RecordMoreAtEndEvent += SceneEffectsPanel_RecordMoreAtEndEvent;
            Unloaded += EditingToolbarView_Unloaded;
            Pivot.SelectionChanged += Pivot_SelectionChanged;
            Shortcut.Manager.Instance.EditShortcutsManager.OpenShortcutEvent += ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.SaveShortcutEvent += ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NewShortcutEvent += ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ExportShortcutEvent += EditShortcutsManager_ExportShortcutEvent;
            ExportPanel.ExportButtonPressedEvent += ExportPanel_ExportButtonPressedEvent;
        }

        private void EditingToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            SceneEffectsPanel.RetakeShotEvent -= SceneEffectsPanel_RetakeShotEvent;
            SceneEffectsPanel.RecordMoreAtEndEvent -= SceneEffectsPanel_RecordMoreAtEndEvent;
            Unloaded -= EditingToolbarView_Unloaded;
            Pivot.SelectionChanged -= Pivot_SelectionChanged;
            Shortcut.Manager.Instance.EditShortcutsManager.OpenShortcutEvent -= ShortcutsManager_OpenShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.SaveShortcutEvent -= ShortcutsManager_SaveShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NewShortcutEvent -= ShortcutsManager_NewShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ExportShortcutEvent -= EditShortcutsManager_ExportShortcutEvent;
            ExportPanel.ExportButtonPressedEvent -= ExportPanel_ExportButtonPressedEvent;
        }
#if DEBUG
        ~EditingToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("************ EditingToolbarView Destructor *************");
        }
#endif
        #endregion

        #region Public Methods

        public void SwitchToEffectsItem()
        {
            Pivot.SelectedItem = SceneEffectsPivotItem;
        }

        public void ProcessWithNewMediClip(TcMediaClip clip)
        {
            SceneEffectsPanel.Update(clip);
        }

        #endregion

        #region Callbacks

        private void Pivot_PivoItemLoaded(Pivot sender, PivotItemEventArgs args)
        {
            Shortcut.Manager.Instance.Activate(Shortcut.ShortcutsManagerId.Edit);
        }

        private void SceneEffectsPanel_RetakeShotEvent()
        {
            RetakeShotEvent?.Invoke();
        }

        private void SceneEffectsPanel_RecordMoreAtEndEvent()
        {
            RecordMoreAtEndEvent?.Invoke();
        }

        private Task<bool> ShortcutsManager_OpenShortcutEvent()
        {
            OpenProjectEvent?.Invoke(this, EventArgs.Empty);
            return Task.FromResult(true);
        }

        private Task<bool> ShortcutsManager_SaveShortcutEvent()
        {
            SaveProjectEvent?.Invoke(this, EventArgs.Empty);
            return Task.FromResult(true);
        }

        private Task<bool> ShortcutsManager_NewShortcutEvent()
        {
            SwitchToFileMenuEvent?.Invoke(FileMenuKey.NewMenu);
            return Task.FromResult(true);
        }
        private async Task<bool> EditShortcutsManager_ExportShortcutEvent()
        {
            SwitchToFileMenuEvent?.Invoke(FileMenuKey.FinalizeMenu);
            return true;
        }
        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Pivot.SelectedIndex == 0)
            {
                SwitchToFileMenuEvent?.Invoke(FileMenuKey.InfoMenu);
            }
        }
        private void ExportPanel_ExportButtonPressedEvent()
        {
            SwitchToFileMenuEvent?.Invoke(FileMenuKey.FinalizeMenu);
        }

        #endregion
    }
}
