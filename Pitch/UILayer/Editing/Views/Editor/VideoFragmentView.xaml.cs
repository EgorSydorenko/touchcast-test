﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Pitch.Helpers.Extensions;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Media.Editing;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.DataLayer;
using Pitch.Models;
using Pitch.Helpers.Logger;
using Pitch.Helpers.Analytics;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views
{
    public delegate void VideoFragmentViewChanged(VideoFragment videoFragment);

    public sealed partial class VideoFragmentView : ICompositionItemView
    {
        enum ResizingZone
        {
            None,
            LeftBorder,
            RightBorder
        }

        #region Static Properties and Constants

        public static readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 1);
        public static readonly CoreCursor SizeWestEastCursor = new CoreCursor(CoreCursorType.SizeWestEast, 1);
        public static readonly Size FrameSize = new Size(128, 72);
        public static readonly int MarkersPanelHeight = 10;

        private static readonly Thickness LayoutRootBorderThickness = new Thickness(1, 0, 1, 0);
        private static readonly TimeSpan MinClipTrimmedDuration = TimeSpan.FromSeconds(0.5);

        private static readonly double MinBorderDistanceForToolTipsShow = 65.0;

        public static readonly DependencyProperty IsSelectedProperty = 
            DependencyProperty.Register(
                "IsSelected", typeof(bool), 
                typeof(VideoFragmentView),
                new PropertyMetadata(false));

        #endregion

        #region Private Fields and Properties

        private double _maxWidth;
        private AsyncLock _asynclock = new AsyncLock(1);
        private readonly TcMediaClip _tcMediaClip;
        private VideoFragment _videoFragment => _tcMediaClip.ActiveTake.VideoFragments.FirstOrDefault();
        private readonly MediaClip _mediaClip;

        private ResizingZone _resizingZone;
        private Point _lastPoint;
        private Point _startResizePoint;
        private double _maxStartTrimLength;
        private double _trimWidth;

        private CancellationToken _cancellationToken;
        private MediaComposition _composition;
        private double _pixelsPerSecond;
        private Rect _visibleRegion;

        #endregion

        #region Events

        public event VideoFragmentViewChanged VideoFragmentViewWillChangeEvent;
        public event VideoFragmentViewChanged VideoFragmentViewChangedEvent;

        #endregion

        #region Public Properties

        public override TimeSpan StartTimeInComposition => _videoFragment.MediaClip.StartTimeInComposition;
        public override TimeSpan EndTimeInComposition => _videoFragment.MediaClip.EndTimeInComposition;
        public override TimeSpan TrimmedDuration => _videoFragment.MediaClip.TrimmedDuration;

        public bool IsEditEnabled { set; get; }
        public VideoFragment ClipViewModel => _videoFragment;
        public override TcMediaClip TcMediaClip => _tcMediaClip;

        public bool IsSelected
        {
            get => (bool)GetValue(IsSelectedProperty);
            set
            {
                SetValue(IsSelectedProperty, value);
                if (value == false)
                {
                    LeftBorderToolTip.Opacity = 0;
                    RightBorderToolTip.Opacity = 0;
                }
            }
        }
        #endregion

        #region Life Cycle

        public VideoFragmentView(TcMediaClip tcMediaClip, double pixelsPerSecond, CancellationToken cancellationToken)
        {
            InitializeComponent();
            _tcMediaClip = tcMediaClip;
            _cancellationToken = cancellationToken;
            ItemType = CompositionItemTypes.Clip;
            _pixelsPerSecond = pixelsPerSecond;
            _mediaClip = _videoFragment.MediaClip.Clone();
            _mediaClip.TrimTimeFromStart = _mediaClip.TrimTimeFromEnd = TimeSpan.Zero;
            _composition = new MediaComposition();
            _composition.Clips.Add(_mediaClip);
            IsEditEnabled = true;
            CalculateNewRanges(_pixelsPerSecond);
            Loaded += TouchcastCompositionClipView_Loaded;
            Unloaded += TouchcastCompositionClipView_Unloaded;
        }

#if DEBUG
        ~VideoFragmentView()
        {
            System.Diagnostics.Debug.WriteLine("******************** VideoFragmentView Destructor********************");
        }
#endif
        #endregion

        #region Callbacks

        #region TouchcastCompositionClipView

        private void TouchcastCompositionClipView_Loaded(object sender, RoutedEventArgs e)
        {
            LeftBorder.PointerPressed += LeftBorder_PointerPressed;
            LeftBorder.PointerMoved += Border_PointerMoved;
            LeftBorder.PointerReleased += Border_PointerReleased;
            LeftBorder.PointerExited += Border_PointerExited;

            RightBorder.PointerPressed += RightBorder_PointerPressed;
            RightBorder.PointerMoved += Border_PointerMoved;
            RightBorder.PointerReleased += Border_PointerReleased;
            RightBorder.PointerExited += Border_PointerExited;

            PointerEntered += ClipView_PointerEntered;
            PointerExited += ClipView_PointerExited;

            UpdateBorderToolTips();
        }

        private void TouchcastCompositionClipView_Unloaded(object sender, RoutedEventArgs e)
        {
            LeftBorder.PointerPressed -= LeftBorder_PointerPressed;
            LeftBorder.PointerMoved -= Border_PointerMoved;
            LeftBorder.PointerReleased -= Border_PointerReleased;
            LeftBorder.PointerExited -= Border_PointerExited;

            RightBorder.PointerPressed -= RightBorder_PointerPressed;
            RightBorder.PointerMoved -= Border_PointerMoved;
            RightBorder.PointerReleased -= Border_PointerReleased;
            RightBorder.PointerExited -= Border_PointerExited;

            PointerEntered -= ClipView_PointerEntered;
            PointerExited -= ClipView_PointerExited;
        }

        #endregion

        #region Border

        private void LeftBorder_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (IsSelected)
            {
                CaptureResizeBorder(e, ResizingZone.LeftBorder);
                LeftBorderToolTip.Visibility = Visibility.Visible;
                UpdateBorderToolTips();
            }
        }

        private void RightBorder_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (IsSelected)
            {
                CaptureResizeBorder(e, ResizingZone.RightBorder);
                RightBorderToolTip.Visibility = Visibility.Visible;
                UpdateBorderToolTips();
            }
        }

        private void Border_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (IsSelected && IsEditEnabled)
            {
                Window.Current.CoreWindow.PointerCursor = SizeWestEastCursor;

                if (_resizingZone != ResizingZone.None)
                {
                    var currentPoint = e.GetCurrentPoint(this);
                    var newTransformX = CalculateTransform(currentPoint.Position);
                    CalculateTrimWidth(newTransformX);
                    UpdateTrimZone(newTransformX);
                    UpdateBorderToolTips();
                }
            }
        }

        private async void Border_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            VideoFragmentViewWillChangeEvent?.Invoke(_videoFragment);

            RightBorder.ReleasePointerCapture(e.Pointer);
            LeftBorder.ReleasePointerCapture(e.Pointer);
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;

            var visibleRegion = new Rect(_visibleRegion.X, _visibleRegion.Y, _visibleRegion.Width, _visibleRegion.Height);

            var time = WidthToDuration(_trimWidth);
            if (_resizingZone == ResizingZone.LeftBorder)
            {
                visibleRegion.X = 0;
                visibleRegion.Width -= _trimWidth;
                _videoFragment.TrimFromStart(time);
                GoogleAnalyticsManager.TrySend(AnalyticCommandType.AnalyticsCommandClipTrimmed);
            }
            else
            {
                visibleRegion.Width += _trimWidth;
                _videoFragment.TrimFromEnd(-time);
                GoogleAnalyticsManager.TrySend(AnalyticCommandType.AnalyticsCommandClipTrimmed);
            }

            _resizingZone = ResizingZone.None;

            Width -= LeftShadow.Width + RightShadow.Width;
            _maxStartTrimLength -= LeftShadow.Width;
            FramesTransform.X = _maxStartTrimLength;

            if (visibleRegion.Width < 0) visibleRegion.Width = 0;
            await CreateThumbnails(visibleRegion, _cancellationToken);

            ResetTrimVariables();
            UpdateBorderToolTips();
            VideoFragmentViewChangedEvent?.Invoke(_videoFragment);
        }

        private void Border_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private async void ClipView_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (_resizingZone == ResizingZone.None && RightBorderToolTip.Opacity != 1)
            {
                await ShowTooltipAnimation.BeginAsync();
            }
        }

        private async void ClipView_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (_resizingZone == ResizingZone.None && !IsSelected)
            {
                await HideTooltipAnimation.BeginAsync();
            }
        }

        #endregion

        #endregion

        #region Private Methods

        private void UpdateBorderToolTips()
        {
            var startTime = _videoFragment.TrimTimeRange.Start - _videoFragment.TimeRange.Start;
            var endTime = startTime + _videoFragment.TrimTimeRange.Duration;
            if (_resizingZone != ResizingZone.None)
            {
                var time = WidthToDuration(_trimWidth);
                if (_resizingZone == ResizingZone.LeftBorder)
                {
                    startTime += time;
                }
                else
                {
                    endTime += time;
                }
            }

            var rightToLeftDistance = RightBorderTransform.X - LeftBorderTransform.X;
            LeftBorderToolTip.Text = rightToLeftDistance >= MinBorderDistanceForToolTipsShow ? TimeToText(startTime) : String.Empty;
            RightBorderToolTip.Text = rightToLeftDistance >= MinBorderDistanceForToolTipsShow ? TimeToText(endTime) : String.Empty;
        }

        private string TimeToText(TimeSpan time)
        {
            if (time == TimeSpan.Zero) return "00:00";

            var toolTipFormat = (time.Hours == 0 && time.Minutes == 0) ? @"mm\:ss" : @"hh\:mm\:ss";
            return time.ToString(toolTipFormat);
        }

        private void CaptureResizeBorder(Windows.UI.Xaml.Input.PointerRoutedEventArgs e, ResizingZone zone)
        {
            var point = e.GetCurrentPoint(this);
            _lastPoint = point.Position;
            _startResizePoint = point.Position;
            _resizingZone = zone;

            var border = (_resizingZone == ResizingZone.LeftBorder) ? LeftBorder : RightBorder;
            border.CapturePointer(e.Pointer);
        }

        private double CalculateTransform(Point pointerPosition)
        {
            var translateTransformX = (_resizingZone == ResizingZone.LeftBorder) ? LeftBorderTransform.X : RightBorderTransform.X;
            var trimDelta = (MinClipTrimmedDuration < _mediaClip.OriginalDuration) ? (MinClipTrimmedDuration + ClipViewModel.HeadTransitionTimeRange.Duration + ClipViewModel.TailTransitionTimeRange.Duration).TotalSeconds * _pixelsPerSecond : _maxWidth;

            var minValue = (_resizingZone == ResizingZone.LeftBorder) ? _maxStartTrimLength : LeftBorderTransform.X + trimDelta;
            var maxValue = (_resizingZone == ResizingZone.LeftBorder) ? RightBorderTransform.X - trimDelta : _maxWidth + _maxStartTrimLength;

            var x = translateTransformX + pointerPosition.X - _lastPoint.X;
            _lastPoint = pointerPosition;
            if (x < minValue) x = minValue;
            if (x > maxValue) x = maxValue;
            if (x < 0 && _resizingZone == ResizingZone.LeftBorder) _maxStartTrimLength -= x;

            return x;
        }

        private void CalculateTrimWidth(double position)
        {
            _trimWidth += position;
            if (position >= Width)
            {
                _trimWidth -= Width;
            }
            else if (position >= 0)
            {
                _trimWidth -= ((_resizingZone == ResizingZone.LeftBorder) ? LeftBorderTransform.X : RightBorderTransform.X);
            }
        }

        private void UpdateTrimZone(double position)
        {
            if (position >= Width)
            {
                Width = position;
                RightBorderTransform.X = Width;
                RightShadowTransform.X = Width;
            }
            else
            {
                if (position < 0)
                {
                    Width -= position;
                    RightBorderTransform.X = Width;
                    RightShadowTransform.X = Width;
                    FramesTransform.X -= position;
                }
                else
                {
                    if (_resizingZone == ResizingZone.LeftBorder)
                    {
                        LeftBorderTransform.X = position;
                        LeftShadow.Width = position;
                    }
                    else
                    {
                        RightBorderTransform.X = position;
                        RightShadowTransform.X = position;
                        RightShadow.Width = Width - position;
                    }
                }
            }
        }

        private List<TimeSpan> CreateTimes(double x, double width)
        {
            List<TimeSpan> times = new List<TimeSpan>();

            int startRenderingFrame = (int)(x / FrameSize.Width);
            int endRenderingFrame = (int)Math.Ceiling((x + width) / FrameSize.Width); ;

            if ((_videoFragment.TrimTimeRange.Start - _videoFragment.TimeRange.Start + _videoFragment.HeadTransitionTimeRange.Duration).TotalSeconds > 0)
            {
                var trimmedFromStartWidth = (_videoFragment.TrimTimeRange.Start - _videoFragment.TimeRange.Start + _videoFragment.HeadTransitionTimeRange.Duration).TotalSeconds * _pixelsPerSecond;
                int delta = (int)(trimmedFromStartWidth / FrameSize.Width);
                startRenderingFrame += delta;
                endRenderingFrame += delta;
                if (((x + width) % FrameSize.Width) > (FrameSize.Width - (trimmedFromStartWidth % FrameSize.Width)))
                {
                    ++endRenderingFrame;
                }
            }

            double msTimeStep = FrameSize.Width / _pixelsPerSecond;
            int step;
            for (step = startRenderingFrame; step < endRenderingFrame; ++step)
            {
                times.Add(TimeSpan.FromSeconds(msTimeStep * step));
            }

            return times;
        }

        private async Task CreateThumbnails(Rect visibleRegion, CancellationToken token)
        {
            _visibleRegion = visibleRegion;
            if (token.IsCancellationRequested) return;

            var times = CreateTimes(visibleRegion.X, visibleRegion.Width);

            if (token.IsCancellationRequested) return;

            int index = (int)(visibleRegion.X / FrameSize.Width);
            var trimmedFrames = (_videoFragment.TrimTimeRange.Start - _videoFragment.TimeRange.Start).TotalSeconds * _pixelsPerSecond/ FrameSize.Width;
            while (index + trimmedFrames > Frames.Children.Count)
            {
                Frames.Children.Add(new ImageView() { Width = FrameSize.Width });
            }
            index += (int)trimmedFrames;

            foreach (var time in times)
            {
                if (token.IsCancellationRequested) return;

                var imageView = (index < Frames.Children.Count) ?
                Frames.Children[index] as ImageView :
                new ImageView() { Width = FrameSize.Width };

                if (!imageView.UpdateImageForZoom(_pixelsPerSecond))
                {
                    var bitmapImage = await CreateThumbnailImageAsync(time + _videoFragment.TimeRange.Start, token);
                    if (bitmapImage != null)
                    {
                        imageView.ImageSourceForZoom(bitmapImage, _pixelsPerSecond);
                    }

                    if (index >= Frames.Children.Count)
                    {
                        Frames.Children.Add(imageView);
                    }
                }
                ++index;
            }
        }

        private async Task<BitmapImage> CreateThumbnailImageAsync(TimeSpan time, CancellationToken token)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (token.IsCancellationRequested) return null;

                ImageStream stream =
                    await _composition.GetThumbnailAsync(time, (int)FrameSize.Width, (int)FrameSize.Height, VideoFramePrecision.NearestKeyFrame);

                if (token.IsCancellationRequested) return null;

                if (stream != null)
                {
                    bitmapImage = new BitmapImage()
                    {
                        DecodePixelHeight = (int)FrameSize.Height
                    };
                    bitmapImage.SetSource(stream);
                }
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.CreateThumbnailImageAsync({time}, token) : {ex.Message}");
            }
            return bitmapImage;
        }

        private TimeSpan WidthToDuration(double width)
        {
            return TimeSpan.FromSeconds(width / _pixelsPerSecond);
        }

        private void ResetTrimVariables()
        {
            LeftBorderTransform.X = 0;
            RightBorderTransform.X = Width;
            RightShadowTransform.X = Width;
            LeftShadow.Width = 0;
            RightShadow.Width = 0;
            _trimWidth = 0;
        }

        private double CalculateMarkerPositionForTime(double time)
        {
            return _pixelsPerSecond * (time - ClipViewModel.MediaClip.StartTimeInComposition.TotalSeconds);
        }

        #endregion

        #region Public Methods

        public Task Clean()
        {
            return Task.CompletedTask;
        }

        public void ClipViewForCurrentTrimTime()
        {
            var trimmedFromStartWidth = (_videoFragment.TrimTimeRange.Start - _videoFragment.TimeRange.Start + _videoFragment.HeadTransitionTimeRange.Duration).TotalSeconds * _pixelsPerSecond;
            var trimmedFromEndWidth = (_videoFragment.TrimTimeFromEnd + _videoFragment.TailTransitionTimeRange.Duration).TotalSeconds * _pixelsPerSecond;
            Width = _maxWidth - trimmedFromStartWidth - trimmedFromEndWidth;

            //Width = _maxWidth - (_videoFragment.TimeRange.Duration - _videoFragment.TrimTimeRange.Duration).TotalSeconds * _pixelsPerSecond;
            Frames.Width = _maxWidth;
            RightBorderTransform.X = Width;
            RightShadowTransform.X = Width;

            FramesTransform.X = -trimmedFromStartWidth;

            UpdateBorderToolTips();
        }

        public async Task UpdateThumbnailsByNewZoom(double pixelsPerSecond, Rect visibleRegion, CancellationToken token)
        {
            _pixelsPerSecond = pixelsPerSecond;
            await _asynclock.WaitAsync();
            try
            {
                await CreateThumbnails(visibleRegion, token);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.UpdateThumbnailsByNewZoom({pixelsPerSecond}, {visibleRegion}, token) : {ex.Message}");
            }
            finally
            {
                _asynclock.Release();
                _cancellationToken = token;
            }
        }

        public void UpdateActionStreamPanel(List<Command> comanndsToDraw)
        {
            MarkersPanel.Children.Clear();
            foreach (var item in comanndsToDraw)
            {
                    TouchcastVappMarkView view = new TouchcastVappMarkView(item);
                    var xPosition = CalculateMarkerPositionForTime(item.Time);
                    view.RenderTransform = new TranslateTransform() { X = xPosition, Y = 1 };
                    MarkersPanel.Children.Add(view);
            }
        }

        public void ResizeActionStreamPanel()
        {
            var markerViews = MarkersPanel.Children.Cast<TouchcastVappMarkView>().ToList();
            foreach (var markerView in markerViews)
            {
                var xPosition = CalculateMarkerPositionForTime(markerView.Time);
                if (markerView.RenderTransform is TranslateTransform)
                {
                    (markerView.RenderTransform as TranslateTransform).X = xPosition;
                }
            }
        }

        public void CalculateNewRanges(double pixelsPerSecond)
        {
            try
            {
                _pixelsPerSecond = pixelsPerSecond;
                //Height = FrameSize.Height + MarkersPanelHeight + FramesContainer.BorderThickness.Top + FramesContainer.BorderThickness.Bottom + 14;

                _maxWidth = _videoFragment.Duration.TotalSeconds * _pixelsPerSecond;

                _maxStartTrimLength = -(_videoFragment.TrimTimeRange.Start - _videoFragment.TimeRange.Start).TotalSeconds * _pixelsPerSecond;
                ClipViewForCurrentTrimTime();
                ResetTrimVariables();
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.CalculateNewRanges({pixelsPerSecond}) : {ex.Message}");
            }
        }

        public async Task UpdateVisibleRegion(Rect visibleRegion, CancellationToken token)
        {
            await _asynclock.WaitAsync();
            try
            {
                await CreateThumbnails(visibleRegion, token);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in VideoFragmentView.UpdateVisibleRegion({visibleRegion}) : {ex.Message}");
            }
            finally
            {
                _asynclock.Release();
                _cancellationToken = token;
            }
        }

        #endregion
    }
}
