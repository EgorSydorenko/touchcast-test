﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using GalaSoft.MvvmLight.Ioc;
using Pitch.DataLayer;
using Pitch.DataLayer.Shots;
using Pitch.Helpers;
using Pitch.UILayer.Helpers.Messages;
using Pitch.Helpers.Logger;
using Pitch.DataLayer.Helpers.Extensions;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views
{
    public class TcCompositionSeekEventArgs
    {
        public TimeSpan Position { get; }

        public TcCompositionSeekEventArgs(TimeSpan position)
        {
            Position = position;
        }
    }

    public class ClipViewVisibleRegion
    {
        public VideoFragmentView View { get; set; }
        public Rect VisibleArea { get; set; }
    }

    public delegate void TcCompositionSeek(TcCompositionSeekEventArgs args);
    public delegate void TcCompositionRefresh(bool isNeedResetStream);
    public delegate void ActiveTcMediaClipChanged(object sender, TcMediaClip clip);
    public sealed partial class MediaCompositionView
    {
        #region Static and constant values

        public static readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 1);
        public static readonly CoreCursor SizeWestEastCursor = new CoreCursor(CoreCursorType.SizeWestEast, 1);

        private const double ScrollBarHeight = 12;
        private readonly Point ZeroPoint = new Point(0, 0);
        #endregion

        #region Private fields
        private bool _isFirstLoad = true;
        private WeakReference<AuthoringSession> _authoringSession;
        private Point _lastPoint;
        private TimeSpan _position = TimeSpan.Zero;
        private bool _isPressed;
        private CancellationToken _cancellationToken;
        private double _pixelsPerSecond = 0.0;
        private ICompositionItemView _selectedView;
        private VideoFragmentView _selectedVideoFragmentView;
        private ScrollViewer _scrollViewer;
        private GeneralTransform _currentViewGeneralTransform = null;
        private bool _isEnabledLastMessageWasSend;
        private List<VideoFragmentView> _videoFragmentViews;
        private List<ICompositionItemView> _views;
        private Transition.Type _movedTransitionKind;
        private int _startDragItemIndex;
        private bool _isDragItemInProcess;
        private Transition.Type _splitTransitionType;
        private TcMediaClip _clipAfterDeleted;
        private ICompositionItemView SelectedView
        {
            set
            {
                _selectedView = value;
                ActiveTcMediaClipChanged?.Invoke(this, _selectedView?.TcMediaClip);
            }
            get => _selectedView;
        }
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        #endregion

        public event TcCompositionSeek TcCompositionSeekEvent;
        public event TcCompositionRefresh TcCompositionRefreshEvent;
        public event ActiveTcMediaClipChanged ActiveTcMediaClipChanged;
        public TcMediaClip MediaClip => SelectedView?.TcMediaClip;

        #region Life Cycle

        public MediaCompositionView(CancellationToken cancellationToken, double pixelsPerSecond)
        {
            InitializeComponent();
            _pixelsPerSecond = pixelsPerSecond;
            Loaded += TcCompositionItemsView_Loaded;
            Unloaded += TcCompositionItemsView_Unloaded;
            _cancellationToken = cancellationToken;
            Thumbnails.ApplyTemplate();
            _authoringSession = new WeakReference<AuthoringSession>(SimpleIoc.Default.GetInstance<AuthoringSession>());
        }

#if DEBUG
        ~MediaCompositionView()
        {
            System.Diagnostics.Debug.WriteLine("********************TouchcastMediaCompositionView Destructor********************");
        }
#endif
        private async void TcCompositionItemsView_Loaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("TcCompositionItemsView_Loaded");
            _videoFragmentViews = new List<VideoFragmentView>();

            RefreshComposition();

            _scrollViewer = UIHelper.FindChild<ScrollViewer>(Thumbnails, null);
            _scrollViewer.ViewChanged += Scroll_ViewChanged;

            if (ActualHeight > ScrollBarHeight)
            {
                Playhead.Height = ActualHeight - ScrollBarHeight;
            }
            PlayheadTransform.X = Thumbnails.Padding.Left;

            _currentViewGeneralTransform = null;

            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.TcMediaComposition.ActiveMediaClipWillBeChanged += TcMediaComposition_ActiveMediaClipWillBeChanged;
                session.Project.TcMediaComposition.ActiveMediaClipWasChanged += TcMediaComposition_ActiveMediaClipWasChanged;
            }
            await UpdateVideoFragmentViews();
            
            if (session != null)
            {
                session.Project.TcMediaComposition.MediaClipWasCreated += TcMediaComposition_MediaClipWasCreated;
                session.Project.TcMediaComposition.MediaClipWillBeRemoved += TcMediaComposition_MediaClipWillBeRemoved;
                session.Project.TcMediaComposition.MediaClipWasRemoved += TcMediaComposition_MediaClipWasRemoved;
                session.Project.TcMediaComposition.MediaClipWasChanged += TcMediaComposition_MediaClipWasChanged;
                session.Project.TcMediaComposition.MediaClipWillBeMoved += TcMediaComposition_MediaClipWillBeMoved;
                session.Project.TcMediaComposition.MediaClipWasMoved += TcMediaComposition_MediaClipWasMoved;
                session.Project.TcMediaComposition.MediaClipWillBeSplitted += TcMediaComposition_MediaClipWillBeSplitted;
                session.Project.TcMediaComposition.MediaClipWasSplitted += TcMediaComposition_MediaClipWasSplitted;
            }
            Thumbnails.SizeChanged += Thumbnails_SizeChanged;
        }
        private void TcCompositionItemsView_Unloaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("TcCompositionItemsView_Unloaded");
            var session = AuthoringSession;
            if (session != null)
            {
                foreach (var shot in session.Project.Shots)
                {
                    UnsubscribeShotFromTakeEvents(shot);
                }

                session.Project.TcMediaComposition.MediaClipWasCreated -= TcMediaComposition_MediaClipWasCreated;
                session.Project.TcMediaComposition.MediaClipWillBeRemoved -= TcMediaComposition_MediaClipWillBeRemoved;
                session.Project.TcMediaComposition.MediaClipWasRemoved -= TcMediaComposition_MediaClipWasRemoved;
                session.Project.TcMediaComposition.MediaClipWasChanged -= TcMediaComposition_MediaClipWasChanged;
                session.Project.TcMediaComposition.MediaClipWillBeMoved -= TcMediaComposition_MediaClipWillBeMoved;
                session.Project.TcMediaComposition.MediaClipWasMoved -= TcMediaComposition_MediaClipWasMoved;
                session.Project.TcMediaComposition.MediaClipWillBeSplitted -= TcMediaComposition_MediaClipWillBeSplitted;
                session.Project.TcMediaComposition.MediaClipWasSplitted -= TcMediaComposition_MediaClipWasSplitted;
                session.Project.TcMediaComposition.ActiveMediaClipWillBeChanged -= TcMediaComposition_ActiveMediaClipWillBeChanged;
                session.Project.TcMediaComposition.ActiveMediaClipWasChanged -= TcMediaComposition_ActiveMediaClipWasChanged;
            }
            Thumbnails.SizeChanged -= Thumbnails_SizeChanged;
            _scrollViewer.ViewChanged -= Scroll_ViewChanged;
            Thumbnails.Items.Clear();
        }

        #endregion

        #region Callbacks

        private async void Shot_TakeWasRemoved(Take take)
        {
            var views = _videoFragmentViews.Where(v => v.ClipViewModel.Take.Id == take.Id).ToList();
            foreach(var view in views)
            {
                view.VideoFragmentViewChangedEvent -= VideoFragmentView_VideoFragmentViewChangedEvent;
                RemoveVideoFragmentView(view);
            }

            RefreshComposition();
            UpdateClipViewsList();
            Thumbnails.UpdateLayout();

            await UpdateClipViewsVisibleRegions();
            _position = TimeSpan.Zero;
            SeekAndUpdatePlayheadPosition(_position);
        }

        #region Playhead

        private void Playhead_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = SizeWestEastCursor;

            if (_isPressed)
            {
                var currentPoint = e.GetCurrentPoint(this);
                var transformX = PlayheadTransform.X + currentPoint.Position.X - _lastPoint.X;
                _lastPoint = currentPoint.Position;

                if (_selectedVideoFragmentView == null)
                {
                    UpdateCurrentView();
                }

                InvokeSeekEvent(transformX);
            }
        }

        private void Playhead_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private void Playhead_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var point = e.GetCurrentPoint(this);
            _lastPoint = point.Position;
            Playhead.CapturePointer(e.Pointer);
            _isPressed = true;
        }

        private void Playhead_PointerCaptureLost(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            _isPressed = false;
        }

        private void Playhead_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            Playhead.ReleasePointerCapture(e.Pointer);
            _isPressed = false;
        }

        #endregion

        #region ScrollView

        private async void Scroll_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (_selectedVideoFragmentView != null)
            {
                UpdateSelectedView(_selectedVideoFragmentView);
                UpdatePlayheadTransform(_position);
            }
            if (!e.IsIntermediate)
            {
                await UpdateClipViewsVisibleRegions();
            }
        }

        private List<ClipViewVisibleRegion> ClipViewsVisibleRegions()
        {
            var result = new List<ClipViewVisibleRegion>();

            if (_scrollViewer != null)
            {
                var viewportStartX = _scrollViewer.HorizontalOffset - VideoFragmentView.FrameSize.Width;
                if (viewportStartX < 0) viewportStartX = 0;
                var viewportEndX = _scrollViewer.HorizontalOffset + _scrollViewer.ViewportWidth;

                var clipStartX = Thumbnails.Padding.Left - _scrollViewer.HorizontalOffset;
                if (clipStartX < 0) clipStartX = 0;

                foreach (FrameworkElement view in Thumbnails.Items)
                {
                    var viewWidth = 16.0;
                    if (view is VideoFragmentView)
                    {
                        viewWidth = view.Width;

                        var clipEndX = clipStartX + viewWidth;
                        if (viewportStartX < clipEndX && clipStartX < viewportEndX)
                        {
                            var regionX = (clipStartX < viewportStartX) ? viewportStartX - clipStartX : 0;

                            var regionWidth = (clipStartX + viewWidth > viewportEndX) ?
                                viewportEndX - clipStartX - regionX : viewWidth - regionX;

                            result.Add(new ClipViewVisibleRegion()
                            {
                                View = view as VideoFragmentView,
                                VisibleArea = new Rect(regionX, 0, regionWidth, view.Height)
                            });
                        }
                    }

                    clipStartX += viewWidth;
                    if (clipStartX > viewportEndX)
                    {
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        private async void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Playhead.Height = ActualHeight - ScrollBarHeight;

            await UpdateClipViewsVisibleRegions();
        }

        #region TouchcastCompositionClipView

        private void ClipView_TouchcastCompositionClipViewWillChangeEvent(Take take)
        {
        }

        #endregion

        #region ListView

        private void Thumbnails_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            var point = e.GetPosition(this);
            _lastPoint = point;

            point = e.GetPosition(null);
            var selectedView = VisualTreeHelper.FindElementsInHostCoordinates(point, Thumbnails).FirstOrDefault(c => c is VideoFragmentView);
            if (selectedView != null)
            {
                UpdateSelectedView(selectedView as VideoFragmentView);
                InvokeSeekEvent(_lastPoint.X);
            }
        }

        private void Thumbnails_DragItemsStarting(object sender, DragItemsStartingEventArgs e)
        {
            if (_isDragItemInProcess)
            {
                e.Cancel = true;
                return;
            }

            var clipsCount = AuthoringSession?.Project?.TcMediaComposition.MediaClips.Count ?? 0;

            var item = e.Items.First();
            if (item is VideoFragmentView && clipsCount > 1)
            {
                _isDragItemInProcess = true;
                RemoveAllTransitionViews();
                _startDragItemIndex = Thumbnails.Items.IndexOf(item);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private async void Thumbnails_DragItemsCompleted(ListViewBase sender, DragItemsCompletedEventArgs args)
        {
            if (args.DropResult == Windows.ApplicationModel.DataTransfer.DataPackageOperation.Move)
            {
                var movedView = (VideoFragmentView)args.Items.First();
                if (movedView != null)
                {
                    var newClipIndex = Thumbnails.Items.OfType<VideoFragmentView>().ToList().IndexOf(movedView);
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        if (session.Project.TcMediaComposition.MediaClips.Count > newClipIndex)
                        {
                            await session.Project.TcMediaComposition.MoveClip(movedView.TcMediaClip, newClipIndex);
                        }
                        else
                        {
                            Thumbnails.Items.Remove(movedView);
                            Thumbnails.Items.Insert(_startDragItemIndex, movedView);
                            UpdateClipViewsList();
                        }
                    }
                }
            }
            else 
            {
                UpdateClipViewsList();
            }
            _isDragItemInProcess = false;
        }

        private void Thumbnails_SizeChanged(object sender, SizeChangedEventArgs e)
        {
             UpdatePlayheadTransform(_position);
        }

        #endregion
        
        #endregion

        #region Public Methods

        public void IsEditClipsEnabled(bool isEnable)
        {
            Thumbnails.CanReorderItems = isEnable;
            Thumbnails.CanDragItems = isEnable;

            foreach (Control item in Thumbnails.Items)
            {
                if (item is VideoFragmentView)
                {
                    (item as VideoFragmentView).IsEditEnabled = isEnable;
                }
                else
                {
                    item.IsEnabled = isEnable;
                }
            }
        }

        public async Task Cleanup()
        {
            if (_videoFragmentViews != null)
            {
                foreach (var item in _videoFragmentViews)
                {
                    item.VideoFragmentViewChangedEvent -= VideoFragmentView_VideoFragmentViewChangedEvent;
                    await item.Clean();
                }
                _videoFragmentViews.Clear();
            }
            
            _selectedVideoFragmentView = null;
            SelectedView = null;
            Thumbnails.Items.Clear();
        }

        public void UpdatePlayheadPositionIfNeeded(TimeSpan position)
        {
            if (_currentViewGeneralTransform == null)
            {
                UpdateMatrixTransform();
            }

            if (_currentViewGeneralTransform != null)
            {
                UpdatePlayheadPosition(position);
            }
        }

        public void UpdatePlayheadPosition(TimeSpan position)
        {
            if (_videoFragmentViews.Count == 0)
            {
                Playhead.Visibility = Visibility.Collapsed;
                return;
            }
            Playhead.Visibility = Visibility.Visible;

            _position = position;

            if (_selectedVideoFragmentView == null || _selectedVideoFragmentView.StartTimeInComposition > position || _selectedVideoFragmentView.EndTimeInComposition < position)
            {
                SelectedView = _views.Where(v => v.StartTimeInComposition <= position && v.EndTimeInComposition >= position).FirstOrDefault();

                if (SelectedView is VideoFragmentView)
                {
                    UpdateSelectedView(SelectedView as VideoFragmentView);
                }
                else
                {
                    if (_selectedVideoFragmentView != null)
                    {
                        SelectMediaClip(null);
                        _selectedVideoFragmentView = null;
                    }
                }
            }

            if (_selectedVideoFragmentView != null)
            {
                UpdatePlayheadTransform(_position);
            }
            else
            {
                if (SelectedView != null)
                {
                    var transform = SelectedView.TransformToVisual(Thumbnails);
                    var pt = transform.TransformPoint(ZeroPoint);
                    PlayheadTransform.X = pt.X + (SelectedView.ActualWidth / SelectedView.TrimmedDuration.TotalSeconds)
                        * (position - SelectedView.StartTimeInComposition).TotalSeconds;

                    Playhead.Visibility = (PlayheadTransform.X < 0) ? Visibility.Collapsed : Visibility.Visible;
                }
            }
            SendEnabledSplitMessage();
        }

        public async Task UpdateThumbnailsByNewZoom(Double pixelsPerSecond, CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            if (cancellationToken.IsCancellationRequested) return;

            foreach (var item in _videoFragmentViews)
            {
                if (cancellationToken.IsCancellationRequested) return;

                item.CalculateNewRanges(pixelsPerSecond);
                item.ResizeActionStreamPanel();
            }

            if (cancellationToken.IsCancellationRequested) return;

            Thumbnails.UpdateLayout();
            var regions = ClipViewsVisibleRegions();
            foreach (var item in regions)
            {
                if (cancellationToken.IsCancellationRequested) return;
                await item.View.UpdateThumbnailsByNewZoom(pixelsPerSecond, item.VisibleArea, cancellationToken);
            }

            if (cancellationToken.IsCancellationRequested) return;

            UpdateMatrixTransformIfNeeded(pixelsPerSecond);
            _pixelsPerSecond = pixelsPerSecond;
            UpdatePlayheadTransform(_position);
        }

        public async Task SplitCurrentVideoFragment(CancellationToken token)
        {
            _cancellationToken = token;
            var session = AuthoringSession;
            if (_selectedVideoFragmentView != null && session != null)
            {
                await session.Project.TcMediaComposition.SplitClip(_selectedVideoFragmentView.TcMediaClip, _position);
            }
        }

        public async Task DuplicateCurrentVideoFragment(CancellationToken token)
        {
            _cancellationToken = token;
            var session = AuthoringSession;
            if (_selectedVideoFragmentView != null && session != null)
            {
                await session.Project.TcMediaComposition.DuplicateClip(_selectedVideoFragmentView.TcMediaClip);
            }
        }

        public async Task DeleteCurrentVideoFragment(CancellationToken token)
        {
            _cancellationToken = token;
            var session = AuthoringSession;
            if (_selectedVideoFragmentView != null && session != null)
            {
                await session.Project.TcMediaComposition.RemoveClip(_selectedVideoFragmentView.TcMediaClip);
            }
        }
        private async Task TcMediaComposition_MediaClipWillBeRemoved(TcMediaClip sender)
        {
            _clipAfterDeleted = MediaClipAfter(sender);
        }

        private async Task TcMediaComposition_MediaClipWasRemoved(TcMediaClip sender)
        {
            RefreshComposition();
            await UpdateVideoFragmentViews();
            Thumbnails.UpdateLayout();

            if (_clipAfterDeleted == null)
            {
                _clipAfterDeleted = AuthoringSession?.Project.TcMediaComposition.MediaClips.LastOrDefault();
            }
            var fragment = _clipAfterDeleted?.ActiveTake.VideoFragments.FirstOrDefault();
            _position = (fragment == null) ? TimeSpan.Zero : (fragment.MediaClip.StartTimeInComposition + TimeSpan.FromMilliseconds(1));
            SeekAndUpdatePlayheadPosition(_position);
        }

        private async Task TcMediaComposition_MediaClipWasCreated(TcMediaClip sender)
        {
            RefreshComposition();
            await UpdateVideoFragmentViews();
            Thumbnails.UpdateLayout();
            SeekAndUpdatePlayheadPosition(_position);
        }
        private async Task TcMediaComposition_MediaClipWillBeSplitted(TcMediaClip oldClip, TcMediaClip newClip)
        {
            var nextMediaClip = MediaClipAfter(oldClip);
            _splitTransitionType = oldClip.Transition.Kind;
            if (_splitTransitionType != Transition.Type.None)
            {
                oldClip.Transition.Kind = Transition.Type.None;

                var videoFragment = oldClip.ActiveTake.VideoFragments.Last();
                videoFragment.UpdateTailTransition(TimeSpan.Zero);
                if (nextMediaClip != null)
                {
                    videoFragment = nextMediaClip.ActiveTake.VideoFragments.First();
                    videoFragment.UpdateHeadTransition(TimeSpan.Zero);
                }
            }
        }

        private async Task TcMediaComposition_MediaClipWasSplitted(TcMediaClip oldClip, TcMediaClip newClip)
        {
            RefreshComposition();

            var nextMediaClip = MediaClipAfter(newClip);
            if (_splitTransitionType != Transition.Type.None && nextMediaClip != null)
            {
                var result = newClip.Transition.TryCreateTransitionBetweenShots(newClip, nextMediaClip, _splitTransitionType);
                if (result)
                {
                    newClip.Transition.Kind = _splitTransitionType;
                }
                await UpdateVideoFragmentViews();
                await ApplyTransitionBetweenClips(newClip, nextMediaClip);
            }
            else
            {
                await UpdateVideoFragmentViews();
                Thumbnails.UpdateLayout();
                SeekAndUpdatePlayheadPosition(_position);
            }
        }

        private void TcMediaComposition_ActiveMediaClipWillBeChanged(TcMediaClip sender)
        {
            var view = _videoFragmentViews.FirstOrDefault(v => v.TcMediaClip.IsSelected);
            if (view != null)
            {
                view.IsSelected = false;
            }
        }

        private void TcMediaComposition_ActiveMediaClipWasChanged(TcMediaClip sender)
        {
            var view = _videoFragmentViews.FirstOrDefault(v => v.TcMediaClip.Id == sender?.Id);
            if (view != null)
            {
                view.IsSelected = true;
                if (_position > view.EndTimeInComposition || _position < view.StartTimeInComposition)
                {
                    _position = view.StartTimeInComposition + TimeSpan.FromMilliseconds(1);
                    SeekAndUpdatePlayheadPosition(_position);
                }
            }
        }

        private void SelectMediaClip(TcMediaClip mediaClip)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.TcMediaComposition.SelectMediaClip(mediaClip);
            }
        }

        private async Task TcMediaComposition_MediaClipWasChanged(TcMediaClip sender)
        {
            RefreshComposition();
            await UpdateVideoFragmentViews();
            Thumbnails.UpdateLayout();

            if (sender.IsSelected)
            {
                var clip = sender.ActiveTake.VideoFragments.FirstOrDefault();
                if (_position > clip.MediaClip.EndTimeInComposition)
                {
                    _position = clip.MediaClip.EndTimeInComposition;
                    SeekAndUpdatePlayheadPosition(_position);
                }
                _selectedVideoFragmentView = _videoFragmentViews.FirstOrDefault(v => v.StartTimeInComposition <= _position && v.EndTimeInComposition >= _position);
                SelectedView = _selectedVideoFragmentView;
            }
        }

        private async Task TcMediaComposition_MediaClipWillBeMoved(TcMediaClip sender, int newindex)
        {
            RemoveAllTransitionViews();
            _movedTransitionKind = sender.Transition.Kind;
            var nextMediaClip = MediaClipAfter(sender);
            var result = sender.Transition.TryCreateTransitionBetweenShots(sender, nextMediaClip, Transition.Type.None);
            if (result)
            {
                await ApplyTransitionBetweenClips(sender, nextMediaClip);
            }
        }

        private async Task TcMediaComposition_MediaClipWasMoved(TcMediaClip sender, int newindex)
        {
            var view = Thumbnails.Items.OfType<VideoFragmentView>().FirstOrDefault(i => i.TcMediaClip == sender);
            if (view != null)
            {
                Thumbnails.Items.Remove(view);
                Thumbnails.Items.Insert(newindex, view);
            }

            var nextMediaClip = MediaClipAfter(sender);
            var result = sender.Transition.TryCreateTransitionBetweenShots(sender, nextMediaClip, _movedTransitionKind);
            if (result)
            {
                await ApplyTransitionBetweenClips(sender, nextMediaClip);
            }
            UpdateClipViewsList();
        }

        public void NextClip()
        {
            if (_videoFragmentViews != null && SelectedView != null)
            {
                try
                {
                    var nextClip = _videoFragmentViews.FirstOrDefault(c => c.StartTimeInComposition > SelectedView.StartTimeInComposition);
                    if (nextClip != null)
                    {
                        ChangePositionToClipStartTime(nextClip, true);
                    }
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile(ex.Message);
                }
            }
        }

        public void PreviousClip()
        {
            if (_videoFragmentViews != null && SelectedView != null)
            {
                try
                {
                    var prevClip = _videoFragmentViews.LastOrDefault(c => c.StartTimeInComposition < SelectedView.StartTimeInComposition);
                    if (prevClip != null)
                    {
                        ChangePositionToClipStartTime(prevClip, true);
                    }
                    else if (_selectedVideoFragmentView != null)
                    {
                        ChangePositionToClipStartTime(_selectedVideoFragmentView, true);
                    }
                }
                catch (Exception ex)
                {
                    LogQueue.WriteToFile(ex.Message);
                }
            }
        }

        public void IncreasePosition(TimeSpan deltaTime)
        {
            var position = TimeInComposition(_position + deltaTime);
            SeekAndUpdatePlayheadPosition(position);
        }

        #endregion

        #region Private Methods

        private void DefaultSelection()
        {
            var isNeedToSelect = !AuthoringSession?.Project.TcMediaComposition.MediaClips.Any(v => v.IsSelected);
            if (isNeedToSelect.Value)
            {
                _selectedVideoFragmentView = _videoFragmentViews.FirstOrDefault(v => v.StartTimeInComposition <= _position && v.EndTimeInComposition >= _position);
                if (_selectedVideoFragmentView != null)
                {
                    SelectedView = _selectedVideoFragmentView;
                    SelectMediaClip(_selectedVideoFragmentView.TcMediaClip);
                }
            }
        }

        private void UpdateClipViewsList()
        {
            _videoFragmentViews = Thumbnails.Items.Cast<ICompositionItemView>().Where(i => i.ItemType == CompositionItemTypes.Clip).Cast<VideoFragmentView>().ToList();
            UpdateTransitionViews();
            _views = Thumbnails.Items.Cast<ICompositionItemView>().ToList();
        }

        private void ChangePositionToClipStartTime(VideoFragmentView view, bool isScrolled = false)
        {
            var prevPositionX = CalculateXOffset();
            UpdateSelectedView(view);

            if (isScrolled)
            {
                var currentPositionX = CalculateXOffset();
                if (currentPositionX > _scrollViewer.ViewportWidth || currentPositionX < 0)
                {
                    _scrollViewer.ChangeView(_scrollViewer.HorizontalOffset + currentPositionX - prevPositionX, null, null);
                }
            }

            _position = _selectedVideoFragmentView.StartTimeInComposition + TimeSpan.FromMilliseconds(1);
            SeekAndUpdatePlayheadPosition(_position);
        }

        private void UpdateSelectedView(VideoFragmentView newView)
        {
            if (newView != null)
            {
                _selectedVideoFragmentView = newView;
                SelectMediaClip(_selectedVideoFragmentView?.TcMediaClip);

                SelectedView = _selectedVideoFragmentView;
                UpdateMatrixTransform();
            }
        }

        private void UpdatePlayheadTransform(TimeSpan position)
        {
            if (_selectedVideoFragmentView != null)
            {
                if (!_isFirstLoad)
                {
                    PlayheadTransform.X = CalculateXOffset() + (_selectedVideoFragmentView.Width / _selectedVideoFragmentView.TrimmedDuration.TotalSeconds)
                        * (position - _selectedVideoFragmentView.StartTimeInComposition).TotalSeconds;
                }
                _isFirstLoad = false;
                Playhead.Visibility = (PlayheadTransform.X < 0) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private void UpdateMatrixTransformIfNeeded(double newPixelsPerSecodn)
        {
            if (newPixelsPerSecodn != _pixelsPerSecond)
            {
                UpdateMatrixTransform();
            }
        }

        private void UpdateMatrixTransform()
        {
            try
            {
                if (_selectedVideoFragmentView == null)
                {
                    UpdateCurrentView();
                }

                var transform = _selectedVideoFragmentView.TransformToVisual(Thumbnails);
                if (transform != null)
                {
                    _currentViewGeneralTransform = transform;
                }
            }
            catch (Exception)
            {

            }
        }

        private double CalculateXOffset()
        {
            if (_currentViewGeneralTransform == null)
            {
                UpdateMatrixTransform();
            }
            var pt = _currentViewGeneralTransform.TransformPoint(ZeroPoint);
            return pt.X;
        }

        private void InvokeSeekEvent(double pointerPositionX)
        {
            if (_selectedVideoFragmentView != null)
            {
                var timeInComposition = CalculatePositionInComposition(pointerPositionX);
                SeekAndUpdatePlayheadPosition(timeInComposition);
            }
            else
            {
                var view = _views.Where(v => v.StartTimeInComposition <= _position && v.EndTimeInComposition >= _position).FirstOrDefault();
                if (view != null)
                {
                    var transform = view.TransformToVisual(Thumbnails);
                    var pt = transform.TransformPoint(ZeroPoint);
                    var rate = (pointerPositionX - pt.X) / view.ActualWidth;

                    var timeInComposition = TimeSpan.FromSeconds(
                        view.StartTimeInComposition.TotalSeconds + view.TrimmedDuration.TotalSeconds * rate);

                    SeekAndUpdatePlayheadPosition(TimeInComposition(timeInComposition));
                }
            }
        }

        private TimeSpan CalculatePositionInComposition(double pointerPositionX)
        {
            var rate = (pointerPositionX - CalculateXOffset()) / _selectedVideoFragmentView.Width;

            var timeInComposition = TimeSpan.FromSeconds(
                _selectedVideoFragmentView.StartTimeInComposition.TotalSeconds + _selectedVideoFragmentView.TrimmedDuration.TotalSeconds * rate);

            return TimeInComposition(timeInComposition);
        }

        private TimeSpan TimeInComposition(TimeSpan time)
        {
            if (time < TimeSpan.Zero)
            {
                time = TimeSpan.Zero;
            }
            var authoringSession = AuthoringSession;
            if (authoringSession != null && time > authoringSession.CompositionBuilder.MediaComposition.Duration)
            {
                time = authoringSession.CompositionBuilder.MediaComposition.Duration;
            }
            return time;
        }

        private int CalculateViewIndexForTake(Take take)
        {
            int index = 0;
            var view = _videoFragmentViews.FirstOrDefault(f => f.ClipViewModel.Take.Id == take.Id);
            if (view != null)
            {
                index = Thumbnails.Items.IndexOf(view);
            }
            else
            {
                var session = AuthoringSession;
                if (session != null)
                {
                    foreach (var mediaClip in session.Project.TcMediaComposition.MediaClips)
                    {
                        if (mediaClip.ActiveTake != null)
                        {
                            if (mediaClip.ActiveTake.Id != take.Id)
                            {
                                view = _videoFragmentViews.LastOrDefault(f => f.ClipViewModel.Take.Id == mediaClip.ActiveTake.Id);
                                if (view != null)
                                {
                                    index = Thumbnails.Items.IndexOf(view) + 1;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return index;
        }

        private VideoFragmentView CreateAndInsertView(TcMediaClip mediaClip, int index)
        {
            var videoFragmentView = new VideoFragmentView(mediaClip, _pixelsPerSecond, _cancellationToken);
            videoFragmentView.VideoFragmentViewChangedEvent += VideoFragmentView_VideoFragmentViewChangedEvent;
            
            if (index == -1)
            {
                index = Thumbnails.Items.IndexOf(_selectedVideoFragmentView) + 1;
            }
            Thumbnails.Items.Insert(index, videoFragmentView);
            UpdateVideoFragmanetActionStreamPanel();
            return videoFragmentView;
        }

        private async void VideoFragmentView_VideoFragmentViewChangedEvent(VideoFragment videoFragment)
        {
            RefreshComposition();
            Thumbnails.UpdateLayout();

            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (_position > videoFragment.MediaClip.EndTimeInComposition)
                {
                    _position = videoFragment.MediaClip.EndTimeInComposition;
                }
                if (_position < videoFragment.MediaClip.StartTimeInComposition)
                {
                    _position = videoFragment.MediaClip.StartTimeInComposition;
                }
            }
            SeekAndUpdatePlayheadPosition(_position);
            await UpdateClipViewsVisibleRegions();
        }

        public async Task<bool> TryCreateTransition(TcMediaClip mediaClip, Transition.Type transitionType)
        {
            var result = false;
            var nextMediaClip = MediaClipAfter(mediaClip);
            if (nextMediaClip != null)
            {
                result = mediaClip.Transition.TryCreateTransitionBetweenShots(mediaClip, nextMediaClip, transitionType);
                if (result)
                {
                    await ApplyTransitionBetweenClips(mediaClip, nextMediaClip);
                }
            }
            return result;
        }

        private TcMediaClip MediaClipAfter(TcMediaClip mediaClip)
        {
            TcMediaClip nextMediaClip = null;
            var session = AuthoringSession;
            if (session != null)
            {
                var indexOfMediaClip = session.Project.TcMediaComposition.IndexOf(mediaClip);
                if (indexOfMediaClip > -1)
                {
                    for (var i = indexOfMediaClip + 1; i < session.Project.TcMediaComposition.MediaClips.Count; i++)
                    {
                        if (session.Project.TcMediaComposition.MediaClips[i].ActiveTake != null)
                        {
                            nextMediaClip = session.Project.TcMediaComposition.MediaClips[i];
                            break;
                        }
                    }
                }
            }

            return nextMediaClip;
        }

        private async Task ApplyTransitionBetweenClips(TcMediaClip mediaClip, TcMediaClip nextMediaClip)
        {
            var videoFragment = mediaClip.ActiveTake.VideoFragments.Last();
            videoFragment.UpdateTailTransition(mediaClip.Transition.IncreaseTime);

            var view = _videoFragmentViews.FirstOrDefault(v => v.ClipViewModel == videoFragment);
            view.CalculateNewRanges(_pixelsPerSecond);

            if (nextMediaClip != null)
            {
                videoFragment = nextMediaClip.ActiveTake.VideoFragments.First();
                videoFragment.UpdateHeadTransition(mediaClip.Transition.IncreaseTime);
                view = _videoFragmentViews.FirstOrDefault(v => v.ClipViewModel == videoFragment);
                view.CalculateNewRanges(_pixelsPerSecond);
            }

            RefreshComposition(true);

            //UpdateClipViewsList();
            Thumbnails.UpdateLayout();

            await UpdateClipViewsVisibleRegions();
            _position = view.StartTimeInComposition;
            SeekAndUpdatePlayheadPosition(_position);
        }

        private void RefreshComposition(bool isNeedToSetStream = false)
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.CompositionBuilder.Refresh();
                TcCompositionRefreshEvent?.Invoke(isNeedToSetStream);
            }
        }

        private void SendEnabledSplitMessage()
        {
            if (_selectedVideoFragmentView != null)
            {
                var isEnabled = (_selectedVideoFragmentView.StartTimeInComposition + VideoFragment.MinSplitDuration <= _position &&
                    _selectedVideoFragmentView.EndTimeInComposition - VideoFragment.MinSplitDuration >= _position);
                if (_isEnabledLastMessageWasSend != isEnabled)
                {
                    Messenger.Default.Send(new SwitchSplitButtonEnableMessage(isEnabled));
                    _isEnabledLastMessageWasSend = isEnabled;
                }
            }
        }

        private void SeekAndUpdatePlayheadPosition(TimeSpan time)
        {
            TcCompositionSeekEvent?.Invoke(new TcCompositionSeekEventArgs(time));
            UpdatePlayheadPosition(time);
        }

        private async Task UpdateClipViewsVisibleRegions()
        {
            var regions = ClipViewsVisibleRegions();
            foreach (var item in regions)
            {
                await item.View.UpdateVisibleRegion(item.VisibleArea, _cancellationToken);
            }
        }

        private void UpdateCurrentView()
        {
            var view = _videoFragmentViews.FirstOrDefault(v => v.StartTimeInComposition <= _position && v.EndTimeInComposition >= _position);
            UpdateSelectedView(view);
        }

        private async Task UpdateVideoFragmentViews()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await RemoveViews();

                int index = 0;
                foreach (var clip in session.Project.TcMediaComposition.MediaClips)
                {
                    try
                    {
                        if (clip.ActiveTake != null)
                        {
                            var views = Thumbnails.Items.OfType<VideoFragmentView>().ToList();
                            var view = views.FirstOrDefault(v => v.TcMediaClip.Id == clip.Id);
                            if (view != null)
                            {
                                view.CalculateNewRanges(_pixelsPerSecond);
                                ++index;
                                continue;
                            }
                            CreateAndInsertView(clip, index);
                            ++index;
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                }

                UpdateClipViewsList();
                await UpdateClipViewsVisibleRegions();
                DefaultSelection();
            }
        }

        private async Task RemoveViews()
        {
            var transitions = Thumbnails.Items.OfType<CompositionTransitionView>().ToList();
            foreach (var transition in transitions)
            {
                Thumbnails.Items.Remove(transition);
            }
            var session = AuthoringSession;
            if (session != null)
            {
                var views = Thumbnails.Items.OfType<VideoFragmentView>().ToList();
                foreach (var view in views)
                {
                    if (!session.Project.TcMediaComposition.MediaClips.Any(f => f.Id == view.TcMediaClip.Id))
                    {
                        view.VideoFragmentViewChangedEvent -= VideoFragmentView_VideoFragmentViewChangedEvent;
                        await view.Clean();
                        Thumbnails.Items.Remove(view);
                    }
                }
            }
        }

        private void SubscribeShotToTakeEvents(Shot shot)
        {

        }

        private void UnsubscribeShotFromTakeEvents(Shot shot)
        {

        }

        private void RemoveVideoFragmentView(VideoFragmentView view)
        {
            Thumbnails.Items.Remove(view);
            UpdateClipViewsList();
        }

        private void RemoveAllTransitionViews()
        {
            CompositionTransitionView transitionView = null;
            do
            {
                transitionView = Thumbnails.Items.OfType<CompositionTransitionView>().FirstOrDefault();
                Thumbnails.Items.Remove(transitionView);
            } while (transitionView != null);
        }
        private void UpdateTransitionViews()
        {
            var previousClip = _videoFragmentViews.FirstOrDefault()?.TcMediaClip;
            if (String.IsNullOrEmpty(previousClip?.Id))
            {
                return;
            }

            var mediaClipChanged = false;
            int i = 0;
            while (i < Thumbnails.Items.Count)
            {
                if (Thumbnails.Items[i] is VideoFragmentView)
                {
                    VideoFragmentView view = Thumbnails.Items[i] as VideoFragmentView;
                    mediaClipChanged = (view.TcMediaClip.Id != previousClip.Id);
                }
                if (Thumbnails.Items[i] is CompositionTransitionView && !mediaClipChanged)
                {
                    Thumbnails.Items.RemoveAt(i);
                    continue;
                }

                if (Thumbnails.Items[i] is VideoFragmentView && mediaClipChanged)
                {
                    var currentClip = (Thumbnails.Items[i] as VideoFragmentView).ClipViewModel.Take.TcMediaClip;
                    CreateAndInsertTransitionView(i, previousClip);
                    mediaClipChanged = false;
                    previousClip = currentClip;
                }
                ++i;
            }
        }

        private void CreateAndInsertTransitionView(int index, TcMediaClip mediaClip)
        {
            CompositionTransitionView transitionView = new CompositionTransitionView(mediaClip, this);
            Thumbnails.Items.Insert(index, transitionView);
        }

        private void UpdateVideoFragmanetActionStreamPanel()
        {
            _videoFragmentViews = Thumbnails.Items.Cast<ICompositionItemView>().Where(i => i.ItemType == CompositionItemTypes.Clip).Cast<VideoFragmentView>().ToList();
            var session = AuthoringSession;
            if (session != null)
            {
                foreach (var fragmentView in _videoFragmentViews)
                {
                    var commands = session.CompositionBuilder.ActionsStream.Commands.Where(x => x.ClipId == fragmentView.ClipViewModel.Id
                    && (x.Type == Models.CommandTypes.Create || x.Type == Models.CommandTypes.Navigate)).ToList();
                    fragmentView.UpdateActionStreamPanel(commands);
                }
            }
        }

        #endregion
    }
}
