﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Pitch.DataLayer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views
{
    public delegate void TransitionSelected(object sender, Transition.Type type);
    public sealed partial class TransitionsListView : UserControl
    {
        public event TransitionSelected TransitionSelectedEvent;
        public int SelectedIndex
        {
            set
            {
                TransitionListView.SelectedIndex = value;
            }
            get
            {
                return TransitionListView.SelectedIndex;
            }
        }
        public TransitionsListView()
        {
            InitializeComponent();
            Loaded += TransitionsListView_Loaded;
        }

        private void TransitionsListView_Loaded(object sender, RoutedEventArgs e)
        {
            Unloaded += TransitionsListView_Unloaded;
        }

        private void TransitionsListView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= TransitionsListView_Loaded;
            Unloaded -= TransitionsListView_Unloaded;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(sender is ListView)
            {
                var listView = sender as ListView;
                var value = (Transition.Type)listView.SelectedIndex;
                TransitionSelectedEvent?.Invoke(this, value);
            }
        }
    }
}
