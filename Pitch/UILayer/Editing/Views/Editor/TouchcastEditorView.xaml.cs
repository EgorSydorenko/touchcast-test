﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.Helpers.Extensions;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Pitch.UILayer.Editing.Views.PreviewViews;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views
{
    public delegate void MediaPlayerStateChanged(bool isPlaying);

    public sealed partial class TouchcastEditorView
    {
        #region Static and constant values
        private readonly TimeSpan MediaPlayerStatePollingInterval = TimeSpan.FromMilliseconds(33);
        private readonly Duration _duration = new Duration(TimeSpan.FromMilliseconds(150));
        private readonly Duration _opacityAnimationDuration = new Duration(TimeSpan.FromMilliseconds(500));
        private readonly Size CompactPlayerSize = new Size(320, 180);
        private readonly CoreCursor ArrowCursor = new CoreCursor(CoreCursorType.Arrow, 0);
        private readonly CoreCursor SizeAllCursor = new CoreCursor(CoreCursorType.SizeAll, 0);
        private readonly string SurfaceDialMenuText = "Touchcast";
        #endregion

        #region Private Fields
        private TimeSpan _delta = TimeSpan.FromMilliseconds(10);
        private bool _playing;
        private bool _isPlayerLayoutCompact;
        private bool _isShortcutSubscribed;
        private double _bottomPanelHeight;
        private Point _previousPosition = new Point();

        private CompositeTransform _transform;
        private DispatcherTimer _timer;
        private MediaCompositionView _compositionView;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private WeakReference<AuthoringSession> _authoringSession;
        private ActionStreamAnalyzer _analyzer;
        private readonly Size _videoSize;

        private bool _isAnimationStarted;
        private bool _isNeedAnimate;
        private bool _isButtonPressed;
        private bool _isPlayAfterLoading = false;
        private DoubleAnimation _xScaleAnimation = new DoubleAnimation();
        private DoubleAnimation _yScaleAnimation = new DoubleAnimation();
        private DoubleAnimation _xPositionAnimation = new DoubleAnimation();
        private DoubleAnimation _yPositionAnimation = new DoubleAnimation();
        private DoubleAnimation _bottomPanelAnimation = new DoubleAnimation();

        private Storyboard _playerModeAnimation = new Storyboard();

        private DoubleAnimation _opacityAnimation = new DoubleAnimation();
        private Storyboard _fadeInAnimation = new Storyboard();
        private Point _lastPoint;
        private object _lockObject = new object();

        private double _millisecondsPerFrame = 33.3333;

        private TimeSpan _positionIncreaseStep;
        private double _framesPerSecond = 30;

        private bool _isEnabledSplitButtonMessage;
        private bool _isEditEnabled;
        private MediaElementState _currentState;
        private object _radialControler;
        private object _seekLockObject = new object();
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        private bool IsEditEnabled
        {
            get => _isEditEnabled;
            set
            {
                _isEditEnabled = value;

                DeleteClipButton.IsEnabled = _isEditEnabled;
                DuplicateClipButton.IsEnabled = _isEditEnabled;
                SplitClipButton.IsEnabled = _isEditEnabled ? _isEnabledSplitButtonMessage : false;
            }
        }

        public event ActiveTcMediaClipChanged ActiveTcMediaClipChangedEvent;
        public event MediaPlayerStateChanged MediaPlayerStateChangedEvent;
        #endregion

        #region Life Cycle

        public TouchcastEditorView()
        {
            InitializeComponent();
            _authoringSession = new WeakReference<AuthoringSession>(SimpleIoc.Default.GetInstance<AuthoringSession>());

            Loaded += TcMediaCompositionView_Loaded;
            Unloaded += TcMediaCompositionView_Unloaded;

            var zoomValue = 6;
            var timeValue = zoomValue == 0 ? 1 : zoomValue;
            var pixselsPerSecond = VideoFragmentView.FrameSize.Width / timeValue;
            _compositionView = new MediaCompositionView(_cancellationTokenSource.Token, pixselsPerSecond);
            _compositionView.TcCompositionRefreshEvent += CompositionView_TcCompositionRefreshEvent;
            _compositionView.ActiveTcMediaClipChanged += CompositionView_ActiveTcMediaClipChanged;

            var bitmapSize = ServiceLocator.Current.GetInstance<Services.IMediaMixerService>().ResultVideoSize;
            _videoSize = new Size(bitmapSize.Width, bitmapSize.Height);

            if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.Input.RadialController"))
            {
                _radialControler = RadialController.CreateForCurrentView();
            }

            //TODO
            CompositionItemContainer.Children.Add(_compositionView);
            Storyboard.SetTarget(_xPositionAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_xPositionAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateX)");
            _xPositionAnimation.Duration = _duration;
            _xPositionAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_yPositionAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_yPositionAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateY)");
            _yPositionAnimation.Duration = _duration;
            _yPositionAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_xScaleAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_xScaleAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleX)");
            _xScaleAnimation.Duration = _duration;
            _xScaleAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_yScaleAnimation, PlayerLayout);
            Storyboard.SetTargetProperty(_yScaleAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleY)");
            _yScaleAnimation.Duration = _duration;
            _yScaleAnimation.EnableDependentAnimation = false;

            Storyboard.SetTarget(_bottomPanelAnimation, PlayerBottomPanel);
            Storyboard.SetTargetProperty(_bottomPanelAnimation, "(UIElement.Height)");
            _bottomPanelAnimation.Duration = _duration;
            _bottomPanelAnimation.EnableDependentAnimation = false;

            _playerModeAnimation.Children.Add(_xPositionAnimation);
            _playerModeAnimation.Children.Add(_yPositionAnimation);
            _playerModeAnimation.Children.Add(_xScaleAnimation);
            _playerModeAnimation.Children.Add(_yScaleAnimation);
            _playerModeAnimation.Children.Add(_bottomPanelAnimation);
            _playerModeAnimation.FillBehavior = FillBehavior.HoldEnd;

            Storyboard.SetTarget(_opacityAnimation, PlayerViewBorder);
            Storyboard.SetTargetProperty(_opacityAnimation, "UIElement.Opacity");
            _opacityAnimation.Duration = _opacityAnimationDuration;
            _opacityAnimation.EnableDependentAnimation = false;
            _fadeInAnimation.Children.Add(_opacityAnimation);
            _fadeInAnimation.FillBehavior = FillBehavior.HoldEnd;
        }

#if DEBUG
        ~TouchcastEditorView()
        {
            System.Diagnostics.Debug.WriteLine("********************TouchcastEditorView Destructor********************");
        }
#endif

        public async Task Cleanup()
        {
            CancelUpdateThumbnails();
            await _compositionView.Cleanup();
        }

        public void Hide()
        {
            MediaPlayerElement.Stop();
            Visibility = Visibility.Collapsed;

            RestorePlayerFromCompactWithoutAnimation();
        }

        public void Show()
        {
            Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await Task.Delay(TimeSpan.FromMilliseconds(100));
                PlayButton.Focus(FocusState.Programmatic);
            });
        }

        #endregion

        #region Callbacks

        #region TouchcastEditorView

        private void TcMediaCompositionView_Loaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("TcMediaCompositionView_Loaded");
            if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.Input.RadialController"))
            {
                if (_radialControler != null)
                {
                    RandomAccessStreamReference icon =
                            RandomAccessStreamReference.CreateFromUri(
                            new Uri("ms-appx:///Assets/SurfaceDialIcon.png"));
                    RadialControllerMenuItem myItem =
                        RadialControllerMenuItem.CreateFromIcon(SurfaceDialMenuText, icon);
                    (_radialControler as RadialController).Menu.Items.Add(myItem);

                    (_radialControler as RadialController).RotationChanged += RadialControler_RotationChanged;
                    (_radialControler as RadialController).ButtonClicked += RadialControler_ButtonClicked;
                }
            }
            MediaPlayerElement.CurrentStateChanged += MediaPlayerElement_CurrentStateChanged;
            MediaPlayerElement.SeekCompleted += MediaPlayerElement_SeekCompleted;
            MediaPlayerElement.MediaFailed += MediaPlayerElement_MediaFailed;
            MediaPlayerElement.MediaOpened += MediaPlayerElement_MediaOpened;

            PlayerLayout.PointerPressed += PlayerLayout_PointerPressed;
            PlayerLayout.PointerReleased += PlayerLayout_PointerReleased;
            PlayerLayout.PointerMoved += PlayerLayout_PointerMoved;
            PlayerLayout.PointerExited += PlayerLayout_PointerExited;
            PlayerLayout.DoubleTapped += PlayerLayout_DoubleTapped;
            _compositionView.TcCompositionSeekEvent += MediaItems_TcCompositionSeekEvent;
            VappContainer.PointerMoved += VappContainer_PointerMoved;

            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                ControlPanelEnabled(authoringSession.CompositionBuilder.MediaStreamSource != null);
                MediaPlayerElement.SetMediaStreamSource(authoringSession.CompositionBuilder.MediaStreamSource);
                CalculateFrameRate();
                if (_analyzer == null)
                {
                    _analyzer = new ActionStreamAnalyzer(authoringSession.CompositionBuilder.ActionsStream.Commands);
                }
                else
                {
                    _analyzer.ActionStream = authoringSession.CompositionBuilder.ActionsStream.Commands;
                }
            }

            _timer = new DispatcherTimer();
            _timer.Interval = MediaPlayerStatePollingInterval;
            _timer.Tick += Timer_Tick;

            _transform = new CompositeTransform();
            _transform.TranslateX = 0;
            _transform.TranslateY = 0;
            _transform.ScaleX = 1;
            _transform.ScaleY = 1;
            PlayerLayout.RenderTransform = _transform;
            Windows.UI.Xaml.Controls.Canvas.SetZIndex(PlayerLayout, 300);
            Windows.UI.Xaml.Controls.Canvas.SetZIndex(VappContainer, 200);

            Messenger.Default.Register<SwitchSplitButtonEnableMessage>(this, msg => {
                _isEnabledSplitButtonMessage = msg.IsEnabled;
                SplitClipButton.IsEnabled = _isEnabledSplitButtonMessage ? _isEditEnabled : false;
            });

            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            VappContainer.Visibility = Visibility.Collapsed;
            _compositionView.IsEnabled = true;
            _isPlayerLayoutCompact = false;
            _isPlayAfterLoading = App.Locator.AppState != ApplicationState.Editing;

            Shortcut.Manager.Instance.EditShortcutsManager.PlayMediaShortcutEvent += EditShortcutsManager_PlayMediaShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomInShortcutEvent += EditShortcutsManager_ZoomInShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomOutShortcutEvent += EditShortcutsManager_ZoomOutShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NextClipShortcutEvent += EditShortcutsManager_NextClipShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.PrevClipShortcutEvent += EditShortcutsManager_PrevClipShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.PrevFrameShortcutEvent += EditShortcutsManager_PrevFrameShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NextFrameShortcutEvent += EditShortcutsManager_NextFrameShortcutEvent;
            SubscribeToEditShortcuts();
            //Show();
            IsEnabledChanged += TouchcastEditorView_IsEnabledChanged;
        }

        private void TcMediaCompositionView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= TcMediaCompositionView_Loaded;
            Unloaded -= TcMediaCompositionView_Unloaded;
            LogQueue.WriteToFile("TcMediaCompositionView_Unloaded");

            RestorePlayerFromCompactWithoutAnimation();

            Messenger.Default.Unregister(this);
            _timer.Stop();

            Shortcut.Manager.Instance.EditShortcutsManager.PlayMediaShortcutEvent -= EditShortcutsManager_PlayMediaShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomInShortcutEvent -= EditShortcutsManager_ZoomInShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.ZoomOutShortcutEvent -= EditShortcutsManager_ZoomOutShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NextClipShortcutEvent -= EditShortcutsManager_NextClipShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.PrevClipShortcutEvent -= EditShortcutsManager_PrevClipShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.PrevFrameShortcutEvent -= EditShortcutsManager_PrevFrameShortcutEvent;
            Shortcut.Manager.Instance.EditShortcutsManager.NextFrameShortcutEvent -= EditShortcutsManager_NextFrameShortcutEvent;
            UnsubscribeFromEditShortcuts();
            if (_radialControler != null)
            {
                (_radialControler as RadialController).Menu.Items.Clear();
                (_radialControler as RadialController).RotationChanged -= RadialControler_RotationChanged;
                (_radialControler as RadialController).ButtonClicked -= RadialControler_ButtonClicked;
                _radialControler = null;
            }
            MediaPlayerElement.CurrentStateChanged -= MediaPlayerElement_CurrentStateChanged;
            MediaPlayerElement.SeekCompleted -= MediaPlayerElement_SeekCompleted;
            MediaPlayerElement.MediaFailed -= MediaPlayerElement_MediaFailed;
            MediaPlayerElement.MediaOpened -= MediaPlayerElement_MediaOpened;

            PlayerLayout.PointerPressed -= PlayerLayout_PointerPressed;
            PlayerLayout.PointerReleased -= PlayerLayout_PointerReleased;
            PlayerLayout.PointerMoved -= PlayerLayout_PointerMoved;
            PlayerLayout.PointerExited -= PlayerLayout_PointerExited;
            PlayerLayout.DoubleTapped -= PlayerLayout_DoubleTapped;
            _compositionView.TcCompositionRefreshEvent -= CompositionView_TcCompositionRefreshEvent;
            _compositionView.ActiveTcMediaClipChanged -= CompositionView_ActiveTcMediaClipChanged;
            _compositionView.TcCompositionSeekEvent -= MediaItems_TcCompositionSeekEvent;
            VappContainer.PointerMoved -= VappContainer_PointerMoved;
            _timer.Tick -= Timer_Tick;
            VappContainer.Children.Clear();
            IsEnabledChanged -= TouchcastEditorView_IsEnabledChanged;
        }

        #endregion

        #region MediaPlayerElement

        private void MediaPlayerElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            ControlPanelEnabled(true);
            UpdateTimeTexts();
            if (_isPlayAfterLoading)
            {
                _isPlayAfterLoading = false;
                MediaPlayerElement.Play();
            }
        }

        private void MediaPlayerElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            LogQueue.WriteToFile($"MediaPlayerElement_MediaFailed: {e.ErrorMessage}");
            ControlPanelEnabled(false);
        }

        private void MediaItems_TcCompositionSeekEvent(TcCompositionSeekEventArgs args)
        {
            lock (_seekLockObject)
            {
                _currentState = MediaPlayerElement.CurrentState;
                MediaPlayerElement.Pause();
                var time = args.Position;
                if (args.Position >= MediaPlayerElement.NaturalDuration.TimeSpan)
                {
                    time = args.Position - _delta;
                }
                MediaPlayerElement.Position = time;
            }
        }

        private void MediaPlayerElement_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (MediaPlayerElement.CurrentState)
            {
                case MediaElementState.Opening:
                    Messenger.Default.Send(new PausePlayerMessage());
                    IsEditEnabled = true;
                    break;
                case MediaElementState.Playing:
                    UnsubscribeFromEditShortcuts();
                    _compositionView.IsEditClipsEnabled(false);
                    _timer.Start();
                    if (!_isPlayerLayoutCompact)
                    {
                        IsEditEnabled = false;
                        Messenger.Default.Send(new PlayPlayerMessage());
                    }
                    PauseContent.Visibility = Visibility.Visible;
                    PlayContent.Visibility = Visibility.Collapsed;
                    break;
                case MediaElementState.Paused:
                    _timer.Stop();
                    UpdatePlayheadPositionIfNeeded();
                    if (!_isPlayerLayoutCompact)
                    {
                        IsEditEnabled = true;
                        Messenger.Default.Send(new PausePlayerMessage());
                    }
                    PlayContent.Visibility = Visibility.Visible;
                    PauseContent.Visibility = Visibility.Collapsed;
                    _compositionView.IsEditClipsEnabled(true);
                    SubscribeToEditShortcuts();
                    break;
                case MediaElementState.Stopped:
                    _timer.Stop();
                    IsEditEnabled = true;
                    Messenger.Default.Send(new PausePlayerMessage());
                    PlayContent.Visibility = Visibility.Visible;
                    PauseContent.Visibility = Visibility.Collapsed;
                    SubscribeToEditShortcuts();
                    break;
            }
            MediaPlayerStateChangedEvent?.Invoke(MediaPlayerElement.CurrentState == MediaElementState.Playing);
        }

        private void MediaPlayerElement_SeekCompleted(object sender, RoutedEventArgs e)
        {
            lock (_seekLockObject)
            {
                UpdateTimeTexts();
                if (_currentState == MediaElementState.Playing)
                {
                    MediaPlayerElement.Play();
                }
            }
        }
        #endregion

        #region PlayerLayout Pointer
        private void PlayerLayout_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            _isButtonPressed = true;
            _isNeedAnimate = true;
            MediaPlayerElement.CapturePointer(e.Pointer);

            if (_isPlayerLayoutCompact)
            {
                var point = e.GetCurrentPoint(this);
                _lastPoint = point.Position;
            }
        }

        private void PlayerLayout_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            lock (_lockObject)
            {
                if (_isPlayerLayoutCompact)
                {
                    if (_isButtonPressed)
                    {
                        _isNeedAnimate = false;

                        Window.Current.CoreWindow.PointerCursor = SizeAllCursor;
                        var currentPoint = e.GetCurrentPoint(this);

                        var maxX = RootLayout.ActualWidth - PlayerLayout.ActualWidth * _transform.ScaleX;
                        var x = _transform.TranslateX + currentPoint.Position.X - _lastPoint.X;
                        if (x < 0) x = 0;
                        if (x > maxX) x = maxX;

                        var maxY = RootLayout.ActualHeight - PlayerLayout.ActualHeight * _transform.ScaleY;
                        var y = _transform.TranslateY + currentPoint.Position.Y - _lastPoint.Y;
                        if (y < 0) y = 0;
                        if (y > maxY) y = maxY;

                        _transform.TranslateX = x;
                        _transform.TranslateY = y;

                        _lastPoint = currentPoint.Position;
                    }
                }
            }
        }

        private async void PlayerLayout_DoubleTapped(object sender, Windows.UI.Xaml.Input.DoubleTappedRoutedEventArgs e)
        {
            if (e.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Touch || e.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Pen)
            {
                if (_isPlayerLayoutCompact)
                {
                    if (_isAnimationStarted || !_isNeedAnimate)
                    {
                        return;
                    }
                    _isAnimationStarted = true;

                    await RestorePlayerFromCompactMode();
                    _isAnimationStarted = false;
                }
            }
        }

        private async Task RestorePlayerFromCompactMode()
        {
            _isPlayerLayoutCompact = false;
            _previousPosition.X = _transform.TranslateX;
            _previousPosition.Y = _transform.TranslateY;

            PlaybackControlPanel.Visibility = Visibility.Visible;
            _compositionView.Visibility = Visibility.Visible;

            await UpdateUi(false);

            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
            if (MediaPlayerElement.CurrentState == MediaElementState.Paused && _playing)
            {
                MediaPlayerElement.Play();
                _playing = false;
            }

            VappContainer.Children.Clear();
        }

        private async void PlayerLayout_PointerReleased(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            lock (_lockObject)
            {
                _isButtonPressed = false;

                if (_isAnimationStarted || !_isNeedAnimate)
                {
                    return;
                }
                _isAnimationStarted = true;
            }

            if (_isPlayerLayoutCompact)
            {
                await RestorePlayerFromCompactMode();
                _isAnimationStarted = false;
                return;
            }

            var point = e.GetCurrentPoint(MediaPlayerElement).Position;
            var cmd = _analyzer.GetCommandByTimeAndPosition(MediaPlayerElement.Position, point, _videoSize);
            if (cmd != null)
            {
                UIElement element = null;
                if (!String.IsNullOrEmpty(cmd.Url))
                {
                    var authoringSession = AuthoringSession;
                    if (authoringSession != null)
                    {
                        var extension = System.IO.Path.GetExtension(cmd.Url);
                        if (!String.IsNullOrEmpty(extension) && !(cmd.Url.Contains("http://") || cmd.Url.Contains("https://")))
                        {
                            extension = extension.ToLower();
                            var url = cmd.Url.Replace("($PERFORMANCE_BASE_URL)/", authoringSession.Project.WorkingFolder.Path + "\\");
                            if (FileToAppHelper.DocumentExtensions.Contains(extension))
                            {
                                element = new PreviewDocumentView(url);
                            }
                            else if (FileToAppHelper.ImageExtensions.Contains(extension))
                            {
                                element = new PreviewImageView(url);
                            }
                            else if (FileToAppHelper.VideoExtensions.Contains(extension))
                            {
                                element = new PreviewVideoView(url);
                            }
                        }
                        else
                        {
                            element = new PreviewWebView(cmd.Url);
                        }

                        if (element != null)
                        {
                            _isPlayerLayoutCompact = true;

                            if (cmd.PauseOnInteration.Equals("YES") && MediaPlayerElement.CurrentState == MediaElementState.Playing)
                            {
                                MediaPlayerElement.Pause();
                                _playing = true;
                            }
                            VappContainer.Children.Add(element);
                            await UpdateUi(true);
                        }
                    }

                }
            }
            _isAnimationStarted = false;
        }

        private void VappContainer_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        private void PlayerLayout_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            Window.Current.CoreWindow.PointerCursor = ArrowCursor;
        }

        #endregion

        #region Playback

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            PlayPause();
        }

        private void PlayPause()
        {
            if (MediaPlayerElement.CurrentState == MediaElementState.Playing)
            {
                MediaPlayerElement.Pause();
            }
            else
            {
                if (MediaPlayerElement.Position + _delta >= MediaPlayerElement.NaturalDuration)
                {
                    MediaPlayerElement.Position = TimeSpan.FromSeconds(0);
                }
                MediaPlayerElement.Play();
            }
        }

        private void PreviousClipButton_Click(object sender, RoutedEventArgs e)
        {
            _compositionView.PreviousClip();
        }

        private void NextClipButton_Click(object sender, RoutedEventArgs e)
        {
            _compositionView.NextClip();
        }

        private void FastForwardButton_Click(object sender, RoutedEventArgs e)
        {
            IncreasePosition(_positionIncreaseStep);
        }

        private void FastBackwardButton_Click(object sender, RoutedEventArgs e)
        {
            IncreasePosition(-_positionIncreaseStep);
        }
        #endregion

        #region Timer

        private void Timer_Tick(object sender, object e)
        {
            UpdatePlayheadPosition();
        }

        #endregion

        #region Surface Dial

        private double _currentDialAngle = 0.0;
        private const double DialAngleToSwitchOneFrame = 5;
        private void RadialControler_RotationChanged(RadialController sender, RadialControllerRotationChangedEventArgs args)
        {
            try
            {
                if (MediaPlayerElement.CurrentState == MediaElementState.Playing)
                {
                    MediaPlayerElement.Pause();
                    _currentState = MediaElementState.Paused;
                }

                _currentDialAngle = _currentDialAngle + args.RotationDeltaInDegrees;
                var composition = AuthoringSession?.CompositionBuilder;
                if (composition != null)
                {
                    var compositionTime = composition.MediaStreamSource.Duration;
                    if (Math.Abs(_currentDialAngle) > DialAngleToSwitchOneFrame)
                    {
                        var frames = _currentDialAngle / DialAngleToSwitchOneFrame;
                        _currentDialAngle = 0;
                        var timeToSeek = TimeSpan.FromMilliseconds(frames * 33);
                        var nextTime = MediaPlayerElement.Position + timeToSeek;
                        if (nextTime >= TimeSpan.Zero && nextTime <= compositionTime)
                        {
                            MediaPlayerElement.Position = nextTime;
                            _compositionView.UpdatePlayheadPositionIfNeeded(nextTime);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                LogQueue.WriteToFile($"Exception in TouchcastEditorView.RadialControler_RotationChanged : {e.Message}");
            }
        }

        private async void RadialControler_ButtonClicked(RadialController sender, RadialControllerButtonClickedEventArgs args)
        {
            CancelUpdateThumbnails();
            await _compositionView.SplitCurrentVideoFragment(_cancellationTokenSource.Token);
            //Trick for update buffer of mediaplayer. 
            var volumeLevel = MediaPlayerElement.Volume;
            MediaPlayerElement.Volume = 0.0;
            MediaPlayerElement.Play();
            MediaPlayerElement.Pause();
            MediaPlayerElement.Volume = volumeLevel;
        }

        #endregion

        private void CompositionView_TcCompositionRefreshEvent(bool isNeedToResetStream)
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (MediaPlayerElement.CurrentState == MediaElementState.Closed || isNeedToResetStream)
                {
                    MediaPlayerElement.SetMediaStreamSource(authoringSession.CompositionBuilder.MediaStreamSource);
                    CalculateFrameRate();
                    MediaPlayerElement.Play();
                    MediaPlayerElement.Pause();
                }
                if (authoringSession.CompositionBuilder.MediaStreamSource != null)
                {
                    VideoDurationTextBlock.Text = TimeWithFrames(authoringSession.CompositionBuilder.MediaStreamSource.Duration);
                }
                ControlPanelEnabled(authoringSession.CompositionBuilder.MediaStreamSource != null);
                UpdateTimeTexts();

                if (_analyzer == null)
                {
                    _analyzer = new ActionStreamAnalyzer(authoringSession.CompositionBuilder.ActionsStream.Commands);
                }
                else
                {
                    _analyzer.ActionStream = authoringSession.CompositionBuilder.ActionsStream.Commands;
                }
            }
        }

        private void CompositionView_ActiveTcMediaClipChanged(object sender, TcMediaClip clip)
        {
            ActiveTcMediaClipChangedEvent?.Invoke(sender, clip);
        }

        private void CalculateFrameRate()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                var profile = authoringSession.CompositionBuilder.MediaComposition.CreateDefaultEncodingProfile();
                var frameRate = profile.Video.FrameRate;
                _framesPerSecond = (double)frameRate.Numerator / frameRate.Denominator;
                _millisecondsPerFrame = (double)frameRate.Denominator / frameRate.Numerator * 1000f;
                _positionIncreaseStep = TimeSpan.FromTicks((long)(_millisecondsPerFrame * TimeSpan.TicksPerMillisecond));
            }
        }

        private void CancelUpdateThumbnails()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource = new CancellationTokenSource();
        }

        private async Task AuthoringSession_AuthoringSessionDidResetEvent()
        {
            await _compositionView.Cleanup();
            _analyzer = null;
        }

        #region Shortcuts callbacks
        private Task<bool> EditShortcutsManager_PlayMediaShortcutEvent()
        {
            PlayPause();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_ZoomInShortcutEvent()
        {
            //ZoomIn();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_ZoomOutShortcutEvent()
        {
            //ZoomOut();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_PrevClipShortcutEvent()
        {
            _compositionView.PreviousClip();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_NextClipShortcutEvent()
        {
            _compositionView.NextClip();
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_NextFrameShortcutEvent()
        {
            IncreasePosition(_positionIncreaseStep);
            return Task.FromResult(true);
        }

        private Task<bool> EditShortcutsManager_PrevFrameShortcutEvent()
        {
            IncreasePosition(-_positionIncreaseStep);
            return Task.FromResult(true);
        }

        private async Task<bool> EditShortcutsManager_DuplicateClipShortcutEvent()
        {
            if (MediaPlayerElement.CurrentState != MediaElementState.Playing)
            {
                CancelUpdateThumbnails();
                await _compositionView.DuplicateCurrentVideoFragment(_cancellationTokenSource.Token);
            }
            return true;
        }

        private async Task<bool> EditShortcutsManager_DeleteClipShortcutEvent()
        {
            await DeleteClip();
            return true;
        }
        private async Task<bool> EditShortcutsManager_BackspaceShortcutEvent()
        {
            await DeleteClip();
            return true;
        }

        private async Task<bool> EditShortcutsManager_SplitClipShortcutEvent()
        {
            if (MediaPlayerElement.CurrentState != MediaElementState.Playing)
            {
                CancelUpdateThumbnails();
                await _compositionView.SplitCurrentVideoFragment(_cancellationTokenSource.Token);
            }
            return true;
        }

        #endregion

        private async void SplitClipButton_Click(object sender, RoutedEventArgs e)
        {
            CancelUpdateThumbnails();
            await _compositionView.SplitCurrentVideoFragment(_cancellationTokenSource.Token);
        }

        private async void DeleteClipButton_Click(object sender, RoutedEventArgs e)
        {
            CancelUpdateThumbnails();
            await _compositionView.DeleteCurrentVideoFragment(_cancellationTokenSource.Token);
        }

        private async void DuplicateClipButton_Click(object sender, RoutedEventArgs e)
        {
            CancelUpdateThumbnails();
            await _compositionView.DuplicateCurrentVideoFragment(_cancellationTokenSource.Token);
        }

        private void RootLayout_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_isPlayerLayoutCompact == true && _transform != null)
            {
                var transform = CalculateTransform();
                _transform.TranslateX = transform.TranslateX;
                _transform.TranslateY = transform.TranslateY;
                _transform.ScaleX = transform.ScaleX;
                _transform.ScaleY = transform.ScaleY;
            }
        }

        #endregion

        #region Private Methods

        private async Task DeleteClip()
        {
            if (MediaPlayerElement.CurrentState != MediaElementState.Playing)
            {
                CancelUpdateThumbnails();
                await _compositionView.DeleteCurrentVideoFragment(_cancellationTokenSource.Token);
            }
        }

        private void UpdatePlayheadPosition()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                var position = MediaPlayerElement.Position;
                if (position > authoringSession.CompositionBuilder.MediaComposition.Duration)
                {
                    position = (authoringSession.CompositionBuilder.MediaComposition.Duration);
                    _timer.Stop();
                }
                VideoTimeTextBlock.Text = TimeWithFrames(position);
                _compositionView.UpdatePlayheadPosition(position);
            }
        }

        private void UpdatePlayheadPositionIfNeeded()
        {
            if (AuthoringSession?.CompositionBuilder.MediaComposition.Duration > MediaPlayerElement.Position)
            {
                VideoTimeTextBlock.Text = TimeWithFrames(MediaPlayerElement.Position);
                _compositionView.UpdatePlayheadPositionIfNeeded(MediaPlayerElement.Position);
            }
        }

        private void IncreasePosition(TimeSpan deltaTime)
        {
            _compositionView.IncreasePosition(deltaTime);
        }

        private void ControlPanelEnabled(bool isEnabled)
        {
            ZoomControl.IsEnabled = isEnabled;
            PlaybackControl.IsEnabled = isEnabled;
        }

        private void UpdateTimeTexts()
        {
            VideoTimeTextBlock.Text = TimeWithFrames(MediaPlayerElement.Position);
        }

        private string TimeWithFrames(TimeSpan time)
        {
            int frames = (int)(time.Milliseconds / _millisecondsPerFrame); //-1;
            return time.ToString(@"mm\:ss") + $":{frames:00}";
        }

        private async Task UpdateUi(bool isVappShown)
        {
            if (!_playing)
            {
                if (isVappShown)
                {
                    Messenger.Default.Send(new PlayPlayerMessage());
                    IsEditEnabled = false;
                }
                else
                {
                    Messenger.Default.Send(new PausePlayerMessage());
                    IsEditEnabled = true;
                }
            }

            if (isVappShown)
            {
                Messenger.Default.Send(new DisableExportButtonMessage());
            }
            else
            {
                PlayerViewBorder.Opacity = 0;
            }

            VappContainer.Visibility = isVappShown ? Visibility.Visible : Visibility.Collapsed;
            ControlPanelEnabled(!isVappShown);
            _compositionView.IsEnabled = !isVappShown;

            var transform = CalculateTransform();

            _xPositionAnimation.From = isVappShown ? 0 : transform.TranslateX;
            _xPositionAnimation.To = isVappShown ? transform.TranslateX : 0;
            _yPositionAnimation.From = isVappShown ? 0 : transform.TranslateY;
            _yPositionAnimation.To = isVappShown ? transform.TranslateY : 0;

            _xScaleAnimation.From = isVappShown ? 1 : transform.ScaleX;
            _xScaleAnimation.To = isVappShown ? transform.ScaleX : 1;
            _yScaleAnimation.From = isVappShown ? 1 : transform.ScaleY;
            _yScaleAnimation.To = isVappShown ? transform.ScaleY : 1;

            if (!isVappShown)
            {
                _bottomPanelHeight = PlayerBottomPanel.ActualHeight;
            }

            _bottomPanelAnimation.From = isVappShown ? 1 : _bottomPanelHeight;
            _bottomPanelAnimation.To = isVappShown ? _bottomPanelHeight : 1;

            await _playerModeAnimation.BeginAsync();
            _transform = PlayerLayout.RenderTransform as CompositeTransform;

            if (isVappShown)
            {
                _isPlayerLayoutCompact = true;
                _compositionView.Visibility = Visibility.Collapsed;
                PlaybackControlPanel.Visibility = Visibility.Collapsed;

                _opacityAnimation.From = 0;
                _opacityAnimation.To = 1;
                await _fadeInAnimation.BeginAsync();
            }
        }

        private CompositeTransform CalculateTransform()
        {
            var transform = new CompositeTransform();

            var aspect = PlayerLayout.ActualWidth / PlayerLayout.ActualHeight;
            var _xAspectValue = CompactPlayerSize.Height * aspect / PlayerLayout.ActualWidth;

            _previousPosition.X = PlayerLayout.ActualWidth - CompactPlayerSize.Height * aspect - 10;
            _previousPosition.Y = PlayerLayout.ActualHeight - CompactPlayerSize.Height - 48;

            transform.TranslateX = _previousPosition.X;
            transform.TranslateY = _previousPosition.Y;

            transform.ScaleX = _xAspectValue;
            transform.ScaleY = CompactPlayerSize.Height / PlayerLayout.ActualHeight;

            return transform;
        }

        private void SubscribeToEditShortcuts()
        {
            if (!_isShortcutSubscribed)
            {
                _isShortcutSubscribed = true;
                Shortcut.Manager.Instance.EditShortcutsManager.DeleteClipShortcutEvent += EditShortcutsManager_DeleteClipShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.DuplicateClipShortcutEvent += EditShortcutsManager_DuplicateClipShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.SplitClipShortcutEvent += EditShortcutsManager_SplitClipShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.UndoShortcutEvent += EditShortcutsManager_UndoShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.RedoShortcutEvent += EditShortcutsManager_RedoShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.BackspaceShortcutEvent += EditShortcutsManager_BackspaceShortcutEvent;
            }
        }

        private void UnsubscribeFromEditShortcuts()
        {
            if (_isShortcutSubscribed)
            {
                _isShortcutSubscribed = false;
                Shortcut.Manager.Instance.EditShortcutsManager.DeleteClipShortcutEvent -= EditShortcutsManager_DeleteClipShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.DuplicateClipShortcutEvent -= EditShortcutsManager_DuplicateClipShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.SplitClipShortcutEvent -= EditShortcutsManager_SplitClipShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.UndoShortcutEvent -= EditShortcutsManager_UndoShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.RedoShortcutEvent -= EditShortcutsManager_RedoShortcutEvent;
                Shortcut.Manager.Instance.EditShortcutsManager.BackspaceShortcutEvent -= EditShortcutsManager_BackspaceShortcutEvent;
            }
        }

        private async Task<bool> EditShortcutsManager_RedoShortcutEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.EditorUndoRedoOriginator.Redo();
            }
            return true;
        }

        private async Task<bool> EditShortcutsManager_UndoShortcutEvent()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                await session.Project.EditorUndoRedoOriginator.Undo();
            }
            return true;
        }
        private void TouchcastEditorView_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is Boolean)
            {
                var isEnabled = (bool)e.NewValue;
                if (isEnabled)
                {
                    SubscribeToEditShortcuts();
                }
                else
                {
                    UnsubscribeFromEditShortcuts();
                }
            }
        }

        private void RestorePlayerFromCompactWithoutAnimation()
        {
            if (_isPlayerLayoutCompact)
            {
                _isPlayerLayoutCompact = false;
                _transform.TranslateX = 0;
                _transform.TranslateY = 0;
                _transform.ScaleX = 1;
                _transform.ScaleY = 1;
                _previousPosition.X = _transform.TranslateX;
                _previousPosition.Y = _transform.TranslateY;

                PlayerViewBorder.Opacity = 0;

                PlaybackControlPanel.Visibility = Visibility.Visible;
                _compositionView.Visibility = Visibility.Visible;

                ControlPanelEnabled(true);
                _compositionView.IsEnabled = true;

                Window.Current.CoreWindow.PointerCursor = ArrowCursor;
                VappContainer.Children.Clear();
            }
        }

        #endregion

        #region Public Methods

        public async void UpdateZoom(double value)
        {
            try
            {
                if (_compositionView != null)
                {
                    CancelUpdateThumbnails();
                    var timeValue = value == 0 ? 1 : value;
                    var pixselsPerSecond = VideoFragmentView.FrameSize.Width / timeValue;
                    await Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal,
                        async () =>
                        {
                            if (_cancellationTokenSource.Token.IsCancellationRequested) return;

                            await _compositionView.UpdateThumbnailsByNewZoom(pixselsPerSecond, _cancellationTokenSource.Token);
                        }
                    );
                }
            }
            catch (Exception)
            {

            }
        }

        public void PauseMediaContent()
        {
            if (MediaPlayerElement.CurrentState == MediaElementState.Playing)
            {
                MediaPlayerElement.Pause();
            }
            var previewElement = VappContainer.Children.FirstOrDefault();
            if (previewElement == null)
            {
                return;
            }
            (previewElement as PreviewVideoView)?.PauseMultimediaContent();
            (previewElement as PreviewWebView)?.PauseMultimediaContent();
        }

        #endregion
    }
}
