﻿using System;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.UILayer.Editing.Views
{
    public sealed partial class CompositionTransitionView : ICompositionItemView
    {
        #region Private fields
        private WeakReference<TcMediaClip> _mediaClip;
        private WeakReference<MediaCompositionView> _parent;
        private new MediaCompositionView Parent => _parent?.TryGetObject();
        #endregion

        public override TimeSpan TrimmedDuration => TcMediaClip.Transition.Duration;
        public override TimeSpan StartTimeInComposition => TcMediaClip.Transition.StartTimeInComposition;
        public override TimeSpan EndTimeInComposition => TcMediaClip.Transition.EndTimeInComposition;
        public override TcMediaClip TcMediaClip => _mediaClip?.TryGetObject();

        #region Life cycle
        public CompositionTransitionView(TcMediaClip mediaClip, MediaCompositionView parent)
        {
            _mediaClip = new WeakReference<TcMediaClip>(mediaClip);
            _parent = new WeakReference<MediaCompositionView>(parent);
            InitializeComponent();
            ItemType = CompositionItemTypes.Transition;
            
            Loaded += CompositionTransitionView_Loaded;
        }

        private void CompositionTransitionView_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            TransitionsView.TransitionSelectedEvent += TransitionsView_TransitionSelectedEvent;
            UpdateTransitionImage();
            Unloaded += CompositionTransitionView_Unloaded;
        }

        private void CompositionTransitionView_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            TransitionsView.TransitionSelectedEvent -= TransitionsView_TransitionSelectedEvent;
            Unloaded -= CompositionTransitionView_Unloaded;
        }

#if DEBUG
        ~CompositionTransitionView()
        {
            System.Diagnostics.Debug.WriteLine("******************** CompositionTransitionView Destructor********************");
        }
#endif
        #endregion

        #region Callbacks
        private async void TransitionsView_TransitionSelectedEvent(object sender, Transition.Type type)
        {
            var mediaClip = TcMediaClip;
            if(mediaClip != null)
            {
                TransitionFloyout.Hide();
                var parent = Parent;
                if(parent != null)
                {
                    var result = await parent.TryCreateTransition(mediaClip, type);
                    if (result)
                    {
                        UpdateTransitionImage();
                    }
                }
            }
        }

        private void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var mediaClip = TcMediaClip;
            if (mediaClip != null)
            {
                TransitionsView.SelectedIndex = (int)mediaClip.Transition.Kind;
            }
            TransitionFloyout.ShowAt(this);
        }
        #endregion

        #region Private methods
        private void UpdateTransitionImage()
        {
            var shot = TcMediaClip;
            if (shot != null)
            {
                Uri uri = null;
                switch (shot.Transition.Kind)
                {
                    case Transition.Type.None:
                        uri = new Uri("ms-appx:///Assets/Timeline/no_transition.png");
                        break;
                    case Transition.Type.Crossfade:
                        uri = new Uri("ms-appx:///Assets/Timeline/icon_transition_fade.png");
                        break;
                    case Transition.Type.FadeThroughBlack:
                        uri = new Uri("ms-appx:///Assets/Timeline/icon_transition_crossfade_black.png");
                        break;
                    case Transition.Type.FadeThroughWhite:
                        uri = new Uri("ms-appx:///Assets/Timeline/icon_transition_crossfade_white.png");
                        break;
                }
                if (uri != null)
                {
                    TransitionImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(uri);
                }
            }
        }
        #endregion
    }
}
