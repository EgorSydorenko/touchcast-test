﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using System;
using Pitch.DataLayer;
using Pitch.Models;
using Pitch.UILayer.Helpers.Converters;
using Pitch.Helpers;
using Windows.Storage;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Editing.Views
{
    public sealed partial class TouchcastVappMarkView : UserControl
    {
        private Command _vappMarkCommand;

        #region Life cycle

        public double Time => _vappMarkCommand.Time;

        public TouchcastVappMarkView(Command command)
        {
            _vappMarkCommand = command;
            InitializeComponent();
            Loaded += TouchcastVappMarkView_Loaded;
        }

        #endregion

        #region Callbacks

        private async void TouchcastVappMarkView_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            try
            {
                var authoringSession = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
                if (!String.IsNullOrEmpty(_vappMarkCommand.ThumbnailUrl))
                {
                    var thumbUrl = _vappMarkCommand.ThumbnailUrl.Replace("($PERFORMANCE_BASE_URL)/", string.Empty);
                    if (!String.IsNullOrEmpty(thumbUrl) && !String.IsNullOrWhiteSpace(thumbUrl))
                    {
                        var file = await authoringSession.Project.WorkingFolder.TryGetItemAsync(thumbUrl);
                        if (file is StorageFile)
                        {
                            Uri thumbnailUri;
                            if (Uri.TryCreate(file.Path, System.UriKind.RelativeOrAbsolute, out thumbnailUri))
                            {
                                ThumbnailImageBrush.ImageSource = new Windows.UI.Xaml.Media.Imaging.BitmapImage(thumbnailUri);
                            }
                        }
                    }
                }

                Loaded -= TouchcastVappMarkView_Loaded;
                if (!String.IsNullOrEmpty(_vappMarkCommand.Url))
                {
                    var extension = String.Empty;
                    try
                    {
                        extension = System.IO.Path.GetExtension(_vappMarkCommand.Url);
                    }
                    catch
                    {
                        extension = String.Empty;
                    }
                    if (!String.IsNullOrEmpty(extension) && !(_vappMarkCommand.Url.Contains("http://") || _vappMarkCommand.Url.Contains("https://")))
                    {
                        extension = extension.ToLower();
                        if (FileToAppHelper.DocumentExtensions.Contains(extension))
                        {
                            VappMark.Fill = new SolidColorBrush(HexToColorConverter.ConvertHexToRgbColor("#f15d50"));
                        }
                        else if (FileToAppHelper.ImageExtensions.Contains(extension))
                        {
                            VappMark.Fill = new SolidColorBrush(HexToColorConverter.ConvertHexToRgbColor("#9d9d9d"));
                        }
                        else if (FileToAppHelper.VideoExtensions.Contains(extension))
                        {
                            VappMark.Fill = new SolidColorBrush(HexToColorConverter.ConvertHexToRgbColor("#9fc361"));
                        }
                        else if (FileToAppHelper.TextExtensions.Contains(extension))
                        {
                            VappMark.Fill = new SolidColorBrush(HexToColorConverter.ConvertHexToRgbColor("#9f9d50"));
                        }
                    }
                    else
                    {
                        VappMark.Fill = new SolidColorBrush(HexToColorConverter.ConvertHexToRgbColor("#73cdd7"));
                    }
                }
            }
            catch (Exception ex)
            {

            }

            PointerEntered += TouchcastVappMarkView_PointerEntered;
            Unloaded += TouchcastVappMarkView_Unloaded;

            ThumbnailBorder.BorderBrush = VappMark.Fill;
        }

        private void TouchcastVappMarkView_Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            PointerEntered -= TouchcastVappMarkView_PointerEntered;
            Unloaded -= TouchcastVappMarkView_Unloaded;
        }

        private void TouchcastVappMarkView_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {

        }

        #endregion

        #region Private

        private void ShowTHumbnailPopup()
        { }

        private void HideThumbnailPopup()
        { }
        #endregion
    }
}
