﻿using System.Collections.Generic;
using System.Linq;
using Pitch.DataLayer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.IntroPage.Views
{
    public delegate void ThemeSelectionChanged(string name);

    public sealed partial class ThemeSelectorView : UserControl
    {
        #region Events

        public event ThemeSelectionChanged ThemeSelectionChangedEvent;

        #endregion

        #region Fields
        private const double ItemWidth = 300;
        private ThemeManager.Theme.Type _currentType = ThemeManager.Theme.Type.theme;

        #endregion

        #region Properties

        public ThemeManager.Theme.Type CurrentType
        {
            get => _currentType;
            set => _currentType = value;
        }

        public IList<ThemeManager.Theme> CurrrentThemes => ThemeManager.Instance.Themes.Where(t => t.type == CurrentType).ToList();

        #endregion

        #region Life Cycle

        public ThemeSelectorView()
        {
            InitializeComponent();
        }

#if DEBUG
        ~ThemeSelectorView()
        {
            System.Diagnostics.Debug.WriteLine("************ ThemeSelectorView Debug **************");
        }
#endif
        #endregion

        #region Public Methods

        public void ShowThemesByTitle(string searchString, bool isCaseSensetive = false, bool isSearchInDescription = true)
        {
            if (!isCaseSensetive)
            {
                searchString = searchString.ToLower();
            }
            var themes = CurrrentThemes.Where(t => t.type == CurrentType && (isCaseSensetive ? t.name : t.name.ToLower()).Contains(searchString)
                        || (isSearchInDescription ? (isCaseSensetive ? t.description : t.description.ToLower()).Contains(searchString) : true)).ToList();

            UpdateThemeList(themes);
        }
        public void UpdateItemSizes()
        {
            var elementWidth = this.ThemeList.ActualWidth / 4;
            elementWidth = elementWidth - 10;
            if (elementWidth > 0)
            {
                foreach (var item in ThemeList.Items)
                {
                    if (item is ThemePreviewView previewView)
                    {
                        previewView.Width = elementWidth;
                    }
                }
            }
        }

        #endregion

        #region Callbacks

        private void ThemeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ThemeList.SelectedItem is ThemePreviewView item)
            {
                ThemeSelectionChangedEvent?.Invoke(item.ThemeName);
            }
        }

        #endregion

        #region Private Methods

        private void UpdateThemeList(IList<ThemeManager.Theme> themes)
        {
            var viewNames = ThemeList.Items.OfType<ThemePreviewView>().Select(v => v.ThemeName).ToList();

            foreach (var name in viewNames)
            {
                if (!themes.Any(t => name == t.name))
                {
                    var themeView = ThemeList.Items.OfType<ThemePreviewView>().FirstOrDefault(v => v.ThemeName == name);
                    ThemeList.Items.Remove(themeView);
                }
            }

            int index = 0;
            foreach (var theme in themes)
            {
                if (theme.organization == ThemeManager.Theme.Organization.accenture && !TfSdk.ApiClient.Shared.UserInfo.IsAccentureUser)
                {
                    continue;
                }

                if (!viewNames.Any(name => name == theme.name))
                {
                    var themeView = new ThemePreviewView(theme);
                    themeView.Width = ItemWidth;
                    ThemeList.Items.Insert(index, themeView);
                }
                ++index;
            }
            UpdateItemSizes();
        }

        private void ThemeList_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateItemSizes();
        }

        #endregion
    }
}
