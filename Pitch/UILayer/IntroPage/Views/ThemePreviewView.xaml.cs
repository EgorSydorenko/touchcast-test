﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer;
using Windows.Foundation.Metadata;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.IntroPage.Views
{
    public sealed partial class ThemePreviewView : UserControl
    {
        public delegate void ViewSelected(ThemePreviewView sender);

        #region Private fields
        private BitmapImage _bmp;
        private bool _isTitleVisible;
        private ThemeManager.Theme _viewModel;
        private FrameworkElement _element;
        private Brush _brush = new SolidColorBrush(Colors.Transparent);
        private bool _isAutoPlayRequired = false;

        #endregion
        public string ThemeName => _viewModel.name;

        public ThemeManager.Theme ViewModel => _viewModel;

        public bool IsTitleVisible
        {
            get
            {
                return _isTitleVisible;
            }
            set
            {
                _isTitleVisible = value;
                Title.Visibility = _isTitleVisible ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public event ViewSelected ViewSelectedEvent;

        #region Life Cycle
        public ThemePreviewView(ThemeManager.Theme theme, bool isAutoPlayRequired = false)
        {
            InitializeComponent();
            _viewModel = theme;
            _isAutoPlayRequired = isAutoPlayRequired;

            Loaded += ThemePreviewView_Loaded;
            Unloaded += ThemePreviewView_Unloaded;

            var brush = App.Current.Resources["TouchCastSelectedBrush"] as Brush;
            if (brush != null)
            {
                _brush = brush;
            }
        }

#if DEBUG
        ~ThemePreviewView()
        {
            System.Diagnostics.Debug.WriteLine("*********************** ThemePreviewView Destructor *********************");
        }
#endif

        private void ThemePreviewView_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateUI();
            PointerExited += ThemePreviewView_PointerExited;
            PointerEntered += ThemePreviewView_PointerEntered;
            PointerMoved += ThemePreviewView_PointerMoved;
            Tapped += ThemePreviewView_Tapped;
        }

        private void ThemePreviewView_Unloaded(object sender, RoutedEventArgs e)
        {
            PointerEntered -= ThemePreviewView_PointerEntered;
            PointerExited -= ThemePreviewView_PointerExited;
            PointerMoved -= ThemePreviewView_PointerMoved;
            Tapped -= ThemePreviewView_Tapped;
        }
        #endregion

        public async Task UpdateModel(ThemeManager.Theme model)
        {
            _viewModel = model;
            UpdateUI();
        }
        private void ThemePreviewView_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            ThumbnailContainer.BorderBrush = _brush;
            Title.Foreground = _brush;
        }
        private void ThemePreviewView_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            ThumbnailContainer.BorderBrush = new SolidColorBrush(Colors.Transparent);
            Title.Foreground = new SolidColorBrush(Colors.White);
            /*
            if (_isAutoPlayRequired == false && ApiInformation.IsPropertyPresent("Windows.UI.Xaml.Media.Imaging.BitmapImage", nameof(BitmapImage.IsAnimatedBitmap)))
            {
                _bmp.Stop();
            }*/
        }
        private void ThemePreviewView_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            /*
            if (_isAutoPlayRequired == false && ApiInformation.IsPropertyPresent("Windows.UI.Xaml.Media.Imaging.BitmapImage", nameof(BitmapImage.IsAnimatedBitmap)))
            {
                _bmp.Play();
            }*/
        }
        private void ThemePreviewView_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ViewSelectedEvent?.Invoke(this);
        }

        private void UpdateUI()
        {
            ThumbnailContainer.Children.Remove(_element);

            if (_viewModel.thumbnail_file_path.EndsWith(".png") ||
               (_viewModel.thumbnail_file_path.EndsWith(".gif")))
            {
                _bmp = new BitmapImage(new Uri(_viewModel.thumbnail_file_path));
               /* if (ApiInformation.IsPropertyPresent("Windows.UI.Xaml.Media.Imaging.BitmapImage", nameof(BitmapImage.IsAnimatedBitmap)))
                {
                    _bmp.AutoPlay = _isAutoPlayRequired;
                }*/
                _element = new Image();
                (_element as Image).Source = _bmp;
                Canvas.SetZIndex(_element, 7);
            }

            if (_element != null)
            {
                Grid.SetRow(_element, 0);
                ThumbnailContainer.Children.Add(_element);
            }
            Title.Text = _viewModel.name;
        }
    }
}
