﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using TfSdk;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.Services;
using Pitch.UILayer.Authoring.Views;
using Pitch.UILayer.Helpers.Messages;
using Pitch.UILayer.Authoring.Views.Overlays;
using Windows.ApplicationModel.Core;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.IntroPage.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NewThemePage : Page
    {
        private enum DialogType
        {
            None,
            MediaCaptureNoAccess,
            MediaCaptureDeviceLock,
            MediaCaptureFailed
        }

        #region Constants
        private const string NewThemePageMediaCaptureNoAccessTitle = "NewThemePageMediaCaptureNoAccessTitle";
        private const string NewThemePageMediaCaptureNoAccessContent = "NewThemePageMediaCaptureNoAccessContent";
        private const string NewThemePageMediaCaptureNoAccessAllowCameraText = "NewThemePageMediaCaptureNoAccessAllowCameraText";
        private const string NewThemePageMediaCaptureNoAccessAllowMicroText = "NewThemePageMediaCaptureNoAccessAllowMicroText";
        private const string NewThemePageMediaCaptureDeviceLockedTitle = "NewThemePageMediaCaptureDeviceLockedTitle";
        private const string NewThemePageMediaCaptureDiviceLockedContent = "NewThemePageMediaCaptureDiviceLockedContent";
        private const string NewThemePageMediaCaptureFailedTitle = "NewThemePageMediaCaptureFailedTitle";
        private const string NewThemePageMediaCaptureFailedContent = "NewThemePageMediaCaptureFailedContent";
        private const string NewThemePageCloseText = "NewThemePageCloseText";
        private const string NewThemePageOkText = "NewThemePageOkText";


        private readonly string AcademyUrl = @"https://touchcastweb.s3.amazonaws.com/pitch/PitchBasics.mp4";

        private readonly string MediaCaptureNoAccessTitle;
        private readonly string MediaCaptureNoAccessContent;
        private readonly string MediaCaptureNoAccessAllowCameraText;
        private readonly string MediaCaptureNoAccessAllowMicroText;
        private readonly string MediaCaptureDeviceLockedTitle;
        private readonly string MediaCaptureDiviceLockedContent;
        private readonly string MediaCaptureFailedTitle;
        private readonly string MediaCaptureFailedContent;
        private readonly string CloseText;
        private readonly string OkText;
        #endregion

        #region Private Fields

        private DialogType _dialogType = DialogType.None;
        private IEnumerable<ThemeMenuItem> _menuItems;
        private OnboardingVideoOverlay _introOverlay;
        private double _introOverlayScale = 0.25;
        bool _isFileSelectionInPorcess = false;
        #endregion

        #region Properties

        public IEnumerable<ThemeMenuItem> MenuItems => _menuItems;

        #endregion

        #region Life Cycle

        public NewThemePage()
        {
            InitializeMenuItems();
            InitializeComponent();

            ResourceLoader loader = new ResourceLoader();
            MediaCaptureNoAccessTitle = loader.GetString(NewThemePageMediaCaptureNoAccessTitle);
            MediaCaptureNoAccessContent = loader.GetString(NewThemePageMediaCaptureNoAccessContent);
            MediaCaptureNoAccessAllowCameraText = loader.GetString(NewThemePageMediaCaptureNoAccessAllowCameraText);
            MediaCaptureNoAccessAllowMicroText = loader.GetString(NewThemePageMediaCaptureNoAccessAllowMicroText);
            MediaCaptureDeviceLockedTitle = loader.GetString(NewThemePageMediaCaptureDeviceLockedTitle);
            MediaCaptureDiviceLockedContent = loader.GetString(NewThemePageMediaCaptureDiviceLockedContent);
            MediaCaptureFailedTitle = loader.GetString(NewThemePageMediaCaptureFailedTitle);
            MediaCaptureFailedContent = loader.GetString(NewThemePageMediaCaptureFailedContent);
            CloseText = loader.GetString(NewThemePageCloseText);
            OkText = loader.GetString(NewThemePageOkText);

            Cycle.UpdateData(GetUserThemes(ThemeManager.Instance.Themes));

            Loaded += NewThemePage_Loaded;
        }

#if DEBUG
        ~NewThemePage()
        {
            System.Diagnostics.Debug.WriteLine(" * *********** NewThemePage Destructor **************");
        }
#endif
        private async void NewThemePage_Loaded(object sender, RoutedEventArgs e)
        {
            TitleBar.UpdateTitle("TouchCast Pitch");
            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(TitleBar.MovementContainer);
            TitleBar.SetDarkMode();
            TitleBar.SetShowTrainingButtonVisibility(true);
            TitleBar.SignOutPressedEvent += TitleBar_SignOutPressedEvent;
            TitleBar.ShowTrainingPressedEvent += TitleBar_ShowTrainingPressedEvent;
            ThemeSelector.ThemeSelectionChangedEvent += ThemeSelector_ThemeSelectionChangedEvent;
            Cycle.SelectionChangedEvent += Cycle_SelectionChangedEvent;

            MenuListView.SelectedIndex = 0;
            ThemeSelector.UpdateItemSizes();
            Unloaded += NewThemePage_Unloaded;

            if (_dialogType != DialogType.None)
            {
                var dlg = new MessageDialog(String.Empty);
                switch (_dialogType)
                {
                    case DialogType.MediaCaptureNoAccess:
                        dlg.Title = MediaCaptureNoAccessTitle;
                        dlg.Content = MediaCaptureNoAccessContent;
                        dlg.Commands.Add(new UICommand(MediaCaptureNoAccessAllowCameraText) { Id = 1});
                        dlg.Commands.Add(new UICommand(MediaCaptureNoAccessAllowMicroText) { Id = 2 });
                        dlg.Commands.Add(new UICommand(CloseText) { Id = 3 });
                        break;
                    case DialogType.MediaCaptureDeviceLock:
                        dlg.Title = MediaCaptureDeviceLockedTitle;
                        dlg.Content = MediaCaptureDiviceLockedContent;
                        dlg.Commands.Add(new UICommand(OkText) { Id = 3 });
                        break;
                    case DialogType.MediaCaptureFailed:
                        dlg.Title = MediaCaptureFailedTitle;
                        dlg.Content = MediaCaptureFailedContent;
                        dlg.Commands.Add(new UICommand(OkText) { Id = 3 });
                        break;
                    default:
                        break;
                }
                var dialogResult = await dlg.ShowAsync();
                if (dialogResult.Id.Equals(1))
                {
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings:privacy-webcam"));
                }
                else if (dialogResult.Id.Equals(2))
                {
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings:privacy-microphone"));
                }
                _dialogType = DialogType.None;
            }

            Messenger.Default.Register<OpenNewProjectMessage>(this, async m =>
            {
                App.Locator.ProjectFile = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(m.FileTocken);
                NavigateToAuthoring(false);
            });

            ISettingsService settingsService = SimpleIoc.Default.GetInstance<ISettingsService>();
            bool isFirstTimeLaunched = !settingsService.LoadFromSettings<bool>("IsFirstTimeLaunched");
            if (isFirstTimeLaunched == true)
            {
                settingsService.SaveToSettings("IsFirstTimeLaunched", true);
                ShowTrainingOverlay();
            }
        }

        private void NewThemePage_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
            Loaded -= NewThemePage_Loaded;
            Unloaded -= NewThemePage_Unloaded;
            TitleBar.SetShowTrainingButtonVisibility(false);
            TitleBar.SignOutPressedEvent -= TitleBar_SignOutPressedEvent;
            TitleBar.ShowTrainingPressedEvent -= TitleBar_ShowTrainingPressedEvent;
            ThemeSelector.ThemeSelectionChangedEvent -= ThemeSelector_ThemeSelectionChangedEvent;
            Cycle.SelectionChangedEvent -= Cycle_SelectionChangedEvent;
            _introOverlay = null;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null && e.Parameter is TouchcastComposerServiceState)
            {
                var serviceState = (TouchcastComposerServiceState)e.Parameter;
                switch (serviceState)
                {
                    case TouchcastComposerServiceState.DoNotHasAccess:
                        _dialogType = DialogType.MediaCaptureNoAccess;
                        break;
                    case TouchcastComposerServiceState.DeviceLock:
                        _dialogType = DialogType.MediaCaptureDeviceLock;
                        break;
                    case TouchcastComposerServiceState.UnhandledFail:
                    case TouchcastComposerServiceState.NoDevices:
                        _dialogType = DialogType.MediaCaptureFailed;
                        break;
                    default:
                        _dialogType = DialogType.None;
                        break;
                }
            }
            base.OnNavigatedTo(e);
        }

        #endregion

        #region Callbacks

        private async void TitleBar_SignOutPressedEvent()
        {
            ApiClient.Shared.ApiClientStateChangedEvent += Shared_ApiClientChangedEvent;
            await ApiClient.Shared.SignOut();
        }

        private void Shared_ApiClientChangedEvent()
        {
            ApiClient.Shared.ApiClientStateChangedEvent -= Shared_ApiClientChangedEvent;
            if (ApiClient.Shared.Condition == ApiClient.State.NotAuthorized)
            {
                App.Frame.Navigate(typeof(SignInPage), null, new DrillInNavigationTransitionInfo());
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }
        }

        private void ThemeSelector_ThemeSelectionChangedEvent(string name)
        {
            SelectTheme(name);
        }

        private void Cycle_SelectionChangedEvent(ThemePreviewView sender)
        {
            SelectTheme(sender.ThemeName);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ThemeSelector.ShowThemesByTitle(SearchTextBox.Text);
        }

        private async void OpenProjectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!_isFileSelectionInPorcess)
            {
                _isFileSelectionInPorcess = true;
                StorageFile file = await FileToAppHelper.SelectFile(FileToAppHelper.ProjectExtensions.ToArray());
                if (file != null)
                {
                    App.Locator.ProjectFile = file;
                    NavigateToAuthoring(false);
                }
                _isFileSelectionInPorcess = false;
            }
        }

        private void MenuListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MenuListView.SelectedItem is ThemeMenuItem item)
            {
                ThemeSelector.CurrentType = item.ThemeType;
                ThemeSelector.ShowThemesByTitle(SearchTextBox.Text);
            }
        }

        private async void AcademyLinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(App.FabricEndpointData.academy_endpoint))
            {
                await Windows.System.Launcher.LaunchUriAsync(new Uri(App.FabricEndpointData.academy_endpoint));
            }
        }

        private void TitleBar_ShowTrainingPressedEvent()
        {
            ShowTrainingOverlay();
        }

        #endregion

        #region Private Methods

        private void SelectTheme(string themeName)
        {
            ThemeManager.Instance.SelectThemeByName(themeName);
            if (ThemeManager.Instance.SelectedTheme != null && !String.IsNullOrEmpty(ThemeManager.Instance.SelectedTheme.theme_foler_path))
            {
                NavigateToAuthoring(false);
            }
        }

        private void NavigateToAuthoring(bool value)
        {
            App.Locator.RegisterAuthoringSession();
            App.Frame.Navigate(typeof(AuthoringPage), value);
        }

        private void InitializeMenuItems()
        {
            var menuItems = new List<ThemeMenuItem>()
            {
                new ThemeMenuItem("Themes", "ms-appx:///Assets/NewThemePage/themes@2x.png", ThemeManager.Theme.Type.theme, true),
                new ThemeMenuItem("Virtual Sets", "ms-appx:///Assets/NewThemePage/virtual-sets@2x.png", ThemeManager.Theme.Type.virtual_set, true),
            };
            if (ApiClient.Shared.UserInfo.IsAccentureUser)
            {
                menuItems.Add(
                    new ThemeMenuItem("Smart Templates", "ms-appx:///Assets/NewThemePage/smart-templates@3x.png", ThemeManager.Theme.Type.smart_template, false)
                );
            }
            _menuItems = menuItems;
        }

        private List<ThemeManager.Theme> GetUserThemes(List<ThemeManager.Theme> source)
        {
            if (ApiClient.Shared.UserInfo.IsAccentureUser == false)
            {
                return source.Where(t => t.organization != ThemeManager.Theme.Organization.accenture).ToList();
            }
            return source;
        }

        private void InitOverlayPosition(Control overlay)
        {
            Grid.SetRow(overlay, 0);
            Grid.SetRowSpan(overlay, 2);
            Canvas.SetZIndex(overlay, 1000);
        }

        private void ShowTrainingOverlay()
        {
            if (_introOverlay == null)
            {
                _introOverlay = new OnboardingVideoOverlay(LayoutRoot);
                InitOverlayPosition(_introOverlay);
            }
            LayoutRoot.Children.Add(_introOverlay);
        }

        #endregion
    }

    public class ThemeMenuItem
    {
        private string _caption;
        private Uri _iconSource;
        private ThemeManager.Theme.Type _themeType;
        private bool _enabled = true;

        public string Caption => _caption;
        public bool Enabled => _enabled;
        public Uri IconSource => _iconSource;
        public ThemeManager.Theme.Type ThemeType => _themeType;

        public ThemeMenuItem(string caption, string uri, DataLayer.ThemeManager.Theme.Type type, bool enabled)
        {
            _caption = caption;
            _enabled = enabled;
            _themeType = type;
            if (Uri.TryCreate(uri, UriKind.Absolute, out Uri result))
            {
                _iconSource = result;
            }
        }
    }
}