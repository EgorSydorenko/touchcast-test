﻿using System;
using TfSdk;
using TfSdk.UI;
using Pitch.Services;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.IntroPage.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SignInPage : Page
    {
        #region Private Fields

        private const string SignInLabel = "SignInLabelMessage";
        private const string SignInTitlebarTitle = "SignInTitlebarTitle";
        private readonly string SignInLabelText;
        private readonly string SignInTitlebarTitleText;
        private ApiClientConfiguration _configuration;
        private bool _isNeedFastRedirectToAuthoring;
        private bool _isReconected = false;
        private object _reconnectionLock = new object();
        private readonly TimeSpan ReconectPeriod = TimeSpan.FromSeconds(30);
        private readonly DispatcherTimer _reconnectTimer = new DispatcherTimer();
        private SignInControl _signInView;

        private enum StateUI
        {
            Welcome,
            SignIn,
            Login,
            NoConnection
        }

        #endregion

        #region Life Cycle

        public SignInPage()
        {
            InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            SignInLabelText = loader.GetString(SignInLabel);
            SignInTitlebarTitleText = loader.GetString(SignInTitlebarTitle);

            _configuration = App.PrepareConfiguration();

            Loaded += SignInPage_Loaded;
            Unloaded += SignInPage_Unloaded;
        }

#if DEBUG
        ~SignInPage()
        {
            System.Diagnostics.Debug.WriteLine("********************SignInPage Destructor********************");
        }
#endif
        private void SignInPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.ApplicationVersion != null)
            {
                AppVersionTextBlock.Text = $"v{App.ApplicationVersion}";
            }
            TitleBar.SetSignInMode();
            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(TitleBar.MovementContainer);
            TitleBar.BackRequested += TitleBar_BackRequested;

            ApiClient.Shared.ApiClientStateChangedEvent += Shared_ApiClientChangedEvent;

            _reconnectTimer.Interval = ReconectPeriod;
            _reconnectTimer.Tick += ReconnectTimer_Tick;

            ApiClient.Shared.RestoreSavedSession(_configuration);
        }

        private System.Threading.Tasks.Task SignIn_SignInCompletedEvent(string token, string groupUniquePath, string error)
        {
            PrepareProcessingPanel();
            return System.Threading.Tasks.Task.CompletedTask;
        }

        private void SignInPage_Unloaded(object sender, RoutedEventArgs e)
        {
            TitleBar.BackRequested -= TitleBar_BackRequested;
            ApiClient.Shared.ApiClientStateChangedEvent -= Shared_ApiClientChangedEvent;
            RemoveSignInView();
            
            _reconnectTimer.Tick -= ReconnectTimer_Tick;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter != null && e.Parameter is bool)
            {
                _isNeedFastRedirectToAuthoring = (bool)e.Parameter;
            }
        }

        #endregion

        #region Callbacks

        private void ReconnectTimer_Tick(object sender, object e)
        {
            Reconnect();
        }

        private void SignIn_NoInternetConnectionEvent()
        {
            PrepareNoInternetConnectionPanel();
        }

        private void TitleBar_BackRequested()
        {
            PrepareWelcomePanel();
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            _reconnectTimer.Stop();
            PrepareSignInPanel();
        }

        private void NoConnection_TryToConnectEvent()
        {
            Reconnect();
        }

        private async void Shared_ApiClientChangedEvent()
        {  
            switch (ApiClient.Shared.Condition)
            {
                case ApiClient.State.NotAuthorized:
                    {
                        PrepareWelcomePanel();
                    }
                    break;
                case ApiClient.State.Authorizing:
                    {
                        //PrepareProcessingPanel();
                    }
                    break;
                case ApiClient.State.Authorized:
                    {
                        Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticCommandUse);
                        if (!_isNeedFastRedirectToAuthoring)
                        {
                            var frame = Window.Current.Content as Frame;
                            frame.Navigate(typeof(NewThemePage));
                        }
                        else
                        {
                            App.Locator.RegisterAuthoringSession();
                            App.Locator.ComposingMode = TouchcastComposerServiceMode.CameraComposingMode;
                            var frame = Window.Current.Content as Frame;
                            frame.Navigate(typeof(Authoring.Views.AuthoringPage));
                        }
                    }
                    break;
                case ApiClient.State.Failed:
                    {
                        await ApiClient.Shared.SignOut();
                    }
                    break;
            }

        }

        #endregion

        private void PrepareWelcomePanel()
        {
            TitleBar.SetSignInMode();
            TitleBar.BackButtonVisibility = Visibility.Collapsed;

            Label.Text = SignInLabelText;
            ApplyButton.Visibility = Visibility.Visible;
            Spinner.Visibility = Visibility.Collapsed;
            HyperLinks.Visibility = Visibility.Visible;
            Label.Visibility = Visibility.Collapsed;
            RemoveSignInView();
            NoConnection.Visibility = Visibility.Collapsed;
            Welcome.Visibility = Visibility.Visible;
        }

        private void PrepareSignInPanel()
        {
            CreateSignInView();
            _signInView.SignIn();

            TitleBar.SetLoginMode();
            TitleBar.UpdateTitle(SignInTitlebarTitleText);
            TitleBar.BackButtonVisibility = Visibility.Visible;
            NoConnection.Visibility = Visibility.Collapsed;
        }

        private void PrepareNoInternetConnectionPanel()
        {
            if (!_reconnectTimer.IsEnabled)
            {
                _reconnectTimer.Start();
            }
            _isReconected = false;

            TitleBar.SetSignInMode();
            TitleBar.BackButtonVisibility = Visibility.Collapsed;

            NoConnection.Visibility = Visibility.Visible;
            Welcome.Visibility = Visibility.Collapsed;
            RemoveSignInView();
            HyperLinks.Visibility = Visibility.Collapsed;
            Label.Visibility = Visibility.Collapsed; 
        }

        private void PrepareProcessingPanel()
        {
            TitleBar.SetSignInMode();
            TitleBar.BackButtonVisibility = Visibility.Collapsed;
            Label.Visibility = Visibility.Collapsed;
            RemoveSignInView();
            ApplyButton.Visibility = Visibility.Collapsed;
            Spinner.Visibility = Visibility.Visible;
            HyperLinks.Visibility = Visibility.Collapsed;
        }

        private void Reconnect()
        {
            lock (_reconnectionLock)
            {
                if (_isReconected)
                {
                    return;
                }
                if (App.HasConnection())
                {
                    _reconnectTimer.Stop();
                    PrepareSignInPanel();
                    _isReconected = true;
                }
            }
        }

        private void CreateSignInView()
        {
            _signInView = new SignInControl();
            _signInView.UpdateConfiguration(_configuration.LoginEndPointUrl, _configuration.RedirectUrl);
            _signInView.NoInternetConnectionEvent += SignIn_NoInternetConnectionEvent;
            _signInView.SignInCompletedEvent += SignIn_SignInCompletedEvent;
            Grid.SetRow(_signInView, 1);
            MainContainer.Children.Add(_signInView);
        }

        private void RemoveSignInView()
        {
            if (_signInView != null)
            {
                _signInView.NoInternetConnectionEvent -= SignIn_NoInternetConnectionEvent;
                _signInView.SignInCompletedEvent -= SignIn_SignInCompletedEvent;
                MainContainer.Children.Remove(_signInView);
            }
        }
    }
}
