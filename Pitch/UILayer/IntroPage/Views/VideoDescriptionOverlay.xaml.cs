﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.IntroPage.Views
{
    
    public sealed partial class VideoDescriptionOverlay : UserControl
    {
        private PointerEventHandler _handler;

        public delegate void VideoDescriptionOverlayDelegate();
        public event VideoDescriptionOverlayDelegate ClosingEvent;
        private string _url;
        public string Url
        {
            set
            {
                if (_url != value)
                {
                    _url = value;
                    Player.Source = new Uri(_url);
                }
            }
        }

        public VideoDescriptionOverlay()
        {
            this.InitializeComponent();
           
            Player.AutoPlay = true;
            Player.TransportControls.IsCompact = true;
            Loaded += VideoDescriptionOverlay_Loaded;
        }

        private void VideoDescriptionOverlay_Loaded(object sender, RoutedEventArgs e)
        {
            _handler = new PointerEventHandler(CloseButton_PointerReleased);
            CloseButton.AddHandler(PointerReleasedEvent, _handler, true);
        }

        private void CloseButton_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            ClosingEvent?.Invoke();
        }
    }
}
