﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Pitch.DataLayer;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Recording.Views.Toolbars
{
    public sealed partial class RecordToolbarView : UserControl
    {
        #region Private fields
        private WeakReference<RecordingManager> _recordingManager;
        private RecordingManager RecordingManager => _recordingManager?.TryGetObject();
        #endregion

        #region Life cycle
        public RecordToolbarView()
        {
            InitializeComponent();
            Loaded += RecordToolbarView_Loaded;
            Unloaded += RecordToolbarView_Unloaded;
        }
#if DEBUG
        ~RecordToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("******************** RecordToolbarView Destructor********************");
        }
#endif

        private void RecordToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            _recordingManager = new WeakReference<RecordingManager>(SimpleIoc.Default.GetInstance<RecordingManager>());
            Messenger.Default.Register<StartRecordingMessage>(this, msg => UpdateUI(true));
        }

        private void RecordToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
            Loaded -= RecordToolbarView_Loaded;
            Unloaded -= RecordToolbarView_Unloaded;
        }
        #endregion

        #region Callbacks
        private void RecordImage_Tapped(object sender, TappedRoutedEventArgs e)
        {
            e.Handled = true;
            Messenger.Default.Send(new StartCountingMessage());
        }

        private async void PauseImage_Tapped(object sender, TappedRoutedEventArgs e)
        {
            await StopRecording();
            UpdateUI(false);
        }
        #endregion

        #region Private methods
        private async Task StopRecording()
        {
            var manager = RecordingManager;
            if (manager != null)
            {
                await manager.StopRecording();
            }
        }
        #endregion

        #region Public
        public void UpdateUI(bool isRecording)
        {
            RecordImage.Visibility = isRecording ? Visibility.Collapsed : Visibility.Visible;
            PauseImage.Visibility = isRecording ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion
    }
}
