﻿using GalaSoft.MvvmLight.Ioc;
using System;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Recording.Views.Toolbars
{
    public sealed partial class RightToolbarView : UserControl
    {
        #region Private Fields
        private string _sceneLabelText = "Scene ";
        private WeakReference<DataLayer.AuthoringSession> _authoringSession;
        private FrameworkElement _inkToolbar;
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();
        #endregion

        #region Life Cycle
        public RightToolbarView()
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            _authoringSession = new WeakReference<AuthoringSession>(session);
            InitializeComponent();
            Loaded += RightToolbarView_Loaded;
            Unloaded += RightToolbarView_Unloaded;
        }

#if DEBUG
        ~RightToolbarView()
        {
            System.Diagnostics.Debug.WriteLine("******************** RightToolbarView Destructor********************");
        }
#endif

        private void RightToolbarView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Windows.Foundation.Metadata.ApiInformation.IsApiContractPresent("Windows.Foundation.UniversalApiContract", 5))
            {
                DrawPanel.Visibility = Visibility.Visible;
            }

            var session = AuthoringSession;
            if(session != null)
            {
                session.ShotManager.ShotWasHidden += ShotManager_ShotWasHidden;
                session.ShotManager.ShotWasDisplayed += ShotManager_ShotWasDisplayed;
                SceneNumberText.Text = _sceneLabelText + (session.Project.Shots.IndexOf(session.Project.ActiveShot) + 1);
                UpdateInkingPanel(session.Project.ActiveShot.IsInkingTurnedOn);
            }
        }

        private void RightToolbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_inkToolbar != null)
            {
                InkPanelContainer.Children.Remove(_inkToolbar);
            }
            var session = AuthoringSession;
            if (session != null)
            {
                session.ShotManager.ShotWasHidden -= ShotManager_ShotWasHidden;
                session.ShotManager.ShotWasDisplayed -= ShotManager_ShotWasDisplayed;
            }
        }

        #endregion
        public void UpdateInkingPanel(bool isInking)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                _inkToolbar = session.ShotManager.ActiveShotView.TurnOnInkingIfNeeded(isInking);
                if (isInking)
                {
                    if (!InkPanelContainer.Children.Contains(_inkToolbar))
                    {
                        InkPanelContainer.Children.Add(_inkToolbar);
                    }
                }
                else
                {
                    InkPanelContainer.Children.Remove(_inkToolbar);
                }
            }
        }

        #region Callbacks

        private System.Threading.Tasks.Task ShotManager_ShotWasHidden(DataLayer.Shots.Shot model)
        {
            UpdateInkingPanel(false);
            return System.Threading.Tasks.Task.CompletedTask;
        }

        private System.Threading.Tasks.Task ShotManager_ShotWasDisplayed(DataLayer.Shots.Shot model)
        {
            SceneNumberText.Text = _sceneLabelText + (model.Project.Shots.IndexOf(model) + 1);
            UpdateInkingPanel(model.IsInkingTurnedOn);
            return System.Threading.Tasks.Task.CompletedTask;
        }

        private void DrawButton_Click(object sender, RoutedEventArgs e)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.ActiveShot.IsInkingTurnedOn = !session.Project.ActiveShot.IsInkingTurnedOn;
                UpdateInkingPanel(session.Project.ActiveShot.IsInkingTurnedOn);
            }
        }

        private async void DecSceneButton_Click(object sender, RoutedEventArgs e)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                var shot = session.Project.PrevShot();
                if (shot != null)
                {
                    await session.ShotManager.ShowShot(shot);
                }
            }
        }

        private async void IncriseSceneButton_Click(object sender, RoutedEventArgs e)
        {
            var session = AuthoringSession;
            if (session != null)
            {
                var shot = session.Project.NextShot();
                if (shot != null)
                {
                    await session.ShotManager.ShowShot(shot);
                }
            }
        }
        #endregion
    }
}
