﻿using GalaSoft.MvvmLight.Messaging;
using System;
using Pitch.UILayer.Helpers.Messages;
using Pitch.UILayer.Recording.ViewModels.Toolbars;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Recording.Views.Toolbars
{
    public sealed partial class TopToolbarView : UserControl
	{
        #region Private Fields

        private const double MinFontSize = 16;
        private const double MaxFontSize = 61;
        private const double DefaultFontSize = 31;
        private const double DeltaFontSize = 5;
        private TopToolbarViewModel ViewModel => DataContext as TopToolbarViewModel;
		private int _teleprompterScriptSpeedChangingPercentage = 10;
        private double _fontSize = DefaultFontSize;

        #endregion

        #region Life Cycle

        public TopToolbarView()
		{
			InitializeComponent();
			DataContext = new TopToolbarViewModel();
			Loaded += TopToolbarView_Loaded;
			Unloaded += TopToolbarView_Unloaded;
		}

		private void TopToolbarView_Loaded(object sender, RoutedEventArgs e)
		{
            TeleprompterScript.UpdateFontSize(_fontSize);
            TeleprompterScriptSwitch.Toggled += TeleprompterScriptSwitch_Toggled;
            Bindings.Update();
		}

#if DEBUG
        ~TopToolbarView()
		{
			System.Diagnostics.Debug.WriteLine("******************** TopToolbarView Destructor********************");
		}
#endif
		private void TopToolbarView_Unloaded(object sender, RoutedEventArgs e)
		{
			Loaded -= TopToolbarView_Loaded;
			Unloaded -= TopToolbarView_Unloaded;
            TeleprompterScriptSwitch.Toggled -= TeleprompterScriptSwitch_Toggled;
            Bindings.StopTracking();
            ViewModel?.Cleanup();
			DataContext = null;
		}

        #endregion

        #region Callbacks

		private void IncreaseTeleprompterScriptSpeed_Clicked(object sender, RoutedEventArgs e)
		{
			var percentage = _teleprompterScriptSpeedChangingPercentage;
			var currentSpeed = Convert.ToInt32(TeleprompterScriptSpeed.Text.Substring(0,
				TeleprompterScriptSpeed.Text.Length - 1));

			if (currentSpeed < 300)
			{
				TeleprompterScriptSpeed.Text = $"{currentSpeed + percentage}%";
				Messenger.Default.Send(new TeleprompterScriptIncreaseSpeedMessage());
			}
            IncreaseTeleprompterScriptSpeed.IsEnabled = (currentSpeed < 300);
            DecreaseTeleprompterScriptSpeed.IsEnabled = (currentSpeed > 10);
        }

		private void DecreaseTeleprompterScriptSpeed_Clicked(object sender, RoutedEventArgs e)
		{
			var percentage = _teleprompterScriptSpeedChangingPercentage;
			var currentSpeed = Convert.ToInt32(TeleprompterScriptSpeed.Text.Substring(0,
				TeleprompterScriptSpeed.Text.Length - 1));
			if (currentSpeed > 10)
			{
				TeleprompterScriptSpeed.Text = $"{currentSpeed - percentage}%";
				Messenger.Default.Send(new TeleprompterScriptDecreaseSpeedMessage());
			}
            IncreaseTeleprompterScriptSpeed.IsEnabled = (currentSpeed < 300);
            DecreaseTeleprompterScriptSpeed.IsEnabled = (currentSpeed > 10);
        }

        private void NewTakeButton_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
	        Messenger.Default.Send(new NewTakeInitializedMessage());
			ViewModel.NewTakeCommand.Execute(null);
            e.Handled = true;
        }

        private void DecriseFontSizeButton_Click(object sender, RoutedEventArgs e)
        {
            _fontSize -= DeltaFontSize;
            if (_fontSize < MinFontSize)
            {
                _fontSize = MinFontSize;
            }

            TeleprompterScript.UpdateFontSize(_fontSize);
            DecriseFontSizeButton.IsEnabled = (_fontSize > MinFontSize);
            IncriseFontSizeButton.IsEnabled = (_fontSize < MaxFontSize);
        }

        private void IncriseFontSizeButton_Click(object sender, RoutedEventArgs e)
        {
            _fontSize += DeltaFontSize;
            if (_fontSize > MaxFontSize)
            {
                _fontSize = MaxFontSize;
            }
            TeleprompterScript.UpdateFontSize(_fontSize);
            IncriseFontSizeButton.IsEnabled = (_fontSize < MaxFontSize);
            DecriseFontSizeButton.IsEnabled = (_fontSize > MinFontSize);
        }

        private void TeleprompterScriptSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            if (ViewModel.IsUpdatingInProcess)
            {
                return;
            }
            ViewModel.SetShotTeleprompterState(TeleprompterScriptSwitch.IsOn);
        }

        #endregion
    }
}
