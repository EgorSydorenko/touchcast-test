﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Pitch.DataLayer;
using Pitch.Helpers;
using Pitch.Services;
using Pitch.UILayer.Helpers.Messages;
using Pitch.UILayer.Recording.ViewModels.Toolbars;
using System.Threading.Tasks;
using Pitch.DataLayer.Helpers.Extensions;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Recording.Views.Toolbars
{
    public sealed partial class TeleprompterScript : UserControl
	{
        #region Private Fields
        private TopToolbarViewModel ViewModel => DataContext as TopToolbarViewModel;
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private WeakReference<AuthoringSession> _authoringSession;

        private IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();
        private ScrollViewerAnimator _scrollViewerAnimator;
		private int _speedChangingPercentage = 10;
		#endregion

        #region Life Cycle
        public TeleprompterScript()
		{
			InitializeComponent();
			_authoringSession = new WeakReference<AuthoringSession>(SimpleIoc.Default.GetInstance<AuthoringSession>());
			_mediaMixerService = new WeakReference<IMediaMixerService>(SimpleIoc.Default.GetInstance<IMediaMixerService>());
			Loaded += TeleprompterScript_Loaded;
        }

#if DEBUG
        ~TeleprompterScript()
        {
            System.Diagnostics.Debug.WriteLine("*************** TeleprompterScript Destructor **************");
        }
#endif

        private void TeleprompterScript_Loaded(object sender, RoutedEventArgs e)
		{
            _scrollViewerAnimator = new ScrollViewerAnimator(TeleprompterScriptScrollViewer);

            Visibility = Visibility.Collapsed;
			Opacity = 1.0;

			Messenger.Default.Register<StartRecordingMessage>(this, message => {
                _scrollViewerAnimator.Start();
            });
			Messenger.Default.Register<NewTakeInitializedMessage>(this, message =>
			{
				_scrollViewerAnimator.Pause();
				_scrollViewerAnimator.Reset();

                if (MediaMixer?.State == TouchcastComposerServiceState.Recording)
                {
                    _scrollViewerAnimator.Restart();
                }
			});
			Messenger.Default.Register<TeleprompterScriptIncreaseSpeedMessage>(this,
				message => _scrollViewerAnimator.IncreaseSpeedByPercentage(_speedChangingPercentage));
			Messenger.Default.Register<TeleprompterScriptDecreaseSpeedMessage>(this,
				message => _scrollViewerAnimator.DecreaseSpeedByPercentage(_speedChangingPercentage));

			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.Project.ActiveShotWillBeChanged += Project_ActiveShotWillBeChanged;
                authoringSession.ShotManager.ShotWasDisplayed += ShotManager_ShotWasDisplayed;
            }

			var mediaMixer = MediaMixer;
            if (mediaMixer != null)
            {
                mediaMixer.StateChangedEvent += MediaMixer_StateChangedEvent;
            }

			Unloaded += TeleprompterScript_Unloaded;
        }

        private void TeleprompterScript_Unloaded(object sender, RoutedEventArgs e)
		{
            _scrollViewerAnimator.Stop();
            _scrollViewerAnimator = null;
            
            Messenger.Default.Unregister(this);

			var authoringSession = AuthoringSession;
			if (authoringSession != null)
			{
				authoringSession.Project.ActiveShotWillBeChanged -= Project_ActiveShotWillBeChanged;
                authoringSession.ShotManager.ShotWasDisplayed -= ShotManager_ShotWasDisplayed;
            }

			var mediaMixer = MediaMixer;
			if (mediaMixer != null)
				MediaMixer.StateChangedEvent -= MediaMixer_StateChangedEvent;

			Unloaded -= TeleprompterScript_Unloaded;
        }

        #endregion

        #region Callbacks

        private void Project_ActiveShotWillBeChanged()
		{
			_scrollViewerAnimator.Pause();
			_scrollViewerAnimator.Reset();
        }

        private Task ShotManager_ShotWasDisplayed(DataLayer.Shots.Shot model)
        {
            var mediaMixer = MediaMixer;
            if (mediaMixer?.State == TouchcastComposerServiceState.Recording)
            {
                _scrollViewerAnimator.Restart();
            }

            return Task.CompletedTask;
        }

        private void MediaMixer_StateChangedEvent(object sender)
		{
			var mediaMixer = MediaMixer;
            if (mediaMixer?.State == TouchcastComposerServiceState.Completed)
            {
                _scrollViewerAnimator.Pause();
            }
            if (mediaMixer?.State == TouchcastComposerServiceState.Recording)
            {
                _scrollViewerAnimator.Start();
            }
        }

        private void TeleprompterScriptScrollViewer_PointerWheelChanged(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (sender is ScrollViewer)
            {
                var mouseWheelDelta = e.GetCurrentPoint(null).Properties.MouseWheelDelta;
                _scrollViewerAnimator.SeekBy(-mouseWheelDelta);

                e.Handled = true;
            }
        }

        public void UpdateFontSize(double fontSize)
        {
            TeleprompterScriptText.FontSize = fontSize;
        }

        #endregion
    }
}
