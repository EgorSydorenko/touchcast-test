﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Pitch.Helpers.Extensions;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Pitch.DataLayer;
using Pitch.UILayer.Authoring.Views;
using Pitch.UILayer.Authoring.Views.Overlays;
using Pitch.UILayer.Helpers.Messages;
using Pitch.UILayer.Recording.Views.Overlays;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using System.Linq;
using Pitch.Helpers.Logger;
using Pitch.Services;
using Pitch.UILayer.Editing.Views;
using Pitch.Helpers;
using Windows.UI.Popups;
using Windows.ApplicationModel.Resources;
using Pitch.DataLayer.Helpers.Extensions;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Pitch.UILayer.Recording.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RecordingScreenPage : Page
    {
        #region Constants

        private const double MsDuration = 150;
        private const int VappMoveStep = 10;

        private const string PowerSupplyTitle = "PowerSupplyPopupTitle";
        private const string PowerSupplyMessage = "PowerSupplyPopupBody";
        #endregion

        #region Private Fields

        private readonly Duration _duration = new Duration(TimeSpan.FromMilliseconds(MsDuration));
        private DoubleAnimation _xPositionAnimation = new DoubleAnimation();
        private Storyboard _sidebarAnimation = new Storyboard();
        private WeakReference<AuthoringSession> _authoringSession;
        private WeakReference<RecordingManager> _recordingManager;
        private TappedEventHandler _tappedEventHandler;
        private PointerEventHandler _pointerHandler;
        private BaseLayoutView _selectedView;
        private bool _isStartOnLoad = false;

        #region Popups
        private BusyIndicatorOverlay _busyOverlay;
        private CountdownOverlay _countdownOverlay;
        private MessageDialog _powerSupplyPopup;
        #endregion

        #endregion

        #region Properties

        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        private RecordingManager RecordingManager => _recordingManager?.TryGetObject();

        #endregion

        #region Life Cycle

        public RecordingScreenPage()
        {
            App.Locator.AppState = ApplicationState.Recording;
            InitializeComponent();
            _authoringSession = new WeakReference<AuthoringSession>(SimpleIoc.Default.GetInstance<AuthoringSession>());
            _recordingManager = new WeakReference<RecordingManager>(SimpleIoc.Default.GetInstance<RecordingManager>());

            Loaded += RecordingScreen_Loaded;
            Unloaded += RecordingScreen_Unloaded;

            Storyboard.SetTarget(_xPositionAnimation, Sidebar);
            Storyboard.SetTargetProperty(_xPositionAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateX)");
            _xPositionAnimation.Duration = _duration;
            _xPositionAnimation.EnableDependentAnimation = false;
            _sidebarAnimation.Children.Add(_xPositionAnimation);
            _sidebarAnimation.FillBehavior = FillBehavior.HoldEnd;

            InitCountdownOverlay();
            var loader = new ResourceLoader();
            var message = loader.GetString(PowerSupplyMessage);
            var title = loader.GetString(PowerSupplyTitle);
            _powerSupplyPopup = new MessageDialog(message, title);
            _powerSupplyPopup.Commands.Add(new UICommand("OK"));
        }

#if DEBUG
        ~RecordingScreenPage()
        {
            System.Diagnostics.Debug.WriteLine("******************** RecordingScreenPage Destructor********************");
        }
#endif

        private async void RecordingScreen_Loaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("RecordingScreen_Loaded");

            if (Windows.Foundation.Metadata.ApiInformation.IsApiContractPresent("Windows.Foundation.UniversalApiContract", 5))
            {
                _pointerHandler = new PointerEventHandler(Page_PointerEntered);
                AddHandler(PointerEnteredEvent, _pointerHandler, true);
            }

            Window.Current.VisibilityChanged += Current_VisibilityChanged;
            _busyOverlay = new BusyIndicatorOverlay();
            InitOverlayPosition(_busyOverlay);

            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(TitleBar.MovementContainer);
            
            var session = AuthoringSession;
            if (session != null)
            {
                TitleBar.UpdateModeSwitcherState(true, session.Project.TcMediaComposition.MediaClips.Any(i => i.Takes.Any()));

                session.ShotManager.ShotContainerView = Container;
                await session.ShotManager.Initialize();
                var title = session.Project.Name;
                if (!String.IsNullOrEmpty(title))
                {
                    title = " - " + title;
                }
                title = String.Format("TouchCast Pitch{0}", title);
                TitleBar.UpdateTitle(title);

                session.Project.ActiveShotWillBeChanged += Project_ActiveShotWillBeChanged;
                session.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent += ActiveShotView_LayoutViewSelectedEvent;
                session.Project.TcMediaComposition.MediaClipWasCreated += TcMediaComposition_MediaClipWasCreated;
                session.UpdateGreenScreenSettingsForActiveShot();
            }

            TitleBar.BackRequested += TitleBar_BackRequested;
            TitleBar.ChangeModeEvent += TitleBar_ChangeModeEvent;

            Sidebar.CheckReadyShots();
            Sidebar.ShowSideBarEvent += Sidebar_ShowSideBarEvent;
            Subscribe();

            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveDownShortcutEvent += RecordingShortcutManager_VappMoveDownShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveLeftShortcutEvent += RecordingShortcutManager_VappMoveLeftShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveRightShortcutEvent += RecordingShortcutManager_VappMoveRightShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveUpShortcutEvent += RecordingShortcutManager_VappMoveUpShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveDownShortcutEvent += RecordingShortcutManager_MoveDownShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveLeftShortcutEvent += RecordingShortcutManager_MoveLeftShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveRightShortcutEvent += RecordingShortcutManager_MoveRightShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveUpShortcutEvent += RecordingShortcutManager_MoveUpShortcutEvent;

            var recordingManager = RecordingManager;
            if (recordingManager != null)
            {
                await recordingManager.StartPreview();
                var mixer = recordingManager.MediaMixer;
                if(mixer != null)
                {
                    mixer.StateChangedEvent += Mixer_StateChangedEvent;
                }
            }

            await CheckBatteryChargingStatus();
            PowerManagerHelper.StartObserve();

            if (_isStartOnLoad)
            {
                await ShowCoundownOverlay();
            }
        }

        private void RecordingScreen_Unloaded(object sender, RoutedEventArgs e)
        {
            LogQueue.WriteToFile("RecordingScreen_Unloaded");
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveDownShortcutEvent -= RecordingShortcutManager_VappMoveDownShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveLeftShortcutEvent -= RecordingShortcutManager_VappMoveLeftShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveRightShortcutEvent -= RecordingShortcutManager_VappMoveRightShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.VappMoveUpShortcutEvent -= RecordingShortcutManager_VappMoveUpShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveDownShortcutEvent -= RecordingShortcutManager_MoveDownShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveLeftShortcutEvent -= RecordingShortcutManager_MoveLeftShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveRightShortcutEvent -= RecordingShortcutManager_MoveRightShortcutEvent;
            Shortcut.Manager.Instance.RecordingShortcutManager.MoveUpShortcutEvent -= RecordingShortcutManager_MoveUpShortcutEvent;

            var recordingManager = RecordingManager;
            if (recordingManager != null)
            {
                var mixer = recordingManager.MediaMixer;
                if (mixer != null)
                {
                    mixer.StateChangedEvent -= Mixer_StateChangedEvent;
                }
            }
                
            var session = AuthoringSession;
            if (session != null)
            {
                session.Project.ActiveShotWillBeChanged -= Project_ActiveShotWillBeChanged;
                session.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent -= ActiveShotView_LayoutViewSelectedEvent;
                session.Project.TcMediaComposition.MediaClipWasCreated -= TcMediaComposition_MediaClipWasCreated;
            }

            PowerManagerHelper.StopObserve();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Shortcut.Manager.Instance.Activate(Shortcut.ShortcutsManagerId.Recording);
            _isStartOnLoad = (e.Parameter is bool && (bool)e.Parameter == true);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            TitleBar.Unsubscribe();

            base.OnNavigatingFrom(e);
            Messenger.Default.Unregister(this);

            var session = AuthoringSession;
            if (session != null)
            {
                foreach (var item in session.ShotManager.ShotViews)
                {
                    item.CleanLayoutLayerContainer();
                }
            }
            if (_countdownOverlay != null)
            {
                StopCountdown();
                _countdownOverlay.CountdownComplete -= CountdownOverlay_CountdownComplete;
            }
            RemoveHandler(TappedEvent, _tappedEventHandler);
            if (_pointerHandler != null)
            {
                RemoveHandler(PointerEnteredEvent, _pointerHandler);
            }
            Window.Current.VisibilityChanged -= Current_VisibilityChanged;
            Sidebar.ShowSideBarEvent -= Sidebar_ShowSideBarEvent;
            TitleBar.BackRequested -= TitleBar_BackRequested;
            TitleBar.ChangeModeEvent -= TitleBar_ChangeModeEvent;
            RecordingBackgroundView.Unsubscribe();
            Container.Children.Clear();
            App.Locator.UnregisterRecordingManager();
        }

        #endregion

        #region Callbacks

        private void Mixer_StateChangedEvent(object sender)
        {
            if (sender is IMediaMixerService)
            {
                var mixer = sender as IMediaMixerService;
                if (mixer.State == TouchcastComposerServiceState.DoNotHasAccess
                    || mixer.State == TouchcastComposerServiceState.DeviceLock
                    || mixer.State == TouchcastComposerServiceState.UnhandledFail)
                {
                    App.Locator.UnRegisterAuthoringSession();
                    App.Frame.Navigate(typeof(IntroPage.Views.NewThemePage), mixer.State);
                    App.Frame.BackStack.Clear();
                    App.Frame.ForwardStack.Clear();
                    App.Frame.CacheSize = 0;
                }
            }
        }

        private void Project_ActiveShotWillBeChanged()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent -= ActiveShotView_LayoutViewSelectedEvent;
            }
        }

        private void Project_ActiveShotWasChanged()
        {
            var session = AuthoringSession;
            if (session != null)
            {
                session.ShotManager.ActiveShotView.LayoutViewSelectedEvent += ActiveShotView_LayoutViewSelectedEvent;
                _selectedView = session.ShotManager.ActiveShotView.LayoutViews.FirstOrDefault(v => v.LayoutViewState != Authoring.Views.LayoutViews.LayoutViewState.NormalState);
            }
        }

        private Task<bool> RecordingShortcutManager_VappMoveUpShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, -VappMoveStep);
            return Task.FromResult(true);
        }

        private Task<bool> RecordingShortcutManager_VappMoveRightShortcutEvent()
        {
            _selectedView?.ChangePositionBy(VappMoveStep, 0);
            return Task.FromResult(true);
        }

        private Task<bool> RecordingShortcutManager_VappMoveLeftShortcutEvent()
        {
            _selectedView?.ChangePositionBy(-VappMoveStep, 0);
            return Task.FromResult(true);
        }

        private Task<bool> RecordingShortcutManager_VappMoveDownShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, VappMoveStep);
            return Task.FromResult(true);
        }
        private Task<bool> RecordingShortcutManager_MoveUpShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, -1);
            return Task.FromResult(true);
        }

        private Task<bool> RecordingShortcutManager_MoveRightShortcutEvent()
        {
            _selectedView?.ChangePositionBy(1, 0);
            return Task.FromResult(true);
        }

        private Task<bool> RecordingShortcutManager_MoveLeftShortcutEvent()
        {
            _selectedView?.ChangePositionBy(-1, 0);
            return Task.FromResult(true);
        }

        private Task<bool> RecordingShortcutManager_MoveDownShortcutEvent()
        {
            _selectedView?.ChangePositionBy(0, 1);
            return Task.FromResult(true);
        }

        private void ActiveShotView_LayoutViewSelectedEvent(BaseLayoutView view)
        {
            _selectedView = view;
        }

        private async Task TcMediaComposition_MediaClipWasCreated(TcMediaClip sender)
        {
            TitleBar.UpdateModeSwitcherState(true, true);

            await Task.CompletedTask;
        }

        private async void Current_VisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            StopCountdown();
            var recordingManager = RecordingManager;
            if (recordingManager == null) return;
            await recordingManager.ProcessWindowVisibilityChanging(e.Visible);
            if (e.Visible)
            {
                RecordingToolbar.UpdateUI(false);
            }
        }

        private async void TitleBar_ChangeModeEvent(ApplicationState futureState)
        {
            if (futureState == ApplicationState.Preparation)
            {
                var manager = RecordingManager;
                if (manager != null)
                {
                    await manager.StopRecording(false);
                }
                SwitchToAuthoringPage();
            }
            else if (futureState == ApplicationState.Editing)
            {
                var manager = RecordingManager;
                if (manager != null)
                {
                    await manager.StopRecording(false);
                }

                var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
                if (mixer != null)
                {
                    await mixer.Reset();
                    TouchCastComposingEngine.ComposingContext.Shared().ResetContext();
                }
                App.Frame.Navigate(typeof(EditingPage));
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }
        }

        private async void TitleBar_BackRequested()
        {
            var manager = RecordingManager;
            if (manager != null)
            {
                await manager.StopRecording();
            }
            SwitchToAuthoringPage();
        }

        private async Task Sidebar_ShowSideBarEvent(double from, double to)
        {
            _xPositionAnimation.From = from;
            _xPositionAnimation.To = to;

            await _sidebarAnimation.BeginAsync();
        }

        #region Counterdown callbacks

        private async void CountdownOverlay_CountdownComplete(object sender, RoutedEventArgs e)
        {
            LayoutRoot.Children.Remove(_countdownOverlay);
            Messenger.Default.Send(new StartRecordingMessage());
            await StartRecording();
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (e.Handled) return;

            StopCountdown();
        }

        private void Page_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Pen)
            {
                var session = AuthoringSession;
                if (session != null && !session.Project.ActiveShot.IsInkingTurnedOn)
                {
                    session.Project.ActiveShot.IsInkingTurnedOn = true;
                    RightToolbar.UpdateInkingPanel(true);
                }
            }
        }
        #endregion

        #endregion

        #region Private Methods

        private void InitCountdownOverlay()
        {
            _countdownOverlay = new CountdownOverlay();
            InitOverlayPosition(_countdownOverlay);
            _countdownOverlay.CountdownComplete += CountdownOverlay_CountdownComplete;
            _tappedEventHandler = new TappedEventHandler(LayoutRoot_Tapped);
        }

        private void InitOverlayPosition(Control overlay)
        {
            Grid.SetRow(overlay, 0);
            Grid.SetRowSpan(overlay, 4);
            Canvas.SetZIndex(overlay, 1000);
        }

        private void SwitchToAuthoringPage()
        {
            App.Frame.Navigate(typeof(AuthoringPage));
            App.Frame.BackStack.Clear();
            App.Frame.ForwardStack.Clear();
            App.Frame.CacheSize = 0;
        }

        private async Task ShowCoundownOverlay()
        {
            LayoutRoot.Children.Add(_countdownOverlay);
            AddHandler(TappedEvent, _tappedEventHandler, true);
            await _countdownOverlay.StartCountdownAsync(3);
            RemoveHandler(TappedEvent, _tappedEventHandler);
            LayoutRoot?.Children?.Remove(_countdownOverlay);
        }

        private void StopCountdown()
        {
            _countdownOverlay.IsCountingDown = false;
        }

        private void Subscribe()
        {
            Messenger.Default.Register<BusyIndicatorShowMessage>(this, (m) =>
            {
                LayoutRoot.Children.Add(_busyOverlay);
            });

            Messenger.Default.Register<BusyIndicatorHideMessage>(this, (m) =>
            {
                LayoutRoot.Children.Remove(_busyOverlay);
            });

            Messenger.Default.Register<StartCountingMessage>(this, async (m) =>
            {
                await ShowCoundownOverlay();
            });

            Messenger.Default.Register<ShowBatteryStatusPopup>(this, async (m) =>
            {
                await ShowPowerSupplyPopup();
            });
        }

        private async Task StartRecording()
        {
            var manager = RecordingManager;
            if (manager != null)
            {
                await manager.StartRecording();
            }
        }

        private async Task CheckBatteryChargingStatus()
        {
            if (PowerManagerHelper.GetPowerSupplyStatus == Windows.System.Power.PowerSupplyStatus.NotPresent)
            {
                await ShowPowerSupplyPopup();
            }
        }

        private async Task ShowPowerSupplyPopup()
        {
            if (_powerSupplyPopup != null)
            {
                await Dispatcher.TryRunAsync(CoreDispatcherPriority.Normal, async () => await _powerSupplyPopup.ShowAsync());
            }
        }

        #endregion
    }
}
