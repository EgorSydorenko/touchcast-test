﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Pitch.UILayer.Recording.ViewModels;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Recording.Views
{
    public sealed partial class StatusbarView : UserControl
    {
        private StatusbarViewModel ViewModel => DataContext as StatusbarViewModel;
        public StatusbarView()
        {
            InitializeComponent();
            DataContext = new StatusbarViewModel();
            Loaded += StatusbarView_Loaded;
            Unloaded += StatusbarView_Unloaded;
        }

#if DEBUG
        ~StatusbarView()
        {
            System.Diagnostics.Debug.WriteLine("******************** StatusbarView Destructor********************");
        }
#endif
        private void StatusbarView_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void StatusbarView_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= StatusbarView_Loaded;
            Unloaded -= StatusbarView_Unloaded;
            ViewModel.Cleanup();
            Bindings.StopTracking();
            DataContext = null;
        }

    }
}
