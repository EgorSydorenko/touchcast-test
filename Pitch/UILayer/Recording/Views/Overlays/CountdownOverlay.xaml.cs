﻿using System;
using System.Threading.Tasks;
using Pitch.Helpers.Extensions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Pitch.UILayer.Recording.Views.Overlays
{
    public sealed partial class CountdownOverlay : UserControl
    {
        public event RoutedEventHandler CountdownComplete;

        private bool _isCountingDown;
        private Storyboard _storyboard;
        private readonly TimeSpan _duration = TimeSpan.FromSeconds(1d);
        public bool IsCountingDown
        {
            get
            {
                return _isCountingDown;
            }
            set
            {
                _isCountingDown = value;
                if (!_isCountingDown)
                {
                    _storyboard?.Seek(_duration);
                    Seconds = 0;
                }
            }
        }
        public int Seconds
        {
            get;
            set;
        }

        #region Life Cycle
        public CountdownOverlay()
        {
            InitializeComponent();
        }
#if DEBUG
        ~CountdownOverlay()
        {
            System.Diagnostics.Debug.WriteLine("******************** CountdownOverlay Destructor********************");
        }
#endif
        #endregion

        public async Task StartCountdownAsync(int seconds)
        {
            _isCountingDown = true;
            /*GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<CancelCountingMessage>(this,
                (m) =>
                {
                    IsCountingDown = false;
                }
            );*/
            Seconds = seconds;

            while (Seconds > 0 && _isCountingDown)
            {
                _storyboard = new Storyboard();
                var da = new DoubleAnimation
                {
                    From = 1d,
                    To = 0,
                    Duration = new Duration(_duration),
                    EnableDependentAnimation = true
                };

                _storyboard.Children.Add(da);
                _storyboard.RepeatBehavior = new RepeatBehavior(1);
                Storyboard.SetTargetProperty(da, "Opacity");
                Storyboard.SetTarget(_storyboard, Counter);

                Counter.Text = Seconds.ToString();

                await _storyboard.BeginAsync();

                Seconds--;
            }

            if (_isCountingDown)
            {
                Counter.Text = Seconds.ToString();
                CountdownComplete?.Invoke(this, new RoutedEventArgs());
            }

            _isCountingDown = false;
            //GalaSoft.MvvmLight.Messaging.Messenger.Default.Unregister(this);
        }
    }
}
