﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using Windows.UI.Xaml;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.UILayer.Recording.ViewModels
{
    public class StatusbarViewModel : ViewModelBase
    {
        private WeakReference<ComposingManager> _composingManager;
        private DispatcherTimer _timer;
        private string _recordTimer;
        private ComposingManager ComposingManager => _composingManager?.TryGetObject();

        public string RecordTime
        {
            get
            {
                return _recordTimer;
            }
            set
            {
                Set(ref _recordTimer, value);
            }
        }

        public StatusbarViewModel()
        {
            _composingManager = new WeakReference<ComposingManager>(SimpleIoc.Default.GetInstance<ComposingManager>());
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(250);
            _timer.Tick += Timer_Tick;
            _timer.Start();
        }

#if DEBUG
        ~StatusbarViewModel()
        {
            System.Diagnostics.Debug.WriteLine("********************StatusbarViewModel Destructor********************");
        }
#endif

        public override void Cleanup()
        {
            _timer.Stop();
            base.Cleanup();
        }
        private void Timer_Tick(object sender, object e)
        {
            var result = TimeSpan.Zero;
            var composingManager = ComposingManager;
            if (composingManager != null)
            {
                result = TimeSpan.FromSeconds(composingManager.ElapsedSeconds);
            }
            RecordTime = result.ToString(@"hh\:mm\:ss");
        }

    }
}
