﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Pitch.DataLayer;
using Pitch.DataLayer.Shots;
using Pitch.Services;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.UILayer.Editing.Views;

namespace Pitch.UILayer.Recording.ViewModels.Toolbars
{
    public class TopToolbarViewModel : ViewModelBase
    {
        #region Private Fields

        private WeakReference<AuthoringSession> _authoringSession;
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private WeakReference<RecordingManager> _recordingManager;
        private int _takesCounter;
        private bool _isFinishEnabled;
        private string _teleprompterText;
        private bool _isTeleprompterVisible;
        private bool _hasTelepromterText;
        private bool _isTeleprompterTextShow;
        private bool _isFontButtonEnabled;

        #endregion

        #region Private Properties

        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        private IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();

        private RecordingManager RecordingManager => _recordingManager?.TryGetObject();

        #endregion

        #region Public Properties

        public string TeleprompterText
	    {
		    get => _teleprompterText;
		    set => Set(ref _teleprompterText, value);
	    }

	    public bool IsTeleprompterVisible
        {
            get
            {
                return _isTeleprompterVisible;
            }
            set
            {
                Set(ref _isTeleprompterVisible, value);
            }
        }

        public bool HasTelepromterText
        {
            get { return _hasTelepromterText; }
            set { Set(ref _hasTelepromterText, value); }
        }

        public bool IsFontButtonEnabled
        {
            get { return _isFontButtonEnabled; }
            set { Set(ref _isFontButtonEnabled, value); }
        }

        public bool IsTeleprompterTextShow
        {
            get { return _isTeleprompterTextShow; }
            set
            {
                Set(ref _isTeleprompterTextShow, value);
                IsTeleprompterVisible = value;
            }
        }

        public int TakesCounter
        {
            get
            {
                return _takesCounter;
            }
            set
            {
                if (value == 0) value = 1;
                Set(ref _takesCounter, value);
            }
        }

        public bool IsFinishEnabled
        {
            get
            {
                return _isFinishEnabled;
            }
            set
            {
                Set(ref _isFinishEnabled, value);
            }
        }

        public bool IsUpdatingInProcess { get; set; }

        #endregion

        #region Commands

        public ICommand NewTakeCommand { get; private set; }

        public ICommand FinishCommand { get; private set; }

        #endregion

        #region Life Cycle

        public TopToolbarViewModel()
        {
            var recordingManager = SimpleIoc.Default.GetInstance<RecordingManager>();
            if (recordingManager != null)
            {
                _recordingManager = new WeakReference<RecordingManager>(recordingManager);
            }
            
            var authoringSession = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (authoringSession.Project.TcMediaComposition.MediaClipToProcess != null && !authoringSession.Project.TcMediaComposition.IsRecordMoreAtEnd)
            {
                _takesCounter = authoringSession.Project.TcMediaComposition.MediaClipToProcess.Takes.Count();
            }
            _takesCounter++;
            _authoringSession = new WeakReference<AuthoringSession>(authoringSession);
            var mediaMixerService = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            mediaMixerService.StateChangedEvent += MediaMixerService_StateChangedEvent;
            _mediaMixerService = new WeakReference<IMediaMixerService>(mediaMixerService);

            authoringSession.Project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
            authoringSession.Project.TcMediaComposition.MediaClipWasCreated += TcMediaComposition_MediaClipWasCreated;
            
            UpdateTelepromterValues();
            NewTakeCommand = new RelayCommand(() =>
            {
                try
                {
                    NewTake();
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
            FinishCommand = new RelayCommand(async () => 
            {
                try
                {
                    await Finish(true);
                }
                catch (System.Exception ex)
                {
                    Pitch.Helpers.Logger.LogQueue.WriteToFile(ex.Message);
                }
            });
            IsFinishEnabled = authoringSession.Project.TcMediaComposition.MediaClips.Any(s => s.Takes.Any(j => j.VideoFragments.Any()));
        }

#if DEBUG
        ~TopToolbarViewModel()
        {
            System.Diagnostics.Debug.WriteLine("******************** TopToolbarViewModel Destructor********************");
        }
#endif

        public override void Cleanup()
        {
            NewTakeCommand = null;
	        TeleprompterText = String.Empty;

            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.Project.ActiveShotWasChanged -= Project_ActiveShotWasChanged;
                authoringSession.Project.TcMediaComposition.MediaClipWasCreated -= TcMediaComposition_MediaClipWasCreated;
            }
            var mediaMixerService = MediaMixer;
            if (mediaMixerService != null)
            {
                mediaMixerService.StateChangedEvent -= MediaMixerService_StateChangedEvent;
            }
            base.Cleanup();
        }

        #endregion

        #region Public Methods

        public void SetShotTeleprompterState(bool state)
        {
            AuthoringSession authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.Project.ActiveShot.IsShowTeleprompterInRecordMode = state;
            }
        }

        #endregion

        #region Callbacks

        private void Project_ActiveShotWasChanged()
        {
            IsUpdatingInProcess = true;
            try
            { 
                AuthoringSession authoringSession = AuthoringSession;
                if (authoringSession != null)
                {
                    UpdateTelepromterValues();
                }
            }
            finally
            {
                IsUpdatingInProcess = false;
            }
        }

        private Task TcMediaComposition_MediaClipWasCreated(TcMediaClip sender)
        {
            IsFinishEnabled = true;
            TakesCounter = sender.Takes.Count;

            return Task.CompletedTask;
        }

        private void MediaMixerService_StateChangedEvent(object sender)
        {
            IsFontButtonEnabled = HasTelepromterText && (MediaMixer?.State != TouchcastComposerServiceState.Recording);
        }

        #endregion

        #region Private Methods

        private void NewTake()
        {
            var manager = RecordingManager;
            if (manager != null)
            {
                manager.CreateNewTake();
                var authoringSession = AuthoringSession;
                if (authoringSession != null)
                {
                    var lastMediaClip = authoringSession.Project.TcMediaComposition.MediaClips.LastOrDefault();
                    if (lastMediaClip != null)
                    {
                        TakesCounter = lastMediaClip.Takes.Count;
                    }
                }
            }
        }

        private async Task Finish(bool isSwitchToEdit)
        {
            if (isSwitchToEdit)
            {
                await StopRecording();
                var mixer = MediaMixer;
                if(mixer != null)
                {
                    await mixer.Reset();
                    TouchCastComposingEngine.ComposingContext.Shared().ResetContext();
                }
                App.Frame.Navigate(typeof(EditingPage));
                App.Frame.BackStack.Clear();
                App.Frame.ForwardStack.Clear();
                App.Frame.CacheSize = 0;
            }
        }

        private async Task StopRecording()
        {
            var manager = RecordingManager;
            if (manager != null)
            {
                await manager.StopRecording(false);
            }
        }

        private void UpdateTelepromterValues()
        {
            AuthoringSession authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                Shot activeShot = authoringSession.Project.ActiveShot;
                TeleprompterText = "\n" + activeShot.TeleprompterText;
                HasTelepromterText = !(String.IsNullOrEmpty(TeleprompterText) || String.IsNullOrEmpty(TeleprompterText.Trim()));
                IsFontButtonEnabled = HasTelepromterText && (MediaMixer?.State != TouchcastComposerServiceState.Recording);
                IsTeleprompterTextShow = HasTelepromterText && activeShot.IsShowTeleprompterInRecordMode;
            }
        }

        #endregion
    }
}