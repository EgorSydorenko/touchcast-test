﻿using System;
using System.Threading.Tasks;
using Windows.System;

namespace Pitch.Shortcut
{
    public delegate Task<bool> ShortcutManagerDelegate();
    public abstract class BaseShortcutsManager
    {
        public async Task<bool> ShortcutAction(ShortcutActionCode shortcutActionCode)
        {
            var actionToProceed = ChooseAction(shortcutActionCode);
            bool result = actionToProceed != null;
            if (result)
            {
                result = await actionToProceed.Invoke();
            }
            return result;
        }

#if DEBUG
        ~BaseShortcutsManager()
        {
            System.Diagnostics.Debug.WriteLine("********************BaseShortcutsManager Destructor********************");
        }
#endif
        protected virtual ShortcutManagerDelegate ChooseAction(ShortcutActionCode actionCode)
        {
            switch (actionCode)
            {
                case ShortcutActionCode.ShowLogs:
                    OpenLogDirectory();
                    break;
                default:
                    break;
            }

            return null;
        }

        private async void OpenLogDirectory()
        {
            await Launcher.LaunchFolderAsync(App.LogsFolder);
        }
    }
}
