﻿using System.Collections.Generic;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Pitch.Shortcut
{
    enum KeyModifier
    {
        None = 0,
        Control = 0x100,
        Shift = 0x200,
        Alt = 0x400
    }

    public enum ShortcutActionCode
    {
        None = VirtualKey.None,
        Export = KeyModifier.Control | VirtualKey.E,
        Open = KeyModifier.Control | VirtualKey.O,
        Save = KeyModifier.Control | VirtualKey.S,
        New = KeyModifier.Control | VirtualKey.N,
        Undo = KeyModifier.Control | VirtualKey.Z,
        Redo = KeyModifier.Control | VirtualKey.Y,
        Cut = KeyModifier.Control | VirtualKey.X,
        Delete = VirtualKey.Delete,
        Backspace = VirtualKey.Back,
        Copy = KeyModifier.Control | VirtualKey.C,
        Duplicate = KeyModifier.Control | VirtualKey.D,
        Split = KeyModifier.Control | VirtualKey.Q,
        Paste = KeyModifier.Control | VirtualKey.V,
        InsertText = KeyModifier.Control | VirtualKey.Enter,
        StepObjectBack = KeyModifier.Control | 219, // [
        StepObjectForward = KeyModifier.Control | 221, // ]
        SendObjectBack = KeyModifier.Control | KeyModifier.Shift | 219,
        SendObjectForward = KeyModifier.Control | KeyModifier.Shift | 221,
        SelectAll = KeyModifier.Control | VirtualKey.A,
        PlayMedia = KeyModifier.Control | VirtualKey.Space,
        InsertHyperlink = KeyModifier.Control | VirtualKey.K,
        ApplyBold = KeyModifier.Control | VirtualKey.B,
        ApplyUnderline = KeyModifier.Control | VirtualKey.U,
        ApplyItalic = KeyModifier.Control | VirtualKey.I,
        IncFontSize = KeyModifier.Control | KeyModifier.Shift | 190, // >
        DecFontSize = KeyModifier.Control | KeyModifier.Shift | 188, // <
        ZoomIn = KeyModifier.Control | VirtualKey.Add,
        ZoomOut = KeyModifier.Control | VirtualKey.Subtract,
        NextFrame = KeyModifier.Shift | VirtualKey.Right,
        NextClip = KeyModifier.Control | VirtualKey.Right,
        PrevFrame = KeyModifier.Shift | VirtualKey.Left,
        PrevClip = KeyModifier.Control | VirtualKey.Left,
        MoveRight = VirtualKey.Right,
        MoveLeft = VirtualKey.Left,
        MoveUp = VirtualKey.Up,
        MoveDown = VirtualKey.Down,
        NextObject = VirtualKey.Tab,
        PrevObject = KeyModifier.Shift | VirtualKey.Tab,
        CenterParagraph = KeyModifier.Control | VirtualKey.E,
        JustifyParagraph = KeyModifier.Control | VirtualKey.J,
        LeftAlignParagraph = KeyModifier.Control | VirtualKey.L,
        RightAlignParagraph = KeyModifier.Control | VirtualKey.R,
        NewShot = KeyModifier.Control | VirtualKey.M,
        VappMoveRight = KeyModifier.Shift | VirtualKey.Right,
        VappMoveLeft = KeyModifier.Shift | VirtualKey.Left,
        VappMoveUp = KeyModifier.Shift | VirtualKey.Up,
        VappMoveDown = KeyModifier.Shift | VirtualKey.Down,
        ShowLogs = KeyModifier.Alt | KeyModifier.Control | VirtualKey.L,
    }

    public enum ShortcutsManagerId
    {
        Vapp,
        Format,
        Edit,
        Recording
    }

    public class Manager
    {
        private static Manager _instance;
        private Dictionary<ShortcutsManagerId, BaseShortcutsManager> _shortcutsManagers;
        private ShortcutsManagerId _currentManager = ShortcutsManagerId.Vapp;
        private bool _isEnabled;

        private readonly List<ShortcutActionCode> RichEditShortcuts = new List<ShortcutActionCode>
        {
            ShortcutActionCode.Copy, ShortcutActionCode.Cut, ShortcutActionCode.Paste, ShortcutActionCode.Delete, ShortcutActionCode.Backspace,
            ShortcutActionCode.MoveRight, ShortcutActionCode.MoveLeft, ShortcutActionCode.MoveUp, ShortcutActionCode.MoveDown,
            ShortcutActionCode.VappMoveRight, ShortcutActionCode.VappMoveLeft, ShortcutActionCode.VappMoveUp, ShortcutActionCode.VappMoveDown
        };

        public VappShortcutsManager VappShortcutsManager => _shortcutsManagers[ShortcutsManagerId.Vapp] as VappShortcutsManager;
        public FormatShortcutsManager FormatShortcutsManager => _shortcutsManagers[ShortcutsManagerId.Format] as FormatShortcutsManager;
        public EditShortcutsManager EditShortcutsManager => _shortcutsManagers[ShortcutsManagerId.Edit] as EditShortcutsManager;
        public RecordingShortcutsManager RecordingShortcutManager => _shortcutsManagers[ShortcutsManagerId.Recording] as RecordingShortcutsManager;

        public static Manager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Manager();
                }
                return _instance;
            }
        }

        public void Init()
        {
            IsEnabled = true;
        }

        public bool IsEnabled
        {
            set
            {
                if (_isEnabled == value) return;

                _isEnabled = value;
                if (_isEnabled)
                {
                    Window.Current.CoreWindow.Dispatcher.AcceleratorKeyActivated += Dispatcher_AcceleratorKeyActivated;
                }
                else
                {
                    Window.Current.CoreWindow.Dispatcher.AcceleratorKeyActivated -= Dispatcher_AcceleratorKeyActivated;
                }
            }
            get
            {
                return _isEnabled;
            }
        }

        public void Activate(ShortcutsManagerId currentManager)
        {
            _currentManager = currentManager;
        }

        #region Private methods
        private Manager()
        {
            _shortcutsManagers = new Dictionary<ShortcutsManagerId, BaseShortcutsManager>()
            {
                { ShortcutsManagerId.Format, new FormatShortcutsManager() },
                { ShortcutsManagerId.Edit, new EditShortcutsManager() },
                { ShortcutsManagerId.Vapp, new VappShortcutsManager() },
                { ShortcutsManagerId.Recording, new RecordingShortcutsManager() }
            };
        }

        private async void Dispatcher_AcceleratorKeyActivated(CoreDispatcher sender, AcceleratorKeyEventArgs args)
        {
            IsEnabled = false;
            
            var key = args.VirtualKey;
            var keyModifier = KeyModifier.None;
            var systemVirtualKey = VirtualKey.None;
            if ((Window.Current.CoreWindow.GetKeyState(VirtualKey.Menu) & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down)
            {
                keyModifier |= KeyModifier.Alt;
                systemVirtualKey = VirtualKey.Menu;
            }

            if ((Window.Current.CoreWindow.GetKeyState(VirtualKey.Control) & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down)
            {
                keyModifier |= KeyModifier.Control;
                systemVirtualKey = VirtualKey.Control;
            }

            if ((Window.Current.CoreWindow.GetKeyState(VirtualKey.Shift) & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down)
            {
                keyModifier |= KeyModifier.Shift;
                systemVirtualKey = VirtualKey.Shift;
            }

            if ((args.EventType == CoreAcceleratorKeyEventType.KeyDown || args.EventType == CoreAcceleratorKeyEventType.SystemKeyDown)
                && key != systemVirtualKey)
            {
                var element = Windows.UI.Xaml.Input.FocusManager.GetFocusedElement();
                var actionCode = (ShortcutActionCode)((int)args.VirtualKey | (int)keyModifier);
                if (!(element is TextBox
                      || (element is RichEditBox && RichEditShortcuts.Contains(actionCode))
                      || element is WebView))
                {
                   
                    var manager = _shortcutsManagers[_currentManager];
                    var actionResult = await manager.ShortcutAction(actionCode);
                    if (!args.Handled)
                    {
                        args.Handled = actionResult;
                    }
                }
            }

            IsEnabled = true;
        }
        #endregion
    }
}
