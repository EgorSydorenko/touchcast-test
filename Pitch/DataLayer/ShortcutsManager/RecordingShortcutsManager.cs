﻿namespace Pitch.Shortcut
{
    public class RecordingShortcutsManager : BaseShortcutsManager
    {
        public event ShortcutManagerDelegate VappMoveRightShortcutEvent;
        public event ShortcutManagerDelegate VappMoveLeftShortcutEvent;
        public event ShortcutManagerDelegate VappMoveUpShortcutEvent;
        public event ShortcutManagerDelegate VappMoveDownShortcutEvent;
        public event ShortcutManagerDelegate MoveRightShortcutEvent;
        public event ShortcutManagerDelegate MoveLeftShortcutEvent;
        public event ShortcutManagerDelegate MoveUpShortcutEvent;
        public event ShortcutManagerDelegate MoveDownShortcutEvent;

        protected override ShortcutManagerDelegate ChooseAction(ShortcutActionCode actionCode)
        {
            ShortcutManagerDelegate actionToProceed = base.ChooseAction(actionCode);
            switch (actionCode)
            {
                case ShortcutActionCode.MoveRight:
                    actionToProceed = MoveRightShortcutEvent;
                    break;
                case ShortcutActionCode.MoveLeft:
                    actionToProceed = MoveLeftShortcutEvent;
                    break;
                case ShortcutActionCode.MoveUp:
                    actionToProceed = MoveUpShortcutEvent;
                    break;
                case ShortcutActionCode.MoveDown:
                    actionToProceed = MoveDownShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveRight:
                    actionToProceed = VappMoveRightShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveLeft:
                    actionToProceed = VappMoveLeftShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveUp:
                    actionToProceed = VappMoveUpShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveDown:
                    actionToProceed = VappMoveDownShortcutEvent;
                    break;
            }
            return actionToProceed;
        }
    }
}
