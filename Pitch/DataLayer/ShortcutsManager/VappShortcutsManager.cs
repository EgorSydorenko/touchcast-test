﻿namespace Pitch.Shortcut
{
    public class VappShortcutsManager : BaseShortcutsManager
    {
        public event ShortcutManagerDelegate StepObjectBackShortcutEvent;
        public event ShortcutManagerDelegate StepObjectForwardShortcutEvent;
        public event ShortcutManagerDelegate SendObjectBackShortcutEvent;
        public event ShortcutManagerDelegate SendObjectForwardShortcutEvent;
        public event ShortcutManagerDelegate OpenShortcutEvent;
        public event ShortcutManagerDelegate SaveShortcutEvent;
        public event ShortcutManagerDelegate NewShortcutEvent;
        public event ShortcutManagerDelegate ExportShortcutEvent;
        public event ShortcutManagerDelegate UndoShortcutEvent;
        public event ShortcutManagerDelegate RedoShortcutEvent;
        public event ShortcutManagerDelegate CutShortcutEvent;
        public event ShortcutManagerDelegate CopyShortcutEvent;
        public event ShortcutManagerDelegate PasteShortcutEvent;
        public event ShortcutManagerDelegate DeleteShortcutEvent;
        public event ShortcutManagerDelegate BackspaceShortcutEvent;
        public event ShortcutManagerDelegate SelectAllShortcutEvent;
        public event ShortcutManagerDelegate InsertHyperlinkShortcutEvent;
        public event ShortcutManagerDelegate NextObjectShortcutEvent;
        public event ShortcutManagerDelegate PrevObjectShortcutEvent;
        public event ShortcutManagerDelegate InsertTextShortcutEvent;
        public event ShortcutManagerDelegate NewShotShortcutEvent;
        public event ShortcutManagerDelegate MoveRightShortcutEvent;
        public event ShortcutManagerDelegate MoveLeftShortcutEvent;
        public event ShortcutManagerDelegate MoveUpShortcutEvent;
        public event ShortcutManagerDelegate MoveDownShortcutEvent;
        public event ShortcutManagerDelegate VappMoveRightShortcutEvent;
        public event ShortcutManagerDelegate VappMoveLeftShortcutEvent;
        public event ShortcutManagerDelegate VappMoveUpShortcutEvent;
        public event ShortcutManagerDelegate VappMoveDownShortcutEvent;
        public event ShortcutManagerDelegate DuplicateClipShortcutEvent;

        protected override ShortcutManagerDelegate ChooseAction(ShortcutActionCode actionCode)
        {
            ShortcutManagerDelegate actionToProceed = base.ChooseAction(actionCode);
            switch (actionCode)
            {
                case ShortcutActionCode.InsertText:
                    actionToProceed = InsertTextShortcutEvent;
                    break;
                case ShortcutActionCode.Export:
                    actionToProceed = ExportShortcutEvent;
                    break;
                case ShortcutActionCode.Undo:
                    actionToProceed = UndoShortcutEvent;
                    break;
                case ShortcutActionCode.Redo:
                    actionToProceed = RedoShortcutEvent;
                    break;
                case ShortcutActionCode.Cut:
                    actionToProceed = CutShortcutEvent;
                    break;
                case ShortcutActionCode.Copy:
                    actionToProceed = CopyShortcutEvent;
                    break;
                case ShortcutActionCode.Paste:
                    actionToProceed = PasteShortcutEvent;
                    break;
                case ShortcutActionCode.Delete:
                    actionToProceed = DeleteShortcutEvent;
                    break;
                case ShortcutActionCode.Backspace:
                    actionToProceed = BackspaceShortcutEvent;
                    break;
                case ShortcutActionCode.InsertHyperlink:
                    actionToProceed = InsertHyperlinkShortcutEvent;
                    break;
                case ShortcutActionCode.Open:
                    actionToProceed = OpenShortcutEvent;
                    break;
                case ShortcutActionCode.Save:
                    actionToProceed = SaveShortcutEvent;
                    break;
                case ShortcutActionCode.New:
                    actionToProceed = NewShortcutEvent;
                    break;
                case ShortcutActionCode.SelectAll:
                    actionToProceed = SelectAllShortcutEvent;
                    break;
                case ShortcutActionCode.SendObjectBack:
                    actionToProceed = SendObjectBackShortcutEvent;
                    break;
                case ShortcutActionCode.SendObjectForward:
                    actionToProceed = SendObjectForwardShortcutEvent;
                    break;
                case ShortcutActionCode.StepObjectBack:
                    actionToProceed = StepObjectBackShortcutEvent;
                    break;
                case ShortcutActionCode.StepObjectForward:
                    actionToProceed = StepObjectForwardShortcutEvent;
                    break;
                case ShortcutActionCode.NextObject:
                    actionToProceed = NextObjectShortcutEvent;
                    break;
                case ShortcutActionCode.PrevObject:
                    actionToProceed = PrevObjectShortcutEvent;
                    break;
                case ShortcutActionCode.NewShot:
                    actionToProceed = NewShotShortcutEvent;
                    break;
                case ShortcutActionCode.MoveRight:
                    actionToProceed = MoveRightShortcutEvent;
                    break;
                case ShortcutActionCode.MoveLeft:
                    actionToProceed = MoveLeftShortcutEvent;
                    break;
                case ShortcutActionCode.MoveUp:
                    actionToProceed = MoveUpShortcutEvent;
                    break;
                case ShortcutActionCode.MoveDown:
                    actionToProceed = MoveDownShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveRight:
                    actionToProceed = VappMoveRightShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveLeft:
                    actionToProceed = VappMoveLeftShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveUp:
                    actionToProceed = VappMoveUpShortcutEvent;
                    break;
                case ShortcutActionCode.VappMoveDown:
                    actionToProceed = VappMoveDownShortcutEvent;
                    break;
                case ShortcutActionCode.Duplicate:
                    actionToProceed = DuplicateClipShortcutEvent;
                    break;
            }
            return actionToProceed;
        }
    }
}
