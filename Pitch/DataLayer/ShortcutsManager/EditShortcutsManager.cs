﻿namespace Pitch.Shortcut
{
    public class EditShortcutsManager : BaseShortcutsManager
    {
        public event ShortcutManagerDelegate DuplicateClipShortcutEvent;
        public event ShortcutManagerDelegate DeleteClipShortcutEvent;
        public event ShortcutManagerDelegate SplitClipShortcutEvent;
        public event ShortcutManagerDelegate PlayMediaShortcutEvent;
        public event ShortcutManagerDelegate ZoomInShortcutEvent;
        public event ShortcutManagerDelegate ZoomOutShortcutEvent;
        public event ShortcutManagerDelegate NextFrameShortcutEvent;
        public event ShortcutManagerDelegate PrevFrameShortcutEvent;
        public event ShortcutManagerDelegate NextClipShortcutEvent;
        public event ShortcutManagerDelegate PrevClipShortcutEvent;
        public event ShortcutManagerDelegate ExportShortcutEvent;
        public event ShortcutManagerDelegate UndoShortcutEvent;
        public event ShortcutManagerDelegate RedoShortcutEvent;
        public event ShortcutManagerDelegate NextSceneShortcutEvent;
        public event ShortcutManagerDelegate PrevSceneShortcutEvent;
        public event ShortcutManagerDelegate OpenShortcutEvent;
        public event ShortcutManagerDelegate SaveShortcutEvent;
        public event ShortcutManagerDelegate NewShortcutEvent;
        public event ShortcutManagerDelegate BackspaceShortcutEvent;

        protected override ShortcutManagerDelegate ChooseAction(ShortcutActionCode actionCode)
        {
            ShortcutManagerDelegate actionToProceed = base.ChooseAction(actionCode);
            switch (actionCode)
            {
                case ShortcutActionCode.Export:
                    actionToProceed = ExportShortcutEvent;
                    break;
                case ShortcutActionCode.Duplicate:
                    actionToProceed = DuplicateClipShortcutEvent;
                    break;
                case ShortcutActionCode.Backspace:
                    actionToProceed = BackspaceShortcutEvent;
                    break;
                case ShortcutActionCode.Delete:
                    actionToProceed = DeleteClipShortcutEvent;
                    break;
                case ShortcutActionCode.Split:
                    actionToProceed = SplitClipShortcutEvent;
                    break;
                case ShortcutActionCode.PlayMedia:
                    actionToProceed = PlayMediaShortcutEvent;
                    break;
                case ShortcutActionCode.ZoomIn:
                    actionToProceed = ZoomInShortcutEvent;
                    break;
                case ShortcutActionCode.ZoomOut:
                    actionToProceed = ZoomOutShortcutEvent;
                    break;
                case ShortcutActionCode.NextFrame:
                    actionToProceed = NextFrameShortcutEvent;
                    break;
                case ShortcutActionCode.NextClip:
                    actionToProceed = NextClipShortcutEvent;
                    break;
                case ShortcutActionCode.PrevFrame:
                    actionToProceed = PrevFrameShortcutEvent;
                    break;
                case ShortcutActionCode.PrevClip:
                    actionToProceed = PrevClipShortcutEvent;
                    break;
                case ShortcutActionCode.Undo:
                    actionToProceed = UndoShortcutEvent;
                    break;
                case ShortcutActionCode.Redo:
                    actionToProceed = RedoShortcutEvent;
                    break;
                case ShortcutActionCode.MoveUp:
                    actionToProceed = PrevSceneShortcutEvent;
                    break;
                case ShortcutActionCode.MoveDown:
                    actionToProceed = NextSceneShortcutEvent;
                    break;
                case ShortcutActionCode.Open:
                    actionToProceed = OpenShortcutEvent;
                    break;
                case ShortcutActionCode.Save:
                    actionToProceed = SaveShortcutEvent;
                    break;
                case ShortcutActionCode.New:
                    actionToProceed = NewShortcutEvent;
                    break;
            }
            return actionToProceed;
        }
    }
}
