﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pitch.DataLayer
{
    public enum ServerType
    {
        dev,
        stage,
        production
    }
    public class FabricEndpoint
    {
        public string api_endpoint { set; get; }
        public string login_endpoint { set; get; }
        public string redirect_endpoint { set; get; }
        public string change_privacy_endpoint { set; get; }
        public string profile_endpoint { set; get; }
        public string academy_endpoint { set; get; }
    }
}
