﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouchCastUWP.Data_Layer.MetaData
{
    public class GreenScreenSettingsMetaData
    {
        public string Color { set; get; }
        public double Smothness { set; get; }
        public double Sensitivity { set; get; }
        public bool isEnabled { set; get; }
        public string BackgroundResourceId { set; get; }
    }

    public class RecordableItemMetaData
    {
        public string Type { set; get; }
        public string ResourceId { set; get; }
        public string CameraMode { set; get; }
        public string Transform { set; get; }
        public double TimIn { set; get; }
        public double TimeOut { set; get; }
        public string Title { set; get; }
        public string WebUrl { set; get; }
    }

    public class ResourceMetaData
    {
        public string Id { set; get; }
        public string File { set; get; }
        public string ActionStream { set; get; }
        public double Duration { set; get; }
    }

    public class RecordingMetaData
    {

    }

    public class ProjectMetaData
    {

    }
}

namespace asdasda
{
    class AuthoringSession
    {
        TouchCastUWP.Models.TouchcastResource GetResourceById(string id)
        {
            return null;
        }
    }
}
