﻿using System.Threading.Tasks;
using Pitch.UILayer.Helpers.Converters;
using Windows.Storage;
using Windows.UI;

namespace Pitch.DataLayer
{
    public delegate void GreenScreenSettingsChangeEvent(GreenScreenSettings settings);
    public class GreenScreenSettings
    {
        #region Private Fields
        private double _smoothness;
        private double _sensetivity;
        private Color _color;
        private bool _isColorPickerOn;
        private bool _isOn;
        #endregion

        #region Public Properties

        public double Smoothness
        {
            get
            {
                return _smoothness;
            }
            set
            {
                GreenScreenSettingsWillBeChanged?.Invoke(this);
                _smoothness = value;
                GreenScreenSettingsWasChanged?.Invoke(this);
            }
        }

        public bool IsColorPickerOn
        {
            get
            {
                return _isColorPickerOn;
            }
            set
            {
                GreenScreenSettingsWillBeChanged?.Invoke(this);
                _isColorPickerOn = value;
                GreenScreenSettingsWasChanged?.Invoke(this);
            }
        }

        public double Sensetivity
        {
            get
            {
                return _sensetivity;
            }
            set
            {
                GreenScreenSettingsWillBeChanged?.Invoke(this);
                _sensetivity = value;
                GreenScreenSettingsWasChanged?.Invoke(this);
            }
        }

        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                GreenScreenSettingsWillBeChanged?.Invoke(this);
                _color = value;
                GreenScreenSettingsWasChanged?.Invoke(this);
            }
        }

        public bool IsOn
        {
            get
            {
                return _isOn;
            }
            set
            {
                GreenScreenSettingsWillBeChanged?.Invoke(this);
                _isOn = value;
                GreenScreenSettingsWasChanged?.Invoke(this);
            }
        }
        #endregion

        public event GreenScreenSettingsChangeEvent GreenScreenSettingsWillBeChanged;
        public event GreenScreenSettingsChangeEvent GreenScreenSettingsWasChanged;

        public static async Task<GreenScreenSettings> FromMetaData(StorageFolder folder, TcpInfo.SegmentationSettings metadata)
        {
            GreenScreenSettings settings = new GreenScreenSettings();
            if (metadata.manual_segmentation_settings.color.Length == 7)
            {
                try
                {
                    settings._color = HexToColorConverter.ConvertHexToRgbColor(metadata.manual_segmentation_settings.color);
                }
                catch
                {
                    settings._color = Colors.Green;
                }
            }
            else if (metadata.manual_segmentation_settings.color.Length == 9)
            {
                try
                {
                    settings._color = HexToColorConverter.ConvertHexToArgbColor(metadata.manual_segmentation_settings.color);
                }
                catch
                {
                    settings._color = Colors.Green;
                }
            }
            settings._sensetivity = metadata.manual_segmentation_settings.sensitivity;
            settings._smoothness = metadata.manual_segmentation_settings.smoothness;
            settings._isOn = metadata.enabled;
            return settings;
        }

        public GreenScreenSettings()
        {
            Sensetivity = 0.3;
            Smoothness = 0;
            Color = Colors.Green;
        }

        public GreenScreenSettings Clone()
        {
            var clone = new GreenScreenSettings
            {
                _color = Color,
                _smoothness = Smoothness,
                _sensetivity = Sensetivity,
                _isOn = IsOn,
            };

            return clone;
        }

        public TcpInfo.SegmentationSettings GetMetaData()
        {
            TcpInfo.SegmentationSettings meta = new TcpInfo.SegmentationSettings();
            meta.manual_segmentation_settings = new TcpInfo.ManualSegmentationSettings();
            meta.manual_segmentation_settings.color = HexToColorConverter.ConvertColorToHex(_color);
            meta.enabled = _isOn;
            meta.manual_segmentation_settings.sensitivity = _sensetivity;
            meta.manual_segmentation_settings.smoothness = _smoothness;
            return meta;
        }
    }
}
