﻿using GalaSoft.MvvmLight.Ioc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.Shots;
using Pitch.DataLayer.UndoRedo;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Pitch.Services;
using Pitch.UILayer.Authoring.Views;
using Pitch.UILayer.Helpers.Converters;
using Pitch.DataLayer.Helpers.Extensions;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI;
using Pitch.Helpers.Analytics;

namespace Pitch.DataLayer
{
    public class Project
    {
        #region Constants

        public readonly static string DefaultProjectName = "New Project";
        public readonly static string TeleprompterSplitPattern = @"[\r\n\s]*---- Scene [0-9]+ ----[\r\n\s]*";
        public readonly static TimeSpan AutoSavePeriod = TimeSpan.FromMinutes(5);
        public readonly static string ProjectInfoFileName = "project.tcpinfo";
        public readonly static string ProjectThemeFolderName = "theme";

        #endregion

        #region Privat Fields
        private DateTime _creationTime;
        private DateTime _lastModifyTime;
        private ulong _size;
        private Project _theme;
        private const float _currentTcpFileVersion = 1.1f;
        private List<Shot> _shots;
        private StorageFolder _workingFolder;
        private Shot _activeShot;
        private TcMediaComposition _tcMediaComposition;
        private bool _hasUnsavedChanges;
        private string _fileToken;
        private UndoRedoOriginator _undoRedoOriginator;
        private UndoRedoOriginator _editorUndoRedoOriginator;
        private string _name = String.Empty;
        private string _description;
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private LayerLayout _bufferedLayer;
        private readonly Models.AsyncLock _saveProjectLocker = new Models.AsyncLock(1);
        private BackgroundRenderableObject _backgroundRenderableObject;
        private ThreadPoolTimer _autoSaveTimer;
        private bool _isAutoSaveEnabled;
        private AsyncLock _ioOperationLocker = new AsyncLock(1);

        private IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();

        private StorageFile _teleprompterFile;
        #endregion

        #region Events

        public delegate Task ProjectEvent();
        public delegate void ProjectSaveEvent();
        public delegate Task ShotEvent(Shot shot);
        public delegate void ActiveShotEvent();
        public delegate void RecordingsEmpty();
        public delegate void RecordingIsRestored(Shot shot);
#pragma warning disable CS0067
        public event ProjectEvent ProjectWillBeOpened;
        public event ProjectEvent ProjectWasOpened;
        public event ProjectSaveEvent ProjectWillBeSaved;
        public event ProjectSaveEvent ProjectWasSaved;
        public event ShotEvent ShotWillBeCreated;
        public event ShotEvent ShotWasCreated;
        public event ShotEvent ShotWillBeRemoved;
        public event ShotEvent ShotWasRemoved;
        public event ShotEvent ShotWillBeMoved;
        public event ShotEvent ShotWasMoved;
        public event ActiveShotEvent ActiveShotWillBeChanged;
        public event ActiveShotEvent ActiveShotWasChanged;
        public event RecordingsEmpty RecordingsEmptyEvent;
        public event RecordingIsRestored RecordingIsRestoredEvent;
#pragma warning restore CS0067
        #endregion

        #region Public Properties
        public BackgroundRenderableObject BackgroundRenderableObject => _backgroundRenderableObject;
        public Project Theme => _theme;
        public DateTime CreationTime => _creationTime;
        public DateTime LastModifyTime => _lastModifyTime;
        public ulong Size => _size;
        public List<Shot> Shots => _shots;
        public TcMediaComposition TcMediaComposition => _tcMediaComposition;
        public string Name => _name;
        public string Description => _description;
        public UndoRedoOriginator UndoRedoOriginator => _undoRedoOriginator;
        public UndoRedoOriginator EditorUndoRedoOriginator => _editorUndoRedoOriginator;
        
        public bool HasUnsavedChanges
        {
            get { return _hasUnsavedChanges; }
            set { _hasUnsavedChanges = value; }
        }

        public bool IsTemporary => (_fileToken == null);

        public StorageFolder WorkingFolder => _workingFolder;

        public Shot ActiveShot
        {
            set
            {
                ActiveShotWillBeChanged?.Invoke();
                _activeShot = value;
                LogQueue.WriteToFile($"Project Active shot {_activeShot.Id}");
                ActiveShotWasChanged?.Invoke();
            }
            get => _activeShot;
        }
       
        public LayerLayout BufferedLayer => _bufferedLayer;

        public AsyncLock SaveProjectLocker => _saveProjectLocker;

        #endregion

        #region Life Cycle

        private Project()
        {
            _undoRedoOriginator = new UndoRedoOriginator();
            _editorUndoRedoOriginator = new UndoRedoOriginator();
            _creationTime = DateTime.Now;
            _lastModifyTime = DateTime.Now;
            _shots = new List<Shot>();
            _tcMediaComposition = new TcMediaComposition(this);
            _tcMediaComposition.Subscribe();
            var mixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            _mediaMixerService = new WeakReference<IMediaMixerService>(mixer);
            _undoRedoOriginator.RedoEvent += UndoRedoOriginator_RedoEvent;
            _undoRedoOriginator.UndoEvent += UndoRedoOriginator_UndoEvent;
            _backgroundRenderableObject = new BackgroundRenderableObject();
        }

#if DEBUG
        ~Project()
        {
            System.Diagnostics.Debug.WriteLine("*********************** Project Destructor *************************");
        }
#endif
        #endregion

        #region Static
        public static async Task<Project> OpenNewProjectWithTcpx(StorageFile file)
        {
            Project resultProject = null;
            StorageFolder temporaryFolder = null;

            try
            {
                temporaryFolder = await ApplicationData.Current.LocalCacheFolder.CreateFolderAsync(Guid.NewGuid().ToString(), CreationCollisionOption.OpenIfExists);
                await UnArchive(file, temporaryFolder);
                resultProject = await CreateNewProjectFromThemeFolder(temporaryFolder.Path, false);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.OpenNewProjectWithTcpx {ex.Message}");
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(new UILayer.Helpers.Messages.ProjectCorruptedMessage(OpenProjecFileErrorType.ProjectFileCorrupted));
                resultProject = null;
            }
            finally
            {
                if (temporaryFolder != null)
                {
                    try
                    {
                        await temporaryFolder.DeleteAsync();
                    }
                    catch { }
                }
                if (resultProject == null)
                {
                    var template = ThemeManager.Instance.DefaultTheme;
                    resultProject = await OpenNewProject(template.theme_foler_path);
                }
            }
            return resultProject;
        }

        public static async Task<Project> OpenNewProject(string themeFolderPath)
        {
            LogQueue.WriteToFile("Project OpenNewProject");

            String extension = String.Empty;
            try
            {
                extension = Path.GetExtension(themeFolderPath);
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile(ex.Message);
            }
            Project resultProject = null;
            if(String.IsNullOrEmpty(extension))
            {
                resultProject = await CreateNewProjectFromThemeFolder(themeFolderPath, true);
            }
            else if(extension == FileToAppHelper.TouchcastProjectExtension)
            {
                var file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(themeFolderPath);
                resultProject = await OpenFromFile(file, ThemeManager.Instance.DefaultTheme.theme_foler_path);
                resultProject._fileToken = null;
            }
            return resultProject;

        }

        protected static async Task<Project> LoadProjectAsTheme(StorageFolder folder)
        {
            var project = new Project();
            project._workingFolder = folder;
            var tcpInfoFile = await project._workingFolder.GetFileAsync(ProjectInfoFileName);
            var str = File.ReadAllText(tcpInfoFile.Path);
            var result = JsonConvert.DeserializeObject<TcpInfo.Project>(str);
            await LoadProjectBasicData(project, result, true);

            if (result.recordings != null)
            {
                foreach (var record in result.recordings)
                {
                    await DeleteFile(folder, record.actions_file);
                    await DeleteFile(folder, record.thumbnail_file);
                    await DeleteFile(folder, record.video_file);
                }
                result.recordings = null;
            }
            if (result.video_composition != null)
            {
                foreach (var record in result.video_composition)
                {
                    await DeleteFile(folder, record.thumbnail_file);
                }
                result.video_composition = null;
            }

            var json = JsonConvert.SerializeObject(result,
                            Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
            await project.CreateTcpInfo(json);
            return project;
        }

        protected static async Task<Project> LoadProjectBasicData(Project project, TcpInfo.Project metaData, bool isTheme)
        {
            try
            {
                var shotsCount = metaData.scene_thumbnails.Count;
                if (shotsCount == 0)
                {
                    try
                    {
                        shotsCount = metaData.elements.Max(e => e.scene_index);
                    }
                    catch
                    {

                    }
                    shotsCount += 1;
                }

                var teleprompterTexts = new string[0];

                if (metaData.teleprompter.text_file != null)
                {
                    teleprompterTexts = await ReadTeleprompterFromFile(project, metaData.teleprompter.text_file);
                }

                for (int i = 0; i < shotsCount; i++)
                {
                    try
                    {
                        var elementsForShot = metaData.elements.Where(x => x.scene_index == i).ToList();
                        var greenscreenForShot = metaData.segmentations.Where(g => g.scene_index == i).FirstOrDefault();

                        var backgroundColorForShot = Colors.Black;
                        if (metaData.background_colors != null && metaData.background_colors.Count > i)
                        {
                            backgroundColorForShot = HexToColorConverter.ConvertHexToArgbColor(metaData.background_colors[i]);
                        }
                        var shot = await Shot.Create(project, elementsForShot, greenscreenForShot, isTheme, backgroundColorForShot);

                        if (teleprompterTexts != null && teleprompterTexts.Length > i)
                        {
                            shot.TeleprompterText = teleprompterTexts[i];
                        }
                        project._shots.Add(shot);
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Project.LoadProjectBasicData : {ex.Message}");
                    }
                }
            }
            catch
            {

            }
            finally
            {

            }
            return project;
        }
        private static async Task DeleteFile(StorageFolder folder, string fileName)
        {
            if (fileName != null)
            {
                try
                {
                    var file = await folder.GetFileAsync(fileName);
                    await file.DeleteAsync();
                }
                catch { }
            }
        }
        private static async Task<Project> CreateNewProjectFromThemeFolder(string themeFolderPath, bool isOpenFromInstalledLocation)
        {
            var project = new Project()
            {
                _name = DefaultProjectName,
            };
            await project._ioOperationLocker.WaitAsync();
            var isError = false;
            try
            {
                await project.Init();

                var folder = (isOpenFromInstalledLocation) ? await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(themeFolderPath)
                    : await StorageFolder.GetFolderFromPathAsync(themeFolderPath);

                var themeFolder = await project._workingFolder.CreateFolderAsync(ProjectThemeFolderName, CreationCollisionOption.ReplaceExisting);
                await CopyFolderAsync(folder, themeFolder, isOpenFromInstalledLocation);
                project._theme = await LoadProjectAsTheme(themeFolder);
                project._theme.ActiveShot = project._theme.Shots.FirstOrDefault();
                await project.PrepopulateShots();
                GoogleAnalyticsManager.TrySend(AnalyticCommandType.AnalyticCommandProjectCreated,
                    ThemeManager.Instance.SelectedTheme == null ?
                    ThemeManager.Instance.DefaultTheme.name :
                    ThemeManager.Instance.SelectedTheme.name);
            }
            catch (Exception ex)
            {
                isError = true;
                LogQueue.WriteToFile($"Exception in Project.CreateNewProjectFromThemeFolder {ex.Message}");
            }
            finally
            {
                project._ioOperationLocker.Release();
                if (isError)
                {
                    project = null;
                }
            }
            return project;
        }
        private static async Task CopyFolderAsync(StorageFolder source, StorageFolder destinationFolder, bool isNeedCopyFolders = true)
        {
            foreach (var file in await source.GetFilesAsync())
            {
                await file.CopyAsync(destinationFolder, file.Name, NameCollisionOption.ReplaceExisting);
            }

            if (isNeedCopyFolders)
            {
                foreach (var folder in await source.GetFoldersAsync())
                {
                    await CopyFolderAsync(folder, destinationFolder);
                }
            }
        }

        public static async Task<Project> OpenFromFile(StorageFile file, string defaultThemePath)
        {
            LogQueue.WriteToFile("Project OpenFromFile");
            Project project = new Project();

            await project._ioOperationLocker.WaitAsync();
            var isError = false;
            try
            {
                if (file.Name.EndsWith(ProjectInfoFileName))
                {
                    var folderName = file.Path.Substring(0, file.Path.LastIndexOf('\\'));
                    folderName = folderName.Substring(folderName.LastIndexOf('\\') + 1);
                    project._workingFolder = await App.TemporaryFolder.GetFolderAsync(folderName);
                }
                else
                {
                    await project.Init();
                    project._name = Path.GetFileNameWithoutExtension(file.Name);
                    await Project.UnArchive(file, project._workingFolder);
                    project._fileToken = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.Add(file);
                }

                var tcpInfoFile = await project._workingFolder.GetFileAsync(ProjectInfoFileName);
                var str = File.ReadAllText(tcpInfoFile.Path);
                var result = JsonConvert.DeserializeObject<TcpInfo.Project>(str);
                if (!result.version.HasValue)
                {
                    result.version = 1f;
                }
                if (_currentTcpFileVersion < result.version)
                {
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(new UILayer.Helpers.Messages.ProjectCorruptedMessage(OpenProjecFileErrorType.ProjectFileOldVersion));
                    isError = true;
                }
                else
                {
                    project = await LoadProjectBasicData(project, result, false);

                    var recordings = new List<Recording>();
                    if (result.recordings != null)
                    {
                        foreach (var rec in result.recordings)
                        {
                            try
                            {
                                var recording = await Recording.Create(project._workingFolder, rec);
                                recordings.Add(recording);
                            }
                            catch (Exception ex)
                            {
                                LogQueue.WriteToFile($"Exception in Project.OpenFromFile : {ex.Message}");
                            }
                        }
                    }

                    if (result.video_composition != null && result.video_composition.Count > 0)
                    {
                        var tcClipsCount = result.video_composition.Max(c => c.clip_index) + 1;
                        for (int clipIndex = 0; clipIndex < tcClipsCount; clipIndex++)
                        {
                            try
                            {
                                var composingItemsForCurrentClip = result.video_composition.Where(c => c.clip_index == clipIndex).ToList();
                                if (composingItemsForCurrentClip.Any())
                                {
                                    var clip = await TcMediaClip.Create(composingItemsForCurrentClip, recordings, project);
                                    if (clip != null)
                                    {
                                        project._tcMediaComposition.AddClip(clip);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogQueue.WriteToFile($"Exception in Project.OpenFromFile : {ex.Message}");
                            }
                        }
                    }

                    await project.UpdateFileInfo(file);
                    project._activeShot = project._shots.FirstOrDefault();

                    StorageFolder themeFolder = null;
                    var folderName = result.theme_folder ?? ProjectThemeFolderName;
                    try
                    {
                        themeFolder = await project._workingFolder.GetFolderAsync(ProjectThemeFolderName);
                    }
                    catch
                    {
                        themeFolder = null;
                    }

                    if (themeFolder == null)
                    {
                        var defaultThemeFolder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(defaultThemePath);
                        themeFolder = await project._workingFolder.CreateFolderAsync(ProjectThemeFolderName);
                        await CopyFolderAsync(defaultThemeFolder, themeFolder);
                    }

                    if (themeFolder != null)
                    {
                        project._theme = await LoadProjectAsTheme(themeFolder);
                        project._theme.ActiveShot = project._theme.Shots.FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.OpenFromFile : {ex.Message}");
                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(new UILayer.Helpers.Messages.ProjectCorruptedMessage(OpenProjecFileErrorType.ProjectFileCorrupted));
                isError = true;
            }
            finally
            {
                if (isError)
                {
                    await project.DeleteWorkFolder();
                    project = null;
                    var template = ThemeManager.Instance.DefaultTheme;
                    project = await OpenNewProject(template.theme_foler_path);
                }
                project._ioOperationLocker.Release();
            }

            Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticCommandProjectOpened);
            return project;
        }

        #endregion

        #region Public Methods

        public async Task<Shot> CreateShot()
        {     
            LogQueue.WriteToFile("Project CreateShot");

            var newShot = await Shot.CreateFrom(_theme.ActiveShot, this);
            await CreateShotWithoutUndoRedo(newShot);
            _undoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, new ShotMementoModel { Shot = newShot, Index = _shots.IndexOf(newShot) });

            return newShot;
        }

        public async Task<Shot> CreateShot(Shot shot)
        {
            LogQueue.WriteToFile("Project CreateShot from shot");

            var newShot = await Shot.CreateFrom(shot, this);
            await CreateShotWithoutUndoRedo(newShot);
            _undoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, new ShotMementoModel { Shot = newShot, Index = _shots.IndexOf(newShot) });

            return newShot;
        }

        public async Task CreateShotsFromDocumentPages(StorageFile pdfFile, PlaceholderLayerLayout placeholderLayer = null)
        {
            if (pdfFile == null)
            {
                return;
            }
            var pdfDoc = await Windows.Data.Pdf.PdfDocument.LoadFromFileAsync(pdfFile);
            var pdfPageRenderOptions = new Windows.Data.Pdf.PdfPageRenderOptions();

            UndoRedoBaseMementoModel undoRedoModel = null;
            StorageFile fileForActiveShot = null;
            uint index = 0;
            uint pageNumber = 0;
            var shot = ActiveShot;
            do
            {
                try
                {
                    var file = await _workingFolder.CreateFileAsync($"{Guid.NewGuid().ToString()}.jpg");
                    using (var page = pdfDoc.GetPage(pageNumber))
                    {
                        pdfPageRenderOptions.DestinationWidth = (uint)page.Size.Width;
                        pdfPageRenderOptions.DestinationHeight = (uint)page.Size.Height;

                        using (var randomStream = await file.OpenAsync(FileAccessMode.ReadWrite))
                        {
                            await page.RenderToStreamAsync(randomStream, pdfPageRenderOptions);
                            await randomStream.FlushAsync();
                        }
                    }
                    if (shot == null)
                    {
                        shot = await Shot.CreateFrom(ActiveShot, this);
                        await CreateShotWithoutUndoRedo(shot);
                        undoRedoModel = new UndoRedoBaseMementoModel(UndoRedoMementoType.Remove, new ShotMementoModel { Shot = shot, Index = _shots.IndexOf(shot) }, undoRedoModel);
                        await shot.PrepareForPowerPointSlide(file, placeholderLayer);
                    }
                    else
                    {
                        fileForActiveShot = file;
                    }
                    shot = null;
                }
                catch (Exception e)
                {
                    LogQueue.WriteToFile($"Exception in CreateShotsFromDocumentPages: {e.Message}");
                }
                
                ++index;
                pageNumber = pdfDoc.PageCount - index;
            } while (index < pdfDoc.PageCount);

            if (fileForActiveShot != null)
            {
                var layer = await ActiveShot.PrepareForPowerPointSlide(fileForActiveShot, placeholderLayer);
                if (placeholderLayer == null)
                {
                    undoRedoModel = new UndoRedoBaseMementoModel(UndoRedoMementoType.Remove, layer, undoRedoModel);
                }
                else
                {
                    undoRedoModel = new UndoRedoBaseMementoModel(UndoRedoMementoType.Change, new ReplaceLayersMementoModel(ActiveShot, placeholderLayer, layer), undoRedoModel);
                }
            }

            _undoRedoOriginator.PushNewUndoAction(undoRedoModel);
        }

        public async Task RemoveShot(Shot shot)
        {
            LogQueue.WriteToFile($"Project RemoveShot {shot.Id}");
            var index = _shots.IndexOf(shot);
            await RemoveShotWithoutUndoRedo(shot);
            _undoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Add, new ShotMementoModel { Shot = shot, Index = index });
        }

        public async Task<Shot> Duplicate(Shot model)
        {
            LogQueue.WriteToFile("Project Duplicate");
            if (ShotWillBeCreated != null)
            {
                await ShotWillBeCreated?.Invoke(model);
            }
            var duplicatedShot = await Shot.CreateFrom(model);
            duplicatedShot.Subscribe();
            var currentIndex = _shots.IndexOf(model);
            try
            {
                if (currentIndex < _shots.Count)
                {
                    _shots.Insert(currentIndex + 1, duplicatedShot);
                }
                else
                {
                    _shots.Add(duplicatedShot);
                }
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"exception in public async Task<Shot> Duplicate(Shot model) : {ex.Message}");
            }
            
            _hasUnsavedChanges = true;
            if (ShotWasCreated != null)
            {
                await ShotWasCreated?.Invoke(duplicatedShot);
            }
            _undoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, new ShotMementoModel { Shot = duplicatedShot, Index = _shots.IndexOf(duplicatedShot) });
            return duplicatedShot;
        }

        public async Task MoveShot(int from, int to)
        {
            await MoveShotWhitoutUndoRedo(from, to);
            _undoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new ShotMementoModel { Shot = _shots[to], Index = from });
        }
        public Shot NextShot()
        {
            var index = _shots.IndexOf(_activeShot);
            return ShotAt(++index);
        }
        public Shot PrevShot()
        {
            var index = _shots.IndexOf(_activeShot);
            return ShotAt(--index);

        }
        public Shot ShotAt(int index)
        {
            Shot shot = null;
            try
            {
                if (index >= 0 && index < _shots.Count)
                {
                    shot = _shots[index];
                }
            }
            catch
            {

            }
            return shot;
        }
        public void StartAutoSaveThread()
        {
            _autoSaveTimer = ThreadPoolTimer.CreatePeriodicTimer(AutoSaveTimer_Tick, AutoSavePeriod);
            _isAutoSaveEnabled = true;
        }

        public void StopAutoSaveThread()
        {
            _autoSaveTimer?.Cancel();
            _isAutoSaveEnabled = false;
        }

        public Task Import(StorageFile file)
        {
            return Task.CompletedTask;
        }

        public async Task Save()
        {
            LogQueue.WriteToFile("Project Save");
            ProjectWillBeSaved?.Invoke();

            var file = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(_fileToken);
            await Save(file);

            ProjectWasSaved?.Invoke();
        }

        public async Task FastSave()
        {
            if (App.Locator.AppState == ApplicationState.Recording || !_isAutoSaveEnabled)
            {
                return;
            }
            await _saveProjectLocker.WaitAsync();
            try
            {
                await UpdateUsedFiles();
                var json = Serialize();
                await CreateTcpInfo(json);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.FastSave(file) : {ex.Message}");
            }
            finally
            {
                _saveProjectLocker.Release();
            }
        }

        public async Task SaveAs(StorageFile file)
        {
            LogQueue.WriteToFile("Project SaveAs");
            ProjectWillBeSaved?.Invoke();

            _fileToken = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.Add(file);
            await Save(file);
            _name = Path.GetFileNameWithoutExtension(file.Name);

            ProjectWasSaved?.Invoke();
        }

        public async Task<StorageFile> CopyFileToLocalSandbox(StorageFile sourceFile)
        {
            StorageFile localFile = null;
            var extension = Path.GetExtension(sourceFile.Name).ToLower();
            if (FileToAppHelper.ImageExtensions.Contains(extension))
            {
                using (var sourceStream = await sourceFile.OpenAsync(FileAccessMode.Read))
                {
                    string destinationFileName = $"{Guid.NewGuid().ToString()}.png";
                    localFile = await FileToAppHelper.SaveImageStreamToPngFile(sourceStream, MediaMixerService.VideoSize, _workingFolder, destinationFileName);
                }
            }
            else if (FileToAppHelper.VideoExtensions.Contains(extension))
            {
                if (sourceFile.Path.Contains(ApplicationData.Current.LocalFolder.Path))
                {
                    try
                    {
                        localFile = await _workingFolder.GetFileAsync(sourceFile.Name);
                    }
                    catch
                    {
                        localFile = null;
                    }

                    if (localFile == null)
                    {
                        localFile = await sourceFile.CopyAsync(_workingFolder, sourceFile.Name);
                    }
                }
                else
                {
                    localFile = await sourceFile.CopyAsync(_workingFolder, $"{Guid.NewGuid().ToString()}.{Path.GetExtension(sourceFile.Name)}");
                }
            }
            else if (FileToAppHelper.DocumentExtensions.Contains(extension)
                || FileToAppHelper.TextExtensions.Contains(extension))
            {
                localFile = await sourceFile.CopyAsync(_workingFolder, $"{Guid.NewGuid().ToString()}.{Path.GetExtension(sourceFile.Name)}");
            }

            return localFile;
        }

        public void ActivateNewShot(Shot shot)
        {
            if (_shots.Contains(shot))
            {
                ActiveShot = shot;
            }
        }

        public async Task Clean()
        {
            LogQueue.WriteToFile("Project Clean");
            StopAutoSaveThread();
            UndoRedoOriginator.UndoEvent -= UndoRedoOriginator_UndoEvent;
            UndoRedoOriginator.RedoEvent -= UndoRedoOriginator_RedoEvent;
            _tcMediaComposition.Clean();
            foreach (var shot in _shots)
            {
                shot.Clean();
            }
            
            _shots.Clear();
            _bufferedLayer = null;
            if (_theme != null)
            {
                await _theme.Clean();
            }
            try
            {
                await _ioOperationLocker.WaitAsync();
                await DeleteWorkFolder();
                _ioOperationLocker.Release();
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.Clean {ex.Message}");
            }
        }

        public void RaiseEmptyEvent()
        {
            RecordingsEmptyEvent?.Invoke();
        }

        public void RaiseRestoringEvent(Shot shot)
        {
            RecordingIsRestoredEvent?.Invoke(shot);
        }

        public void CopyLayer(LayerLayout layer)
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session.ShotManager.IsShowShotInProcess) return;
            if (layer != null)
            {
                _bufferedLayer = layer.Clone();
            }
        }

        public async void CutLayer(LayerLayout layer)
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session.ShotManager.IsShowShotInProcess) return;
            if (layer != null)
            {
                _bufferedLayer = layer;
                await ActiveShot.RemoveLayer(layer);
            }
        }

        public void PasteLayer(LayerLayout layer)
        {
            var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (session.ShotManager.IsShowShotInProcess) return;
            if (layer != null)
            {
                ActiveShot.InsertLayer(layer);
            }
        }

        public async Task WaitIoOperationCompleted()
        {
            await _ioOperationLocker.WaitAsync();
            _ioOperationLocker.Release();
        }

        #endregion

        #region Private Methods
        private async Task Init()
        {
            _workingFolder = await App.TemporaryFolder.CreateFolderAsync(Guid.NewGuid().ToString());
        }

        private async Task PrepopulateShots()
        {
            var prepopulatingShot = _theme.Shots.FirstOrDefault();
            var shot = await Shot.CreateFrom(prepopulatingShot, this);
            shot.Subscribe();
            _shots.Add(shot);
            _activeShot = _shots.FirstOrDefault();
        }

        private string Serialize()
        {
            TcpInfo.Project metaData = new TcpInfo.Project();
            metaData.version = _currentTcpFileVersion;
            metaData.canvas_size = new TcpInfo.Size { width = MediaMixerService.VideoSize.Width, height = MediaMixerService.VideoSize.Height };
            metaData.name = Name;
            metaData.description = Description;
            metaData.theme_folder = _theme?.WorkingFolder?.Name;
            metaData.teleprompter.text_file = _teleprompterFile?.Name;
            for (int i = 0; i < _shots.Count; i++)
            {
                try
                {
                    var shot = _shots[i];
                    var greenscreenMetaData = shot.GreenScreenSettings.GetMetaData();
                    greenscreenMetaData.scene_index = i;
                    metaData.cameras.Add(new TcpInfo.Camera()
                    {
                        name = MediaMixer.SelectedCamera.Id,
                        scene_index = i,
                        mirroring = true
                    });

                    metaData.microphones.Add(new TcpInfo.Microphone()
                    {
                        name = MediaMixer.SelectedMicrophone.Id,
                        scene_index = i,
                        volume = MediaMixer.MicrophoneVolumeLevel
                    });

                    metaData.segmentations.Add(greenscreenMetaData);

                    foreach (var layer in shot.Layers)
                    {
                        try
                        {
                            var data = layer.GetMetaData();
                            data.scene_index = i;
                            metaData.elements.Add(data);
                        }
                        catch (Exception ex)
                        {
                            LogQueue.WriteToFile($"Exception in Project.Serialize -> layer cycle : {ex.Message}");
                        }
                    }

                    metaData.background_colors.Add(HexToColorConverter.ConvertColorToHex(shot.BackgroundColor));
                }
                catch (Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in Project.Serialize -> shot cycle : {ex.Message}");
                }
            }

            foreach (var mediaClip in TcMediaComposition.MediaClips)
            {
                for (int k = 0; k < mediaClip.Takes.Count; k++)
                {
                    try
                    {
                        var take = mediaClip.Takes[k];
                        for (int j = 0; j < take.VideoFragments.Count; j++)
                        {
                            try
                            {
                                var item = take.VideoFragments[j];
                                var data = item.GetMetaData();
                                if (_shots.Contains(take.Shot))
                                {
                                    data.scene_index = _shots.IndexOf(take.Shot);
                                }
                                data.take = k;
                                data.active = (mediaClip.Takes.IndexOf(mediaClip.ActiveTake) == k) ? 1 : 0;
                                data.clip_index = TcMediaComposition.IndexOf(mediaClip);
                                if (metaData.video_composition == null)
                                {
                                    metaData.video_composition = new List<TcpInfo.CompositionItem>();
                                }
                                if (mediaClip.Transition.Kind != Transition.Type.None)
                                {
                                    if (j == take.VideoFragments.Count - 1)
                                    {
                                        data.out_transition = new TcpInfo.Transition();
                                        data.out_transition.type = Transition.ConvertToString(mediaClip.Transition.Kind);
                                        data.out_transition.duration = 0.5;
                                    }
                                }
                                metaData.video_composition.Add(data);
                                if (metaData.recordings.FirstOrDefault(r => r.id == item.Recording.Id) == null)
                                {
                                    var rec = item.Recording.GetMetaData();
                                    if (metaData.recordings == null)
                                    {
                                        metaData.recordings = new List<TcpInfo.Recording>();
                                    }
                                    metaData.recordings.Add(rec);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogQueue.WriteToFile($"Exception in Project.Serialize -> VideoFragments cycle : {ex.Message}");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Project.Serialize -> take cycle : {ex.Message}");
                    }
                }
            }


            return JsonConvert.SerializeObject(metaData,
                            Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
        }

        private async Task SaveTeleprompterToFile()
        {
            StringBuilder teleprompterBuilder = new StringBuilder();
            try
            {
                for (int i = 0; i < _shots.Count; i++)
                {
                    teleprompterBuilder.Append($"{_shots[i].TeleprompterText}\n---- Scene {i + 1} ----\n");
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.SaveTeleprompterToFile -> teleprompter cycle : {ex.Message}");
            }

            _teleprompterFile = await _workingFolder.CreateFileAsync($"{Guid.NewGuid().ToString()}.tpx", CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(_teleprompterFile, teleprompterBuilder.ToString());
        }

        private static async Task<string[]> ReadTeleprompterFromFile(Project project, string filename)
        {
            string[] result = null;
            project._teleprompterFile = await project._workingFolder.GetFileAsync(filename);
            if (null != project._teleprompterFile)
            {
                Regex teleprompterRegex = new Regex(TeleprompterSplitPattern);
                string teleprompterText = await FileIO.ReadTextAsync(project._teleprompterFile);
                result = teleprompterRegex.Split(teleprompterText);
            }

            return result;
        }

        private async Task Save(StorageFile file)
        {
            await _saveProjectLocker.WaitAsync();
            try
            {
                await UpdateUsedFiles();
                await SaveTeleprompterToFile();
                var json = Serialize();
                await CleanNotUsedFiles();
                await CreateTcpInfo(json);
                var zip = await Archive();
                await UpdateFileInfo(zip);
                await FileToAppHelper.MoveFile(zip, file);
                _hasUnsavedChanges = false;
                GoogleAnalyticsManager.TrySend(AnalyticCommandType.AnalyticCommandProjectSaved);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.Save(file) : {ex.Message}");
            }
            finally
            {
                _saveProjectLocker.Release();
            }
        }

        private async Task CreateTcpInfo(string json)
        {
            var file = await _workingFolder.CreateFileAsync(ProjectInfoFileName, CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(file, json);
        }

        private async Task UpdateUsedFiles()
        {
            foreach (var shot in _shots)
            {
                foreach (var layer in shot.Layers)
                {
                    try
                    {
                        if (layer is TextLayerLayout)
                        {
                            await (layer as TextLayerLayout).SaveData();
                        }
                        if (layer is ShapeLayerLayout)
                        {
                            await (layer as ShapeLayerLayout).SaveData();
                        }
                    }
                    catch(Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Project.UpdateUsedFiles() : {ex.Message}");
                    }
                }
            }
        }

        private async Task UpdateFileInfo(StorageFile file)
        {
            try
            {
                Windows.Storage.FileProperties.BasicProperties basicProperties = await file.GetBasicPropertiesAsync();
                _lastModifyTime = basicProperties.DateModified.DateTime;
                _creationTime = basicProperties.ItemDate.DateTime;
                _size = basicProperties.Size;
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.UpdateFileInfo() : {ex.Message}");
            }
        }

        private async Task CleanNotUsedFiles()
        {
            List<StorageFile> usedFiles = new List<StorageFile>();
            if (_teleprompterFile != null)
            {
                usedFiles.Add(_teleprompterFile);
            }
            foreach (var shot in _shots)
            {
                foreach (var layer in shot.Layers)
                {
                    if (layer.ThumbnailFile != null)
                    {
                        usedFiles.Add(layer.ThumbnailFile);
                    }

                    if (layer is ImageLayerLayout imageLayer)
                    {
                        if(imageLayer.File != null)
                        {
                            usedFiles.Add((layer as ImageLayerLayout).File);
                        }
                    }
                    else if (layer is VideoLayerLayout videoLayer)
                    {
                        if (videoLayer.File != null)
                        {
                            usedFiles.Add((layer as VideoLayerLayout).File);
                        }
                        if (videoLayer.HighDefinitionThumbnailFile != null)
                        {
                            usedFiles.Add(videoLayer.HighDefinitionThumbnailFile);
                        }
                    }
                    else if (layer is DocumentLayerLayout documentLayer)
                    {
                        if (documentLayer.File != null)
                        {
                            usedFiles.Add(documentLayer.File);
                        }
                    }
                    else if (layer is TextLayerLayout textLayer)
                    {
                        if (textLayer.File != null)
                        {
                            usedFiles.Add(textLayer.File);
                        }
                        if (textLayer.PlaceholderFile != null)
                        {
                            usedFiles.Add(textLayer.PlaceholderFile);
                        }
                    }
                    else if (layer is ShapeLayerLayout shapeLayer)
                    {
                        if (shapeLayer.File != null)
                        {
                            usedFiles.Add(shapeLayer.File);
                        } 
                    }
                }
            }
            foreach (var mediaClip in TcMediaComposition.MediaClips)
            {
                foreach (var take in mediaClip.Takes)
                {
                    foreach (var fragment in take.VideoFragments)
                    {
                        if (fragment?.Recording?.ActionsXml != null)
                        {
                            usedFiles.Add(fragment.Recording.ActionsXml);
                        }
                        if (fragment?.Recording?.File != null)
                        {
                            usedFiles.Add(fragment.Recording.File);
                        }
                        if (fragment?.Recording?.Resources != null)
                        {
                            foreach (var resource in fragment.Recording.Resources)
                            {
                                usedFiles.Add(resource);
                            }
                        }
                    }
                }
            }
            usedFiles = usedFiles.Distinct().ToList();
            try
            {
                var filesInFolder = await _workingFolder.GetFilesAsync();
                foreach (var file in filesInFolder)
                {
                    try
                    {
                        if (usedFiles.FirstOrDefault(f => f.Name == file.Name) == null)
                        {
                            await file.DeleteAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Project.CleanNotUsedFiles : {ex.Message}");
                    }
                }
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.CleanNotUsedFiles : {ex.Message}");
            }
        }

        private async Task<StorageFile> Archive()
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var projectFileName = $"project{FileToAppHelper.TouchcastProjectExtension}";
                    try
                    {
                        var file = await App.TemporaryFolder.TryGetItemAsync(projectFileName);
                        if (file != null)
                        {
                            await file.DeleteAsync();
                        }
                    }
                    catch(Exception e)
                    {
                        LogQueue.WriteToFile(e.Message);
                    }

                    ZipFile.CreateFromDirectory(_workingFolder.Path,
                        $"{App.TemporaryFolder.Path}/project{FileToAppHelper.TouchcastProjectExtension}", CompressionLevel.NoCompression, false);
                    var zip = await App.TemporaryFolder.GetFileAsync(projectFileName);
                    return zip;
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in Project.Archive : {ex.Message}");
                }
                return null;
            });
        }

        private static async Task UnArchive(StorageFile project, StorageFolder workingFolder)
        {
            try
            {
                var file = await project.CopyAsync(ApplicationData.Current.LocalCacheFolder);
                try
                {  
                    ZipFile.ExtractToDirectory(file.Path, workingFolder.Path);
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in Project.UnArchive : {ex.Message}");
                }
                finally
                {
                    await file.DeleteAsync();
                }
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Project.UnArchive : {ex.Message}");
            }
        }

        private async Task DeleteWorkFolder()
        {
            var _hasError = false;

            var tcpInfoFile = await _workingFolder.TryGetItemAsync(ProjectInfoFileName);
            if (tcpInfoFile != null && tcpInfoFile.IsOfType(StorageItemTypes.File))
            {
                try
                {
                    await tcpInfoFile.DeleteAsync();
                }
                catch (Exception ex)
                {
                    _hasError = true;
                    LogQueue.WriteToFile($"Exception in Project.DeleteWorkFolder {ProjectInfoFileName} {ex.Message}");
                }
            }

            if (!_hasError)
            {
                try
                {
                    await _workingFolder.DeleteAsync();
                }
                catch (Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in Project.DeleteWorkFolder WorkingFolder {ex.Message}");
                    var cacheItems = await _workingFolder.GetItemsAsync();
                    foreach (var item in cacheItems)
                    {
                        try
                        {
                            await item.DeleteAsync();
                        }
                        catch (Exception ex1)
                        {
                            _hasError = true;
                            LogQueue.WriteToFile($"Exception in Project.DeleteWorkFolder {item.Name} {ex1.Message}");
                        }
                    }
                }
            }
        }

        private async Task CopyFileToFolder(StorageFolder folder, StorageFile file)
        {
            if (file != null)
            {
                try
                {
                    await file.CopyAsync(folder, file.Name, NameCollisionOption.FailIfExists);
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in Project.CopyFileToFolder : {ex.Message}");
                }
            }
        }

        private async Task RestoreToPreviousShotPosition(Shot shot, int index)
        {
            if (ShotWillBeCreated != null)
            {
                await ShotWillBeCreated.Invoke(null);
            }
            shot.Subscribe();
            _shots.Insert(index, shot);
            _hasUnsavedChanges = true;
            if (ShotWasCreated != null)
            {
                await ShotWasCreated.Invoke(shot);
            }
        }

        private async Task CreateShotWithoutUndoRedo(Shot shot)
        {
            if (ShotWillBeCreated != null)
            {
                await ShotWillBeCreated.Invoke(null);
            }

            shot.Subscribe();
            _shots.Insert(Shots.IndexOf(ActiveShot) + 1, shot);
            _hasUnsavedChanges = true;

            if (ShotWasCreated != null)
            {
                await ShotWasCreated.Invoke(shot);
            }
        }

        private async Task MoveShotWhitoutUndoRedo(int from, int to)
        {
            var shot = _shots[from];
            LogQueue.WriteToFile($"Project MoveShot {shot.Id} from {from} to {to}");
            if (ShotWillBeMoved != null)
            {
                await ShotWillBeMoved.Invoke(shot);
            }

            _shots.Remove(shot);
            _shots.Insert(to, shot);
            _hasUnsavedChanges = true;

            if (ShotWasMoved != null)
            {
                await ShotWasMoved.Invoke(shot);
            }
        }

        private async Task RemoveShotWithoutUndoRedo(Shot shot)
        {
            if (Shots.Count <= 1)
            {
                return;
            }
            if (ShotWillBeRemoved != null)
            {
                await ShotWillBeRemoved.Invoke(shot);
            }
            var currentActiveShot = ActiveShot;
            Shot nextActiveShot = null;
            if (shot == ActiveShot)
            {
                var currentActiveShotIndex = _shots.IndexOf(currentActiveShot);
                if (currentActiveShot.Id == _shots.Last().Id)
                {
                    nextActiveShot = _shots[--currentActiveShotIndex];
                }
                else
                {
                    nextActiveShot = _shots[++currentActiveShotIndex];
                }

                if (nextActiveShot != null)
                {
                    var session = SimpleIoc.Default.GetInstance<AuthoringSession>();
                    await session.ShotManager.ShowShotWithoutUndoRedo(nextActiveShot);
                }
            }
            _shots.Remove(shot);
            shot.Clean();

            _activeShot = nextActiveShot ?? currentActiveShot;

            _hasUnsavedChanges = true;
            if (ShotWasRemoved != null)
            {
                await ShotWasRemoved.Invoke(shot);
            }
        }

        private async void AutoSaveTimer_Tick(ThreadPoolTimer timer)
        {
            await FastSave();
        }

        #region UndoRedo
        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_UndoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_RedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> ProcessUndoRedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            if (mementoModel.MementoObject is ShotMementoModel model)
            {
                switch (mementoModel.ActionType)
                {
                    case UndoRedoMementoType.Add:
                    {
                         await RestoreToPreviousShotPosition(model.Shot, model.Index);
                    }
                    break;
                    case UndoRedoMementoType.Remove:
                    {
                         await RemoveShotWithoutUndoRedo(model.Shot);
                    }
                    break;
                    case UndoRedoMementoType.Change:
                    {
                        int from = _shots.IndexOf(model.Shot);
                        await MoveShotWhitoutUndoRedo(from, model.Index);
                        model.Index = from;
                    }
                    break;
                }
                return mementoModel;
            }
            return null;
        }
        #endregion

        #endregion
    }
}
