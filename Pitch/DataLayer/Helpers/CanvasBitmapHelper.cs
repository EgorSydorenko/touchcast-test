﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pitch.Helpers
{
    public class CanvasBitmapHelper
    {
        public  async static Task<CanvasBitmap> FromFile(Windows.Storage.StorageFile file)
        {
            CanvasBitmap bmp = null;
            try
            {
                if (file != null)
                {
                    if (TouchCastComposingEngine.ComposingContext.Shared().PreviewingDevice != null)
                    {

                        using (var stream = await file.OpenReadAsync())
                        {
                            using (var canvasBitmap = await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(TouchCastComposingEngine.ComposingContext.Shared().PreviewingDevice, stream))
                            {
                                var composingRenderTarget = new Microsoft.Graphics.Canvas.CanvasRenderTarget(TouchCastComposingEngine.ComposingContext.Shared().PreviewingDevice, (float)canvasBitmap.Size.Width, (float)canvasBitmap.Size.Height, 96);
                                using (var ds = composingRenderTarget.CreateDrawingSession())
                                {
                                    ds.DrawImage(canvasBitmap);
                                }
                                bmp = composingRenderTarget;
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Pitch.Helpers.Logger.LogQueue.WriteToFile(e.Message);
            }
            return bmp;
        }
    }
}
