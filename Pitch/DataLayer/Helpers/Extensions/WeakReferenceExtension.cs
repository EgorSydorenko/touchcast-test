﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pitch.DataLayer.Helpers.Extensions
{
    public static class WeakReferenceExtension
    {
        public static T TryGetObject<T>(this WeakReference<T> curerntObject)
            where T : class
        {
            try
            {
                T obj;
                if (curerntObject.TryGetTarget(out obj))
                {
                    return obj;
                }
            }
            catch(Exception ex)
            {

            }
            return default(T);
        }
    }
}
