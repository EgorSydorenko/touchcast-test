﻿using System.Text.RegularExpressions;

namespace Pitch.Helpers.Extensions
{
    public static class ExtensionMethods
    {
        public static string GetCdataContent(this string str)
        {
            if (str.Contains("CDATA"))
            {
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex(@"\<\!\[CDATA\[(?<text>[^\]]*)\]\]\>", options);
                Match match = regex.Match(str);
                return match.Groups["text"].Value;
            }
            return str;
        }
    }
}
