﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pitch.UILayer.Authoring.Views.LayoutViews;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace Pitch.Helpers
{
    public sealed class HelperLinesGrid : Grid
    {
        private enum LineAlignment
        {
            Left,
            Top,
            Right,
            Bottom,
            CenterVertically,
            CenterHorizontally
        }

        public Grid LayoutApps { get; set; }

        ExtendedLine LeftLine = new ExtendedLine();
        ExtendedLine RightLine = new ExtendedLine();
        ExtendedLine TopLine = new ExtendedLine();
        ExtendedLine BottomLine = new ExtendedLine();
        ExtendedLine CenterVerticalLine = new ExtendedLine();
        ExtendedLine CenterHorizontalLine = new ExtendedLine();

        IDictionary<LineAlignment, ExtendedLine> verticalLinesGroup;
        IDictionary<LineAlignment, ExtendedLine> horizontalLinesGroup;

        private int _showLineDistanse = 2;
        private int _magneticDistanse = 3;

        public HelperLinesGrid()
        {
            LeftLine.AddLineToView(this);
            Canvas.SetZIndex(LeftLine, 10000);
            RightLine.AddLineToView(this);
            Canvas.SetZIndex(RightLine, 10000);
            TopLine.AddLineToView(this);
            Canvas.SetZIndex(TopLine, 10000);
            BottomLine.AddLineToView(this);
            Canvas.SetZIndex(BottomLine, 10000);
            CenterVerticalLine.AddLineToView(this);
            Canvas.SetZIndex(CenterVerticalLine, 10000);
            CenterHorizontalLine.AddLineToView(this);
            Canvas.SetZIndex(CenterHorizontalLine, 10000);

            verticalLinesGroup = new Dictionary<LineAlignment, ExtendedLine>()
            {
                { LineAlignment.Left, LeftLine },
                { LineAlignment.Right, RightLine },
                { LineAlignment.CenterVertically, CenterVerticalLine }
            };
            horizontalLinesGroup = new Dictionary<LineAlignment, ExtendedLine>()
            {
                { LineAlignment.Top, TopLine },
                { LineAlignment.Bottom, BottomLine },
                { LineAlignment.CenterHorizontally, CenterHorizontalLine },
            };
        }

        public Rect CheckGuideLines(BaseLayoutView layoutView, Rect futurePosition, SenderType senderType)
        {
            //Reset
            ClearLineTempValues();
            HideLines();

            //Get Lines And Distance
            ExtendedLine Vertical = null;
            ExtendedLine Horizontal = null;
            //Rect rectCurrent = GetLayoutRect(layoutView);
            foreach (var child in LayoutApps.Children)
            {
                if (child == layoutView) continue;
                
                var otherLayotView = child as BaseLayoutView;
                var rectOther = GetLayoutRect(otherLayotView);

                if (rectOther.IsEmpty) continue;

                GetNearestLine(verticalLinesGroup, ref futurePosition, rectOther, ref Vertical, senderType);
                GetNearestLine(horizontalLinesGroup, ref futurePosition, rectOther, ref Horizontal, senderType);
            }
          
            //Show Lines
            Vertical?.ShowLine();
            Horizontal?.ShowLine();

            return futurePosition;
        }

        private bool CheckHelperLines(LineAlignment lineAlignment, Rect currentRect, Rect otherRect, ExtendedLine outLine)
        {
            var result = false;
            switch (lineAlignment)
            {
                case LineAlignment.Left:
                    var sameLeft = Math.Abs((int)currentRect.X - (int)otherRect.X) < _showLineDistanse;
                    var sameLeftRight = Math.Abs((int)(currentRect.X) - (int)(otherRect.X + otherRect.Width)) < _showLineDistanse;

                    if (sameLeft || sameLeftRight)
                    {
                        result = true;
                        SetVerticalLineCoords(lineAlignment, currentRect, otherRect, outLine);
                    }
                    break;
                case LineAlignment.Right:
                    var sameRight = Math.Abs((int)(currentRect.X + currentRect.Width) - (int)(otherRect.X + otherRect.Width)) < _showLineDistanse;
                    var sameRightLeft = Math.Abs((int)(currentRect.X + currentRect.Width) - (int)otherRect.X) < _showLineDistanse;

                    if (sameRight || sameRightLeft)
                    {
                        result = true;
                        SetVerticalLineCoords(lineAlignment, currentRect, otherRect, outLine);
                    }
                    break;
                case LineAlignment.CenterVertically:
                    var sameVerticalCenter = Math.Abs((int)(currentRect.X + currentRect.Width / 2) - (int)(otherRect.X + otherRect.Width / 2)) < _showLineDistanse;
                    if (sameVerticalCenter)
                    {
                        result = true;
                        SetVerticalLineCoords(lineAlignment, currentRect, otherRect, outLine);
                    }
                    break;
                case LineAlignment.Top:
                    var sameTop = Math.Abs((int)currentRect.Y - (int)otherRect.Y) < _showLineDistanse;
                    var sameTopBottom = Math.Abs((int)(currentRect.Y) - (int)(otherRect.Y + otherRect.Height)) < _showLineDistanse;

                    if (sameTop || sameTopBottom)
                    {
                        result = true;
                        SetHorizontalLineCoords(lineAlignment, currentRect, otherRect, outLine);
                    }
                    break;
                case LineAlignment.Bottom:
                    var sameBottom = Math.Abs((int)(currentRect.Y + currentRect.Height) - (int)(otherRect.Y + otherRect.Height)) < _showLineDistanse;
                    var sameBottomTop = Math.Abs((int)(currentRect.Y + currentRect.Height) - (int)otherRect.Y) < _showLineDistanse;

                    if (sameBottom || sameBottomTop)
                    {
                        result = true;
                        SetHorizontalLineCoords(lineAlignment, currentRect, otherRect, outLine);
                    }
                    break;
                case LineAlignment.CenterHorizontally:
                    var sameHorizontalCenter = Math.Abs((int)(currentRect.Y + currentRect.Height / 2) - (int)(otherRect.Y + otherRect.Height / 2)) < _showLineDistanse;
                    if (sameHorizontalCenter)
                    {
                        result = true;
                        SetHorizontalLineCoords(lineAlignment, currentRect, otherRect, outLine);
                    }
                    break;
                default:
                    break;
            }

            return result;
        }
        
        public void HideLines()
        {
            LeftLine.HideLine();
            RightLine.HideLine();
            TopLine.HideLine();
            BottomLine.HideLine();
            CenterVerticalLine.HideLine();
            CenterHorizontalLine.HideLine();
        }

        private void ClearLineTempValues()
        {
            RightLine.ClearValues();
            LeftLine.ClearValues();
            TopLine.ClearValues();
            BottomLine.ClearValues();
            CenterVerticalLine.ClearValues();
            CenterHorizontalLine.ClearValues();
        }

        private Rect GetLayoutRect(BaseLayoutView layoutView)
        {
            Rect result = Rect.Empty;
            if (layoutView != null)
            {
                if (layoutView is LineLayerLayoutView)
                {
                    LineLayerLayoutView lineLayoutView = layoutView as LineLayerLayoutView;
                    result = new Rect(lineLayoutView.ViewModel.LineFirstPoint, lineLayoutView.ViewModel.LineSecondPoint);
                }
                else
                {
                    GeneralTransform transform = layoutView.TransformToVisual(LayoutApps);
                    Point ElementPosition = transform.TransformPoint(new Point(0, 0));
                    result = new Rect(ElementPosition.X, ElementPosition.Y, layoutView.ActualWidth, layoutView.ActualHeight);
                }
            }

            return result;
        }

        private void SetVerticalLineCoords(LineAlignment lineAlignment, Rect currentRect, Rect otherRect, ExtendedLine line)
        {
            line.MinY = Math.Min(currentRect.Y, otherRect.Y);
            line.MaxY = Math.Max(currentRect.Y, otherRect.Y);

            double h = currentRect.Y > otherRect.Y ? currentRect.Height : otherRect.Height;
            double H = h + line.MaxY + 20;

            line.Y1 = line.MinY - 10;
            line.Y2 = H;

            switch (lineAlignment)
            {
                case LineAlignment.Left:
                    var sameLeft = Math.Abs((int)currentRect.X - (int)otherRect.X) < _showLineDistanse;
                    line.X1 = sameLeft ? otherRect.X : otherRect.X + otherRect.Width;
                    line.X2 = sameLeft ? otherRect.X : otherRect.X + otherRect.Width;
                    break;
                case LineAlignment.Right:
                    var sameRight = Math.Abs((int)(currentRect.X + currentRect.Width) - (int)(otherRect.X + otherRect.Width)) < _showLineDistanse;
                    line.X1 = sameRight ? otherRect.X + otherRect.Width : otherRect.X;
                    line.X2 = sameRight ? otherRect.X + otherRect.Width : otherRect.X;
                    break;
                case LineAlignment.CenterVertically:
                    line.X1 = otherRect.X + otherRect.Width / 2;
                    line.X2 = otherRect.X + otherRect.Width / 2;
                    break;
                default:
                    break;
            }
        }

        private void SetHorizontalLineCoords(LineAlignment lineAlignment, Rect currentRect, Rect otherRect, ExtendedLine line)
        {
            line.MinX = Math.Min(currentRect.X, otherRect.X);
            line.MaxX = Math.Max(currentRect.X, otherRect.X);

            double w = currentRect.X > otherRect.X ? currentRect.Width : otherRect.Width;
            double W = w + line.MaxX + 20;

            line.X1 = line.MinX - 10;
            line.X2 = W;

            switch (lineAlignment)
            {
                case LineAlignment.Top:
                    var sameTop = Math.Abs((int)currentRect.Y - (int)otherRect.Y) < _showLineDistanse;
                    line.Y1 = sameTop ? otherRect.Y : otherRect.Y + otherRect.Height;
                    line.Y2 = sameTop ? otherRect.Y : otherRect.Y + otherRect.Height;
                    break;
                case LineAlignment.Bottom:
                    var sameBottom = Math.Abs((int)(currentRect.Y + currentRect.Height) - (int)(otherRect.Y + otherRect.Height)) < _showLineDistanse;
                    line.Y1 = sameBottom ? otherRect.Y + otherRect.Height : otherRect.Y;
                    line.Y2 = sameBottom ? otherRect.Y + otherRect.Height : otherRect.Y;
                    break;
                case LineAlignment.CenterHorizontally:
                    line.Y1 = otherRect.Y + otherRect.Height / 2;
                    line.Y2 = otherRect.Y + otherRect.Height / 2;
                    break;
                default:
                    break;
            }
        }

        private double GetDistance(LineAlignment lineAlignment, Rect currentRect, Rect otherRect)
        {
            double result = Double.NaN;
            switch (lineAlignment)
            {
                case LineAlignment.Left:
                    var isLeft = Math.Abs((int)currentRect.X - (int)otherRect.X) < _showLineDistanse;                    
                    result = isLeft ? (currentRect.X - otherRect.X) 
                        : (currentRect.X - (otherRect.X + otherRect.Width));
                    break;
                case LineAlignment.Top:
                    var isTop = Math.Abs((int)currentRect.Y - (int)otherRect.Y) < _showLineDistanse;
                    result = isTop ? currentRect.Y - otherRect.Y
                        : currentRect.Y - (otherRect.Y + otherRect.Height);
                    break;
                case LineAlignment.Right:
                    var isRight = Math.Abs((int)(currentRect.X + currentRect.Width) - (int)(otherRect.X + otherRect.Width)) < _showLineDistanse;
                    result = isRight ? (currentRect.X + currentRect.Width) - (otherRect.X + otherRect.Width)
                        : currentRect.X + (currentRect.Width - otherRect.X);
                    break;
                case LineAlignment.Bottom:
                    var isBottom = Math.Abs((int)(currentRect.Y + currentRect.Height) - (int)(otherRect.Y + otherRect.Height)) < _showLineDistanse;
                    result = isBottom ? (currentRect.Y + currentRect.Height) - (otherRect.Y + otherRect.Height)
                        : currentRect.Y + (currentRect.Height - otherRect.Y);
                    break;
                case LineAlignment.CenterVertically:
                    result = (currentRect.X + currentRect.Width / 2) - (otherRect.X + otherRect.Width / 2);
                    break;
                case LineAlignment.CenterHorizontally:
                    result = (currentRect.Y + currentRect.Height / 2) - (otherRect.Y + otherRect.Height / 2);
                    break;
                default:
                    break;
            }

            return result;
        }

        private void GetNearestLine(IDictionary<LineAlignment, ExtendedLine> lineGroup, ref Rect rectCurrent, Rect rectOther, ref ExtendedLine line, SenderType senderType)
        {
            var minDelta = Double.NaN;
            var distance = 0.0;

            foreach (var item in lineGroup)
            {
                if (CheckHelperLines(item.Key, rectCurrent, rectOther, item.Value))
                {
                    distance = GetDistance(item.Key, rectCurrent, rectOther);
                    if (Double.IsNaN(minDelta))
                    {
                        minDelta = distance;
                        line = item.Value;
                    }
                    else if (Math.Abs(distance) < Math.Abs(minDelta))
                    {
                        minDelta = distance;
                        line = item.Value;
                    }
                }
                if (senderType == SenderType.Mouse)
                {
                    ProcessMagnetic(item.Key, ref rectCurrent, rectOther);
                }
            }
        }

        private void ProcessMagnetic(LineAlignment aligment, ref Rect rectCurrent, Rect rectOther)
        {
            if (aligment == LineAlignment.Left)
            {
                if (Math.Abs(rectCurrent.X - rectOther.X) < _magneticDistanse)
                {
                    rectCurrent.X = rectOther.X;
                }
                else if (Math.Abs(rectCurrent.X - rectOther.X - rectOther.Width) < _magneticDistanse)
                {
                    rectCurrent.X = rectOther.X + rectOther.Width;
                }
            }
            else if (aligment == LineAlignment.Right)
            {
                if (Math.Abs(rectCurrent.X + rectCurrent.Width - rectOther.X - rectOther.Width) < _magneticDistanse)
                {
                    rectCurrent.X = rectOther.X + rectOther.Width - rectCurrent.Width;
                }
                else if (Math.Abs(rectCurrent.X + rectCurrent.Width - rectOther.X) < _magneticDistanse)
                {
                    rectCurrent.X = rectOther.X - rectCurrent.Width;
                }
            }
            else if (aligment == LineAlignment.Bottom)
            {
                if (Math.Abs(rectCurrent.Y + rectCurrent.Height - rectOther.Y - rectOther.Height) < _magneticDistanse)
                {
                    rectCurrent.Y = rectOther.Y + rectOther.Height - rectCurrent.Height;
                }
                else if (Math.Abs(rectCurrent.Y + rectCurrent.Height - rectOther.Y) < _magneticDistanse)
                {
                    rectCurrent.Y = rectOther.Y - rectCurrent.Height;
                }
            }
            else if (aligment == LineAlignment.Top)
            {
                if (Math.Abs(rectCurrent.Y - rectOther.Y) < _magneticDistanse)
                {
                    rectCurrent.Y = rectOther.Y;
                }
                else if (Math.Abs(rectCurrent.Y - rectOther.Y - rectOther.Height) < _magneticDistanse)
                {
                    rectCurrent.Y = rectOther.Y + rectOther.Height;
                }
            }
            else if (aligment == LineAlignment.CenterHorizontally)
            {
                if (Math.Abs((rectCurrent.Y + rectCurrent.Height / 2) - (rectOther.Y + rectOther.Height / 2)) < _magneticDistanse)
                {
                    rectCurrent.Y = rectOther.Y + ((rectOther.Height - rectCurrent.Height) / 2);
                }
            }
            else if (aligment == LineAlignment.CenterVertically)
            {
                if (Math.Abs((rectCurrent.X + rectCurrent.Width / 2) - (rectOther.X + rectOther.Width / 2)) < _magneticDistanse)
                {
                    rectCurrent.X = rectOther.X + ((rectOther.Width - rectCurrent.Width) / 2);
                }
            }
        }
    }

    class ExtendedLine : Control
    {
        public double MinY { get; set; }
        public double MinX { get; set; }
        public double MaxY { get; set; }
        public double MaxX { get; set; }

        public double X1 { get { return GuideLine.X1; } set { GuideLine.X1 = value; } }
        public double X2 { get { return GuideLine.X2; } set { GuideLine.X2 = value; } }
        public double Y1 { get { return GuideLine.Y1; } set { GuideLine.Y1 = value; } }
        public double Y2 { get { return GuideLine.Y2; } set { GuideLine.Y2 = value; } }

        private Line GuideLine { get; set; }

        public ExtendedLine()
        {
            MinX = 0;
            MinY = 0;
            MaxX = 0;
            MaxY = 0;
            GuideLine = new Line();
        }

        public void AddLineToView(Grid view)
        {
            GuideLine.Stroke = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            GuideLine.X1 = GuideLine.X2 = GuideLine.Y1 = GuideLine.Y2 = 0;
            GuideLine.StrokeThickness = 1;
            GuideLine.StrokeDashArray = new DoubleCollection() { 4, 2 };
            GuideLine.StrokeDashCap = PenLineCap.Flat;
            GuideLine.Visibility = Visibility.Collapsed;

            view.Children.Add(GuideLine);
        }

        public void ShowLine()
        {
            GuideLine.Visibility = Visibility.Visible;
        }

        public void HideLine()
        {
            GuideLine.Visibility = Visibility.Collapsed;
        }

        public void ClearValues()
        {
            MinX = 0;
            MinY = 0;
            MaxX = 0;
            MaxY = 0;
        }
    }
}
