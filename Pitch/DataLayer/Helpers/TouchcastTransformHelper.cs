﻿using System;
using TouchCastComposingEngine;
using Windows.Foundation;

namespace Pitch.Helpers
{
    public class TouchcastTransformHelper
    {
        struct TouchcastTransformationdata
        {
            Rect position;
            double conterClockwiseAngle;
        }
        public static Rect CreateRectangleFromTransformString(string transform, Size size)
        {
            Rect resultRect = Rect.Empty;
            if (string.IsNullOrEmpty(transform) || string.IsNullOrWhiteSpace(transform)) return resultRect;
            if(transform.Contains("["))
            {
                transform = transform.Replace("[", String.Empty);
            }
            if (transform.Contains("]"))
            {
                transform = transform.Replace("]", String.Empty);
            }
            var result = transform.Split(',');

            double widthMultiplier = Double.Parse(result[0]);
            double heigthMultiplier = Double.Parse(result[3]);
            double xMultiplier = Double.Parse(result[4]);
            double yMultiplier = Double.Parse(result[5]);
            resultRect.Width = widthMultiplier * size.Width;
            resultRect.Height = heigthMultiplier * size.Height;
            resultRect.X = (xMultiplier - 0.5 * (widthMultiplier - 1)) * size.Width;
            resultRect.Y = (0.5 * (1 - heigthMultiplier) - yMultiplier) * size.Height;
            return resultRect;
        }

        /// <summary>
        ///   {a,b,0,d,tX,tY}
        ///    a - width
        ///    b - sin(alpha) (alpha - conterclockwise)
        ///    d - height
        ///    tX- X Coordinate
        ///    tY - y Coordinate
        ///    angel we can calculate by  arctangens(b/a)
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public static string CreateTouchcastTransformFromRectangle(Rect position)
        {
            var transformWidth = position.Width / ComposingContext.VideoSize.Width;
            var transformHeight = position.Height / ComposingContext.VideoSize.Height;
            var transformX = position.X / ComposingContext.VideoSize.Width +
                             0.5 * (position.Width / ComposingContext.VideoSize.Width - 1);
            var transfromY = 0.5 * (1 - position.Height / ComposingContext.VideoSize.Height) -
                             position.Y / ComposingContext.VideoSize.Height;
            return $"[{transformWidth},0,0,{transformHeight},{transformX},{transfromY}]";
        }
    }
}
