﻿using Windows.UI.Xaml.Controls;

namespace Pitch.Helpers.Frames
{
    public enum FrameType
    {
        MainFrame = 0,
        SecondaryFrame = 1
    }

    public class TouchcastFrame : Frame
    {
        public string Id { get; }
        public FrameType Type { set; get; }

        public TouchcastFrame(FrameType type, string id)
        {
            Id = id;
            Type = type;
        }
    }
}
