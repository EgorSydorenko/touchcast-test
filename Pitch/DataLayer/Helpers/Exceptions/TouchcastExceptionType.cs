﻿namespace Pitch.Helpers.Exceptions
{
    public enum TouchcastExceptionType
    {
        TouchcastOpenFileException,
        TouchcastMissingEntityException
    }
}
