﻿using System;
using System.Linq;
using System.Reflection;

namespace Pitch.Helpers.Exceptions
{
    internal static class ExceptionExtension
    {
        public static string GetExceptionDetails(this Exception exception)
        {
            try
            {
                var properties = exception.GetType()
                                        .GetProperties();
                var fields = properties
                                 .Select(property => new
                                 {
                                     Name = property.Name,
                                     Value = property.GetValue(exception, null)
                                 })
                                 .Select(x => String.Format(
                                     "{0} = {1}",
                                     x.Name,
                                     x.Value != null ? x.Value.ToString() : String.Empty
                                 ));
                return String.Join("\n", fields);
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}
