﻿using System;
using System.Collections.Generic;

namespace Pitch.Helpers.Exceptions
{
    public class TouchcastException : Exception
    {
        private TouchcastExceptionType exceptionType;

        private static readonly Dictionary<TouchcastExceptionType, string> touchcastExceptionMessages = new Dictionary<TouchcastExceptionType, string>()
        {
            {TouchcastExceptionType.TouchcastOpenFileException, "Cannot open tct file"},
            {TouchcastExceptionType.TouchcastMissingEntityException, "File not found" }
        };

        public TouchcastException()
        {

        }

        public TouchcastException(TouchcastExceptionType type) : base(touchcastExceptionMessages[type])
        {
            exceptionType = type;
        }

        public TouchcastException(TouchcastExceptionType type, Exception innerException) : base(touchcastExceptionMessages[type], innerException)
        {
            exceptionType = type;
        }

        public TouchcastException(string message) : base(message)
        {
            
        }

        public TouchcastException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public TouchcastExceptionType ExceptionType => exceptionType;

    }
}
