﻿using GoogleAnalytics;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace Pitch.Helpers.Analytics
{

    public enum AnalyticCommandType
    {
        AnalyticCommandStart,
        AnalyticCommandUse,
        AnalyticCommandProjectCreated,
        AnalyticCommandProjectOpened,
        AnalyticCommandProjectSaved,
        AnalyticCommandTouchcastRecorded,
        AnalyticCommandTouchcastExported,

        AnalyticsCommandSceneAdded,
        AnalyticsCommandLocalFileAdded,
        AnalyticsCommandTCStockAdded,
        AnalyticsCommandTextAdded,
        AnalyticsCommandShapeAdded,
        AnalyticsCommandRetakeSceneInRecording,
        AnalyticsCommandRetakeSceneInEditing,
        AnalyticsCommandClipDeleted,
        AnalyticsCommandClipSplited,
        AnalyticsCommandClipTrimmed,
        AnalyticsCommandTCTOpened

    }

    public class GoogleAnalyticsManager
    {
        public const string BasicEventCategoryValue = "Basic";
        public const string VappsEventCategoryValue = "vApps";
        public const string ScenesEventCategoryValue = "Scenes";
        public const string ClipsEventCategoryValue = "Clips";
        public const string PlayerEventCategoryValue = "Player";

        private const string ProtocolVersionKey = "v";
        private const string TrackerIdKey = "tid";
        private const string DataSourceKey = "ds";
        private const string ClientIDKey = "cid";
        private const string UserIDKey = "uid";
        private const string AppNameKey = "an";
        private const string AppVersionKey = "av";
        private const string EventKey = "t";
        private const string EventCategoryKey = "ec";
        private const string EventActionKey = "ea";
        private const string EventLabelKey = "el";
        private const string OrganizationKey = "cd1";
        private const string GroupKey = "cd2";
        private const string OrganizationIdKey = "cd3";
        private const string GroupIdKey = "cd4";
        private const string CacheBusterKey = "z";

        private const string ProtocolVersion = "1";

        private const string DataSource = "App";
        private const string AppNameValue = "Pitch Win";
        private const string EventValue = "event";


        private const string AppStartedAction = "App Started";
        private const string AppUsedAction = "App Used";
        private const string ProjectCreatedAction = "Project Created";
        private const string ProjectOpenedAction = "Project Opened";
        private const string ProjectSavedAction = "Project Saved";
        private const string TouchcastRecordedAction = "Touchcast Recorded";
        private const string TouchcastExportedAction = "Touchcast Exported";

        private const string SceneAddedAction = "Scene Added";
        private const string LocalFileAddedAction = "Local file Added";
        private const string TCStockAddedAction = "TC Stock Added";
        private const string TextAddedAction = "Text Added";
        private const string ShapeAddedAction = "Shape Added";
        private const string RetakeSceneFromRecordModeAction = "Retake Scene(Record mode)";
        private const string RetakeSceneFromEditModeAction = "Retake Scene(Edit mode)";
        private const string ClipDeletedAction = "Clip Deleted";
        private const string ClipSplitedAction = "Clip Splited";
        private const string ClipTrimmedAction = "Clip Trimmed";
        private const string TCTOpenedAction = "TCT Opened";

        private static readonly Dictionary<AnalyticCommandType, Tuple<String, String>> _keyValuePairs = new Dictionary<AnalyticCommandType, Tuple<string, string>>()
        {
            //BASIC:
            { AnalyticCommandType.AnalyticCommandStart, new Tuple<string, string>(BasicEventCategoryValue, AppStartedAction) },
            { AnalyticCommandType.AnalyticCommandUse, new Tuple<string, string>(BasicEventCategoryValue, AppUsedAction) },
            { AnalyticCommandType.AnalyticCommandProjectCreated, new Tuple<string, string>(BasicEventCategoryValue, ProjectCreatedAction) },
            { AnalyticCommandType.AnalyticCommandProjectOpened, new Tuple<string, string>(BasicEventCategoryValue, ProjectOpenedAction) },
            { AnalyticCommandType.AnalyticCommandProjectSaved, new Tuple<string, string>(BasicEventCategoryValue, ProjectSavedAction) },
            { AnalyticCommandType.AnalyticCommandTouchcastRecorded, new Tuple<string, string>(BasicEventCategoryValue, TouchcastRecordedAction) },
            { AnalyticCommandType.AnalyticCommandTouchcastExported, new Tuple<string, string>(BasicEventCategoryValue, TouchcastExportedAction) },
                
            //SCENE:
            { AnalyticCommandType.AnalyticsCommandSceneAdded, new Tuple<string, string>(ScenesEventCategoryValue, SceneAddedAction) },

             //VAPPS:
            { AnalyticCommandType.AnalyticsCommandTextAdded, new Tuple<string, string>(VappsEventCategoryValue, TextAddedAction) },
            { AnalyticCommandType.AnalyticsCommandShapeAdded, new Tuple<string, string>(VappsEventCategoryValue, ShapeAddedAction) },
            { AnalyticCommandType.AnalyticsCommandLocalFileAdded, new Tuple<string, string>(VappsEventCategoryValue, LocalFileAddedAction) },
            { AnalyticCommandType.AnalyticsCommandTCStockAdded, new Tuple<string, string>(VappsEventCategoryValue, TCStockAddedAction) },
            
            //RETAKES:
            { AnalyticCommandType.AnalyticsCommandRetakeSceneInEditing, new Tuple<string, string>(ClipsEventCategoryValue, RetakeSceneFromEditModeAction) },
            { AnalyticCommandType.AnalyticsCommandRetakeSceneInRecording, new Tuple<string, string>(ClipsEventCategoryValue, RetakeSceneFromRecordModeAction) },
            
            //CLIPS:
            { AnalyticCommandType.AnalyticsCommandClipDeleted, new Tuple<string, string>(ClipsEventCategoryValue, ClipDeletedAction) },
            { AnalyticCommandType.AnalyticsCommandClipSplited, new Tuple<string, string>(ClipsEventCategoryValue, ClipSplitedAction) },
            { AnalyticCommandType.AnalyticsCommandClipTrimmed, new Tuple<string, string>(ClipsEventCategoryValue, ClipTrimmedAction) },

            //TCT:
            { AnalyticCommandType.AnalyticsCommandTCTOpened, new Tuple<string, string>(PlayerEventCategoryValue, TCTOpenedAction) },
        };

        private static Tracker _tracker;

        private static bool _isEnabled = false;
        public static bool IsEnabled
        {
            set => _isEnabled = value;
            get => _isEnabled;
        }
        static GoogleAnalyticsManager()
        {
            AnalyticsManager.Current.DispatchPeriod = TimeSpan.Zero; //immediate mode, sends hits immediately
            AnalyticsManager.Current.ReportUncaughtExceptions = true; //catch unhandled exceptions and send the details
            AnalyticsManager.Current.AutoAppLifetimeMonitoring = true;//handle suspend/resume and empty hit batched hits on suspend
            _tracker = AnalyticsManager.Current.CreateTracker(App.GoogleAnalyticsClientId);
            _tracker.AppName = AppNameValue;
            
        }


        public static void TrySend(AnalyticCommandType commandType ,String extra = "")
        {
            if (String.IsNullOrEmpty(App.GoogleAnalyticsClientId)) return;
            try
            {
                var parametrs = InitializeParametersForCommand(commandType,extra);
#if DEBUG
                System.Diagnostics.Debug.WriteLine(string.Join(";", parametrs.Select(x => x.Key + "=" + x.Value).ToArray()));
#endif
                _tracker.Send(parametrs);
            }
            catch(Exception ex)
            {
                Logger.LogQueue.WriteToFile($"Exception in GoogleAnalyticsManager.TrySend : {ex.Message}");
            }
        }

        private static Dictionary<string, string> InitializeParametersForCommand(AnalyticCommandType commandType, string extra)
        {
            string userID = String.Empty;
            string organization = String.Empty;
            string group = String.Empty;
            string organizationId = String.Empty;
            string groupId = String.Empty; 
            if (TfSdk.ApiClient.Shared?.UserInfo?.token != null)
            {
                var userInfo = TfSdk.ApiClient.Shared.UserInfo;
                
                userID = ComputeMD5(userInfo.info.user_id);
                if (userInfo.organization != null)
                {
                    organization = userInfo.organization.name;
                    organizationId = userInfo.organization.organization_id;
                }
                
                group = userInfo.group.name;
                groupId = userInfo.group.group_id;
            }

            Dictionary<string, string> parametrs = new Dictionary<string, string>();
            parametrs.Add(ProtocolVersionKey, ProtocolVersion);
            parametrs.Add(TrackerIdKey, App.GoogleAnalyticsClientId);
            parametrs.Add(DataSourceKey, DataSource);
            parametrs.Add(ClientIDKey, App.DeviceId);
            if (!String.IsNullOrEmpty(userID))
            {
                parametrs.Add(UserIDKey, userID);
            }
            
            parametrs.Add(AppNameKey, AppNameValue);
            parametrs.Add(AppVersionKey, App.ApplicationVersion);
            parametrs.Add(EventKey, EventValue);

            if(_keyValuePairs.ContainsKey(commandType))
            {
                var tuple = _keyValuePairs[commandType];
                parametrs.Add(EventCategoryKey, tuple.Item1);
                parametrs.Add(EventActionKey, tuple.Item2);
            }

            if(!String.IsNullOrEmpty(extra))
            {
                parametrs.Add(EventLabelKey, extra);
            }

            if (!String.IsNullOrEmpty(organization))
            {
                parametrs.Add(OrganizationKey, organization);
                parametrs.Add(OrganizationIdKey, organizationId);
            }

            if (!String.IsNullOrEmpty(group))
            {
                parametrs.Add(GroupKey, group);
                parametrs.Add(GroupIdKey, groupId);
            }
            
            parametrs.Add(CacheBusterKey, Guid.NewGuid().ToString());
            return parametrs;
        }
        private static string ComputeMD5(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }
    }
}
