﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Pitch.Models;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using Windows.Storage.Streams;

namespace Pitch.Helpers
{
    internal enum FileSavingResult
    {
        CanceledByUser,
        Success,
        Fail,
        FileNotChanged
    }

    public enum FileImportResult
    {
        Cancel,
        FileAsIs,
        ScenePerPowerPointSlide
    }
    public class FileInsertResult
    {
        public StorageFile File { get; private set; }
        public FileImportResult Result { get; private set; }
        public FileInsertResult(FileImportResult result, StorageFile file)
        {
            Result = result;
            File = file;
        }
    }

    internal static class FileToAppHelper
    {
        const uint MaximumBufferSize = 400 * 1024 * 1024;

        public static readonly string TouchcastExtension = ".tct";
        public static readonly string TouchcastProjectExtension = ".tcp";
        public static readonly string TouchcastThemeExtension = ".tcpx";


        public static List<string> ImageExtensions = new List<string>
        {
            ".jpg"
            ,".jpeg"
            ,".png"
        };
        public static List<string> VideoExtensions = new List<string>
        {
            ".mp4"
        };
        public static List<string> DocumentExtensions = new List<string>
        {
            ".pdf"
        };
        public static List<string> TextExtensions = new List<string>
        {
            ".rtf"
        };
        public static List<string> ShapeExtensions = new List<string>
        {
            ".shp"
        };
        public static List<string> OfficeExtensions = new List<string>()
        {
            ".docx", ".doc", ".xlsx", ".xls", ".pptx", ".ppt"
        };
        public static List<string> ProjectExtensions = new List<string>()
        {
            TouchcastProjectExtension,
            TouchcastThemeExtension
        };
        public static List<string> PitchExtensions = new List<string>(ProjectExtensions)
        {
            TouchcastExtension,
        };

        static private string _fileAccessToken = String.Empty;

        public static async Task<StorageFile> SelectFile(string fileExtension)
        {
            FileOpenPicker openPicker = new FileOpenPicker
            {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary
            };
            openPicker.FileTypeFilter.Clear();
            openPicker.FileTypeFilter.Add(fileExtension);
            
            return await openPicker.PickSingleFileAsync();
        }

        public static async Task<StorageFile> SelectFile(string[] fileExtensions)
        {
            FileOpenPicker openPicker = new FileOpenPicker()
            {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary
            };
            openPicker.FileTypeFilter.Clear();
            foreach (string ext in fileExtensions)
            {
                openPicker.FileTypeFilter.Add(ext);
            }
            return await openPicker.PickSingleFileAsync();
        }

        public static async Task<StorageFolder> SelectFolder()
        {
            FolderPicker folderPicker = new FolderPicker();
            folderPicker.SuggestedStartLocation = PickerLocationId.Desktop;
            folderPicker.FileTypeFilter.Add("*");
            return await folderPicker.PickSingleFolderAsync();
        }

        public static async Task<StorageFile> SaveFileDialog(string extension, string extensionDescription, string defaultFileName)
        {
            FileSavePicker savePicker = new FileSavePicker { SuggestedStartLocation = PickerLocationId.DocumentsLibrary };
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add(extensionDescription, new List<string>() { extension });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = defaultFileName;
            StorageFile fileToSave = await savePicker.PickSaveFileAsync();
            return fileToSave;
        }

        public static async Task<FileSavingResult> SaveAsArchiveFile(string name, bool isWithVideo = true)
        {
            //FileSavePicker savePicker = new FileSavePicker { SuggestedStartLocation = PickerLocationId.DocumentsLibrary };
            //// Dropdown of file types the user can save the file as
            //savePicker.FileTypeChoices.Add("Touchcast", new List<string>() { ".tct" });
            //// Default file name if the user does not type one in or select a file to replace
            //savePicker.SuggestedFileName = "New Touchcast";

            FileSavingResult result = FileSavingResult.CanceledByUser;
            //StorageFile fileToSave = await savePicker.PickSaveFileAsync();
            if (String.IsNullOrEmpty(name))
            {
                name = "New Touchcast";
            }

            var fileToSave = await SaveFileDialog(TouchcastExtension, "Touchcast", name);
            if (fileToSave != null)
            {
                _fileAccessToken = StorageApplicationPermissions.FutureAccessList.Add(fileToSave);
                result = await SaveArchive(fileToSave, isWithVideo);
            }
            return result;
        }

        public static async Task<FileSavingResult> SaveArchiveFile(string name, bool isWithVideo = true)
        {
            if (String.IsNullOrEmpty(_fileAccessToken))
            {
                return await SaveAsArchiveFile(name, isWithVideo);
            }
            StorageFile fileToSave = await StorageApplicationPermissions.FutureAccessList.GetFileAsync(_fileAccessToken);
            return await SaveArchive(fileToSave, isWithVideo);
        }

        private static async Task<FileSavingResult> SaveArchive(StorageFile fileToSave, bool isWithVideo = true)
        {
            FileSavingResult result = FileSavingResult.CanceledByUser;
            if (fileToSave != null)
            {
                Touchcast.Instance.Performance.Metadata.Name = fileToSave.DisplayName;
                StorageFile zip = null;
                result = FileSavingResult.FileNotChanged;
                try
                {
                    zip = await Touchcast.Instance.SaveArchive();
                    result = FileSavingResult.Fail;

                    CachedFileManager.DeferUpdates(fileToSave);

                    await MoveFile(zip, fileToSave);

                    result = FileSavingResult.Success;
                }
                finally
                {
                    if (zip != null)
                    {
                        // Let Windows know that we're finished changing the file so the other app can update the remote version of the file.
                        // Completing updates may require Windows to ask for user input.
                        // If you prevented updates to the specified file by calling DeferUpdates, you must call this method to initiate 
                        // updates to the file.
                        if (FileUpdateStatus.Complete != await CachedFileManager.CompleteUpdatesAsync(fileToSave))
                        {
                            result = FileSavingResult.Fail;
                        }
                    }
                }
                try
                {
                    if (result == FileSavingResult.Fail)
                    {
                        await fileToSave.DeleteAsync();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogQueue.WriteToFile($"Exception in FileToAppHelper.SaveArchive : {ex.Message}");
                }
            }

            return result;
        }

        public static async Task MoveFile(StorageFile sourceFile, StorageFile destinationFile)
        {
            /*
             * 
            await sourceFile.MoveAndReplaceAsync(destinationFile);
            */

            using (var inStream = await sourceFile.OpenAsync(FileAccessMode.Read))
            {
                uint size = inStream.Size > MaximumBufferSize ? MaximumBufferSize : (uint)inStream.Size;

                IBuffer buffer = new Windows.Storage.Streams.Buffer(size);
                using (var trStream = await destinationFile.OpenAsync(FileAccessMode.ReadWrite))
                {
                    trStream.Size = 0;
                    ulong currentSize = 0;
                    do
                    {
                        var res = await inStream.ReadAsync(buffer, size, InputStreamOptions.None);
                        await trStream.WriteAsync(buffer);
                        currentSize += res.Length;
                    } while (currentSize < inStream.Size);
                }
            }
            await sourceFile.DeleteAsync();
        }

        public static void ClearFilePermisiions()
        {
            _fileAccessToken = String.Empty;
            StorageApplicationPermissions.FutureAccessList.Clear();
        }

        public static async Task<StorageFile> SaveImageStreamToPngFile(IRandomAccessStream sourceStream, BitmapSize destinationSize, StorageFolder destinationFolder, string destinationFileName)
        {
            var decoder = await BitmapDecoder.CreateAsync(sourceStream);
            var width = decoder.PixelWidth;
            var height = decoder.PixelHeight;
            if (decoder.OrientedPixelWidth > destinationSize.Width || decoder.OrientedPixelHeight > destinationSize.Height)
            {
                var aspectRatio = (double)decoder.PixelWidth / decoder.PixelHeight;
                if (aspectRatio >= 1)
                {
                    width = destinationSize.Width;
                    height = (uint)(width / aspectRatio);
                }
                else
                {
                    height = destinationSize.Height;
                    width = (uint)(height * aspectRatio);
                }
            }
            //decoder.OrientedPixelHeight
            var destinationFile = await destinationFolder.CreateFileAsync(destinationFileName, CreationCollisionOption.ReplaceExisting);
            using (var resizedStream = await destinationFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                //create encoder based on decoder of the source file
                var encoder = await BitmapEncoder.CreateForTranscodingAsync(resizedStream, decoder);

                //you can adjust interpolation and other options here, so far linear is fine for thumbnails
                encoder.BitmapTransform.InterpolationMode = BitmapInterpolationMode.Linear;
                encoder.BitmapTransform.Bounds = new BitmapBounds()
                {
                    Width = width,
                    Height = height,
                    X = 0,
                    Y = 0
                };
                encoder.BitmapTransform.ScaledWidth = width;
                encoder.BitmapTransform.ScaledHeight = height;
                //encoder.BitmapTransform.Flip = BitmapFlip.Vertical;

                await encoder.FlushAsync();

            }

            return destinationFile;
        }

        public static async Task<FileInsertResult> InsertFile(Action action)
        {
            var extensions = new List<string>(ImageExtensions);
            extensions.AddRange(VideoExtensions);
            extensions.AddRange(DocumentExtensions);
            extensions.AddRange(OfficeExtensions);
            var file = await SelectFile(extensions.ToArray());
            if (file == null)
            {
                return new FileInsertResult(FileImportResult.Cancel, null);
            }

            FileImportResult fileImportResult = FileImportResult.FileAsIs;
            if (file.FileType == ".ppt" || file.FileType == ".pptx")
            {
                var dialog = new UILayer.Helpers.CustomControls.ImportPowerPointDialog();
                await dialog.ShowAsync();
                var dialogResult = dialog.Result;
                if (dialogResult == UILayer.Helpers.CustomControls.ImportPowerPointResult.Cancel)
                {
                    return new FileInsertResult(FileImportResult.Cancel, null);
                }
                fileImportResult = dialogResult == UILayer.Helpers.CustomControls.ImportPowerPointResult.AllSlidesInSingleScene ?
                    FileImportResult.FileAsIs : FileImportResult.ScenePerPowerPointSlide;
            }

            action?.Invoke();
            if (OfficeExtensions.Contains(file.FileType))
            {
                file = await ConvertDocumentToPdf(file);
            }
            return new FileInsertResult(fileImportResult, file);
        }
        private static async Task<StorageFile> ConvertDocumentToPdf(StorageFile officeDocumentFile)
        {
            StorageFile result = null;
            var dstFilePath = App.TemporaryFolder.Path + "\\" + Guid.NewGuid().ToString() + ".pdf";
            var responce = await TouchCastComposingEngine.Win32BackgroundPipe.OfficeConverter.ConvertMicrosoftOfficeDocument(officeDocumentFile.Path, dstFilePath);
            if (TouchCastComposingEngine.Win32BackgroundPipe.OfficeConverter.IsResponceSuccessfull(responce))
            {
                result = await App.TemporaryFolder.GetFileAsync(Path.GetFileName(dstFilePath));
            }
            else
            {
                var error = TouchCastComposingEngine.Win32BackgroundPipe.OfficeConverter.GetError(responce);
            }
            return result;
        }
    }
}
