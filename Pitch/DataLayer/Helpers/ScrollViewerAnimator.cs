﻿using System;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Pitch.Helpers
{
    public class ScrollViewerAnimator
	{
        #region Private Fields
        private const double OffsetStep = 1;
        private const double ScrollStep = 20;

        private readonly ScrollViewer _scrollViewer;
		private readonly double _initialScrollSpeed;
		private double _scrollSpeed;

        private Task _backgroundTask;
        private TimeSpan _delay;
        private bool _isStart;
        private bool _isCompleted;
        private bool _isNeedSkip;
        private double _scrollDelta;
        private object _locker = new object();
        #endregion

        #region Life Cycle

        public ScrollViewerAnimator(ScrollViewer scrollViewer, double scrollSpeed = 20.0)
		{
			_scrollViewer = scrollViewer;
			_scrollSpeed = _initialScrollSpeed = scrollSpeed;

            _delay = TimeSpan.FromSeconds(OffsetStep / _scrollSpeed);

            _backgroundTask = new Task(async () => await Process());
            _backgroundTask.Start();
        }
#if DEBUG
        ~ScrollViewerAnimator()
        {
            System.Diagnostics.Debug.WriteLine("*************** ScrollViewerAnimator Destructor **************");
        }
#endif

        #endregion

        #region Public Methods

        public void Start()
		{
            _isStart = true;
        }

		public void Stop()
		{
            _isStart = false;
            _isCompleted = true;
        }

        public void Pause()
        {
            _isStart = false;
        }

        public void Reset()
		{
            lock (_locker)
            {
                _scrollViewer.ChangeView(null, 0.0, null, true);
                _isNeedSkip = true;
            }
        }

		public void Restart()
		{         
                Pause();
                Reset();
                Start(); 
		}

		public void IncreaseSpeedByPercentage(double percentage)
		{
			ChangeSpeedByPercentage(percentage);
		}

		public void DecreaseSpeedByPercentage(double percentage)
		{
			ChangeSpeedByPercentage(-percentage);
		}

        public void SeekTo(double pos)
        {
        }
        public void SeekBy(double delta)
        {
            lock (_locker)
            {
                _scrollDelta += delta > 0 ? ScrollStep : -ScrollStep;
            }
        }

        #endregion

        #region Private Methods

		private void ChangeSpeedByPercentage(double percentage)
		{
			var factor = _initialScrollSpeed * percentage / 100;
    		_scrollSpeed += factor;
            _delay = TimeSpan.FromSeconds(OffsetStep / _scrollSpeed);
		}
        
        private async Task Process()
        {
            while (true)
            {
                await Task.Delay(_delay);

                if (_isCompleted) return;

                var scrollDelta = 0.0;
                if (_isStart)
                {
                    scrollDelta += OffsetStep;
                }
                await _scrollViewer.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    lock (_locker)
                    {
                        if(_isNeedSkip)
                        {
                            _isNeedSkip = false;
                            return;
                        }
                        scrollDelta += _scrollDelta;
                        _scrollDelta = 0;

                        _scrollViewer.ChangeView(null, _scrollViewer.VerticalOffset + scrollDelta, null, true);
                    }
                });
            }
        }

		#endregion
	}
}
