﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;

namespace Pitch.Helpers.Logger
{
    public static class LogQueue
    {
        private static Task _LoggerTask;
        private static readonly List<string> Messages = new List<string>();
        private static readonly object lockObject = new object();
        private static string _fileName;
        private static readonly object _taskLocker = new object();
        public static bool IsEmpty => (Messages.Count == 0);
        public static void WriteToFile(string str)
        {
            PushNewMessage(str);

            lock (_taskLocker)
            {
                if (_LoggerTask != null)
                {
                    return;
                }

                _LoggerTask = new Task(async () =>
                {
                    while (true)
                    {
                        await Task.Delay(TimeSpan.FromSeconds(1));

                        var list = PopMessages();
                        if (list.Any() && App.LogsFolder != null)
                        {
                            var file = await App.LogsFolder.CreateFileAsync(_fileName, CreationCollisionOption.OpenIfExists);
                            await FileIO.AppendLinesAsync(file, list);
                        }
                    }
                });

                _fileName = $"{DateTime.Now.ToString("yyyyMMddHHmmss")}.txt";
                _LoggerTask.Start();
            }
        }

        private static void PushNewMessage(string str)
        {
            lock (lockObject)
            {
                Messages.Add($"{DateTime.Now.ToString("HH:mm:ss")} : {str}");
            }
        }

        private static List<string> PopMessages()
        {
            lock (lockObject)
            {
                var messages = new List<string>(Messages);
                Messages.Clear();
                return messages;
            }
        }

        public static string VideoInfo
        {
            get
            {
                //var bounds = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView();
                var a = Windows.Graphics.Display.DisplayInformation.GetForCurrentView();
                try
                {
                    var heightInRawPixels = a.ScreenHeightInRawPixels;
                    var widthInRawPixels = a.ScreenWidthInRawPixels;
                    var scale = a.RawPixelsPerViewPixel;
                    return $"{widthInRawPixels} x {heightInRawPixels}";
                }
                catch(Exception)
                {

                }
                return String.Empty;
            }
        }
    }
}
