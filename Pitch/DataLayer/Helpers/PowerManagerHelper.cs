﻿using GalaSoft.MvvmLight.Messaging;
using Pitch.UILayer.Helpers.Messages;
using Windows.System.Power;

namespace Pitch.Helpers
{
    public static class PowerManagerHelper
    {
        public static PowerSupplyStatus GetPowerSupplyStatus
        {
            get
            {
                return PowerManager.PowerSupplyStatus;
            }
        }

        public static void StartObserve()
        {
            PowerManager.PowerSupplyStatusChanged += PowerManager_PowerSupplyStatusChanged;
        }

        public static void StopObserve()
        {
            PowerManager.PowerSupplyStatusChanged -= PowerManager_PowerSupplyStatusChanged;
        }

        private static void PowerManager_PowerSupplyStatusChanged(object sender, object e)
        {
            if (PowerManager.PowerSupplyStatus == PowerSupplyStatus.NotPresent)
            {
                Messenger.Default.Send(new ShowBatteryStatusPopup());
            }
            else
            {
                Messenger.Default.Send(new HideBatteryStatusPopup());
            }
        }
    }
}
