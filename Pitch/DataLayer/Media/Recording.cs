﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using Pitch.Models;
using Pitch.Helpers.Logger;

namespace Pitch.DataLayer
{
    public class Recording
    {
        #region Private fields
        private string _id;
        private StorageFile _file;
        private ActionsStream _actionsStream;
        private TimeSpan _duration;
        private StorageFile _actionsXml;
        private List<StorageFile> _resources;
        #endregion

        #region Public properties
        public StorageFile File => _file;
        public string Id
        {
            set => _id = value;
            get => _id;
        }
        public TimeSpan Duration => _duration;

        public ActionsStream ActionsStream => _actionsStream;
        public StorageFile ActionsXml => _actionsXml;
        public List<StorageFile> Resources => _resources;
        #endregion

        public static async Task<Recording> Create(StorageFolder folder, TcpInfo.Recording recordingMetaData)
        {
            var recording = new Recording();
            recording._id = recordingMetaData.id;
            recording._duration = TimeSpan.FromSeconds(recordingMetaData.duration);
            if (recordingMetaData.actions_file != null)
            {
                recording._actionsXml = await folder.GetFileAsync(recordingMetaData.actions_file);
            }

            recording._file = await folder.GetFileAsync(recordingMetaData.video_file);
            var touchccastSerializer = new XmlSerializer(typeof(Touchcast));
            using (var infoStream = await recording._actionsXml.OpenReadAsync())
            {
                try
                {
                    var info = touchccastSerializer.Deserialize(infoStream.AsStream()) as Touchcast;
                    recording._actionsStream = info.ActionsStream;
                }
                catch(Exception e)
                {
                    LogQueue.WriteToFile($"Exception in Recording.Create {e.Message}");
                }
            }
            //TODO
            recording._resources = new List<StorageFile>();
            foreach (var item in recordingMetaData.resources)
            {
                var resource = await folder.GetFileAsync(item);
                recording._resources.Add(resource);
            }
            return recording;
        }

        private Recording() { }

        public Recording(StorageFile videoFile, StorageFile actionsXml, TimeSpan duration, ActionsStream actionsStream)
        {
            _file = videoFile;
            _duration = duration;
            _actionsStream = actionsStream;
            _actionsXml = actionsXml;
            _resources = new List<StorageFile>();
        }

        public TcpInfo.Recording GetMetaData()
        {
            var recording = new TcpInfo.Recording();
            recording.id = _id;
            recording.video_file = _file.Name;
            recording.duration = _duration.TotalSeconds;
            recording.actions_file = _actionsXml.Name;
            recording.resources = new List<string>();
            foreach (var item in _resources)
            {
                recording.resources.Add(item.Name);
            }
            return recording;
        }

        public async Task CheckUsedResources()
        {
            //_resources

            var commands = _actionsStream.Commands.Where(x => x.Type == CommandTypes.Close).ToList();
            var urls = new List<string>();
            foreach (var command in commands)
            {
                if (command.ThumbnailUrl != null && !urls.Contains(command.ThumbnailUrl))
                {
                    urls.Add(command.ThumbnailUrl);
                }
                if (command.Url != null && !command.Url.Contains(":\\") && !urls.Contains(command.Url))
                {
                    urls.Add(command.Url);
                }
            }

            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<DataLayer.AuthoringSession>();
            var result = new List<StorageFile>();
            if (session != null)
            {
                var workingFolder = session.Project.WorkingFolder;
                foreach (var url in urls)
                {
                    try
                    {
                        if (!(url.Contains("http://") || url.Contains("https://")))
                        {
                            var fileName = System.IO.Path.GetFileName(url);
                            var file = await workingFolder.GetFileAsync(fileName);
                            result.Add(file);
                        }
                    }
                    catch 
                    {
                    }
                }
            }
            _resources = result;
        }
    }
}
