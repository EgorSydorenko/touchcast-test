﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Pitch.DataLayer.Shots;
using Pitch.Helpers.Logger;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.DataLayer
{
    public class TrimVideoFragmentEventArgs
    {
        protected string _videoFragmentId;
        protected TimeRange _trimTimeRange;

        public string VideoFragmentId => _videoFragmentId;
        public TimeRange TrimTimeRange => _trimTimeRange;

        public TrimVideoFragmentEventArgs(VideoFragment videoFragment, TimeRange trimTimeRange)
        {
            _videoFragmentId = videoFragment.Id;
            _trimTimeRange = trimTimeRange;
        }

        protected TrimVideoFragmentEventArgs(TrimVideoFragmentEventArgs origin)
        {
            _videoFragmentId = origin._videoFragmentId;
            _trimTimeRange = origin._trimTimeRange;
        }
    }

    public delegate void VideoFragmentTrimEvent(TrimVideoFragmentEventArgs args);

    public class Take : IUsedFilesInterface
    {
        #region Private fields
        private string _id;
        private List<VideoFragment> _videoFragments;
        private WeakReference<Shot> _shot;
        private WeakReference<TcMediaClip> _tcMediaClip;
        #endregion

        #region Public properties

        public string Id => _id;
        public IReadOnlyList<VideoFragment> VideoFragments => _videoFragments;
        public TcMediaClip TcMediaClip => _tcMediaClip?.TryGetObject();

        public Shot Shot => _shot?.TryGetObject();

        #endregion

        public event VideoFragmentTrimEvent VideoFragmentWillBeTrimmedEvent;

        public static async Task<Take> CreateFromMetaData(Shot shot,TcMediaClip clip ,List<TcpInfo.CompositionItem> compositionItems, List<Recording> recordings)
        {
            var take = new Take(shot,clip ,null);
            foreach (var compositionItem in compositionItems)
            {
                var recording = recordings.FirstOrDefault(x => x.Id == compositionItem.recording_id);
                if (recording != null)
                {
                    var fragment = await VideoFragment.CreateFromMetaData(take, compositionItem, recording);
                    if (fragment != null)
                    {
                        take._videoFragments.Add(fragment);
                    }
                }
            }
            if(!take._videoFragments.Any())
            {
                take = null;
            }
            if (take?._videoFragments != null)
            {
                LogQueue.WriteToFile($"Take {take.Id} CreateFromMetaData. Total videos: {take._videoFragments.Count}");
            }
            return take;
        }

        public Take(Shot shot, TcMediaClip mediaClip, string id = null)
        {
            _videoFragments = new List<VideoFragment>();
            if (shot != null)
            {
                _shot = new WeakReference<Shot>(shot);
            }
            _tcMediaClip = new WeakReference<TcMediaClip>(mediaClip);

            _id = id;
            if (String.IsNullOrEmpty(id))
            {
                _id = Guid.NewGuid().ToString();
            }
            LogQueue.WriteToFile($"Take {_id} created");
        }

#if DEBUG
        ~Take()
        {
            System.Diagnostics.Debug.WriteLine("********** Take Destructor *****************");
        }
#endif

        public void CreateVideoFragment()
        {
            var fragment = new VideoFragment(this);
            AddVideoFragment(fragment);
            LogQueue.WriteToFile($"Take {_id} CreateVideoFragment. Total videos: {_videoFragments.Count}");
        }

        public IList<StorageFile> UsedFiles()
        {
            return new List<StorageFile>();
        }

        public async Task<Take> Clone(Shot shot = null, TcMediaClip mediaClip = null)
        {
            var usedShot = shot ?? Shot;
            var usedClip = mediaClip ?? TcMediaClip;
            var duplicatedTake = new Take(usedShot, usedClip, Guid.NewGuid().ToString());
            foreach (var fragment in VideoFragments)
            {
                duplicatedTake.AddVideoFragment(await fragment.Clone(duplicatedTake));
            }
            LogQueue.WriteToFile($"Take {duplicatedTake._id} created from take {_id}. Total videos: {duplicatedTake._videoFragments.Count}");
            return duplicatedTake;
        }


        public void Subscribe()
        {
        }
        public void Unsubscribe()
        {
        }

        public void Clean()
        {
            LogQueue.WriteToFile($"Take {_id} Clean");
            Unsubscribe();
            foreach(var fragment in _videoFragments)
            {
                fragment.VideoFragmentTrimRangeWillChangedEvent -= Fragment_VideoFragmentTrimRangeWillChangedEvent;
                //fragment.Clean();
            }
        }

        #region Callbacks
        private void Fragment_VideoFragmentTrimRangeWillChangedEvent(VideoFragment sender)
        {
            VideoFragmentWillBeTrimmedEvent?.Invoke(new TrimVideoFragmentEventArgs(sender, new TimeRange(sender.TrimTimeRange.Start, sender.TrimTimeRange.Duration)));
        }
        #endregion

        #region Private methods
        private void AddVideoFragment(VideoFragment videoFragment)
        {
            videoFragment.VideoFragmentTrimRangeWillChangedEvent += Fragment_VideoFragmentTrimRangeWillChangedEvent;
            _videoFragments.Add(videoFragment);
            UpdateProjectHasUnsavedChanges();
        }

        public void UpdateProjectHasUnsavedChanges()
        {
            Shot?.UpdateProjectHasUnsavedChanges();
        }
        #endregion
    }
}
