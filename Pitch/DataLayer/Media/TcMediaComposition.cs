using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.DataLayer.UndoRedo;
using Pitch.Helpers.Analytics;
using Pitch.Helpers.Logger;

namespace Pitch.DataLayer
{
    public delegate Task MediaClipEvent(TcMediaClip sender);
    public delegate Task MediaClipMoveEvent(TcMediaClip sender, int newindex);
    public delegate void MediaCompositionEmpty();
    public delegate Task MediaClipSplitEvent(TcMediaClip oldClip, TcMediaClip newClip);
    public delegate void ActiveMediaClipEvent(TcMediaClip sender);
    public class TcMediaComposition
    {
        #region Private fields
        private List<TcMediaClip> _mediaClips = new List<TcMediaClip>();
        private Project _project;
        #endregion

        public IReadOnlyList<TcMediaClip> MediaClips => _mediaClips;
        public TcMediaClip MediaClipToProcess { get; set; }
        public bool IsRecordMoreAtEnd { get; set; }

        public event MediaClipEvent MediaClipWillBeCreated;
        public event MediaClipEvent MediaClipWasCreated;
        public event MediaClipEvent MediaClipWillBeRemoved;
        public event MediaClipEvent MediaClipWasRemoved;
        public event MediaClipEvent MediaClipWasChanged;
        public event MediaCompositionEmpty MediaCompositionEmptyEvent;
        public event MediaClipMoveEvent MediaClipWillBeMoved;
        public event MediaClipMoveEvent MediaClipWasMoved;
        public event MediaClipSplitEvent MediaClipWillBeSplitted;
        public event MediaClipSplitEvent MediaClipWasSplitted;
        public event ActiveMediaClipEvent ActiveMediaClipWillBeChanged;
        public event ActiveMediaClipEvent ActiveMediaClipWasChanged;

        #region Life cycle
        public TcMediaComposition(Project project)
        {
            _project = project;
        }

#if DEBUG
        ~TcMediaComposition()
        {
            System.Diagnostics.Debug.WriteLine("********** TcMediaComposition Destructor *****************");
        }
#endif
        #endregion

        #region Public methods
        public async Task AddNewClip(Shot shot)
        {
            LogQueue.WriteToFile($"TcMediaComposition.AddNewClip for Shot {shot.Id}");
            if (MediaClipWillBeCreated != null)
            {
                await MediaClipWillBeCreated.Invoke(null);
            }

            var mediaClip = new TcMediaClip(shot);
            SubscribeToMediaClip(mediaClip);
            _mediaClips.Add(mediaClip);
            _project.HasUnsavedChanges = true;
            if (MediaClipWasCreated != null)
            {
                await MediaClipWasCreated.Invoke(mediaClip);
            }
        }

        public void AddClip(TcMediaClip mediaClip)
        {
            SubscribeToMediaClip(mediaClip);
            _mediaClips.Add(mediaClip);
            _project.HasUnsavedChanges = true;
        }
        public int IndexOf(TcMediaClip mediaClip)
        {
            return _mediaClips.IndexOf(mediaClip);
        }

        public async Task MoveClip(TcMediaClip mediaClip, int newIndex)
        {
            LogQueue.WriteToFile($"TcMediaComposition.MoveClip {mediaClip.Id} at {newIndex} position");
            var index = await MoveClipWithoutUndoRedo(mediaClip, newIndex);
            _project.EditorUndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new TcMediaClipMementoModel(mediaClip, index));
            LogQueue.WriteToFile($"MediaClip {mediaClip.Id} was moved. Total clips: {_mediaClips.Count}");
        }

        public async Task DuplicateClip(TcMediaClip mediaClip)
        {
            LogQueue.WriteToFile($"TcMediaComposition.DuplicateClip {mediaClip.Id}");
            var index = _mediaClips.IndexOf(mediaClip);
            if (index > -1)
            {
                if (MediaClipWillBeCreated != null)
                {
                    await MediaClipWillBeCreated.Invoke(null);
                }

                var newClip = await mediaClip.Clone();
                SubscribeToMediaClip(newClip);
                _mediaClips.Insert(index + 1, newClip);
                _project.HasUnsavedChanges = true;
                if (MediaClipWasCreated != null)
                {
                    await MediaClipWasCreated.Invoke(newClip);
                }

                _project.EditorUndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, new TcMediaClipMementoModel(newClip, index + 1));
                LogQueue.WriteToFile($"MediaClip {newClip.Id} was added. Total clips: {_mediaClips.Count}");
            }
        }

        public async Task RemoveClip(TcMediaClip mediaClip)
        {
            LogQueue.WriteToFile($"TcMediaComposition.RemoveClip {mediaClip.Id}");
            var index = _mediaClips.IndexOf(mediaClip);
            await RemoveClipWithoutUndoRedo(mediaClip);
            GoogleAnalyticsManager.TrySend(AnalyticCommandType.AnalyticsCommandClipDeleted);
            _project.EditorUndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Add, new TcMediaClipMementoModel(mediaClip, index));
            LogQueue.WriteToFile($"MediaClip {mediaClip.Id} was removed. Total clips: {_mediaClips.Count}");
        }

        public async Task SplitClip(TcMediaClip mediaClip, TimeSpan position)
        {
            LogQueue.WriteToFile($"TcMediaComposition.SplitClip {mediaClip.Id} at {position.ToString(@"hh\:mm\:ss")}");
            var newClip = await SplitMediaClipWithoutUndoRedo(mediaClip, position);

            var videoFragment = mediaClip.ActiveTake.VideoFragments.FirstOrDefault();
            GoogleAnalyticsManager.TrySend(AnalyticCommandType.AnalyticsCommandClipSplited);
            _project.EditorUndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new SplitTcMediaClipMementoModel(mediaClip, newClip)
            {
                SplitTime = position,
                TrimTimeRange = new TimeRange(videoFragment.TrimTimeRange.Start, videoFragment.TrimTimeRange.Duration),
            });
        }

        public void Subscribe()
        {
            _project.EditorUndoRedoOriginator.UndoEvent += UndoRedoOriginator_UndoEvent;
            _project.EditorUndoRedoOriginator.RedoEvent += UndoRedoOriginator_RedoEvent;
        }

        public void Clean()
        {
            LogQueue.WriteToFile("TcMediaComposition.Clean");
            _project.EditorUndoRedoOriginator.UndoEvent -= UndoRedoOriginator_UndoEvent;
            _project.EditorUndoRedoOriginator.RedoEvent -= UndoRedoOriginator_RedoEvent;
            foreach(var mediaClip in _mediaClips)
            {
                UnsubscribeFromMediaClip(mediaClip);
                mediaClip.Clean();
            }
        }

        public void SelectMediaClip(TcMediaClip clip)
        {
            ActiveMediaClipWillBeChanged?.Invoke(clip);
            var selectedClip = MediaClips.FirstOrDefault(c => c.IsSelected);
            if (selectedClip != null)
            {
                selectedClip.IsSelected = false;
            }
            if (clip != null)
            {
                clip.IsSelected = true;
            }
            ActiveMediaClipWasChanged?.Invoke(clip);
        }

        #endregion


        #region Private methods

        private void SubscribeToMediaClip(TcMediaClip mediaClip)
        {
            if (mediaClip == null) return;

            mediaClip.ActiveTakeWasChanged += MediaClip_ActiveTakeWasChanged;
            mediaClip.TcMediaClipWillBeTrimmedEvent += MediaClip_VideoFragmentWillBeTrimmedEvent;
        }

        private void UnsubscribeFromMediaClip(TcMediaClip mediaClip)
        {
            if (mediaClip == null) return;

            mediaClip.ActiveTakeWasChanged -= MediaClip_ActiveTakeWasChanged;
            mediaClip.TcMediaClipWillBeTrimmedEvent -= MediaClip_VideoFragmentWillBeTrimmedEvent;
        }
        private void MediaClip_VideoFragmentWillBeTrimmedEvent(TrimTcMediaClipEventArgs args)
        {
            _project.EditorUndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new TrimTcMediaClipMementoModel(args.TcMediaClipId, args.VideoFragmentId, args.TrimTimeRange));
        }

        private void MediaClip_ActiveTakeWasChanged(TcMediaClip mediaClip, Take oldTake)
        {
            MediaClipWasChanged?.Invoke(mediaClip);
            _project.EditorUndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new TakeMementoModel {TcMediaClipId = mediaClip.Id, TakeId = oldTake.Id });
        }

        private async Task RemoveClipWithoutUndoRedo(TcMediaClip mediaClip)
        {
            if (MediaClipWillBeRemoved != null)
            {
                await MediaClipWillBeRemoved.Invoke(mediaClip);
            }
            UnsubscribeFromMediaClip(mediaClip);
            _mediaClips.Remove(mediaClip);
            mediaClip.IsSelected = false;
            _project.HasUnsavedChanges = true;
            if (MediaClipWasRemoved != null)
            {
                await MediaClipWasRemoved.Invoke(mediaClip);
            }
            if (!_mediaClips.Any())
            {
                MediaCompositionEmptyEvent?.Invoke();
            }
        }

        private async Task CreateClipWithoutUndoRedo(TcMediaClip mediaClip, int index)
        {
            if (MediaClipWillBeCreated != null)
            {
                await MediaClipWillBeCreated.Invoke(null);
            }

            SubscribeToMediaClip(mediaClip);
            _mediaClips.Insert(index, mediaClip);
            _project.HasUnsavedChanges = true;
            if (MediaClipWasCreated != null)
            {
                await MediaClipWasCreated.Invoke(mediaClip);
            }
        }

        #region UndoRedo

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_RedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_UndoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> ProcessUndoRedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            if (mementoModel.MementoObject is TakeMementoModel)
            {
                var takeMementoModel = mementoModel.MementoObject as TakeMementoModel;
                var mediaClip = _mediaClips.FirstOrDefault(c => c.Id == takeMementoModel.TcMediaClipId);
                UnsubscribeFromMediaClip(mediaClip);
                var id = mediaClip?.ChangeActiveTake(takeMementoModel.TakeId);
                MediaClipWasChanged?.Invoke(mediaClip);
                SubscribeToMediaClip(mediaClip);
                if (!String.IsNullOrEmpty(id))
                {
                    mementoModel.MementoObject = new TakeMementoModel { TcMediaClipId = mediaClip.Id, TakeId = id };
                    return mementoModel;
                }
            }
            else if (mementoModel.MementoObject is TcMediaClipMementoModel)
            {
                var mementoCopy = mementoModel.MementoObject as TcMediaClipMementoModel;
                switch (mementoModel.ActionType)
                {
                    case UndoRedoMementoType.Change:
                        var mediaClip = _mediaClips.FirstOrDefault(c => c.Id == mementoCopy.TcMediaClip.Id);
                        var index = await MoveClipWithoutUndoRedo(mediaClip, mementoCopy.MediaClipIndex);
                        mementoModel.MementoObject = new TcMediaClipMementoModel(mediaClip, index);
                        break;
                    case UndoRedoMementoType.Add:
                        await CreateClipWithoutUndoRedo(mementoCopy.TcMediaClip, mementoCopy.MediaClipIndex);
                        break;
                    case UndoRedoMementoType.Remove:
                        await RemoveClipWithoutUndoRedo(mementoCopy.TcMediaClip);
                        break;
                }
                SelectMediaClip(mementoCopy.TcMediaClip);
                return mementoModel;
            }
            else if (mementoModel.MementoObject is SplitTcMediaClipMementoModel)
            {
                if (mementoModel.ActionType != UndoRedoMementoType.Change)
                {
                    return null;
                }

                var mementoCopy = mementoModel.MementoObject as SplitTcMediaClipMementoModel;
                var originalClip = _mediaClips.FirstOrDefault(x => x.Id == mementoCopy.MediaClipId);
                if (originalClip == null)
                {
                    return null;
                }

                var copySecondClip = _mediaClips.FirstOrDefault(x => x.Id == mementoCopy.SecondMediaClipId);
                if (copySecondClip != null)
                {
                    await RemoveClipWithoutUndoRedo(copySecondClip);

                    var startDelta = TimeSpan.Zero;
                    var endDelta = mementoCopy.SecondMediaClip.ActiveTake.VideoFragments.FirstOrDefault().TrimmedDuration;
                    await TrimTimeWitoutUndoRedo(originalClip, startDelta, endDelta);
                }
                else
                {
                    var startDelta = TimeSpan.Zero;
                    var endDelta = mementoCopy.SplitTime - originalClip.ActiveTake.VideoFragments.FirstOrDefault().MediaClip.StartTimeInComposition - originalClip.ActiveTake.VideoFragments.FirstOrDefault().TrimmedDuration;
                    await TrimTimeWitoutUndoRedo(originalClip, startDelta, endDelta);

                    var index = _mediaClips.IndexOf(originalClip) + 1;
                    await CreateClipWithoutUndoRedo(mementoCopy.SecondMediaClip, index);
                }
                SelectMediaClip(originalClip);
                return mementoModel;
            }
            else if (mementoModel.MementoObject is TrimTcMediaClipMementoModel)
            {
                var mementoCopy = mementoModel.MementoObject as TrimTcMediaClipMementoModel;
                if (mementoModel.ActionType != UndoRedoMementoType.Change)
                {
                    return null;
                }
                var originalClip = _mediaClips.FirstOrDefault(x => x.Id == mementoCopy.TcMediaClipId);
                if (originalClip != null)
                {
                    var start = originalClip.ActiveTake.VideoFragments.FirstOrDefault(x => x.Id == mementoCopy.VideoFragmentId).TrimTimeRange.Start;
                    var trimmedDuration = originalClip.ActiveTake.VideoFragments.FirstOrDefault(x => x.Id == mementoCopy.VideoFragmentId).TrimTimeRange.Duration;

                    var startDelta = mementoCopy.TrimTimeRange.Start - start;
                    var endDelta = TimeSpan.Zero;
                    if (startDelta == TimeSpan.Zero)
                    {
                        endDelta = mementoCopy.TrimTimeRange.Duration - trimmedDuration;
                    }
                    mementoCopy.TrimTimeRange = new TimeRange(start, trimmedDuration);
                    await TrimTimeWitoutUndoRedo(originalClip, startDelta, endDelta);

                    return mementoModel;
                }
            }

            return null;
        }
        private async Task TrimTimeWitoutUndoRedo(TcMediaClip clip, TimeSpan startDelta, TimeSpan endDelta)
        {
            clip.TcMediaClipWillBeTrimmedEvent -= MediaClip_VideoFragmentWillBeTrimmedEvent;
            var fragment = clip.ActiveTake.VideoFragments.FirstOrDefault();
            fragment.TrimFromStart(startDelta);
            fragment.TrimFromEnd(-endDelta);
            clip.TcMediaClipWillBeTrimmedEvent += MediaClip_VideoFragmentWillBeTrimmedEvent;
            if (MediaClipWasChanged != null)
            {
                await MediaClipWasChanged.Invoke(clip);
            }
            LogQueue.WriteToFile($"Trim clip: TimeRange[{fragment.TimeRange.Start}, {fragment.TimeRange.Duration}] TrimTimeRange[{fragment.TrimTimeRange.Start}, {fragment.TrimTimeRange.Duration}] MediaClip.TrimmedDuration = {fragment.MediaClip.TrimmedDuration} MediaClip.TrimTimeFromStart = {fragment.MediaClip.TrimTimeFromStart} MediaClip.TrimTimeFromEnd = {fragment.MediaClip.TrimTimeFromEnd}");
        }
        public async Task<TcMediaClip> SplitMediaClipWithoutUndoRedo(TcMediaClip mediaClip, TimeSpan position, string id = null)
        {
            TcMediaClip newClip = null;

            var index = _mediaClips.IndexOf(mediaClip);
            if (index > -1)
            {
                if (MediaClipWillBeSplitted != null)
                {
                    await MediaClipWillBeSplitted.Invoke(mediaClip, null);
                }
                newClip = await mediaClip.Clone(true, id);
                SubscribeToMediaClip(newClip);

                var videoFragment = mediaClip.ActiveTake.VideoFragments.FirstOrDefault();
                var clip = videoFragment.MediaClip;
                var startDelta = position - clip.StartTimeInComposition + videoFragment.HeadTransitionTimeRange.Duration;
                var endDelta = startDelta - clip.TrimmedDuration - videoFragment.HeadTransitionTimeRange.Duration;
                await TrimTimeWitoutUndoRedo(mediaClip, TimeSpan.Zero, endDelta);
                await TrimTimeWitoutUndoRedo(newClip, startDelta, TimeSpan.Zero);

                _mediaClips.Insert(index + 1, newClip);
                _project.HasUnsavedChanges = true;
                if (MediaClipWasSplitted != null)
                {
                    await MediaClipWasSplitted.Invoke(mediaClip, newClip);
                }
                LogQueue.WriteToFile($"Old clip: TimeRange[{videoFragment.TimeRange.Start}, {videoFragment.TimeRange.Duration}] TrimTimeRange[{videoFragment.TrimTimeRange.Start}, {videoFragment.TrimTimeRange.Duration}] MediaClip.TrimmedDuration = {videoFragment.MediaClip.TrimmedDuration}");
                videoFragment = newClip.ActiveTake.VideoFragments.FirstOrDefault();
                LogQueue.WriteToFile($"New clip: TimeRange[{videoFragment.TimeRange.Start}, {videoFragment.TimeRange.Duration}] TrimTimeRange[{videoFragment.TrimTimeRange.Start}, {videoFragment.TrimTimeRange.Duration}] MediaClip.TrimmedDuration = {videoFragment.MediaClip.TrimmedDuration}");
            }
            return newClip;
        }
        public async Task<int> MoveClipWithoutUndoRedo(TcMediaClip mediaClip, int newIndex)
        {
            if (MediaClipWillBeMoved != null)
            {
                await MediaClipWillBeMoved.Invoke(mediaClip, newIndex);
            }
            var index = _mediaClips.IndexOf(mediaClip);
            _mediaClips.Remove(mediaClip);
            _project.HasUnsavedChanges = true;
            _mediaClips.Insert(newIndex, mediaClip);
            if (MediaClipWasMoved != null)
            {
                await MediaClipWasMoved.Invoke(mediaClip, newIndex);
            }
            return index;
        }

        #endregion
        #endregion
    }
}
