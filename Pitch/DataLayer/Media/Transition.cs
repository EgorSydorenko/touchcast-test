﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.Editing;

namespace Pitch.DataLayer
{
    public class PrepareTransitionResult
    {
        public List<MediaOverlay> Overlays { get; set; }
        public MediaClip MediaClip { get; set; }
    }

    public class Transition
    {
        public enum Type
        {
            None,
            Crossfade,
            FadeThroughBlack,
            FadeThroughWhite
        }

        private Type _type;
        private MediaClip _mediaClip;
        private static readonly TimeSpan _duration;

        public TimeSpan Duration => _type == Type.None ? TimeSpan.Zero : _duration;

        public TimeSpan IncreaseTime
        {
            get
            {
                var result = TimeSpan.Zero;
                switch (_type)
                {
                    case Transition.Type.None:
                        break;
                    case Transition.Type.Crossfade:
                        result = _duration;
                        break;
                    case Transition.Type.FadeThroughBlack:
                        result = TimeSpan.FromSeconds(_duration.TotalSeconds / 2);
                        break;
                    case Transition.Type.FadeThroughWhite:
                        result = TimeSpan.FromSeconds(_duration.TotalSeconds / 2);
                        break;
                }
                return result;
            }
        }

        public Type Kind
        {
            set => _type = value;
            get => _type;
        }

        public MediaClip MediaClip => _mediaClip;
        public TimeSpan StartTimeInComposition => (_type == Type.None || _mediaClip == null) ? TimeSpan.MinValue : _mediaClip.StartTimeInComposition;
        public TimeSpan EndTimeInComposition => (_type == Type.None || _mediaClip == null) ? TimeSpan.MinValue : _mediaClip.EndTimeInComposition;

        static Transition()
        {
            _duration = TimeSpan.FromSeconds(0.5);
        }

        public Transition()
        {
            _type = Type.None;
        }

        public bool TryCreateTransitionBetweenShots(TcMediaClip parentMediaClip, TcMediaClip nextMediaClip, Transition.Type transitionType)
        {
            switch (transitionType)
            {
                case Type.None:
                    {
                        _type = transitionType;
                        return true;
                    }
                case Type.Crossfade:
                case Type.FadeThroughBlack:
                case Type.FadeThroughWhite:
                    if (parentMediaClip.ActiveTake.VideoFragments.Last()?.MediaClip?.TrimmedDuration > _duration
                           && nextMediaClip?.ActiveTake.VideoFragments.First()?.MediaClip?.TrimmedDuration > _duration)
                    {
                        _type = transitionType;
                        return true;
                    }
                    break;
            }
            return false;
        }

        public PrepareTransitionResult PrepareCrossFadeTranstion(VideoFragment previousVideoFragment, VideoFragment currentVideoFragment)
        {
            _mediaClip = previousVideoFragment.MediaClip.Clone();
            _mediaClip.TrimTimeFromStart = previousVideoFragment.TrimTimeRange.Start + previousVideoFragment.TrimmedDuration - _duration;
            _mediaClip.TrimTimeFromEnd = (_mediaClip.OriginalDuration - (previousVideoFragment.TimeRange.Start + previousVideoFragment.Duration)) + previousVideoFragment.TrimTimeFromEnd;
            _mediaClip.Volume = 1;
            previousVideoFragment.MediaClip.TrimTimeFromEnd = previousVideoFragment.MediaClip.OriginalDuration - (previousVideoFragment.TrimTimeRange.Start + previousVideoFragment.TrimTimeRange.Duration) + _duration;

            var currentMediaClipCopy = currentVideoFragment.MediaClip.Clone();
            currentMediaClipCopy.TrimTimeFromStart = currentVideoFragment.TrimTimeRange.Start;
            currentMediaClipCopy.TrimTimeFromEnd = currentMediaClipCopy.OriginalDuration - (currentVideoFragment.TimeRange.Start + currentVideoFragment.Duration) +
                currentVideoFragment.TrimmedDuration + currentVideoFragment.TrimTimeFromEnd - _duration;
            currentVideoFragment.MediaClip.TrimTimeFromStart = currentVideoFragment.TrimTimeRange.Start + _duration;

            return new PrepareTransitionResult
            {
                Overlays = new List<MediaOverlay>() { CreateOverlay(currentMediaClipCopy, previousVideoFragment.MediaClip.EndTimeInComposition, 0.0) },
                MediaClip = _mediaClip
            };

        }

        public PrepareTransitionResult PrepareFadeThroughColorTransition(VideoFragment previousVideoFragment, VideoFragment currentVideoFragment, Windows.UI.Color color)
        {
            var halfDuration = TimeSpan.FromSeconds(_duration.TotalSeconds / 2);
            var fadeInClip = previousVideoFragment.MediaClip.Clone();
            fadeInClip.TrimTimeFromEnd = (fadeInClip.OriginalDuration - (previousVideoFragment.TimeRange.Start + previousVideoFragment.Duration)) + previousVideoFragment.TrimTimeFromEnd;
            var fadeOutClip = currentVideoFragment.MediaClip.Clone();
            fadeOutClip.TrimTimeFromStart = currentVideoFragment.TrimTimeRange.Start;

            _mediaClip = MediaClip.CreateFromColor(color, _duration);

            previousVideoFragment.MediaClip.TrimTimeFromEnd = previousVideoFragment.MediaClip.OriginalDuration - (previousVideoFragment.TrimTimeRange.Start + previousVideoFragment.TrimmedDuration) + halfDuration;
            currentVideoFragment.MediaClip.TrimTimeFromStart = currentVideoFragment.TrimTimeRange.Start + halfDuration;

            fadeInClip.TrimTimeFromStart = fadeInClip.TrimTimeFromStart + (fadeInClip.TrimmedDuration - halfDuration);
            fadeOutClip.TrimTimeFromEnd = fadeOutClip.TrimTimeFromEnd + (fadeOutClip.TrimmedDuration - halfDuration);

            var fadeInOverlay = CreateOverlay(fadeInClip, previousVideoFragment.MediaClip.EndTimeInComposition, 1.0);
            var fadeOutOverlay = CreateOverlay(fadeOutClip, previousVideoFragment.MediaClip.EndTimeInComposition + halfDuration, 0.0);
            fadeInOverlay.Clip.Volume = 1;
            fadeOutOverlay.Clip.Volume = 1;

            fadeInOverlay.AudioEnabled = true;
            fadeOutOverlay.AudioEnabled = true;

            return new PrepareTransitionResult
            {
                Overlays = new List<MediaOverlay>() { fadeInOverlay, fadeOutOverlay },
                MediaClip = _mediaClip
            };
        }

        public void Clean()
        {
            _mediaClip = null;
        }

        private static MediaOverlay CreateOverlay(MediaClip clip, TimeSpan delay, double opacity)
        {
            var videoOverlay = new MediaOverlay(clip);
            videoOverlay.Position = new Windows.Foundation.Rect(0, 0, 1280, 720);
            videoOverlay.Delay = delay;
            videoOverlay.Opacity = opacity;
            return videoOverlay;
        }


        public static Transition.Type ConvertFromString(string str)
        {
            Transition.Type result = Transition.Type.None;
            if (str == "crossfade")
            {
                result = Transition.Type.Crossfade;
            }
            else if (str == "fadetoblack")
            {
                result = Transition.Type.FadeThroughBlack;
            }
            else if (str == "fadetowhite")
            {
                result = Transition.Type.FadeThroughWhite;
            }
            return result;
        }

        public static string ConvertToString(Transition.Type s1)
        {
            string result = String.Empty;
            switch (s1)
            {
                case Transition.Type.None:
                    break;
                case Transition.Type.Crossfade:
                    result = "crossfade";
                    break;
                case Transition.Type.FadeThroughBlack:
                    result = "fadetoblack";
                    break;
                case Transition.Type.FadeThroughWhite:
                    result = "fadetowhite";
                    break;
            }
            return result;
        }
    }
}
