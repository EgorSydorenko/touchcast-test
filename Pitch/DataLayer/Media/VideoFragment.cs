﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Pitch.Models;
using Pitch.Helpers.Logger;
using Windows.Media.Editing;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.DataLayer
{
    public enum VideoFragmentState
    {
        Created,
        Loaded
    }

    public class TimeRange
    {
        TimeSpan _start;
        TimeSpan _duration;
        public TimeRange() { }
        public TimeRange(TimeSpan start, TimeSpan duration)
        {
            _start = start;
            _duration = duration;
        }
        public TimeSpan Start
        {
            set => _start = value;
            get => _start;
        }
        public TimeSpan Duration
        {
            set => _duration = value;
            get => _duration;
        }
    }
    public class VideoFragment
    {
        #region Static props and Constants
        public static readonly TimeSpan MinSplitDuration = TimeSpan.FromSeconds(0.5);
        #endregion

        #region Privat fields

        private string _id;
        private Recording _recording;
        private TimeRange _headTransitionTimeRange;
        private TimeRange _tailTransitionTimeRange;
        private TimeRange _timeRange;
        private TimeRange _trimTimeRange;
        private MediaClip _mediaClip;
        private VideoFragmentState _state;
        private readonly WeakReference<Take> _take;
        private int _index;
        private readonly BitmapImage _thumbnailImage = new BitmapImage();
        #endregion

        public delegate void VideoFragmentTrimRangeChanged(VideoFragment sender);
        public event VideoFragmentTrimRangeChanged VideoFragmentTrimRangeWillChangedEvent;
        public event VideoFragmentTrimRangeChanged VideoFragmentTrimRangeWasChangedEvent;

        #region Public properties
        public string Id => _id;
        public string Type => "clip";
        public Recording Recording => _recording;
        public VideoFragmentState State => _state;

        public Take Take => _take?.TryGetObject();
        public TimeRange TimeRange
        {
            set => _timeRange = value;
            get => _timeRange;
        }

        public MediaClip MediaClip => _mediaClip;

        public TimeRange TrimTimeRange => _trimTimeRange;
        public TimeSpan TrimmedDuration => _trimTimeRange.Duration;
        public TimeSpan TrimTimeFromEnd
        {
            get
            {
                var result = _timeRange.Duration - _trimTimeRange.Duration - (_trimTimeRange.Start - _timeRange.Start);
                if (result < TimeSpan.Zero)
                {
                    result = TimeSpan.Zero;
                }
                return result;
            }
        }

        public TimeSpan Duration => _timeRange.Duration;

        public TimeRange HeadTransitionTimeRange => _headTransitionTimeRange;
        public TimeRange TailTransitionTimeRange => _tailTransitionTimeRange;

        public int Index { get => _index; set => _index = value; }
        public BitmapImage ThumbnailImage
        {
            get { return _thumbnailImage; }
        }
        #endregion

        #region Life cycle
        public static async Task<VideoFragment> CreateFromMetaData(Take take, TcpInfo.CompositionItem item, Recording recording)
        {
            VideoFragment fragment = new VideoFragment(take);
            fragment._timeRange.Start = TimeSpan.FromSeconds(item.max_range.start);
            fragment._timeRange.Duration = TimeSpan.FromSeconds(item.max_range.duration);
            fragment._trimTimeRange.Start = TimeSpan.FromSeconds(item.range.start);
            fragment._trimTimeRange.Duration = TimeSpan.FromSeconds(item.range.duration);
            var result = await fragment.LoadMedia(recording);
            if(result == false)
            {
                fragment = null;
            }
            return fragment;
        }

        public VideoFragment(Take parentTake) : this(parentTake, new TimeRange(), new TimeRange())
        {

        }

        public VideoFragment(Take parentTake, TimeRange timeRange, TimeRange trimTimeRange, string id = null)
        {
            _id = (id == null) ? Guid.NewGuid().ToString() : id;
            _state = VideoFragmentState.Created;
            _take = new WeakReference<Take>(parentTake);
            _headTransitionTimeRange = new TimeRange(TimeSpan.Zero, TimeSpan.Zero);
            _tailTransitionTimeRange = new TimeRange(TimeSpan.Zero, TimeSpan.Zero);
            _timeRange = timeRange;
            _trimTimeRange = trimTimeRange;
        }

#if DEBUG
        ~VideoFragment()
        {
            System.Diagnostics.Debug.WriteLine("********** VideoFragment Destructor *****************");
        }
#endif
        #endregion

        #region Public

        public async Task<bool> LoadMedia(Recording recording)
        {
            bool result = true;
            try
            {
                _recording = recording;
                _mediaClip = await MediaClip.CreateFromFileAsync(_recording.File);
                if (_mediaClip.OriginalDuration < _timeRange.Start + _timeRange.Duration)
                {
                    _timeRange.Duration = _mediaClip.OriginalDuration - _timeRange.Start;
                }
                if (_mediaClip.OriginalDuration < _trimTimeRange.Start + _trimTimeRange.Duration)
                {
                    _trimTimeRange.Duration = _mediaClip.OriginalDuration - _trimTimeRange.Start;
                }
                _mediaClip.TrimTimeFromStart = _trimTimeRange.Start;
                _mediaClip.TrimTimeFromEnd = _mediaClip.OriginalDuration - (_trimTimeRange.Start + _trimTimeRange.Duration);
                _state = VideoFragmentState.Loaded;

                var composition = new MediaComposition();
                try
                {
                    composition.Clips.Add(_mediaClip);
                    var stream = await composition.GetThumbnailAsync(TimeSpan.Zero, 320, 180, VideoFramePrecision.NearestKeyFrame);
                    if (stream != null)
                    {
                        _thumbnailImage.DecodePixelHeight = 180;
                        _thumbnailImage.SetSource(stream);
                    }
                }
                finally
                {
                    composition.Clips.Clear();
                }
            }
            catch (Exception ex)
            {
                result = false;
                LogQueue.WriteToFile($"Exception in VideoFragment.LoadMedia : {ex.Message}");
            }
            return result;
        }

        public async Task<VideoFragment> Clone(Take take = null, string id = null)
        {
            if (take == null)
            {
                take = Take;
            }
            var newVideoFragment = new VideoFragment(take, new TimeRange(_timeRange.Start, _timeRange.Duration), new TimeRange(_trimTimeRange.Start, _trimTimeRange.Duration));
            if (!String.IsNullOrEmpty(id))
            {
                newVideoFragment._id = id;
            }
            newVideoFragment._index = _index;
            var actionStreamCopy = _recording.ActionsStream.Clone();
            var newRocording = new Recording(_recording.File, _recording.ActionsXml, _recording.Duration, actionStreamCopy);
            await newVideoFragment.LoadMedia(_recording);

            return newVideoFragment;
        }

        public void TrimFromStart(TimeSpan timeDelta)
        {
            VideoFragmentTrimRangeWillChangedEvent?.Invoke(this);
            _trimTimeRange.Start += timeDelta;
            _trimTimeRange.Duration -= timeDelta;
            if (_mediaClip != null)
            {
                var time = _mediaClip.TrimTimeFromStart + timeDelta;
                if (time < TimeSpan.Zero) time = TimeSpan.Zero;

                _mediaClip.TrimTimeFromStart = time;
            }
            Take?.UpdateProjectHasUnsavedChanges();
            VideoFragmentTrimRangeWasChangedEvent?.Invoke(this);
        }

        public void TrimFromEnd(TimeSpan timeDelta)
        {
            VideoFragmentTrimRangeWillChangedEvent?.Invoke(this);
            _trimTimeRange.Duration -= timeDelta;
            if (_mediaClip != null)
            {
                var time = _mediaClip.TrimTimeFromEnd + timeDelta;
                if (time < TimeSpan.Zero) time = TimeSpan.Zero;

                _mediaClip.TrimTimeFromEnd = time;
            }
            Take?.UpdateProjectHasUnsavedChanges();
            VideoFragmentTrimRangeWasChangedEvent?.Invoke(this);
        }

        public void UpdateStartTime(TimeSpan time)
        {
            _timeRange.Start = time;
            _trimTimeRange.Start = time;
        }

        public TcpInfo.CompositionItem GetMetaData()
        {
            TcpInfo.CompositionItem meta = new TcpInfo.CompositionItem();
            meta.max_range = new TcpInfo.Range();
            meta.max_range.start = _timeRange.Start.TotalSeconds;
            meta.max_range.duration = _timeRange.Duration.TotalSeconds;

            meta.range = new TcpInfo.Range();
            meta.range.start = _trimTimeRange.Start.TotalSeconds;
            meta.range.duration = _trimTimeRange.Duration.TotalSeconds;
            meta.recording_id = _recording.Id;
            return meta;
        }

        public void UpdateEndTime(TimeSpan time)
        {
            _timeRange.Duration = time - _timeRange.Start;
            _trimTimeRange.Duration = time - _trimTimeRange.Start;
        }
        public void UpdateHeadTransition(TimeSpan time)
        {
            if (time == TimeSpan.Zero && _headTransitionTimeRange.Duration != TimeSpan.Zero)
            {
                MediaClip.TrimTimeFromStart = MediaClip.TrimTimeFromStart - _headTransitionTimeRange.Duration;
            }
            _headTransitionTimeRange.Start = TimeSpan.Zero;
            _headTransitionTimeRange.Duration = time;
        }

        public void UpdateTailTransition(TimeSpan time)
        {
            if (time == TimeSpan.Zero && _tailTransitionTimeRange.Duration != TimeSpan.Zero)
            {
                MediaClip.TrimTimeFromEnd = MediaClip.TrimTimeFromEnd - _tailTransitionTimeRange.Duration;
            }
            _tailTransitionTimeRange.Start = _trimTimeRange.Start + _trimTimeRange.Duration - time;
            _tailTransitionTimeRange.Duration = time;
        }

        #endregion
    }
}
