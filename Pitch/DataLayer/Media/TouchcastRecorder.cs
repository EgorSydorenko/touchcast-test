﻿using System.Collections.Generic;
using System.Linq;
using Pitch.DataLayer.Layouts;
using Pitch.Models;
using Pitch.Services;

namespace Pitch.DataLayer
{
    public enum AppMessageType
    {
        AddApp,
        BringToFrontApp,
        NavigateApp,
        MoveApp,
        CloseApp,
        PageChanged
    }

    public enum TouchcastEventType
    {
        SceneChanged,
        TextAvailable,
        UserTalking,
        UserStoppedTalking,
        ObjectDetected
    }

    class CommandComparer : IEqualityComparer<Command>
    {
        public bool Equals(Command x, Command y)
        {
            if (x.Id == y.Id && x.Type == y.Type && x.Time == y.Time) return true;
            return false;
        }

        public int GetHashCode(Command obj)
        {
            return obj.GetHashCode();
        }
    }

    public class TouchcastRecorder
    {
        private List<CommandInformation> _commandInfoList = new List<CommandInformation>();
        private static TouchcastRecorder _actionStreamRecorder;
        private bool _isRecordingPaused;
        private long _indexator = 1;
        private Touchcast _touchcastUploadData;

        public static TouchcastRecorder SharedInstance => _actionStreamRecorder;
        public Touchcast TouchcastUploadData => _touchcastUploadData;

        public static TouchcastRecorder InitializeActionStreamRecorder()
        {
            if(_actionStreamRecorder == null)
            {
                _actionStreamRecorder = new TouchcastRecorder();
            }
            return _actionStreamRecorder;
        }

        public TouchcastRecorder()
        {
            _touchcastUploadData = new Touchcast();
        }

        #region Creation actionStream Commands

        #region Public Methods

        public void Start(LayerLayout vapp)
        {
            if (_isRecordingPaused)
            {
                _isRecordingPaused = false;
            }
            else
            {
                ResetRecorder();
                CreateNewActionStream();
                _isRecordingPaused = false;
            }   
        }

        public void Pause()
        {
            _isRecordingPaused = true;
        }

        public void Finish(double elapsedSeconds)
        {
            _isRecordingPaused = false;
            var commandsToRemove =  _touchcastUploadData.ActionsStream.Commands.Where(x => x.Type == CommandTypes.Close && x.Time == 0).ToList();
            foreach (var closeCommand in commandsToRemove)
            {
                var toRemove = _touchcastUploadData.ActionsStream.Commands.Where(x => x.Id == closeCommand.Id).ToList();
                foreach (var removeCommand in toRemove)
                {
                    _touchcastUploadData.ActionsStream.Commands.Remove(removeCommand);
                }
            }

            var groupedCommands = _touchcastUploadData.ActionsStream.Commands.GroupBy(x => x.Id).ToList();

            foreach (var commands in groupedCommands)
            {
                var groupedMoveCommands = commands.Where(c => c.Type == CommandTypes.Move).GroupBy(x => x.Time).ToList();
                
                foreach (var moveCommands in groupedMoveCommands)
                {
                    var orderedMoveCommands = moveCommands.OrderByDescending(x => x.Index).ToList();
                    int index = 0;
                    foreach (var commmand in orderedMoveCommands)
                    {
                        if (index == 0)
                        {
                            index++;
                            continue;
                        }
                        _touchcastUploadData.ActionsStream.Commands.Remove(commmand);
                    }
                }
            }
        }

        public void PushNewVappMessage(int id, AppMessageType messageType, LayerLayout activeApp, double elapsedSeconds)
        {
            switch (messageType)
            {
                case AppMessageType.AddApp:
                    CreateAction(id, activeApp, elapsedSeconds);
                    break;
                case AppMessageType.BringToFrontApp:
                    BringToFrontAction(id, activeApp, elapsedSeconds);
                    break;
                case AppMessageType.CloseApp:
                    CloseAction(id, activeApp, elapsedSeconds,false);
                    break;
                case AppMessageType.NavigateApp:
                    NavigateCommand(id, activeApp, elapsedSeconds);
                    break;
                case AppMessageType.MoveApp:
                    MoveAction(id, activeApp, elapsedSeconds);
                    break;
                case AppMessageType.PageChanged:
                    PageChangedAction(id, activeApp, elapsedSeconds);
                    break;
            }
        }
        public void PushNewEvent(TouchcastEventType type, double elapsedSeconds, object contents)
        {
            var eventType = string.Empty;
            switch (type)
            {
                case TouchcastEventType.ObjectDetected:
                    eventType = EventTypes.ObjectDetected;
                    break;
                case TouchcastEventType.SceneChanged:
                    eventType = EventTypes.SceneChanged;
                    break;
                case TouchcastEventType.TextAvailable:
                    eventType = EventTypes.TextAvailable;
                    break;
                case TouchcastEventType.UserStoppedTalking:
                    eventType = EventTypes.UserStoppedTalking;
                    break;
                case TouchcastEventType.UserTalking:
                    eventType = EventTypes.UserTalking;
                    break;
            }

            _touchcastUploadData.ActionsStream.Events.Add(new Event
            {
                Type = eventType, Time = elapsedSeconds, Contents = contents
            });
        }
        #endregion

        #region Private Methods
        private void CreateNewActionStream()
        {
            _touchcastUploadData.ActionsStream.OriginalVideoSize = new VideoSize { Height = MediaMixerService.VideoSize.Height, Width = MediaMixerService.VideoSize.Width };        
        }

        private Command CreateNewCommand(int id, LayerLayout vapp, string commandType, double timeOfAction = 0)
        {
            var cmd = new Command
            {
                Type = commandType,
                Id = id,
                ResourceId = vapp.Id,
                Url = vapp.ContentUrl(),
                ThumbnailUrl = vapp.ThumbnailUrl(),
                Title = string.Empty,
                Time = timeOfAction,
                PauseOnInteration = vapp.IsPauseOnPlayback ? "YES" : "NO",
                Index = _indexator++,
                Transform = vapp.TouchcastTransform,
                Widget = vapp.IsWidget(),
                ContentType = vapp.ContentType(),
                UrlProperties = vapp.UrlProperties()
            };

            //if(String.Is vapp.InteractivityUrl)
            //Debug.WriteLine($"<Command id={cmd.Id} Type={commandType} Url={cmd.Url} Time={cmd.Time} Transform={cmd.Transform}>");
            //var cmd = new Command
            //{
            //    Type = commandType,
            //    Id= vapp.ViewId,
            //    ResourceId = vapp.LayerLayoutId,
            //    Url = vapp.File == null ? vapp.Url : vapp.File.Name,
            //    ThumbnailUrl = vapp.ThumbnailFile?.Name,
            //    Time = timeOfAction,
            //    Title = vapp.Title,
            //    PauseOnInteration = "YES",
            //    Index = _indexator++
            //};

            //cmd.ContentType = vapp.DraggableRecordableItemViewModelContentType();
            //cmd.Widget = vapp.DraggableRecordableItemViewModelIsWidget();
            //cmd.Url = vapp.DraggableRecordableItemViewModelContentUrl();
            //cmd.ThumbnailUrl = vapp.DraggableRecordableItemViewModelThumbnailUrl();
            //cmd.Transform = vapp.TouchcastTransform;
            //var recource = new TouchcastResource
            //{
            //    Resource = vapp.DraggableRecordableItemViewModelResource(),
            //    ID = vapp.RecordableItemId,
            //    Name = vapp.Title,
            //    Thumbnail = vapp.ThumbnailFile,
            //    Type = vapp.Type,
            //    Title = vapp.Title
            //};

            //_touchcastUploadData.AddeNewRecourceIfNeeded(recource);

            return cmd;
        }

        private void NavigateCommand(int id,LayerLayout vapp, double elapsedSeconds)
        {
            if (_touchcastUploadData.ActionsStream.Commands.Count > 0)
            {
                _touchcastUploadData.ActionsStream.Commands.Add(CreateNewCommand(id, vapp, CommandTypes.Navigate, elapsedSeconds));
            }
        }

        private void CreateAction(int id, LayerLayout activeApp, double elapsedSeconds)
        {
            _commandInfoList.Add(new CommandInformation { CommandName = CommandTypes.Create, IsForcedUrl = false, ElapsedSeconds = elapsedSeconds });
            ProceedNewActionStreamCommands(id, activeApp, _commandInfoList);
            _commandInfoList.Clear();
        }

        private void BringToFrontAction(int id, LayerLayout activeApp, double elapsedSeconds)
        {
            _commandInfoList.Add(new CommandInformation { CommandName = CommandTypes.BringToFront, IsForcedUrl = false, ElapsedSeconds = elapsedSeconds });
            ProceedNewActionStreamCommands(id, activeApp, _commandInfoList);
            _commandInfoList.Clear();
        }

        private void CloseAction(int id, LayerLayout activeApp, double elapsedSeconds, bool isForcedUrl)
        {
            _commandInfoList.Add(new CommandInformation { CommandName = CommandTypes.Close, IsForcedUrl = false, ElapsedSeconds = elapsedSeconds });
            ProceedNewActionStreamCommands(id, activeApp, _commandInfoList);
            _commandInfoList.Clear();
        }
        private void MoveAction(int id, LayerLayout activeApp, double elapsedSeconds)
        {
            _commandInfoList.Add(new CommandInformation { CommandName = CommandTypes.Move, IsForcedUrl = false, ElapsedSeconds = elapsedSeconds });
            ProceedNewActionStreamCommands(id, activeApp, _commandInfoList);
            _commandInfoList.Clear();
        }

        private void PageChangedAction(int id, LayerLayout activeApp, double elapsedSeconds)
        {
            _commandInfoList.Add(new CommandInformation { CommandName = CommandTypes.PageChanged, IsForcedUrl = false, ElapsedSeconds = elapsedSeconds });
            ProceedNewActionStreamCommands(id, activeApp, _commandInfoList);
            _commandInfoList.Clear();
        }
        private void ProceedNewActionStreamCommands(int id, LayerLayout activevapp, List<CommandInformation> commandInfoList)
        {
            foreach (var commandObject in commandInfoList)
            {
                _touchcastUploadData.ActionsStream.Commands.Add(CreateNewCommand(id, activevapp,
                    commandObject.CommandName,
                    commandObject.ElapsedSeconds));
            }
        }
        //private void ProceedNewActionStreamEvents(List<CommandInformation> commandInfoList)
        //{
        //    foreach (var commandObject in commandInfoList)
        //    {
        //        _touchcastUploadData.ActionsStream.Add(CreateNewCommand(id, activevapp,
        //            commandObject.CommandName,
        //            commandObject.ElapsedSeconds));
        //    }
        //}

        #endregion

        #endregion

        public void ResetRecorder()
        {
            _touchcastUploadData.Reset();    
        }
    }

    class CommandInformation
    {
        public string CommandName { set; get; }
        public bool IsForcedUrl { set; get; }
        public double ElapsedSeconds { set; get; }
    }
}
