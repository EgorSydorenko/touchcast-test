﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.DataLayer.UndoRedo;
using Pitch.Helpers.Logger;

namespace Pitch.DataLayer
{
    public class TakeMementoModel : ICleanable
    {
        private string _tcMediaClipId;
        private string _takeId;

        public string TcMediaClipId { get { return _tcMediaClipId; } set { _tcMediaClipId = value; } }
        public string TakeId { get { return _takeId; } set { _takeId = value; } }

        public void Clean()
        {

        }
    }

    public class TcMediaClipMementoModel : ICleanable
    {
        private TcMediaClip _mediaClip;
        private int _mediaClipIndex;
        public TcMediaClip TcMediaClip => _mediaClip;
        public int MediaClipIndex => _mediaClipIndex;
        public TcMediaClipMementoModel(TcMediaClip mediaClip, int mediaClipIndex)
        {
            _mediaClip = mediaClip;
            _mediaClipIndex = mediaClipIndex;
        }
        public void Clean()
        {

        }
    }

    public class SplitTcMediaClipMementoModel : ICleanable
    {
        private string _mediaClipId;
        private TcMediaClip _secondMediaClip;
        private TimeSpan _splitTime;
        private TimeRange _trimTimeRange;

        public string MediaClipId => _mediaClipId;
        public string SecondMediaClipId => _secondMediaClip.Id;
        public TcMediaClip SecondMediaClip => _secondMediaClip;
        public TimeSpan SplitTime { set { _splitTime = value; } get { return _splitTime; } }
        public TimeRange TrimTimeRange { set { _trimTimeRange = value; } get { return _trimTimeRange; } }

        public SplitTcMediaClipMementoModel(TcMediaClip mediaClip, TcMediaClip secondMediaClip)
        {
            _mediaClipId = mediaClip.Id;
            _secondMediaClip = secondMediaClip;
        }

        public void Clean()
        {
        }
    }
    public class TrimTcMediaClipMementoModel : ICleanable
    {
        private string _tcMediaClipId;
        private string _videoFragmentId;
        private TimeRange _trimTimeRange;

        public string TcMediaClipId => _tcMediaClipId;
        public string VideoFragmentId => _videoFragmentId;
        public TimeRange TrimTimeRange {set{ _trimTimeRange = value; } get{ return _trimTimeRange; }}

        public TrimTcMediaClipMementoModel(string tcMediaClipId, string videoFragmentId, TimeRange trimTimeRange)
        {
            _tcMediaClipId = tcMediaClipId;
            _videoFragmentId = videoFragmentId;
            _trimTimeRange = trimTimeRange;
        }

        public void Clean()
        {

        }
    }

    public class TrimTcMediaClipEventArgs : TrimVideoFragmentEventArgs
    {
        private string _tcMediaClipId;
        public string TcMediaClipId => _tcMediaClipId;
        public TrimTcMediaClipEventArgs(TcMediaClip mediaClip, TrimVideoFragmentEventArgs args) : base(args)
        {
            _tcMediaClipId = mediaClip.Id;
        }
    }

    public delegate void ActiveTakeEvent(TcMediaClip mediaClip, Take oldTake);
    public delegate void TakeEvent(Take take);
    public delegate void TcMediaClipTrimEvent(TrimTcMediaClipEventArgs args);
    public class TcMediaClip
    {
        #region Private fields
        private Transition _transition = new Transition();
        private string _id;
        private List<Take> _takes = new List<Take>();
        private Take _activeTake;
        #endregion

        public event ActiveTakeEvent ActiveTakeWillBeChanged;
        public event ActiveTakeEvent ActiveTakeWasChanged;
        public event TcMediaClipTrimEvent TcMediaClipWillBeTrimmedEvent;

        public List<Take> Takes => _takes;
        public Take ActiveTake
        {
            get => _activeTake;
            
            private set
            {
                if (_activeTake?.Id != value?.Id)
                {
                    var oldTake = _activeTake;
                    ActiveTakeWillBeChanged?.Invoke(this, oldTake);

                    _activeTake = value;

                    ActiveTakeWasChanged?.Invoke(this, oldTake);
                }
            }
        }

        public Transition Transition => _transition;
        public string Id => _id;

        public bool IsSelected { get; set; }

        #region Life cycle
        public TcMediaClip(Shot shot) : this()
        {
            var take = CreateTake(shot);
            _activeTake = take;
        }
        private TcMediaClip(string id = null)
        {
            _id = id;
            if (String.IsNullOrEmpty(_id))
            {
                _id = Guid.NewGuid().ToString();
            }
            LogQueue.WriteToFile($"Create TcMediaClip {_id}");
        }

        public static async Task<TcMediaClip> Create(List<TcpInfo.CompositionItem> videofragments, List<Recording> recordings ,Project project)
        {
            if (videofragments != null && videofragments.Count > 0)
            {
                TcMediaClip mediaCLip = new TcMediaClip();
                var takesCount = videofragments.Max(x => x.take) + 1;

                for (int i = 0; i < takesCount; i++)
                {
                    try
                    {
                        var videoFragmentsForCurrenTake = videofragments.Where(t => t.take == i).ToList();
                        var item = videoFragmentsForCurrenTake.FirstOrDefault();
                        Shot shot = null;
                        if (item.scene_index.HasValue && item.scene_index.Value < project.Shots.Count)
                        {
                            shot = project.Shots[item.scene_index.Value];
                        }
                        if (videoFragmentsForCurrenTake.Any())
                        {
                            var take = await Take.CreateFromMetaData(shot, mediaCLip, videoFragmentsForCurrenTake, recordings);
                            if (take != null)
                            {
                                take.Subscribe();
                                mediaCLip._takes.Add(take);
                                if (item.active != 0)
                                {
                                    mediaCLip.ActiveTake = take;
                                }
                            }
                        }
                        if(mediaCLip.ActiveTake == null)
                        {
                            mediaCLip.ActiveTake = mediaCLip.Takes.FirstOrDefault();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Task<Shot> Create : {ex.Message}");
                    }
                }

                if(mediaCLip != null && !mediaCLip._takes.Any() && mediaCLip.ActiveTake != null)
                {
                    mediaCLip = null;
                }

                if (mediaCLip != null)
                {
                    try
                    {
                        var itemWithTransition = videofragments.Where(x => x.out_transition != null).FirstOrDefault();
                        if (itemWithTransition != null)
                        {
                            var type = Transition.ConvertFromString(itemWithTransition.out_transition.type);
                            if (type != Transition.Type.Crossfade)
                            {
                                mediaCLip._transition.Kind = type;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Task<Shot> Create : {ex.Message}");
                    }
                }
                return mediaCLip;
            }
            return null;
        }

        public void Clean()
        {
            foreach(var take in _takes)
            {
                take.VideoFragmentWillBeTrimmedEvent -= ActiveTake_VideoFragmentWillBeTrimmedEvent;
                take.Clean();
            }
            _transition.Clean();
            _takes.Clear();
            _activeTake = null;
        }

#if DEBUG
        ~TcMediaClip()
        {
            System.Diagnostics.Debug.WriteLine("********** TcMediaClip Destructor *****************");
        }
#endif
        #endregion

        #region Public methods
        public Take CreateTake(Shot shot)
        {
            var take = new Take(shot, this);
            take.CreateVideoFragment();
            take.Subscribe();
            AddTake(take);
            _activeTake = take;
            LogQueue.WriteToFile($"CreateTake {take.Id} TcMediaClip {_id} for Shot {shot.Id}. Total takes: {_takes.Count}");

            return take;
        }

        public async Task<TcMediaClip> Clone(bool justActiveTake = false, string id = null)
        {
            LogQueue.WriteToFile($"Clone TcMediaClip {_id}. " + (justActiveTake ? "Only active take" : "All takes"));

            var clone = new TcMediaClip(id);
            if (justActiveTake)
            {
                var take = await _activeTake.Clone(_activeTake.Shot, clone);
                clone.AddTake(take);
                clone.ActiveTake = take;
            }
            else
            { 
                foreach(var item in Takes)
                {
                    var take = await item.Clone(item.Shot, clone);
                    clone.AddTake(take);
                    if (item == _activeTake)
                    {
                        clone.ActiveTake = take;
                    }
                }
            }
            return clone;
        }
        private void AddTake(Take take)
        {
            take.VideoFragmentWillBeTrimmedEvent += ActiveTake_VideoFragmentWillBeTrimmedEvent;
            _takes.Add(take);
        }
        public string ChangeActiveTake(string takeId)
        {
            string result = null;
            var take = Takes.FirstOrDefault(t => t.Id == takeId);
            if (take != null)
            {
                result = _activeTake.Id;
                ActiveTake = take;
            }
            return result;
        }
        public void NextTake()
        {
            if(_activeTake != null)
            {
                var index = _takes.IndexOf(_activeTake) + 1;
                if (index < _takes.Count)
                {
                    ActiveTake = _takes[index];
                }
            }

        }
        public void PreviousTake()
        {
            if (_activeTake != null)
            {
                var index = _takes.IndexOf(_activeTake) - 1;
                if (index >= 0 )
                {
                    ActiveTake = _takes[index];
                }
            }
        }
        #endregion

        #region Callbacks
        private void ActiveTake_VideoFragmentWillBeTrimmedEvent(TrimVideoFragmentEventArgs args)
        {
            TcMediaClipWillBeTrimmedEvent?.Invoke(new TrimTcMediaClipEventArgs(this, args));
        }
        #endregion
    }
}
