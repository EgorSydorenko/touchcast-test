﻿using System;
using System.Threading.Tasks;
using Pitch.Models;
using Pitch.Services;
using Pitch.Helpers;
using Windows.Foundation;
using Windows.Media.Editing;
using Windows.Media.MediaProperties;
using Windows.Media.Transcoding;
using Windows.Storage;
using Pitch.Helpers.Logger;
using System.Threading;

namespace Pitch.DataLayer
{
    public delegate void CompositionExporterCompleted(Touchcast touchcast);
    public delegate void CompositionExporterCanceled();
    public delegate void CompositionExporterProgressChanged(double percent);
    public class Exporter
    {
        private double _progress;
        private IAsyncOperationWithProgress<TranscodeFailureReason, double> _operation;
        private CompositionBuilder _compositionBuilder;

        public double Progress => _progress;
        public event CompositionExporterCompleted CompositionExporterCompletedEvent;
        public event CompositionExporterCanceled CompositionExporterCanceledEvent;
        public event CompositionExporterProgressChanged CompositionExporterProgressChangedEvent;

        public Exporter(CompositionBuilder compositionBuilder)
        {
            _compositionBuilder = compositionBuilder;
        }

        public async Task Export(CancellationToken cancellationToken = default(CancellationToken))
        {
            LogQueue.WriteToFile("Exporter Export");
            _progress = 0;
            _compositionBuilder.Refresh();

            var file = await _compositionBuilder.Project.WorkingFolder.CreateFileAsync("video.mp4", CreationCollisionOption.ReplaceExisting);
            _operation = _compositionBuilder.MediaComposition.RenderToFileAsync(file, MediaTrimmingPreference.Precise, MediaEncodingProfile.CreateMp4(VideoEncodingQuality.HD720p));

            _operation.Progress = (reason, progress) =>
            {
                _progress = progress;
                if (cancellationToken.IsCancellationRequested)
                {
                    _progress = 0;
                    _operation.Cancel();
                }
                CompositionExporterProgressChangedEvent?.Invoke(_progress);
            };

            _operation.Completed = async (reason, p) =>
            {
                switch (reason.Status)
                {
                    case AsyncStatus.Canceled:
                    case AsyncStatus.Error:
                        CompositionExporterCanceledEvent?.Invoke();
                        break;
                    case AsyncStatus.Completed:
                        await Complete(file);
                        break;
                }
            };
        }

        public void Cancel()
        {
            LogQueue.WriteToFile("Exporter Cancel");
            try
            {
                if (_operation != null)
                {
                    _operation.Cancel();
                }
            }
            catch (Exception) { }
        }

        private async Task Complete(StorageFile file)
        {
            LogQueue.WriteToFile("Exporter Complete");
            try
            {
                var thumbnailStream = await _compositionBuilder.MediaComposition.GetThumbnailAsync(
                    TimeSpan.FromSeconds(_compositionBuilder.MediaComposition.Duration.TotalSeconds / 2),
                    (int)MediaMixerService.VideoSize.Width,
                    (int)MediaMixerService.VideoSize.Height,
                    VideoFramePrecision.NearestKeyFrame);
                var thumbnailFile = await FileToAppHelper.SaveImageStreamToPngFile(thumbnailStream, MediaMixerService.VideoSize, _compositionBuilder.Project.WorkingFolder, "thumbnail.png");
                var touchcast = new Touchcast();
                //touchcast.Title = _compositionBuilder.Project.Name;
                touchcast.Description = _compositionBuilder.Project.Description;
                touchcast.ActionsStream = _compositionBuilder.ActionsStream;
                touchcast.SetVideoInformation(file, thumbnailFile, _compositionBuilder.MediaComposition.Duration.TotalSeconds.ToString());
                touchcast.TouchcastFile = await touchcast.SaveArchive();
                await touchcast.TouchcastFile.RenameAsync($"New Touchcast.tct", NameCollisionOption.ReplaceExisting);

                CompositionExporterCompletedEvent?.Invoke(touchcast);
            }
            catch (Exception e)
            {
                LogQueue.WriteToFile($"Exporter Complete {e.Message}");
            }
        }
    }
}
