﻿using System;
using System.IO;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.Helpers.Logger;
using Pitch.DataLayer;
using Windows.Graphics.Imaging;
using Pitch.Helpers;

namespace Pitch.DataLayer.Layouts
{
    public class WebLayerLayout : LayerLayout
    {
        #region Private Fields

        private UrlWithProperties _url;

        #endregion

        #region Properties

        public UrlWithProperties Url
        {
            set
            {
                _url = value;
            }
            get
            {
                return _url;
            }
        }

        public bool IsNeedThumbnail => _thumbnailFile == null;

        #endregion

        public static async Task<WebLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            WebLayerLayout layerLayout = new WebLayerLayout(shot);
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }

        #region Life Cycle

        protected WebLayerLayout(Shot shot) : base(shot)
        {
            Init();
        }

        protected WebLayerLayout(WebLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
            _url = layerLayout._url;
            _interactivityUrl = layerLayout._interactivityUrl;
            LogQueue.WriteToFile($"WebLayerLayout: {_url}");
        }

        public WebLayerLayout(UrlWithProperties url, LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _url = url;
            if (_interactivityUrl == null)
            {
                _interactivityUrl = _url;
            }
            Init();
            LogQueue.WriteToFile($"WebLayerLayout: {_url}");
        }

        public WebLayerLayout(UrlWithProperties url, PlaceholderLayerLayout placeholder, Shot shot) : base(placeholder, shot)
        {
            _url = url;
            if (_interactivityUrl == null)
            {
                _interactivityUrl = _url;
            }
            Init();
            LogQueue.WriteToFile($"WebLayerLayout: {_url}");
        }

        #endregion

        private void Init()
        {
            _isEditable = true;
            ComposingContextElement.Type = TouchCastComposingEngine.ComposingContextElementType.Bitmap;
        }

        protected override async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            await base.UpdateFromMetaData(element);
            _url = new UrlWithProperties(element.resource_url, element.resource_url_properties);
            LogQueue.WriteToFile($"UpdateFromMetaData {_url}");
        }

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.resource_url = _url.UrlString;
            data.resource_url_properties = _url.PropertiesInTCPFormat();
            return data;
        }

        public virtual async Task UpdateUrlWithProperties(Uri url)
        {
            _url = await UrlWithProperties.Check(url);
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {
            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? _url.UrlString : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return "html";
        }

        public override LayerLayout Clone(Shot shot = null)
        {
            return new WebLayerLayout(this, shot);
        }

        public override string UrlProperties()
        {
            if (_interactivityUrl?.UrlString == null)
            {
                return _url?.PropertiesAsString();
            }
            else
            {
                return _interactivityUrl?.PropertiesAsString();
            }
        }
    }
}
