﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchCastComposingEngine;

namespace Pitch.DataLayer
{
    public class BackgroundRenderableObject
    {
        public ComposingContextElement BackgroundContextElement { get; }

        public bool IsBackgroundEnabled { set; get; } = false;

        public BackgroundRenderableObject()
        {
            BackgroundContextElement = new ComposingContextElement("background")
            {
                Type = ComposingContextElementType.Bitmap,
                IsVideoVappFrame = true,
                ContentMode = ContentMode.ScaleAspectFit,
                RelativePosition = new Windows.Foundation.Rect(0, 0, 1, 1)
            };
        }
    }
}
