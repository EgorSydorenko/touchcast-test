﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.Helpers;
using TouchCastComposingEngine;
using Windows.Graphics.Imaging;
using Windows.Media.Editing;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace Pitch.DataLayer.Layouts
{
    public enum DevicePerformanceType
    {
        WeakDevice,
        StrongDevice
    }
    public class VideoLayerLayout : LayerLayout
    {
        private BitmapSize _thumbnailSize = new BitmapSize() { Width = 800, Height = 450 };
        #region Private Fields

        protected MediaClip _clip;
        protected MediaComposition _composition;
        protected double _volume;
        protected TimeSpan _seekTime;
        protected StorageFile _file;
        protected BitmapSize _videoFrame;
        protected StorageFile _hdThumbnailFile;
        protected BitmapImage _hdThumbImage;
        private double _aspectRatio;
        private bool _isPlayAutomatically = true;

        #endregion

        #region Properties
        public double AspectRatio => _aspectRatio;
        public MediaComposition Composition => _composition;
        public MediaClip MediaClip => _clip;
        public double Volume
        {
            set => _volume = value;
            get => _volume;
        }

        public TimeSpan SeekTime
        {
            set => _seekTime = value;
            get => _seekTime;
        }

        public StorageFile File => _file;

        public bool IsPlayAutomatically
        {
            get => _isPlayAutomatically;
            set => Set(ref _isPlayAutomatically, value);
        }

        public BitmapImage HighDefinitionThumbnailImage => _hdThumbImage;
        public StorageFile HighDefinitionThumbnailFile => _hdThumbnailFile;
        #endregion

        #region Static Methods

        public static async Task<VideoLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            VideoLayerLayout layerLayout = new VideoLayerLayout(shot);
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }

        #endregion

        #region Life Cycle

        public VideoLayerLayout(StorageFile file, PlaceholderLayerLayout placeholder, Shot shot) : base(placeholder, shot)
        {
            _file = file;
            Init();
        }

        public VideoLayerLayout(StorageFile file, LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _file = file;
            Init();
        }

        protected VideoLayerLayout(Shot shot) : base(shot)
        {
            Init();
        }

        protected VideoLayerLayout(VideoLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
            _volume = layerLayout._volume;
            _seekTime = layerLayout._seekTime;
            _file = layerLayout._file;
            _isPlayAutomatically = layerLayout._isPlayAutomatically;
            _hdThumbImage = layerLayout._hdThumbImage;
            _hdThumbnailFile = layerLayout._hdThumbnailFile;
            _keepAspectRatio = true;
            _aspectRatio = layerLayout._aspectRatio;
            _volume = layerLayout._volume;
            InitComposingContextElement();
        }

        #endregion

        #region Protected Methods

        protected override async Task GenerateThumbnail()
        {
            if (_thumbnailFile == null)
            {
                using (var imageStream = await _composition.GetThumbnailAsync(TimeSpan.FromSeconds(0), (int)_thumbnailSize.Width, (int)_thumbnailSize.Height, VideoFramePrecision.NearestKeyFrame))
                {
                    var fileName = $"{Guid.NewGuid().ToString()}.png";
                    _thumbnailFile = await FileToAppHelper.SaveImageStreamToPngFile(imageStream, _thumbnailSize, Shot?.Project?.WorkingFolder, fileName);
                }
                _thumbImage = new BitmapImage(new Uri(_thumbnailFile.Path));
            }

            if (_hdThumbnailFile == null)
            {
                using (var imageStream = await _composition.GetThumbnailAsync(TimeSpan.FromSeconds(0), (int)_videoFrame.Width, (int)_videoFrame.Height, VideoFramePrecision.NearestKeyFrame))
                {
                    var fileName = $"{Guid.NewGuid().ToString()}.png";
                    _hdThumbnailFile = await FileToAppHelper.SaveImageStreamToPngFile(imageStream, _videoFrame, Shot?.Project?.WorkingFolder, fileName);
                }
                _hdThumbImage = new BitmapImage(new Uri(_hdThumbnailFile.Path));
            }
            await AddVideoVappBitmap();
        }

        public async Task AddVideoVappBitmap()
        {
            if (_hdThumbnailFile != null || _thumbnailFile != null)
            {
                var fileToUse = _hdThumbnailFile ?? _thumbnailFile;
                if (fileToUse != null)
                {
                    var bmp = await CanvasBitmapHelper.FromFile(fileToUse);
                    if (!App.Locator.IsLightMode)
                    {
                        ComposingContext.Shared().AddVideoVappBitmapById(bmp, _isBackground ? Shot.Project.BackgroundRenderableObject.BackgroundContextElement.RecordableItemID : _id);
                    }
                    else
                    {
                        ComposingContextElement.Bitmap = bmp;
                    }
                }
            }
        }
        public override LayerLayoutMementoModel GenerateMementoModel()
        {
            return new VideoLayerLayoutMementoModel(this);
        }

        public override LayerLayoutMementoModel ProcessMementoModel(LayerLayoutMementoModel mementoModel, bool isNeedToCheckPreviousPosition = true)
        {
            var previousePlayAutomatically = false;
            if (mementoModel is VideoLayerLayoutMementoModel model)
            {
                previousePlayAutomatically = _isPlayAutomatically;
                _isPlayAutomatically = model.IsPlayAutomatically;
            }

            var modelForRedo = base.ProcessMementoModel(mementoModel, isNeedToCheckPreviousPosition);

            if (modelForRedo is VideoLayerLayoutMementoModel forRedo)
            {
                forRedo.IsPlayAutomatically = previousePlayAutomatically;
            }
            return modelForRedo;
        }

        public override async Task CopyDataToProjectFolder()
        {
            await base.CopyDataToProjectFolder();
            if (_file != null)
            {
                _file = await Shot.Project.CopyFileToLocalSandbox(_file);
            }
            if (_hdThumbnailFile != null)
            {
                _hdThumbnailFile = await Shot.Project.CopyFileToLocalSandbox(_hdThumbnailFile);
            }
        }
        protected override async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            await base.UpdateFromMetaData(element);
            _isPlayAutomatically = element.autostart.HasValue ? element.autostart.Value : true;
            if (!String.IsNullOrEmpty(element.resource_url))
            {
                _file = await Shot.Project.WorkingFolder.GetFileAsync(element.resource_url);
            }

            if (!String.IsNullOrEmpty(element.hd_thumbnail_file))
            {
                try
                {
                    _hdThumbnailFile = await Shot.Project.WorkingFolder.GetFileAsync(element.hd_thumbnail_file);
                    _hdThumbImage = new BitmapImage();
                    using (var stream = await _hdThumbnailFile.OpenReadAsync())
                    {
                        await _hdThumbImage.SetSourceAsync(stream);
                    }
                }
                catch (Exception)
                {
                    _hdThumbnailFile = null;
                }
            }
        }

        #endregion

        #region Public Methods
        public void UpdateVideoCompositor(string id = null)
        {

            string compositorId = id ?? _id;
            var properties = new Windows.Foundation.Collections.PropertySet
                {
                    { ComposingContextElement.EffectKeyType, VideoEffectType.VideoVappType},
                    { "Id", compositorId }
                };

            var overlay = new MediaOverlayLayer((new Windows.Media.Effects.VideoCompositorDefinition("TouchCastComposingEngine.VideoVappCompositor", properties)));
            overlay.Overlays.Add(new MediaOverlay(MediaClip.CreateFromColor(Windows.UI.Colors.Transparent, _clip.OriginalDuration)));
            _composition.OverlayLayers.Add(overlay);
        }
        public void ClearVideoCompositor()
        {
            _composition?.OverlayLayers?.Clear();
        }

        public async override Task Initialize()
        {
            if (_clip == null)
            {
                _clip = await MediaClip.CreateFromFileAsync(_file);
            }
            var properties = _clip.GetVideoEncodingProperties();
            var props = await _file.Properties.GetVideoPropertiesAsync();
            _videoFrame = new BitmapSize() { Width = properties.Width, Height = properties.Height };
            _aspectRatio = (double)properties.Width / properties.Height;
            if (_aspectRatio != (double)_thumbnailSize.Width/_thumbnailSize.Height)
            {
               
                _thumbnailSize = new BitmapSize() { Width = _thumbnailSize.Width, Height = (uint)(_thumbnailSize.Width / _aspectRatio) };
            }

            if (_composition == null)
            {
                _composition = new MediaComposition();
                _composition.Clips.Add(_clip);
                
            }
            await GenerateThumbnail();
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {
            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? "($PERFORMANCE_BASE_URL)/" + _file?.Name : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return (_interactivityUrl?.UrlString != null && _interactivityUrl.UrlString.Contains("http")) ? "html" : "video";
        }

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.resource_url = _file.Name;
            data.autostart = _isPlayAutomatically;
            data.hd_thumbnail_file = _hdThumbnailFile == null ? null : _hdThumbnailFile.Name;
            return data;
        }

        public override LayerLayout Clone(Shot shot = null)
        {
            return new VideoLayerLayout(this, shot);
        }

        #endregion

        #region Private Methods

        private void Init()
        {

            _keepAspectRatio = true;
            _isEditable = true;
            InitComposingContextElement();
        }

        private void InitComposingContextElement()
        {
            var isDeviceStrong = !App.Locator.IsLightMode;

            ComposingContextElement.IsVideoVappFrame = isDeviceStrong;
            ComposingContextElement.Type = ComposingContextElementType.Bitmap;
            ComposingContextElement.ContentMode = isDeviceStrong ? ContentMode.ScaleAspectFit : ContentMode.ScaleAspectFill;
        }

        #endregion
    }
}
