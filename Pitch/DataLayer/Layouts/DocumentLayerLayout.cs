﻿using System;
using System.Threading.Tasks;
using Windows.Data.Pdf;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Pitch.DataLayer.Shots;
using Pitch.Helpers;
using Windows.Graphics.Imaging;

namespace Pitch.DataLayer.Layouts
{
    public class DocumentLayerLayout : LayerLayout
    {
        protected PdfDocument _pdfDoc;
        protected StorageFile _file;

        public PdfDocument PdfDoc => _pdfDoc;

        public StorageFile File => _file;

        public static async Task<DocumentLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            DocumentLayerLayout layerLayout = new DocumentLayerLayout(shot);
            await layerLayout.UpdateFromMetaData(element);
            await layerLayout.Initialize();
            return layerLayout;
        }

        protected DocumentLayerLayout(Shot shot) : base(shot)
        {
            _keepAspectRatio = false;
            _isEditable = true;

            ComposingContextElement.ContentMode = TouchCastComposingEngine.ContentMode.ScaleAspectFit;
            ComposingContextElement.Type = TouchCastComposingEngine.ComposingContextElementType.Bitmap;
        }

        protected DocumentLayerLayout(DocumentLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
            _pdfDoc = layerLayout._pdfDoc;
            _file = layerLayout._file;
        }
        public DocumentLayerLayout(StorageFile file, PlaceholderLayerLayout placeholder, Shot shot) : base(placeholder, shot)
        {
            _file = file;
            _keepAspectRatio = false;
            _isEditable = true;

            ComposingContextElement.ContentMode = TouchCastComposingEngine.ContentMode.ScaleAspectFit;
            ComposingContextElement.Type = TouchCastComposingEngine.ComposingContextElementType.Bitmap;
        }
        public DocumentLayerLayout(StorageFile file, LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _file = file;
            _keepAspectRatio = false;
            _isEditable = true;

            ComposingContextElement.ContentMode = TouchCastComposingEngine.ContentMode.ScaleAspectFit;
            ComposingContextElement.Type = TouchCastComposingEngine.ComposingContextElementType.Bitmap;
        }

        protected override async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            await base.UpdateFromMetaData(element);
            if (!String.IsNullOrEmpty(element.resource_url))
            {
                _file = await Shot.Project.WorkingFolder.GetFileAsync(element.resource_url);
            }
        }
        public override async Task CopyDataToProjectFolder()
        {
            await base.CopyDataToProjectFolder();
            if (_file != null)
            {
                _file = await Shot.Project.CopyFileToLocalSandbox(_file);
            }
        }

        public async override Task Initialize()
        {
            await _preparationLock.WaitAsync();
            if (_pdfDoc == null)
            {
                _pdfDoc = await PdfDocument.LoadFromFileAsync(_file);
            }
            await GenerateThumbnail();
            _preparationLock.Release();
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {
            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? "($PERFORMANCE_BASE_URL)/" + _file?.Name : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return "html";
        }

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.resource_url = _file.Name;
            return data;
        }

        public override LayerLayout Clone(Shot shot = null)
        {
            return new DocumentLayerLayout(this, shot);
        }

        public override void Clean()
        {
            base.Clean();
        }
    }
}
