﻿using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.Models;
using Pitch.UILayer.Helpers.Converters;
using TouchCastComposingEngine;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI;

namespace Pitch.DataLayer.Layouts
{
    public class ShapeLayerLayout : LayerLayout
    {
        #region Constants

        public static readonly Color DefaultFillColor = Color.FromArgb(0xff, 0x5b, 0x9b, 0xd5);
        public static readonly Color DefaultStrokeColor = Color.FromArgb(0xff, 0x41, 0x71, 0x9c);
        public static readonly int DefaultStrokeSize = 4;

        public const string RectangleShapeTypeName = "rect";
        public const string ArrowShapeTypeName = "arrow";
        public const string LineShapeTypeName = "line";
        public const string EllipseShapeTypeName = "ellipse";

        #endregion

        #region Events

        public delegate void StyleChangedEventHandler();
        public event StyleChangedEventHandler StyleChanged;

        #endregion

        #region Fields

        protected InkShapeType _shapeType;
        protected Point _previousFirstPoint;
        protected Point _previousSecondPoint;
        protected Point _lineFirstPoint;
        protected Point _lineSecondPoint;

        protected Color _fillColor;
        protected Color _strokeColor;
        protected double _strokeSize;
        protected StorageFile _file;

        #endregion

        #region Properties

        public StorageFile File => _file;

        public InkShapeType ShapeType => _shapeType;

        public Point LineFirstPoint
        {
            get { return _lineFirstPoint; }
            set { _lineFirstPoint = value; }
        }

        public Point LineSecondPoint
        {
            get { return _lineSecondPoint; }
            set { _lineSecondPoint = value; }
        }

        public Point PreviousFirstPoint => _previousFirstPoint; 
        public Point PreviousSecondPoint => _previousSecondPoint;

        public Size SizeExtension
        {
            get
            {
                var halfThickness = _strokeSize / 2;
                var minX = Math.Abs(halfThickness * Math.Sin(_transform.Rotation * Math.PI / 180));
                var minY = Math.Abs(halfThickness * Math.Cos(_transform.Rotation * Math.PI / 180));
                return new Size(minX, minY);
            }
        }
        public Color FillColor
        {
            get { return _fillColor; }
            set
            {
                _fillColor = value;
                StyleChanged?.Invoke();
            }
        }

        public Color StrokeColor
        {
            get { return _strokeColor; }
            set
            {
                _strokeColor = value;
                StyleChanged?.Invoke();
            }
        }

        public double StrokeSize
        {
            get { return _strokeSize; }
            set
            {
                _strokeSize = value;
                StyleChanged?.Invoke();
            }
        }

        #endregion

        #region Life Cycle

        protected ShapeLayerLayout(ShapeLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
            IsNeedUpdateThumbnail = true;
            _shapeType = layerLayout._shapeType;
            _lineFirstPoint = layerLayout._lineFirstPoint;
            _lineSecondPoint = layerLayout._lineSecondPoint;
            _fillColor = layerLayout._fillColor;
            _strokeColor = layerLayout._strokeColor;
            _strokeSize = layerLayout._strokeSize;
            if (_shapeType == InkShapeType.Line || _shapeType == InkShapeType.Arrow)
            {
                _lineFirstPoint = new Point(layerLayout._lineFirstPoint.X, layerLayout._lineFirstPoint.Y);
                _lineSecondPoint = new Point(layerLayout._lineSecondPoint.X, layerLayout._lineSecondPoint.Y);
            }
            _initialData.FillColor = layerLayout._fillColor;
            _initialData.StrokeColor = layerLayout._strokeColor;
            _initialData.StrokeSize = layerLayout._strokeSize;

            InitComposingElement();
            _composingElement.ShapeElement.ShapeSubType = layerLayout._composingElement.ShapeElement.ShapeSubType;
        }

        public ShapeLayerLayout(InkShapeType shapeType, LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            IsNeedUpdateThumbnail = true;
            _shapeType = shapeType;
            _fillColor = DefaultFillColor;
            _strokeColor = DefaultStrokeColor;
            _strokeSize = DefaultStrokeSize;
            _isEditable = false;
            if (_shapeType == InkShapeType.Line || _shapeType == InkShapeType.Arrow)
            {
                Transform.TranslateX = Position.X;
                Transform.TranslateY = Position.Y + Position.Height / 2;

                LineFirstPoint = new Point(Transform.TranslateX, Transform.TranslateY);
                LineSecondPoint = new Point(Transform.TranslateX + Position.Width, Transform.TranslateY);

                UpdatePosition(Position.Width, 25);
            }
            _initialData.FillColor = DefaultFillColor;
            _initialData.StrokeColor = DefaultStrokeColor;
            _initialData.StrokeSize = DefaultStrokeSize;
            InitComposingElement();
        }

        #endregion

        protected override Task GenerateThumbnail()
        {
            return Task.CompletedTask;
        }

        public override LayerLayout Clone(Shot shot = null)
        {
            return new ShapeLayerLayout(this, shot);
        }

        public static async Task<ShapeLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            var layerLayout = new ShapeLayerLayout(InkShapeType.Rectangle, LayerLayoutTemplate.GenerateDefaultTemplate("Shape"), shot);
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }

        public override void RefreshRelativePosition()
        {
            _composingElement.ShapeElement.Color = _fillColor;
            _composingElement.ShapeElement.StrokeColor = _strokeColor;
            _composingElement.ShapeElement.StrokeWidth = (float)_strokeSize;
           
            if (ShapeType == InkShapeType.Line || ShapeType == InkShapeType.Arrow)
            {
                var rect = new Rect
                (
                    _lineFirstPoint.X / ComposingContext.VideoSize.Width,
                    _lineFirstPoint.Y / ComposingContext.VideoSize.Height,
                    _lineSecondPoint.X / ComposingContext.VideoSize.Width,
                    _lineSecondPoint.Y / ComposingContext.VideoSize.Height
                );
                _composingElement.RelativePosition = rect;
            }
            else
            {
                base.RefreshRelativePosition();
            }
        }

        public override LayerLayoutMementoModel GenerateMementoModel()
        {
            var model = new ShapeLayerLayoutMementoModel(this);
            return model;
        }

        public override LayerLayoutMementoModel ProcessMementoModel(LayerLayoutMementoModel mementoModel, bool isNeedToCheckPreviousPosition = true)
        {
            if (mementoModel is ShapeLayerLayoutMementoModel)
            {
                var model = mementoModel as ShapeLayerLayoutMementoModel;

                _previousFirstPoint = new Point(_lineFirstPoint.X, _lineFirstPoint.Y);
                _previousSecondPoint = new Point(_lineSecondPoint.X, _lineSecondPoint.Y);

                _lineFirstPoint = new Point(model.PreviousFirstPoint.X, model.PreviousFirstPoint.Y);
                _lineSecondPoint = new Point(model.PreviousSecondPoint.X, model.PreviousSecondPoint.Y);

                var previousFillColor = FillColor;
                var previousStrokeColor = StrokeColor;
                var previousStrokeSize = StrokeSize;

                FillColor = model.FillColor;
                StrokeColor = model.StrokeColor;
                StrokeSize = model.StrokeSize;

                var modelForRedo = base.ProcessMementoModel(mementoModel, !(_shapeType == InkShapeType.Line || _shapeType == InkShapeType.Arrow));
                var shapeModelForRedo = modelForRedo as ShapeLayerLayoutMementoModel;

                shapeModelForRedo.FillColor = previousFillColor;
                shapeModelForRedo.StrokeColor = previousStrokeColor;
                shapeModelForRedo.StrokeSize = previousStrokeSize;

                return shapeModelForRedo;
            }

            return base.ProcessMementoModel(mementoModel, !(_shapeType == InkShapeType.Line || _shapeType == InkShapeType.Arrow));
        }

        public override void UpdatePreviousPosition()
        {
            UpdatePreviousLinePoints();
            base.UpdatePreviousPosition();
        }
        public void UpdatePreviousLinePoints()
        {
            _previousFirstPoint = new Point(_lineFirstPoint.X, _lineFirstPoint.Y);
            _previousSecondPoint = new Point(_lineSecondPoint.X, _lineSecondPoint.Y);
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {
            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? String.Empty : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return "image";
        }

        public override void UpdateToInitialData()
        {
            base.UpdateToInitialData();
            InitSizeAndPosition();
            FillColor = _initialData.FillColor;
            StrokeColor = _initialData.StrokeColor;
            StrokeSize = _initialData.StrokeSize;
        }
        public void InitSizeAndPosition()
        {
            if (ShapeType == InkShapeType.Line || ShapeType == InkShapeType.Arrow)
            {
                Transform.TranslateY = LayerLayoutTemplate.DefaultHeight + LayerLayoutTemplate.DefaultHeight / 2;
                LineFirstPoint = new Point(Transform.TranslateX, Transform.TranslateY);
                LineSecondPoint = new Point(LineFirstPoint.X + LayerLayoutTemplate.DefaultWidth, Transform.TranslateY);
                UpdatePosition(LayerLayoutTemplate.DefaultWidth, 25);
            }
        }


        #region TcpInfo

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.resource_url = _file.Name;

            if (_shapeType == InkShapeType.Arrow || _shapeType == InkShapeType.Line)
            {
                var xProjection = _lineSecondPoint.X - _lineFirstPoint.X;
                var yProjection = _lineSecondPoint.Y - _lineFirstPoint.Y;
                var width = (xProjection == 0) ? _strokeSize : 0.0;
                var height = (yProjection == 0) ? _strokeSize : 0.0;

                var x = (xProjection > 0) ? _lineFirstPoint.X : _lineSecondPoint.X;
                var y = (yProjection > 0) ? _lineFirstPoint.Y : _lineSecondPoint.Y;
                
                var rectangle = new Rect(x, y, Math.Abs(xProjection) + width, Math.Abs(yProjection) + height);
                data.transform = Pitch.Helpers.TouchcastTransformHelper.CreateTouchcastTransformFromRectangle(rectangle);
            }
            return data;
        }

        public async Task SaveData()
        {
            if (_file == null)
            {
                _file = await Shot.Project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid()}.shp");
            }
            var str = Serialize();
            await FileIO.WriteTextAsync(_file, str);
        }

        public void CalculateLineWidthAndRotation()
        {
            var dY = _lineSecondPoint.Y - _lineFirstPoint.Y;
            var dX = _lineSecondPoint.X - _lineFirstPoint.X;

            var width = Math.Sqrt(Math.Pow(dX, 2) + Math.Pow(dY, 2));

            UpdatePosition(width, 25);

            var sinus = dY / width;
            if (sinus > 1.0 || sinus < -1.0)
            {
                return;
            }
            var alfa = Math.Asin(sinus) * 180 / Math.PI;
            if (dX < 0)
            {
                if (alfa < 0) alfa = -180 - alfa;
                else alfa = 180 - alfa;
            }
            _transform.Rotation = alfa;
        }

        protected override async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            await base.UpdateFromMetaData(element);
            if (!String.IsNullOrEmpty(element.resource_url))
            {
                _file = await Shot.Project.WorkingFolder.GetFileAsync(element.resource_url);
                var json = await FileIO.ReadTextAsync(_file);

                var shape = JsonConvert.DeserializeObject<TcpInfo.Shape>(json);
                if (shape != null)
                {
                    _shapeType = InkShapeType.Rectangle;
                    switch (shape.type)
                    {
                        case RectangleShapeTypeName:
                            _shapeType = InkShapeType.Rectangle;
                            break;
                        case ArrowShapeTypeName:
                            _shapeType = InkShapeType.Arrow;
                            break;
                        case LineShapeTypeName:
                            _shapeType = InkShapeType.Line;
                            break;
                        case EllipseShapeTypeName:
                            _shapeType = InkShapeType.Circle;
                            break;
                    }
                    if (_shapeType == InkShapeType.Line || _shapeType == InkShapeType.Arrow)
                    {
                        if (_position.Width == 0)
                        {
                            shape.start_point.x = shape.end_point.x;
                        }
                        if (_position.Height == 0)
                        {
                            shape.start_point.y = shape.end_point.y;
                        }
                        _lineFirstPoint = new Point(_position.X + _position.Width * shape.start_point.x, _position.Y + _position.Height * shape.start_point.y);
                        _lineSecondPoint = new Point(_position.X + _position.Width * shape.end_point.x, _position.Y + _position.Height * shape.end_point.y);
                        Transform.TranslateX = _lineFirstPoint.X;
                        Transform.TranslateY = _lineFirstPoint.Y;

                        CalculateLineWidthAndRotation();
                    }
                    _composingElement.ShapeElement = new TouchcastShapeElement(_fillColor, _strokeColor, (float)_strokeSize);
                    _strokeSize = shape.stroke_width;
                    _strokeColor = HexToColorConverter.ConvertHexToArgbColor(shape.stroke_color);
                    _fillColor = HexToColorConverter.ConvertHexToArgbColor(shape.fill_color);
                }
                InitComposingElement();
            }
        }

        #endregion

        #region Private Methods

        private string Serialize()
        {
            TcpInfo.Point shapeStartPoint = null;
            TcpInfo.Point shapeEndPoint = null;
            if (_shapeType == InkShapeType.Line || _shapeType == InkShapeType.Arrow)
            {
                var lineFirstPointX = (int)(_lineFirstPoint.X * 1000);
                var lineFirstPointY = (int)(_lineFirstPoint.Y * 1000);
                var lineSecondPointX = (int)(_lineSecondPoint.X * 1000);
                var lineSecondPointY = (int)(_lineSecondPoint.Y * 1000);
                var startRelativeX = (lineSecondPointX < lineFirstPointX) ? 1.0 : 0.0;
                var endRelativeX = (lineSecondPointX <= lineFirstPointX) ? 0.0 : 1.0;
                var startRelativeY = (lineSecondPointY < lineFirstPointY) ? 1.0 : 0.0;
                var endRelativeY = (lineSecondPointY <= lineFirstPointY) ? 0.0 : 1.0;
                shapeStartPoint = new TcpInfo.Point { x = startRelativeX, y = startRelativeY };
                shapeEndPoint = new TcpInfo.Point { x = endRelativeX, y = endRelativeY };
            }

            var metaData = new TcpInfo.Shape
            {
                type = (ShapeType == InkShapeType.Arrow) ? ArrowShapeTypeName :
                (ShapeType == InkShapeType.Line) ? LineShapeTypeName :
                (ShapeType == InkShapeType.Rectangle) ? RectangleShapeTypeName :
                (ShapeType == InkShapeType.Circle) ? EllipseShapeTypeName : string.Empty,
                fill_color = HexToColorConverter.ConvertColorToHex(_fillColor),
                stroke_color = HexToColorConverter.ConvertColorToHex(_strokeColor),
                stroke_width = _strokeSize,
                start_point = shapeStartPoint,
                end_point = shapeEndPoint
            };

            return JsonConvert.SerializeObject(metaData, Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
        }

        private void InitComposingElement()
        {
            _composingElement.ShapeElement = new TouchcastShapeElement(_fillColor, _strokeColor, (float)_strokeSize);
            _composingElement.Type = ComposingContextElementType.Shape;
            switch (_shapeType)
            {
                case InkShapeType.Line:
                    _composingElement.ShapeElement.ShapeSubType = ComposingShapeSubType.Line;
                    break;
                case InkShapeType.Arrow:
                    _composingElement.ShapeElement.ShapeSubType = ComposingShapeSubType.Arrow;
                    break;
                case InkShapeType.Rectangle:
                    _composingElement.ShapeElement.ShapeSubType = ComposingShapeSubType.Rectangle;
                    break;
                case InkShapeType.Circle:
                    _composingElement.ShapeElement.ShapeSubType = ComposingShapeSubType.Circle;
                    break;
            }
        }

        #endregion
    }
}
