﻿using GalaSoft.MvvmLight;
using Pitch.DataLayer.Helpers.Extensions;
using Pitch.DataLayer.Shots;
using Pitch.DataLayer.UndoRedo;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Pitch.Models;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using TouchCastComposingEngine;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Pitch.DataLayer.Layouts
{
    public class LayerLayoutTemplate
    {
        public static readonly string MixerType = "Mixer";
        public static readonly string PlaceHolderType = "PlaceHolder";
        public static readonly string VideoType = "Video";
        public static readonly string WebType = "Web";
        public static readonly string DocType = "Document";
        public static readonly string TextType = "Text";
        public static readonly string ImageType = "Image";
        public static readonly string ShapeType = "Shape";

        public static readonly double MinWidth = 126;
        public static readonly double MinHeight = 70;
        public static readonly double DefaultWidth = ComposingContext.VideoSize.Width / 3;
        public static readonly double DefaultHeight = ComposingContext.VideoSize.Height / 3;

        public static readonly Rect DefaultVappPosition = new Rect((ComposingContext.VideoSize.Width - DefaultWidth) / 2, (ComposingContext.VideoSize.Height - DefaultHeight) / 2, DefaultWidth, DefaultHeight);
        public static readonly Rect DefaultSquareVappPosition = new Rect((ComposingContext.VideoSize.Width - DefaultHeight) / 2, (ComposingContext.VideoSize.Height - DefaultHeight) / 2, DefaultHeight, DefaultHeight);
        private static LayerLayoutTemplate _staticTemplate;

        private string _id;
        public LayerLayoutInitialData InitialiData { private set; get; }
        public StorageFile File { set; get; }
        public string Text { set; get; }
        public string Url { set; get; }
        public string Type { set; get; }
        public string Id => _id;
        public static LayerLayoutTemplate GenerateDefaultTemplate(string type)
        {
            var defaultTemplate = new LayerLayoutTemplate()
            {
                Type = type
            };
            bool liveOnPlayback = !(type == MixerType || type == TextType || type == ShapeType);
            bool pauseOnPlayback = !(type == MixerType || type == TextType || type == ShapeType);
            defaultTemplate.InitialiData = new LayerLayoutInitialData(DefaultVappPosition, false, true, false, liveOnPlayback, pauseOnPlayback, String.Empty, false);

            return defaultTemplate;
        }

        public static LayerLayoutTemplate GenerateStaticTemplate(string type)
        {
            if (_staticTemplate == null)

            {
                _staticTemplate = new LayerLayoutTemplate()
                {
                    Type = type
                };
                _staticTemplate.InitialiData = new LayerLayoutInitialData(DefaultVappPosition, true, false, false, true, true, String.Empty, false);
            }

            return _staticTemplate;
        }

        public static LayerLayoutTemplate GenerateCustomTemplate(string type, Rect position, bool isStatic, bool isSelected, bool isHotspot,
                                                                 bool liveOnPlayback, bool pauseOnPlayback, bool isBackground)
        {
            var customTemplate = new LayerLayoutTemplate()
            {
                Type = type,
                InitialiData = new LayerLayoutInitialData(position, isStatic, isSelected, isHotspot, liveOnPlayback, pauseOnPlayback, String.Empty, isBackground)
            };
            return customTemplate;
        }
        public static LayerLayoutTemplate GenerateFullScreenTemplate(string type, bool isStatic, bool isSelected, bool isHotspot,
                                                                 bool liveOnPlayback, bool pauseOnPlayback, bool isBackground)
        {
            var customTemplate = new LayerLayoutTemplate()
            {
                Type = type,
                InitialiData = new LayerLayoutInitialData(new Rect(0, 0, ComposingContext.VideoSize.Width, ComposingContext.VideoSize.Height), 
                    isStatic, isSelected, isHotspot, liveOnPlayback, pauseOnPlayback, String.Empty, isBackground)
            };
            return customTemplate;
        }


        public LayerLayoutTemplate(string id = null)
        {
            _id = id;
            if (String.IsNullOrEmpty(_id))
            {
                _id = Guid.NewGuid().ToString();
            }
        }
    }

    public class LayerLayoutMementoModel : ICleanable
    {
        public string Id { private set; get; }
        public CompositeTransform PreviousTransform { set; get; }
        public bool IsHotSpot { set; get; }
        public bool IsStatic { set; get; }
        public Rect PreviousPosition { set; get; }
        public bool IsLiveOnPlayback { set; get; }
        public bool IsPauseOnPlayback { set; get; }
        public UrlWithProperties InteractiveUrl { set; get; }
        public int ZIndex { get; set; }

        public LayerLayoutMementoModel(LayerLayout layer)
        {
            Id = layer.Id;
            PreviousTransform = new CompositeTransform()
            {
                TranslateX = layer.PreviousTransform.TranslateX,
                TranslateY = layer.PreviousTransform.TranslateY,
                ScaleX = layer.PreviousTransform.ScaleX,
                ScaleY = layer.PreviousTransform.ScaleY
            };
            PreviousPosition = layer.PreviousPosition;
            IsStatic = layer.IsStatic;
            IsHotSpot = layer.IsHotSpot;
            IsLiveOnPlayback = layer.IsLiveOnPlayback;
            IsPauseOnPlayback = layer.IsPauseOnPlayback;
            InteractiveUrl = layer.InteractivityUrl;
            ZIndex = layer.ZIndex;
        }
        public void Clean()
        {
        }
    }

    public class ShapeLayerLayoutMementoModel : LayerLayoutMementoModel
    {
        public Point PreviousFirstPoint { set; get; }
        public Point PreviousSecondPoint { set; get; }
        public Color FillColor { set; get; }
        public Color StrokeColor { set; get; }
        public double StrokeSize { set; get; }

        public ShapeLayerLayoutMementoModel(ShapeLayerLayout layer) : base(layer)
        {
            PreviousFirstPoint = new Point(layer.PreviousFirstPoint.X, layer.PreviousFirstPoint.Y);
            PreviousSecondPoint = new Point(layer.PreviousSecondPoint.X, layer.PreviousSecondPoint.Y);
            FillColor = layer.FillColor;
            StrokeColor = layer.StrokeColor;
            StrokeSize = layer.StrokeSize;
        }
    }

    public class VideoLayerLayoutMementoModel : LayerLayoutMementoModel
    {
        public bool IsPlayAutomatically { get; set; }
        public VideoLayerLayoutMementoModel(VideoLayerLayout layer) : base(layer)
        {
            IsPlayAutomatically = layer.IsPlayAutomatically;
        }
    }

    public class LayerLayoutInitialData
    {
        private int _zIndex;

        public Rect Position { private set; get; }
        public bool IsStatic { private set; get; }
        public bool IsSelected { private set; get; }
        public bool IsHotSpotEnabled { private set; get; }
        public bool LiveOnPlayback { private set; get; }
        public bool PauseOnPlayback { private set; get; }
        public string Url { private set; get; }
        public string InteractivityUrl { set; get; }
        public bool IsSiluethMode { set; get; }
        public int ZIndex { set { _zIndex = value; } get { return _zIndex; } }
        public bool IsBackground { set; get; }
        public Color FillColor { set; get; }
        public Color StrokeColor { set; get; }
        public double StrokeSize { set; get; }

        public LayerLayoutInitialData(Rect position, bool isStatic, bool isSelected, bool isHotSpot, bool liveOnPlayback, bool pauseOnPlayback, string url, bool isBackground)
        {
            Position = position;
            IsStatic = isStatic;
            IsSelected = isSelected;
            IsHotSpotEnabled = isHotSpot;
            LiveOnPlayback = liveOnPlayback;
            PauseOnPlayback = pauseOnPlayback;
            Url = url;
            IsBackground = isBackground;
        }

        public LayerLayoutInitialData Clone()
        {
            return new LayerLayoutInitialData(Position, IsStatic, IsSelected, IsHotSpotEnabled, LiveOnPlayback, PauseOnPlayback, Url, IsBackground)
            {
                ZIndex = _zIndex
            };
        }
    }

    public abstract class LayerLayout : ViewModelBase, ICleanable
    {
        #region Events

        public delegate void LayoutViewPreviewChanged(object sender);
        public delegate Task LayoutViewPreviewChangedAsync(object sender);
        public delegate void LayerLayoutWasChanged(LayerLayoutMementoModel model);
        public delegate void LayerLayoutPositionChanged(Rect position);
        public event LayerLayoutWasChanged LayerLayoutWasChangedEvent;
        public event LayoutViewPreviewChanged LayoutViewPreviewChangedEvent;
        public event LayerLayoutPositionChanged LayerLayoutPositionChangedEvent;
        public event LayoutViewPreviewChangedAsync LayerLayoutRaiseThumbnailGenerationEvent;

        #endregion

        #region Constants

        protected readonly BitmapSize ThumbnailSize = new BitmapSize() { Width = 320, Height = 180 };
        public static double BorderButtonLedge = 8;

        #endregion

        #region Fields

        protected bool _isSelectedAfterCreation;
        protected ComposingContextElement _composingElement;
        protected WeakReference<Shot> _shot;
        protected StorageFile _thumbnailFile;
        protected BitmapImage _thumbImage;
        protected bool _isStatic;
        protected bool _isLiveOnPlayback;
        protected UrlWithProperties _interactivityUrl;
        protected bool _isHotSpot;
        protected bool _isPauseOnPlayback;
        protected int _zIndex;
        protected bool _isBackground;
        protected readonly string _id;
        protected Rect _position;
        protected Rect _previousPosition;
        protected bool _keepAspectRatio;
        protected CompositeTransform _transform;
        protected CompositeTransform _previousTransform;
        protected bool _isEditable;
        protected LayerLayoutInitialData _initialData;
        protected bool _isSSL = false;
        protected bool _iframeBlocking = true;
        protected AsyncLock _preparationLock = new AsyncLock(1);
        private object _lockPosition = new object();

        #endregion

        #region Life Cycle

        public LayerLayout(PlaceholderLayerLayout placeholder, Shot shot) : this(shot)
        {
            _initialData = placeholder.GenerateLayerLayoutInitialData();
            LayerLayoutInitFromTemplate(_initialData);
        }

        public LayerLayout(LayerLayoutTemplate template, Shot shot) : this(shot)
        {
            _initialData = template.InitialiData;
            LayerLayoutInitFromTemplate(_initialData);
        }

        public LayerLayout(Shot shot)
        {
            _shot = new WeakReference<Shot>(shot);
            _id = Guid.NewGuid().ToString();
            _isLiveOnPlayback = true;
            _composingElement = new ComposingContextElement(_id);
            _isSelectedAfterCreation = false;
        }

        protected LayerLayout(LayerLayout layerLayout, Shot shot = null)
        {
            _shot = new WeakReference<Shot>(shot ?? layerLayout.Shot);
            _isStatic = layerLayout._isStatic;
            _interactivityUrl = layerLayout._interactivityUrl;
            _isBackground = layerLayout._isBackground;

            if (layerLayout is PlaceholderLayerLayout)
            {
                _isLiveOnPlayback = false;
                _isPauseOnPlayback = false;
                _isHotSpot = false;
            }
            else
            {
                _isPauseOnPlayback = layerLayout._isPauseOnPlayback;
                _isLiveOnPlayback = layerLayout._isLiveOnPlayback;
                _isHotSpot = layerLayout._isHotSpot;
            }

            _zIndex = layerLayout._zIndex;
            _id = Guid.NewGuid().ToString();
            _position = layerLayout._position;
            _previousPosition = _position;
            _keepAspectRatio = layerLayout._keepAspectRatio;
            _isEditable = layerLayout._isEditable;
            _composingElement = new ComposingContextElement(_id);
            _composingElement.ContentMode = layerLayout.ComposingContextElement.ContentMode;
            _composingElement.Type = layerLayout.ComposingContextElement.Type;

            _thumbnailFile = layerLayout._thumbnailFile;
            _thumbImage = layerLayout._thumbImage;
            _transform = new CompositeTransform()
            {
                TranslateX = layerLayout._transform.TranslateX,
                TranslateY = layerLayout._transform.TranslateY,
                ScaleX = layerLayout._transform.ScaleX,
                ScaleY = layerLayout._transform.ScaleY
            };
            _previousTransform = new CompositeTransform()
            {
                TranslateX = layerLayout._previousTransform.TranslateX,
                TranslateY = layerLayout._previousTransform.TranslateY,
                ScaleX = layerLayout._previousTransform.ScaleX,
                ScaleY = layerLayout._previousTransform.ScaleY
            };
            _initialData = layerLayout._initialData.Clone();
        }

#if DEBUG
        ~LayerLayout()
        {
            Debug.WriteLine("************************* LayerLayout Desctructor **************************");
        }
#endif
        public virtual async Task Initialize()
        {
            await GenerateThumbnail();
        }

        public virtual void Clean() { }

        protected virtual async Task GenerateThumbnail()
        {
            if (_thumbnailFile != null)
            {
                _composingElement.Bitmap = await CanvasBitmapHelper.FromFile(_thumbnailFile);
                _composingElement.ContentMode = ContentMode.ScaleToFill;
            }

            if (_thumbnailFile == null)
            {
                _thumbnailFile = await Shot.Project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid()}.png");
            }
        }

        abstract public LayerLayout Clone(Shot shot = null);

        #endregion

        #region Public Properties

        public bool IsNeedUpdateThumbnail { set; get; }

        public Rect Position
        {
            get
            {
                lock (_lockPosition)
                {
                    return _position;
                }
            }
            protected set
            {
                lock (_lockPosition)
                {
                    _position = value;
                }
            }
        }

        public Rect PreviousPosition
        {
            get => _previousPosition;
            set => _previousPosition = value;
        }

        public bool IsSelectedAfterCreation
        {
            get => _isSelectedAfterCreation;
            set => _isSelectedAfterCreation = value;
        }

        public bool IsLiveOnPlayback
        {
            set => _isLiveOnPlayback = value;
            get => _isLiveOnPlayback;
        }

        public bool IsEditable => _isEditable;

        public Shot Shot => _shot?.TryGetObject();

        public string Id => _id;

        public BitmapImage ThumbnailImage => _thumbImage;

        public StorageFile ThumbnailFile => _thumbnailFile;

        public string TouchcastTransform => TouchcastTransformHelper.CreateTouchcastTransformFromRectangle(_position);

        public bool IsHotSpot
        {
            get => _isHotSpot;
            set => _isHotSpot = value;
        }

        public bool IsPauseOnPlayback
        {
            get => _isPauseOnPlayback;
            set => _isPauseOnPlayback = value;
        }

        public virtual bool IsStatic
        {
            get => _isStatic;
            set => _isStatic = value;
        }

        public int ZIndex
        {
            get => _zIndex;
            set => _zIndex = value;
        }

        public bool IsBackground
        {
            get => _isBackground;
            set => _isBackground = value;
        }

        public bool KeepAspectRatio
        {
            get => _keepAspectRatio;
            set => _keepAspectRatio = value;
        }

        public UrlWithProperties InteractivityUrl
        {
            get => _interactivityUrl;
            set => _interactivityUrl = value;
        }

        public CompositeTransform Transform => _transform;

        public CompositeTransform PreviousTransform => _previousTransform;

        public ComposingContextElement ComposingContextElement
        {
            get => _composingElement;
            set => _composingElement = value;
        }

        public string Type { get { return "vapp"; } }

        public bool IsInitializedAsBackground => _initialData.IsBackground;

        #endregion

        #region Public Methods

        public virtual async Task CopyDataToProjectFolder()
        {
            if (_thumbnailFile != null)
            {
                _thumbnailFile = await _thumbnailFile.CopyAsync(Shot.Project.WorkingFolder, $"{Guid.NewGuid()}.png");
            }
        }

        public Task UpdateThumbnail()
        {
            RaiseEvent();
            return Task.CompletedTask;
        }

        public virtual LayerLayoutMementoModel GenerateMementoModel()
        {
            return new LayerLayoutMementoModel(this);
        }

        public virtual LayerLayoutMementoModel ProcessMementoModel(LayerLayoutMementoModel mementoModel, bool isNeedToCheckPreviousPosition = true)
        {
            var previousTransform = Transform;
            var previousPosition = Position;

            _previousTransform = new CompositeTransform
            {
                TranslateX = _transform.TranslateX,
                TranslateY = _transform.TranslateY,
                ScaleX = _transform.ScaleX,
                ScaleY = _transform.ScaleY,
                Rotation = _transform.Rotation
            };
            _previousPosition = Position;

            _transform = mementoModel.PreviousTransform;

            if (isNeedToCheckPreviousPosition)
            {
                UpdatePosition(mementoModel.PreviousPosition.Width, mementoModel.PreviousPosition.Height);
            }

            LayerLayoutWasChangedEvent?.Invoke(mementoModel);
            var mementoModelForRedo = GenerateMementoModel();

            mementoModelForRedo.PreviousPosition = previousPosition;
            mementoModelForRedo.PreviousTransform = previousTransform;

            var isZIndexChanged = (_zIndex != mementoModel.ZIndex);
            _isHotSpot = mementoModel.IsHotSpot;
            _isLiveOnPlayback = mementoModel.IsLiveOnPlayback;
            _isPauseOnPlayback = mementoModel.IsPauseOnPlayback;
            _interactivityUrl = mementoModel.InteractiveUrl;
            _zIndex = mementoModel.ZIndex;
            _isStatic = mementoModel.IsStatic;

            if (Position.Width != _previousPosition.Width
                || Position.Height != _previousPosition.Height
                || Position.X != _previousPosition.X
                || Position.Y != _previousPosition.Y
                || isZIndexChanged)
            {
                RaiseThumbnailGeneration();
            }
            return mementoModelForRedo;
        }

        public virtual void UpdatePreviousPosition()
        {
            PreviousTransform.TranslateX = Transform.TranslateX;
            PreviousTransform.TranslateY = Transform.TranslateY;
            PreviousTransform.SkewX = Transform.SkewX;
            PreviousTransform.SkewY = Transform.SkewY;
            PreviousTransform.ScaleX = Transform.ScaleX;
            PreviousTransform.ScaleY = Transform.ScaleY;
            PreviousTransform.Rotation = Transform.Rotation;
        }

        public void UpdatePosition(double width, double height, double shiftX = 0, double shiftY = 0)
        {
            Rect position = new Rect
            {
                X = Transform.TranslateX + shiftX,
                Y = Transform.TranslateY + shiftY,
                Width = width,
                Height = height
            };

            _position = position;

            LayerLayoutPositionChangedEvent?.Invoke(_position);
        }

        public virtual async Task UpdateUrlWithProperties(Uri url)
        {
            _interactivityUrl = await UrlWithProperties.Check(url);
        }

        public virtual void UpdateUrlWithProperties(UrlWithProperties urlWithProps)
        {
            _interactivityUrl = urlWithProps;
        }

        public virtual void RefreshRelativePosition()
        {
            var pos = Position;
            var rect = new Rect
            (
                pos.X / ComposingContext.VideoSize.Width,
                pos.Y / ComposingContext.VideoSize.Height,
                pos.Width / ComposingContext.VideoSize.Width,
                pos.Height / ComposingContext.VideoSize.Height
            );

            _composingElement.RelativePosition = rect;
        }

        public virtual string IsWidget()
        {
            throw new Exception("new to override");
        }

        public virtual string ContentUrl()
        {
            throw new Exception("new to override");
        }

        public virtual string ThumbnailUrl()
        {
            throw new Exception("new to override");
        }

        public virtual string UrlProperties()
        {
            return _interactivityUrl?.PropertiesAsString();
        }

        public virtual string ContentType()
        {
            return String.Empty;
        }

        public async Task UpdateThumbnailFile(IRandomAccessStream source, double currentWidth, double currentHeight, bool isNeedScaling = true)
        {
            if (currentWidth > 0 && currentHeight > 0)
            {
                try
                {
                    Services.IMediaMixerService mixer = null;
                    try
                    {
                        mixer = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<Services.IMediaMixerService>();
                    }
                    catch (Exception) { }

                    if (mixer != null && mixer.State != Services.TouchcastComposerServiceState.Recording)
                    {
                        uint width, height;
                        double scaleFactor = currentWidth / currentHeight;
                        if (isNeedScaling)
                        {
                            if (scaleFactor < 1)
                            {
                                height = ThumbnailSize.Height;
                                width = (uint)(height * scaleFactor);
                            }
                            else
                            {
                                width = ThumbnailSize.Width;
                                height = (uint)(width / scaleFactor);
                            }
                        }
                        else
                        {
                            width = (uint)currentWidth;
                            height = (uint)currentHeight;
                        }

                        var small = await Resize(width, height, source);
                        await SaveThumbnailToFile(small);
                    }
                }
                catch (Exception e)
                {
                    LogQueue.WriteToFile(e.Message);
                }
            }
        }

        public virtual TcpInfo.Element GetMetaData()
        {
            TcpInfo.Element element = new TcpInfo.Element();
            element.opacity = IsHotSpot ? 0 : 1;
            element.thumbnail_file = _thumbnailFile == null ? null : _thumbnailFile.Name;
            element.type = "vapp";
            element.z_index = _zIndex;

            if (_interactivityUrl != null)
            {
                element.interactivity_url = _interactivityUrl?.UrlString;
                element.interactivity_url_properties = _interactivityUrl?.PropertiesInTCPFormat();
            }

            element.transform = TouchcastTransform;
            element.locked = IsStatic;
            element.pause_on_interaction = IsPauseOnPlayback;
            element.interactive = IsLiveOnPlayback;
            element.keep_aspect_ratio = _keepAspectRatio;
            element.background = _isBackground;
            return element;
        }

        public void UpdateShotReference(Shot shot)
        {
            if (shot == null || Shot == shot)
            {
                return;
            }
            _shot = new WeakReference<Shot>(shot);
        }

        public virtual void UpdateToInitialData()
        {
            LayerLayoutInitFromTemplate(_initialData);
        }

        #endregion

        #region Private Methods

        protected virtual async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            _isStatic = element.locked;
            _isHotSpot = element.opacity == 0 ? true : false;
            _isPauseOnPlayback = element.pause_on_interaction;
            _isLiveOnPlayback = element.interactive;

            _position = TouchcastTransformHelper.CreateRectangleFromTransformString(element.transform, new Size(Services.MediaMixerService.VideoSize.Width, Services.MediaMixerService.VideoSize.Height));

            if (!String.IsNullOrEmpty(element.thumbnail_file))
            {
                IsNeedUpdateThumbnail = false;
                try
                {
                    _thumbnailFile = await Shot.Project.WorkingFolder.GetFileAsync(element.thumbnail_file);
                    _thumbImage = new BitmapImage();
                    using (var stream = await _thumbnailFile.OpenReadAsync())
                    {
                        await _thumbImage.SetSourceAsync(stream);
                    }
                }
                catch (Exception)
                {
                    IsNeedUpdateThumbnail = true;
                    _thumbnailFile = null;
                }
            }
            _zIndex = element.z_index;
            if (element.resource_url_properties == null)
            {
                _interactivityUrl = new UrlWithProperties(element.interactivity_url);
            }
            else
            {
                _interactivityUrl = new UrlWithProperties(element.interactivity_url, element.resource_url_properties);
            }
            _previousTransform = new CompositeTransform();
            _transform = new CompositeTransform()
            {
                TranslateX = _position.X,
                TranslateY = _position.Y
            };
            _initialData = new LayerLayoutInitialData(_position, _isStatic, false, _isHotSpot, _isLiveOnPlayback, _isPauseOnPlayback, String.Empty, element.background);
            _initialData.ZIndex = _zIndex;
            _initialData.FillColor = ShapeLayerLayout.DefaultFillColor;
            _initialData.StrokeColor = ShapeLayerLayout.DefaultStrokeColor;
            _initialData.StrokeSize = ShapeLayerLayout.DefaultStrokeSize;
            _isSelectedAfterCreation = false;
            _keepAspectRatio = element.keep_aspect_ratio;
            _isBackground = element.background;
        }

        private void LayerLayoutInitFromTemplate(LayerLayoutInitialData initialData)
        {
            _isStatic = initialData.IsStatic;
            _position = initialData.Position;
            _previousPosition = _position;
            _isHotSpot = initialData.IsHotSpotEnabled;
            _interactivityUrl = new UrlWithProperties(initialData.InteractivityUrl);
            _isSelectedAfterCreation = initialData.IsSelected;
            _zIndex = initialData.ZIndex;
            _isLiveOnPlayback = initialData.LiveOnPlayback;
            _isPauseOnPlayback = initialData.LiveOnPlayback;
            _isBackground = initialData.IsBackground;
            _previousTransform = new CompositeTransform();
            _transform = new CompositeTransform()
            {
                ScaleX = 1,
                ScaleY = 1,
                TranslateX = _position.X,
                TranslateY = _position.Y
            };
            _previousTransform = new CompositeTransform()
            {
                ScaleX = 1,
                ScaleY = 1,
                TranslateX = _position.X,
                TranslateY = _position.Y
            };
        }

        private async Task SaveThumbnailToFile(WriteableBitmap WB)
        {
            var session = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
            if (_thumbnailFile == null)
            {
                _thumbnailFile = await session.Project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid().ToString()}.png");
            }
            using (IRandomAccessStream stream = await _thumbnailFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);
                var pixelStream = WB.PixelBuffer.AsStream();
                byte[] pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied,
                          (uint)WB.PixelWidth,
                          (uint)WB.PixelHeight,
                          96.0,
                          96.0,
                          pixels);
                await encoder.FlushAsync();
                stream.Seek(0);
                _thumbImage = new BitmapImage();
                await _thumbImage.SetSourceAsync(stream);
                RaiseEvent();
            }
        }

        private async Task<WriteableBitmap> Resize(uint width, uint height, IRandomAccessStream source)
        {
            var small = new WriteableBitmap((int)width, (int)height);
            var decoder = await BitmapDecoder.CreateAsync(source);
            var transform = new BitmapTransform();
            transform.ScaledHeight = height;
            transform.ScaledWidth = width;
            var pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Bgra8,
                BitmapAlphaMode.Premultiplied,
                transform,
                ExifOrientationMode.RespectExifOrientation,
                ColorManagementMode.DoNotColorManage);
            pixelData.DetachPixelData().CopyTo(small.PixelBuffer);

            return small;
        }

        protected void RaiseEvent()
        {
            LayoutViewPreviewChangedEvent?.Invoke(this);
        }

        protected async Task RaiseThumbnailGeneration()
        {
            if (LayerLayoutRaiseThumbnailGenerationEvent != null)
            {
                await LayerLayoutRaiseThumbnailGenerationEvent.Invoke(this);
            }
            await UpdateThumbnail();

        }

        #endregion
    }
}
