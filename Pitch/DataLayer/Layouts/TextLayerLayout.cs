﻿using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.UI.Text;

namespace Pitch.DataLayer.Layouts
{
    public class TextLayerLayout : LayerLayout
    {
        #region Private Fields

        private StorageFile _documentFile;
        private StorageFile _placeholderFile;
        private string _documentData;
        private string _placeHolderData = @"{\rtf1\fbidis\ansi\ansicpg1252\deff0\nouicompat\deflang1033{\fonttbl{\f0\fnil\fcharset0 Segoe UI;}}
{\colortbl ;\red0\green0\blue0;\red0\green0\blue0;}
{\*\generator Riched20 10.0.16299}\viewkind4\uc1 
\pard\qc\tx720\cf1\f0\fs72 Double-click to add text\par
}";
        private string _placeHoldertext = String.Empty;
        private string DocumentPlainText
        {
            get
            {
                var text = String.Empty;
                Document.GetText(TextGetOptions.NoHidden, out text);
                return text;
            }
        }
        #endregion

        #region Properties
        public ITextDocument Document { get; set; }
        public StorageFile File => _documentFile;
        public StorageFile PlaceholderFile => _placeholderFile;
        public bool IsNeedThumbnail => _thumbnailFile == null;
        public bool IsEmptyDocumentData => String.IsNullOrEmpty(_documentData);
        #endregion

        #region Life Cycle
        public TextLayerLayout(LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _documentData = String.Empty;
            SetDefaultParams();
        }

        public TextLayerLayout(string file, LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _documentData = file;
            SetDefaultParams();
        }

        protected TextLayerLayout(TextLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
            _documentData = layerLayout._documentData;
            _placeHolderData = layerLayout._placeHolderData;
            SetDefaultParams();
        }

        public TextLayerLayout(PlaceholderLayerLayout placeholderLayer, Shot shot) : base(placeholderLayer, shot)
        {
            _documentData = String.Empty;
            SetDefaultParams();
        }

#if DEBUG
        ~TextLayerLayout()
        {
            System.Diagnostics.Debug.WriteLine("*******************TextLayerLayout Destructor ********************");
        }
#endif

        #endregion

        #region Public Methods

        public override LayerLayout Clone(Shot shot = null)
        {
            SaveDocumentData();
            return new TextLayerLayout(this, shot);
        }

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.resource_url = _documentFile?.Name;
            data.placeholder_url = _placeholderFile?.Name;

            return data;
        }

        public static async Task<TextLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            var layerLayout = new TextLayerLayout(LayerLayoutTemplate.GenerateDefaultTemplate("Text"), shot);
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }

        public void SaveDocumentData()
        {
            if (Document == null || DocumentPlainText.Equals(_placeHoldertext)) return;

            try
            {
                Document.GetText(TextGetOptions.FormatRtf, out _documentData);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in TextLayerLayout.SaveDocumentToArray : {ex.Message}");
            }
        }
        public override async Task CopyDataToProjectFolder()
        {
            await base.CopyDataToProjectFolder();
            if (_documentFile != null)
            {
                _documentFile = await Shot.Project.CopyFileToLocalSandbox(_documentFile);
            }
            if (_placeholderFile != null)
            {
                _placeholderFile = await Shot.Project.CopyFileToLocalSandbox(_placeholderFile);
            }
        }
        public async Task SaveData()
        {
            if (Document == null) return;
            System.Text.Encoding encoding = new System.Text.UTF8Encoding(false);
            try
            {
                var str = String.Empty;
                Document.GetText(TextGetOptions.FormatRtf, out str);
                if (!DocumentPlainText.Equals(_placeHoldertext))
                {
                    if (!String.IsNullOrEmpty(str))
                    {
                        if (_documentFile == null)
                        {
                            _documentFile = await Shot.Project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid()}.rtf");
                        }

                        await SaveToFile(_documentFile, str, encoding);
                    }
                }
                else
                {
                    _documentFile = null;
                }

                if (_placeholderFile == null)
                {
                    _placeholderFile = await Shot.Project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid()}.rtf");
                }
                await SaveToFile(_placeholderFile, _placeHolderData, encoding);
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in TextLayerLayout.SaveData : {ex.Message}");
            }
        }

        public void LoadData()
        {
            if (Document == null) return;
            try
            {
                if (String.IsNullOrEmpty(_placeHoldertext))
                {
                    Document.SetText(TextSetOptions.FormatRtf | TextSetOptions.ApplyRtfDocumentDefaults, _placeHolderData);
                    _placeHoldertext = DocumentPlainText;
                }

                if (!String.IsNullOrEmpty(_documentData))
                {
                    Document.SetText(TextSetOptions.FormatRtf | TextSetOptions.ApplyRtfDocumentDefaults, _documentData);
                }
                ShowPlaceholder();
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in TextLayerLayout.LoadData : {ex.Message}");
            }
        }

        public void HidePlaceholder()
        {
            try
            {
                if (DocumentPlainText.Equals(_placeHoldertext))
                {
                    var textRange = Document.Selection.GetClone();

                    var outString = String.Empty;
                    Document.GetText(TextGetOptions.NoHidden, out outString);
                    textRange.SetRange(0, outString.Length - 1);

                    var paragraphFormat = textRange.ParagraphFormat.GetClone();
                    Document.SetText(TextSetOptions.FormatRtf | TextSetOptions.ApplyRtfDocumentDefaults, String.Empty);
                    Document.Selection.ParagraphFormat = paragraphFormat;
                    Document.Selection.CharacterFormat = textRange.CharacterFormat.GetClone();
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in TextLayerLayout.LoadData : {ex.Message}");
            }
        }

        public void ShowPlaceholder()
        {
            //if (App.Locator.AppState != ApplicationState.Recording)
            //{
                if (String.IsNullOrEmpty(_documentData) || String.IsNullOrEmpty(DocumentPlainText))
                {
                    if (String.IsNullOrEmpty(DocumentPlainText))
                    {
                        SaveDocumentData();
                    }
                    Document.SetText(TextSetOptions.FormatRtf | TextSetOptions.ApplyRtfDocumentDefaults, _placeHolderData);
                    _placeHoldertext = DocumentPlainText;
                }
            //}
        }

        public void UpdateMarkerList()
        {
            var textRange = Document.Selection.GetClone();
            if (textRange.ParagraphFormat.ListType != MarkerType.None && textRange.ParagraphFormat.ListType != MarkerType.Undefined)
            {
                var selection = textRange.GetClone();
                selection.EndPosition -= 1;
                selection.StartPosition = selection.EndPosition - 1;
                var lastFormat = selection.CharacterFormat.GetClone();

                selection = textRange.GetClone();
                selection.EndPosition += 1;
                var format = selection.CharacterFormat.GetClone();

                var cursorPosition = selection.EndPosition;
                selection.EndOf(TextRangeUnit.Paragraph, true);
                if (cursorPosition == selection.EndPosition)
                {
                    format = lastFormat;
                }
                selection.StartPosition = selection.EndPosition - 1;
                selection.CharacterFormat = format;
                Document.Selection.CharacterFormat = format.GetClone();

                selection = textRange.GetClone();
                selection.Move(TextRangeUnit.Line, -1);
                selection.StartOf(TextRangeUnit.Line, false);
                selection.EndPosition += 1;

                format = selection.CharacterFormat.GetClone();

                selection.EndOf(TextRangeUnit.Paragraph, false);
                selection.StartPosition -= 1;
                selection.CharacterFormat = format;
            }
        }
        public void UpdateDocumentStyle(ITextRange textRange)
        {
            var selection = textRange.GetClone();
            selection.StartOf(TextRangeUnit.Line, true);
            selection.EndOf(TextRangeUnit.Paragraph, true);

            var startPosition = selection.StartPosition;
            var endPosition = selection.EndPosition;
            selection.StartOf(TextRangeUnit.Line, false);

            if (textRange.ParagraphFormat.ListType != MarkerType.None && textRange.ParagraphFormat.ListType != MarkerType.Undefined)
            {
                do
                {
                    selection.EndOf(TextRangeUnit.Line, true);

                    var end = selection.EndPosition;
                    var markerSelection = selection.GetClone();

                    markerSelection.StartOf(TextRangeUnit.Line, false);
                    markerSelection.EndPosition += 1;

                    var format = markerSelection.CharacterFormat.GetClone();
                    markerSelection.SetRange(end - 1, end);
                    markerSelection.CharacterFormat = format;

                    startPosition = end + 1;
                    selection.SetRange(startPosition, startPosition + 1);
                } while (startPosition < endPosition);
            }
            else
            {
                do
                {
                    selection.EndOf(TextRangeUnit.Line, false);
                    selection.EndOf(TextRangeUnit.Line, true);
                    var end = selection.EndPosition;
                    var lineSelection = selection.GetClone();

                    lineSelection.StartPosition -= 1;

                    var format = lineSelection.CharacterFormat.GetClone();
                    lineSelection.SetRange(end, end + 1);
                    lineSelection.CharacterFormat = format;

                    startPosition = end + 1;
                    selection.SetRange(startPosition, startPosition + 1);
                } while (startPosition < endPosition);
            }

            Document.Selection.CharacterFormat = textRange.CharacterFormat.GetClone();
        }


        public override void Clean()
        {
            //TODO clean?
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {
            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? String.Empty : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return "html";
        }

        #endregion

        #region Protected Methods

        protected override async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            await base.UpdateFromMetaData(element);
            if (!String.IsNullOrEmpty(element.resource_url))
            {
                _documentFile = await Shot.Project.WorkingFolder.GetFileAsync(element.resource_url);
                _documentData = await FileIO.ReadTextAsync(_documentFile);
            }
            if (!String.IsNullOrEmpty(element.placeholder_url))
            {
                _placeholderFile = await Shot.Project.WorkingFolder.GetFileAsync(element.placeholder_url);
                _placeHolderData = await FileIO.ReadTextAsync(_placeholderFile);
            }
        }

        #endregion

        #region Private Methods

        private async Task SaveToFile(StorageFile file, string rtf, System.Text.Encoding encoding)
        {
            using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                using (TextWriter tw = new StreamWriter(stream.AsStream(), encoding))
                {
                    tw.WriteLine(rtf);
                }
            }
        }

        private void SetDefaultParams()
        {
            _isEditable = true;
            ComposingContextElement.Type = TouchCastComposingEngine.ComposingContextElementType.Bitmap;
            //ComposingContextElement.ContentMode = TouchCastComposingEngine.ContentMode.ScaleToFill;
        }

        #endregion
    }
}
