﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography.Certificates;
using Windows.Web.Http;

namespace Pitch.DataLayer
{
    public enum UrlProperties
    {
        Unknown             = 1,
        NoSSL               = 2,
        SSL                 = 4,
        IFrameBlocking      = 8,
        NoIframeBlocking    = 16
    }
    public class UrlWithProperties
    {
        #region Constants 

        private const string XFrameOptionHeader = "x-frame-options";
        private const string ContentSecurityHeader = "content-security-policy";
        private const string XContentSecurityHeader = "x-content-security-policy";

        #endregion

        #region Private
        UrlProperties _urlProperties;
        #endregion
        #region Properties

        public string UrlString { private set; get; }

        public UrlProperties Properties => _urlProperties;

        #endregion

        #region Constructors

        public UrlWithProperties(string url) : this(url, null, null) { }
        public UrlWithProperties(string url, Pitch.DataLayer.TcpInfo.UrlProperties urlProps) : this(url, urlProps?.ssl, urlProps?.iframe_blocking) { }
        public UrlWithProperties(string url, bool? ssl, bool? iframeBlocking)
        {
            UrlString = url;
            if(ssl.HasValue)
            {
                _urlProperties |= ssl.Value ? UrlProperties.SSL : UrlProperties.NoSSL;
            }
            
            if(iframeBlocking.HasValue)
            {
                _urlProperties |= iframeBlocking.Value ? UrlProperties.IFrameBlocking : UrlProperties.NoIframeBlocking;
            }
        }

        #endregion

        #region Public
        
        public string PropertiesAsString()
        {
            String str = null;
            if(_urlProperties.HasFlag(UrlProperties.NoSSL) || _urlProperties.HasFlag(UrlProperties.SSL))
            {
                str += $"ssl:{(_urlProperties.HasFlag(UrlProperties.SSL) ? 1 : 0)};";
            }

            if (_urlProperties.HasFlag(UrlProperties.IFrameBlocking) || _urlProperties.HasFlag(UrlProperties.NoIframeBlocking))
            {
                str += $"iframe-blocking:{(_urlProperties.HasFlag(UrlProperties.IFrameBlocking) ? 1 : 0)}";
            }
            return str;
        }

        public Pitch.DataLayer.TcpInfo.UrlProperties PropertiesInTCPFormat()
        {
            Pitch.DataLayer.TcpInfo.UrlProperties props = new Pitch.DataLayer.TcpInfo.UrlProperties();
            if (_urlProperties.HasFlag(UrlProperties.NoSSL) || _urlProperties.HasFlag(UrlProperties.SSL))
            {
                props.ssl = _urlProperties.HasFlag(UrlProperties.SSL);
            }

            if (_urlProperties.HasFlag(UrlProperties.IFrameBlocking) || _urlProperties.HasFlag(UrlProperties.NoIframeBlocking))
            {
                props.iframe_blocking = _urlProperties.HasFlag(UrlProperties.IFrameBlocking);
            }

            if(props.ssl == null && props.iframe_blocking == null)
            {
                props = null;
            }
            return props;
        }
   
        #endregion

        #region Static

        static UrlWithProperties()
        {
            _visitedUrls = new Dictionary<Uri, UrlWithProperties>();
        }

        private static Dictionary<Uri, UrlWithProperties> _visitedUrls;

        public static async Task<UrlWithProperties> Check(Uri uri)
        {
            UrlWithProperties returnValue = null;
            if (_visitedUrls.ContainsKey(uri))
            {
                returnValue = _visitedUrls[uri];
            }
            try
            {
                HttpResponseMessage response = await new HttpClient().GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var isFrameBlocking = CheckSecurityHeaders(response);
                    var isSSL = CheckServerCertificate(response);
                    returnValue = new UrlWithProperties(uri.ToString(), isSSL, isFrameBlocking);
                    _visitedUrls.Add(uri, returnValue);
                }
                else
                {
                    returnValue = new UrlWithProperties(uri.ToString());
                    _visitedUrls.Add(uri, returnValue);
                }
            }
            catch (Exception) { }

            return returnValue;
        }

        private static bool CheckSecurityHeaders(HttpResponseMessage response)
        {
            return (response.Headers.ContainsKey(XFrameOptionHeader)
                || response.Headers.ContainsKey(ContentSecurityHeader)
                || response.Headers.ContainsKey(XContentSecurityHeader));
        }

        private static bool CheckServerCertificate(HttpResponseMessage response)
        {
            Certificate certificate = response.RequestMessage.TransportInformation.ServerCertificate;
            if (certificate != null)
            {
                IReadOnlyList<ChainValidationResult> errors = response.RequestMessage.TransportInformation.ServerCertificateErrors;
                if (errors.Count != 0)
                {
                    return errors.Any(e => !e.HasFlag(ChainValidationResult.Success));
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
