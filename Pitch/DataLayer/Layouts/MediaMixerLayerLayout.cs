﻿using System.Threading.Tasks;
using System;
using Pitch.DataLayer.Shots;
using Pitch.Helpers.Logger;

namespace Pitch.DataLayer.Layouts
{
    public class MediaMixerLayerLayout : LayerLayout
    {
        public new string Type { get { return "mixer"; } }
        public static async Task<MediaMixerLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            MediaMixerLayerLayout layerLayout = new MediaMixerLayerLayout(shot);
            layerLayout._keepAspectRatio = false;
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }
        protected MediaMixerLayerLayout(Shot shot) : base(shot)
        {
            MediaMixerInit();
        }

        protected MediaMixerLayerLayout(MediaMixerLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
           
        }

        public MediaMixerLayerLayout(LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            MediaMixerInit();
        }
        private void MediaMixerInit()
        {
            _keepAspectRatio = false;
            _isEditable = false;
            _isLiveOnPlayback = false;
            _isHotSpot = false;
            _isPauseOnPlayback = false;
            ComposingContextElement.Type = TouchCastComposingEngine.ComposingContextElementType.CameraPreview;
        }

        public override async Task Initialize()
        {
        }

        public async Task UpdateMediaMixerThumbnail()
        {
            try
            {
                if (_thumbnailFile == null)
                {
                    _thumbnailFile = await Shot.Project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid()}.png", Windows.Storage.CreationCollisionOption.OpenIfExists);
                }
                using (var stream = await _thumbnailFile.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite))
                {    
                    try
                    {
                        await TouchCastComposingEngine.ComposingContext.Shared().SaveRenderTargetToFile(stream);
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in MediaMixerLayerLayoutPreview.UpdateThumbnail: {ex.Message}");
                    }
                    stream.Seek(0);

                    _thumbImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                    await _thumbImage.SetSourceAsync(stream);
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in MediaMixerLayerLayoutPreview.UpdateThumbnailFileIfNeeded : {ex.Message}");
            }
        }
        protected override Task GenerateThumbnail()
        {
            return Task.CompletedTask;
        }

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.type = "mixer";
            return data;
        }

        public override LayerLayout Clone(Shot shot = null)
        {
            return new MediaMixerLayerLayout(this, shot);
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {
            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? String.Empty : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return "html";
        }
    }
}
