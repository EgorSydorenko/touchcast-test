﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using TouchCastComposingEngine;

namespace Pitch.DataLayer.Layouts
{
    public class PlaceholderLayerLayout : LayerLayout
    {
        #region Life Cycle
        public static async Task<PlaceholderLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            PlaceholderLayerLayout layerLayout = new PlaceholderLayerLayout(shot);
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }

        public PlaceholderLayerLayout(Shot shot) : base(shot)
        {
            _isLiveOnPlayback = false;
            _isHotSpot = false;
            _isPauseOnPlayback = false;
            _isEditable = false;
        }

        public PlaceholderLayerLayout(LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _isLiveOnPlayback = false;
            _isHotSpot = false;
            _isPauseOnPlayback = false;
            _isEditable = false;
            _thumbnailFile = null;
        }

        public PlaceholderLayerLayout(LayerLayout layerLayout, Shot shot = null) : base(layerLayout, shot)
        {
            _isLiveOnPlayback = false;
            _isHotSpot = false;
            _isPauseOnPlayback = false;
            _isEditable = false;
        }
#if DEBUG
        ~PlaceholderLayerLayout()
        {
            System.Diagnostics.Debug.WriteLine("****************** PlaceholderLayerLayout Destructor ***********************");
        }
#endif
        #endregion

        public LayerLayoutInitialData GenerateLayerLayoutInitialData()
        {
            LayerLayoutInitialData result = new LayerLayoutInitialData(
                Position, _initialData.IsStatic, true, _initialData.IsHotSpotEnabled,
                IsLiveOnPlayback, IsPauseOnPlayback, InteractivityUrl?.UrlString, false)
            {
                ZIndex = _zIndex
            };
            return result;
        }

        public override LayerLayout Clone(Shot shot = null)
        {
            return new PlaceholderLayerLayout(this, shot);
        }

        protected override Task GenerateThumbnail()
        {
            return Task.CompletedTask;
        }
    }
}
