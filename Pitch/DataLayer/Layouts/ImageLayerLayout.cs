﻿using System;
using System.Threading.Tasks;
using Pitch.DataLayer.Shots;
using Pitch.Helpers;
using TouchCastComposingEngine;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace Pitch.DataLayer.Layouts
{
    public class ImageLayerLayout : LayerLayout
    {
        private StorageFile _file;

        public StorageFile File
        {
            get
            {
                return _file;
            }
        }
        public override bool IsStatic
        {
            get
            {
                return _isStatic;
            }
            set
            {
                _isStatic = value;
                _keepAspectRatio = !_isStatic;
            }
        }
        public static async Task<ImageLayerLayout> CreateFromMetaData(Shot shot, TcpInfo.Element element)
        {
            ImageLayerLayout layerLayout = new ImageLayerLayout(shot);
            await layerLayout.UpdateFromMetaData(element);
            return layerLayout;
        }

        protected ImageLayerLayout(Shot shot) : base(shot)
        {
            _keepAspectRatio = false;
            _isEditable = false;
            InitComposingContextElement();
        }

        protected ImageLayerLayout(ImageLayerLayout layerLayout, Shot shot) : base(layerLayout, shot)
        {
            _file = layerLayout._file;
        }
        public ImageLayerLayout(StorageFile file, PlaceholderLayerLayout placeholderLayer, Shot shot) : base(placeholderLayer, shot)
        {
            _file = file;
            _keepAspectRatio = !placeholderLayer.IsStatic;
            _isEditable = false;

            InitComposingContextElement();
        }
        public ImageLayerLayout(StorageFile file, LayerLayoutTemplate template, Shot shot) : base(template, shot)
        {
            _file = file;
            _keepAspectRatio = !template.InitialiData.IsStatic;
            _isEditable = false;

            InitComposingContextElement();
        }

        protected override async Task GenerateThumbnail()
        {
            if (_thumbnailFile == null)
            {
                string fileName = $"{Guid.NewGuid().ToString()}.png";
                using (IRandomAccessStream sourceStream = await _file.OpenAsync(FileAccessMode.Read))
                {
                    _thumbnailFile = await FileToAppHelper.SaveImageStreamToPngFile(sourceStream, ThumbnailSize, Shot?.Project?.WorkingFolder, fileName);
                }
                _thumbImage = new BitmapImage(new Uri(_thumbnailFile.Path));
            }
            try
            {
                using (var fileStream = await File.OpenReadAsync())
                {
                    ComposingContextElement.Bitmap = await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(ComposingContext.Shared().PreviewingDevice, fileStream);
                }
            }
            catch (Exception e)
            { }
        }

        protected override async Task UpdateFromMetaData(TcpInfo.Element element)
        {
            await base.UpdateFromMetaData(element);
            if (!String.IsNullOrEmpty(element.resource_url))
            {
                _file = await Shot.Project.WorkingFolder.GetFileAsync(element.resource_url);
            }
        }

        public override string IsWidget()
        {
            return "NO";
        }

        public override string ContentUrl()
        {

            return String.IsNullOrEmpty(_interactivityUrl?.UrlString) ? "($PERFORMANCE_BASE_URL)/" + _file?.Name : _interactivityUrl?.UrlString;
        }

        public override string ThumbnailUrl()
        {
            return "($PERFORMANCE_BASE_URL)/" + ThumbnailFile?.Name;
        }

        public override string ContentType()
        {
            return "image";
        }

        public override TcpInfo.Element GetMetaData()
        {
            var data = base.GetMetaData();
            data.resource_url = _file.Name;
            return data;
        }
        public override async Task CopyDataToProjectFolder()
        {
            await base.CopyDataToProjectFolder();
            if (_file != null)
            {
                _file = await Shot.Project.CopyFileToLocalSandbox(_file);
            }
        }
        public override LayerLayout Clone(Shot shot = null)
        {
            return new ImageLayerLayout(this, shot);
        }
        #region Private Methods

        private void InitComposingContextElement()
        {
            ComposingContextElement.Type = ComposingContextElementType.Bitmap;
            ComposingContextElement.ContentMode = ContentMode.ScaleAspectFill;
        }

        #endregion
    }
}
