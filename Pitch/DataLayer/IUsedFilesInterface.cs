﻿using System.Collections.Generic;
using Windows.Storage;

namespace Pitch.DataLayer
{
    interface IUsedFilesInterface
    {
        IList<StorageFile> UsedFiles();
    }
}
