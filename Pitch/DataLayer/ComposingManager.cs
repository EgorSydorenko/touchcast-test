﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.Services;
using Pitch.Helpers.Logger;
using TouchCastComposingEngine;

namespace Pitch.DataLayer
{
    public class ComposingManager
    {
        #region Private Fields

        private int _messageRate;
        private int _durationCounter;
        private int _uiMessageRate;

        private IMediaMixerService _mediaMixerService;
        private AuthoringSession _authoringSession;

        private const double TimeMessagesPerSecond = 10;

        private const double UpdateUITimeMessagesPerSecond = 2;

        //stopwatch is used to measure time spend on vapps rendering
        //just for debug
        private Stopwatch _debugStopwatch;
        private Task _vappSnapshotUpdateTask;
        private CancellationTokenSource _cancelTokenSource;
        private TouchcastRecorder _recorder;

        #endregion

        #region Properties 

        public double ElapsedSeconds
        {
            get
            {
                double elapsedSeconds = 0;
                if (_mediaMixerService.State == TouchcastComposerServiceState.Recording
                    || _mediaMixerService.State == TouchcastComposerServiceState.Paused
                    || _mediaMixerService.State == TouchcastComposerServiceState.Completed)
                    elapsedSeconds = _mediaMixerService.VideoDuration.TotalSeconds;

                return elapsedSeconds;
            }
        }

        private TimeSpan RecordingDuration
        {
            get { return _mediaMixerService.VideoDuration; }
        }

        public TouchcastRecorder TouchcastRecorder => _recorder;

        private AuthoringSession AuthoringSession
        {
            get
            {
                if (_authoringSession == null)
                {
                    _authoringSession = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
                }
                return _authoringSession;
            }
        }

        #endregion

        #region Life Cycle

        public ComposingManager()
        {
            _mediaMixerService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<IMediaMixerService>();
            _recorder = new TouchcastRecorder();
            //will be listening for the update renderable items list
            //and provide requested information
            if (_mediaMixerService != null)
            {
                _mediaMixerService.StateChangedEvent += MediaMixerServiceStateChangedEvent;
                _mediaMixerService.WillStartRecordingEvent += MediaMixerServiceWillStartRecordingEvent;
            }
            _debugStopwatch = new Stopwatch();
        }

#if DEBUG
        ~ComposingManager()
        {
            Debug.WriteLine("********************AppsService Destructor********************");
        }
#endif

        #endregion

        #region Public Methods

        public void Reset()
        {
            LogQueue.WriteToFile("ComposingManager Reset");
            Messenger.Default.Unregister(this);
            _mediaMixerService.StateChangedEvent -= MediaMixerServiceStateChangedEvent;
            _mediaMixerService.WillStartRecordingEvent -= MediaMixerServiceWillStartRecordingEvent;
            _mediaMixerService = null;
            if (_cancelTokenSource != null)
            {
                _cancelTokenSource.Cancel();
                _cancelTokenSource = null;
            }
            _vappSnapshotUpdateTask = null;
            _authoringSession = null;
        }

        public void RecordCommand(int id, AppMessageType type, LayerLayout layer)
        {
            _recorder.PushNewVappMessage(id, type, layer, _mediaMixerService.VideoDuration.TotalSeconds);
        }
        public void RecordEvent(TouchcastEventType type, object contents)
        {
            _recorder.PushNewEvent(type, _mediaMixerService.VideoDuration.TotalSeconds, contents);
        }

        #endregion

        #region Composer Service Events

        private void MediaMixerServiceWillStartRecordingEvent(object sender)
        {
            try
            {
                UpdateRenderableElementsList();
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in ComposingManager.MediaMixerServiceWillStartRecordingEvent : {ex.Message}");
            }
        }

        private void MediaMixerServiceStateChangedEvent(object sender)
        {
            var composerObject = sender as IMediaMixerService;
            if (composerObject == null) return;

            if (composerObject.State == TouchcastComposerServiceState.Recording)
            {
                _cancelTokenSource = new CancellationTokenSource();
                _vappSnapshotUpdateTask = new Task(OnVappSnapshotTask, _cancelTokenSource.Token);
                _vappSnapshotUpdateTask.Start();
                _debugStopwatch.Start();
            }
            else
            {
                //no need to update vapps if recording is not started 
                try
                {
                    _debugStopwatch.Stop();
                    if (_cancelTokenSource != null)
                    {
                        _cancelTokenSource.Cancel();
                        _cancelTokenSource = null;
                    }
                    _vappSnapshotUpdateTask = null;
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        #endregion

        #region Managing vapps Shanpshots

        private void UpdateRenderableElementsList()
        {
            var activeShot = AuthoringSession.Project.ActiveShot;
            var models = activeShot.Layers.Where(x=> !(x is PlaceholderLayerLayout)).OrderBy(x => x.ZIndex);
            IList<ComposingContextElement> elements = new List<ComposingContextElement>();
            if (AuthoringSession.Project.BackgroundRenderableObject.IsBackgroundEnabled)
            {
                elements.Add(AuthoringSession.Project.BackgroundRenderableObject.BackgroundContextElement);
            }
            foreach (var model in models)
            {
                if (model.IsHotSpot == true)
                {
                    continue;
                }

                if (!App.Locator.IsLightMode && 
                    AuthoringSession.Project.BackgroundRenderableObject.IsBackgroundEnabled &&
                    model.IsBackground)
                {
                    continue;
                }

                model.RefreshRelativePosition();
                elements.Add(model.ComposingContextElement);
            }
            if(AuthoringSession.Project.ActiveShot.IsInkingTurnedOn)
            {
                elements.Add(AuthoringSession.Project.ActiveShot.StrokesComposingElement);
            }
            ComposingContext.Shared().Elements = elements;
            ComposingContext.Shared().FrameBackgroundColor = activeShot.BackgroundColor;
        }

        private async void OnVappSnapshotTask()
        {
            var frameRate = TimeSpan.FromMilliseconds(50);
            _messageRate = (int)(1000.0 / TimeMessagesPerSecond / frameRate.Milliseconds);
            _durationCounter = 0;
            _uiMessageRate = (int)(1000.0 / UpdateUITimeMessagesPerSecond / frameRate.Milliseconds);

            while (_cancelTokenSource != null
                && !_cancelTokenSource.IsCancellationRequested
                && _mediaMixerService != null)
            {
                try
                {
                    DurationChanged();

                    var startTime = _debugStopwatch.Elapsed;
                    try
                    {
                        UpdateRenderableElementsList();
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in ComposingManager.OnVappSnapshotTask : {ex.Message}");
                    }

                    var workTime = _debugStopwatch.Elapsed - startTime;

                    if (workTime < frameRate)
                    {
                        await Task.Delay(frameRate - workTime);
                    }
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in ComposingManager.OnVappSnapshotTask : {ex.Message}");
                    return;
                }
            }
        }

        private void DurationChanged()
        {
            if (++_durationCounter == _messageRate)
            {
                _durationCounter = 0;
            }
        }

        #endregion
    }
}
