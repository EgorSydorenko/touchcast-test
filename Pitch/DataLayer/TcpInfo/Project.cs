﻿using System.Collections.Generic;

namespace Pitch.DataLayer.TcpInfo
{
    public class Teleprompter
    {
        public string text_file { set; get; }
        public double speed { set; get; }
    }
    public class Transition
    {
        public string type { set; get; }
        public double duration { set; get; }
    }
    public class SegmentationSettings
    {
        public bool enabled { set; get; }
        public bool auto { set; get; }
        public ManualSegmentationSettings manual_segmentation_settings { set; get; }
        public string background_file { set; get; }
        public bool silhouette_mode { set; get; }
        public int scene_index { set; get; }
    }
    public class ManualSegmentationSettings
    {
        public string color { set; get; }
        public double smoothness { set; get; }
        public double sensitivity { set; get; }
    }
    public class Camera
    {
        public int scene_index { set; get; }
        public string name { set; get; }
        public bool mirroring { set; get; }
    }
    public class Microphone
    {
        public int scene_index { set; get; }
        public string name { set; get; }
        public double volume { set; get; }
    }
    public class SceneThumbnail
    {
        public static string LayerBackgroundKey = "background_file";
        public static string LayerCameraKey = "camera_file";
        public static string LayerForegroundKey = "foreground_file";

        public int scene_index { set; get; }
        public string composed_file { set; get; }
        public Dictionary<string,string> layers { set; get; }
    }
    public class UrlProperties
    {
        public bool? ssl { set; get; }
        public bool? iframe_blocking { set; get; }
    }
    public class Element
    {
        public string type { set; get; }
        public string interactivity_url { set; get; }
        //http(s) - webpage 
        //local:
        //jpg, png - image
        //mp4 - video
        //pdf - document
        //rtf - is for text,
        //shp - for shape
        public string resource_url { set; get; }
        public string placeholder_url { set; get; }
        public string thumbnail_file { set; get; }
        public string hd_thumbnail_file { set; get; }
        public int scene_index { set; get; }
        public string transform { set; get; }
        public bool keep_aspect_ratio { set; get; }
        public int z_index { set; get; }
        public double opacity { set; get; }
        public bool locked {set;get;}
        public bool pause_on_interaction { set; get; }
        public bool interative { set { interactive = value; } get { return interactive; } }
        public bool interactive { set; get; }
        public bool background { set; get; }
        public bool? autostart { set; get; }
        public UrlProperties resource_url_properties { set; get; }
        public UrlProperties interactivity_url_properties { set; get; }
    }
    public class Recording
    {
        public string id { set; get; }
        public string video_file { set; get; }
        public string actions_file { set; get; }
        public string thumbnail_file { set; get; }
        public double duration { set; get; }
        public List<string> resources { set; get; }
    }
    public class CompositionItem
    {
        public string recording_id { set; get; }
        public string thumbnail_file { set; get; }
        public Range range { set; get; }
        public Range max_range { set; get; }
        public int? scene_index { set; get; }
        public int clip_index { set; get; }
        public int take { set; get; }
        public int active { set; get; }
        public Transition out_transition { set; get; }

    }
    public class Range
    {
        public double start { set; get; }
        public double duration { set; get; }
    }
    public class Project
    {
        public float? version { set; get; }
        public Size canvas_size { set; get; }
        public string name { set; get; }
        public string description { set; get; }
        public string theme_folder { set; get; }
        public List<Element> elements { set; get; }
        public List<SceneThumbnail> scene_thumbnails { set; get; }
        public Teleprompter teleprompter { set; get; }
        public List<Camera> cameras { set; get; }
        public List<Microphone> microphones { set; get; }
        //public Teleprompter teleprompter { set; get; }
        public List<SegmentationSettings> segmentations { set; get; }
        public List<Recording> recordings { set; get; }
        public List<CompositionItem> video_composition { set; get; }
        public List<string> background_colors { set; get; }
        public Project()
        {
            elements = new List<Element>();
            segmentations = new List<SegmentationSettings>();
            recordings = new List<Recording>();
            scene_thumbnails = new List<SceneThumbnail>();
            teleprompter = new Teleprompter();
            cameras = new List<Camera>();
            microphones = new List<Microphone>();
            background_colors = new List<string>();
        }
    }
    public class Point
    {
        public double x { set; get; }
        public double y { set; get; }
    }
    public class Shape
    {
        public string version { set; get; }
        public string type { set; get; }
        public string fill_color { set; get; }
        public string stroke_color { set; get; }
        public double stroke_width { set; get; }
        public Point start_point { set; get; }
        public Point end_point { set; get; }
    }
    public class Size
    {
        public double width;
        public double height;
    }
}
