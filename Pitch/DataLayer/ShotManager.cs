﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Pitch.DataLayer.Shots;
using Pitch.DataLayer.UndoRedo;
using Pitch.Services;
using Pitch.UILayer.Authoring.Views;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Pitch.UILayer.Helpers.Messages;
using System.Threading;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.DataLayer
{
    //Need to Create
    public class ShotManager
    {
        public delegate Task ShotManagerEvent(Shot model);

        #region Events
        public event ShotManagerEvent ShotManagerWillBeInialized;
        public event ShotManagerEvent ShotManagerWasInitialized;
        public event ShotManagerEvent ShotManagerWillBeCleaned;
        public event ShotManagerEvent ShotManagerWasCleaned;
        public event ShotManagerEvent ShotWasHidden;
        public event ShotManagerEvent ShotWillBeDisplayed;
        public event ShotManagerEvent ShotWasDisplayed;
        public event ShotManagerEvent ShotWillBeHidden;
        #endregion

        #region Private Fields
        private CancellationTokenSource _cancellationTokenSource;
        private Project _project;
        private List<ShotView> _shotViews;
        private Grid _shotContainerView;
        private ShotView _activeShotView;
        private bool _isShowShotInProcess;
        private readonly object _locker = new object();
        private WeakReference<IMediaMixerService> _imediaMixerService;

        private IMediaMixerService MediaMixer => _imediaMixerService?.TryGetObject();

        #endregion

        #region Public Properties
        public Grid ShotContainerView
        {
            set => _shotContainerView = value;
            get => _shotContainerView;
        }

        public List<ShotView> ShotViews => _shotViews;

        public bool IsShowShotInProcess => _isShowShotInProcess;

        public ShotView ActiveShotView => _activeShotView;
        #endregion

        #region Life Cycle
        public ShotManager(Project project)
        {
            _project = project;
            _shotViews = new List<ShotView>();

            var mediaMixer = SimpleIoc.Default.GetInstance<IMediaMixerService>();
            _imediaMixerService = new WeakReference<IMediaMixerService>(mediaMixer);
        }

#if DEBUG
        ~ShotManager()
        {
            System.Diagnostics.Debug.WriteLine("****************** ShotManager Destructor******************");
        }
#endif
        #endregion

        #region Public Methods
        public async Task Initialize()
        {
            if (ShotManagerWillBeInialized != null)
            {
                await ShotManagerWillBeInialized.Invoke(null);
            }
            foreach (var shot in _project.Shots)
            {
                var oldView = _shotViews.FirstOrDefault(v => v.Shot == shot);
                if (oldView == null)
                {
                    var shotView = new ShotView(shot)
                    {
                        Visibility = Visibility.Collapsed
                    };
                    _shotViews.Add(shotView);
                }
            }
            _activeShotView = _shotViews.FirstOrDefault(v => v.Shot == _project.ActiveShot);

            if (_activeShotView != null && !_shotContainerView.Children.Contains(_activeShotView))
            {
                _activeShotView.Visibility = Windows.UI.Xaml.Visibility.Visible;
                _activeShotView.PrepareShotBeforeDisplaying();
                _shotContainerView.Children.Add(_activeShotView);
            }
            _project.ShotWasRemoved += Project_ShotWasRemoved;
            _project.UndoRedoOriginator.UndoEvent += UndoRedoOriginator_UndoEvent;
            _project.UndoRedoOriginator.RedoEvent += UndoRedoOriginator_RedoEvent;


            if (ShotManagerWasInitialized != null)
            {
                await ShotManagerWasInitialized.Invoke(null);
            }
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async Task<bool> ShowShot(Shot model)
        {
            if (model != _project.ActiveShot)
            {
                var shotId = _project.ActiveShot.Id;
                var result = await ShowShotWithoutUndoRedo(model);
                if (result)
                {
                    _project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change,
                        new ShotChangedMementoModel { ShotId = shotId, Visibility = Visibility.Visible });
                }
                    return result;
            }
                return false;

        }

        public async Task<bool> ShowShotWithoutUndoRedo(Shot model)
        {
            lock(_locker)
            {
                if (_isShowShotInProcess)
                {
                    return false;
                }
                _isShowShotInProcess = true;
            }
            TouchCastComposingEngine.ComposingContext.Shared().IsNeedToUpdatePreview = false;
            var isNeedToRestoreRecording = await HideShot(_project.ActiveShot);
            var oldShot = _project.ActiveShot;
            if (ShotWillBeDisplayed != null)
            {
                await ShotWillBeDisplayed.Invoke(model);
            }
            _activeShotView = _shotViews.FirstOrDefault(v => v.Shot == model);

            if (_activeShotView == null)
            {
                _activeShotView = new ShotView(model);
                _shotViews.Add(_activeShotView);
            }
            if (!_shotContainerView.Children.Contains(_activeShotView))
            {
                _shotContainerView.Children.Add(_activeShotView);
            }

            _activeShotView.PrepareShotBeforeDisplaying();
            
            _project.ActivateNewShot(model);
            _activeShotView.Visibility = Visibility.Visible;

            if (ShotWasDisplayed != null)
            {
                await ShotWasDisplayed?.Invoke(model);
            }
            if (isNeedToRestoreRecording)
            {
                Messenger.Default.Send(new BusyIndicatorHideMessage());
            }
            //TouchCastComposingEngine.ComposingContext.Shared().IsNeedToUpdatePreview = true;
            lock (_locker)
            {
                _isShowShotInProcess = false;
            }
            return true;
        }
        public void BlockShotChanging()
        {
            _isShowShotInProcess = true;
        }
        public void UnBlockShotChanging()
        {
            _isShowShotInProcess = false;
        }
        public void Clean()
        {
            if(ShotManagerWillBeCleaned != null)
            {
                ShotManagerWasCleaned?.Invoke(null);
            }
            var mixer = MediaMixer;

            _shotContainerView = null;
            _project.UndoRedoOriginator.UndoEvent -= UndoRedoOriginator_UndoEvent;
            _project.UndoRedoOriginator.RedoEvent -= UndoRedoOriginator_RedoEvent;
            _project.ShotWasRemoved -= Project_ShotWasRemoved;
            _project = null;
            _shotViews.Clear();
            _shotContainerView = null;
            if (ShotManagerWasCleaned != null)
            {
                ShotManagerWasCleaned.Invoke(null);
            }
        }

        #endregion

        #region Private Methods
        private async Task<bool> HideShot(Shot model)
        {
            bool result = MediaMixer?.State == TouchcastComposerServiceState.Recording;
            if (result)
            {
                Messenger.Default.Send(new BusyIndicatorShowMessage());
            }
            if (ShotWillBeHidden != null)
            {
                await ShotWillBeHidden?.Invoke(model);
            }

            var shotView = _shotViews.FirstOrDefault(x => x.Shot.Id == model.Id);
            if (shotView != null)
            {
                await shotView.PrepareShotBeforeHiding();
                shotView.Visibility = Visibility.Collapsed;
            }

            if (ShotWasHidden != null)
            {
                await ShotWasHidden?.Invoke(model);
            }
            return result;
        }
        
        private Task Project_ShotWasRemoved(Shot shot)
        {
            var shotViewToRemove = _shotViews.FirstOrDefault(s => s.Shot == shot);
            if (shotViewToRemove != null)
            {
                _shotViews.Remove(shotViewToRemove);
            }

            return Task.CompletedTask;
        }

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_RedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_UndoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> ProcessUndoRedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            if (mementoModel.MementoObject is ShotChangedMementoModel && mementoModel.ActionType == UndoRedoMementoType.Change)
            {
                var shotMementoModel = mementoModel.MementoObject as ShotChangedMementoModel;
                var shot = _project.Shots.FirstOrDefault(s => s.Id == shotMementoModel.ShotId);
                if (shot != null)
                {
                    if (shotMementoModel.Visibility == Visibility.Visible)
                    {
                        var currentlyActiveShotId = _project.ActiveShot.Id;
                        await ShowShotWithoutUndoRedo(shot);
                        shotMementoModel.ShotId = currentlyActiveShotId;
                        return mementoModel;
                    }
                }
            }
            return null;
        }
        #endregion
    }
}
