﻿using Windows.Storage;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Pitch.Services;
using System;

namespace Pitch.DataLayer
{
    public class GlobalManager
    {
        private ApplicationState _appState;
        public GlobalManager()
        {
            try
            {
                ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
                SimpleIoc.Default.Register<ISettingsService, SettingsService>(true);

                var settingsService = SimpleIoc.Default.GetInstance<ISettingsService>();
                IsLightMode = (settingsService.LightModeUserPermission == ModeUserPermission.Undefined) ? (!settingsService.IsDeviceStrong)
                    : (settingsService.LightModeUserPermission == ModeUserPermission.LightModeEnabled);
            }
            catch (Exception)
            {
            }
        }

        public TouchcastComposerServiceMode ComposingMode { set; get; }

        public ApplicationState AppState {
            set
            {
                _appState = value;
            }
            get
            {
                return _appState;
            }
        }

        public StorageFile ProjectFile { set; get; }

        public bool IsShowPerformanceWarning { set; get; } = true;

        public bool IsLightMode { private set; get; }

        public void RegisterAuthoringSession()
        {
            SimpleIoc.Default.Register<IMediaMixerService, MediaMixerService>(true);
            SimpleIoc.Default.Register<AuthoringSession>();
            SimpleIoc.Default.Register<ComposingManager>(true);
        }

        public void UnRegisterAuthoringSession()
        {
            SimpleIoc.Default.Unregister<IMediaMixerService>();
            SimpleIoc.Default.Unregister<AuthoringSession>();
            SimpleIoc.Default.Unregister<ComposingManager>();
            if (ProjectFile != null)
            {
                ProjectFile = null;
            }
        }

        public void RegisterRecordingManager()
        {
            SimpleIoc.Default.Register<RecordingManager>();
        }

        public void UnregisterRecordingManager()
        {
            SimpleIoc.Default.Unregister<RecordingManager>();
        }
    }
}
