﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;

namespace Pitch.DataLayer
{
    public class ThemeManager
    {
        public class Theme
        {
            public enum Type
            {
                theme,
                virtual_set,
                smart_template
            }
            public enum Organization
            {
                all,
                accenture
            }
            public string name { set; get; }
            public string description { set; get; }
            public string thumbnail_file_path { set; get; }
            public string theme_foler_path { set; get; }
            public bool is_default { set; get; }
            public bool is_selected { set; get; }
            public Type type { set; get; }
            public bool is_visible { set; get; }
            public Organization organization { set; get; }
        }
        #region Private fields
        private List<Theme> _themes;
        private string _initialFilePath = "PreloadedThemes\\PreloadThemes.txt";
        private static ThemeManager _instance;
        #endregion

        public static ThemeManager Instance => _instance;

        static ThemeManager()
        {
            _instance = new ThemeManager();
        }
        
        private ThemeManager()
        {
            _themes = new List<Theme>();
        }
        public List<Theme> Themes => _themes.Where(x => x.is_visible).ToList();
        public Theme DefaultTheme => Themes.FirstOrDefault(x => x.is_default);
        public Theme SelectedTheme => Themes.FirstOrDefault(x => x.is_selected);

        public async Task Initialize()
        {
            var file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(_initialFilePath);
            string text = await FileIO.ReadTextAsync(file);
            _themes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Theme>>(text);
        }

        public void SelectThemeByName(string Name)
        {
            foreach (var item in _themes)
            {
                item.is_selected = false;
            }
            var selectedTheme  = _themes.FirstOrDefault(x => x.name == Name);
            if(selectedTheme != null)
            {
                selectedTheme.is_selected = true;
            }
            else
            {
                var defaultTheme = _themes.FirstOrDefault(x => x.is_default);
                defaultTheme.is_selected = true;
            }
        }
    }
}
