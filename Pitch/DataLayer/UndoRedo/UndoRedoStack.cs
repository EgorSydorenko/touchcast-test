﻿using System.Collections.Generic;
using System.Linq;

namespace Pitch.DataLayer.UndoRedo
{
    public class UndoRedoStack<IUndoRedoBaseMementoModelInterface>
    {
        private uint _maxStackSize;
        private List<IUndoRedoBaseMementoModelInterface> _stack;
        public IReadOnlyList<IUndoRedoBaseMementoModelInterface> Stack => _stack;
        public bool IsEmpty => _stack.Count == 0;
        public UndoRedoStack(uint maxSize)
        {
            _maxStackSize = maxSize;
            _stack = new List<IUndoRedoBaseMementoModelInterface>();
        }

        public void Push(IUndoRedoBaseMementoModelInterface item)
        {
            if (_stack.Count >= _maxStackSize)
            {
                var i = _stack.First();
                
                _stack.Remove(i);

                if (i is UndoRedoBaseMementoModel)
                {
                    (i as UndoRedoBaseMementoModel).MementoObject.Clean();
                }
            }
            _stack.Add(item);
        }

        public IUndoRedoBaseMementoModelInterface Pop()
        {
            var item = _stack.LastOrDefault();
            _stack.Remove(item);
            return item;
        }

        public void Clear()
        {
            _stack.Clear();
        }
    }
}
