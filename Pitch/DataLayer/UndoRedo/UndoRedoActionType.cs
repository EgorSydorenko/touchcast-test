﻿namespace Pitch.DataLayer.UndoRedo
{
    public enum UndoRedoMementoType
    {
        Add,
        Remove,
        Change
    }
}
