﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pitch.DataLayer.UndoRedo
{
    //1. Clean undoRedo stack after saving Project. 
    //2. Clean undo redo stack after project closing
    // 1 OR 2

    public class UndoRedoStacksChangedEventArgs
    {
        public bool IsUndoStackEmpty { get; }
        public bool IsRedoStackEmpty { get; }
        public UndoRedoStacksChangedEventArgs(bool isUndoStackEmpty, bool isRedoStackEmpty)
        {
            IsUndoStackEmpty = isUndoStackEmpty;
            IsRedoStackEmpty = isRedoStackEmpty;
        }
    }

    public delegate Task<UndoRedoBaseMementoModel> UndoRedoOriginatorDelegate(UndoRedoBaseMementoModel mementoModel);
    public delegate void UndoRedoStacksChanged(UndoRedoStacksChangedEventArgs args);
    public class UndoRedoOriginator
    {
        #region Private fields
        private const uint MaxStackSize = 8;
        private UndoRedoStack<UndoRedoBaseMementoModel> _undoStack;
        private UndoRedoStack<UndoRedoBaseMementoModel> _redoStack;
        #endregion

        public event UndoRedoOriginatorDelegate UndoEvent;
        public event UndoRedoOriginatorDelegate RedoEvent;
        public event UndoRedoStacksChanged UndoRedoStacksChangedEvent;

        #region Life cycle
        public UndoRedoOriginator()
        {
            _undoStack = new UndoRedoStack<UndoRedoBaseMementoModel>(MaxStackSize);
            _redoStack = new UndoRedoStack<UndoRedoBaseMementoModel>(MaxStackSize);
        }
        #endregion

        #region Public 
        public void PushNewUndoAction(UndoRedoMementoType type, ICleanable obj)
        {
            var model = new UndoRedoBaseMementoModel(type, obj);
            PushNewUndoAction(model);
        }
        public void PushNewUndoAction(UndoRedoBaseMementoModel model)
        {
            if (model != null)
            {
                _undoStack.Push(model);
                _redoStack.Clear();
                FireStacksChangedEvent();
            }
        }
        public void Clean()
        {
            foreach (var item in _undoStack.Stack)
            {
                if (item is UndoRedoBaseMementoModel)
                {
                    (item as UndoRedoBaseMementoModel).MementoObject.Clean();
                }
            }
            _undoStack.Clear();
            foreach (var item in _redoStack.Stack)
            {
                if (item is UndoRedoBaseMementoModel)
                {
                    (item as UndoRedoBaseMementoModel).MementoObject.Clean();
                }
            }
            _redoStack.Clear();
        }

        public async Task Undo()
        {
            var actionModelToProceed = _undoStack.Pop();
            if (UndoEvent != null && actionModelToProceed != null)
            {
                var models = await ProcessModel(UndoEvent, actionModelToProceed);
                if (models.Any())
                {
                    _redoStack.Push(GenerateUndoChain(models));
                }
            }
            FireStacksChangedEvent();
        }

        public async Task Redo()
        {
            var actionModelToProceed = _redoStack.Pop();
            if (RedoEvent != null && actionModelToProceed != null)
            {
                var models = await ProcessModel(RedoEvent, actionModelToProceed);
                if (models.Any())
                {
                    _undoStack.Push(GenerateUndoChain(models));
                }
            }
            FireStacksChangedEvent();
        }

        public UndoRedoStacksChangedEventArgs State => new UndoRedoStacksChangedEventArgs(_undoStack.IsEmpty, _redoStack.IsEmpty);

        #endregion

        #region Private Region
        private void PushNewRedoAction(UndoRedoMementoType type, ICleanable obj)
        {
            //reverse Type before adding model to redo stack
            var model = new UndoRedoBaseMementoModel(type, obj);
            if (model != null)
            {
                _redoStack.Push(model);
            }
        }
        private void FireStacksChangedEvent()
        {
            UndoRedoStacksChangedEvent?.Invoke(new UndoRedoStacksChangedEventArgs(_undoStack.IsEmpty, _redoStack.IsEmpty));
        }
        private UndoRedoBaseMementoModel GenerateUndoChain(List<UndoRedoBaseMementoModel> models)
        {
            var i = models.Count - 1;
            var result = models[i];
            for (; i > 0; --i)
            {
                models[i].ChainNode = models[i - 1];
                models[i - 1].ChainNode = null;
            }
            return result;
        }

        private async Task<UndoRedoBaseMementoModel> Invoke(UndoRedoOriginatorDelegate delegat, UndoRedoBaseMementoModel actionModelToProceed)
        {
            var invokers = delegat.GetInvocationList().ToList().OfType<UndoRedoOriginatorDelegate>();
            foreach (var invoker in invokers)
            {
                var model = await invoker.Invoke(actionModelToProceed);
                if (model != null)
                {
                    return model;
                }
            }
            return null;
        }

        private async Task<List<UndoRedoBaseMementoModel>> ProcessModel(UndoRedoOriginatorDelegate delegat, UndoRedoBaseMementoModel modelToProceed)
        {
            var models = new List<UndoRedoBaseMementoModel>();
            do
            {
                var model = await Invoke(delegat, modelToProceed);
                model?.ReverseAction();
                models.Add(model);
                modelToProceed = modelToProceed.ChainNode;
            } while (modelToProceed != null);

            return models;
        }

        #endregion
    }
}
