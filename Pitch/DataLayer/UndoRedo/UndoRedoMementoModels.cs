﻿using System;

namespace Pitch.DataLayer.UndoRedo
{
    public interface ICleanable
    {
        void Clean();
    }
    public interface IUndoRedoBaseMementoModelInterface
    {
        ICleanable MementoObject { set; get; }
        UndoRedoMementoType ActionType { get; }
        UndoRedoBaseMementoModel ChainNode { get; set; }
        void ReverseAction();
    }
    public class UndoRedoBaseMementoModel : IUndoRedoBaseMementoModelInterface
    {
        protected UndoRedoMementoType _actionType;
        protected ICleanable _mementoObject;
        protected UndoRedoBaseMementoModel _chainNode;
        public UndoRedoMementoType ActionType => _actionType;
        public ICleanable MementoObject
        {
            set => _mementoObject = value;
            get => _mementoObject;
        }
        public UndoRedoBaseMementoModel ChainNode { get => _chainNode; set => _chainNode = value; }
        public UndoRedoBaseMementoModel(UndoRedoMementoType type, ICleanable mementoObject, UndoRedoBaseMementoModel node = null)
        {
            _actionType = type;
            _mementoObject = mementoObject;
            _chainNode = node;
        }
        public void ReverseAction()
        {
            _actionType = _actionType == UndoRedoMementoType.Add ? UndoRedoMementoType.Remove 
                : _actionType == UndoRedoMementoType.Remove ? UndoRedoMementoType.Add 
                : UndoRedoMementoType.Change;
        }
    }
}
