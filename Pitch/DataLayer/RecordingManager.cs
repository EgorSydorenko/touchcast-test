﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Pitch.Models;
using Pitch.Services;
using Pitch.Helpers.Logger;
using Pitch.UILayer.Helpers.Messages;
using Pitch.DataLayer.Helpers.Extensions;
using System.Collections.Generic;

namespace Pitch.DataLayer
{
    public class RecordingManager
    {
        #region Private fields
        private AsyncLock _lock = new AsyncLock(1);
        private WeakReference<AuthoringSession> _authoringSession;
        private WeakReference<IMediaMixerService> _mediaMixerService;
        private TcMediaClip _activeMediaClip;
        private AuthoringSession AuthoringSession => _authoringSession?.TryGetObject();

        #endregion

        #region Life cycle
        public RecordingManager()
        {
            _authoringSession = new WeakReference<AuthoringSession>(SimpleIoc.Default.GetInstance<AuthoringSession>());
            _mediaMixerService = new WeakReference<IMediaMixerService>(SimpleIoc.Default.GetInstance<IMediaMixerService>());
        }

#if DEBUG
        ~RecordingManager()
        {
            System.Diagnostics.Debug.WriteLine("****************** RecordingManager Destructor ********************");
        }
#endif

        #endregion

        #region Public methods
        public IMediaMixerService MediaMixer => _mediaMixerService?.TryGetObject();


        public void CreateNewTake()
        {
            LogQueue.WriteToFile("RecordingManager CreateNewTake");
            CreateNewTakeIfNeeded();
            Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticsCommandRetakeSceneInRecording);
            var mixer = MediaMixer;
            if (mixer != null && mixer.State != TouchcastComposerServiceState.Recording)
            {
                Messenger.Default.Send(new StartCountingMessage());
            }
        }

        public async Task StartRecording()
        {
            LogQueue.WriteToFile("RecordingManager StartRecording");
            var mixer = MediaMixer;
            if (mixer != null)
            {
                var session = AuthoringSession;
                if (session != null)
                {
                    if (session.Project.TcMediaComposition.MediaClipToProcess != null)
                    {
                        _activeMediaClip = session.Project.TcMediaComposition.MediaClipToProcess;
                        session.Project.TcMediaComposition.MediaClipToProcess = null;
                    }

                    if (_activeMediaClip == null || (session.Project.TcMediaComposition.IsRecordMoreAtEnd && _activeMediaClip != null))
                    {
                        await session.Project.TcMediaComposition.AddNewClip(session.Project.ActiveShot);
                        _activeMediaClip = session.Project.TcMediaComposition.MediaClips.LastOrDefault();
                    }
                    else
                    {
                        _activeMediaClip.CreateTake(_activeMediaClip.ActiveTake.Shot);   
                    }
                    var videoFragment = _activeMediaClip.ActiveTake.VideoFragments.LastOrDefault();
                    if (videoFragment != null)
                    {
                        var time = TimeSpan.Zero;
                        videoFragment.UpdateStartTime(time);
                        LogQueue.WriteToFile($"{videoFragment.Id} UpdateStartTime 3: {time} TimeRange: {videoFragment.TimeRange.Start}, {videoFragment.TimeRange.Duration}");
                    }
                    session.ShotManager.ActiveShotView.StartRendering();
                    await mixer.StartRecordingAsync();
                    SubscribeToShotManager();
                }
            }
        }

        public async Task StopRecording(bool isNeedStartPreview = true)
        {
            LogQueue.WriteToFile("RecordingManager StopRecording");
            Messenger.Default.Send(new BusyIndicatorShowMessage());
            AuthoringSession?.ShotManager.BlockShotChanging();
            await StopRecordingWithoutUI();
            if (isNeedStartPreview)
            {
                await StartPreview();
            }

            UnsubscribeFromShotManager();
            
            _activeMediaClip = null;
            AuthoringSession?.ShotManager.UnBlockShotChanging();
            Messenger.Default.Send(new BusyIndicatorHideMessage());
        }

        public async Task ProcessWindowVisibilityChanging(bool isVisible)
        {
            await _lock.WaitAsync();
            var mediaMixer = MediaMixer;

            if (mediaMixer != null)
            {
                if (isVisible)
                {
                    var session = AuthoringSession;
                    if (session != null)
                    {
                        await session.InitializeCamera();
                        await session.UpdateScreenshotsForNewRecordingDevice();
                    }
                }
                else
                {

                    await HardStopRecordingWithoutUI();
                    if (mediaMixer.State == TouchcastComposerServiceState.Previewing 
                        || mediaMixer.State == TouchcastComposerServiceState.Completed)
                    {
                        await mediaMixer.Reset();
                    }
                    UnsubscribeFromShotManager();
                    _activeMediaClip = null;
                }
            }
            _lock.Release();
        }
        #endregion

        #region Callbacks
        private async Task ShotManager_ShotWillBeHidden(Shots.Shot model)
        {
            var mediaMixer = MediaMixer;
            if (mediaMixer != null && mediaMixer.State == TouchcastComposerServiceState.Recording)
            {
                var authoringSession = AuthoringSession;
                if (authoringSession != null)
                {
                    authoringSession.ShotManager.ShotViews.FirstOrDefault(x => x.Shot.Id == model.Id)?.StopRendering();
                    //await authoringSession.CreateNewRecordingForActiveShot();
                }
            }
        }
        private async Task ShotManager_ShotWasDisplayed(Shots.Shot model)
        {
            var authoringSession = AuthoringSession;
            var mediaMixer = MediaMixer;
            if (mediaMixer != null && authoringSession != null)
            {
                var activeShotView = authoringSession.ShotManager.ShotViews.FirstOrDefault(v => v.Shot == model);
                await activeShotView.UpdateLayerViews();
                activeShotView.StartRendering();
            }
        }
        #endregion

        #region Private methods
        private void CreateNewTakeIfNeeded()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                if (_activeMediaClip != null)
                {
                    var time = TouchCastComposingEngine.ComposingContext.Shared().Time;
                    var videoFragment = _activeMediaClip.ActiveTake?.VideoFragments.LastOrDefault();
                    if (videoFragment != null)
                    {
                        videoFragment.UpdateEndTime(time);
                        LogQueue.WriteToFile($"{videoFragment.Id} UpdateEndTime 3: {time} TimeRange: {videoFragment.TimeRange.Start}, {videoFragment.TimeRange.Duration}");
                    }
                    var take = _activeMediaClip.CreateTake(authoringSession.Project.ActiveShot);
                    videoFragment = take.VideoFragments.LastOrDefault();
                    if (videoFragment != null)
                    {
                        videoFragment.UpdateStartTime(time);
                        LogQueue.WriteToFile($"{videoFragment.Id} UpdateStartTime 4: {time} TimeRange: {videoFragment.TimeRange.Start}, {videoFragment.TimeRange.Duration}");
                    }
                }
            }
        }

        private async Task StopRecordingWithoutUI()
        {
            var mixer = MediaMixer;
            var session = AuthoringSession;
            if (mixer != null)
            {
                await mixer.StopRecordingAsync();
                if (session != null)
                {
                    session.ShotManager.ActiveShotView.StopRendering();
                    await session.CreateNewRecordings(_activeMediaClip);
                }
            }
        }

        private async Task HardStopRecordingWithoutUI()
        {
            var mixer = MediaMixer;
            var session = AuthoringSession;
            if (mixer != null)
            {
                await mixer.HardStopRecordingAsync();
                if (session != null)
                {
                    await session.CreateNewRecordingForActiveShot();
                }
            }
        }

        public async Task StartPreview()
        {
            var mixer = MediaMixer;
            if (mixer != null)
            {
                await mixer.StartPreviewingAsync();
            }
            var session = AuthoringSession;
            if(session != null)
            {
                Dictionary<string, List<TouchCastComposingEngine.ComposingContextElement>> dictionary = new Dictionary<string, List<TouchCastComposingEngine.ComposingContextElement>>();
                var shotObjectsToUpdate = new List<TouchCastComposingEngine.ComposingContextElement>();
                foreach (var shot in session.Project.Shots)
                {
                    foreach (var layer in shot.Layers)
                    {
                        if (layer is Layouts.VideoLayerLayout videoLayer)
                        {
                            await videoLayer.AddVideoVappBitmap();
                        }

                        if (layer.ComposingContextElement.Bitmap != null
                            && layer.ComposingContextElement.Bitmap is Microsoft.Graphics.Canvas.CanvasBitmap bmp)
                        {
                            shotObjectsToUpdate.Add(layer.ComposingContextElement);
                        }
                    }
                }

                if (shotObjectsToUpdate.Any())
                {
                    TouchCastComposingEngine.ComposingContext.Shared().UpdateComposingItems(shotObjectsToUpdate);
                }

                session.ShotManager.ActiveShotView.PrepareToRecordingNewActionStream();
            }
        }

        private void SubscribeToShotManager()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ShotWillBeHidden += ShotManager_ShotWillBeHidden;
                authoringSession.ShotManager.ShotWasDisplayed += ShotManager_ShotWasDisplayed;
            }
        }
        private void UnsubscribeFromShotManager()
        {
            var authoringSession = AuthoringSession;
            if (authoringSession != null)
            {
                authoringSession.ShotManager.ShotWillBeHidden -= ShotManager_ShotWillBeHidden;
                authoringSession.ShotManager.ShotWasDisplayed -= ShotManager_ShotWasDisplayed;
            }
        }
        #endregion
    }
}
