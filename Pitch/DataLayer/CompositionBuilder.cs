﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pitch.Models;
using Windows.Media.Core;
using Windows.Media.Editing;

namespace Pitch.DataLayer
{
    public class CompositionBuilder
    {
        #region Private fields

        private Project _project;
        private MediaComposition _mediaComposition;
        private MediaOverlayLayer _overlayLayer;
        private const double TimeDelta = 0.005;
        private object _lockObject = new object();
        private readonly TimeSpan _transitionDuration = TimeSpan.FromSeconds(0.5);
        private readonly Windows.Foundation.Rect _transitionPosition = new Windows.Foundation.Rect(0, 0, Services.MediaMixerService.VideoSize.Width, Services.MediaMixerService.VideoSize.Height);
        #endregion

        #region Life cycle

        public CompositionBuilder(ref Project project)
        {
            _project = project;
            ActionsStream = new ActionsStream();
            _mediaComposition = new MediaComposition();
            _overlayLayer = new MediaOverlayLayer(new Windows.Media.Effects.VideoCompositorDefinition("TouchCastComposingEngine.TransitionVideoCompositor"));
        }

        #endregion

        #region Public Interface

        #region Properties

        public MediaStreamSource MediaStreamSource => _mediaComposition.GenerateMediaStreamSource();
        public Project Project
        {
            get => _project;
            set => _project = value;
        }

        public MediaComposition MediaComposition => _mediaComposition;

        public ActionsStream ActionsStream { get; set; }

        #endregion

        public void Refresh()
        {
            lock (_lockObject)
            {
                _mediaComposition.Clips.Clear();
                _mediaComposition.OverlayLayers.Clear();
                _overlayLayer.Overlays.Clear();

                for (int i = 0; i < _project.TcMediaComposition.MediaClips.Count; i++)
                {
                    var currentMediaClip = _project.TcMediaComposition.MediaClips[i];
                    if (currentMediaClip.ActiveTake != null)
                    {
                        for (int j = 0; j < currentMediaClip.ActiveTake.VideoFragments.Count; j++)
                        {
                            _mediaComposition.Clips.Add(currentMediaClip.ActiveTake.VideoFragments[j].MediaClip);

                            if (j == 0)
                            {
                                for (int a = i - 1; a >= 0; a--)
                                {
                                    if (_project.TcMediaComposition.MediaClips[a].ActiveTake != null && _project.TcMediaComposition.MediaClips[a].ActiveTake.VideoFragments.Count > 0)
                                    {
                                        PrepareTransitionResult prepareResult = null;
                                        var transition = _project.TcMediaComposition.MediaClips[a].Transition;
                                        switch (transition.Kind)
                                        {
                                            case Transition.Type.Crossfade:
                                                prepareResult = transition.PrepareCrossFadeTranstion(_project.TcMediaComposition.MediaClips[a].ActiveTake.VideoFragments.Last(), currentMediaClip.ActiveTake.VideoFragments[j]);
                                                break;
                                            case Transition.Type.FadeThroughBlack:
                                                prepareResult = transition.PrepareFadeThroughColorTransition(_project.TcMediaComposition.MediaClips[a].ActiveTake.VideoFragments.Last(), currentMediaClip.ActiveTake.VideoFragments[j], Windows.UI.Colors.Black);
                                                break;
                                            case Transition.Type.FadeThroughWhite:
                                                prepareResult = transition.PrepareFadeThroughColorTransition(_project.TcMediaComposition.MediaClips[a].ActiveTake.VideoFragments.Last(), currentMediaClip.ActiveTake.VideoFragments[j], Windows.UI.Colors.White);
                                                break;
                                        }
                                        if (prepareResult != null)
                                        {
                                            _mediaComposition.Clips.Insert(_mediaComposition.Clips.Count - 1, prepareResult.MediaClip);
                                            foreach (var overlay in prepareResult.Overlays)
                                            {
                                                _overlayLayer.Overlays.Add(overlay);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
                _mediaComposition.OverlayLayers.Add(_overlayLayer);

                PrepareActionStream();
            }
        }

        public void Clean()
        {
            if (_mediaComposition != null)
            {
                _mediaComposition.Clips.Clear();
                _mediaComposition.BackgroundAudioTracks.Clear();
                _mediaComposition.OverlayLayers.Clear();
                _mediaComposition = null;
            }

            ActionsStream?.Clear();
            _overlayLayer?.Overlays?.Clear();
            _overlayLayer = null;
        }

        #endregion

        #region Private

        //private void ProcessTransitions()
        //{
        //    _overlayLayer.Overlays.Clear();
        //    _mediaComposition.OverlayLayers.Clear();
        //    for (int i = 0; i < _mediaComposition.Clips.Count; i++)
        //    {
        //        if(i > 0)
        //        {
        //            var previousClip = _mediaComposition.Clips[i - 1];
        //            var currentClip = _mediaComposition.Clips[i];
        //            /* CreateCrossFadeTranstion(previousClip, currentClip.Clone());*/
        //            CreateFadeInTransition(previousClip, Windows.UI.Colors.Black);
        //            CreateFadeOutTransition(currentClip, Windows.UI.Colors.Black);
        //        }
        //    }
        //    _mediaComposition.OverlayLayers.Add(_overlayLayer);
        //}


        //private void CreateCrossFadeTranstion(MediaClip previousMediaClip, MediaClip currentMediaClipCopy)
        //{

        //    var delay = currentMediaClipCopy.TrimmedDuration - _transitionDuration;
        //    currentMediaClipCopy.TrimTimeFromEnd = delay;
        //    var videoOverlay = new MediaOverlay(currentMediaClipCopy);
        //    videoOverlay.Position = _transitionPosition;
        //    videoOverlay.Delay = previousMediaClip.EndTimeInComposition - _transitionDuration;
        //    videoOverlay.Opacity = 0.0;
        //    _overlayLayer.Overlays.Add(videoOverlay);
        //}

        //private void CreateFadeInTransition(MediaClip previousMediaClip, Windows.UI.Color color)
        //{
        //    MediaClip clip = MediaClip.CreateFromColor(color, _transitionDuration);
        //    var delay = previousMediaClip.EndTimeInComposition - _transitionDuration;
        //    var videoOverlay = new MediaOverlay(clip);
        //    videoOverlay.Position = _transitionPosition;
        //    videoOverlay.Delay = delay;
        //    videoOverlay.Opacity = 0.0;
        //    _overlayLayer.Overlays.Add(videoOverlay);
        //}
        //private void CreateFadeOutTransition(MediaClip curentClip ,Windows.UI.Color color)
        //{
        //    MediaClip clip = MediaClip.CreateFromColor(color, _transitionDuration);
        //    var delay = curentClip.StartTimeInComposition;
        //    var videoOverlay = new MediaOverlay(clip);
        //    videoOverlay.Opacity = 1.0;
        //    videoOverlay.Position = _transitionPosition;
        //    videoOverlay.Delay = delay;
        //    _overlayLayer.Overlays.Add(videoOverlay);
        //}

        private void PrepareActionStream()
        {
            var temporaryActionStream = new List<Command>();

            int idCounter = 1;
            foreach (var mediaClip in _project.TcMediaComposition.MediaClips)
            {
                var take = mediaClip.ActiveTake;
                if (take == null) continue;
                //foreach (var take in shot.Takes)
                {
                    foreach (var videoFragment in take.VideoFragments)
                    {
                        var clipStartTimeInSeconds = videoFragment.TrimTimeRange.Start.TotalSeconds;
                        var clipEndTimeFromSeconds = (videoFragment.TrimTimeRange.Start + videoFragment.TrimTimeRange.Duration).TotalSeconds;
                        var bringToFrontTime = clipStartTimeInSeconds + 0.005;
                        var groupedCommandsToUse = videoFragment.Recording.ActionsStream.Commands.Select(x => x.Clone())
                            //.Where(x => x.Time >= clipStartTimeInSeconds && x.Time <= clipEndTimeFromSeconds)
                            .GroupBy(c => c.Id).ToList();

                        foreach (var groupCommands in groupedCommandsToUse)
                        {
                            var createCommand = groupCommands.FirstOrDefault(x => x.Type == CommandTypes.Create);
                            var closeCommand = groupCommands.FirstOrDefault(x => x.Type == CommandTypes.Close);
                            
                            var commandsInRange = groupCommands.Where(x => x.Time >= clipStartTimeInSeconds && x.Time <= clipEndTimeFromSeconds).OrderBy(g => g.Time).ToList();
                            var nearestCommand = groupCommands.Where(x => x.Time <=clipStartTimeInSeconds).OrderByDescending(g => g.Time).FirstOrDefault();
                            var navigateCommand = groupCommands.Where(x => x.Type == CommandTypes.Navigate && x.Time < clipStartTimeInSeconds).OrderByDescending(c => c.Time).FirstOrDefault();
                            if (commandsInRange.Any()
                                || (createCommand != null && clipStartTimeInSeconds > createCommand.Time
                                    && closeCommand != null && clipEndTimeFromSeconds < closeCommand.Time))
                            {
                                if (commandsInRange.FirstOrDefault(x => x.Type == CommandTypes.Create) == null)
                                {
                                    var command = nearestCommand ?? createCommand;
                                    
                                    if (command != null)
                                    {
                                        command = command.Clone();
                                        if (navigateCommand != null && navigateCommand.Time < clipStartTimeInSeconds)
                                        {
                                            command.Url = navigateCommand.Url;
                                        }
                                        command.Time = clipStartTimeInSeconds;
                                        command.Type = CommandTypes.Create;
                                        commandsInRange.Add(command);
                                    }
                                }

                                if (commandsInRange.FirstOrDefault(x => x.Type == CommandTypes.BringToFront) == null)
                                {
                                    var command = nearestCommand ?? createCommand;
                                    
                                    if (command != null)
                                    {
                                        command = command.Clone();
                                        if (navigateCommand != null && navigateCommand.Time < clipStartTimeInSeconds)
                                        {
                                            command.Url = navigateCommand.Url;
                                        }
                                        command.Time = clipStartTimeInSeconds;
                                        command.Type = CommandTypes.BringToFront;
                                        commandsInRange.Add(command);
                                    }
                                }

                                if (commandsInRange.FirstOrDefault(x => x.Type == CommandTypes.Close) == null)
                                {
                                    var command = commandsInRange.OrderByDescending(x => x.Time).FirstOrDefault() ?? closeCommand;
                                    
                                    if (command != null)
                                    {
                                        command = command.Clone();
                                        command.Time = clipEndTimeFromSeconds;
                                        command.Type = CommandTypes.Close;
                                        commandsInRange.Add(command);
                                    }
                                }

                                foreach (var command in commandsInRange)
                                {
                                    command.Time = command.Time + videoFragment.MediaClip.StartTimeInComposition.TotalSeconds - videoFragment.TrimTimeRange.Start.TotalSeconds;
                                    //command.Time += videoFragment.MediaClip.StartTimeInComposition.TotalSeconds - clipStartTimeInSeconds;
                                    command.Id = idCounter;
                                    command.ClipId = videoFragment.Id;
                                    temporaryActionStream.Add(command);
                                }
                                idCounter++;
                            }
                        }
                    }
                }
            }

            temporaryActionStream = (from command in temporaryActionStream orderby command.Time, command.Index descending select command).ToList();

            //Uncomment if need Remove marks wich contains same vaps

            //for (int i = 1; i < temporaryActionStream.Count; i++)
            //{
            //    var previousCommand = temporaryActionStream[i - 1];
            //    var currentCommand = temporaryActionStream[i];
            //    if (previousCommand.Type == CommandTypes.Close
            //        && currentCommand.Type == CommandTypes.Create
            //        && currentCommand.Time - previousCommand.Time < 0.1)
            //    {
            //        if (previousCommand.ResourceId == currentCommand.ResourceId)
            //        {
            //            var commands = temporaryActionStream.Where(x => x.Id == currentCommand.Id).ToList();
            //            foreach (var command in commands)
            //            {
            //                command.Id = previousCommand.Id;
            //            }

            //            temporaryActionStream.Remove(previousCommand);
            //            temporaryActionStream.Remove(currentCommand);
            //        }
            //    }
            //}

            //var resources = temporaryActionStream.Select(x => x.ResourceId).Distinct();

            //_resources.Clear();

            //foreach (var clip in _project.Shots)
            //{
            //    _resources.InsertRange(_resources.Count, clip.ActiveTake.Recording.Resources.Where(x => resources.Contains(x.ID)));
            //}

            ActionsStream.Commands = temporaryActionStream;

            // Events
            var tempEvents = new List<Event>();
            foreach (var mediaClip in _project.TcMediaComposition.MediaClips)
            {
                var take = mediaClip.ActiveTake;
                if (take == null) continue;
                foreach (var videoFragment in take.VideoFragments)
                {
                    if (videoFragment?.MediaClip != null)
                    {
                        var events = videoFragment.Recording.ActionsStream.Events.Select(x => x.Clone()).ToList();
                        for (int i = 0; i < events.Count; i++)
                        {
                            double startTimeInClip = events[i].Time;
                            double endtimeInClip = ((i + 1) < events.Count ? events[i + 1].Time : videoFragment.MediaClip.OriginalDuration.TotalSeconds);
                            var clipEndTime = videoFragment.MediaClip.TrimTimeFromStart.TotalSeconds + videoFragment.MediaClip.TrimmedDuration.TotalSeconds;
                            var clipStartTime = videoFragment.MediaClip.TrimTimeFromStart.TotalSeconds;

                            if (
                                    (startTimeInClip < clipStartTime && endtimeInClip < clipEndTime && clipStartTime < endtimeInClip)
                                || (startTimeInClip > clipStartTime && endtimeInClip > clipEndTime && startTimeInClip < clipEndTime)
                                || (startTimeInClip > clipStartTime && endtimeInClip < clipEndTime)
                                || (startTimeInClip < clipStartTime && endtimeInClip > clipEndTime)
                              )
                            {
                                //System.Diagnostics.Debug.WriteLine($"Time : {startTimeInClip}");
                                var time = videoFragment.MediaClip.StartTimeInComposition.TotalSeconds + startTimeInClip - videoFragment.MediaClip.TrimTimeFromStart.TotalSeconds;
                                if (time < 0)
                                    time = 0;
                                //System.Diagnostics.Debug.WriteLine($"Start Time = {time}");
                                events[i].Time = time;
                                tempEvents.Add(events[i]);
                            }
                        }
                    }
                }
            }
            ActionsStream.Events = tempEvents.OrderBy(e => e.Time).ToList();
        }
        #endregion
    }
}
