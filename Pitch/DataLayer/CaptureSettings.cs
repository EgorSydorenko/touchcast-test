﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pitch.DataLayer
{
    public class CaptureSettings
    {
        public CameraSettings CameraSettings { set; get; }
        public MicrophoneSettings MicrophoneSettings { set; get; }
        public CaptureSettings()
        {
            CameraSettings = new CameraSettings();
            MicrophoneSettings = new MicrophoneSettings();
        }
    }

    public class CameraSettings
    {
        public string Name { set; get; }
    }

    public class MicrophoneSettings
    {
        public string Name { set; get; }
        public double Volume { set; get; }
    }
}
