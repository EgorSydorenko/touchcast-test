﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Pitch.Services;
using Pitch.Helpers.Logger;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Pitch.Helpers;

namespace Pitch.DataLayer
{
    public enum ApplicationState
    {
        None,
        Preparation,
        Editing,
        Recording,
        Uploading
    }

    public class AuthoringSession
    {
        public delegate void AuthoringSessionEvent();
        public delegate Task AuthoringSessionProjectEvent();
        private Project _project;
        private IMediaMixerService _mediaMixerService;
        private ShotManager _shotManager;
        private CompositionBuilder _buider;

        public AuthoringSession()
        {
            if (_mediaMixerService == null)
            {
                _mediaMixerService = SimpleIoc.Default.GetInstance<IMediaMixerService>();            
            }
        }

        public async Task Init(Grid view)
        {
            LogQueue.WriteToFile("AuthoringSession Init");
            _shotManager.ShotContainerView = view;
            await _shotManager.Initialize();

            if (AuthroingSessionWasInitialized != null)
            {
                await AuthroingSessionWasInitialized.Invoke();
            }
        }

#if DEBUG
        ~AuthoringSession()
        {
            System.Diagnostics.Debug.WriteLine("***************** AuthoringSession Destructor ***************");
        }
#endif
        public void Reset()
        {
            LogQueue.WriteToFile("AuthoringSession Reset");
            if (_mediaMixerService != null)
            {
                _mediaMixerService = null;
            }
            //TouchCastComposingEngine.ComposingContext.Shared().ResetContext();
            if (_project != null)
            {
                _project.UndoRedoOriginator.Clean();
            }
        }

        public async Task Clean()
        {
            LogQueue.WriteToFile("AuthoringSession Clean");
            if (AuthoringSessionWillBeCleaned != null)
            {
                await AuthoringSessionWillBeCleaned.Invoke();
            }

            _buider?.Clean();

            if (_project != null)
            {
                await _project.Clean();
                _project.ActiveShotWillBeChanged -= Project_ActiveShotWillBeChanged;
            }
        }
        
        public async Task OpenNew(string pathToFolder)
        {
            LogQueue.WriteToFile("AuthoringSession OpenNew");
            Project?.StopAutoSaveThread();
            await Clean();
            _shotManager?.Clean();
            await InitializeCamera();
            Project = await Project.OpenNewProject(pathToFolder);
            _shotManager = new ShotManager(_project);
            _buider = new CompositionBuilder(ref _project);
            Project.StartAutoSaveThread();
            AuthoringSeesionDidOpenProject?.Invoke();
        }

        public void UpdateGreenScreenSettingsForActiveShot()
        {
            UpdateGreenscreenSettings(_project.ActiveShot.GreenScreenSettings, false);
        }
        private void Project_ActiveShotWasChanged()
        {
            UpdateGreenscreenSettings(_project.ActiveShot.GreenScreenSettings, true);
            _project.ActiveShot.GreenScreenSettings.GreenScreenSettingsWasChanged += GreeenScreenSettings_GreenScreenSettingsWasChanged;
        }

        private  void GreeenScreenSettings_GreenScreenSettingsWasChanged(GreenScreenSettings settings)
        {
             UpdateGreenscreenSettings(settings, false);
        }

        private void UpdateGreenscreenSettings(GreenScreenSettings settings, bool isShotChanged)
        {
            LogQueue.WriteToFile("AuthoringSession UpdateGreenscreenSettings");
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.State = settings.IsOn ?
            TouchCastComposingEngine.ChromaKeyState.ChromaKeyEnabled : TouchCastComposingEngine.ChromaKeyState.ChromaKeyDisabled;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.Smoothness = (float)settings.Smoothness;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.Tolerance = (float)settings.Sensetivity;
            TouchCastComposingEngine.ComposingContext.Shared().ChromaKeyEffect.ChromaKeyColor = settings.Color;
        }

        private void Project_ActiveShotWillBeChanged()
        {
            _project.ActiveShot.GreenScreenSettings.GreenScreenSettingsWasChanged -= GreeenScreenSettings_GreenScreenSettingsWasChanged;
        }

        public async Task Open(StorageFile file)
        {
            LogQueue.WriteToFile("AuthoringSession Open file");
            if (AuthoringSeesionWillOpenProject != null && _project != null)
            {
                await AuthoringSeesionWillOpenProject.Invoke();
            }

            await Clean();
            _shotManager?.Clean();
            await InitializeCamera();
            Project?.StopAutoSaveThread();

            var project = (file.FileType == FileToAppHelper.TouchcastThemeExtension) ? 
                await Project.OpenNewProjectWithTcpx(file) : 
                await Project.OpenFromFile(file, ThemeManager.Instance.DefaultTheme.theme_foler_path);

            Project = project;

            _buider = new CompositionBuilder(ref _project);
            
            _shotManager = new ShotManager(project);
            Project?.StartAutoSaveThread();
            if (AuthoringSeesionDidOpenProject != null)
            {
               await AuthoringSeesionDidOpenProject.Invoke();
            }

            foreach (var shot in project.Shots)
            {
                shot.Subscribe();
            }
            await _shotManager.ShowShotWithoutUndoRedo(project.Shots.FirstOrDefault());
        }

        public async Task Open(Project project)
        {
            LogQueue.WriteToFile("AuthoringSession Open project");
            await InitializeCamera();
            Project?.StopAutoSaveThread();
            Project = project;
            Project?.StartAutoSaveThread();
        }

        public Project Project
        {
            private set
            {
                _project = value;
                _project.ActiveShotWillBeChanged += Project_ActiveShotWillBeChanged;
                _project.ActiveShotWasChanged += Project_ActiveShotWasChanged;
                _project.ActiveShot.GreenScreenSettings.GreenScreenSettingsWasChanged += GreeenScreenSettings_GreenScreenSettingsWasChanged;
            }
            get => _project;
        }

        public ShotManager ShotManager =>  _shotManager;

        public CompositionBuilder CompositionBuilder => _buider;

        public async Task CreateNewRecordings(TcMediaClip mediaClip)
        {
            LogQueue.WriteToFile("AuthoringSession CreateNewRecording");

            if (mediaClip == null)
            {
                return;
            }

            if (mediaClip.ActiveTake != null)
            {
                var videoFragment = mediaClip.ActiveTake.VideoFragments.LastOrDefault();
                if (videoFragment != null && videoFragment.State == VideoFragmentState.Created)
                {
                    var time = _mediaMixerService.VideoDuration;
                    videoFragment.UpdateEndTime(time);
                    LogQueue.WriteToFile($"{videoFragment.Id} UpdateEndTime 1: {time} TimeRange: {videoFragment.TimeRange.Start}, {videoFragment.TimeRange.Duration}");

                    var recording = await CreateRecording();

                    //Update Active Video Fragments
                    foreach (var mClip in _project.TcMediaComposition.MediaClips)
                    {
                        foreach (var take in mClip.Takes)
                        {
                            var createdVideoFragments = take.VideoFragments.Where(x => x.State == VideoFragmentState.Created).ToList();
                            foreach (var fragment in createdVideoFragments)
                            {
                                await fragment.LoadMedia(recording);
                            }
                        }
                    }
                }
            }
        }

        public async Task CreateNewRecordingForActiveShot()
        {
            var lastMediaClip = _project.TcMediaComposition.MediaClips.LastOrDefault();
            if (lastMediaClip?.ActiveTake != null)
            {
               var activeTakeLastVideoFragment = lastMediaClip.ActiveTake.VideoFragments.LastOrDefault();
                if(activeTakeLastVideoFragment != null)
                {
                    var time = _mediaMixerService.VideoDuration;
                    activeTakeLastVideoFragment.UpdateEndTime(time);
                    LogQueue.WriteToFile($"{activeTakeLastVideoFragment.Id} UpdateEndTime 1: {time} TimeRange: {activeTakeLastVideoFragment.TimeRange.Start}, {activeTakeLastVideoFragment.TimeRange.Duration}");
                }

                var recording = await CreateRecording();
                foreach (var item in lastMediaClip.Takes)
                {
                    var lastVideoFragment = item.VideoFragments.LastOrDefault();
                    if(lastVideoFragment != null &&  lastVideoFragment.State == VideoFragmentState.Created)
                    {
                        await lastVideoFragment.LoadMedia(recording);
                    }
                }
            }
        }

        public Exporter Export()
        {
            return new Exporter(_buider);
        }

        public async Task UpdateScreenshotsForNewRecordingDevice()
        {
            if (_project != null)
            {
                foreach (var shot in _project.Shots)
                {
                    await shot.PrepareForRecording();
                
                }
                _shotManager.ActiveShotView.PrepareToRecordingNewActionStream();
            }
        }

        #region Events
        
        #pragma warning disable CS0067
        public event AuthoringSessionEvent AuthoringSessionWillBeReseted;
        public event AuthoringSessionEvent AuthoringSessionWasReseted;

        public event AuthoringSessionEvent AuthoringSessionWillBeRefreshed;
        public event AuthoringSessionEvent AuthoringSessionWasRefreshed;

        public event AuthoringSessionProjectEvent AuthoringSeesionWillOpenProject;
        public event AuthoringSessionProjectEvent AuthoringSeesionDidOpenProject;

        public event AuthoringSessionEvent AuthroingSessionWillAddNewRecording;
        public event AuthoringSessionEvent AuthroingSessionWasNewRecording;

        public event AuthoringSessionProjectEvent AuthroingSessionWasInitialized;

        public event AuthoringSessionProjectEvent AuthoringSessionWillBeCleaned;
        public event AuthoringSessionEvent AuthoringSessionWasCleaned;
        #pragma warning restore CS0067
        #endregion

        #region Private 

        private async Task<Recording> CreateRecording()
        {
            #region Create ActionStream 
            await _shotManager.ActiveShotView.PrepareShotBeforeHiding();
            var composer = SimpleIoc.Default.GetInstance<ComposingManager>();
            composer.TouchcastRecorder.Finish(_mediaMixerService.VideoDuration.TotalSeconds);
            var actionsXml = await _project.WorkingFolder.CreateFileAsync($"{Guid.NewGuid()}.xml");
            await FileIO.WriteTextAsync(actionsXml, composer.TouchcastRecorder.TouchcastUploadData.ActionsXmlToString());
            var actionsStream = composer.TouchcastRecorder.TouchcastUploadData.ActionsStream;
            composer.TouchcastRecorder.ResetRecorder();
            #endregion

            var recording = new Recording(_mediaMixerService.VideoFile, actionsXml, _mediaMixerService.VideoDuration, actionsStream);
            recording.Id = Guid.NewGuid().ToString();
            await recording.CheckUsedResources();
            Pitch.Helpers.Analytics.GoogleAnalyticsManager.TrySend(Pitch.Helpers.Analytics.AnalyticCommandType.AnalyticCommandTouchcastRecorded);
            return recording;
        }

        public async Task InitializeCamera()
        {
            if (_mediaMixerService.State == TouchcastComposerServiceState.Idle)
            {
                _mediaMixerService.InitWithCamera();
                if (_mediaMixerService.State == TouchcastComposerServiceState.ComposingModeDefined)
                {
                    await _mediaMixerService.StartPreviewingAsync();
                }
            }
        }

        #endregion
    }
}
