﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Pitch.Helpers.Logger;
using Windows.Web.Http;

namespace Pitch.DataLayer
{
    public class PartnerData
    {
        public string config { set; get; }
        public string endpoint { set; get; }
        public string hash { set; get; }
        public string partner_name { set; get; }
    }

    public class PartnersManager
    {
        static public async Task<Dictionary<string, string>> CheckPartnerId(string partnerId)
        {
            //partnerId = "6647a1a666";
            var url = "https://api-enterprise.touchcast.com/verify_partner_id?apikey=123&client=ipad";
            Dictionary<string, string> responseDictionary = null;
            using (var httpClient = new HttpClient())
            {
                var httpResponseMessage = await httpClient.GetAsync(new Uri($"{url}&partner_id={partnerId}")).AsTask();
                string responseString = await httpResponseMessage.Content.ReadAsStringAsync();
                try
                {
                    responseDictionary = DeSerializeResponse(responseString);
                }
                catch(Exception ex)
                {
                   LogQueue.WriteToFile($"Exception in PartnersManager.CheckPartnerId : {ex.Message}");
                } 
            }
            return responseDictionary;
        }

        static private Dictionary<string,string> DeSerializeResponse(string responseString)
        {
            XDocument doc = XDocument.Parse(responseString);
            Dictionary<string, string> dataDictionary = new Dictionary<string, string>();

            foreach (XElement element in doc.Descendants().Where(p => p.HasElements == false))
            {
                int keyInt = 0;
                string keyName = element.Name.LocalName;

                while (dataDictionary.ContainsKey(keyName))
                {
                    keyName = element.Name.LocalName + "_" + keyInt++;
                }

                dataDictionary.Add(keyName, element.Value);
            }
            return dataDictionary;
        }
    }
}
