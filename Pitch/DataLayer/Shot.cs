﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Pitch.DataLayer.Layouts;
using Pitch.DataLayer.UndoRedo;
using Pitch.Models;
using Pitch.Helpers;
using Pitch.Helpers.Logger;
using TouchCastComposingEngine;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI;
using Windows.Foundation;
using Pitch.DataLayer.Helpers.Extensions;

namespace Pitch.DataLayer.Shots
{
    public enum ZIndexPosition
    {
        Previous = -1,
        Next = 1,
        Top,
        Bottom
    }

    public class IndexRange
    {
        public int MinIndex { get; private set; }
        public int MaxIndex { get; private set; }
        public IndexRange(int minIndex, int maxIndex)
        {
            MinIndex = minIndex;
            MaxIndex = maxIndex;
        }
    }

    public class ShotMementoModel : ICleanable
    {
        private Shot _shot;
        private int _index;

        public Shot Shot { set { _shot = value; } get { return _shot; } }
        public int Index { set { _index = value; } get { return _index; } }

        public ShotMementoModel()
        {
        }
        public void Clean() { }
    }
    public class ShotChangedMementoModel : ICleanable
    {
        private string _shotId;
        private Visibility _visibility;
        
        public string ShotId{set { _shotId = value; } get { return _shotId; }}
        public Visibility Visibility { set { _visibility = value; } get { return _visibility; } }

        public void Clean() { }
    }

    public class ReplaceLayersMementoModel : ICleanable
    {
        private string _shotId;
        private LayerLayout _oldLayer;
        private LayerLayout _newLayer;

        public string ShotId => _shotId;
        public LayerLayout OldLayer => _oldLayer;
        public LayerLayout NewLayer => _newLayer;
        public ReplaceLayersMementoModel(Shot shot, LayerLayout oldLayer, LayerLayout newLayer)
        {
            _shotId = shot.Id;
            _oldLayer = oldLayer;
            _newLayer = newLayer;
        }
        public void Clean() { }
    }

    internal class BackgroundVappState
    {
        private Rect _position;
        private int _zIndex;
        public Rect Position => _position;
        public int ZIndex => _zIndex;
        public BackgroundVappState(LayerLayout layer)
        {
            if (layer != null)
            {
                _position = layer.Position;
                _zIndex = layer.ZIndex;
            }
        }
    }

    internal class SetAsBackgroundMementoModel : ICleanable
    {
        private string _shotId;
        private LayerLayout _currentBackgroundLayer;
        private LayerLayout _newBackgroundLayer;
        private Color _currentBackgroundColor;
        private Color _newBackgroundColor;
        private BackgroundVappState _backgroundVappState;
        private bool _isColorChanged;
        public string ShotId => _shotId;
        public LayerLayout CurrentBackgroundLayer => _currentBackgroundLayer;
        public LayerLayout NewBackgroundLayer => _newBackgroundLayer;
        public Color CurrentBackgroundColor => _currentBackgroundColor;
        public Color NewBackgroundColor => _newBackgroundColor;
        public BackgroundVappState BackgroundVappState => _backgroundVappState;
        public bool IsColorChanged => _isColorChanged;

        public SetAsBackgroundMementoModel(Shot shot, LayerLayout currentLayer, LayerLayout newLayer)
        {
            _shotId = shot.Id;
            _currentBackgroundLayer = currentLayer;
            _newBackgroundLayer = newLayer;
            _backgroundVappState = new BackgroundVappState((currentLayer == null) ? newLayer : currentLayer);
            _isColorChanged = false;
        }

        public SetAsBackgroundMementoModel(Shot shot, Color currentColor, Color newColor, LayerLayout currentBackgroundLayer)
        {
            _shotId = shot.Id;
            _currentBackgroundColor = currentColor;
            _newBackgroundColor = newColor;
            _currentBackgroundLayer = currentBackgroundLayer;
            _isColorChanged = true;
        }

        public void Clean() { }
    }

    internal class ShotTeleprompterChangedMementoModel : ICleanable
    {
        private Shot _shot;
        private string _teleprompter;

        public Shot Shot { get { return _shot; } set { _shot = value; } }
        public string TeleprompterText { get { return _teleprompter; } set { _teleprompter = value; } }
        public void Clean() { }
    }

    public class Shot
    {
        #region Private Fields
        private const double NewVappShiftXPos = 15;
        private const double NewVappShiftYPos = 15;

        private string _id;
        private bool _isNeedFastThumbnailUpdating = true;
        private Color _backgroundColor = Colors.Black;
        private ComposingContextElement _strokesComposingElement;
        #endregion

        #region Protected Fields
        protected int _zOrderingCounter;
        protected List<LayerLayout> _layers;
        protected List<LayerLayoutTemplate> _placeHolderTempaltes;
        protected GreenScreenSettings _greenScreenSettings;
        protected WeakReference<Project> _project;
        protected string _teleprompterText;
        protected CaptureSettings _captureSettings;
        #endregion

        #region Events

        public delegate Task LayoutEvent(LayerLayout model);
        public delegate Task ReplaceLayoutEvent(LayerLayout oldModel, LayerLayout newModel);
        public delegate void TeleprompterTextEvent();
        public delegate void ShotPreviewUpdated(object sender);
        public delegate void UpdateShotView();
        public delegate void UpdateBackgroundLayer(LayerLayout newBackgroundLayer, LayerLayout currentBackgroundLayer);
        public event LayoutEvent LayoutWillBeCreated;
        public event LayoutEvent LayoutWasCreated;
        public event LayoutEvent LayoutWillBeRemoved;
        public event LayoutEvent LayoutWasRemoved;
        public event ReplaceLayoutEvent LayoutWillBeReplaced;
        public event ReplaceLayoutEvent LayoutWasReplaced;
        public event TeleprompterTextEvent TeleprompterTextWillBeChanged;
        public event TeleprompterTextEvent TeleprompterTextWasChanged;
        public event ShotPreviewUpdated ShotPreviewUpdatedevent;
        public event UpdateShotView UpdateShotViewEvent;
        public event UpdateBackgroundLayer UpdateBackgroundLayerEvent;

        #endregion

        #region Public Properties

        public bool IsInkingTurnedOn { set; get; }
        public ComposingContextElement StrokesComposingElement => _strokesComposingElement;
        public bool IsNeedFastThumbnailUpdating
        {
            set { _isNeedFastThumbnailUpdating = value; }
            get { return _isNeedFastThumbnailUpdating; }
        }
        public List<LayerLayoutTemplate> PlaceholderTemplates => _placeHolderTempaltes;
        public string Id => _id;
        public IReadOnlyCollection<LayerLayout> Layers => _layers;
        public GreenScreenSettings GreenScreenSettings => _greenScreenSettings;
        public string TeleprompterText
        {
            get
            {
                return _teleprompterText;
            }
            set
            {
                if (_teleprompterText != value)
                {
                    TeleprompterTextWillBeChanged?.Invoke();
                    _teleprompterText = value;
                    TeleprompterTextWasChanged?.Invoke();
                }
            }
        }
        public bool IsShowTeleprompterInRecordMode { get; set; } = true;
        public Color BackgroundColor => _backgroundColor;
        /// <summary>
        /// Return null from weak reference of object
        /// </summary>
        public Project Project => _project?.TryGetObject();

        #endregion

        #region Public Methods
        public VideoLayerLayout FindVideoBackground()
        {
            return _layers.OfType<VideoLayerLayout>().FirstOrDefault(l => l.IsBackground);
        }

        public void ThumbnailUpdatedEvent()
        {
            ShotPreviewUpdatedevent?.Invoke(this);
        }

        public static async Task<Shot> Create(Project project, List<TcpInfo.Element> elements, 
            TcpInfo.SegmentationSettings greenscreen, bool isShotForTheme, Color backgroundColor)
        {
            var shot = new Shot(project);
            LogQueue.WriteToFile($"Shot {shot._id} Created");
            try
            {
                if (greenscreen != null)
                {
                    shot._greenScreenSettings = await GreenScreenSettings.FromMetaData(project.WorkingFolder, greenscreen);
                }
                else
                {
                    shot._greenScreenSettings = new GreenScreenSettings();
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"Exception in Task<Shot> Create : {ex.Message}");
            }

            shot._backgroundColor = backgroundColor;

            #region LayerLayouts 
            if (elements != null)
            {
                foreach (var element in elements)
                {
                    try
                    {
                        LayerLayout layer = null;
                        if (element.type == "mixer")
                        {
                            layer = await MediaMixerLayerLayout.CreateFromMetaData(shot, element);
                        }
                        else
                        {
                            var url = String.IsNullOrEmpty(element.resource_url) ? element.placeholder_url : element.resource_url;

                            if (url != null)
                            {
                                if (url.Contains("https://") || url.Contains("http://"))
                                {
                                    layer = await WebLayerLayout.CreateFromMetaData(shot, element);
                                }
                                else
                                {
                                    var extension = Path.GetExtension(url).ToLower();
                                    if (FileToAppHelper.ImageExtensions.Contains(extension))
                                    {
                                        layer = await ImageLayerLayout.CreateFromMetaData(shot, element);
                                    }
                                    else if (FileToAppHelper.VideoExtensions.Contains(extension))
                                    {
                                        layer = await VideoLayerLayout.CreateFromMetaData(shot, element);
                                    }
                                    else if (FileToAppHelper.DocumentExtensions.Contains(extension))
                                    {
                                        layer = await DocumentLayerLayout.CreateFromMetaData(shot, element);
                                    }
                                    else if (FileToAppHelper.TextExtensions.Contains(extension))
                                    {
                                        layer = await TextLayerLayout.CreateFromMetaData(shot, element);
                                    }
                                    else if (FileToAppHelper.ShapeExtensions.Contains(extension))
                                    {
                                        layer = await ShapeLayerLayout.CreateFromMetaData(shot, element);
                                    }
                                }
                            }
                            else
                            {
                                layer = await PlaceholderLayerLayout.CreateFromMetaData(shot, element);
                            }
                        }

                        if (layer != null)
                        {
                            if (!isShotForTheme)
                            {
                                await layer.Initialize();
                            }
                            shot._layers.Add(layer);
                            if (layer.ZIndex > shot._zOrderingCounter)
                            {
                                shot._zOrderingCounter = layer.ZIndex;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogQueue.WriteToFile($"Exception in Task<Shot> Create : {ex.Message}");
                    }
                }
                shot._zOrderingCounter++;
            }
            #endregion

            return shot;
        }

        public async static Task<Shot> CreateFrom(Shot shot, Project project = null)
        {
            Shot duplicatedShot = new Shot(shot, project);
            await duplicatedShot.CopyLayerLayoutData();
            LogQueue.WriteToFile($"Shot {duplicatedShot._id} CreateFrom {shot._id}");

            return duplicatedShot;
        }

        public void UpdateProjectHasUnsavedChanges()
        {
            var project = Project;
            if (project != null)
            {
                project.HasUnsavedChanges = true;
            }
        }

        public void SetTeleprompterTextWithUndoRedo(string value)
        {
            string oldTeleprompterText = _teleprompterText;
            TeleprompterText = value;
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new ShotTeleprompterChangedMementoModel() { Shot = this, TeleprompterText = oldTeleprompterText });
        }

        #endregion

        #region Life Cycle

        private Shot(Project project)
        {
            _id = Guid.NewGuid().ToString();
            _layers = new List<LayerLayout>();
            _project = new WeakReference<Project>(project);
            _placeHolderTempaltes = new List<LayerLayoutTemplate>();
            _captureSettings = new CaptureSettings();
            _strokesComposingElement = new ComposingContextElement(_id);
            _strokesComposingElement.Type = ComposingContextElementType.Strokes;
        }

        private Shot(Shot shot, Project project = null) : this(project ?? shot.Project)
        {
            _zOrderingCounter = shot._zOrderingCounter;
            _teleprompterText = shot._teleprompterText;
            _backgroundColor = shot._backgroundColor;
            _greenScreenSettings = shot.GreenScreenSettings.Clone();
            foreach (var layer in shot.Layers)
            {
                _layers.Add(layer.Clone(this));
            }
        }

        public void Subscribe()
        {
            var project = Project;
            if (project != null)
            {
                project.UndoRedoOriginator.UndoEvent += UndoRedoOriginator_UndoEvent;
                project.UndoRedoOriginator.RedoEvent += UndoRedoOriginator_RedoEvent;
            }
        }

        public void Clean()
        {
            LogQueue.WriteToFile($"Shot {_id} Clean");
            var project = Project;
            if (project != null)
            {
                project.UndoRedoOriginator.UndoEvent -= UndoRedoOriginator_UndoEvent;
                project.UndoRedoOriginator.RedoEvent -= UndoRedoOriginator_RedoEvent;
            }
            foreach(var layer in _layers)
            {
                layer.Clean();
            }
        }

#if DEBUG
        ~Shot()
        {
            System.Diagnostics.Debug.WriteLine("*********************** Shot Destructor *************************");
        }
#endif

        #endregion

        #region LayerLayout

        public async Task CopyLayerLayoutData()
        {
            foreach (var layer in Layers)
            {
                if (!(layer is PlaceholderLayerLayout))
                {
                    await layer.CopyDataToProjectFolder();
                    await layer.Initialize();
                }
            }
        }

        public async Task CreateLayer(StorageFile file)
        {
            var template = LayerLayoutTemplate.GenerateDefaultTemplate("File");
            await CreateLayerLayoutFromFileAndTemplate(file, template);
            LogQueue.WriteToFile($"Shot {_id} CreateLayer. Total layers: {_layers.Count}");
        }

        public async Task<bool> CreateLayers(IReadOnlyList<IStorageItem> items)
        {
            bool isShowUnsupportedFormatMessage = false;
            var xPos = LayerLayoutTemplate.DefaultVappPosition.X;
            var yPos = LayerLayoutTemplate.DefaultVappPosition.Y;
            foreach (var item in items)
            {
                try
                {
                    StorageFile file = item as StorageFile;
                    if ((file != null) && (FileToAppHelper.ImageExtensions.Contains(file.FileType) ||
                        FileToAppHelper.VideoExtensions.Contains(file.FileType) ||
                        FileToAppHelper.DocumentExtensions.Contains(file.FileType)))
                    {
                        Rect position = new Rect(xPos, yPos, LayerLayoutTemplate.DefaultWidth, LayerLayoutTemplate.DefaultHeight);
                        var template = LayerLayoutTemplate.GenerateCustomTemplate("File", position, false, true, false, true, true, false);
                        await CreateLayerLayoutFromFileAndTemplate(file, template);
                        xPos += NewVappShiftXPos;
                        yPos += NewVappShiftYPos;
                        if (yPos + LayerLayoutTemplate.DefaultHeight > ComposingContext.VideoSize.Height ||
                            xPos + LayerLayoutTemplate.DefaultWidth > ComposingContext.VideoSize.Width)
                        {
                            yPos = 0;
                            xPos = 0;
                        }
                    }
                    else
                    {
                        isShowUnsupportedFormatMessage = true;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    LogQueue.WriteToFile($"Exception in Shot.CreateLayers {ex.Message}");
                }
            }

            return isShowUnsupportedFormatMessage;
        }

        public async Task<bool> CreateLayerAsBackground(StorageFile file)
        {
            bool isShowUnsupportedFormatMessage = true;
            var template = LayerLayoutTemplate.GenerateDefaultTemplate("File");

            var project = Project;
            if (project != null)
            {
                template.InitialiData.ZIndex = _zOrderingCounter++;
                var localFile = await project.CopyFileToLocalSandbox(file);
                if (localFile == null) return isShowUnsupportedFormatMessage;
                var extension = Path.GetExtension(localFile.Name).ToLower();
                LayerLayout layer = null;
                if (FileToAppHelper.ImageExtensions.Contains(extension))
                {
                    layer = new ImageLayerLayout(localFile, template, this);
                }
                else if (FileToAppHelper.DocumentExtensions.Contains(extension))
                {
                    layer = new DocumentLayerLayout(localFile, template, this);
                }
                else if (FileToAppHelper.VideoExtensions.Contains(extension))
                {
                    layer = new VideoLayerLayout(localFile, template, this);
                }

                if (layer != null)
                {
                    await layer.Initialize();

                    var currentBackgroundLayer = await CreateLayerLayoutAsBackgroundWithoutUndoRedo(layer);

                    Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, new SetAsBackgroundMementoModel(this, currentBackgroundLayer, layer));
                    SetAsBackgroundLikeWithoutUndoRedo(layer, currentBackgroundLayer, null);
                    UpdateBackgroundLayerEvent?.Invoke(layer, currentBackgroundLayer);
                    isShowUnsupportedFormatMessage = false;
                    LogQueue.WriteToFile($"Shot {_id} Total layers: {_layers.Count}");
                }
            }

            LogQueue.WriteToFile($"Shot {_id} CreateLayer as background. Total layers: {_layers.Count}");

            return isShowUnsupportedFormatMessage;
        }

        public async Task<LayerLayout> PrepareForPowerPointSlide(StorageFile file, PlaceholderLayerLayout placeholderLayer)
        {
            var placeholder = FindPlaceholderLike(placeholderLayer);

            var initData = placeholder?.GenerateLayerLayoutInitialData();

            var template = (initData == null) ? LayerLayoutTemplate.GenerateFullScreenTemplate("File", false, false, false, true, false, false) :
            LayerLayoutTemplate.GenerateCustomTemplate(LayerLayoutTemplate.PlaceHolderType, initData.Position, initData.IsStatic, initData.IsSelected, initData.IsHotSpotEnabled, true, true, initData.IsBackground);

            template.InitialiData.ZIndex = (template.Type != LayerLayoutTemplate.PlaceHolderType) ? _zOrderingCounter++ : placeholder.ZIndex;

            var extension = Path.GetExtension(file.Name).ToLower();
            LayerLayout layer = null;
            if (FileToAppHelper.ImageExtensions.Contains(extension))
            {
                layer = new ImageLayerLayout(file, template, this);
            }
            else if (FileToAppHelper.DocumentExtensions.Contains(extension))
            {
                layer = new DocumentLayerLayout(file, template, this);
            }
            else if (FileToAppHelper.VideoExtensions.Contains(extension))
            {
                layer = new VideoLayerLayout(file, template, this);
            }

            if (layer != null)
            {
                await layer.Initialize();

                if (placeholder != null)
                {
                    await ReplaceLayerWithoutUndoRedo(placeholder, layer);
                }
                else
                {
                    await CreateLayerLayoutWithoutUndoRedo(layer);
                }

                LogQueue.WriteToFile($"Shot {_id} Total layers: {_layers.Count}");
            }
            return layer;
        }
        
        public async Task CreateLayerLayoutFromFileAndTemplate(StorageFile file, LayerLayoutTemplate template)
        {
            LogQueue.WriteToFile($"Shot {_id} CreateLayerLayoutFromFileAndTemplate");
            var project = Project;
            if (project != null)
            {
                if (template.Type != LayerLayoutTemplate.PlaceHolderType)
                {
                    template.InitialiData.ZIndex = _zOrderingCounter++;
                }

                var localFile = await project.CopyFileToLocalSandbox(file);
                if (localFile == null) return;
                var extension = Path.GetExtension(localFile.Name).ToLower();
                LayerLayout layer = null;
                if (FileToAppHelper.ImageExtensions.Contains(extension))
                {
                    layer = new ImageLayerLayout(localFile, template, this);
                }
                else if (FileToAppHelper.DocumentExtensions.Contains(extension))
                {
                    layer = new DocumentLayerLayout(localFile, template, this);
                }
                else if (FileToAppHelper.VideoExtensions.Contains(extension))
                {
                    layer = new VideoLayerLayout(localFile, template, this);
                }

                if (layer != null)
                {
                    await layer.Initialize();

                    await CreateLayerLayoutWithoutUndoRedo(layer);
                    project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, layer);

                    LogQueue.WriteToFile($"Shot {_id} Total layers: {_layers.Count}");
                }
            }
        }

        public async Task CreateTextLayer(LayerLayoutTemplate template = null)
        {
            if (template == null)
            {
                template = LayerLayoutTemplate.GenerateDefaultTemplate("Text");
            }
            if (template.Type != LayerLayoutTemplate.PlaceHolderType)
            {
                template.InitialiData.ZIndex = _zOrderingCounter++;
            }
            var layer = new TextLayerLayout(template, this);
            await layer.Initialize();
            await CreateLayerLayoutWithoutUndoRedo(layer);
            Project?.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, layer);
            LogQueue.WriteToFile($"Shot {_id} CreateTextLayer. Total layers: {_layers.Count}");
        }

        public async Task CreateLayer(UrlWithProperties url)
        {
            await CreateLayerLayoutFromUrlAndTemplate(url, LayerLayoutTemplate.GenerateDefaultTemplate("Web"));
            LogQueue.WriteToFile($"Shot {_id} CreateLayer {url}. Total layers: {_layers.Count}");
        }

        public async Task CreateLayerLayoutFromUrlAndTemplate(UrlWithProperties url, LayerLayoutTemplate template)
        {
            if (template.Type != LayerLayoutTemplate.PlaceHolderType)
            {
                template.InitialiData.ZIndex = _zOrderingCounter++;
            }
            var layer = new WebLayerLayout(url, template, this);
            await layer.Initialize();
            await CreateLayerLayoutWithoutUndoRedo(layer);
            Project?.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, layer);
            LogQueue.WriteToFile($"Shot {_id} CreateLayerLayoutFromUrlAndTemplate {url}. Total layers: {_layers.Count}");
        }

        public async Task CreateShapeLayer(InkShapeType inkShapeType)
        {
            if (inkShapeType != InkShapeType.Circle)
            {
                await CreateLayerLayoutFromInkShapeAndTemplate(inkShapeType, 
                    LayerLayoutTemplate.GenerateDefaultTemplate("Shape"));
            }
            else
            {
                await CreateLayerLayoutFromInkShapeAndTemplate(inkShapeType, 
                    LayerLayoutTemplate.GenerateCustomTemplate("Shape", LayerLayoutTemplate.DefaultSquareVappPosition, false, true, false, false, false, false));
            }
            LogQueue.WriteToFile($"Shot {_id} CreateShapeLayer. Total layers: {_layers.Count}");
        }

        public async Task CreateLayerLayoutFromInkShapeAndTemplate(InkShapeType inkShapeType, LayerLayoutTemplate template)
        {
            if (template.Type != LayerLayoutTemplate.PlaceHolderType)
            {
                template.InitialiData.ZIndex = _zOrderingCounter++;
            }
            var layer = new ShapeLayerLayout(inkShapeType, template, this);

            await CreateLayerLayoutWithoutUndoRedo(layer);
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Remove, layer);
            LogQueue.WriteToFile($"Shot {_id} CreateLayerLayoutFromInkShapeAndTemplate. Total layers: {_layers.Count}");
        }

        public async Task ReplaceLayerInsteadOfPlaysholder(StorageFile file, PlaceholderLayerLayout placeholder)
        {
            LogQueue.WriteToFile($"Shot {_id} ReplaceLayerInsteadOfPlaysholder");
            var project = Project;
            if (project != null)
            {
                placeholder.IsLiveOnPlayback = placeholder.IsPauseOnPlayback = true;
                var localFile = await project.CopyFileToLocalSandbox(file);
                if (localFile == null) return;
                var extension = Path.GetExtension(localFile.Name).ToLower();
                LayerLayout layer = null;
                if (FileToAppHelper.ImageExtensions.Contains(extension))
                {
                    layer = new ImageLayerLayout(localFile, placeholder, this);
                }
                else if (FileToAppHelper.DocumentExtensions.Contains(extension))
                {
                    layer = new DocumentLayerLayout(localFile, placeholder, this);
                }
                else if (FileToAppHelper.VideoExtensions.Contains(extension))
                {
                    layer = new VideoLayerLayout(localFile, placeholder, this);
                }
                if (layer != null)
                {
                    await layer.Initialize();
                    await ReplaceLayerWithoutUndoRedo(placeholder, layer);
                    placeholder.IsLiveOnPlayback = placeholder.IsPauseOnPlayback = false;
                    Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new ReplaceLayersMementoModel(this, placeholder, layer));
                }
            }
        }

        public async Task ReplaceLayerInsteadOfPlaysholder(UrlWithProperties url, PlaceholderLayerLayout placeholder)
        {
            LogQueue.WriteToFile($"Shot {_id} ReplaceLayerInsteadOfPlaysholder");
            placeholder.IsLiveOnPlayback = placeholder.IsPauseOnPlayback = true;
            var layer = new WebLayerLayout(url, placeholder, this);
            await layer.Initialize();
            await ReplaceLayerWithoutUndoRedo(placeholder, layer);
            placeholder.IsLiveOnPlayback = placeholder.IsPauseOnPlayback = false;
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new ReplaceLayersMementoModel(this, placeholder, layer));
        }

        public async Task ReplaceLayerInsteadOfPlaysholder(InkShapeType inkShapeType, PlaceholderLayerLayout playceholder)
        {
            LogQueue.WriteToFile($"Shot {_id} ReplaceLayerInsteadOfPlaysholder");
        }

        public async Task ReplaceLayerInsteadOfPlaysholder(PlaceholderLayerLayout placeholder)
        {
            LogQueue.WriteToFile($"Shot {_id} ReplaceLayerInsteadOfPlaysholder");

            var layer = new TextLayerLayout(placeholder, this);
            await ReplaceLayerWithoutUndoRedo(placeholder, layer);
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new ReplaceLayersMementoModel(this, placeholder, layer));
        }

        public async Task ReplaceLayerByPlaceholder(LayerLayout layer)
        {
            var placeholder = new PlaceholderLayerLayout(layer, this);
            await ReplaceLayerWithoutUndoRedo(layer, placeholder);
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new ReplaceLayersMementoModel(this, layer, placeholder));
        }

        private PlaceholderLayerLayout FindPlaceholderLike(PlaceholderLayerLayout placeholderLayer)
        {
            return _layers.OfType<PlaceholderLayerLayout>().FirstOrDefault(l => l.Position == placeholderLayer?.Position);
        }

        private async Task<LayerLayout> CreateLayerLayoutAsBackgroundWithoutUndoRedo(LayerLayout layerLayout)
        {
            LayerLayout currentBackgroundLayer = null;
            if (layerLayout != null)
            {
                currentBackgroundLayer = Layers.FirstOrDefault(l => l.IsBackground);
                layerLayout.IsStatic = true;

                if (LayoutWillBeCreated != null)
                {
                    await LayoutWillBeCreated.Invoke(null);
                }
                _layers.Add(layerLayout);
                UpdateProjectHasUnsavedChanges();
                if (LayoutWasCreated != null)
                {
                    await LayoutWasCreated.Invoke(layerLayout);
                }
            }
            return currentBackgroundLayer;
        }
        private async Task CreateLayerLayoutWithoutUndoRedo(LayerLayout layerLayout)
        {
            if (layerLayout != null)
            {
                if (LayoutWillBeCreated != null)
                {
                    await LayoutWillBeCreated.Invoke(null);
                }
                _layers.Add(layerLayout);
                UpdateProjectHasUnsavedChanges();
                if (LayoutWasCreated != null)
                {
                    await LayoutWasCreated.Invoke(layerLayout);
                }
            }
        }

        private async Task ReplaceLayerWithoutUndoRedo(LayerLayout oldLayer, LayerLayout newLayer)
        {
            if (LayoutWillBeReplaced != null)
            {
                await LayoutWillBeReplaced.Invoke(oldLayer, newLayer);
            }

            _layers.Remove(oldLayer);
            newLayer.IsSelectedAfterCreation = true;
            _layers.Add(newLayer);
            UpdateProjectHasUnsavedChanges();

            if (LayoutWasReplaced != null)
            {
                await LayoutWasReplaced.Invoke(oldLayer, newLayer);
            }
        }

        private async Task RemoveLayerLayoutWithoutUndoRedo(LayerLayout layerLayout)
        {
            if (LayoutWillBeRemoved != null)
            {
                await LayoutWillBeRemoved.Invoke(layerLayout);
            }
            _layers.Remove(layerLayout);
            UpdateProjectHasUnsavedChanges();
            if (LayoutWasRemoved != null)
            {
                await LayoutWasRemoved.Invoke(layerLayout);
            }
        }

        public async Task RemoveLayer(LayerLayout layerLayout)
        {
            LogQueue.WriteToFile($"Shot {_id} RemoveLayer. Total layers: {_layers.Count}");
            await RemoveLayerLayoutWithoutUndoRedo(layerLayout);
            Project?.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Add, layerLayout);
        }

        public async void InsertLayer(LayerLayout model)
        {
            if (LayoutWillBeCreated != null)
            {
                await LayoutWillBeCreated.Invoke(model);
            }

            model.ZIndex = _zOrderingCounter++;
            model.UpdateShotReference(this);
            _layers.Add(model);
            UpdateProjectHasUnsavedChanges();
            if (LayoutWasCreated != null)
            {
                await LayoutWasCreated.Invoke(model);
            }
            LogQueue.WriteToFile($"Shot {_id} InsertLayer. Total layers: {_layers.Count}");
        }

        public void MovementChanged(LayerLayoutMementoModel model)
        {
            UpdateProjectHasUnsavedChanges();
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, model);
        }

        public async Task UpdateBackgroundColor(Color color)
        {
            var currentBackgroundLayer = Layers.FirstOrDefault(l => l.IsBackground);
            if (currentBackgroundLayer != null)
            {
                await RemoveLayerLayoutWithoutUndoRedo(currentBackgroundLayer);
            }

            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new SetAsBackgroundMementoModel(this, _backgroundColor, color, currentBackgroundLayer));
            ComposingContext.Shared().FrameBackgroundColor = color;
            _backgroundColor = color;

            UpdateShotViewEvent?.Invoke();
        }

        public void SetAsBackground(LayerLayout newBackgroundLayer)
        {
            var currentBackgroundLayer = Layers.FirstOrDefault(l => l.IsBackground);

            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, new SetAsBackgroundMementoModel(this, currentBackgroundLayer, newBackgroundLayer));

            SetAsBackgroundLikeWithoutUndoRedo(newBackgroundLayer, currentBackgroundLayer, null);
            UpdateBackgroundLayerEvent?.Invoke(newBackgroundLayer, currentBackgroundLayer);
        }

        public List<LayerLayout> ChangeZIndex(LayerLayout layer, ZIndexPosition position)
        {
            var result = new List<LayerLayout>();

            var mementoModel = new UndoRedoBaseMementoModel(UndoRedoMementoType.Change, GenerateMementoModel(layer));
            switch (position)
            {
                case ZIndexPosition.Previous:
                    result.Add(ChangeZIndex(layer, -1, mementoModel));
                    break;
                case ZIndexPosition.Next:
                    result.Add(ChangeZIndex(layer, 1, mementoModel));
                    break;
                case ZIndexPosition.Bottom:
                    {
                        var indexRange = GetZIndexRange();
                        if (indexRange != null)
                        {
                            while (indexRange.MinIndex < layer.ZIndex)
                            {
                                result.Add(ChangeZIndex(layer, -1, mementoModel));
                            }
                        }
                    }
                    break;
                case ZIndexPosition.Top:
                    {
                        var indexRange = GetZIndexRange();
                        if (indexRange != null)
                        {
                            while (indexRange.MaxIndex > layer.ZIndex)
                            {
                                result.Add(ChangeZIndex(layer, 1, mementoModel));
                            }
                        }
                    }
                    break;
            }

            Project.UndoRedoOriginator.PushNewUndoAction(mementoModel);
            return result;
        }

        public void InteractivityParametersChanged(LayerLayoutMementoModel model)
        {
            UpdateProjectHasUnsavedChanges();
            Project.UndoRedoOriginator.PushNewUndoAction(UndoRedoMementoType.Change, model);
        }

        public IndexRange GetZIndexRange()
        {
            var notStaticViews = _layers.Where(v => !v.IsStatic).OrderBy(v => v.ZIndex).ToList();
            if (notStaticViews.Count > 1)
            {
                int minZIndex = notStaticViews.Min(l => l.ZIndex);
                int maxZIndex = notStaticViews.Max(l => l.ZIndex);

                return new IndexRange(minZIndex, maxZIndex);
            }

            return null;
        }

        public async Task DuplicateLayer(LayerLayout layer)
        {
            if (layer == null) return;

            LayerLayout duplicateLayer = layer.Clone();
            duplicateLayer.IsSelectedAfterCreation = true;
            await duplicateLayer.CopyDataToProjectFolder();
            InsertLayer(duplicateLayer);
        }

        #endregion

        #region Recording
        public async Task PrepareForRecording()
        {
            foreach (var layer in _layers)
            {
                if (layer?.ComposingContextElement?.Bitmap != null && ComposingContext.Shared().PreviewingDevice != null)
                {
                    var canvasBitmap = (layer.ComposingContextElement.Bitmap as Microsoft.Graphics.Canvas.CanvasBitmap);
                    if (canvasBitmap.Device != ComposingContext.Shared().PreviewingDevice)
                    {
                        layer.ComposingContextElement.Bitmap = Microsoft.Graphics.Canvas.CanvasBitmap.CreateFromBytes(ComposingContext.Shared().PreviewingDevice,
                            canvasBitmap.GetPixelBytes(),
                            (int)canvasBitmap.SizeInPixels.Width,
                            (int)canvasBitmap.SizeInPixels.Height,
                            canvasBitmap.Format
                            );
                        canvasBitmap.Dispose();
                    }
                }
            }
        }

        #endregion

        #region Private Methods    
        private LayerLayoutMementoModel GenerateMementoModel(LayerLayout layer)
        {
            var mementoModel = layer.GenerateMementoModel();
            mementoModel.PreviousPosition = layer.Position;
            mementoModel.PreviousTransform = layer.Transform;
            return mementoModel;
        }
        private LayerLayout ChangeZIndex(LayerLayout layer, int delta, UndoRedoBaseMementoModel mementoModel)
        {
            LayerLayout layerToSwap = null;
            var layers = _layers.Where(v => !v.IsStatic).OrderBy(v => v.ZIndex).ToList();
            var index = layers.IndexOf(layer) + delta;
            if (index > -1 && index < layers.Count)
            {
                layerToSwap = layers[index];
                var mementoModelToSwap = new UndoRedoBaseMementoModel(UndoRedoMementoType.Change, GenerateMementoModel(layerToSwap));
                while (mementoModel.ChainNode != null) mementoModel = mementoModel.ChainNode;
                mementoModel.ChainNode = mementoModelToSwap;

                var newZIndex = layerToSwap.ZIndex;
                var indexToSwap = layer.ZIndex;
                if (indexToSwap == newZIndex)
                {
                    newZIndex += delta;
                    if (newZIndex < 0) newZIndex = 0;
                }
                layer.ZIndex = newZIndex;
                layerToSwap.ZIndex = indexToSwap;
                layerToSwap.UpdateThumbnail();
                layer.UpdateThumbnail();
            }
            return layerToSwap;
        }

        #region UndoRedo

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_RedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> UndoRedoOriginator_UndoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            return await ProcessUndoRedoEvent(mementoModel);
        }

        private async Task<UndoRedoBaseMementoModel> ProcessUndoRedoEvent(UndoRedoBaseMementoModel mementoModel)
        {
            if (mementoModel.MementoObject is LayerLayout)
            {
                var layer = mementoModel.MementoObject as LayerLayout;
                if (layer.Shot?.Id == Id)
                {
                    switch (mementoModel.ActionType)
                    {
                        case UndoRedoMementoType.Add:
                            {
                                layer.IsSelectedAfterCreation = true;
                                await CreateLayerLayoutWithoutUndoRedo(layer);
                            }
                            break;
                        case UndoRedoMementoType.Remove:
                            {
                                await RemoveLayerLayoutWithoutUndoRedo(layer);
                            }
                            break;
                    }
                    return mementoModel;
                }
            }
            else if (mementoModel.MementoObject is LayerLayoutMementoModel)
            {
                var layerLayoutMementoModel = mementoModel.MementoObject as LayerLayoutMementoModel;
                var layerLayout = _layers.FirstOrDefault(l => l.Id == layerLayoutMementoModel.Id);
                if (layerLayout != null)
                {
                    var newMementoModel = layerLayout.ProcessMementoModel(layerLayoutMementoModel);

                    mementoModel.MementoObject = newMementoModel;
                    return mementoModel;
                }
            }
            else if (mementoModel.MementoObject is ReplaceLayersMementoModel)
            {
                var replaceLayerMementoModel = mementoModel.MementoObject as ReplaceLayersMementoModel;
                if (replaceLayerMementoModel.ShotId == Id)
                {
                    await ReplaceLayerWithoutUndoRedo(replaceLayerMementoModel.NewLayer, replaceLayerMementoModel.OldLayer);
                    mementoModel.MementoObject = new ReplaceLayersMementoModel(this, replaceLayerMementoModel.NewLayer, replaceLayerMementoModel.OldLayer);
                    return mementoModel;
                }
            }
            else if (mementoModel.MementoObject is SetAsBackgroundMementoModel)
            {
                var setBackgroundMementoModel = mementoModel.MementoObject as SetAsBackgroundMementoModel;
                if (setBackgroundMementoModel.ShotId == Id)
                {
                    var newModel = setBackgroundMementoModel.CurrentBackgroundLayer;
                    var currentModel = setBackgroundMementoModel.NewBackgroundLayer;
                    switch (mementoModel.ActionType)
                    {
                        case UndoRedoMementoType.Add:
                            {
                                var currentBackgroundLayer = await CreateLayerLayoutAsBackgroundWithoutUndoRedo(newModel);

                                mementoModel.MementoObject = new SetAsBackgroundMementoModel(this, currentModel, newModel);

                                SetAsBackgroundLikeWithoutUndoRedo(newModel, currentBackgroundLayer, null);
                                UpdateBackgroundLayerEvent?.Invoke(newModel, currentBackgroundLayer);
                            }
                            break;

                        case UndoRedoMementoType.Remove:
                            {
                                mementoModel.MementoObject = new SetAsBackgroundMementoModel(this, currentModel, newModel);

                                SetAsBackgroundLikeWithoutUndoRedo(newModel, currentModel, setBackgroundMementoModel.BackgroundVappState);
                                UpdateBackgroundLayerEvent?.Invoke(newModel, currentModel);

                                await RemoveLayerLayoutWithoutUndoRedo(currentModel);
                            }
                            break;

                        case UndoRedoMementoType.Change:
                            {
                                if (setBackgroundMementoModel.IsColorChanged)
                                {
                                    var currentBackgroundLayer = setBackgroundMementoModel.CurrentBackgroundLayer;
                                    if (currentBackgroundLayer != null)
                                    {
                                        await CreateLayerLayoutWithoutUndoRedo(currentBackgroundLayer);
                                        UpdateBackgroundLayerEvent?.Invoke(currentBackgroundLayer, null);
                                        currentBackgroundLayer = null;
                                    }
                                    else
                                    {
                                        currentBackgroundLayer = Layers.FirstOrDefault(l => l.IsBackground);
                                        if (currentBackgroundLayer != null)
                                        {
                                            await RemoveLayerLayoutWithoutUndoRedo(currentBackgroundLayer);
                                        }
                                    }
                                    _backgroundColor = setBackgroundMementoModel.CurrentBackgroundColor;
                                    ComposingContext.Shared().FrameBackgroundColor = _backgroundColor;
                                    UpdateShotViewEvent?.Invoke();
                                    mementoModel.MementoObject = new SetAsBackgroundMementoModel(this, setBackgroundMementoModel.NewBackgroundColor, setBackgroundMementoModel.CurrentBackgroundColor, currentBackgroundLayer);
                                }
                                else
                                {
                                    mementoModel.MementoObject = new SetAsBackgroundMementoModel(this, currentModel, newModel);

                                    SetAsBackgroundLikeWithoutUndoRedo(newModel, currentModel, setBackgroundMementoModel.BackgroundVappState);
                                    UpdateBackgroundLayerEvent?.Invoke(newModel, currentModel);
                                }
                            }
                            break;
                    }
                    return mementoModel;
                }
            }
            else if (mementoModel.MementoObject is ShotTeleprompterChangedMementoModel)
            {
                ShotTeleprompterChangedMementoModel shotMementoModel = mementoModel.MementoObject as ShotTeleprompterChangedMementoModel;
                string currentTeleprompterText = shotMementoModel.Shot.TeleprompterText;
                shotMementoModel.Shot.TeleprompterText = shotMementoModel.TeleprompterText;
                shotMementoModel.TeleprompterText = currentTeleprompterText;
                return mementoModel;
            }
            return null;
        }

        private void SetAsBackgroundLikeWithoutUndoRedo(LayerLayout newBackgroundLayer, LayerLayout currentBackgroundLayer, BackgroundVappState vappState)
        {
            if (newBackgroundLayer == null)
            {
                currentBackgroundLayer.IsBackground = false;
                currentBackgroundLayer.IsStatic = false;
                currentBackgroundLayer.ZIndex = vappState.ZIndex;
                currentBackgroundLayer.Transform.TranslateX = vappState.Position.X;
                currentBackgroundLayer.Transform.TranslateY = vappState.Position.Y;
                currentBackgroundLayer.UpdatePosition(vappState.Position.Width, vappState.Position.Height);
                return;
            }
            
            int zIndex = 0;
            if (currentBackgroundLayer != null)
            {
                zIndex = currentBackgroundLayer.ZIndex;
                currentBackgroundLayer.IsBackground = false;
                currentBackgroundLayer.IsStatic = false;
                currentBackgroundLayer.ZIndex = newBackgroundLayer.ZIndex;
                currentBackgroundLayer.Transform.TranslateX = newBackgroundLayer.Transform.TranslateX;
                currentBackgroundLayer.Transform.TranslateY = newBackgroundLayer.Transform.TranslateY;
                currentBackgroundLayer.UpdatePosition(newBackgroundLayer.Position.Width, newBackgroundLayer.Position.Height);
            }
            else
            {
                zIndex = Layers.Min(l => l.ZIndex) - 1;
            }

            newBackgroundLayer.ZIndex = zIndex;
            newBackgroundLayer.IsBackground = true;
            newBackgroundLayer.IsStatic = true;
            newBackgroundLayer.IsLiveOnPlayback = (newBackgroundLayer is WebLayerLayout) || !String.IsNullOrEmpty(newBackgroundLayer.InteractivityUrl.UrlString);
            newBackgroundLayer.IsHotSpot = false;
            newBackgroundLayer.IsPauseOnPlayback = (newBackgroundLayer is WebLayerLayout) || !String.IsNullOrEmpty(newBackgroundLayer.InteractivityUrl.UrlString);

            newBackgroundLayer.Transform.TranslateX = (vappState == null) ? 0 : vappState.Position.X;
            newBackgroundLayer.Transform.TranslateY = (vappState == null) ? 0 : vappState.Position.Y;
            var width = (vappState == null) ? ComposingContext.VideoSize.Width : vappState.Position.Width;
            var height = (vappState == null) ? ComposingContext.VideoSize.Height : vappState.Position.Height;
            newBackgroundLayer.UpdatePosition(width, height);
        }
        #endregion

        #endregion
    }
}
