﻿using Newtonsoft.Json;
using System.Linq;
using Windows.Storage;

namespace Pitch.Services
{
    public class SettingsService : ISettingsService
    {
        public const string StrongDevice = "IsStrongDevice";
        public const string UserPermission = "UserPermission";
        public const string DeviceId = "DeviceID";
        public const string GeForceName = "GeForce";
        public const string RadeonName = "Radeon";
        private const double MinimumRamSizeInGB = 4;
        private const int MinimumCoreNumber = 4;
        private const int OptimalCoreNumber = 8;
        private const double DisplayWidth = 1550;
        private ApplicationDataContainer _settingsContainer;

        public SettingsService()
        {
            _settingsContainer = ApplicationData.Current.LocalSettings;
        }

        public TValue LoadFromSettings<TValue>(string key)
        {
            if (_settingsContainer.Values.ContainsKey(key))
            {
                return JsonConvert.DeserializeObject<TValue>(_settingsContainer.Values[key].ToString());
            }
            else
            {
                return default(TValue);
            }
        }

        public bool SaveToSettings<TValue>(string key, TValue value)
        {
            bool succeeded = false;

            try
            {
                _settingsContainer.Values[key] = JsonConvert.SerializeObject(value);
                succeeded = true;
            }
            catch
            { }
            
            return succeeded;
        }

        public void RemoveFromSettings(string key)
        {
            _settingsContainer.Values.Remove(key);
        }
        public bool IsDeviceStrong
        {
            get
            {
                if (_settingsContainer.Values.ContainsKey(StrongDevice))
                {
                    return LoadFromSettings<bool>(StrongDevice);
                }

                var hardwareInfo = TouchCastComposingEngine.ComposingContext.TryGetHardwareInformation();
                if (hardwareInfo != null)
                {
                    var isStrong = CheckDeviceHardware(hardwareInfo);
                    SaveToSettings(StrongDevice, isStrong);
                    return isStrong;
                }
                return false;
            }
        }

        public ModeUserPermission LightModeUserPermission
        {
            get
            {
                if (_settingsContainer.Values.ContainsKey(UserPermission))
                {
                    return LoadFromSettings<ModeUserPermission>(UserPermission);
                }
                return ModeUserPermission.Undefined;
            }
            set
            {
                SaveToSettings(UserPermission, value);
            }
        }
        private bool CheckDeviceHardware(TouchCastComposingEngine.HardwareInfo hardwareInfo)
        {
            var deviceIsStrong = false;

            var isExtendedAdapter = hardwareInfo.GraphicAdaptersInfo.Any(i => i.Description.Contains(GeForceName) || i.Description.Contains(RadeonName));

            if (hardwareInfo.RamSizeInGB < MinimumRamSizeInGB)
            {
                deviceIsStrong = false;
            }
            else if (hardwareInfo.NumberOfCores < OptimalCoreNumber || !isExtendedAdapter)
            {
                deviceIsStrong = (hardwareInfo.DisplayWidth < DisplayWidth && hardwareInfo.NumberOfCores >= MinimumCoreNumber);
            }
            else
            {
                deviceIsStrong = true;
            }

            return deviceIsStrong;
        }
    }
}
