﻿namespace Pitch.Services
{
    public enum ModeUserPermission
    {
        Undefined,
        LightModeEnabled,
        LightModeDisabled
    }

    public interface ISettingsService
    {
        bool IsDeviceStrong { get; }
        ModeUserPermission LightModeUserPermission { set; get; }
        TValue LoadFromSettings<TValue>(string key);
        bool SaveToSettings<TValue>(string key, TValue value);
        void RemoveFromSettings(string key);
    }
}
