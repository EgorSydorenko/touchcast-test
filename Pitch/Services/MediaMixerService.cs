﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TouchCastComposingEngine;
using Windows.Devices.Enumeration;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Media.Devices;
using Windows.Media.Editing;
using Windows.Media.Effects;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Pitch.Services
{
    public class MediaMixerService : IMediaMixerService
    {
        #region Constants

        public readonly static BitmapSize VideoSize = new BitmapSize() { Width = 1280, Height = 720 };
        private const long _permissionsDeniedAccesCode = 2147942405;
        private const long _deviceLockCode = 3222091524;
        private const string ThumbnailFileName = "thumbnail.png";
        private static string _selectedCameraSettingsKey = "SelectedCamera";
        private static string _selectedMicrophoneSettingsKey = "SelectedMicrophone";

        #endregion

        #region Private Fields

        private readonly MediaEncodingProfile _profile;
        private readonly object _cameraPreviewDeviceLock = new object();
        private readonly ISettingsService _settingsService;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private TouchcastComposerServiceMode _composerMode;
        // Prevent the screen from sleeping while the camera is running
        private readonly DisplayRequest _preventDisplaySleepingRequest = new DisplayRequest();
        private bool _swapChainEventSubscribed;
        private TouchcastComposerServiceState _state = TouchcastComposerServiceState.Idle;
        private TimeSpan _videoFileDuration;
        private MediaCapture _mediaCapture;
        private LowLagMediaRecording _lowLagRecording;
        private IRandomAccessStream _videoFileStream;
        private StorageFile _recordingFile;
        private StorageFile _videoThumbnailFile;
        private CanvasDevice _cameraPreviewDevice;
        private TimeSpan _videoDuration = new TimeSpan(0);
        private CanvasSwapChain _swapchain = null;
        private CanvasSwapChainPanel _cameraSwapChainPanel;
        private CaptureElement _captureUiElement;
        // Capture devices information
        private DeviceInformationCollection _availableCameras;
        private DeviceInformationCollection _availableMicrophones;
        private DeviceInformation _cameraDevice;
        private DeviceInformation _micDevice;
        private DeviceWatcher _microphoneWatcher;
        private DeviceWatcher _cameraWatcher;
        private Models.AsyncLock _lock = new Models.AsyncLock(1);

        #endregion

        #region Properties

        public TouchcastComposerServiceMode ComposerMode => _composerMode;

        public TouchcastComposerServiceState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;

                //update prevent sleeping request
                //call be done on the main thread only
                try
                {
                    if (_state == TouchcastComposerServiceState.Idle)
                    {
                        //state idle do nothing
                    }
                    else if (_state == TouchcastComposerServiceState.Failed ||
                             _state == TouchcastComposerServiceState.Completed)
                    {
                        _preventDisplaySleepingRequest.RequestRelease();
                    }
                    else
                    {
                        _preventDisplaySleepingRequest.RequestActive();
                    }
                }
                catch
                {
                    //if we alreay requested state exception will be called
                }

                StateChangedEvent?.Invoke(this);
            }
        }

        public BitmapSize ResultVideoSize => VideoSize;

        public StorageFile VideoFile => _recordingFile;

        public StorageFile VideoThumbnailFile => _videoThumbnailFile;

        public TimeSpan VideoDuration
        {
            get
            {
                if (_state == TouchcastComposerServiceState.Recording
                    || _state == TouchcastComposerServiceState.Completed
                    || _state == TouchcastComposerServiceState.Paused)
                {
                   return _stopwatch.Elapsed;
                }
               
                return _videoDuration;
            }
        }

        public TimeSpan RecordedVideoFileDuration => _videoFileDuration;

        public CanvasDevice CameraPreviewDevice
        {
            get
            {
                return _cameraPreviewDevice;
            }
        }

        public CanvasSwapChain SwapChain => _swapchain;

        #endregion

        #region Events 

        public event TouchcastComposerServiceStateChanged StateChangedEvent;
        public event TouchcastComposerServiceWillStartRecording WillStartRecordingEvent;
        public event TouchcastComposerServiceSwapChainReadyEvent CanvasSwapChainReadyEvent;

        #endregion

        #region Service LifeCycle

        public MediaMixerService()
        {
            _composerMode = App.Locator.ComposingMode;
            _settingsService = SimpleIoc.Default.GetInstance<ISettingsService>();

            _profile = MediaEncodingProfile.CreateMp4(VideoEncodingQuality.HD720p);
 
            if (_profile.Video != null)
            {
                _profile.Video.ProfileId = H264ProfileIds.Baseline;
                _profile.Video.Bitrate = 4000000; //for now will record video 6Mb/s
            }

            _microphoneWatcher = DeviceInformation.CreateWatcher(DeviceClass.AudioCapture);
            _cameraWatcher = DeviceInformation.CreateWatcher(DeviceClass.VideoCapture);
        }

#if DEBUG
        ~MediaMixerService()
        {
            Debug.WriteLine("********************MediaMixerService Destructor********************");
        }
#endif

        #endregion

        #region MediaCapture preview object initialisation

        public void InitWithCamera()
        {
            _composerMode = TouchcastComposerServiceMode.CameraComposingMode;
            
            ComposingContext.Shared().ComposerState = ComposingModeState.CameraRecording;
            if (State == TouchcastComposerServiceState.Idle)
            {
                State = TouchcastComposerServiceState.ComposingModeDefined;
            }
        }

        public CanvasSwapChainPanel InitWithCamera(int a)
        {
            _composerMode = TouchcastComposerServiceMode.CameraComposingMode;

            _cameraSwapChainPanel = new CanvasSwapChainPanel
            {
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch
            };

            ComposingContext.Shared().ComposerState = ComposingModeState.CameraRecording;
            if (State == TouchcastComposerServiceState.Idle)
            {
                State = TouchcastComposerServiceState.ComposingModeDefined;
            }
            if(_swapchain != null)
            {
                _cameraSwapChainPanel.SwapChain = _swapchain;
            }
            return _cameraSwapChainPanel;
        }

        #endregion

        #region MediaCapture Public Interface
        
        public async Task StartPreviewingAsync()
        {
            await _lock.WaitAsync();
            try
            {
                if (State == TouchcastComposerServiceState.ComposingModeDefined || State == TouchcastComposerServiceState.Completed)
                {
                    //Refreshign Capturing devices list
                    ComposingContext.Shared().State = ComposingState.Previewing;
                    await RefreshAvailableCapturingDevicesAsync();

                    //cleaning up
                    //as recording might be in the progress
                    await Reset();

                    if (!_swapChainEventSubscribed)
                    {
                        ComposingContext.Shared().SwapChainReadyEvent += SwapChainReadyEvent;
                        _swapChainEventSubscribed = true;
                    }
                    //setup capture settings
                    var captureInitSettings = new MediaCaptureInitializationSettings();

                    //refreshing list of available devices
                    //we can't proceed without camera or mic
                    if (_micDevice == null || _cameraDevice == null)
                    {
                        State = TouchcastComposerServiceState.NoDevices;
                        return;
                    }

                    captureInitSettings.AudioDeviceId = _micDevice.Id;
                    captureInitSettings.VideoDeviceId = _cameraDevice.Id;

                    //refresh orietation and mirroring properties
                    RefreshCameraPreviewOrientationAndMirroring();

                    //listening for the orientation change
                    DisplayInformation.GetForCurrentView().OrientationChanged += OnDisplayOrientationChanged;

                    //we're going to capture video and audio
                    captureInitSettings.StreamingCaptureMode = StreamingCaptureMode.AudioAndVideo;

                    // in case mediaCapture.MediaCaptureSettings.VideoDeviceCharacteristic == AllStreamsIdentical
                    // or mediaCapture.MediaCaptureSettings.VideoDeviceCharacteristic == PreviewRecordStreamsIdentical
                    ComposingContext.Shared().Elements = null;

                    //setup media capure
                    _mediaCapture = new MediaCapture();

                    //if capturing fails, our object should fail as well
                    _mediaCapture.Failed += MediaCaptureFailed;

                    await _mediaCapture.InitializeAsync(captureInitSettings);

                    await FilterCameraEncodingProperties();
                    UpdateCameraResolution();
                    //will listen when compser sends us camera preview image and device
                    if (ComposerMode == TouchcastComposerServiceMode.CameraComposingMode)
                    {
                        await _mediaCapture.AddVideoEffectAsync(new VideoEffectDefinition(typeof(PreviewingVideoEffect).FullName), MediaStreamType.VideoPreview);
                    }
                    else
                    {
                        await _mediaCapture.AddAudioEffectAsync(new AudioEffectDefinition(typeof(ComposingAudioEffect).FullName));
                    }

                    await _mediaCapture.AddVideoEffectAsync(new VideoEffectDefinition(typeof(ComposingVideoEffect).FullName), MediaStreamType.VideoRecord);

                    //this is super weird thing
                    //in order to start preview media capture should have sink
                    //otherwise you will get an exception
                    //creating custom sink appears to be not trivial
                    //using UI element for now
                    //we need to change that later
                    if (ComposerMode == TouchcastComposerServiceMode.CameraComposingMode)
                    {
                        _captureUiElement = new CaptureElement();
                        _captureUiElement.Source = _mediaCapture;
                        await _mediaCapture.StartPreviewAsync();
                    }

                    StartDeviceWatching();

                    State = TouchcastComposerServiceState.Previewing;
                }
            }
            catch (UnauthorizedAccessException)
            {
                State = TouchcastComposerServiceState.DoNotHasAccess;
                return;
            }
            catch (Exception e)
            {
                State = TouchcastComposerServiceState.UnhandledFail;
                return;
            }
            finally
            {
                _lock.Release();
            }

        }

        public async Task StartRecordingAsync()
        {
            await _lock.WaitAsync();
            try
            {
                bool shouldStartVappsUpdate = State == TouchcastComposerServiceState.Previewing ||
                   State == TouchcastComposerServiceState.Paused;

                if (State == TouchcastComposerServiceState.Previewing)
                {
                    try
                    {
                        //@FIXME: Remove that event. Use state change istead
                        WillStartRecordingEvent?.Invoke(this);

                        //create output file
                        _videoThumbnailFile = null;
                        var workFolder = SimpleIoc.Default.GetInstance<DataLayer.AuthoringSession>().Project.WorkingFolder;
                        var videoFileName = $"{Guid.NewGuid()}.mp4";
                        var tmporaryRecordingFile = await workFolder.CreateFileAsync(videoFileName, CreationCollisionOption.ReplaceExisting);
                        _videoFileStream = await tmporaryRecordingFile.OpenAsync(FileAccessMode.ReadWrite);

                        _lowLagRecording = await _mediaCapture.PrepareLowLagRecordToStreamAsync(_profile, _videoFileStream);
                        ComposingContext.Shared().State = ComposingState.Recording;
                        ComposingContext.Shared().IsRecording = true;
                        await _lowLagRecording.StartAsync();

                        State = TouchcastComposerServiceState.Recording;
                        _recordingFile = null;
                        _recordingFile = tmporaryRecordingFile;
                    }
                    catch /*(Exception ex)*/
                    {
                        // ignored
                    }
                }
                else
                {
                    if (State == TouchcastComposerServiceState.Paused)
                    {
                        State = TouchcastComposerServiceState.Recording;

                        ComposingContext.Shared().IsRecording = true;
                        await _lowLagRecording.ResumeAsync();
                    }
                    else
                    {
                        //throw new NotSupportedException();
                    }
                }

                if (shouldStartVappsUpdate)
                {
                    //running vapps update task
                    _stopwatch.Start();
                }
            }
            finally
            {
                _lock.Release();
            }
        }

        public async Task PauseRecordingAsync()
        {
            await _lock.WaitAsync();
            try
            {
                if (State == TouchcastComposerServiceState.Recording)
                {
                    await _lowLagRecording.PauseAsync(MediaCapturePauseBehavior.RetainHardwareResources);

                    ComposingContext.Shared().State = ComposingState.Paused;
                    ComposingContext.Shared().VideoRecordingPaused();
                    _stopwatch.Stop();
                    State = TouchcastComposerServiceState.Paused;

                }
                else
                {
                    //throw new NotSupportedException();
                }
            }
            finally
            {
                _lock.Release();
            }
        }

        public async Task StopRecordingAsync()
        {
            await _lock.WaitAsync();
            try
            {
                if (State == TouchcastComposerServiceState.Recording ||
                    State == TouchcastComposerServiceState.Paused)
                {
                    try
                    {
                        ComposingContext.Shared().IsRecording = false;
                        await _lowLagRecording.StopAsync();
                        await _lowLagRecording.FinishAsync();
                    }
                    catch (Exception)
                    {

                    }

                    _videoFileStream.Dispose();
                    var videoProps = await _recordingFile.Properties.GetVideoPropertiesAsync();
                    _videoFileDuration = videoProps.Duration;
                    _stopwatch.Stop();
                    State = TouchcastComposerServiceState.Completed;
                }
            }
            finally
            {
                _lock.Release();
            }
        }

        public async Task StopPreviewingAsync()
        {
            await _lock.WaitAsync();
            try
            {
                if (ComposerMode == TouchcastComposerServiceMode.CameraComposingMode)
                {
                    await _mediaCapture.StopPreviewAsync();
                    await _mediaCapture.ClearEffectsAsync(MediaStreamType.VideoPreview);
                }
                _stopwatch.Stop();
            }
            finally
            {
                _lock.Release();
            }
        }

        public async Task HardStopRecordingAsync()
        {
            await _lock.WaitAsync();
            try
            {
                if (State == TouchcastComposerServiceState.Recording ||
                    State == TouchcastComposerServiceState.Paused)
                {
                    try
                    {
                        if (ComposerMode == TouchcastComposerServiceMode.CameraComposingMode)
                        {
                            await _mediaCapture.StopPreviewAsync();
                            await _mediaCapture.ClearEffectsAsync(MediaStreamType.VideoPreview);
                        }

                        await _mediaCapture.StopRecordAsync();
                        await _mediaCapture.ClearEffectsAsync(MediaStreamType.VideoRecord);
                    }
                    catch { }

                    _videoFileStream.Dispose();
                    var videoProps = await _recordingFile.Properties.GetVideoPropertiesAsync();
                    _videoFileDuration = videoProps.Duration;
                    _stopwatch.Stop();
                    State = TouchcastComposerServiceState.Completed;
                }
            }
            finally
            {
                _lock.Release();
            }
        }

        public async Task Reset()
        {
            if (_mediaCapture != null)
            {
                _mediaCapture.Failed -= MediaCaptureFailed;
            }

            if (State == TouchcastComposerServiceState.Recording ||
               State == TouchcastComposerServiceState.Paused)
            {
                await StopRecordingAsync();
            }

            _stopwatch.Stop();
            _stopwatch.Reset();
            //reset listings
            if (_swapChainEventSubscribed 
                && State != TouchcastComposerServiceState.ComposingModeDefined )
            {
                if (ComposingContext.Shared().SwapChainReadyEvent != null)
                {
                    ComposingContext.Shared().SwapChainReadyEvent -= SwapChainReadyEvent;
                }
                _swapChainEventSubscribed = false;
            }
            var displayData = DisplayInformation.GetForCurrentView();
            if (displayData != null)
            {
                displayData.OrientationChanged -= OnDisplayOrientationChanged;
            }

            if (_cameraPreviewDevice != null)
            {
                lock (_cameraPreviewDeviceLock)
                {
                    _cameraPreviewDevice.Dispose();
                    _cameraPreviewDevice = null;
                }
            }

            if (_mediaCapture != null)
            {
                _mediaCapture.Failed -= MediaCaptureFailed;
                try
                {
                    if (_mediaCapture.CameraStreamState != CameraStreamState.Shutdown)
                    {
                        await _mediaCapture.ClearEffectsAsync(MediaStreamType.VideoPreview);
                        await _mediaCapture.ClearEffectsAsync(MediaStreamType.VideoRecord);

                        await _mediaCapture.StopPreviewAsync();
                    }
                    _mediaCapture.Dispose();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            _mediaCapture = null;

            StopDeviceWatching();

            State = TouchcastComposerServiceState.Idle;
        }

        #endregion

        #region Managing Capture Devices

        public async Task RefreshAvailableCapturingDevicesAsync()
        {
            await UpdateCurrentCameraDevice();
            await UpdateCurrentMicroPhoneDevice();
        }

        public DeviceInformation SelectedCamera
        {
            get
            {
                return _cameraDevice;
            }

            set
            {
                if(value == null)
                {
                    //camera cant be null
                    throw new NotSupportedException();
                }

                if (State == TouchcastComposerServiceState.Recording ||
                    State == TouchcastComposerServiceState.Paused)
                {
                    //can't change camera and mic while recording is in progress
                    throw new NotSupportedException();  
                }

                _cameraDevice = value;
                _settingsService.SaveToSettings(_selectedCameraSettingsKey, _cameraDevice.Id);
            }
        }

        public DeviceInformation SelectedMicrophone
        {
            get
            {
                 return _micDevice;
            }
            set
            {
                if (value == null)
                {
                    //camera cant be null
                    throw new NotSupportedException();
                }

                if (State == TouchcastComposerServiceState.Recording ||
                    State == TouchcastComposerServiceState.Paused)
                {
                    //can't change camera and mic while recording is in progress
                    throw new NotSupportedException();
                }

                _micDevice = value;
                _settingsService.SaveToSettings(_selectedMicrophoneSettingsKey, _micDevice.Id);
            }
        }
        
        public DeviceInformationCollection AvailableCameras
        {
            get
            {
                return _availableCameras;
            }
        }

        public DeviceInformationCollection AvailableMicrophones
        {
            get
            {
                return _availableMicrophones;
            }
        }

        private double _shadowVolumePercent;
        public double MicrophoneVolumeLevel
        {
            get
            {
                return _mediaCapture?.AudioDeviceController?.VolumePercent ?? _shadowVolumePercent;
            }
            set
            {
                _shadowVolumePercent = value;
                _mediaCapture.AudioDeviceController.VolumePercent = (float)value;
            }
        }

        private void RefreshCameraPreviewOrientationAndMirroring()
        {
            if (_cameraDevice != null)
            {
                //camera image should be flipped in all cases, except camera is placed on the back panel of the device
                if (_cameraDevice.EnclosureLocation != null && _cameraDevice.EnclosureLocation != null && _cameraDevice.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Back)
                {
                    ComposingContext.Shared().CameraPreviewFlippedHorizontally = false;
                }
                else
                {
                    ComposingContext.Shared().CameraPreviewFlippedHorizontally = true;
                }
            }

            TouchCastComposingEngine.BitmapRotation previewRotation = TouchCastComposingEngine.BitmapRotation.NoRotation;

            //it makes sense to rotate camera if it's placed in the 
            //enclosure of the device, no need to rotate remotelly connected camera
            if (_cameraDevice?.EnclosureLocation != null)
            {
                switch (DisplayInformation.GetForCurrentView().CurrentOrientation)
                {
                    case DisplayOrientations.None:
                    case DisplayOrientations.Landscape:
                        previewRotation = TouchCastComposingEngine.BitmapRotation.NoRotation;
                        break;

                    case DisplayOrientations.Portrait:
                        previewRotation = TouchCastComposingEngine.BitmapRotation.Rotation90DegreesCCW;
                        break;

                    case DisplayOrientations.LandscapeFlipped:
                        previewRotation = TouchCastComposingEngine.BitmapRotation.Rotation180DegreesCCW;
                        break;

                    case DisplayOrientations.PortraitFlipped:
                        previewRotation = TouchCastComposingEngine.BitmapRotation.Rotation270DegreesCCW;
                        break;
                }
            }
            ComposingContext.Shared().CameraPreviewRotation = previewRotation;
        }

        #endregion

        #region Callbacks

        bool _isFailed;
        //method is called when capturing failed
        private void MediaCaptureFailed(MediaCapture sender, MediaCaptureFailedEventArgs errorEventArgs)
        {
            if (_isFailed) return;
            _isFailed = true;
            if (errorEventArgs.Code == _permissionsDeniedAccesCode)
            {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () =>
                {
                    State = TouchcastComposerServiceState.DoNotHasAccess;
                });
            }
            else if (errorEventArgs.Code == _deviceLockCode)
            {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.TryRunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () =>
                {
                    State = TouchcastComposerServiceState.DeviceLock;
                });
            }
            _isFailed = false;
            _mediaCapture.Failed -= MediaCaptureFailed;
        }

        private void OnDisplayOrientationChanged(DisplayInformation sender, object args)
        {
            RefreshCameraPreviewOrientationAndMirroring();
        }

        private void SwapChainReadyEvent()
        {
            _swapchain = ComposingContext.Shared().CanvasSwapChain as CanvasSwapChain;
            CanvasSwapChainReadyEvent?.Invoke(this, _swapchain);
        }
        
        private async void Camera_Added(DeviceWatcher sender, DeviceInformation args)
        {
            await RefreshCameraDevicesAsync();
        }

        private async void Camera_Removed(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            await RefreshCameraDevicesAsync();
        }

        private async void Microphone_Added(DeviceWatcher sender, DeviceInformation args)
        {
            await RefreshMicrophoneDeviceAsync();
        }

        private async void Microphone_Removed(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            await RefreshMicrophoneDeviceAsync();
        }

        #endregion

        #region Private help methods

        private async Task FilterCameraEncodingProperties()
        {
            /*int minWidth = 640;
            int maxWidth = (int)VideoSize.Width;
            int minFPS = 15;
            int maxFPS = 30;
            double aspectRatio = (double)VideoSize.Width / (double)VideoSize.Height;

            var currentEncodingProperties = _mediaCapture.VideoDeviceController.GetMediaStreamProperties(MediaStreamType.VideoRecord) as VideoEncodingProperties;
            Helpers.Logger.LogQueue.WriteToFile($"Current Video Encoding Properties: [Width {currentEncodingProperties.Width}],[Height {currentEncodingProperties.Height}],[FrameRate {currentEncodingProperties.FrameRate.Numerator}]");

            try
            {
                var allAvailablePresets = _mediaCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoRecord).ToList().Cast<VideoEncodingProperties>();

                var filteredPresets = from encodingProperty in allAvailablePresets
                                      where encodingProperty.Width >= minWidth && encodingProperty.Width <= maxWidth
                                      && encodingProperty.FrameRate.Numerator >= minFPS && encodingProperty.FrameRate.Numerator <= maxFPS
                                      orderby encodingProperty.Width descending, Math.Abs((double)encodingProperty.Width / encodingProperty.Height - aspectRatio) ascending, encodingProperty.FrameRate.Numerator descending
                                      select encodingProperty;

                var selectedPreset = filteredPresets.FirstOrDefault();

                if (selectedPreset != null)
                {
                    Helpers.Logger.LogQueue.WriteToFile($"New Video Encoding Properties: [Width {selectedPreset.Width}],[Height {selectedPreset.Height}],[FrameRate {selectedPreset.FrameRate.Numerator}]");
                    await _mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.VideoPreview, selectedPreset);
                    await _mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.VideoRecord, selectedPreset);
                }
            }
            catch (Exception e)
            {
                Helpers.Logger.LogQueue.WriteToFile(e.Message);
            }*/
            const int minFps = 20;
            const double aspectRation = 16.0 / 9.0;
            const int minWidth = 960;
            const int maxWidth = 1280;
            const double delta = 0.01;
            var currentEncodingProperties = _mediaCapture.VideoDeviceController.GetMediaStreamProperties(MediaStreamType.VideoRecord) as VideoEncodingProperties;
            Helpers.Logger.LogQueue.WriteToFile($"Current Video Encoding Properties: [Width {currentEncodingProperties.Width}],[Height {currentEncodingProperties.Height}],[FrameRate {currentEncodingProperties.FrameRate.Numerator}]");
            if (currentEncodingProperties == null || currentEncodingProperties.Width < minWidth || currentEncodingProperties.FrameRate.Numerator < minFps)
            {
                try
                {
                    var mediaStreamProperties = _mediaCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoRecord).ToList().Cast<VideoEncodingProperties>();
                    //foreach (var item in mediaStreamProperties)
                    //{
                    //    Helpers.Logger.LogQueue.WriteToFile($"Available Video Encoding Properties: [Width {item.Width}],[Height {item.Height}],[FrameRate {item.FrameRate.Numerator}]");
                    //}
                    var encodingProperties = from encodingPropertie in mediaStreamProperties
                                             where encodingPropertie.Width >= minWidth && encodingPropertie.Width <= maxWidth
                                             && encodingPropertie.FrameRate.Numerator >= minFps
                                             && (Math.Abs(aspectRation - (double)encodingPropertie.Width / encodingPropertie.Height) < delta)
                                             orderby encodingPropertie.Width, (double)encodingPropertie.Width / encodingPropertie.Height, encodingPropertie.FrameRate.Numerator ascending
                                             select encodingPropertie;

                    var recordingResolutionProps = encodingProperties.FirstOrDefault();

                    if (recordingResolutionProps != null)
                    {
                        Helpers.Logger.LogQueue.WriteToFile($"New Video Encoding Properties: [Width {recordingResolutionProps.Width}],[Height {recordingResolutionProps.Height}],[FrameRate {recordingResolutionProps.FrameRate.Numerator}]");
                        await _mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(
                            MediaStreamType.VideoPreview, recordingResolutionProps);
                        await _mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(
                            MediaStreamType.VideoRecord, recordingResolutionProps);


                    }
                }
                catch (Exception e)
                {
                    Helpers.Logger.LogQueue.WriteToFile(e.Message);
                    // ignored
                }
            }

        }

        private void UpdateCameraResolution()
        {
            var currentEncodingProperties = _mediaCapture.VideoDeviceController.GetMediaStreamProperties(MediaStreamType.VideoRecord) as VideoEncodingProperties;
            ComposingContext.Shared().ChromaKeyEffect.CameraResolution = new Windows.Foundation.Size(currentEncodingProperties.Width, currentEncodingProperties.Height);
        }

        private async Task CreateVideoThumbnail()
        {
            if (State == TouchcastComposerServiceState.Recording || State == TouchcastComposerServiceState.Paused)
            {
                _videoThumbnailFile = await App.TemporaryFolder.CreateFileAsync(ThumbnailFileName, CreationCollisionOption.ReplaceExisting);
                try
                {
                    //throw new Exception();
                    MediaClip clip = await MediaClip.CreateFromFileAsync(_recordingFile);
                    MediaComposition composition = new MediaComposition();
                    composition.Clips.Add(clip);


                    using (var thumbnailStream = await composition.GetThumbnailAsync(TimeSpan.FromSeconds(composition.Duration.TotalSeconds / 2), (int)VideoSize.Width, (int)VideoSize.Height, VideoFramePrecision.NearestFrame))
                    {
                        var buffer = new Windows.Storage.Streams.Buffer((uint)thumbnailStream.Size);
                        await thumbnailStream.ReadAsync(buffer, (uint)thumbnailStream.Size, InputStreamOptions.None);

                        await FileIO.WriteBufferAsync(_videoThumbnailFile, buffer);
                    }

                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"process thumbnail error : {ex.Message}");
                    var defaultVideoThumbnail = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(@"Assets\defaultVideoThumbnail.png");
                    _videoThumbnailFile = await defaultVideoThumbnail.CopyAsync(App.TemporaryFolder, ThumbnailFileName, NameCollisionOption.ReplaceExisting);
                }
            }
            else
            {
                var defaultVideoThumbnail = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(@"Assets\defaultVideoThumbnail.png");
                _videoThumbnailFile = await defaultVideoThumbnail.CopyAsync(App.TemporaryFolder, ThumbnailFileName, NameCollisionOption.ReplaceExisting);
            }

        }

        private async Task RefreshCameraDevicesAsync()
        {        
            _availableCameras = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);
        }
        private async Task UpdateCurrentCameraDevice()
        {
            await RefreshCameraDevicesAsync();
            _cameraDevice = null;
            String cameraId = _settingsService.LoadFromSettings<String>(_selectedCameraSettingsKey);

            if (!String.IsNullOrEmpty(cameraId))
            {
                foreach (DeviceInformation info in _availableCameras)
                {
                    if (info.Id == cameraId)
                    {
                        _cameraDevice = info;
                        break;
                    }
                }
            }

            if (_cameraDevice == null)
            {
                _cameraDevice = _availableCameras.FirstOrDefault(x => x.EnclosureLocation != null &&
                                                                 x.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Front);
            }

            if (_cameraDevice == null)
            {
                _cameraDevice = _availableCameras.FirstOrDefault();
            }
        }
        private async Task UpdateCurrentMicroPhoneDevice()
        {
            await RefreshMicrophoneDeviceAsync();
            _micDevice = null;
            String micId = _settingsService.LoadFromSettings<String>(_selectedMicrophoneSettingsKey);

            if (!String.IsNullOrEmpty(micId))
            {
                foreach (DeviceInformation info in _availableMicrophones)
                {
                    if (info.Id == micId)
                    {
                        _micDevice = info;
                        break;
                    }
                }
            }

            if (_micDevice == null)
            {
                _micDevice = _availableMicrophones.FirstOrDefault();
            }
        }
       private async Task RefreshMicrophoneDeviceAsync()
        {           
            _availableMicrophones = await DeviceInformation.FindAllAsync(DeviceClass.AudioCapture);          
        }

        private void StartDeviceWatching()
        {
            _cameraWatcher.Added += Camera_Added;
            _cameraWatcher.Removed += Camera_Removed;
            _microphoneWatcher.Added += Microphone_Added;
            _microphoneWatcher.Removed += Microphone_Removed;

            StartWatcher(_cameraWatcher);
            StartWatcher(_microphoneWatcher);
        }

        private void StopDeviceWatching()
        {
            _cameraWatcher.Added -= Camera_Added;
            _cameraWatcher.Removed -= Camera_Removed;
            _microphoneWatcher.Added -= Microphone_Added;
            _microphoneWatcher.Removed -= Microphone_Removed;

            StopWatcher(_cameraWatcher);
            StopWatcher(_microphoneWatcher);
        }

        private void StartWatcher(DeviceWatcher watcher)
        {
            if (watcher.Status == DeviceWatcherStatus.Created
                || watcher.Status == DeviceWatcherStatus.Stopped
                || watcher.Status == DeviceWatcherStatus.Aborted)
            {
                watcher.Start();
            }
        }

        private void StopWatcher(DeviceWatcher watcher)
        {
            if (watcher.Status == DeviceWatcherStatus.Started
                || watcher.Status == DeviceWatcherStatus.EnumerationCompleted)
            {
                watcher.Stop();
            }
        }

        #endregion
    }
}
