﻿using System;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas;
using Windows.Graphics.Imaging;
using Windows.Devices.Enumeration;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace Pitch.Services
{
    public enum TouchcastComposerServiceState
    {
        Idle,
        ComposingModeDefined,
        Previewing,
        Recording,
        Paused,
        Completed,
        NoDevices,
        DoNotHasAccess,
        Failed,
        DeviceLock,
        UnhandledFail
    } 

    public enum TouchcastComposerServiceMode
    {
        UndefinedComposingMode,
        CameraComposingMode,
        VideoComposingMode
    }

    public delegate void TouchcastComposerServiceStateChanged(object sender);
    public delegate void TouchcastComposerServiceVideoDurationChanged(object sender);

    public delegate void TouchcastComposerServiceCameraPreviewFrameAvailable(object sender, CanvasBitmap cameraPreviewFrame);
    public delegate void TouchcastComposerServiceCameraPreviewDeviceChanged(object sender, CanvasDevice previewDevice);
    public delegate void TouchcastComposerServiceSwapChainReadyEvent(object sender, CanvasSwapChain swpchain);
    public delegate void TouchcastComposerServiceWillStartRecording(object sender);
    //public delegate IList<VideoEffectComposerElementInfo> TouchcastComposerServiceUpdateRenderableItems(object sender);

    public interface IMediaMixerService
    {
        //state control
        TouchcastComposerServiceState State { get; }

        Task StartPreviewingAsync();
        Task StartRecordingAsync();
        Task PauseRecordingAsync();
        Task StopPreviewingAsync();
        Task StopRecordingAsync();
        Task HardStopRecordingAsync();
        Task Reset();
        TouchcastComposerServiceMode ComposerMode { get; }

        void InitWithCamera();
        Microsoft.Graphics.Canvas.UI.Xaml.CanvasSwapChainPanel InitWithCamera(int a);
        //Task<MediaElement> InitWithVideo();

        event TouchcastComposerServiceStateChanged StateChangedEvent;
        event TouchcastComposerServiceSwapChainReadyEvent CanvasSwapChainReadyEvent;
        //proividing list of the renderable items to the composer service
        // event TouchcastComposerServiceUpdateRenderableItems UpdateRenderableItemsEvent;
        CanvasDevice CameraPreviewDevice { get; }


        //@FIXME: Remove that event. Use state change istead
        event TouchcastComposerServiceWillStartRecording WillStartRecordingEvent;

        //getting touchcast info
        StorageFile VideoFile { get; }
        StorageFile VideoThumbnailFile { get; }
        TimeSpan VideoDuration { get; }
        BitmapSize ResultVideoSize { get; }
        TimeSpan RecordedVideoFileDuration { get; }
        //getting available devices list
        Task RefreshAvailableCapturingDevicesAsync();
        CanvasSwapChain SwapChain { get; }

        DeviceInformation SelectedCamera { get; set; }
        DeviceInformation SelectedMicrophone { get; set; }
        double MicrophoneVolumeLevel { get; set; }

        DeviceInformationCollection AvailableCameras { get; }
        DeviceInformationCollection AvailableMicrophones { get; }
    }
}
