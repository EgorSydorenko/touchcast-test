﻿namespace Pitch.Services
{
    public class CameraService : ICameraService
    {
        private CameraMode mode;

        public event CameraServiceEvent CameraServiceWillChangeCameraMode;
        public event CameraServiceEvent CameraServiceDidChangeCameraMode;

        public CameraMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                if (mode != value)
                {
                    CameraServiceWillChangeCameraMode?.Invoke(new CameraServiceEventArgs(mode, value));
                    mode = value;
                    CameraServiceDidChangeCameraMode?.Invoke(new CameraServiceEventArgs(mode, mode));
                }
            }
        }

#if DEBUG
        ~CameraService()
        {
            System.Diagnostics.Debug.WriteLine("********************CameraService Destructor********************");
        }
#endif

        public CameraService()
        {
            Mode = CameraMode.Floating;
        }

        public void Reset()
        {
            
        }
    }
}
