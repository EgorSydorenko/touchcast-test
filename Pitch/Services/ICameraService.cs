﻿using System;

namespace Pitch.Services
{
    public enum CameraMode
    {
        None,
        Pip,
        Halfscreen,
        Floating,
        Fullscreen
    }

    public class CameraServiceEventArgs : EventArgs
    {
        public CameraMode OldMode { get; }
        public CameraMode NewMode { get; }

        public CameraServiceEventArgs(CameraMode oldMode, CameraMode newMode)
        {
            OldMode = oldMode;
            NewMode = newMode;
        }
    }

    public delegate void CameraServiceEvent(CameraServiceEventArgs args);

    public interface ICameraService
    {
        event CameraServiceEvent CameraServiceWillChangeCameraMode;
        event CameraServiceEvent CameraServiceDidChangeCameraMode;

        CameraMode Mode { get; set; }

        void Reset();
    }
}
