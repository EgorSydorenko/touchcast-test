﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
//using TouchCast.Services.Data;

namespace Pitch.Services
{
    public class TouchCastService : ITouchCastService
    {
    //    public static Uri BaseUri { get; set;  } = new Uri("http://unilever-vip.touchcast.com/client_api/");

    //    /// <summary>
    //    /// Helper method for simplifying GET requests
    //    /// </summary>
    //    /// <typeparam name="T"></typeparam>
    //    /// <param name="uriPart"></param>
    //    /// <param name="parameters"></param>
    //    /// <param name="token"></param>
    //    /// <returns></returns>
    //    private static async Task<T> GetRequestAsync<T>(string uriPart, Dictionary<string, string> parameters, CancellationToken token)
    //    {
    //        var client = new HttpClient();
    //        var content = new Dictionary<string, string>
    //        {
    //            { "ver","1" },
    //            {"apikey","123" },
    //            {"client","web" },
    //            {"output","json" }
    //        }.Union(parameters);

    //        var queryParameters = from kvp in content
    //                              select string.Format("{0}={1}", WebUtility.UrlEncode(kvp.Key), WebUtility.UrlEncode(kvp.Value));
    //        var uri = new Uri(BaseUri, uriPart + "?" + String.Join("&", queryParameters));
    //        var message = await client.GetAsync(uri, token);
    //        var json = await message.Content.ReadAsStringAsync();
    //        return await Task.Run(() => JsonConvert.DeserializeObject<T>(json), token);
    //    }

    //    /// <summary>
    //    /// Helper method for simplifying POST requests
    //    /// </summary>
    //    /// <typeparam name="T"></typeparam>
    //    /// <param name="uriPart"></param>
    //    /// <param name="parameters"></param>
    //    /// <param name="token"></param>
    //    /// <returns></returns>
    //    private static async Task<T> PostRequestAsync<T>(string uriPart, Dictionary<string, string> parameters, CancellationToken token)
    //    {
    //        var client = new HttpClient();
    //        var uri = new Uri(BaseUri, uriPart);
    //        var content = new FormUrlEncodedContent(new Dictionary<string, string>
    //        {
    //            { "ver","1" },
    //            {"apikey","123" },
    //            {"client","web" },
    //            {"output","json" }
    //        }.Union(parameters));

    //        var message = await client.PostAsync(uri, content, token);
    //        var json = await message.Content.ReadAsStringAsync();
    //        return await Task.Run(() => JsonConvert.DeserializeObject<T>(json), token);
    //    }

    //    public Task<RootResponse<LoginResponse>> LoginAsync(string email, string password, CancellationToken token)
    //    {
    //        return PostRequestAsync<RootResponse<LoginResponse>>("login", new Dictionary<string, string>
    //        {
    //            {"email", email},
    //            {"password", password}
    //        }, token);
    //    }

    //    public async Task<RootResponse<AvailableVappsResponse>> GetAvailableVappsAsync(CancellationToken token)
    //    {
    //        var client = new HttpClient();
    //        var queryParameters =
    //            from kvp in new Dictionary<string, string>
    //            {
    //                {"apikey", "123"},
    //                {"client", "web"},
    //                {"output", "json"}
    //            }
    //            select string.Format("{0}={1}", WebUtility.UrlEncode(kvp.Key), WebUtility.UrlEncode(kvp.Value));

    //        var uri = new Uri(BaseUri, "available-vapps" + "?" + string.Join("&", queryParameters));

    //        var json = await client.GetStringAsync(uri);
    //        return await Task.Run(() => JsonConvert.DeserializeObject<RootResponse<AvailableVappsResponse>>(json), token);
    //    }

    //    public async Task<RootResponse<RegisterResponse>> RegisterAsync(string displayName, string password, string email, string channel, CancellationToken token)
    //    {
    //        var client = new HttpClient();
    //        var uri = new Uri(BaseUri, $@"register?xml=<registration><registration-data type=""email-registration-form""><password>{password}</password><display-name><![CDATA[{displayName}]]></display-name><email><![CDATA[{email}]]></email><channel-name><![CDATA[{channel}]]></channel-name></registration-data></registration>&ver=1&apikey=123&client=web&output=json");

    //        var stringContent = new StringContent(string.Empty);
    //        var message = await client.PostAsync(uri, stringContent, token);
    //        var json = await message.Content.ReadAsStringAsync();
    //        return await Task.Run(() => JsonConvert.DeserializeObject<RootResponse<RegisterResponse>>(json), token);
    //    }

    //    public Task<RootResponse<PerformanceResponse>> InitializePerformanceAsync(string sessionId, CancellationToken token)
    //    {
    //        return PostRequestAsync<RootResponse<PerformanceResponse>>("initialize-performance", new Dictionary<string, string>
    //        {
    //            {"session_id", sessionId}
    //        }, token);
    //    }

    //    public Task<RootResponse<AwsAccessResponse>> RequestUploadingPermissions(string sessionId, int performanceId, CancellationToken token)
    //    {
    //        return PostRequestAsync<RootResponse<AwsAccessResponse>>("request-uploading-permissions", new Dictionary<string, string>
    //        {
    //            { "session_id", sessionId },
    //            { "performance_id", performanceId.ToString() }
    //        }, token);
    //    }

    //    public Task<RootResponse<BaseInnerResponse>> FinalizeUploadedPerformanceAsync(string sessionId, int performanceId, CancellationToken token)
    //    {
    //        return GetRequestAsync<RootResponse<BaseInnerResponse>>("finalize-uploaded-performance", new Dictionary<string, string>
    //        {
    //            { "session_id", sessionId },
    //            { "performance_id", performanceId.ToString() }
    //        }, token);
    //    }

    //    public Task<RootResponse<PerformanceSearchResultResponse>> SearchPerformancesAsync(string sessionId, int performanceId, CancellationToken token)
    //    {
    //        return GetRequestAsync<RootResponse<PerformanceSearchResultResponse>>("search-performance", new Dictionary<string, string>
    //        {
    //            { "session_id", sessionId },
    //            { "performance_id", performanceId.ToString() },
    //            { "sorting", "own" }
    //        }, token);
    //    }
    }
}
