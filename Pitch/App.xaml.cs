﻿
using GalaSoft.MvvmLight.Messaging;
using Microsoft.HockeyApp;
using Newtonsoft.Json;
using Pitch.DataLayer;
using Pitch.DataLayer.Helpers;
using Pitch.Helpers.Exceptions;
using Pitch.Helpers.Frames;
using Pitch.Helpers.Logger;
using Pitch.Models;
using Pitch.Services;
using Pitch.UILayer.Authoring.Views;
using Pitch.UILayer.Helpers.Messages;
using Pitch.UILayer.IntroPage.Views;
using Pitch.UILayer.Player.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TfSdk;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;

namespace Pitch
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App
    {
        private const string AppFileOpenErrorDialogMessage = "AppFileOpenErrorDialogMessage";
        private const string AppFileOpenErrorDialogTitle = "AppFileOpenErrorDialogTitle";
        private const string AppFileOpenErrorDialogOkButton = "AppFileOpenErrorDialogOkButton";
        private const string AppFile = "AppFile";

        private const string TemporaryFolderName = "Temp";
        private const string LogsFolderName = "Logs";
        private const string AppConfigFile = "AppConfig";
        private const string FabricConfigFileName = "EndpointConfig.json";

        #region App Settings

        public static FabricEndpoint FabricEndpointData { private set; get; } 
        public const string VideoBlocksPublicKey = "VideoBlocksPublicKey";
        public const string VideoBlocksPrivateKey = "VideoBlocksPrivateKey";
        public const string Product = "Product";
        public const string AppId = "app_id";
        //#endif
        #endregion

        public static List<TouchcastFrame> TouchcastFrames;
        public static TouchcastFrame Frame => Window.Current.Content as TouchcastFrame;
        public static DataLayer.GlobalManager Locator { get; private set; }
        public static ResourceLoader AppSettings { get; private set; }
        public static StorageFolder TemporaryFolder { get; private set; }
        public static StorageFolder ThemesFolder { get; private set; }
        public static StorageFolder LogsFolder { get; private set; }
        public static StorageFile DefaultBackgroundFile { get; private set; }

        public static string DeviceId { private set; get; }
        public static string DeviceName { private set; get; }
        public static string DeviceInfo { private set; get; }
        public static string ApplicationVersion { private set; get; }
        public static string GoogleAnalyticsClientId { private set; get; }
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            GoogleAnalyticsClientId = "UA-116607927-4";
            InitializeHockeyapp();
            InitializeComponent();
            UnhandledException += App_UnhandledException;
            Suspending += OnSuspending;
        }

        private void InitializeHockeyapp()
        {
            String hockeyappKey = String.Empty;

#if (DISTRIBUTION_CHANNEL_DEV)
            hockeyappKey = "12bc828b22d84585a6f5072e27ef897f";
#endif
#if (DISTRIBUTION_CHANNEL_RELEASE)
            hockeyappKey = "5ef9cf7eb449474381452ec801282597";
            GoogleAnalyticsClientId = "UA-116607927-2";
#endif
#if (DISTRIBUTION_CHANNEL_DEMO)
            hockeyappKey = "59b93d99c98d4ca28fb9f71c3e90601f";
#endif
            if (hockeyappKey.Length > 0)
            {
                HockeyClient.Current.Configure(hockeyappKey);
            }
        }

        private void App_UnhandledException(object sender, Windows.UI.Xaml.UnhandledExceptionEventArgs e)
        {
            LogQueue.WriteToFile(e.Exception.GetExceptionDetails());
            while (!LogQueue.IsEmpty) { }
            e.Handled = true;
        }

        protected override void OnActivated(IActivatedEventArgs args)
        {
            if (args.Kind == ActivationKind.Protocol)
            {
                // Retrieves the activation Uri.
                var protocolArgs = (ProtocolActivatedEventArgs)args;
                var uri = protocolArgs.Uri;

                var frame = Window.Current.Content as TouchcastFrame ??
                            new TouchcastFrame(FrameType.MainFrame, Guid.NewGuid().ToString());

                AppSettings = ResourceLoader.GetForCurrentView(AppConfigFile);
                // Ensure the current window is active
                Window.Current.Activate();
            }
        }

        protected override void OnBackgroundActivated(BackgroundActivatedEventArgs args)
        {
            base.OnBackgroundActivated(args);
            TouchCastComposingEngine.Win32BackgroundPipe.OfficeConverter.Initialize(args);
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs e)
        {
            if (e.PreviousExecutionState == ApplicationExecutionState.Running) return;

            Locator = Current.Resources["Globalmanager"] as DataLayer.GlobalManager;
            await InitAppInfo();
            await StartBackgroundServices();
            await InitFolders();

            Helpers.Analytics.GoogleAnalyticsManager.TrySend(Helpers.Analytics.AnalyticCommandType.AnalyticCommandStart);
            LogQueue.WriteToFile(DeviceInfo);
            LogQueue.WriteToFile(ApplicationVersion);

            await TryToRestoreProject();

            AppSettings = ResourceLoader.GetForCurrentView(AppConfigFile);

            //removing ContentDialog border for the content dialog
            Current.Resources["ContentDialogBorderWidth"] = new Thickness(0);

            var cacheItems = await ApplicationData.Current.LocalCacheFolder.GetItemsAsync();
            foreach (var item in cacheItems)
            {
                await item.DeleteAsync();
            }

            var frame = Window.Current.Content as TouchcastFrame;
            //ApplicationView.PreferredLaunchViewSize = new Size(1000, 800);
            //ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            

            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(769, 469));
            //// Do not repeat app initialization when the Window already has content,
            //// just ensure that the window is active
            if (frame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                frame = new TouchcastFrame(FrameType.MainFrame, Guid.NewGuid().ToString());

                frame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }
                // Place the frame in the current Window
                Window.Current.Content = frame;
            }
            SetupTitleBarColor();

            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = false;
            if (frame.Content == null)
            {
                Locator.ComposingMode = TouchcastComposerServiceMode.CameraComposingMode;
                frame.Navigate(typeof(SignInPage), (Locator.ProjectFile != null), new Windows.UI.Xaml.Media.Animation.SuppressNavigationTransitionInfo());
            }


            // Ensure the current window is active
            Window.Current.Activate();

            LogQueue.WriteToFile(LogQueue.VideoInfo);
            var settingsService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<ISettingsService>();
            LogQueue.WriteToFile($"Is device strong: {settingsService.IsDeviceStrong}");
            LogQueue.WriteToFile($"User light mode permission: {settingsService.LightModeUserPermission}");
            Shortcut.Manager.Instance.Init();

            LogQueue.WriteToFile("OnLaunched completed");
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            LogQueue.WriteToFile("OnSuspending");
            var deferral = e.SuspendingOperation.GetDeferral();

            try
            {
                if (GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.IsRegistered<AuthoringSession>())
                {
                    var authSession = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<AuthoringSession>();
                    if (authSession?.Project != null)
                    {
                        await authSession.Project.FastSave();
                        await authSession.Project.WaitIoOperationCompleted();
                        while (!LogQueue.IsEmpty)
                        {
                            await Task.Delay(10);
                        }
                    }
                }
            }
            finally
            {
                deferral.Complete();
            }
        }

        public static bool HasConnection()
        {
            var connectionProfile = NetworkInformation.GetInternetConnectionProfile();
            return connectionProfile != null && connectionProfile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
        }

        protected override async void OnFileActivated(FileActivatedEventArgs args)
        {
            await StartBackgroundServices();
            await InitFolders();

            LogQueue.WriteToFile(DeviceInfo);
            LogQueue.WriteToFile(ApplicationVersion);
            LogQueue.WriteToFile("OnFileActivated");

            if (AppSettings == null)
            {
                AppSettings = ResourceLoader.GetForCurrentView(AppConfigFile);
            }

            // Use http://grogansoft.com/blog/?p=1197 as sample
            //await Task.Delay(TimeSpan.FromSeconds(10));
            //removing ContentDialog border for the content dialog
            Current.Resources["ContentDialogBorderWidth"] = new Thickness(0);
            StorageFile file = (StorageFile)args.Files.FirstOrDefault();
            if (Locator == null)
            {
                await StartInSameWindow(file, args);
            }
            else
            {
                await StartInAnotherWindow(file);
            }
            LogQueue.WriteToFile(LogQueue.VideoInfo);
            var settingsService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<ISettingsService>();
            LogQueue.WriteToFile($"Is device strong: {settingsService.IsDeviceStrong}");
            LogQueue.WriteToFile($"User light mode permission: {settingsService.LightModeUserPermission}");

            Shortcut.Manager.Instance.Init();

            base.OnFileActivated(args);
        }

        private async Task StartInAnotherWindow(StorageFile file)
        {
            LogQueue.WriteToFile($"StartInAnotherWindow {file.FileType}");
            if (file.FileType == Helpers.FileToAppHelper.TouchcastExtension)
            {
                await OpenTctInAnotherWindow(file);
            }
            else if (Helpers.FileToAppHelper.ProjectExtensions.Contains(file.FileType))
            {
                string token = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.Add(file);
                Messenger.Default.Send(new OpenNewProjectMessage(token));
            }
        }

        public async Task OpenTctInAnotherWindow(StorageFile tctFile)
        {
            var result = await PrepareTouchcast(tctFile);
            if (result.Item2 == null)
            {
                Touchcast touchcast = result.Item1;
                if (touchcast.isWithVideo)
                {
                    ApplicationView currentAppView = ApplicationView.GetForCurrentView();
                    CoreApplicationView newApplicationView = CoreApplication.CreateNewView();
                    await newApplicationView.Dispatcher.TryRunAsync(
                        CoreDispatcherPriority.Normal,
                        async () =>
                        {
                            Window newWindow = Window.Current;
                            ApplicationView newAppView = ApplicationView.GetForCurrentView();
                            newAppView.Title = "Player";
                            TouchcastFrame frame = PrepareFrame(touchcast, FrameType.SecondaryFrame);
                            newWindow.Content = frame;
                            Locator.RegisterAuthoringSession();
                            frame.Navigate(typeof(ReviewVideoPage), false);
                            newAppView.Consolidated += NewAppView_Consolidated;
                            newWindow.Activate();
                            SetupTitleBarColor();

                            await ApplicationViewSwitcher.TryShowAsStandaloneAsync(
                                newAppView.Id,
                                ViewSizePreference.UseMinimum,
                                currentAppView.Id,
                                ViewSizePreference.UseMinimum);
                        });
                }
            }
            else
            {
                await ShowFileOpenErrorMessageAsync(tctFile?.Name);
            }
        }

        public static async Task ShowFileOpenErrorMessageAsync(string fileName)
        {
            var loader = new ResourceLoader();
            var message = loader.GetString(AppFileOpenErrorDialogMessage);
            var title = loader.GetString(AppFileOpenErrorDialogTitle);
            var okLabel = loader.GetString(AppFileOpenErrorDialogOkButton);
            var file = loader.GetString(AppFile);
            fileName = (String.IsNullOrEmpty(fileName) || String.IsNullOrWhiteSpace(fileName)) ? file : $"'{fileName}'";
            var dialog = new MessageDialog(String.Format(message, fileName), title);
            dialog.Commands.Add(new UICommand
            {
                Label = okLabel
            });
            await dialog.ShowAsync();
        }

        private void NewAppView_Consolidated(ApplicationView sender, ApplicationViewConsolidatedEventArgs args)
        {
            if (!CoreApplication.GetCurrentView().IsMain)
            {
                if (Window.Current.Content is TouchcastFrame)
                {
                    var frame = Window.Current.Content as TouchcastFrame;
                    if (frame.Content is ReviewVideoPage)
                    {
                        var reviewPage = frame.Content as ReviewVideoPage;
                        reviewPage.Clean();
                        Window.Current.Close();
                    }
                }
            }
        }

        private async Task StartInSameWindow(StorageFile file, FileActivatedEventArgs args)
        {
            LogQueue.WriteToFile($"StartInSameWindow {file.FileType}");

            if (file.FileType == Helpers.FileToAppHelper.TouchcastExtension)
            {
                var frame = await PrepareFrame(file, FrameType.SecondaryFrame);
                if (frame != null)
                {
                    Window.Current.Content = frame;
                    Locator = Current.Resources["Globalmanager"] as GlobalManager;
                    await InitAppInfo();
                    if (Touchcast.Instances[frame.Id].isWithVideo)
                    {
                        if (Locator != null)
                        {
                            Locator.RegisterAuthoringSession();
                            frame.Navigate(typeof(ReviewVideoPage), false);
                        }
                    }
                    else
                    {
                        frame.Type = FrameType.MainFrame;
                        if (Locator != null)
                        {
                            Locator.RegisterAuthoringSession();
                            Locator.ComposingMode = TouchcastComposerServiceMode.CameraComposingMode;
                        }
                        frame.Navigate(typeof(AuthoringPage), false);
                        TouchcastFrames = new List<TouchcastFrame>();
                        TouchcastFrames.Add(frame);
                    }
                    
                    Window.Current.Activate();
                    SetupTitleBarColor();
                    CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = false;
                }
            }
            else if (Helpers.FileToAppHelper.ProjectExtensions.Contains(file.FileType))
            {
                Helpers.Analytics.GoogleAnalyticsManager.TrySend(Helpers.Analytics.AnalyticCommandType.AnalyticCommandStart);
                var frame = new TouchcastFrame(FrameType.MainFrame, Guid.NewGuid().ToString());
                Locator = Current.Resources["Globalmanager"] as GlobalManager;
                var cacheItems = await ApplicationData.Current.LocalCacheFolder.GetItemsAsync();
                foreach (var item in cacheItems)
                {
                    await item.DeleteAsync();
                }
                var templfiles = await App.TemporaryFolder.GetItemsAsync();
                foreach (var item in templfiles)
                {
                    await item.DeleteAsync();
                }
                if (Locator != null)
                {
                    //Locator.RegisterThemeTemplate();
                }
                await InitAppInfo();
                Locator.ProjectFile = file;
                TouchcastFrames = new List<TouchcastFrame>();
                TouchcastFrames.Add(frame);
                Window.Current.Content = frame;
                Window.Current.Activate();
                SetupTitleBarColor();

                frame.Navigate(typeof(SignInPage), (Locator.ProjectFile != null), new Windows.UI.Xaml.Media.Animation.SuppressNavigationTransitionInfo());

                //    CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = false;
                //    Analytics.GoogleAnalyticsManager.InitTracker();
                //    Analytics.GoogleAnalyticsManager.LoadDeviceInfo();
                //    Analytics.GoogleAnalyticsManager.TrySend(Analytics.AnalyticCommandType.AnalyticCommandLaunch);
            }
            //else
            //{
            //    Exit();
            //}
        }

        private void SetupTitleBarColor()
        {
            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
        }

        private async Task<TouchcastFrame>PrepareFrame(StorageFile file, FrameType type)
        {
           
            var result = await PrepareTouchcast(file);
            TouchcastFrame frame = null; 
            if (result.Item2 == null)
            {
                var touchcast = result.Item1;
                frame = new TouchcastFrame(type, Guid.NewGuid().ToString());
                Touchcast.Instances.Add(frame.Id, touchcast);
            }
            else
            {
                await ShowFileOpenErrorMessageAsync(file?.Name);
            }
            return frame;
        }

        private TouchcastFrame PrepareFrame(Touchcast tc, FrameType type)
        {
            var frame = new TouchcastFrame(type, Guid.NewGuid().ToString());
            Touchcast.Instances.Add(frame.Id, tc);
            return frame;
        }

        private async Task<Tuple<Touchcast, TouchcastException>> PrepareTouchcast(StorageFile file)
        {
            return await Task.Run(async () => { return await Touchcast.OpenArchive(file); });
        }

        private async Task InitAppInfo()
        {
            try
            {
                StorageFile configFile = null;
                try
                {
                    configFile = await ApplicationData.Current.LocalFolder.GetFileAsync(FabricConfigFileName);
                    LogQueue.WriteToFile("Loaded configuration from local file");
                }
                catch(Exception)
                {
                    configFile = null;
                }
                if (configFile == null)
                {
                    configFile = await Package.Current.InstalledLocation.GetFileAsync(FabricConfigFileName);
                    LogQueue.WriteToFile("Loaded default configuration");
                }
                var text = await FileIO.ReadTextAsync(configFile);
                FabricEndpointData = JsonConvert.DeserializeObject<FabricEndpoint>(text);
            }
            catch(Exception ex)
            {
                LogQueue.WriteToFile($"Exception: Configuration parsing error {ex.Message}");
            }
            var result = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily;

            // get the system version number
            var deviceFamilyVersion = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamilyVersion;
            var version = ulong.Parse(deviceFamilyVersion);
            var majorVersion = (version & 0xFFFF000000000000L) >> 48;
            var minorVersion = (version & 0x0000FFFF00000000L) >> 32;
            var buildVersion = (version & 0x00000000FFFF0000L) >> 16;
            var revisionVersion = (version & 0x000000000000FFFFL);
            result += $" {majorVersion}.{minorVersion}.{buildVersion}.{revisionVersion}\n";

            var clientDeviceInformation = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            result += clientDeviceInformation.SystemManufacturer + "\n";
            result += clientDeviceInformation.SystemProductName;

            DeviceInfo = result;

            DeviceName = clientDeviceInformation.FriendlyName;

            Package package = Package.Current;
            PackageId packageId = package.Id;
            PackageVersion pacjageVersion = packageId.Version;

            ApplicationVersion = string.Format("{0}.{1}.{2}.{3}", pacjageVersion.Major, pacjageVersion.Minor, pacjageVersion.Build, pacjageVersion.Revision);

            try
            {
                var settingsService = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<ISettingsService>();
                DeviceId = settingsService.LoadFromSettings<string>(SettingsService.DeviceId);
                if (String.IsNullOrEmpty(DeviceId))
                {
                    var token = Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);

                    var hardwareId = token.Id;
                    var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

                    byte[] bytes = new byte[hardwareId.Length];
                    dataReader.ReadBytes(bytes);

                    DeviceId = BitConverter.ToString(bytes).Replace("-", "");
                    settingsService.SaveToSettings(SettingsService.DeviceId, DeviceId);
                }

            }
            catch
            {

            }
        }

        private async Task InitFolders()
        {
            LogsFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(LogsFolderName, CreationCollisionOption.OpenIfExists);
            TemporaryFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(TemporaryFolderName, CreationCollisionOption.OpenIfExists);

            await ThemeManager.Instance.Initialize();
        }
        
        private async Task TryToRestoreProject()
        {
            StorageFolder workFolder = null;
            var tempFolders = await TemporaryFolder.GetFoldersAsync();
            foreach (var folder in tempFolders)
            {
                var item = await folder.TryGetItemAsync(Project.ProjectInfoFileName);
                if (item != null && item.IsOfType(StorageItemTypes.File))
                {
                    workFolder = folder;
                }
                else
                {
                    await folder.DeleteAsync();
                }
            }

            if (workFolder != null)
            {
                try
                {
                    Locator.ProjectFile = await workFolder.GetFileAsync(Project.ProjectInfoFileName);
                    LogQueue.WriteToFile($"Restore project from temporary folder: {workFolder.Name}");
                }
                catch(Exception ex)
                {
                    LogQueue.WriteToFile($"Exception in App.TryToRestoreProject {ex.Message}");
                }
            }
        }

        public static void SetupTitleBarColorScheme(TitlebarBackgroundColorMode mode = TitlebarBackgroundColorMode.Default)
        {
            try
            {
                string colorKey = String.Empty;
                switch (mode)
                {
                    case TitlebarBackgroundColorMode.Default:
                        colorKey = "TitlebarBackgroundColor";
                        break;
                    case TitlebarBackgroundColorMode.Dark:
                        colorKey = "TitlebarDarkBackgroundColor";
                        break;
                    case TitlebarBackgroundColorMode.SignIn:
                        colorKey = "TitlebarSignInBackgroundColor";
                        break;
                    case TitlebarBackgroundColorMode.White:
                        colorKey = "TitlebarLoginBackgroundColor";
                        break;
                }

                if (String.IsNullOrEmpty(colorKey)) return;
                
                var titleBar = ApplicationView.GetForCurrentView().TitleBar;

                Color? color = Current.Resources[colorKey] as Color?;
                if (color == null)
                {
                    color = Color.FromArgb(0, 0x47, 0x47, 0x47);
                }
                titleBar.ButtonBackgroundColor = color;
                titleBar.InactiveBackgroundColor = color;
                titleBar.ButtonInactiveBackgroundColor = color;

                color = Current.Resources["PivotHeaderHoverBackgroundColor"] as Color?;
                if (color != null)
                {
                    titleBar.ButtonHoverBackgroundColor = color;
                }
            }
            catch (Exception ex)
            {
                LogQueue.WriteToFile($"App SetupTitleBarColorScheme Error: {ex.Message}");
            }
        }
        private static async Task StartBackgroundServices()
        {
            await TouchCastComposingEngine.Win32BackgroundPipe.OfficeConverter.Start();
        }
        public static ApiClientConfiguration PrepareConfiguration()
        {
            var appId = AppSettings.GetString(AppId);
            var product = AppSettings.GetString(Product);

            var info = new ApplicationInfo(appId, DeviceName, product, ApplicationVersion);
            var configuration = new ApiClientConfiguration(FabricEndpointData.login_endpoint,FabricEndpointData.api_endpoint, FabricEndpointData.redirect_endpoint, FabricEndpointData.profile_endpoint, info);
            return configuration;
        }
    }
}
